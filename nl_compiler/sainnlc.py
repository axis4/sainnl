# coding=UTF-8
"""
   Copyright 2014 IoS (progsdi@gmail.com)

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""
import traceback
from nl.services.compile import CompileService

__author__ = 'sergeionov'


import sys

if len(sys.argv) < 2:
    print """Copyright 2014 IoS (progsdi@gmail.com)

This software compiles packages on SAINNL into runtime formats.
Usage: python sainnlc.py package_name [source_file] [-i ide_config] [-t test_config] [-r runtime_config]
    package_name - the name of the compiled package.
    source_file - file of package on SAINNL. STDIN is used if not defined.
    ide_config - file of XML for IDE.
    test_config - file of config for Perl Test system.
    runtime_config - file of JSON for NNVM.

    If no output files defined, then the runtime_config is directed to STDOUT.
"""
    exit(1)

package = sys.argv[1]
conf = {}
key = ""
for s in sys.argv[2:]:
    if s.startswith('-'):
        key = s
    else:
        conf[key] = s
        key = ""

if "" in conf:
    with open(conf[""], "r") as f:
        text = f.read()
else:
    text = sys.stdin.read()

data = {
    "package": package,
    "text": text.replace('\r\n', '\n')
}

#compiler_package = '.'.join(package.split('.')[:-1])

c = CompileService()
ok = True
i = ""
(t, r) = ("", "")
try:
    i = c.compile(data["text"], package)
except Exception, e:
    ok = False
    traceback.print_exc(file=sys.stderr)
    exit(0)
if ok:
    try:
        (t, r) = c.transform(i, data)
    except Exception, e:
        traceback.print_exc()
        exit(0)

data["-i"] = i.replace('<node>', '\n<node>').replace('</nodes>', '\n</nodes>')
data["-t"] = t
data["-r"] = "{\"packageName\":\"" + package + "\",\"blocks\":[" + r[:-1] + "]}"

any = False
for k, v in data.iteritems():
    if k in conf:
        any = True
        with open(conf[k], "w") as f:
            f.write(v)

if not any:
    print data["-r"]