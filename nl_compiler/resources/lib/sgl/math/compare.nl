
equals A, B {
    """
    (A >= B and A <= B) == (A == B)

    param: A: runtime parameter
    param: B: runtime parameter
    returns: RES: single result
    """
	GE = # A - B >= 0, 1
	LE = # B - A >= 0, 1
	RES = # GE + LE >= 2, 1
	out RES
}

great A, B {
    """
    (not A <= B) == (A > B)

    param: A: runtime parameter
    param: B: runtime parameter
    returns: RES: single result
    """
	LE = # B - A >= 0, 1
	RES = # 0 - LE >= 0, 1
	out RES
}

great_or_equal A, B {
    """
    (A >= B) == (A >= B)

    param: A: runtime parameter
    param: B: runtime parameter
    returns: RES: single result
    """
	GE = # A - B >= 0, 1
	RES = # GE >= 1, 1
	out RES
}

less A, B {
    """
    (not A >= B) == (A < B)

    param: A: runtime parameter
    param: B: runtime parameter
    returns: RES: single result
    """
	GE = # A - B >= 0, 1
	RES = # 0 - GE >= 0, 1
	out RES
}

less_or_equal A, B {
    """
    (A <= B) == (A <= B)

    param: A: runtime parameter
    param: B: runtime parameter
    returns: RES: single result
    """
	LE = # B - A >= 0, 1
	RES = # LE >= 1, 1
	out RES
}

not_equals A, B {
    """
    (not A >= B or not A <= B) == (A != B)

    param: A: runtime parameter
    param: B: runtime parameter
    returns: RES: single result
    """
	GE = # A - B >= 0, 1
	LE = # B - A >= 0, 1
	RES = # 1 - GE - LE >= 0, 1
	out RES
}

compare A, B {
    """
    A < B : -1
    A == B: 0
    A > B : 1

    param: A: runtime parameter
    param: B: runtime parameter
    returns: RES: single result
    """
	GE = # A - B >= 0, 1
	LE = # B - A >= 0, 1
	RES = # GE - LE
	out RES
}
