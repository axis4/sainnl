[DIVIDE_STEPS=8]

sum A, B {
    """
    param: A: runtime parameter
    param: B: runtime parameter
    returns: RES: single result
    """
    RES = A + B
    out RES
}

minus A, B {
    """
    param: A: runtime parameter
    param: B: runtime parameter
    returns: RES: single result
    """
    RES = A - B
    out RES
}

multiply A, B {
    """
    param: A: runtime parameter
    param: B: runtime parameter
    returns: RES: single result
    """
    NEG = # 0 - B
    RES = & B + NEG, A, 0
    out RES
}

divide [k=1..DIVIDE_STEPS, pk=1..DIVIDE_STEPS, k == pk + 1] N, D {
    """
    Q = N[k], 1 = D[k]
    N[i+1] = N[i] * (2 - D[i])
    D[i+1] = D[i] * (2 - D[i])
    N[1] = N
    D[1] = D

    param: N: runtime parameter
    param: D: runtime parameter
    returns: N[*]: single result
    """
    "TODO: D => (0, 1]"
    F[k] = # 2 - D[pk]
    DD[pk] = # D[pk]
    DN[pk] = # N[pk]
    NEG[k] = # 0 - F[k]
    MD[k] = & NEG[k] + F[k], DD[pk], 0
    MN[k] = & NEG[k] + F[k], DN[pk], 0
    D[k] =+ # MD[k]
    N[k] =+ # MN[k]
    D[1] =+ # D
    N[1] =+ # N
    out N[3]
}
