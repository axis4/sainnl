
nonzero A {
    """
    A != 0

    param: A: runtime parameter
    returns: RES: single result
    """
    RES = # A, 1
    out RES
}

zero A {
    """
    (1 - (A != 0)) == !(A != 0)

    param: A: runtime parameter
    returns: RES: single result
    """
    NZ = nonzero A
    RES = 1 - NZ
    out RES
}

or A, B {
    """
    ((A != 0) + (B != 0) >= 1) == (A != 0) || (B != 0)

    param: A: runtime parameter
    param: B: runtime parameter
    returns: RES: single result
    """
    P = sgl.math.arithmetic.plus (nonzero A), (nonzero B)
    RES = # P >= 1, 1
    out RES
}

and A, B {
    """
    ((A != 0) + (B != 0) >= 2) == (A != 0) && (B != 0)

    param: A: runtime parameter
    param: B: runtime parameter
    returns: RES: single result
    """
    P = sgl.math.arithmetic.plus (nonzero A), (nonzero B)
    RES = # P >= 2, 1
    out RES
}

xor A, B {
    """
    (((A != 0) || (B != 0)) && !((A != 0) && (B != 0))) == (A != 0) ^^ (B != 0)

    param: A: runtime parameter
    param: B: runtime parameter
    returns: RES: single result
    """
    P = sgl.math.arithmetic.minus (or), (and)
    RES = # P >= 1, 1
    out RES
}
