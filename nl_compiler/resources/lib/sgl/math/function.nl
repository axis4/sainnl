
factorial N {
    """
    Counts factorial(N) iteratively

    param: N: runtime parameter
    returns: RES: single result
    """
    COUNTER = # N + COUNTER >= 1, N + COUNTER - 1
    ACC = # N + TMP
    TMP = # ACC * COUNTER
    FLUSH = # 0 - COUNTER >= 0, 1
    RES = # TMP >= 1, ACC
    out RES
}

max_static [i=1..LEN, j=1..LEN, i!=j, k=1..LEN, i>k, I] I[i] {
    """
    Find maximum value in static array

    param: LEN: size of static array, compile-time parameter
    param: I[*]: static array, allowed for renaming, runtime parameter
    returns: RES: single result
    """
    DELAY[i] = # I[i]
    "comparison layer"
    CMP[i][j] = # I[i] - I[j] >= 0, 1
    "maximums layer"
    MAXS[i] = # CMP[i][j] >= LEN - 1, DELAY[i]
    "blocking to single winner"
    BLOCK[i] = # 0 - MAXS[k] >= 0, MAXS[i]
    RES = # BLOCK[i]
    out RES
}

max [I] COUNT {
    """
    Find maximum value in dynamic array
    Algorithm grows according to the size of dynamic array

    param: I: name of input dynamic array, compile-time parameter
    param: COUNT: amount of items in dynamic array, runtime parameter
    returns: RES: single result
    """
    INC = # STATE - COUNT >= 1, 1
    STATE = # STATE + INC
    [STATE] in I[STATE]
    FROM = # 0 - TO >= 0, STATE
    TO = # FROM

    "compare array values"
    [FROM, TO] CMP[FROM][TO] = # I[FROM] - I[TO] >= 0, 1
    [FROM, TO] MAXS[FROM] = # CMP[FROM][TO] >= STATE, DELAY_I[FROM]

    "multiply found maximums by their index: MAX * INDEX"
    [FROM] DUPS[FROM] = # FROM * MAXI[FROM]

    "compare modified maximums for the maximum with greatest index"
    [FROM, TO] CMP_DUPS[FROM][TO] = # DUPS[FROM] - DUPS[TO] >= 0, 1
    [FROM, TO] MAX_DUPS[FROM] = # CMP_DUPS[FROM][TO] >= STATE, DELAY_DUPS[FROM]

    "build resulting array with only one value set - the maximum one"
    [FROM] RES_DUPS[FROM] = # MAX_DUPS[FROM] > 0, DELAY3_MAXS[FROM]
    [FROM] RES = # RES_DUPS[FROM]
    out RES

    "delays"
    [FROM] DELAY_I[FROM] = # I[FROM]
    [FROM] DELAY_DUPS[FROM] = # DUPS[FROM]
    [FROM] DELAY1_MAXS[FROM] = # MAXS[FROM]
    [FROM] DELAY2_MAXS[FROM] = # DELAY1_MAXS[FROM]
    [FROM] DELAY3_MAXS[FROM] = # DELAY2_MAXS[FROM]
}
