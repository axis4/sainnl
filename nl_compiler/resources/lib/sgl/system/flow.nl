
delay [i = 1..LEN, j = 1..LEN, i == j + 1] FLOW {
    """
    Delays the data flow for synchronization purpose

    param: LEN: length of delay chain, compile-time parameter
    param: FLOW: incoming data flow, runtime parameter
    returns: S[*]: delayed data flow, single result
    """
	"start of delay chain"
	S[1] =+ # FLOW
	"the chain"
	S[i] =+ # S[j]
	"chain termination"
	out S[LEN]
}

suppress [LEN] {
    """
    Generates suppressing rhythm

    param: LEN: length of suppressing chain, compile-time parameter
    returns: OUT: suppression flag (1 - suppress, 0 - pass), single result
    """
	STATE = # STATE - 256 * OUT >= 0, STATE + 1
	OUT = # STATE >= LEN, 1
	out OUT
}
