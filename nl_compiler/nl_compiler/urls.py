from django.conf.urls import patterns, include, url

from django.contrib import admin
from nl_compiler import settings

#admin.autodiscover()

urlpatterns = patterns('',
                       url(r'^$', 'nl.views.index', name='index'),
                       url(r'^compile/$', 'nl.views.compiler', name='compiler'),
                       url(r'^resources/(?P<path>.*)$', 'django.views.static.serve',
                           {'document_root': settings.STATIC_ROOT, 'show_indexes': True}),
                       #url(r'^admin/', include(admin.site.urls)),
)
