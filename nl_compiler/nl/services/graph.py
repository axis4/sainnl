# coding=UTF-8
"""
   Copyright 2014 IoS (progsdi@gmail.com)

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""
from nl.services import grammar
from nl.services.grammar import GrammarService
from nl.services.graph_util import model
from nl.services.graph_util.definition import DefinitionReader
from nl.services.graph_util.nodes import NodeBuilder
from nl.services.graph_util.permutations import Permutations

__author__ = 'sergeionov'


class GraphService(object):
    def __init__(self, packager, bit_config='int8'):
        """
        :type packager: nl.services.package.PackageService
        :type bit_config: str
        """
        self.packager = packager
        self.definitions = DefinitionReader(self)
        self.nodes = NodeBuilder(model.MINS[bit_config], model.MAXS[bit_config], self)

    def construct(self, prefix, tree):
        """
        :type prefix: unicode
        :type tree: list[grammar.Group | grammar.GlobalArgs]
        :raise model.GraphError:
        :rtype: dict[unicode, nl.services.graph_util.model.Graph]
        """
        if not len(prefix):
            raise model.GraphError('Package name is required')
        names, group_params, global_constants = self.read_tree(tree, prefix)

        #if prefix + "." + DEFAULT_START in names:  # autogenerate static starting group
        #    names[prefix + "." + DEFAULT_START], renamed_template = Permutations.pass_permutations(
        #        None, names[prefix + "." + DEFAULT_START], group_params, global_constants)
        #    if names[prefix + "." + DEFAULT_START].name != prefix + "." + DEFAULT_START:
        #        group_params[prefix + "." + DEFAULT_START] = group_params[names[prefix + "." + DEFAULT_START].name]
        #        del group_params[names[prefix + "." + DEFAULT_START].name]
        #        names[prefix + "." + DEFAULT_START].name = prefix + "." + DEFAULT_START
        #
        res = {}
        missed = set(names.iterkeys())
        if len(missed):
            for name in missed:
                try:
                    names[name], renamed_template = Permutations.pass_permutations(
                        None, names[name], group_params, global_constants)
                except model.DelayError:
                    continue

                gr = self.find_group(name, names, group_params, global_constants)
                try:
                    self.form_group(gr, names, group_params, global_constants, res)
                except model.DelayError, e:
                    ok = False
                    for d in self.nodes.get_template_dependencies(gr.templates):
                        if d.match(e.var):
                            ok = True
                    if not ok:
                        raise
        return res

    def read_tree(self, tree, prefix=''):
        """
        :type tree: list[grammar.Group | grammar.GlobalArgs]
        :type prefix: unicode
        :raise model.GraphError:
        :rtype: dict[unicode, grammar.Group], dict[unicode, dict[str, list]], dict[unicode, grammar.Set]
        """
        names = {}
        """:type: dict[unicode, Group]"""
        global_constants = {}
        """:type: dict[unicode, Argument]"""
        group_params = {}
        """:type: dict[unicode, dict[unicode, list]]"""
        counter = model.Graph()
        for group in tree:
            if isinstance(group, grammar.GlobalArgs):
                for g in group.sequences.arguments:
                    if unicode(g.var_name) in global_constants:
                        raise model.GraphError('Global sequence with name "' + unicode(g.var_name) +
                                               '" is already defined')
                    global_constants[unicode(g.var_name)] = g
                    g.var_name.is_constant = True
            if isinstance(group, grammar.Group):
                if group.name in names:
                    raise model.GraphError('Group with name "' + group.name + '" is already defined')
                names[group.name] = group
                group_params[group.name] = self.prepare_group(group, counter.get_new_id, names,
                                                              group_params, global_constants)
        return self.rename_packaged(prefix, names, group_params, global_constants)

    def find_group(self, name, groups, group_params, global_constants):
        """
        :type name: unicode
        :type groups: dict[unicode, grammar.Group]
        :type group_params: dict[unicode, dict[unicode, list]]
        :type global_constants: dict[unicode, grammar.Set]
        :rtype: grammar.Group
        :raise model.GraphError:
        """
        if name in groups:
            return groups[name]
        elif "." in name:
            package = name.split('.')
            pname = ".".join(package[:-1])
            file_name, file_text = self.packager.find_package(pname)
            try:
                ns, gps, gcs = self.read_tree(GrammarService().parse(file_text), pname)
            except grammar.GrammarError, e:
                if e.source is None:
                    e.source = pname
                raise
            for gn in ns:
                groups[gn] = ns[gn]
            for gn in gps:
                group_params[gn] = gps[gn]
            for gn in gcs:
                global_constants[gn] = gcs[gn]
            if name in groups:
                return groups[name]
            else:
                raise model.GraphError('Group with name "' + package[-1] + '" not found in package ' + pname)
        raise model.GraphError('Group with name "' + name + '" not found')

    def prepare_group(self, group, get_new_id, groups, group_params, global_constants):
        """
        :type group: grammar.Group
        :type get_new_id:
        :type groups: dict[unicode, grammar.Group]
        :type group_params: dict[unicode, dict[unicode, list]]
        :type global_constants: dict[unicode, grammar.Set]
        :rtype: dict[unicode, object]
        """
        if group.name in group_params:
            return group_params[group.name]

        self.nodes.substitute_infinity(group)

        #templated case
        relax = True
        if len(group.templates):
            relax |= self.prepare_static_templates(group)
            relax |= self.prepare_dynamic_templates(group)

        self.definitions.mark_constants(group, global_constants)

        self.definitions.expand_header_arguments(group)
        res = [(k, v) for k, v in self.definitions.simplify_definitions(group, get_new_id, groups,
                                                                        group_params, global_constants).iteritems()]
        return dict(res + [(u"relax", relax)])

    @classmethod
    def prepare_dynamic_templates(cls, group):
        to_process = [x for x in group.templates if isinstance(x, grammar.Generator)]
        if len(to_process):
            group.definitions = [
                grammar.TemplateNamedDefinition(
                    NodeBuilder.replace_names(d, to_process), grammar.MultiArgument(
                        d.line, d.lexpos,
                        [grammar.Argument(u'expression',
                                          grammar.VariableOperand(argument.linked_name, d.line, d.lexpos),
                                          d.line, d.lexpos) for argument in to_process]),
                    d.line, d.lexpos)
                if isinstance(d, grammar.NamedDefinition) and not isinstance(d, grammar.TemplateNamedDefinition)
                else grammar.TemplateDefinition(
                    NodeBuilder.replace_names(d, to_process), grammar.MultiArgument(
                        d.line, d.lexpos,
                        [grammar.Argument(u'expression',
                                          grammar.VariableOperand(argument.linked_name, d.line, d.lexpos),
                                          d.line, d.lexpos) for argument in to_process]),
                    d.line, d.lexpos)
                if isinstance(d, grammar.Definition) and not isinstance(d, grammar.TemplateDefinition)
                else NodeBuilder.replace_names(d, to_process)
                for d in group.definitions] +\
                [grammar.Definition(grammar.IN, grammar.DefinitionArguments(
                    None, grammar.MultiArgument(
                        group.line, group.lexpos, [grammar.Argument(
                            u'expression',
                            grammar.VariableOperand(argument.linked_name, d.line, d.lexpos),
                            d.line, d.lexpos) for argument in to_process]),
                    group.line, group.lexpos), group.line, group.lexpos)]
        return len(to_process) > 0

    @staticmethod
    def prepare_static_templates(group):
        #nothing to prepare - static templates are processed on group expansion
        to_process = [x for x in group.templates if isinstance(x, grammar.Sequence)]
        return len(to_process) > 0

    def form_group(self, group, groups, group_params, global_constants, graphs):
        """
        :type group: grammar.Group
        :type groups: dict[unicode, grammar.Group]
        :type group_params: dict[unicode, dict[unicode, list]]
        :type global_constants: dict[unicode, grammar.Set]
        :type graphs: dict[unicode, nl.services.graph_util.model.Graph]
        :rtype: grammar.Group
        :raise model.GraphError:
        """

        #return cached graph
        if group.name in graphs:
            if graphs[group.name] is None:
                raise model.GraphError('Cycling reference starting with "' + group.name)
            return graphs[group.name]

        graphs[group.name] = None
        print "building group " + group.name.__str__()

        res = model.Graph()
        renamer = {}  # RES -> RES.C translator
        missed = []

        try:
            self.nodes.build_input_neurons(group_params[group.name][u"ins"], missed, renamer, res)
            print "built inputs of group " + group.name.__str__()

            self.nodes.build_internal_neurons(group, res, missed, renamer, groups, group_params, global_constants, graphs)
            print "built internals of group " + group.name.__str__()

            self.nodes.build_output_neurons(group_params[group.name][u"outs"], renamer, res)
            print "built outputs of group " + group.name.__str__()
        except model.DelayError:
            del graphs[group.name]
            raise

        graphs[group.name] = res
        return res

    @staticmethod
    def rename_packaged(prefix, names, group_params, global_constants):
        """
        :type prefix: unicode
        :type names: dict[unicode, grammar.Group]
        :type group_params: dict[unicode, dict[unicode, list]]
        :type global_constants: dict[unicode, grammar.Set]
        :rtype: dict[unicode, grammar.Group], dict[unicode, dict[str, list]], dict[unicode, grammar.Set]
        """
        for n in names:
            # rename by global_constants
            if len(names[n].templates):
                for c in global_constants:
                    nc = global_constants[c].var_name.clone()
                    nc.name = prefix + '.' + nc.name
                    NodeBuilder.replace_name(names[n], global_constants[c].var_name, nc)
            for d in names[n].definitions:
                if d.group_name in names:
                    d.group_name = prefix + "." + d.group_name
            names[n].name = prefix + "." + names[n].name
        # rename by global_constants
        for n in group_params:
            for c in global_constants:
                nc = global_constants[c].var_name.clone()
                nc.name = prefix + '.' + nc.name
                for d in group_params[n][u"ins"]:
                    NodeBuilder.replace_name(d, global_constants[c].var_name, nc)
                for d in group_params[n][u"outs"]:
                    NodeBuilder.replace_name(d, global_constants[c].var_name, nc)
                for d in group_params[n][u"syncs"]:
                    NodeBuilder.replace_name(d, global_constants[c].var_name, nc)
                for d in group_params[n][u"asyncs"]:
                    NodeBuilder.replace_name(d, global_constants[c].var_name, nc)
        for c in global_constants:
            global_constants[c].var_name.name = prefix + "." + global_constants[c].var_name.name
        return (dict([(prefix + "." + k, v) for k, v in names.iteritems()]),
                dict([(prefix + "." + k, v) for k, v in group_params.iteritems()]),
                dict([(prefix + "." + k, v) for k, v in global_constants.iteritems()]))

    @staticmethod
    def find_simplest_paths_to_inputs(graph, sources):
        """
        :type graph: model.Graph
        :type sources: list[model.Node]
        :rtype: list[dict[unicode, list[model.Node]]]
        """
        res = []
        """:type: list[dict[unicode, list[model.Node]]]"""
        for s in sources:
            res.append(GraphService.find_simplest_path_to_inputs(graph, s))
        return res

    @staticmethod
    def find_simplest_path_to_inputs(graph, source):
        """
        :type graph: model.Graph
        :type source: model.Node
        :rtype: dict[unicode, list[model.Node]]
        """
        g = graph.clone(None)
        res = {}
        """:type: dict[unicode, list[model.Node]]"""
        for inp in g.nodes.values():
            if inp.input:
                queue = [inp]
                """:type: list[model.Node]"""
                parent = {}
                """:type: dict[unicode, model.Node]"""
                ok = False
                while len(queue):
                    n = queue.pop(0)
                    if unicode(n.node_id) == unicode(source.node_id):
                        ok = True
                        break
                    if unicode(n.node_id) in g.links:
                        for l in g.links[unicode(n.node_id)]:
                            if unicode(l.node_to) not in parent:
                                queue.append(graph.nodes[unicode(l.node_to)])
                                parent[unicode(l.node_to)] = n
                if ok:
                    res[unicode(inp.node_id)] = [source]
                    st = source
                    while unicode(st.node_id) in parent:
                        st = parent[unicode(st.node_id)]
                        res[unicode(inp.node_id)].append(st)
                else:
                    res[unicode(inp.node_id)] = []
        return res