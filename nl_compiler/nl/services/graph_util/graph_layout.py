# coding=UTF-8
"""
   Copyright 2014 IoS (progsdi@gmail.com)

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""
from collections import defaultdict
from math import sqrt
import math
import traceback
import networkx

__author__ = 'sergeionov'


class Layout(object):
    def build_pos(self, graph):
        return {n.node_id: (n.x, n.y) for n in graph.nodes.itervalues()}

    def layout(self, graph):
        pass


class SquareLayout(Layout):
    def __init__(self):
        self.k = 100.0

    def layout(self, graph):
        wall = int(sqrt(len(graph.nodes)))
        x = 0.0
        y = 0.0
        for v in reversed(sorted(graph.nodes.itervalues(), key=lambda n: unicode(n.node_id))):
            v.x = x * self.k
            v.y = y * self.k
            x += 1.0
            if x > wall:
                x = 0.0
                y += 1.0
        return graph


class NetworkXLayout(SquareLayout):
    @staticmethod
    def correct_positions(graph):
        """
        :type graph: nl.services.graph_util.model.Graph
        """
        center_x = 0.0
        center_y = 0.0
        for v in graph.nodes.values():
            center_x += v.x
            center_y += v.y
        center_x /= len(graph.nodes)
        center_y /= len(graph.nodes)
        for v in graph.nodes.values():
            v.x -= center_x
            v.y -= center_y

    @staticmethod
    def displace_matched(pos):
        keep = defaultdict(list)
        for v, vp in pos.iteritems():
            for u, up in pos.iteritems():
                dist = (vp[0] - up[0]) ** 2 + (vp[1] - up[1]) ** 2
                if dist < 1:
                    l = keep[str(int(vp[0])) + "," + str(int(vp[1]))]
                    if u not in l:
                        l.append(u)
        for k, dots in keep.iteritems():
            push_dot = (pos[dots[0]][0], pos[dots[0]][1])
            force = lambda distance: 1000 / sqrt(distance + 1)
            step = 2 * math.pi / len(dots)
            r = 0
            for d in dots:
                pos[d] = (pos[d][0] + math.cos(r) * force(0), pos[d][1] + math.sin(r) * force(0))
                r += step
            for v in pos.iterkeys():
                if v not in dots:
                    dist = sqrt((pos[v][0] - push_dot[0]) ** 2 + (pos[v][1] - push_dot[1]) ** 2)
                    pos[v] = (pos[v][0] + (pos[v][0] - push_dot[0]) / dist * force(dist),
                              pos[v][1] + (pos[v][1] - push_dot[1]) / dist * force(dist))

    @staticmethod
    def build_g(graph):
        g = networkx.Graph()
        g.add_nodes_from(list(graph.nodes.iterkeys()))
        g.add_edges_from([(unicode(v.node_from), unicode(v.node_to),
                           {"weight": (1 if v.additive_weight != 0 else 0) +
                                      (1 if v.stream_weight != 0 else 0) +
                                      (1 if v.generation_weight != 0 else 0)})
                          for vs in graph.links.values() for v in vs])
        return g

    def spectral(self, graph):
        g = self.build_g(graph)
        pos = networkx.spectral_layout(g, scale=sqrt(len(graph.nodes)) * 200)
        self.displace_matched(pos)
        return pos

    def spring(self, graph, pos, it):
        g = self.build_g(graph)
        pos = networkx.spring_layout(g, k=100.0, pos=pos, iterations=it, scale=sqrt(len(graph.nodes)) * 1000.0)
        self.displace_matched(pos)
        return pos

    def graphviz(self, graph):
        g = self.build_g(graph)
        pos = networkx.graphviz_layout(g, prog='dot')
        self.displace_matched(pos)
        return pos

    def store_pos(self, graph, pos):
        for node, p in pos.iteritems():
            if node in graph.nodes:
                graph.nodes[node].x = p[0]
                graph.nodes[node].y = p[1]
        self.correct_positions(graph)

    def layout(self, graph):
        """
        :type graph: nl.services.graph_util.model.Graph
        :rtype: nl.services.graph_util.model.Graph
        """
        self.k = 100.0
        graph = super(NetworkXLayout, self).layout(graph)
        try:
            pos = self.graphviz(graph)
        except:
            traceback.print_exc()
            pos = self.build_pos(graph)
            pos = self.spring(graph, pos, 200)
        self.store_pos(graph, pos)
        return graph


class FruchtermanLayout(SquareLayout):
    W = 100.0
    L = 100.0
    area = W * L

    def f_a(self, x):
        return x * x / self.k

    def f_r(self, x):
        return self.k * self.k / x if abs(x) > 0 else 0

    def layout(self, graph):
        super(FruchtermanLayout, self).layout(graph)

        max_iter = 21
        t = t0 = 10.0
        self.k = sqrt(self.area / len(graph.nodes))
        for i in range(max_iter):
            for v in graph.nodes.values():
                v.d_x = 0.0
                v.d_y = 0.0
                for u in graph.nodes.values():
                    if unicode(u.node_id) != unicode(v.node_id):
                        b_x = v.x - u.x
                        b_y = v.y - u.y
                        b = sqrt(b_x * b_x + b_y * b_y)
                        if abs(b) == 0:
                            raise Exception(u"Nodes too close: " + repr(u) + " and " + repr(v))
                        v.d_x += b_x * self.f_r(b) / b
                        v.d_y += b_y * self.f_r(b) / b
                        print u"{0:.8f}: {1:s} - {2:s}\t{3:.8f} => ({4:.8f}, {5:.8f})".format(
                            t, unicode(v.node_id), unicode(u.node_id), b, v.d_x, v.d_y)

            for es in graph.links.values():
                for e in es:
                    b_x = graph.nodes[unicode(e.node_from)].x - graph.nodes[unicode(e.node_to)].x
                    b_y = graph.nodes[unicode(e.node_from)].y - graph.nodes[unicode(e.node_to)].y
                    b = sqrt(b_x * b_x + b_y * b_y)
                    graph.nodes[unicode(e.node_from)].d_x -= b_x * self.f_a(b) / b
                    graph.nodes[unicode(e.node_from)].d_y -= b_y * self.f_a(b) / b
                    graph.nodes[unicode(e.node_to)].d_x += b_x * self.f_a(b) / b
                    graph.nodes[unicode(e.node_to)].d_y += b_y * self.f_a(b) / b
                    print u"{0:.8f}: {1:s} - {2:s}\t{3:.8f} => ({4:.8f}, {5:.8f}), ({6:.8f}, {7:.8f})".format(
                        t, unicode(e.node_from), unicode(e.node_to), b, graph.nodes[unicode(e.node_from)].d_x,
                        graph.nodes[unicode(e.node_from)].d_y, graph.nodes[unicode(e.node_to)].d_x,
                        graph.nodes[unicode(e.node_to)].d_y)

            for v in graph.nodes.values():
                v_d = sqrt(v.d_x * v.d_x + v.d_y * v.d_y)
                print u"{0:.8f}: {1:s}\t{2:.8f}".format(t, unicode(v.node_id), v_d)
                v.x += v.d_x * min(v.d_x, t) / v_d if abs(v_d) > 0 else 0
                v.y += v.d_y * min(v.d_y, t) / v_d if abs(v_d) > 0 else 0

            t = (max_iter / (i + 2.0) - 1.0) * t0 / (max_iter - 1.0)
        return graph
