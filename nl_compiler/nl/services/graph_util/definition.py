# coding=UTF-8
"""
   Copyright 2014 IoS (progsdi@gmail.com)

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""
from itertools import izip, count
from nl.services import grammar
from nl.services.graph_util import model, util
from nl.services.graph_util.expression import ExpressionBuilder
from nl.services.graph_util.nodes import NodeBuilder
from nl.services.graph_util.util import get_definition_vars

__author__ = 'sergeionov'


class DefinitionReader(object):
    def __init__(self, graph_service):
        self.graph_service = graph_service

    @staticmethod
    def expand_header_arguments(group):
        """
        :type group: grammar.Group
        """
        if len(group.arguments):
            group.definitions.append(grammar.Definition(grammar.IN, grammar.DefinitionArguments(
                None, grammar.MultiArgument(
                    group.line, group.lexpos,
                    group.arguments[:]),
                group.line, group.lexpos), group.line, group.lexpos))

    @staticmethod
    def mark_constants(group, global_constants):
        """
        :type group: grammar.Group
        :type global_constants: dict[unicode, grammar.Set]
        """
        constants = NodeBuilder.get_template_constants(group.templates, global_constants)
        """:type: list[grammar.Variable]"""
        for d in group.definitions:
            local_constants = []
            if isinstance(d, grammar.TemplateDefinition) or isinstance(d, grammar.TemplateNamedDefinition):
                local_constants = [a.value.text for a in d.template_vars.arguments]
                for lc in local_constants:
                    lc.is_runtime_constant = True
            for v in get_definition_vars(d):
                for c in constants + local_constants:
                    for fv in v.find(c):
                        fv.is_constant = c.is_constant
                        fv.is_runtime_constant = c.is_runtime_constant

    def simplify_definitions(self, group, get_new_id, groups, group_params, global_constants):
        """
        :type group: grammar.Group
        :type get_new_id:
        :type groups: dict[unicode, grammar.Group]
        :type group_params: dict[unicode, dict[unicode, object]]
        :type global_constants: dict[unicode, grammar.Set]
        :return: dict[unicode, object]
        """
        input_names = {}
        sync_names = {}
        async_names = {}
        def_names = {}
        redef_names = {}
        growing_names = {}
        ins = []
        outs = []
        syncs = []
        asyncs = []
        # simplify definitions
        for d in group.definitions:
            do_reduce = None
            #check variable name
            if isinstance(d, grammar.NamedDefinition):
                if unicode(d.result_name) in def_names or (not d.reduction and unicode(d.result_name) in redef_names):
                    raise model.GraphError('Definition result "' + repr(d.result_name) + '" is already defined')
                if d.reduction and unicode(d.result_name) in redef_names:
                    do_reduce = redef_names[unicode(d.result_name)]
                else:
                    (redef_names if d.reduction else def_names)[unicode(d.result_name)] = d
            #check "out" arguments
            if d.group_name == grammar.OUT:
                if util.has_template_arguments(d):
                    raise model.GraphError('Definition "' + grammar.OUT + '" with template arguments')
                if not util.has_arguments(d):
                    raise model.GraphError('Definition "' + grammar.OUT + '" without arguments')
                outs.append(d)
            #check "in" arguments
            if d.group_name == grammar.IN:
                if util.has_template_arguments(d):
                    raise model.GraphError('Definition "' + grammar.IN + '" with template arguments')
                if not util.has_arguments(d):
                    raise model.GraphError('Definition "' + grammar.IN + '" without arguments')
                #check argument complexity
                for a in d.arguments.call_arguments.arguments:
                    if a.kind == u'expression' and isinstance(a.value, grammar.VariableOperand):
                        if unicode(a.value.text) in input_names:
                            raise model.GraphError('"' + grammar.IN + '" argument "' +
                                                   repr(a.value.text) + '" is already defined')
                        input_names[unicode(a.value.text)] = d
                    else:
                        raise model.GraphError('Complex expression as "' + grammar.IN + '" argument "' + repr(a) + '"')
                ins.append(d)
            #"sync" not implemented
            if d.group_name == grammar.SYNC:
                if not util.has_arguments(d):
                    raise model.GraphError('Definition "' + grammar.SYNC + '" without arguments')
                #check argument complexity
                for a in d.arguments.call_arguments.arguments:
                    if a.kind == u'expression' and isinstance(a.value, grammar.VariableOperand):
                        if unicode(a.value.text) in input_names:
                            raise model.GraphError('"' + grammar.SYNC + '" argument "' +
                                                   repr(a.value.text) + '" is already defined')
                        sync_names[unicode(a.value.text)] = d
                    else:
                        raise model.GraphError('Complex expression as "' + grammar.SYNC + '" argument "' +
                                               repr(a) + '"')
                syncs.append(d)
            #"sync" not implemented
            if d.group_name == grammar.ASYNC:
                if not util.has_arguments(d):
                    raise model.GraphError('Definition "' + grammar.ASYNC + '" without arguments')
                #check argument complexity
                for a in d.arguments.call_arguments.arguments:
                    if a.kind == u'expression' and isinstance(a.value, grammar.VariableOperand):
                        if unicode(a.value.text) in input_names:
                            raise model.GraphError('"' + grammar.ASYNC + '" argument "' +
                                                   repr(a.value.text) + '" is already defined')
                        async_names[unicode(a.value.text)] = d
                    else:
                        raise model.GraphError('Complex expression as "' + grammar.ASYNC + '" argument "' +
                                               repr(a) + '"')
                asyncs.append(d)
            #check "additive" arguments
            if d.group_name == grammar.UNIT:
                if util.has_template_arguments(d):
                    raise model.GraphError('Definition "' + grammar.UNIT + '" with template arguments')
                if not util.has_arguments(d) or not (1 <= len(d.arguments.call_arguments.arguments) <= 2):
                    raise model.GraphError('Definition "' + grammar.UNIT + '" must have 1 or 2 arguments')
                if len(d.arguments.call_arguments.arguments) == 1:
                    d.arguments.call_arguments.arguments.insert(0, grammar.Argument(u'condition', grammar.Equation(
                        u'>=',
                        grammar.ConstantOperand(0.0, d.line, d.lexpos),
                        grammar.ConstantOperand(0.0, d.line, d.lexpos),
                        d.line, d.lexpos), d.line, d.lexpos))
                d.arguments.call_arguments.arguments.append(grammar.Argument(
                    u'expression',
                    grammar.ConstantOperand(0.0, d.line, d.lexpos),
                    d.line, d.lexpos))

                #turn expression into conditional
                if d.arguments.call_arguments.arguments[0].kind == u'expression':
                    d.arguments.call_arguments.arguments[0].kind = u'condition'
                    d.arguments.call_arguments.arguments[0].value = grammar.Equation(
                        u'!=',
                        d.arguments.call_arguments.arguments[0].value,
                        grammar.ConstantOperand(0.0, d.line, d.lexpos),
                        d.line, d.lexpos)
            #check "multiplicator" arguments
            if d.group_name == grammar.LINKMOD:
                if util.has_template_arguments(d):
                    raise model.GraphError('Definition "' + grammar.LINKMOD + '" with template arguments')
                if not util.has_arguments(d) or not (2 <= len(d.arguments.call_arguments.arguments) <= 3):
                    raise model.GraphError('Definition "' + grammar.LINKMOD + '" must have 2 or 3 arguments')
                if len(d.arguments.call_arguments.arguments) == 2:
                    d.arguments.call_arguments.arguments.append(grammar.Argument(
                        u'expression',
                        grammar.ConstantOperand(0.0, d.line, d.lexpos),
                        d.line, d.lexpos))

            if d.group_name == grammar.STRING:
                if d.arguments.call_arguments is None or len(d.arguments.call_arguments.arguments) != 1:
                    raise model.GraphError('Definition "' + grammar.STRING + '" must have 1 argument')
                if not isinstance(d.arguments.call_arguments.arguments[0].value, grammar.StringOperand):
                    raise model.GraphError('Definition "' + grammar.STRING + '" must have 1 string argument')
            else:
                if util.has_arguments(d):
                    heights = []

                    for a, i in izip(d.arguments.call_arguments.arguments, count(1)):
                        #expand inline invokation
                        if a.kind == u'invokation':
                            #name new variable for invokation
                            if isinstance(a.value, grammar.ExposeDefinition):
                                field = a.value.field_name
                                if isinstance(a.value.chain_to, grammar.NamedDefinition):
                                    var = a.value.chain_to.result_name
                                    define = a.value.chain_to.chain_to
                                    reduct = a.value.chain_to.reduction
                                else:
                                    var = grammar.Variable(u'exec' + get_new_id(), a.line, a.lexpos)
                                    define = a.value.chain_to
                                    reduct = False
                            else:
                                field = None
                                if isinstance(a.value, grammar.NamedDefinition):
                                    var = a.value.result_name
                                    define = a.value.chain_to
                                    reduct = a.value.reduction
                                else:
                                    var = grammar.Variable(u'exec' + get_new_id(), a.line, a.lexpos)
                                    define = a.value
                                    reduct = False
                            a.kind = u'expression'
                            a.value = grammar.VariableOperand(NodeBuilder.extend_name(var, field), a.line, a.lexpos)

                            #keep name templating
                            if isinstance(d, grammar.TemplateNamedDefinition) or isinstance(d,
                                                                                            grammar.TemplateDefinition):
                                ng = grammar.TemplateNamedDefinition(
                                    grammar.NamedDefinition(var, define, reduct, d.line, d.lexpos),
                                    d.template_vars, d.line, d.lexpos)
                            else:
                                ng = grammar.NamedDefinition(var, define, reduct, d.line, d.lexpos)
                            ng.pristine = False
                            group.definitions.append(ng)

                            #count height of invoked group
                            heights.append(self.graph_service.prepare_group(
                                self.graph_service.find_group(ng.group_name, groups, group_params, global_constants),
                                get_new_id, groups, group_params, global_constants)["height"])

                            #add conditional if invokation is a first argument in additive
                            if d.group_name == grammar.UNIT and i == 1:
                                a.kind = u'condition'
                                a.value = grammar.Equation(u'!=', a.value,
                                                           grammar.ConstantOperand(0.0, a.line, a.lexpos),
                                                           a.line, a.lexpos)
                                heights[-1] += 3
                        #expand complex expression for group invokation
                        if a.kind == u'expression' or a.kind == u'condition':
                            new_defs = []
                            #additive argument
                            if d.group_name == grammar.UNIT:
                                if i == 1:
                                    a.value.loperand, lheight = ExpressionBuilder.normal_expression_to_sum(
                                        a.value.loperand, get_new_id, new_defs)
                                    rop, rheight = ExpressionBuilder.normal_expression_to_sum(
                                        a.value.roperand, get_new_id, new_defs)
                                    consts = NodeBuilder.extract_constant(rop)
                                    if not len(rop.operands):
                                        def clean_extra_koeffs(x):
                                            """
                                            :type x: grammar.Expression | grammar.Operand
                                            :rtype: grammar.Expression | grammar.Operand
                                            """
                                            if isinstance(x, grammar.Expression) and\
                                                    NodeBuilder.is_operand_number(x.loperand) and x.loperand.text == 1:
                                                return x.roperand
                                            return x

                                        consts = map(clean_extra_koeffs,
                                                     [c for c in consts
                                                      if not isinstance(c, grammar.Operand) or c.text != 0])
                                        if any(map(lambda x: NodeBuilder.is_operand_lonely_constant(x), consts)) and\
                                                len(consts) > 1:
                                            raise model.GraphError(
                                                "Dynamic constant is not applicable in constant expression")

                                        if len(consts) > 1:
                                            rop.operands = consts
                                            rop.heights = [0] * len(rop.operands)
                                        elif len(consts) == 1:
                                            rop = consts[0]
                                        else:
                                            rop = grammar.ConstantOperand(0.0, a.line, a.lexpos)
                                        a.value.roperand = rop
                                    else:
                                        rop.operands = consts + rop.operands
                                        rop.heights = [0] * len(rop.operands)
                                        rop, rheight = ExpressionBuilder.normal_expression_to_operand(
                                            rop, get_new_id, new_defs)
                                        rop = grammar.Expression(u"*",
                                                                 grammar.ConstantOperand(-1., rop.line, rop.lexpos),
                                                                 rop, rop.line, rop.lexpos)
                                        a.value.loperand.add_item(rop, rheight)
                                        a.value.roperand = grammar.ConstantOperand(0.0, d.line, d.lexpos)
                                        lheight = ExpressionBuilder.balance_sum_expressions(
                                            a.value.loperand, None, get_new_id, new_defs)
                                    heights.append(lheight)
                                if i == 2:
                                    a.value, sheight = ExpressionBuilder.normal_expression_to_sum(
                                        a.value, get_new_id, new_defs)
                                    heights.append(sheight)
                                if i == 3:
                                    name, fheight = ExpressionBuilder.form_expression(a.value, get_new_id, new_defs)
                                    if isinstance(name, grammar.Variable):
                                        raise model.GraphError('Definition "' + grammar.UNIT +
                                                               '" must have constant 3rd argument')
                                    a.value = grammar.ConstantOperand(name, a.line, a.lexpos)
                                    heights.append(0)

                            #multiplicator argument
                            elif d.group_name == grammar.LINKMOD:
                                if i < 3:
                                    a.value, mheight = ExpressionBuilder.normal_expression_to_sum(
                                        a.value, get_new_id, new_defs)
                                    heights.append(mheight)
                                else:
                                    name, fheight = ExpressionBuilder.form_expression(a.value, get_new_id, new_defs)
                                    if isinstance(name, grammar.Variable):
                                        raise model.GraphError('Definition "' + grammar.LINKMOD +
                                                               '" must have constant 3rd argument')
                                    a.value = grammar.ConstantOperand(name, a.line, a.lexpos)
                                    heights.append(0)

                            #another group argument
                            else:
                                #make all call arguments simple constants or variables
                                height = -1
                                if d.group_name == grammar.OUT:
                                    if not isinstance(a.value, grammar.Operand):
                                        a.value, height = ExpressionBuilder.normal_expression_to_sum(
                                            a.value, get_new_id, new_defs)
                                    else:
                                        height = 0
                                name, fheight = ExpressionBuilder.form_expression(a.value, get_new_id, new_defs)
                                heights.append(max(height + 1, fheight))

                                #constant argument
                                if not isinstance(name, grammar.Variable):
                                    a.kind = u'expression'
                                    a.value = grammar.ConstantOperand(name, a.line, a.lexpos)
                                    continue

                                #variable argument
                                r_name = name
                                #fix names
                                if len(new_defs):
                                    n = 1
                                    while unicode(r_name) in def_names:
                                        n += 1
                                        r_name = NodeBuilder.extend_name(name, grammar.Variable("__" + str(n),
                                                                                                name.line, name.lexpos))
                                    new_defs[-1].result_name = r_name
                                a.kind = u'expression'
                                a.value = grammar.VariableOperand(r_name, a.line, a.lexpos)

                            #keep additional definitions in the end of the group
                            for nd in new_defs:
                                if isinstance(d, grammar.TemplateNamedDefinition)\
                                        or isinstance(d, grammar.TemplateDefinition):
                                    nd = grammar.TemplateNamedDefinition(nd, d.template_vars, d.line, d.lexpos)
                                group.definitions.append(nd)

                    #align heights
                    mheight = max(heights)
                    if mheight > min(heights):
                        new_defs = []
                        for a, i in izip(d.arguments.call_arguments.arguments, count()):
                            ExpressionBuilder.delay_expression(a, mheight - heights[i], get_new_id, new_defs)
                        for nd in new_defs:
                            if isinstance(d, grammar.TemplateNamedDefinition) or isinstance(d,
                                                                                            grammar.TemplateDefinition):
                                nd = grammar.TemplateNamedDefinition(nd, d.template_vars, d.line, d.lexpos)
                            group.definitions.append(nd)
            if do_reduce is not None:
                is_growing = False
                if do_reduce.group_name != d.group_name:
                    is_growing = True
                else:
                    if do_reduce.group_name == grammar.UNIT:
                        if repr(do_reduce.arguments.call_arguments.arguments[0].value) !=\
                                repr(d.arguments.call_arguments.arguments[0].value):
                            is_growing = True
                        else:
                            do_reduce.arguments.call_arguments.arguments[1].value.operands +=\
                                d.arguments.call_arguments.arguments[1].value.operands
                            do_reduce.arguments.call_arguments.arguments[1].value.heights +=\
                                d.arguments.call_arguments.arguments[1].value.heights
                            d.group_name = None
                            d.arguments = None
                    elif do_reduce.group_name == grammar.LINKMOD:
                        do_reduce.arguments.call_arguments.arguments[0].value.operands +=\
                            d.arguments.call_arguments.arguments[0].value.operands
                        do_reduce.arguments.call_arguments.arguments[0].value.heights +=\
                            d.arguments.call_arguments.arguments[0].value.heights
                        do_reduce.arguments.call_arguments.arguments[1].value.operands +=\
                            d.arguments.call_arguments.arguments[1].value.operands
                        do_reduce.arguments.call_arguments.arguments[1].value.heights +=\
                            d.arguments.call_arguments.arguments[1].value.heights
                        do_reduce.arguments.call_arguments.arguments[2].value.text +=\
                            d.arguments.call_arguments.arguments[2].value.text
                        d.group_name = None
                        d.arguments = None
                    else:
                        is_growing = True
                if is_growing:
                    if d.result_name not in growing_names:
                        growing_names[d.result_name] = [do_reduce]
                    growing_names[d.result_name].append(d)

        for k, ds in growing_names.iteritems():
            se = grammar.SumExpression(ds[0].line, ds[0].lexpos)
            for d in ds:
                d.result_name = NodeBuilder.extend_name(d.result_name,
                                                        grammar.Variable(u"#reduction" + get_new_id(),
                                                                         d.line, d.lexpos))
                se.add_item(grammar.Expression(u"*",
                                               grammar.ConstantOperand(1., ds[0].line, ds[0].lexpos),
                                               grammar.VariableOperand(d.result_name, ds[0].line, ds[0].lexpos),
                                               ds[0].line, ds[0].lexpos), 1)
            rd = grammar.NamedDefinition(k, grammar.Definition(
                grammar.UNIT,
                grammar.DefinitionArguments(None, grammar.MultiArgument(
                    ds[0].line, ds[0].lexpos,
                    [grammar.Argument(u"condition",
                                      grammar.Equation(u">=",
                                                       grammar.ConstantOperand(0.0, ds[0].line, ds[0].lexpos),
                                                       grammar.ConstantOperand(0.0, ds[0].line, ds[0].lexpos),
                                                       ds[0].line, ds[0].lexpos), ds[0].line, ds[0].lexpos),
                     grammar.Argument(u"expression", se, ds[0].line, ds[0].lexpos)]), ds[0].line, ds[0].lexpos),
                ds[0].line, ds[0].lexpos), False, ds[0].line, ds[0].lexpos)
            if isinstance(ds[0], grammar.TemplateNamedDefinition):
                rd = grammar.TemplateNamedDefinition(rd, ds[0].template_vars, rd.line, rd.lexpos)
            group.definitions.append(rd)
        group.definitions = [d for d in group.definitions if d.group_name is not None]
        # TODO: check if dropped ins/outs/syncs/asyncs

        height = 0

        return {u"ins": ins, u"outs": outs, u"syncs": syncs, u"asyncs": asyncs, u"height": height}
