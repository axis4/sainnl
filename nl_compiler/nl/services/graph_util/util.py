# coding=UTF-8
"""
   Copyright 2014 IoS (progsdi@gmail.com)

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""
from nl.services import grammar
from nl.services.graph_util.expression_tree import ExpressionTreeTraversal

__author__ = 'sergeionov'


def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False


def get_out_name(node):
    """
    :type node: grammar.Argument | grammar.Operand | grammar.Sequence | grammar.Generator | grammar.Equation
    :rtype: list[grammar.Variable]
    """
    def get_out(item, use_less, childrenvalues):
        if childrenvalues is None:
            childrenvalues = []
        else:
            childrenvalues = [cv for cvs in childrenvalues for cv in cvs.value]
        if isinstance(item, grammar.VariableOperand):
            childrenvalues += [item.text]
        return childrenvalues, False

    return ExpressionTreeTraversal.pass_tree(node, postprocess_current=get_out)


def get_expression_vars(a):
    """
    :type a: grammar.Argument | grammar.Operand | grammar.Sequence | grammar.Generator | grammar.Equation
    :rtype: list[grammar.Variable]
    """
    def get_out(item, use_less, childrenvalues):
        if childrenvalues is None:
            childrenvalues = []
        else:
            childrenvalues = [v for cv in childrenvalues for v in cv.value]
        if isinstance(item, grammar.VariableOperand):
            childrenvalues += [item.text]
        if isinstance(item, grammar.Generator):
            childrenvalues += [item.linked_name]
        return childrenvalues, False

    return ExpressionTreeTraversal.pass_tree(a, postprocess_current=get_out)


def get_definition_vars(d):
    """
    :type d: grammar.Definition
    :rtype: list[grammar.Variable]
    """
    def get_out(item, use_less, childrenvalues):
        if childrenvalues is None:
            childrenvalues = []
        else:
            childrenvalues = [v for cv in childrenvalues for v in cv.value]
        if isinstance(item, grammar.VariableOperand):
            childrenvalues += [item.text]
        if isinstance(item, grammar.Generator):
            childrenvalues += [item.linked_name]
        if isinstance(item, grammar.NamedDefinition):
            childrenvalues += [item.result_name]
        if isinstance(item, grammar.ExposeDefinition):
            childrenvalues += [item.field_name]
        return childrenvalues, False

    return ExpressionTreeTraversal.pass_tree(d, postprocess_current=get_out)


def has_template_arguments(definition):
    return definition.arguments.template_arguments is not None and len(
        definition.arguments.template_arguments.arguments)


def has_arguments(definition):
    return definition.arguments.call_arguments is not None and len(definition.arguments.call_arguments.arguments)


def fix_name(name):
    return name.replace('-', 'm').replace('.', '_').replace('[', '#lb#').replace(']', '#rb#')