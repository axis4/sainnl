# coding=UTF-8
"""
   Copyright 2014 IoS (progsdi@gmail.com)

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""
from itertools import izip, count, chain
from nl.services import grammar
from nl.services.graph_util import model
from nl.services.graph_util.nodes import NodeBuilder
from nl.services.graph_util.util import fix_name

__author__ = 'sergeionov'


class ExpressionBuilder(object):
    @staticmethod
    def primitive_expression(left_value, right_value, expression):
        left_value = float(left_value)
        right_value = float(right_value)
        if expression.action == u'+':
            return left_value + right_value, 0
        if expression.action == u'-':
            return left_value - right_value, 0
        if expression.action == u'*':
            return left_value * right_value, 0
        if expression.action == u'/':
            return left_value / right_value, 0
        if expression.action == u'<':
            return (1. if left_value < right_value else 0.), 0
        if expression.action == u'<=':
            return (1. if left_value <= right_value else 0.), 0
        if expression.action == u'>':
            return (1. if left_value > right_value else 0.), 0
        if expression.action == u'>=':
            return (1. if left_value >= right_value else 0.), 0
        if expression.action == u'==':
            return (1. if left_value == right_value else 0.), 0
        if expression.action == u'!=':
            return (1. if left_value != right_value else 0.), 0

    @staticmethod
    def arithmetic_additionals(def_name, expression, left_name, right_name, defs_array):
        if not isinstance(def_name, grammar.Variable):
            raise Exception("Unexpected parameter state")
        # A + B => additive(value 0 >= value 0, var A + var B)
        # A + 2 => additive(value 0 >= value 0, var A + value 2)
        nd = grammar.NamedDefinition(
            def_name,
            grammar.Definition(
                grammar.UNIT,
                grammar.DefinitionArguments(
                    None,
                    grammar.MultiArgument(
                        expression.line, expression.lexpos,
                        [grammar.Argument(u'condition',
                                          grammar.Equation(
                                              u'>=',
                                              grammar.ConstantOperand(0.0, expression.line, expression.lexpos),
                                              grammar.ConstantOperand(0.0, expression.line, expression.lexpos),
                                              expression.line, expression.lexpos), expression.line, expression.lexpos),
                         grammar.Argument(u'expression',
                                          grammar.Expression(
                                              expression.action,
                                              grammar.VariableOperand(left_name, expression.line, expression.lexpos)
                                              if isinstance(left_name, grammar.Variable)
                                              else
                                              grammar.ConstantOperand(left_name, expression.line, expression.lexpos),
                                              grammar.VariableOperand(right_name, expression.line, expression.lexpos)
                                              if isinstance(right_name, grammar.Variable)
                                              else
                                              grammar.ConstantOperand(right_name, expression.line, expression.lexpos),
                                              expression.line, expression.lexpos), expression.line, expression.lexpos)]
                    ), expression.line, expression.lexpos), expression.line, expression.lexpos),
            False, expression.line, expression.lexpos)
        nd.pristine = False
        defs_array.append(nd)
        return 1 if expression.action == u"*" else 0

    @staticmethod
    def comparison_additionals(def_name, expression, left_name, right_name, defs_array):
        if not isinstance(def_name, grammar.Variable):
            raise Exception("Unexpected parameter state")
        # helper(A >= B) => additive(var A - var B >= value 0, value 1)
        # helper(A >= 2) => additive(var A - value 2 >= value 0, value 1)
        diff_ge = NodeBuilder.extend_name(def_name, grammar.Variable(u'#diff_ge', def_name.line, def_name.lexpos))
        ng = grammar.NamedDefinition(
            diff_ge,
            grammar.Definition(grammar.UNIT,
                               grammar.DefinitionArguments(
                                   None,
                                   grammar.MultiArgument(
                                       expression.line, expression.lexpos,
                                       [grammar.Argument(u'condition',
                                                         grammar.Equation(
                                                             u'>=',
                                                             grammar.Expression(
                                                                 u'-',
                                                                 grammar.VariableOperand(left_name,
                                                                                         expression.line,
                                                                                         expression.lexpos)
                                                                 if isinstance(left_name, grammar.Variable)
                                                                 else
                                                                 grammar.ConstantOperand(left_name,
                                                                                         expression.line,
                                                                                         expression.lexpos),
                                                                 grammar.VariableOperand(right_name,
                                                                                         expression.line,
                                                                                         expression.lexpos)
                                                                 if isinstance(right_name, grammar.Variable)
                                                                 else
                                                                 grammar.ConstantOperand(right_name,
                                                                                         expression.line,
                                                                                         expression.lexpos),
                                                                 expression.line, expression.lexpos),
                                                             grammar.ConstantOperand(0.0,
                                                                                     expression.line,
                                                                                     expression.lexpos),
                                                             expression.line, expression.lexpos),
                                                         expression.line, expression.lexpos),
                                        grammar.Argument(u'expression',
                                                         grammar.ConstantOperand(1.0,
                                                                                 expression.line, expression.lexpos),
                                                         expression.line, expression.lexpos)]
                                   ), expression.line, expression.lexpos), expression.line, expression.lexpos),
            False, expression.line, expression.lexpos)
        ng.pristine = False
        defs_array.append(ng)
        # helper(A <= B) => additive(var B - var A >= value 0, value 1)
        # helper(A <= 2) => additive(value 2 - var A >= value 0, value 1)
        diff_le = NodeBuilder.extend_name(def_name, grammar.Variable(u'#diff_le', def_name.line, def_name.lexpos))
        ng = grammar.NamedDefinition(
            diff_le,
            grammar.Definition(grammar.UNIT,
                               grammar.DefinitionArguments(
                                   None,
                                   grammar.MultiArgument(
                                       expression.line, expression.lexpos,
                                       [grammar.Argument(u'condition',
                                                         grammar.Equation(
                                                             u'>=',
                                                             grammar.Expression(
                                                                 u'-',
                                                                 grammar.VariableOperand(right_name,
                                                                                         expression.line,
                                                                                         expression.lexpos)
                                                                 if isinstance(right_name, grammar.Variable)
                                                                 else
                                                                 grammar.ConstantOperand(right_name,
                                                                                         expression.line,
                                                                                         expression.lexpos),
                                                                 grammar.VariableOperand(left_name,
                                                                                         expression.line,
                                                                                         expression.lexpos)
                                                                 if isinstance(left_name, grammar.Variable)
                                                                 else
                                                                 grammar.ConstantOperand(left_name,
                                                                                         expression.line,
                                                                                         expression.lexpos),
                                                                 expression.line, expression.lexpos),
                                                             grammar.ConstantOperand(0.0,
                                                                                     expression.line,
                                                                                     expression.lexpos),
                                                             expression.line, expression.lexpos),
                                                         expression.line, expression.lexpos),
                                        grammar.Argument(u'expression',
                                                         grammar.ConstantOperand(1.0,
                                                                                 expression.line, expression.lexpos),
                                                         expression.line, expression.lexpos)]
                                   ), expression.line, expression.lexpos), expression.line, expression.lexpos),
            False, expression.line, expression.lexpos)
        ng.pristine = False
        defs_array.append(ng)
        if expression.action == u'<':
            # A < B => additive(-1 * helper(A >= B) >= value 0, value 1)
            # A < 2 => additive(-1 * helper(A >= 2) >= value 0, value 1)
            ng = grammar.NamedDefinition(
                def_name,
                grammar.Definition(
                    grammar.UNIT,
                    grammar.DefinitionArguments(
                        None,
                        grammar.MultiArgument(
                            expression.line, expression.lexpos,
                            [grammar.Argument(
                                u'condition',
                                grammar.Equation(
                                    u'>=',
                                    grammar.Expression(
                                        u'*',
                                        grammar.ConstantOperand(-1.0, expression.line, expression.lexpos),
                                        grammar.VariableOperand(diff_ge.clone(),
                                                                expression.line, expression.lexpos),
                                        expression.line, expression.lexpos),
                                    grammar.ConstantOperand(0.0, expression.line, expression.lexpos),
                                    expression.line, expression.lexpos),
                                expression.line, expression.lexpos),
                             grammar.Argument(u'expression',
                                              grammar.ConstantOperand(1.0, expression.line, expression.lexpos),
                                              expression.line, expression.lexpos)]
                        ), expression.line, expression.lexpos), expression.line, expression.lexpos),
                False, expression.line, expression.lexpos)
            ng.pristine = False
            defs_array.append(ng)
            return 2
        if expression.action == u'<=':
            # A <= B => additive(helper(A <= B) >= value 1, value 1)
            # A <= 2 => additive(helper(A <= 2) >= value 1, value 1)
            ng = grammar.NamedDefinition(
                def_name,
                grammar.Definition(
                    grammar.UNIT,
                    grammar.DefinitionArguments(
                        None,
                        grammar.MultiArgument(
                            expression.line, expression.lexpos,
                            [grammar.Argument(
                                u'condition',
                                grammar.Equation(
                                    u'>=',
                                    grammar.VariableOperand(diff_le.clone(), expression.line, expression.lexpos),
                                    grammar.ConstantOperand(1.0, expression.line, expression.lexpos),
                                    expression.line, expression.lexpos),
                                expression.line, expression.lexpos),
                             grammar.Argument(u'expression',
                                              grammar.ConstantOperand(1.0, expression.line, expression.lexpos),
                                              expression.line, expression.lexpos)]
                        ), expression.line, expression.lexpos), expression.line, expression.lexpos),
                False, expression.line, expression.lexpos)
            ng.pristine = False
            defs_array.append(ng)
            return 2
        if expression.action == u'>':
            # A > B => additive((-1) * helper(A <= B) >= value 0, value 1)
            # A > 2 => additive((-1) * helper(A <= 2) >= value 0, value 1)
            ng = grammar.NamedDefinition(
                def_name,
                grammar.Definition(
                    grammar.UNIT,
                    grammar.DefinitionArguments(
                        None,
                        grammar.MultiArgument(
                            expression.line, expression.lexpos,
                            [grammar.Argument(
                                u'condition',
                                grammar.Equation(
                                    u'>=',
                                    grammar.Expression(
                                        u'*',
                                        grammar.ConstantOperand(-1.0, expression.line, expression.lexpos),
                                        grammar.VariableOperand(diff_le.clone(),
                                                                expression.line, expression.lexpos),
                                        expression.line, expression.lexpos),
                                    grammar.ConstantOperand(0.0, expression.line, expression.lexpos),
                                    expression.line, expression.lexpos),
                                expression.line, expression.lexpos),
                             grammar.Argument(u'expression',
                                              grammar.ConstantOperand(1.0, expression.line, expression.lexpos),
                                              expression.line, expression.lexpos)]
                        ), expression.line, expression.lexpos), expression.line, expression.lexpos),
                False, expression.line, expression.lexpos)
            ng.pristine = False
            defs_array.append(ng)
            return 2
        if expression.action == u'>=':
            # A >= B => additive(helper(A >= B) >= value 1, value 1)
            # A >= 2 => additive(helper(A >= 2) >= value 1, value 1)
            ng = grammar.NamedDefinition(
                def_name,
                grammar.Definition(
                    grammar.UNIT,
                    grammar.DefinitionArguments(
                        None,
                        grammar.MultiArgument(
                            expression.line, expression.lexpos,
                            [grammar.Argument(
                                u'condition',
                                grammar.Equation(
                                    u'>=',
                                    grammar.VariableOperand(diff_ge.clone(), expression.line, expression.lexpos),
                                    grammar.ConstantOperand(1.0, expression.line, expression.lexpos),
                                    expression.line, expression.lexpos),
                                expression.line, expression.lexpos),
                             grammar.Argument(u'expression',
                                              grammar.ConstantOperand(1.0, expression.line, expression.lexpos),
                                              expression.line, expression.lexpos)]
                        ), expression.line, expression.lexpos), expression.line, expression.lexpos),
                False, expression.line, expression.lexpos)
            ng.pristine = False
            defs_array.append(ng)
            return 2
        if expression.action == u'==':
            # A == B => additive(helper(A >= B) + helper(A <= B) >= value 2, value 1)
            # A == 2 => additive(helper(A >= 2) + helper(A <= 2) >= value 2, value 1)
            ng = grammar.NamedDefinition(
                def_name,
                grammar.Definition(
                    grammar.UNIT,
                    grammar.DefinitionArguments(
                        None,
                        grammar.MultiArgument(
                            expression.line, expression.lexpos,
                            [grammar.Argument(
                                u'condition',
                                grammar.Equation(
                                    u'>=',
                                    grammar.Expression(
                                        u'+',
                                        grammar.VariableOperand(diff_ge.clone(), expression.line, expression.lexpos),
                                        grammar.VariableOperand(diff_le.clone(), expression.line, expression.lexpos),
                                        expression.line, expression.lexpos),
                                    grammar.ConstantOperand(2.0, expression.line, expression.lexpos),
                                    expression.line, expression.lexpos),
                                expression.line, expression.lexpos),
                             grammar.Argument(u'expression',
                                              grammar.ConstantOperand(1.0, expression.line, expression.lexpos),
                                              expression.line, expression.lexpos)]
                        ), expression.line, expression.lexpos), expression.line, expression.lexpos),
                False, expression.line, expression.lexpos)
            ng.pristine = False
            defs_array.append(ng)
            return 2
        if expression.action == u'!=':
            # A != B => additive(-1 * helper(A >= B) + (-1) * helper(A <= B) >= value -1, value 1)
            # A != 2 => additive(-1 * helper(A >= 2) + (-1) * helper(A <= 2) >= value -1, value 1)
            ng = grammar.NamedDefinition(
                def_name,
                grammar.Definition(
                    grammar.UNIT,
                    grammar.DefinitionArguments(
                        None,
                        grammar.MultiArgument(
                            expression.line, expression.lexpos,
                            [grammar.Argument(
                                u'condition',
                                grammar.Equation(
                                    u'>=',
                                    grammar.Expression(
                                        u'+',
                                        grammar.Expression(
                                            u'*',
                                            grammar.ConstantOperand(-1.0, expression.line, expression.lexpos),
                                            grammar.VariableOperand(diff_ge.clone(),
                                                                    expression.line, expression.lexpos),
                                            expression.line, expression.lexpos),
                                        grammar.Expression(
                                            u'*',
                                            grammar.ConstantOperand(-1.0, expression.line, expression.lexpos),
                                            grammar.VariableOperand(diff_le.clone(),
                                                                    expression.line, expression.lexpos),
                                            expression.line, expression.lexpos),
                                        expression.line, expression.lexpos),
                                    grammar.ConstantOperand(-1.0, expression.line, expression.lexpos),
                                    expression.line, expression.lexpos),
                                expression.line, expression.lexpos),
                             grammar.Argument(u'expression',
                                              grammar.ConstantOperand(1.0, expression.line, expression.lexpos),
                                              expression.line, expression.lexpos)]
                        ), expression.line, expression.lexpos), expression.line, expression.lexpos),
                False, expression.line, expression.lexpos)
            ng.pristine = False
            defs_array.append(ng)
            return 2

    @staticmethod
    def balance_height(delta, name, get_new_name, defs_array, line, lexpos):
        if not isinstance(name, grammar.Variable) or delta == 0:
            return name
        prevname = name
        tname = None
        for i in range(delta):
            tname = NodeBuilder.extend_name(name, grammar.Variable(u'#delay' + get_new_name(), name.line, name.lexpos))
            ng = grammar.NamedDefinition(tname, grammar.Definition(grammar.UNIT, grammar.DefinitionArguments(
                None, grammar.MultiArgument(
                    line, lexpos,
                    [grammar.Argument(
                        u'expression',
                        grammar.VariableOperand(prevname.clone(), line, lexpos),
                        line, lexpos)]
                ), line, lexpos), line, lexpos), False, line, lexpos)
            defs_array.append(ng)
            prevname = tname
        return tname

    @staticmethod
    def balance_expressions(expression1_name, expression1_height, expression2_name, expression2_height,
                            get_new_name, defs_array, line, lexpos):
        height = max(expression1_height, expression2_height)
        name1 = ExpressionBuilder.balance_height(height - expression1_height, expression1_name,
                                                 get_new_name, defs_array, line, lexpos)
        name2 = ExpressionBuilder.balance_height(height - expression2_height, expression2_name,
                                                 get_new_name, defs_array, line, lexpos)
        return name1, name2, height

    @staticmethod
    def balance_sum_expressions(expression, height, get_new_name, defs_array):
        if height is None:
            height = max(expression.heights)

        for o, i in izip(expression.operands, count()):
            ExpressionBuilder.delay_expression(o, height - expression.heights[i], get_new_name, defs_array)
            expression.heights[i] = height

        return height

    @staticmethod
    def delay_expression(expression, delay, get_new_name, defs_array):
        if isinstance(expression, grammar.Operand):
            if isinstance(expression, grammar.VariableOperand):
                expression.text = ExpressionBuilder.balance_height(delay, expression.text, get_new_name,
                                                                   defs_array, expression.line, expression.lexpos)
        else:
            if isinstance(expression, grammar.Expression):
                if expression.action == u'+':
                    ExpressionBuilder.delay_expression(expression.loperand, delay, get_new_name, defs_array)
                    ExpressionBuilder.delay_expression(expression.roperand, delay, get_new_name, defs_array)
                else:
                    if isinstance(expression.roperand, grammar.VariableOperand):
                        expression.roperand.text = ExpressionBuilder.balance_height(delay, expression.roperand.text,
                                                                                    get_new_name, defs_array,
                                                                                    expression.line, expression.lexpos)

    @staticmethod
    def form_expression(expression, get_new_name, defs_array):
        if isinstance(expression, grammar.SumExpression):
            temp_name = [None]

            def pass_operands(item):
                t_expr, t_height = ExpressionBuilder.normal_arithmetic(item, get_new_name, defs_array)
                if isinstance(t_expr, grammar.SumExpression):
                    parts = t_expr.operands
                elif isinstance(t_expr, grammar.Expression):
                    if t_expr.action == u"+":
                        parts = [t_expr.loperand, t_expr.roperand]
                    else:
                        parts = [t_expr]
                else:
                    parts = [t_expr]
                for p in parts:
                    if isinstance(p, grammar.Expression):
                        p_name = [p.loperand.text, p.roperand.text]
                    elif isinstance(p, grammar.Operand):
                        p_name = [p.text]
                    else:
                        p_name = [p]
                    for pn in p_name:
                        if isinstance(pn, grammar.Variable):
                            if temp_name[0] is None:
                                temp_name[0] = pn.clone()
                            else:
                                pn = pn.clone()
                                temp_name[0].name += "_" + pn.name
                                pn.name = None
                                t = temp_name[0]
                                while t.subvar is not None:
                                    t = t.subvar
                                t.merge(pn)
                        else:
                            if temp_name[0] is None:
                                temp_name[0] = grammar.Variable(fix_name(unicode(pn)), item.line, item.lexpos)
                            else:
                                temp_name[0].name += "_" + fix_name(unicode(pn))

                return t_expr, t_height

            o, h = zip(*[pass_operands(o) for o in expression.operands])
            expression.operands[:], expression.heights[:] = zip(*chain(*[zip(op.operands, op.heights)
                                                                         if isinstance(op, grammar.SumExpression)
                                                                         else [(op, h[i])]
                                                                         for i, op in enumerate(o)]))

            if temp_name[0] is None:
                return 0.0, 0

            height = ExpressionBuilder.balance_sum_expressions(expression, None, get_new_name, defs_array)

            name = temp_name[0]
            name.name = fix_name(name.name)
            name = NodeBuilder.extend_name(name, grammar.Variable('#' + get_new_name(),
                                                                  expression.line, expression.lexpos))

            ng = grammar.NamedDefinition(name, grammar.Definition(grammar.UNIT, grammar.DefinitionArguments(
                None, grammar.MultiArgument(
                    expression.line, expression.lexpos,
                    [grammar.Argument(u'expression', expression, expression.line, expression.lexpos)]
                ),
                expression.line,
                expression.lexpos), expression.line, expression.lexpos), False, expression.line, expression.lexpos)
            defs_array.append(ng)

            return name, height

        if not isinstance(expression, grammar.Expression) and not isinstance(expression, grammar.Equation):
            # simple operand
            return expression.text, 0

        if expression.action == u"*":
            try:
                if float(unicode(expression.loperand.text)) == 1.:
                    return expression.roperand.text, 0
            except ValueError:
                pass

        left_name, left_height = ExpressionBuilder.form_expression(expression.loperand, get_new_name, defs_array)
        right_name, right_height = ExpressionBuilder.form_expression(expression.roperand, get_new_name, defs_array)

        if (not isinstance(left_name, grammar.Variable) or left_name.eval_constant is not None) and \
                (not isinstance(right_name, grammar.Variable) or right_name.eval_constant is not None):
            return ExpressionBuilder.primitive_expression(left_name, right_name, expression)

        if isinstance(left_name, grammar.Variable) and left_name.eval_constant is None and \
                isinstance(right_name, grammar.Variable) and right_name.eval_constant is None:
            name = left_name.clone()
            tmp_name = right_name.clone()
            name.name += "_" + tmp_name.name
            tmp_name.name = None
            m_name = name
            while m_name.subvar is not None:
                m_name = m_name.subvar
            m_name.merge(tmp_name)
            name = NodeBuilder.extend_name(name, grammar.Variable(u'#' + get_new_name(),
                                                                  expression.line, expression.lexpos))

            left_name, right_name, height = ExpressionBuilder.balance_expressions(left_name, left_height,
                                                                                  right_name, right_height,
                                                                                  get_new_name, defs_array,
                                                                                  expression.line, expression.lexpos)

            if expression.action == u'+' or expression.action == u'-' or expression.action == u'*' or \
                    expression.action == u'/':
                height += ExpressionBuilder.arithmetic_additionals(name, expression, left_name, right_name, defs_array)
            else:
                height += ExpressionBuilder.comparison_additionals(name, expression, left_name, right_name, defs_array)
        else:
            return grammar.Variable(fix_name(unicode(left_name)) + "_" + fix_name(unicode(right_name)),
                                    expression.line, expression.lexpos), max(left_height, right_height)

        if name is None:
            raise Exception("Unexpected parameter state")

        return name, height

    @staticmethod
    def normal_equation(expression, get_new_name, defs_array):
        name, height = ExpressionBuilder.form_expression(expression, get_new_name, defs_array)
        if isinstance(name, grammar.Variable):
            return grammar.Expression(u'*',
                                      grammar.ConstantOperand(1.0, expression.line, expression.lexpos),
                                      grammar.VariableOperand(name, expression.line, expression.lexpos),
                                      expression.line, expression.lexpos), height + 1
        else:
            return grammar.Expression(u'*',
                                      grammar.ConstantOperand(1.0, expression.line, expression.lexpos),
                                      grammar.ConstantOperand(name, expression.line, expression.lexpos),
                                      expression.line, expression.lexpos), 1

    @staticmethod
    def normal_operand(expression):
        v = 1
        if isinstance(expression, grammar.VariableOperand):
            if expression.text.negative:
                expression.text.negative = False
                v = -1
        return grammar.Expression(u'*', grammar.ConstantOperand(1.0 * v, expression.line, expression.lexpos),
                                  expression, expression.line, expression.lexpos), 1

    @staticmethod
    def normal_arithmetic(expression, get_new_name, defs_array):
        """
        :type expression: nl.services.grammar.Expression | nl.services.grammar.Operand
        :type get_new_name:
        :type defs_array: list[nl.services.grammar.Definition]
        :rtype: nl.services.grammar.Expression |nl.services.grammar.ConstantOperand | nl.services.grammar.SumExpression, long
        """
        if isinstance(expression, grammar.Operand):
            return expression, 0

        # if (expr1) + (expr2) then its good for addition form, check expr1 and expr2
        if expression.action == u'+':
            sum_expression = grammar.SumExpression(expression.line, expression.lexpos)
            loperand, lheight = ExpressionBuilder.normal_expression_to_sum(expression.loperand,
                                                                           get_new_name, defs_array)
            roperand, rheight = ExpressionBuilder.normal_expression_to_sum(expression.roperand,
                                                                           get_new_name, defs_array)

            lheight -= 1
            rheight -= 1
            height = max(lheight, rheight)

            ExpressionBuilder.balance_sum_expressions(loperand, height, get_new_name, defs_array)
            for o in loperand.operands:
                sum_expression.add_item(o, height)

            ExpressionBuilder.balance_sum_expressions(roperand, height, get_new_name, defs_array)
            for o in roperand.operands:
                sum_expression.add_item(o, height)

            ExpressionBuilder.balance_sum_expressions(sum_expression, height, get_new_name, defs_array)

            return sum_expression, height + 1

        # if (expr1) * (expr2) then its good only if expr1 and expr2 are simple
        elif expression.action == u'*':
            l_name, lheight = ExpressionBuilder.form_expression(expression.loperand, get_new_name, defs_array)
            simple = False
            if isinstance(l_name, grammar.Variable) and l_name.eval_constant is None:
                expression.loperand = grammar.VariableOperand(l_name, expression.line, expression.lexpos)
                if l_name.is_constant or l_name.is_runtime_constant:
                    simple = True
            else:
                expression.loperand = grammar.ConstantOperand(l_name, expression.line, expression.lexpos)
                simple = True
            r_name, rheight = ExpressionBuilder.form_expression(expression.roperand, get_new_name, defs_array)
            if isinstance(r_name, grammar.Variable) and r_name.eval_constant is None:
                expression.roperand = grammar.VariableOperand(r_name, expression.line, expression.lexpos)
                if r_name.is_constant or r_name.is_runtime_constant:
                    simple = True
            else:
                expression.roperand = grammar.ConstantOperand(r_name, expression.line, expression.lexpos)
                simple = True
            if isinstance(expression.loperand, grammar.ConstantOperand) and\
                    isinstance(expression.roperand, grammar.ConstantOperand):
                return grammar.ConstantOperand(float(expression.loperand.text) * float(expression.roperand.text),
                                               expression.line, expression.lexpos), 0
            elif NodeBuilder.is_operand_constant(expression.roperand):
                (expression.loperand, expression.roperand) = (expression.roperand, expression.loperand)

            left_name, right_name, height = ExpressionBuilder.balance_expressions(expression.loperand.text, lheight,
                                                                                  expression.roperand.text, rheight,
                                                                                  get_new_name, defs_array,
                                                                                  expression.line, expression.lexpos)

            expression.loperand.text = left_name
            expression.roperand.text = right_name

            if isinstance(left_name, grammar.Variable) and left_name.eval_constant is None and \
                    isinstance(right_name, grammar.Variable) and right_name.eval_constant is None and \
                    not left_name.is_constant and not right_name.is_constant and \
                    not left_name.is_runtime_constant and not right_name.is_runtime_constant:
                neg = NodeBuilder.extend_name(left_name, grammar.Variable(u'#neg', left_name.line, left_name.lexpos))
                ng = grammar.NamedDefinition(
                    neg,
                    grammar.Definition(grammar.UNIT,
                                       grammar.DefinitionArguments(
                                           None,
                                           grammar.MultiArgument(
                                               expression.line, expression.lexpos,
                                               [grammar.Argument(u'condition',
                                                                 grammar.Equation(
                                                                     u'>=',
                                                                     grammar.ConstantOperand(0.0,
                                                                                             expression.line,
                                                                                             expression.lexpos),
                                                                     grammar.ConstantOperand(0.0,
                                                                                             expression.line,
                                                                                             expression.lexpos),
                                                                     expression.line, expression.lexpos),
                                                                 expression.line, expression.lexpos),
                                                grammar.Argument(u'expression',
                                                                 grammar.Expression(
                                                                     u'*',
                                                                     grammar.ConstantOperand(-1.0,
                                                                                             expression.line,
                                                                                             expression.lexpos),
                                                                     grammar.VariableOperand(left_name,
                                                                                             expression.line,
                                                                                             expression.lexpos),
                                                                     expression.line, expression.lexpos),
                                                                 expression.line, expression.lexpos)]
                                           ), expression.line, expression.lexpos), expression.line, expression.lexpos),
                    False, expression.line, expression.lexpos)
                ng.pristine = False
                defs_array.append(ng)

                tmp_name = right_name.clone()
                name = left_name.clone()
                name.name += "_" + tmp_name.name
                tmp_name.name = None
                name.merge(tmp_name)
                name = NodeBuilder.extend_name(name, grammar.Variable(u'#' + get_new_name(),
                                                                      expression.line, expression.lexpos))
                ng = grammar.NamedDefinition(
                    name,
                    grammar.Definition(grammar.LINKMOD,
                                       grammar.DefinitionArguments(
                                           None,
                                           grammar.MultiArgument(
                                               expression.line, expression.lexpos,
                                               [grammar.Argument(u'expression',
                                                                 grammar.Expression(
                                                                     u'+',
                                                                     grammar.VariableOperand(left_name,
                                                                                             expression.line,
                                                                                             expression.lexpos),
                                                                     grammar.VariableOperand(neg,
                                                                                             expression.line,
                                                                                             expression.lexpos),
                                                                     expression.line, expression.lexpos),
                                                                 expression.line, expression.lexpos),
                                                grammar.Argument(u'expression',
                                                                 grammar.VariableOperand(right_name,
                                                                                         expression.line,
                                                                                         expression.lexpos),
                                                                 expression.line, expression.lexpos)]
                                           ), expression.line, expression.lexpos), expression.line, expression.lexpos),
                    False, expression.line, expression.lexpos)
                ng.pristine = False
                defs_array.append(ng)

                expression.roperand = grammar.VariableOperand(name, expression.line, expression.lexpos)
                expression.loperand = grammar.ConstantOperand(1.0, expression.line, expression.lexpos)

            return expression, height + (1 if simple else 2)

        # if (expr1) - (expr2) then make it good as (expr1) + (-1) * (expr2)
        elif expression.action == u'-':
            expression.action = u'+'
            if isinstance(expression.roperand, grammar.Expression) and expression.roperand.action == u"*":
                if NodeBuilder.is_operand_number(expression.roperand.loperand):
                    if isinstance(expression.roperand.loperand.text, grammar.Variable):
                        expression.roperand.loperand.text.toggle_negative()
                    else:
                        expression.roperand.loperand.text *= -1
                elif NodeBuilder.is_operand_number(expression.roperand.roperand):
                    if isinstance(expression.roperand.roperand.text, grammar.Variable):
                        expression.roperand.roperandoperand.text.toggle_negative()
                    else:
                        expression.roperand.roperand.text *= -1
                else:
                    expression.roperand.loperand.text.toggle_negative()
            else:
                if isinstance(expression.roperand.text, grammar.Variable):
                    expression.roperand.text.toggle_negative()
                else:
                    expression.roperand.text *= -1

            sum_expression = grammar.SumExpression(expression.line, expression.lexpos)
            loperand, lheight = ExpressionBuilder.normal_expression_to_sum(expression.loperand,
                                                                           get_new_name, defs_array)
            roperand, rheight = ExpressionBuilder.normal_expression_to_sum(expression.roperand,
                                                                           get_new_name, defs_array)

            lheight -= 1
            rheight -= 1
            height = max(lheight, rheight)

            ExpressionBuilder.balance_sum_expressions(loperand, height, get_new_name, defs_array)
            for o in loperand.operands:
                sum_expression.add_item(o, height)

            ExpressionBuilder.balance_sum_expressions(roperand, height, get_new_name, defs_array)
            for o in roperand.operands:
                sum_expression.add_item(o, height)

            ExpressionBuilder.balance_sum_expressions(sum_expression, height, get_new_name, defs_array)

            return sum_expression, height + 1
        # if (expr1) / (expr2) then its good only if expr2 is constant,
        # so make it constant and turn into (expr1) * (1/expr2)
        elif expression.action == u'/':
            expression.loperand, lheight = ExpressionBuilder.normal_expression_to_operand(expression.loperand,
                                                                                          get_new_name, defs_array)
            name, rheight = ExpressionBuilder.form_expression(expression.roperand, get_new_name, defs_array)
            if isinstance(name, grammar.Variable):
                raise model.GraphError('Division must have constant denominator. Expression "' + repr(
                    expression.roperand) + '" does not evaluate into constant')
            if abs(name) < 1e-7:
                raise model.GraphError('Division by zero detected in expression "' + repr(expression.roperand))
            expression.action = u'*'
            expression.roperand = expression.loperand
            expression.loperand = grammar.ConstantOperand(1.0 / name, expression.line, expression.lexpos)

            return expression, max(lheight, rheight) + 1

        return expression, 0

    @staticmethod
    def normal_expression_to_sum(expression, get_new_name, defs_array):
        height = 0
        if isinstance(expression, grammar.Equation):
            #SumExpression and number
            expression, height = ExpressionBuilder.normal_equation(expression, get_new_name, defs_array)
            t = grammar.SumExpression(expression.line, expression.lexpos)
            t.add_item(expression, height)
            expression = t
            height += 1
        if isinstance(expression, grammar.Expression):
            #SumExpression and number
            expression, height = ExpressionBuilder.normal_arithmetic(expression, get_new_name, defs_array)
            if not isinstance(expression, grammar.SumExpression):
                t = grammar.SumExpression(expression.line, expression.lexpos)
                t.add_item(expression, height)
                expression = t
                height += 1
        if isinstance(expression, grammar.Operand):
            #SumExpression and number
            expression, height = ExpressionBuilder.normal_operand(expression)
            t = grammar.SumExpression(expression.line, expression.lexpos)
            t.add_item(expression, height)
            expression = t
            height += 1
        return expression, height

    @staticmethod
    def normal_expression_to_operand(expression, get_new_name, defs_array):
        height = 0
        if isinstance(expression, grammar.Equation):
            #Operand and number
            expression, height = ExpressionBuilder.normal_equation(expression, get_new_name, defs_array)
            expression = expression.roperand
            height -= 1
        if isinstance(expression, grammar.Expression) or isinstance(expression, grammar.SumExpression):
            if isinstance(expression, grammar.Expression):
                ex = ExpressionBuilder.normal_arithmetic(expression, get_new_name, defs_array)[0]
            else:
                ex = expression
            n, h = ExpressionBuilder.form_expression(ex, get_new_name, defs_array)
            #Operand and number
            if isinstance(n, grammar.Variable):
                expression, height = grammar.VariableOperand(n, expression.line, expression.lexpos), h
            else:
                expression, height = grammar.ConstantOperand(n, expression.line, expression.lexpos), 0
        if isinstance(expression, grammar.Operand):
            #Operand and number already
            pass
        return expression, height

    @staticmethod
    def get_dependencies(expression):
        res = []
        if isinstance(expression, grammar.Expression):
            res += ExpressionBuilder.get_dependencies(expression.loperand)
            res += ExpressionBuilder.get_dependencies(expression.roperand)
        if isinstance(expression, grammar.VariableOperand):
            res.append(expression.text)
        return res
