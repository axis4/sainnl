# coding=UTF-8
"""
   Copyright 2014 IoS (progsdi@gmail.com)

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""
import copy
from nl.services import grammar
from nl.services.graph_util import model
from nl.services.graph_util.expression import ExpressionBuilder
from nl.services.graph_util.expression_tree import ExpressionTreeTraversal, TreeResult
from nl.services.graph_util.nodes import NodeBuilder
from nl.services.graph_util.util import has_template_arguments, get_expression_vars

__author__ = 'sergeionov'


class Permutations(object):
    @staticmethod
    def rename_permutation(group, group_params, from_var, to_var, is_stable=False):
        h = grammar.GroupHeader(group.name + ("#" + unicode(from_var) + ":" + unicode(to_var) if not is_stable else ""),
                                grammar.MultiArgument(group.line, group.lexpos,
                                                      Permutations.rename_arguments(group.templates, from_var, to_var)),
                                grammar.MultiArgument(group.line, group.lexpos,
                                                      Permutations.rename_arguments(group.arguments, from_var, to_var)),
                                group.line, group.lexpos)
        defs = []
        b = grammar.GroupBody(defs, group.line, group.lexpos)
        res = grammar.Group(h, b, group.line, group.lexpos)
        group_params[res.name] = {"ins": [], "outs": [], "syncs": [], "asyncs": []}
        for d in group.definitions:
            nd = copy.deepcopy(d)
            NodeBuilder.replace_name(nd, from_var, to_var, mark_renamed=True)
            if d.group_name == grammar.IN or d.group_name == grammar.OUT or\
                    d.group_name == grammar.SYNC or d.group_name == grammar.ASYNC:
                key = {grammar.IN: "ins", grammar.OUT: "outs",
                       grammar.SYNC: "syncs", grammar.ASYNC: "asyncs"}[d.group_name]
                group_params[res.name][key].append(nd)
            defs.append(nd)
        return res

    @staticmethod
    def rename_arguments(arguments, from_var, to_var):
        """
        :type arguments: list[grammar.Argument | grammar.Operand | grammar.Sequence | grammar.Generator |
                grammar.Equation]
        :type from_var: grammar.Variable
        :type to_var: grammar.Variable | float
        :rtype:
        """
        arguments = copy.deepcopy(arguments)
        """:type: list[grammar.Argument | grammar.Operand | grammar.Sequence | grammar.Generator | grammar.Equation]"""
        for a in arguments:
            if not isinstance(a, grammar.Argument) or not isinstance(a.value, grammar.Variable):
                NodeBuilder.replace_name(a, from_var, to_var, mark_renamed=True)
        return arguments

    @staticmethod
    def expand_permutation(group, group_params, var, var_range, conditional, is_stable, is_last=False):
        """
        :type group: grammar.Group
        :type group_params:
        :type var: grammar.Variable
        :type var_range: tuple(str | unicode | int | long | float)
        :type conditional: list[grammar.Equation]
        :type is_stable: bool
        :type is_last: bool
        """
        args = grammar.MultiArgument(group.line, group.lexpos, group.arguments)
        Permutations.expand_as_list(args, var, var_range, conditional, is_last)
        h = grammar.GroupHeader(group.name + ("#" + unicode(var) + ":" + "..".join([unicode(i) for i in var_range])
                                              if not is_stable else ""),
                                grammar.MultiArgument(group.line, group.lexpos, group.templates), args,
                                group.line, group.lexpos)
        defs = []
        b = grammar.GroupBody(defs, group.line, group.lexpos)
        res = grammar.Group(h, b, group.line, group.lexpos)
        group_params[res.name] = {"ins": [], "outs": [], "syncs": [], "asyncs": []}
        for d in group.definitions:
            if isinstance(d, grammar.NamedDefinition) and NodeBuilder.has_template_name(d.result_name, var):
                for i in range(int(var_range[0]), int(var_range[1] + 1)):
                    nd = copy.deepcopy(d)
                    nd.permutation_state[unicode(var)] = float(i)
                    if Permutations.check_allowed(nd.permutation_state, conditional, is_last):
                        NodeBuilder.replace_name(nd, var, float(i))

                        if d.group_name == grammar.UNIT or d.group_name == grammar.LINKMOD:
                            Permutations.check_sum(nd, conditional, is_last)
                        else:
                            Permutations.check_list(nd, conditional, is_last)

                        if is_last:
                            Permutations.recount(nd)
                        defs.append(nd)
                        if d.group_name == grammar.IN or d.group_name == grammar.OUT or\
                                d.group_name == grammar.SYNC or d.group_name == grammar.ASYNC:
                            key = {grammar.IN: "ins", grammar.OUT: "outs",
                                   grammar.SYNC: "syncs", grammar.ASYNC: "asyncs"}[d.group_name]
                            group_params[res.name][key].append(nd)
            else:
                nd = copy.deepcopy(d)
                defs.append(nd)
                if d.group_name == grammar.IN or d.group_name == grammar.OUT or\
                        d.group_name == grammar.SYNC or d.group_name == grammar.ASYNC:
                    key = {grammar.IN: "ins", grammar.OUT: "outs",
                           grammar.SYNC: "syncs", grammar.ASYNC: "asyncs"}[d.group_name]
                    group_params[res.name][key].append(nd)
                if NodeBuilder.has_template_name(d, var):
                    if d.group_name == grammar.UNIT or d.group_name == grammar.LINKMOD:
                        Permutations.expand_as_sum(nd, var, var_range, conditional, is_last)
                    else:
                        Permutations.expand_as_list(nd, var, var_range, conditional, is_last)
                if is_last:
                    Permutations.recount(nd)

        return res

    @staticmethod
    def recount(definition):
        """
        :type definition: grammar.Definition
        """
        def counter(item, use_less, children_values):
            if isinstance(item, grammar.VariableOperand):
                if item.text.eval_constant is not None:
                    return grammar.ConstantOperand(float(item.text), item.line, item.lexpos), True
            if isinstance(item, grammar.Expression):
                left_c = None
                if isinstance(children_values[0].value, grammar.ConstantOperand):
                    left_c = children_values[0].value
                elif isinstance(item.loperand, grammar.ConstantOperand):
                    left_c = item.loperand
                right_c = None
                if isinstance(children_values[1].value, grammar.ConstantOperand):
                    right_c = children_values[1].value
                elif isinstance(item.roperand, grammar.ConstantOperand):
                    right_c = item.roperand
                if left_c is not None and right_c is not None:
                    from nl.services.graph_util.expression import ExpressionBuilder
                    value_c = ExpressionBuilder.primitive_expression(
                        left_c.text, right_c.text, item)
                    return grammar.ConstantOperand(value_c[0], item.line, item.lexpos), True
            if isinstance(item, grammar.SumExpression):
                if all(map(
                        lambda x: NodeBuilder.is_operand_number(x[1].value)
                        if x[1].replace
                        else NodeBuilder.is_operand_number(item.operands[x[0]]), enumerate(children_values))):
                    value_c = reduce(lambda x, y: x + ((float(y[1].value.text)
                                                        if y[1].replace else float(item.operands[y[0]].text))
                                                       if isinstance(y[1], TreeResult) else float(y[1])),
                                     enumerate(children_values), 0.0)
                    return grammar.ConstantOperand(value_c, item.line, item.lexpos), True
            return None, False
        ExpressionTreeTraversal.pass_tree(definition, postprocess_current=counter)

    @staticmethod
    def check_allowed(state, conditionals, is_last):
        """
        :type state: dict[str | unicode, int | float]
        :type conditionals: list[grammar.Equation]
        :type is_last: bool
        """
        for c in conditionals:
            lop = c.loperand.clone()
            for s in state:
                NodeBuilder.replace_name(lop, grammar.Variable(s, 0, 0), state[s])
            rop = c.roperand.clone()
            for s in state:
                NodeBuilder.replace_name(rop, grammar.Variable(s, 0, 0), state[s])
            l_name, l_height = ExpressionBuilder.form_expression(lop, lambda x: 10, [])
            r_name, r_height = ExpressionBuilder.form_expression(rop, lambda x: 10, [])
            if not isinstance(l_name, grammar.Variable) or l_name.eval_constant is not None:
                if not isinstance(r_name, grammar.Variable) or r_name.eval_constant is not None:
                    if c.action == '!=' and not float(l_name) != float(r_name):
                        return False
                    if c.action == '==' and not float(l_name) == float(r_name):
                        return False
                    if c.action == '<=' and not float(l_name) <= float(r_name):
                        return False
                    if c.action == '>=' and not float(l_name) >= float(r_name):
                        return False
                    if c.action == '<' and not float(l_name) < float(r_name):
                        return False
                    if c.action == '>' and not float(l_name) > float(r_name):
                        return False
        return True

    @staticmethod
    def check_sum(definition, conditional, is_last):
        """
        :type definition: grammar.Definition
        :type conditional: list[grammar.Equation]
        :type is_last: bool
        """
        if definition.arguments.call_arguments is None:
            definition.arguments.call_arguments = grammar.MultiArgument(definition.line, definition.lexpos)
        for a in definition.arguments.call_arguments.arguments:
            Permutations.check_sum_exp(a, definition, conditional, is_last)

    @staticmethod
    def check_sum_exp(argument, definition, conditional, is_last):
        """
        :type argument: grammar.Argument | grammar.Equation | grammar.SumExpression
        :type definition: grammar.Definition
        :type conditional: list[grammar.Equation]
        :type is_last: bool
        """
        if isinstance(argument, grammar.Argument):
            Permutations.check_sum_exp(argument.value, definition, conditional, is_last)
        elif isinstance(argument, grammar.Equation):
            Permutations.check_sum_exp(argument.loperand, definition, conditional, is_last)
        elif isinstance(argument, grammar.SumExpression):
            nop = []
            nhe = []
            for i in range(len(argument.operands)):
                if Permutations.check_allowed(
                        dict(definition.permutation_state.items() + argument.operands[i].permutation_state.items()),
                        conditional, is_last):
                    nop.append(argument.operands[i])
                    nhe.append(argument.heights[i])
            argument.operands = nop
            argument.heights = nhe

    @staticmethod
    def check_list(definition, conditional, is_last):
        """
        :type definition: grammar.Definition
        :type conditional: list[grammar.Equation]
        :type is_last: bool
        """
        na = []
        if definition.arguments.call_arguments is None:
            definition.arguments.call_arguments = grammar.MultiArgument(definition.line, definition.lexpos)
        for a in definition.arguments.call_arguments.arguments:
            if Permutations.check_allowed(dict(definition.permutation_state.items() + a.permutation_state.items()),
                                          conditional, is_last):
                na.append(a)
        definition.arguments.call_arguments.arguments = na

    @staticmethod
    def expand_as_list(definition, var, var_range, conditional, is_last):
        """
        :type definition: grammar.Definition | grammar.MultiArgument
        :type var: grammar.Variable
        :type var_range: tuple[str | unicode | int | long | float ]
        :type conditional: list[grammar.Equation]
        :type is_last: bool
        """
        na = []
        if isinstance(definition, grammar.Definition):
            target = definition.arguments.call_arguments.arguments
        else:  # expecting MultiArgument
            target = definition.arguments
        for a in target:
            if NodeBuilder.has_template_name(a.value.text, var):
                for i in range(int(var_range[0]), int(var_range[1] + 1)):
                    ta = copy.deepcopy(a)
                    ta.permutation_state[unicode(var)] = float(i)
                    if Permutations.check_allowed(
                            dict(definition.permutation_state.items() + ta.permutation_state.items()),
                            conditional, is_last):
                        NodeBuilder.replace_name(ta, var, float(i))
                        na.append(ta)
            else:
                na.append(a)
        target[:] = na

    @staticmethod
    def expand_as_reduction(definition, var, var_range, conditional, is_last):
        """
        :type definition: grammar.Definition
        :type var: grammar.Variable
        :type var_range: tuple[str | unicode | int | long ]
        :type conditional: list[grammar.Equation]
        :type is_last: bool
        :rtype: list[grammar.Definition]
        """
        res = []
        for i in range(int(var_range[0]), int(var_range[1] + 1)):
            nd = copy.deepcopy(definition)
            NodeBuilder.replace_name(nd, var, float(i))
            nd.permutation_state[unicode(var)] = float(i)
            if Permutations.check_allowed(nd.permutation_state, conditional, is_last):
                res.append(nd)
        if len(res):
            if definition.group_name == grammar.UNIT or definition.group_name == grammar.LINKMOD:
                replacement = res.pop()
                definition.group_name = replacement.group_name
                if hasattr(definition, "result_name"):
                    definition.result_name = replacement.result_name
                definition.arguments = replacement.arguments
            else:
                definition.group_name = grammar.UNIT
                se = grammar.SumExpression(definition.line, definition.lexpos)
                if hasattr(definition, "result_name"):
                    for i, r in enumerate(res):
                        r.result_name = NodeBuilder.extend_name(
                            r.result_name,
                            grammar.Variable(u"#reduction" + str(i), definition.line, definition.lexpos))
                        se.add_item(grammar.VariableOperand(r.result_name, definition.line, definition.lexpos), 0)
                else:
                    se.add_item(grammar.ConstantOperand(0.0, definition.line, definition.lexpos), 0)
                definition.arguments.call_arguments.arguments = [
                    grammar.Argument(u"condition", grammar.Equation(
                        u">=",
                        grammar.ConstantOperand(0.0, definition.line, definition.lexpos),
                        grammar.ConstantOperand(0.0, definition.line, definition.lexpos),
                        definition.line, definition.lexpos), definition.line, definition.lexpos),
                    grammar.Argument(u"expression", se, definition.line, definition.lexpos)]
        return res

    @staticmethod
    def expand_as_sum(definition, var, var_range, conditional, is_last):
        na = []
        if definition.arguments.call_arguments is None:
            definition.arguments.call_arguments = grammar.MultiArgument(definition.line, definition.lexpos)
        for a in definition.arguments.call_arguments.arguments:
            ta = copy.deepcopy(a)
            na.append(ta)
            Permutations.expand_sum_exp(ta, var, var_range, definition.permutation_state, conditional, is_last)
        definition.arguments.call_arguments.arguments = na

    @staticmethod
    def expand_sum_exp(a, var, var_range, permutation_state, conditional, is_last):
        if isinstance(a, grammar.Argument):
            a.value = Permutations.expand_sum_exp(a.value, var, var_range, permutation_state, conditional, is_last)
        if isinstance(a, grammar.Equation):
            a.loperand = Permutations.expand_sum_exp(a.loperand, var, var_range, permutation_state, conditional,
                                                     is_last)
            a.roperand = Permutations.expand_sum_exp(a.roperand, var, var_range, permutation_state, conditional,
                                                     is_last)

        if isinstance(a, grammar.Expression):
            if (isinstance(a.loperand, grammar.VariableOperand) and
                    NodeBuilder.has_template_name(a.loperand.text, var)) or \
                    (isinstance(a.roperand, grammar.VariableOperand) and
                     NodeBuilder.has_template_name(a.roperand.text, var)):
                na = grammar.SumExpression(a.line, a.lexpos)
                for i in range(int(var_range[0]), int(var_range[1] + 1)):
                    no = copy.deepcopy(a)
                    NodeBuilder.replace_name(no, var, float(i))
                    no.permutation_state[unicode(var)] = float(i)
                    if Permutations.check_allowed(dict(permutation_state.items() + no.permutation_state.items()),
                                                  conditional, is_last):
                        na.add_item(no, 2)
                a = na
        if isinstance(a, grammar.SumExpression):
            for k in range(len(a.operands)):
                if (isinstance(a.operands[k], grammar.Operand) and
                        NodeBuilder.has_template_name(a.operands[k].text, var)) or\
                        (isinstance(a.operands[k], grammar.Expression) and
                         ((isinstance(a.operands[k].loperand, grammar.VariableOperand) and
                           NodeBuilder.has_template_name(a.operands[k].loperand.text, var)) or
                          (isinstance(a.operands[k].roperand, grammar.VariableOperand) and
                           NodeBuilder.has_template_name(a.operands[k].roperand.text, var)))):
                    for i in range(int(var_range[0]), int(var_range[1] + 1)):
                        no = copy.deepcopy(a.operands[k])
                        NodeBuilder.replace_name(no, var, float(i))
                        no.permutation_state[unicode(var)] = float(i)
                        if Permutations.check_allowed(dict(permutation_state.items() + no.permutation_state.items()),
                                                      conditional, is_last):
                            a.add_item(no, a.heights[k])
                    a.operands[k] = None
                    a.heights[k] = None
            a.operands[:] = [o for o in a.operands if o is not None]
            a.heights[:] = [o for o in a.heights if o is not None]

        if isinstance(a, grammar.VariableOperand):  # only if equation or argument was too simple
            if NodeBuilder.has_template_name(a.text, var):
                na = grammar.SumExpression(a.line, a.lexpos)
                for i in range(int(var_range[0]), int(var_range[1] + 1)):
                    no = copy.deepcopy(a)
                    NodeBuilder.replace_name(no, var, float(i))
                    no.permutation_state[unicode(var)] = float(i)
                    if Permutations.check_allowed(dict(permutation_state.items() + no.permutation_state.items()),
                                                  conditional, is_last):
                        na.add_item(no, 2)
                a = na
        return a

    @staticmethod
    def pass_permutations(d, gr, group_params, global_constants):
        #prepare state for permutations:
        #1. apply renaming
        #2. apply variables
        renamed_template = {}
        renamable = NodeBuilder.get_template_dependencies(gr.templates)
        is_stable = True

        if d is not None:
            if has_template_arguments(d):
                for a in d.arguments.template_arguments.arguments:
                    if not unicode(a.key) in [unicode(r) for r in renamable]:
                        raise model.GraphError(
                            'Definition "' + d.group_name + '" does not accept template key ' + unicode(a.key) +
                            '. Available keys: ' + ','.join([unicode(r) for r in renamable]))
                    for r in renamable:
                        if unicode(r) == unicode(a.key):
                            renamable.remove(r)
                    renamed_template[unicode(a.key)] = a.value.text if isinstance(a.value, grammar.Operand)\
                        else a.value
                    is_stable = False
                    gr = Permutations.rename_permutation(gr, group_params, a.key, renamed_template[unicode(a.key)], is_stable)
                    if renamed_template[unicode(a.key)] in global_constants:
                        renamable.append(renamed_template[unicode(a.key)])
        else:
            # to name something in errors
            d = grammar.Definition("default_naming_#", grammar.DefinitionArguments(None, None, 0, 0), 0, 0)
        for a in global_constants:
            if a in [unicode(r) for r in renamable]:
                gr = Permutations.rename_permutation(gr, group_params, global_constants[a].var_name,
                                                     global_constants[a].value, is_stable)
                renamed_template[a] = global_constants[a].value

        ranges = {}
        dynamics = {}
        conditional = []
        for s in gr.templates:
            if isinstance(s, grammar.Sequence):
                st_name, st_height = ExpressionBuilder.form_expression(s.start.clone(), lambda: "10", [])
                if isinstance(st_name, grammar.Variable) and st_name.eval_constant is None:
                    raise model.DelayError('Group "' + d.group_name +
                                           '" requires sequence to form constant range ' + repr(s), st_name)
                et_name, et_height = ExpressionBuilder.form_expression(s.end.clone(), lambda: "10", [])
                if isinstance(et_name, grammar.Variable) and et_name.eval_constant is None:
                    raise model.DelayError('Group "' + d.group_name +
                                           '" requires sequence to form constant range ' + repr(s), et_name)
                if float(st_name) > float(et_name):
                    raise model.GraphError(
                        'For group "' + d.group_name +
                        '" defined an empty range %d..%d for sequence ' % (st_name, et_name) + repr(s))

                ranges[s.var_name] = [float(st_name), float(et_name)]
            if isinstance(s, grammar.Equation):
                conditional.append(s)
            if isinstance(s, grammar.Generator):
                dynamics[unicode(s.var_name)] = s.linked_name

        var_names = sorted([k for k in ranges.iterkeys()], key=lambda x: unicode(x))
        if not len(var_names) and len(conditional):
            raise model.GraphError("Conditional is not static")
        for c in conditional:
            for v in get_expression_vars(c):
                if unicode(v) in dynamics:
                    raise model.GraphError("Conditional is not static")
        for vi in range(len(var_names)):
            gr = Permutations.expand_permutation(gr, group_params, var_names[vi], ranges[var_names[vi]], conditional,
                                                 is_stable, vi == len(var_names) - 1)
        return gr, renamed_template
