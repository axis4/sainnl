# coding=UTF-8
"""
   Copyright 2014 IoS (progsdi@gmail.com)

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""
import traceback
from nl.services import grammar

__author__ = 'sergeionov'

ADDITIVE = 'neuron'
LINKMOD = 'multiplicator'
EXPECTED = 'expected'
MINS = {
    'int8': -255.0
}
MAXS = {
    'int8': 255.0
}


class GraphError(Exception):
    pass


class DelayError(GraphError):
    def __init__(self, msg, var, *args, **kwargs):
        self.var = var
        args = (msg,) + args
        super(DelayError, self).__init__(*args, **kwargs)


class Node(object):
    def __init__(self, layer, node_id, threshold, constant, ntype=ADDITIVE, template=False, is_input=False,
                 is_output=False, is_reducable=False):
        if node_id is None or not isinstance(node_id, grammar.Variable) or node_id.name is None:
            raise Exception("Unexpected parameter state")
        self.layer = layer
        self.node_id = node_id
        self.threshold = threshold
        self.constant = constant
        self.kind = ntype
        self.template = template
        self.input = is_input
        self.output = is_output
        self.reducable = is_reducable
        self.x = 0.0
        self.y = 0.0
        self.d_x = 0.0
        self.d_y = 0.0

    def __str__(self):
        return u"%s(%s, %s)" % (unicode(self.node_id), unicode(self.threshold), unicode(self.constant))

    def __repr__(self):
        return u"%s(%s, %s)" % (unicode(self.node_id), unicode(self.threshold), unicode(self.constant))


class Link(object):
    def __init__(self, node_from, node_to, additive_weight, stream_weight, generation_weight,
                 alignable=True):
        if node_from is None or not isinstance(node_from, grammar.Variable):
            raise Exception("Unexpected parameter state")
        if node_to is None or not isinstance(node_to, grammar.Variable):
            raise Exception("Unexpected parameter state")
        self.node_from = node_from
        self.node_to = node_to
        self.additive_weight = additive_weight
        self.stream_weight = stream_weight
        self.generation_weight = generation_weight
        self.alignable = alignable

    def __str__(self):
        return u"%s -> %s" % (self.node_from, self.node_to)

    def __repr__(self):
        return u"%s -> %s" % (self.node_from, self.node_to)


class Graph(object):
    def __init__(self):
        self.id_number = 100000
        self.nodes = {}
        """:type: dict[unicode, Node]"""
        self.links = {}
        """:type: dict[unicode, list[Link]]"""
        self.height = 0

    def set_node(self, node_name, node):
        node_name = unicode(node_name)
        if node_name in self.nodes:
            if node.kind == EXPECTED or node.input:
                return
            if self.nodes[node_name].kind != EXPECTED:
                if not self.nodes[node_name].reducable or self.nodes[node_name].threshold != node.threshold or\
                        self.nodes[node_name].template != node.template or self.nodes[node_name].kind != node.kind:
                    raise GraphError("Node " + node_name + " is not reducable")
                try:
                    node.constant = float(node.constant) + float(self.nodes[node_name].constant)
                except TypeError:
                    pass  # non constants => variables
                node.input = node.input or self.nodes[node_name].input
                node.output = node.input or self.nodes[node_name].output
        self.nodes[node_name] = node

    def update_nodes_and_links(self, nodes, links):
        """
        :type nodes: dict[unicode, nl.services.graph_util.model.Node]
        :type links: dict[unicode, list[nl.services.graph_util.model.Link]]
        :return:
        """
        # TODO: check if additive reduction is required
        for k, v in nodes.iteritems():
            self.set_node(k, v)
        for k, v in links.iteritems():
            if k in self.links:
                for new_l in v:
                    ok = True
                    for existing_l in self.links[k]:
                        if unicode(existing_l.node_to) == unicode(new_l.node_to):
                            ok = False
                    if ok:
                        self.links[k].append(new_l)
            else:
                self.links[k] = v

    def get_new_id(self):
        self.id_number += 1
        return self.id_number.__str__()

    def inputs(self):
        return [n for n in self.nodes.values() if n.input]

    def outputs(self):
        return [n for n in self.nodes.values() if n.output]

    def clone(self, prefix):
        g = Graph()
        from nl.services.graph_util.nodes import NodeBuilder

        if prefix is not None:
            if not isinstance(prefix, grammar.Variable):
                prefix = grammar.Variable(prefix, 0, 0)

        def extend_param(v):
            try:
                return NodeBuilder.extend_name(prefix, v, deep=True)
            except:
                return v

        def rename_node(node):
            return Node(node.layer,
                        NodeBuilder.extend_name(prefix, node.node_id, deep=True) if prefix is not None
                        else node.node_id.clone(),
                        extend_param(node.threshold), extend_param(node.constant), node.kind, template=node.template,
                        is_input=node.input, is_output=node.output, is_reducable=node.reducable)

        def rename_link(ls):
            return [Link(NodeBuilder.extend_name(prefix, link.node_from, deep=True) if prefix is not None
                         else link.node_from.clone(),
                         NodeBuilder.extend_name(prefix, link.node_to, deep=True) if prefix is not None
                         else link.node_to.clone(),
                         extend_param(link.additive_weight), extend_param(link.stream_weight),
                         extend_param(link.generation_weight)) for link in ls]

        g.nodes = dict([(unicode(NodeBuilder.extend_name(prefix, n.node_id, deep=True)
                                 if prefix is not None else n.node_id), rename_node(n))
                        for n in self.nodes.itervalues()])
        g.links = dict([(unicode(NodeBuilder.extend_name(prefix, l[0].node_from, deep=True)
                                 if prefix is not None else l[0].node_from), rename_link(l))
                        for l in self.links.itervalues() if len(l)])
        g.height = self.height
        g.id_number = self.id_number
        return g

    def to_xml(self):
        """
        :rtype: unicode
        """
        print "converting to xml"

        def float_conv(value):
            try:
                return u"{0:f}".format(float(value))
            except TypeError:
                return unicode(value)

        def float_check(value):
            try:
                return abs(float(value)) > 0
            except TypeError:
                return True

        s = ""
        for v in sorted(self.nodes.itervalues(), key=lambda x: unicode(x.node_id)):
            links = ""
            for l in self.links.get(unicode(v.node_id), []):
                links += u"<link><to>{0:s}</to>{1:s}{2:s}{3:s}</link>".format(
                    unicode(l.node_to),
                    u"<activation>{0:s}</activation>".format(float_conv(l.additive_weight))
                    if float_check(l.additive_weight)
                    else "",
                    u"<stream>{0:s}</stream>".format(float_conv(l.stream_weight))
                    if float_check(l.stream_weight)
                    else "",
                    u"<generation>{0:s}</generation>".format(float_conv(l.generation_weight))
                    if float_check(l.generation_weight)
                    else "")
            try:
                fs = u"<node><id>{0:s}</id><type>{1:s}</type><x>{2:d}</x><y>{3:d}</y>{4:s}{5:s}{6:s}{7:s}{8:s}" \
                     u"<links>{9:s}</links></node>"
                s += fs.format(
                    unicode(v.node_id), v.kind, int(v.x), int(v.y),
                    u"<threshold>{0:s}</threshold>".format(float_conv(v.threshold)) if float_check(v.threshold) else "",
                    u"<constant>{0:s}</constant>".format(float_conv(v.constant)) if float_check(v.constant) else "",
                    u"<input>1</input>" if v.input else "",
                    u"<output>1</output>" if v.output else "",
                    u"<template>1</template>" if v.template else "",
                    links)
            except Exception:
                raise Exception("Trouble with " + repr(v) + ": " + traceback.format_exc())
        return u"<nodes>{0:s}</nodes>".format(s)
