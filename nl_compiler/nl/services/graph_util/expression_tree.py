# coding=UTF-8
"""
   Copyright 2014 IoS (progsdi@gmail.com)

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""
from nl.services import grammar

__author__ = 'sergeionov'


class TreeResult(object):
    def __init__(self, value, replace):
        self.value = value
        self.replace = replace


class TreeTraversal(object):
    @staticmethod
    def pass_tree(item, next_step=lambda x: [], preprocess_current=lambda x: None,
                  postprocess_current=lambda x, prevalue, childrenvalues: (None, False)):
        return TreeTraversal._pass_tree(item, next_step, preprocess_current, postprocess_current).value

    @staticmethod
    def _pass_tree(item, next_step, preprocess_current, postprocess_current):
        v = preprocess_current(item)
        cv = []
        ni = next_step(item)
        if ni is not None:
            cv = [TreeTraversal._pass_tree(i, next_step, preprocess_current, postprocess_current) for i in ni]
        v, with_replace = postprocess_current(item, v, cv)
        return TreeResult(v, with_replace)


class ExpressionTreeTraversal(object):
    @staticmethod
    def pass_tree(expression, preprocess_current=lambda x: None,
                  postprocess_current=lambda x, prevalue, childrenvalues: (None, False)):
        return TreeTraversal.pass_tree(expression, ExpressionTreeTraversal._next_step,
                                       preprocess_current,
                                       lambda x, prevalue, childrenvalues:
                                       ExpressionTreeTraversal._replace_step(x, prevalue, childrenvalues,
                                                                             postprocess_current))

    @staticmethod
    def _next_step(expression):
        if isinstance(expression, grammar.Group):
            return expression.templates[:] + expression.arguments[:] + expression.definitions[:]
        elif isinstance(expression, grammar.TemplateDefinition):
            return expression.template_vars.arguments[:] + ExpressionTreeTraversal._next_step(expression.templated)
        elif isinstance(expression, grammar.TemplateNamedDefinition):
            return expression.template_vars.arguments[:] + ExpressionTreeTraversal._next_step(expression.named)
        elif isinstance(expression, grammar.NamedDefinition):
            return [expression.result_name] + ExpressionTreeTraversal._next_step(expression.chain_to)
        elif isinstance(expression, grammar.Definition):
            return (expression.arguments.template_arguments.arguments[:]
                    if expression.arguments.template_arguments is not None else []) +\
                (expression.arguments.call_arguments.arguments[:]
                 if expression.arguments.call_arguments is not None else [])
        elif isinstance(expression, grammar.Argument):
            return [expression.value]
        elif isinstance(expression, grammar.Sequence):
            return [expression.var_name, expression.start, expression.end]
        elif isinstance(expression, grammar.Generator):
            return [expression.var_name, expression.linked_name]
        elif isinstance(expression, grammar.Set):
            return [expression.var_name, expression.value]
        elif isinstance(expression, grammar.Expression) or isinstance(expression, grammar.Equation):
            return [expression.loperand, expression.roperand]
        elif isinstance(expression, grammar.SumExpression):
            return [o for o in expression.operands]
        elif isinstance(expression, grammar.Operand):
            return [expression.text]
        elif isinstance(expression, grammar.Variable):
            res = expression.indexvars[:]
            t = expression
            while t.subvar is not None:
                t = t.subvar
                res += t.indexvars[:]
            return res
        return []

    @staticmethod
    def _replace_step(expression, prevalue, childrenvalues, postprocess_current):
        def list_set(lst, value, index):
            lst[index] = value

        def field_set(obj, field, value):
            setattr(obj, field, value)

        if isinstance(expression, grammar.Group):
            groups = [
                [lambda x, ind: list_set(expression.templates, x, ind) for i in range(len(expression.templates))],
                [lambda x, ind: list_set(expression.arguments, x, ind) for i in range(len(expression.arguments))],
                [lambda x, ind: list_set(expression.definitions, x, ind)
                 for i in range(len(expression.definitions))]]
        elif isinstance(expression, grammar.TemplateDefinition):
            groups = [
                [lambda x, ind: list_set(expression.template_vars.arguments, x, ind)
                 for i in range(len(expression.template_vars.arguments))],
                [lambda x, ind: list_set(expression.arguments.template_arguments.arguments, x, ind)
                 for i in range(len(expression.arguments.template_arguments.arguments))]
                if expression.arguments.template_arguments is not None else [],
                [lambda x, ind: list_set(expression.arguments.template_arguments.arguments, x, ind)
                 for i in range(len(expression.arguments.call_arguments.arguments))]
                if expression.arguments.call_arguments is not None else []]
        elif isinstance(expression, grammar.TemplateNamedDefinition):
            groups = [
                [lambda x, ind: list_set(expression.template_vars.arguments, x, ind)
                 for i in range(len(expression.template_vars.arguments))],
                [lambda x, ind: field_set(expression, "result_name", x)],
                [lambda x, ind: list_set(expression.arguments.template_arguments.arguments, x, ind)
                 for i in range(len(expression.arguments.template_arguments.arguments))]
                if expression.arguments.template_arguments is not None else [],
                [lambda x, ind: list_set(expression.arguments.template_arguments.arguments, x, ind)
                 for i in range(len(expression.arguments.call_arguments.arguments))]
                if expression.arguments.call_arguments is not None else []]
        elif isinstance(expression, grammar.NamedDefinition):
            groups = [
                [lambda x, ind: field_set(expression, "result_name", x)],
                [lambda x, ind: list_set(expression.arguments.template_arguments.arguments, x, ind)
                 for i in range(len(expression.arguments.template_arguments.arguments))]
                if expression.arguments.template_arguments is not None else [],
                [lambda x, ind: list_set(expression.arguments.template_arguments.arguments, x, ind)
                 for i in range(len(expression.arguments.call_arguments.arguments))]
                if expression.arguments.call_arguments is not None else []]
        elif isinstance(expression, grammar.Definition):
            groups = [
                [lambda x, ind: list_set(expression.arguments.template_arguments.arguments, x, ind)
                 for i in range(len(expression.arguments.template_arguments.arguments))]
                if expression.arguments.template_arguments is not None else [],
                [lambda x, ind: list_set(expression.arguments.template_arguments.arguments, x, ind)
                 for i in range(len(expression.arguments.call_arguments.arguments))]
                if expression.arguments.call_arguments is not None else []]
        elif isinstance(expression, grammar.Argument):
            groups = [[lambda x, ind: field_set(expression, "value", x)]]
        elif isinstance(expression, grammar.Sequence):
            groups = [[lambda x, ind: field_set(expression, "var_name", x),
                       lambda x, ind: field_set(expression, "start", x),
                       lambda x, ind: field_set(expression, "end", x)]]
        elif isinstance(expression, grammar.Generator):
            groups = [[lambda x, ind: field_set(expression, "var_name", x),
                       lambda x, ind: field_set(expression, "linked_name", x)]]
        elif isinstance(expression, grammar.Set):
            groups = [[lambda x, ind: field_set(expression, "var_name", x),
                       lambda x, ind: field_set(expression, "value", x)]]
        elif isinstance(expression, grammar.Expression) or isinstance(expression, grammar.Equation):
            groups = [[lambda x, ind: field_set(expression, "loperand", x),
                       lambda x, ind: field_set(expression, "roperand", x)]]
        elif isinstance(expression, grammar.SumExpression):
            groups = [[lambda x, ind: list_set(expression.operands, x, ind)
                      for i in range(len(expression.operands))]]
        elif isinstance(expression, grammar.Operand):
            groups = [[lambda x, ind: field_set(expression, "text", x)]]
        elif isinstance(expression, grammar.Variable):
            l = [lambda x, ind: list_set(expression.indexvars, x, ind) for i in range(len(expression.indexvars))]
            t = expression
            while t.subvar is not None:
                t = t.subvar
                l += (lambda li, start_idx:
                      [lambda x, ind: list_set(li, x, ind - start_idx) for i in range(len(li))])(t.indexvars, len(l))
            groups = [l]
        else:
            groups = []
        n = 0
        for l in groups:
            for i in range(len(l)):
                if childrenvalues[i + n].replace:
                    l[i](childrenvalues[i + n].value, i)
            n += len(l)

        return postprocess_current(expression, prevalue, childrenvalues)
