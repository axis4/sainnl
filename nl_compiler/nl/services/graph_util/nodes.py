# coding=UTF-8
"""
   Copyright 2014 IoS (progsdi@gmail.com)

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""
from nl.services import grammar
from nl.services.graph_util import model, util
from nl.services.graph_util.expression_tree import ExpressionTreeTraversal
from nl.services.graph_util.util import has_template_arguments, get_expression_vars, fix_name

__author__ = 'sergeionov'


class NodeBuilder(object):
    def __init__(self, NETWORK_MIN, NETWORK_MAX, graph_service):
        self.NETWORK_MIN = NETWORK_MIN
        self.NETWORK_MAX = NETWORK_MAX
        self.util = util
        self.graph_service = graph_service

    def substitute_infinity(self, definition):
        def rename(item, use_less, children_values):
            if isinstance(item, grammar.Variable):
                if item.name == grammar.INFINITY and not len(item.indexvars) and item.subvar is None:
                    return self.NETWORK_MAX, True
            if isinstance(item, grammar.VariableOperand):
                if children_values[0].replace and not isinstance(children_values[0].value, grammar.Variable):
                    return grammar.ConstantOperand(children_values[0].value, item.line, item.lexpos), True
            return None, False

        ExpressionTreeTraversal.pass_tree(definition, postprocess_current=rename)

    @classmethod
    def replace_names(cls, definition, template_arguments):
        for a in template_arguments:
            cls.replace_name(definition, a.var_name, a.linked_name)
        return definition

    @classmethod
    def replace_name(cls, definition, from_name, to_name, mark_renamed=False):
        def replacer(item, use_less, children_values):
            if isinstance(item, grammar.Variable) and item.eval_constant is None:
                if item.match(from_name):
                    if not isinstance(to_name, grammar.Variable) and not len(item.indexvars):
                        item = item.clone()
                        item.eval_constant = to_name
                        return item, True
                    else:
                        return grammar.Variable.replace(item, from_name, to_name, mark_renamed), True
            if isinstance(item, grammar.VariableOperand):
                if children_values[0].replace and (not isinstance(children_values[0].value, grammar.Variable) or
                                                   children_values[0].value.eval_constant is not None):
                    return grammar.ConstantOperand(children_values[0].value, item.line, item.lexpos), True
            #if isinstance(item, grammar.Expression):
            #    left_c = None
            #    if isinstance(children_values[0].value, grammar.ConstantOperand):
            #        left_c = children_values[0].value
            #    elif isinstance(item.loperand, grammar.ConstantOperand):
            #        left_c = item.loperand
            #    right_c = None
            #    if isinstance(children_values[1].value, grammar.ConstantOperand):
            #        right_c = children_values[1].value
            #    elif isinstance(item.roperand, grammar.ConstantOperand):
            #        right_c = item.roperand
            #    if left_c is not None and right_c is not None:
            #        from nl.services.graph_util.expression import ExpressionBuilder
            #        value_c = ExpressionBuilder.primitive_expression(
            #            left_c.text, right_c.text, item)
            #        return grammar.ConstantOperand(value_c[0], item.line, item.lexpos), True
            #if isinstance(item, grammar.SumExpression):
            #    if all(map(
            #            lambda x: NodeBuilder.is_operand_number(x[1].value)
            #            if x[1].replace
            #            else NodeBuilder.is_operand_number(item.operands[x[0]]), enumerate(children_values))):
            #        value_c = reduce(lambda x, y: x + ((y[1].value.text if y[1].replace else item.operands[y[0]].text)
            #                                           if isinstance(y[1], TreeResult) else y[1]),
            #                         enumerate(children_values), 0)
            #        return grammar.ConstantOperand(value_c, item.line, item.lexpos), True
            return None, False

        ExpressionTreeTraversal.pass_tree(definition, postprocess_current=replacer)

    @classmethod
    def has_template_name(cls, name, t_name):
        def pre_step(item):
            return isinstance(item, grammar.Variable) and item.eval_constant is None and \
                unicode(item) == unicode(t_name)

        def has_step(item, prevalue, children_values):
            return any([cv.value for cv in children_values]) or prevalue, False

        return ExpressionTreeTraversal.pass_tree(name, preprocess_current=pre_step, postprocess_current=has_step)

    @classmethod
    def extend_name(cls, main_name, sub_name, deep=False):
        if not isinstance(main_name, grammar.Variable) or main_name.eval_constant is not None:
            raise Exception("Unexpected parameter state")
        if sub_name is None:
            return main_name
        if not isinstance(sub_name, grammar.Variable) or sub_name.eval_constant is not None:
            raise Exception("Unexpected parameter state")
        new_name = sub_name.clone()
        if deep:
            new_name.indexvars = [cls.extend_name(main_name, iv, deep) if isinstance(iv, grammar.Variable) and
                                  iv.eval_constant is None else (iv.clone() if hasattr(iv, "clone") else iv)
                                  for iv in sub_name.indexvars]
            nn = new_name
            while nn.subvar is not None:
                nn = nn.subvar
                nn.indexvars = [cls.extend_name(main_name, iv, deep) if isinstance(iv, grammar.Variable) and
                                iv.eval_constant is None else (iv.clone() if hasattr(iv, "clone") else iv)
                                for iv in nn.indexvars]
        if new_name.glob or sub_name.renamed_mark:
            return new_name
        res = main_name.clone()
        res.negative = False
        sv = res
        while sv.subvar is not None:
            sv = sv.subvar
        sv.merge(grammar.Variable(None, new_name.line, new_name.lexpos, subvar=new_name))
        return res

    def build_input_neurons(self, ins, missed, renamer, graph):
        for i in ins:
            if i.arguments.call_arguments is not None:
                for a in i.arguments.call_arguments.arguments:
                    in_name = a.value.text
                    if isinstance(i, grammar.TemplateNamedDefinition) or isinstance(i, grammar.TemplateDefinition):
                        for tv in i.template_vars.arguments:
                            missed.append((renamer.get(unicode(tv.value.text), tv.value.text), in_name,
                                           0, 0, 1, False))
                        n = model.Node(0, in_name, self.NETWORK_MIN, 0, model.ADDITIVE, template=True, is_input=True)
                    else:
                        n = model.Node(0, in_name, self.NETWORK_MIN, 0, model.ADDITIVE, is_input=True)
                    graph.set_node(in_name, n)

    def build_neuron(self, definition, name, missed, renamer, graph, grammar_unit, graph_unit):
        g = model.Graph()

        def activation_set(link, weight):
            nl = list(link)
            nl[2] = weight
            return tuple(nl)

        def stream_set(link, weight):
            nl = list(link)
            nl[3] = weight
            return tuple(nl)

        if definition.arguments.call_arguments is not None:
            # case "additive var, ..."
            if isinstance(definition.arguments.call_arguments.arguments[0].value, grammar.Operand) and\
                    not NodeBuilder.is_operand_constant(definition.arguments.call_arguments.arguments[0].value):
                failed_vars = get_expression_vars(definition.arguments.call_arguments.arguments[0].value)
                raise model.DelayError('Definition "' + grammar_unit + '" must have constant expression as '
                                       'right operand in condition of first argument. See ' + repr(definition),
                                       failed_vars[0])
            # case "additive ... >= var, ..."
            else:
                if not NodeBuilder.is_operand_constant(definition.arguments.call_arguments.arguments[0].value.roperand):
                    failed_vars = get_expression_vars(definition.arguments.call_arguments.arguments[0].value.roperand)
                    raise model.DelayError('Definition "' + grammar_unit + '" must have constant expression as '
                                           'right operand in condition of first argument. See ' + repr(definition),
                                           failed_vars[0])

            # case "additive constant, ..."
            if NodeBuilder.is_operand_number(definition.arguments.call_arguments.arguments[0].value):
                # make it TRUE: 0>=0
                if float(definition.arguments.call_arguments.arguments[0].value.text) > 0:
                    definition.arguments.call_arguments.arguments[0] = grammar.Argument(
                        u"condition",
                        grammar.Equation(u'>=',
                                         grammar.ConstantOperand(0.0, definition.line, definition.lexpos),
                                         grammar.ConstantOperand(0.0, definition.line, definition.lexpos),
                                         definition.line, definition.lexpos),
                        definition.line, definition.lexpos)
                # make it FALSE: 0>=1
                else:
                    definition.arguments.call_arguments.arguments[0] = grammar.Argument(
                        u"condition",
                        grammar.Equation(u'>=',
                                         grammar.ConstantOperand(0.0, definition.line, definition.lexpos),
                                         grammar.ConstantOperand(1.0, definition.line, definition.lexpos),
                                         definition.line, definition.lexpos),
                        definition.line, definition.lexpos)

            template = isinstance(definition, grammar.TemplateNamedDefinition)\
                or isinstance(definition, grammar.TemplateDefinition)

            if definition.arguments.call_arguments.arguments[0].value.action != u'>=':
                #wrong conditional transformation
                action = definition.arguments.call_arguments.arguments[0].value.action
                threshold = definition.arguments.call_arguments.arguments[0].value.roperand.text
                active_expression = definition.arguments.call_arguments.arguments[0].value.loperand
                stream_expression = definition.arguments.call_arguments.arguments[1].value

                if action == u'<':
                    tn1_name = NodeBuilder.extend_name(
                        name, grammar.Variable(u'#condition_helper1', definition.line, definition.lexpos))
                    g.set_node(tn1_name, model.Node(0, tn1_name, threshold, 1, model.ADDITIVE, template=template))
                    self.link_additive(g.nodes[unicode(tn1_name)], active_expression, g, graph, renamer, missed,
                                       graph.get_new_id, activation_set, definition.template_vars.arguments
                                       if template else None)
                    if template:
                        for tv in definition.template_vars.arguments:
                            missed.append((renamer.get(unicode(tv.value.text), tv.value.text), tn1_name, 0, 0, 1,
                                           False))

                    tn2_name = NodeBuilder.extend_name(
                        name, grammar.Variable(u'#condition_helper2', definition.line, definition.lexpos))
                    g.set_node(tn2_name, model.Node(0, tn2_name, 0,
                                                    stream_expression.text
                                                    if isinstance(stream_expression, grammar.ConstantOperand) else 0,
                                                    model.ADDITIVE, template=template))
                    self.link_additive(g.nodes[unicode(tn2_name)], stream_expression, g, graph, renamer, missed,
                                       graph.get_new_id, stream_set, definition.template_vars.arguments
                                       if template else None)
                    if template:
                        for tv in definition.template_vars.arguments:
                            missed.append((renamer.get(unicode(tv.value.text), tv.value.text), tn2_name, 0, 0, 1,
                                           False))

                    definition.arguments.call_arguments.arguments[0].value.roperand.text = 0.0
                    definition.arguments.call_arguments.arguments[0].value.loperand = grammar.Expression(
                        u'*',
                        grammar.ConstantOperand(-1.0, definition.line, definition.lexpos),
                        grammar.VariableOperand(tn1_name, definition.line, definition.lexpos),
                        definition.line, definition.lexpos)
                    definition.arguments.call_arguments.arguments[1].value = grammar.Expression(
                        u'*',
                        grammar.ConstantOperand(1.0, definition.line, definition.lexpos),
                        grammar.VariableOperand(tn2_name, definition.line, definition.lexpos),
                        definition.line, definition.lexpos)

                if action == u'<=':
                    tn1_name = NodeBuilder.extend_name(
                        name, grammar.Variable(u'#condition_helper1', definition.line, definition.lexpos))
                    g.set_node(tn1_name, model.Node(0, tn1_name, 0, 0, model.ADDITIVE, template=template))
                    self.link_additive(g.nodes[unicode(tn1_name)], active_expression, g, graph, renamer, missed,
                                       graph.get_new_id, stream_set, definition.template_vars.arguments
                                       if template else None)
                    if template:
                        for tv in definition.template_vars.arguments:
                            missed.append((renamer.get(unicode(tv.value.text), tv.value.text), tn1_name, 0, 0, 1,
                                           False))

                    tn2_name = NodeBuilder.extend_name(
                        name, grammar.Variable(u'#condition_helper2', definition.line, definition.lexpos))
                    g.set_node(tn2_name, model.Node(0, tn2_name, 0,
                                                    stream_expression.text
                                                    if isinstance(stream_expression, grammar.ConstantOperand) else 0,
                                                    model.ADDITIVE, template=template))
                    self.link_additive(g.nodes[unicode(tn2_name)], stream_expression, g, graph, renamer, missed,
                                       graph.get_new_id, stream_set, definition.template_vars.arguments
                                       if template else None)
                    if template:
                        for tv in definition.template_vars.arguments:
                            missed.append((renamer.get(unicode(tv.value.text), tv.value.text), tn2_name, 0, 0, 1,
                                           False))

                    definition.arguments.call_arguments.arguments[0].value.roperand.text = -1.0 * threshold
                    definition.arguments.call_arguments.arguments[0].value.loperand = grammar.Expression(
                        u'*',
                        grammar.ConstantOperand(-1.0, definition.line, definition.lexpos),
                        grammar.VariableOperand(tn1_name, definition.line, definition.lexpos),
                        definition.line, definition.lexpos)
                    definition.arguments.call_arguments.arguments[1].value = grammar.Expression(
                        u'*',
                        grammar.ConstantOperand(1.0, definition.line, definition.lexpos),
                        grammar.VariableOperand(tn2_name, definition.line, definition.lexpos),
                        definition.line, definition.lexpos)

                if action == u'>':
                    tn1_name = NodeBuilder.extend_name(
                        name, grammar.Variable(u'#condition_helper1', definition.line, definition.lexpos))
                    g.set_node(tn1_name, model.Node(0, tn1_name, 0, 0, model.ADDITIVE, template=template))
                    self.link_additive(g.nodes[unicode(tn1_name)], active_expression, g, graph, renamer, missed,
                                       graph.get_new_id, stream_set, definition.template_vars.arguments
                                       if template else None)
                    if template:
                        for tv in definition.template_vars.arguments:
                            missed.append((renamer.get(unicode(tv.value.text), tv.value.text), tn1_name, 0, 0, 1,
                                           False))

                    tn2_name = NodeBuilder.extend_name(
                        name, grammar.Variable(u'#condition_helper2', definition.line, definition.lexpos))
                    g.set_node(tn2_name, model.Node(0, tn2_name, 0,
                                                    stream_expression.text
                                                    if isinstance(stream_expression, grammar.ConstantOperand) else 0,
                                                    model.ADDITIVE, template=template))
                    self.link_additive(g.nodes[unicode(tn2_name)], stream_expression, g, graph, renamer, missed,
                                       graph.get_new_id, stream_set, definition.template_vars.arguments
                                       if template else None)
                    if template:
                        for tv in definition.template_vars.arguments:
                            missed.append((renamer.get(unicode(tv.value.text), tv.value.text), tn2_name, 0, 0, 1,
                                           False))

                    tn3_name = NodeBuilder.extend_name(
                        name, grammar.Variable(u'#condition_helper3', definition.line, definition.lexpos))
                    g.set_node(tn3_name, model.Node(0, tn3_name, -1 * threshold, 1, model.ADDITIVE, template=template))
                    self.link_additive(g.nodes[unicode(tn3_name)],
                                       grammar.Expression(
                                           u'*',
                                           grammar.ConstantOperand(-1.0, definition.line, definition.lexpos),
                                           grammar.VariableOperand(tn1_name, definition.line, definition.lexpos),
                                           definition.line, definition.lexpos),
                                       g, graph, renamer, missed, graph.get_new_id, activation_set,
                                       definition.template_vars.arguments if template else None)
                    if template:
                        for tv in definition.template_vars.arguments:
                            missed.append((renamer.get(unicode(tv.value.text), tv.value.text), tn3_name, 0, 0, 1,
                                           False))

                    tn4_name = NodeBuilder.extend_name(
                        name, grammar.Variable(u'#condition_helper4', definition.line, definition.lexpos))
                    g.set_node(tn4_name, model.Node(0, tn4_name, 0, 0, model.ADDITIVE, template=template))
                    self.link_additive(g.nodes[unicode(tn4_name)],
                                       grammar.Expression(
                                           u'*',
                                           grammar.ConstantOperand(1.0, definition.line, definition.lexpos),
                                           grammar.VariableOperand(tn2_name, definition.line, definition.lexpos),
                                           definition.line, definition.lexpos), g,
                                       graph, renamer, missed, graph.get_new_id, stream_set,
                                       definition.template_vars.arguments if template else None)
                    if template:
                        for tv in definition.template_vars.arguments:
                            missed.append((renamer.get(unicode(tv.value.text), tv.value.text), tn4_name, 0, 0, 1,
                                           False))

                    definition.arguments.call_arguments.arguments[0].value.roperand.text = 0.0
                    definition.arguments.call_arguments.arguments[0].value.loperand = grammar.Expression(
                        u'*',
                        grammar.ConstantOperand(-1.0, definition.line, definition.lexpos),
                        grammar.VariableOperand(tn3_name, definition.line, definition.lexpos),
                        definition.line, definition.lexpos)
                    definition.arguments.call_arguments.arguments[1].value = grammar.Expression(
                        u'*',
                        grammar.ConstantOperand(1.0, definition.line, definition.lexpos),
                        grammar.VariableOperand(tn4_name, definition.line, definition.lexpos),
                        definition.line, definition.lexpos)

                if action == u'==':
                    tn1_name = NodeBuilder.extend_name(
                        name, grammar.Variable(u'#condition_helper1', definition.line, definition.lexpos))
                    g.set_node(tn1_name, model.Node(0, tn1_name, 0, 0, model.ADDITIVE, template=template))
                    self.link_additive(g.nodes[unicode(tn1_name)], active_expression, g, graph, renamer, missed,
                                       graph.get_new_id, stream_set, definition.template_vars.arguments
                                       if template else None)
                    if template:
                        for tv in definition.template_vars.arguments:
                            missed.append((renamer.get(unicode(tv.value.text), tv.value.text), tn1_name, 0, 0, 1,
                                           False))

                    tn2_name = NodeBuilder.extend_name(
                        name, grammar.Variable(u'#condition_helper2', definition.line, definition.lexpos))
                    g.set_node(tn2_name, model.Node(0, tn2_name, 0,
                                                    stream_expression.text
                                                    if isinstance(stream_expression, grammar.ConstantOperand) else 0,
                                                    model.ADDITIVE, template=template))
                    self.link_additive(g.nodes[unicode(tn2_name)], stream_expression, g, graph, renamer, missed,
                                       graph.get_new_id, stream_set, definition.template_vars.arguments
                                       if template else None)
                    if template:
                        for tv in definition.template_vars.arguments:
                            missed.append((renamer.get(unicode(tv.value.text), tv.value.text), tn2_name, 0, 0, 1,
                                           False))

                    tn3_name = NodeBuilder.extend_name(
                        name, grammar.Variable(u'#condition_helper3', definition.line, definition.lexpos))
                    g.set_node(tn3_name, model.Node(0, tn3_name, -1 * threshold, 1, model.ADDITIVE, template=template))
                    self.link_additive(g.nodes[unicode(tn3_name)],
                                       grammar.Expression(
                                           u'*',
                                           grammar.ConstantOperand(-1.0, definition.line, definition.lexpos),
                                           grammar.VariableOperand(tn1_name, definition.line, definition.lexpos),
                                           definition.line, definition.lexpos),
                                       g, graph, renamer, missed, graph.get_new_id, activation_set,
                                       definition.template_vars.arguments if template else None)
                    if template:
                        for tv in definition.template_vars.arguments:
                            missed.append((renamer.get(unicode(tv.value.text), tv.value.text), tn3_name, 0, 0, 1,
                                           False))

                    tn4_name = NodeBuilder.extend_name(
                        name, grammar.Variable(u'#condition_helper4', definition.line, definition.lexpos))
                    g.set_node(tn4_name, model.Node(0, tn4_name, threshold, 1, model.ADDITIVE, template=template))
                    self.link_additive(g.nodes[unicode(tn4_name)],
                                       grammar.Expression(
                                           u'*',
                                           grammar.ConstantOperand(1.0, definition.line, definition.lexpos),
                                           grammar.VariableOperand(tn1_name, definition.line, definition.lexpos),
                                           definition.line, definition.lexpos), g,
                                       graph, renamer, missed, graph.get_new_id, activation_set,
                                       definition.template_vars.arguments if template else None)
                    if template:
                        for tv in definition.template_vars.arguments:
                            missed.append((renamer.get(unicode(tv.value.text), tv.value.text), tn4_name, 0, 0, 1,
                                           False))

                    tn5_name = NodeBuilder.extend_name(
                        name, grammar.Variable(u'#condition_helper5', definition.line, definition.lexpos))
                    g.set_node(tn5_name, model.Node(0, tn5_name, 0, 0, model.ADDITIVE, template=template))
                    self.link_additive(g.nodes[unicode(tn5_name)],
                                       grammar.Expression(
                                           u'*',
                                           grammar.ConstantOperand(1.0, definition.line, definition.lexpos),
                                           grammar.VariableOperand(tn2_name, definition.line, definition.lexpos),
                                           definition.line, definition.lexpos), g,
                                       graph, renamer, missed, graph.get_new_id, stream_set,
                                       definition.template_vars.arguments if template else None)
                    if template:
                        for tv in definition.template_vars.arguments:
                            missed.append((renamer.get(unicode(tv.value.text), tv.value.text), tn5_name, 0, 0, 1,
                                           False))

                    definition.arguments.call_arguments.arguments[0].value.roperand.text = 2.0
                    definition.arguments.call_arguments.arguments[0].value.loperand = grammar.Expression(
                        u'+',
                        grammar.Expression(
                            u'*',
                            grammar.ConstantOperand(1.0, definition.line, definition.lexpos),
                            grammar.VariableOperand(tn3_name, definition.line, definition.lexpos),
                            definition.line, definition.lexpos),
                        grammar.Expression(
                            u'*',
                            grammar.ConstantOperand(1.0, definition.line, definition.lexpos),
                            grammar.VariableOperand(tn4_name, definition.line, definition.lexpos),
                            definition.line, definition.lexpos),
                        definition.line, definition.lexpos)
                    definition.arguments.call_arguments.arguments[1].value = grammar.Expression(
                        u'*',
                        grammar.ConstantOperand(1.0, definition.line, definition.lexpos),
                        grammar.VariableOperand(tn5_name, definition.line, definition.lexpos),
                        definition.line, definition.lexpos)

                if action == u'!=':
                    tn1_name = NodeBuilder.extend_name(
                        name, grammar.Variable(u'#condition_helper1', definition.line, definition.lexpos))
                    g.set_node(tn1_name, model.Node(0, tn1_name, 0, 0, model.ADDITIVE, template=template))
                    self.link_additive(g.nodes[unicode(tn1_name)], active_expression, g, graph, renamer, missed,
                                       graph.get_new_id, stream_set, definition.template_vars.arguments
                                       if template else None)
                    if template:
                        for tv in definition.template_vars.arguments:
                            missed.append((renamer.get(unicode(tv.value.text), tv.value.text), tn1_name, 0, 0, 1,
                                           False))

                    tn2_name = NodeBuilder.extend_name(
                        name, grammar.Variable(u'#condition_helper2', definition.line, definition.lexpos))
                    g.set_node(tn2_name, model.Node(0, tn2_name, 0,
                                                    stream_expression.text
                                                    if isinstance(stream_expression, grammar.ConstantOperand) else 0,
                                                    model.ADDITIVE, template=template))
                    self.link_additive(g.nodes[unicode(tn2_name)], stream_expression, g, graph, renamer, missed,
                                       graph.get_new_id, stream_set, definition.template_vars.arguments
                                       if template else None)
                    if template:
                        for tv in definition.template_vars.arguments:
                            missed.append((renamer.get(unicode(tv.value.text), tv.value.text), tn2_name, 0, 0, 1,
                                           False))

                    tn3_name = NodeBuilder.extend_name(
                        name, grammar.Variable(u'#condition_helper3', definition.line, definition.lexpos))
                    g.set_node(tn3_name, model.Node(0, tn3_name, -1 * threshold, 1, model.ADDITIVE, template=template))
                    self.link_additive(g.nodes[unicode(tn3_name)],
                                       grammar.Expression(
                                           u'*',
                                           grammar.ConstantOperand(-1., definition.line, definition.lexpos),
                                           grammar.VariableOperand(tn1_name, definition.line, definition.lexpos),
                                           definition.line, definition.lexpos),
                                       g, graph, renamer, missed, graph.get_new_id, activation_set,
                                       definition.template_vars.arguments if template else None)
                    if template:
                        for tv in definition.template_vars.arguments:
                            missed.append((renamer.get(unicode(tv.value.text), tv.value.text), tn3_name, 0, 0, 1,
                                           False))

                    tn4_name = NodeBuilder.extend_name(
                        name, grammar.Variable(u'#condition_helper4', definition.line, definition.lexpos))
                    g.set_node(tn4_name, model.Node(0, tn4_name, threshold, 1, model.ADDITIVE, template=template))
                    self.link_additive(g.nodes[unicode(tn4_name)],
                                       grammar.Expression(
                                           u'*',
                                           grammar.ConstantOperand(1.0, definition.line, definition.lexpos),
                                           grammar.VariableOperand(tn1_name, definition.line, definition.lexpos),
                                           definition.line, definition.lexpos), g,
                                       graph, renamer, missed, graph.get_new_id, activation_set,
                                       definition.template_vars.arguments if template else None)
                    if template:
                        for tv in definition.template_vars.arguments:
                            missed.append((renamer.get(unicode(tv.value.text), tv.value.text), tn4_name, 0, 0, 1,
                                           False))

                    tn5_name = NodeBuilder.extend_name(
                        name, grammar.Variable(u'#condition_helper5', definition.line, definition.lexpos))
                    g.set_node(tn5_name, model.Node(0, tn5_name, 0, 0, model.ADDITIVE, template=template))
                    self.link_additive(g.nodes[unicode(tn5_name)],
                                       grammar.Expression(
                                           u'*',
                                           grammar.ConstantOperand(1.0, definition.line, definition.lexpos),
                                           grammar.VariableOperand(tn2_name, definition.line, definition.lexpos),
                                           definition.line, definition.lexpos), g,
                                       graph, renamer, missed, graph.get_new_id, stream_set,
                                       definition.template_vars.arguments if template else None)
                    if template:
                        for tv in definition.template_vars.arguments:
                            missed.append((renamer.get(unicode(tv.value.text), tv.value.text), tn5_name, 0, 0, 1,
                                           False))

                    definition.arguments.call_arguments.arguments[0].value.roperand.text = -1.0
                    definition.arguments.call_arguments.arguments[0].value.loperand = grammar.Expression(
                        u'+',
                        grammar.Expression(
                            u'*',
                            grammar.ConstantOperand(-1.0, definition.line, definition.lexpos),
                            grammar.VariableOperand(tn3_name, definition.line, definition.lexpos),
                            definition.line, definition.lexpos),
                        grammar.Expression(
                            u'*',
                            grammar.ConstantOperand(-1.0, definition.line, definition.lexpos),
                            grammar.VariableOperand(tn4_name, definition.line, definition.lexpos),
                            definition.line, definition.lexpos),
                        definition.line, definition.lexpos)
                    definition.arguments.call_arguments.arguments[1].value = grammar.Expression(
                        u'*',
                        grammar.ConstantOperand(1.0, definition.line, definition.lexpos),
                        grammar.VariableOperand(tn5_name, definition.line, definition.lexpos),
                        definition.line, definition.lexpos)

            if isinstance(definition.arguments.call_arguments.arguments[1].value, grammar.SumExpression):
                consts = self.extract_constant(definition.arguments.call_arguments.arguments[1].value)
                if len(consts) == 2 and consts[0].text == 0.0:
                    c = consts[1].text
                else:
                    c = consts[0].text
                definition.arguments.call_arguments.arguments[1].value.operands = \
                    consts + definition.arguments.call_arguments.arguments[1].value.operands
                definition.arguments.call_arguments.arguments[1].value.heights = \
                    [0] * len(definition.arguments.call_arguments.arguments[1].value.operands)
            elif isinstance(definition.arguments.call_arguments.arguments[1].value, grammar.ConstantOperand):
                c = definition.arguments.call_arguments.arguments[1].value.text
            else:
                c = 0.0
            g.set_node(name, model.Node(0, name, definition.arguments.call_arguments.arguments[0].value.roperand.text,
                                        c, graph_unit, template=template,
                                        is_reducable=(definition.reduction if hasattr(definition, "reduction")
                                                      else False)))
            self.link_additive(g.nodes[unicode(name)], definition.arguments.call_arguments.arguments[0].value.loperand,
                               g, graph, renamer, missed, graph.get_new_id, activation_set,
                               definition.template_vars.arguments if template else None)
            self.link_additive(g.nodes[unicode(name)], definition.arguments.call_arguments.arguments[1].value,
                               g, graph, renamer, missed, graph.get_new_id, stream_set,
                               definition.template_vars.arguments if template else None)
            if template:
                for tv in definition.template_vars.arguments:
                    missed.append((renamer.get(unicode(tv.value.text), tv.value.text), name, 0, 0, 1, False))
        return g

    def build_unit_neuron(self, definition, name, missed, renamer, graph):
        return self.build_neuron(definition, name, missed, renamer, graph, grammar.UNIT, model.ADDITIVE)

    def build_lmod_neuron(self, definition, name, missed, renamer, graph):
        template = isinstance(definition, grammar.TemplateNamedDefinition)\
            or isinstance(definition, grammar.TemplateDefinition)

        g = model.Graph()
        if definition.arguments.call_arguments is None:
            return g

        g.set_node(name, model.Node(0, name, definition.arguments.call_arguments.arguments[2].value.text, 0,
                                    model.LINKMOD, template=template,
                                    is_reducable=(definition.reduction if hasattr(definition, "reduction") else False)))

        if template:
            for tv in definition.template_vars.arguments:
                missed.append((renamer.get(unicode(tv.value.text), tv.value.text), name, 0, 0, 1, False))

        delta = definition.arguments.call_arguments.arguments[0].value
        expr = definition.arguments.call_arguments.arguments[1].value

        if isinstance(delta, grammar.Expression):
            if isinstance(delta.loperand, grammar.ConstantOperand) and \
                    isinstance(delta.roperand, grammar.ConstantOperand):
                v = float(delta.loperand.text) * float(delta.roperand.text)
                cname = grammar.Variable(u'constant_source#' + fix_name(unicode(v)), expr.line, expr.lexpos)
                if unicode(cname) not in graph.nodes:
                    graph.nodes[unicode(cname)] = model.Node(0, cname, 0, v, model.ADDITIVE, template=template,
                                                             is_reducable=True)
                else:
                    graph.nodes[unicode(cname)].ntype = model.ADDITIVE
                delta = grammar.VariableOperand(cname, definition.line, definition.lexpos)
            else:
                def stream_set(link, weight):
                    nl = list(link)
                    nl[2] = weight
                    return tuple(nl)
                n = model.Node(
                    0, NodeBuilder.extend_name(name, grammar.Variable(u"#delta_helper", expr.line, expr.lexpos)),
                    0, 0, model.ADDITIVE, template=template)
                g.nodes[unicode(n.node_id)] = n
                self.link_additive(n, delta, g, graph, renamer, missed, graph.get_new_id, stream_set,
                                   definition.template_vars.arguments if template else None)
                delta = grammar.VariableOperand(n.node_id, definition.line, definition.lexpos)

        if isinstance(delta, grammar.SumExpression):
            def stream_set(link, weight):
                nl = list(link)
                nl[2] = weight
                return tuple(nl)
            self.link_additive(g.nodes[unicode(name)], delta, g, graph, renamer, missed, graph.get_new_id, stream_set,
                               definition.template_vars.arguments if template else None)
            delta = None

        if isinstance(expr, grammar.Operand):
            if isinstance(expr, grammar.ConstantOperand):
                g.nodes[name].kind = model.ADDITIVE
                g.nodes[name].threshold = 0.0
                g.nodes[name].constant = expr.text
            else:
                g.nodes[name].kind = model.ADDITIVE
                g.nodes[name].threshold = 0.0
                g.nodes[name].constant = 0.0
                missed.append((renamer.get(unicode(expr.text), expr.text), name, 0, 1, 0, False))
                if delta is not None:
                    missed.append((renamer.get(unicode(delta.text), delta.text), name, 1, 0, 0, False))
        else:
            if isinstance(expr, grammar.Expression):
                if isinstance(expr.loperand, grammar.ConstantOperand) and \
                        isinstance(expr.roperand, grammar.ConstantOperand):
                    v = float(expr.loperand.text) * float(expr.roperand.text)
                    cname = grammar.Variable(u'constant_source#' + fix_name(unicode(v)), expr.line, expr.lexpos)
                    if unicode(cname) not in graph.nodes:
                        graph.nodes[unicode(cname)] = model.Node(0, cname, 0, v, model.ADDITIVE, template=template,
                                                                 is_reducable=True)
                    else:
                        graph.nodes[unicode(cname)].ntype = model.ADDITIVE
                    expr = grammar.VariableOperand(cname, definition.line, definition.lexpos)
                else:
                    def stream_set(link, weight):
                        nl = list(link)
                        nl[3] = weight
                        return tuple(nl)
                    n = model.Node(
                        0, NodeBuilder.extend_name(name, grammar.Variable(u"#delta_helper", expr.line, expr.lexpos)),
                        0, 0, model.ADDITIVE, template=template)
                    g.nodes[unicode(n.node_id)] = n
                    self.link_additive(n, expr, g, graph, renamer, missed, graph.get_new_id, stream_set,
                                       definition.template_vars.arguments if template else None)
                    expr = grammar.VariableOperand(n.node_id, definition.line, definition.lexpos)

            if isinstance(expr, grammar.SumExpression):
                def stream_set(link, weight):
                    nl = list(link)
                    nl[3] = weight
                    return tuple(nl)
                self.link_additive(g.nodes[unicode(name)], expr, g, graph, renamer, missed, graph.get_new_id,
                                   stream_set, definition.template_vars.arguments if template else None)
                expr = None

            if expr is not None:
                missed.append((renamer.get(unicode(expr.text), expr.text), name, 0, 1, 0))
            if delta is not None:
                missed.append((renamer.get(unicode(delta.text), delta.text), name, 1, 0, 0))

        return g

    @staticmethod
    def apply_connections(connections, renamer, graph, relax=True):
        for m in connections:
            n = renamer.get(unicode(m[0]), m[0])
            dn = renamer.get(unicode(m[1]), m[1])
            if unicode(n) not in graph.nodes and relax:
                graph.set_node(n, model.Node(0, n, 0, 0, model.EXPECTED))
                reqs = []
                for tn in [n, dn, m[2], m[3], m[4]]:
                    while tn is not None:
                        if isinstance(tn, grammar.Variable):
                            if len(tn.indexvars):
                                for tiv in tn.indexvars:
                                    if isinstance(tiv, grammar.Variable) and tiv.eval_constant is None:
                                        reqs.append(tiv)
                            tn = tn.subvar
                        else:
                            tn = None
                if len(reqs):
                    graph.nodes[unicode(n)].template = True
                    for r in reqs:
                        if not unicode(r) in graph.links:
                            graph.links[unicode(r)] = []
                        ok = True
                        for l in graph.links[unicode(r)]:
                            if unicode(l.node_to) == unicode(n):
                                ok = False
                        if ok:
                            graph.links[unicode(r)].append(model.Link(r, n, 0, 0, 1, True))
            if unicode(n) in graph.nodes:
                if unicode(n) not in graph.links:
                    graph.links[unicode(n)] = []
                fl = None
                for l in graph.links[unicode(n)]:
                    if unicode(l.node_to) == unicode(m[1]):
                        fl = l
                if fl is None:
                    graph.links[unicode(n)].append(model.Link(n, dn, m[2], m[3], m[4], m[5] if len(m) > 5 else True))
                else:
                    fl.additive_weight += m[2]
                    fl.stream_weight += m[3]
                    fl.generation_weight += m[4]
                    if fl.alignable and len(m) > 5:
                        fl.alignable = m[5]
            else:
                raise model.GraphError('Definition argument "' + repr(m[0]) + '" is not linkable')

    def build_internal_neurons(self, group, graph, missed, renamer, groups, group_params, global_constants, graphs):
        from nl.services.graph_util.permutations import Permutations

        relax = group_params[group.name].get("relax", True)
        expanded = []  # expanded[i] = build_subgraph(group.definitions[i])
        delay_sync = []
        """:type: list[tuple[grammar.Definition | model.Graph]] """
        #build other definitions
        for d in sorted(group.definitions, key=lambda x: 0 if x.group_name == grammar.LINKMOD else 1):
            if d.group_name != grammar.IN:
                if d.group_name != grammar.OUT:
                    #name definition result
                    if isinstance(d, grammar.NamedDefinition):
                        name = d.result_name
                    else:
                        name = grammar.Variable(u'noresult' + graph.get_new_id(), d.line, d.lexpos)
                    if d.group_name == grammar.UNIT:
                        g = self.build_unit_neuron(d, name, missed, renamer, graph)
                        expanded.append(g)
                        graph.update_nodes_and_links(g.nodes, g.links)
                    elif d.group_name == grammar.LINKMOD:
                        g = self.build_lmod_neuron(d, name, missed, renamer, graph)
                        expanded.append(g)
                        graph.update_nodes_and_links(g.nodes, g.links)
                    elif d.group_name in (grammar.STDIN, grammar.STDOUT, grammar.EXIT, grammar.TIME, grammar.TICK,
                                          grammar.LISTEN, grammar.SOCKET):
                        g = self.build_func(d, name, missed, renamer)
                        expanded.append(g)
                        graph.update_nodes_and_links(g.nodes, g.links)
                    else:
                        if d.group_name == grammar.STRING:
                            gr = None
                            #build subgraph of a string
                            s = d.arguments.call_arguments.arguments[0].value.text
                            g = self.build_string(s)
                        elif d.group_name == grammar.SYNC or d.group_name == grammar.ASYNC:
                            gr = None
                            g = self.prepare_sync(d)
                            delay_sync.append((d, g))
                        else:
                            gr = self.graph_service.find_group(d.group_name, groups, group_params, global_constants)

                            gr, renamed_template = Permutations.pass_permutations(d, gr, group_params, global_constants)

                            #expand positional arguments into keyed
                            if d.arguments.call_arguments is not None:
                                positional = [a for a in d.arguments.call_arguments.arguments if
                                              not isinstance(a, grammar.KeyArgument)]
                                keyed = dict(
                                    [(unicode(a.key), a) for a in d.arguments.call_arguments.arguments if
                                     isinstance(a, grammar.KeyArgument)])
                            else:
                                positional = []
                                keyed = {}
                                d.arguments.call_arguments = grammar.MultiArgument(d.line, d.lexpos, [])
                            if len(positional) > len(gr.arguments):
                                raise model.GraphError('Definition "' + d.group_name + '" accepts exactly ' +
                                                       len(gr.arguments).__str__() + ' argument(s) but provided ' +
                                                       len(positional).__str__() + ': ' +
                                                       ', '.join([repr(p) for p in positional]))
                            for i in range(len(positional)):
                                if unicode(gr.arguments[i].value.text) in keyed:
                                    raise model.GraphError('Definition "' + d.group_name + '" already has '
                                                           'keyed definition for positional argument ' +
                                                           repr(gr.arguments[i]))
                                d.arguments.call_arguments.arguments[i] = \
                                    grammar.KeyArgument(gr.arguments[i].value.text,
                                                        d.arguments.call_arguments.arguments[i], d.line, d.lexpos)
                                keyed[unicode(d.arguments.call_arguments.arguments[i].key)] =\
                                    d.arguments.call_arguments.arguments[i]

                            # expand missing input keys as scope expected
                            for gra in gr.arguments:
                                if unicode(gra.value.text) not in keyed:
                                    d.arguments.call_arguments.arguments.append(
                                        grammar.KeyArgument(gra.value.text, grammar.Argument(
                                            u'expression',
                                            grammar.VariableOperand(gra.value.text, d.line, d.lexpos),
                                            d.line, d.lexpos), d.line, d.lexpos))
                                    keyed[unicode(gra.value.text)] = d.arguments.call_arguments.arguments[-1]
                            for ind in group_params[gr.name]["ins"]:
                                for ia in ind.arguments.call_arguments.arguments:
                                    if unicode(ia.value.text) not in keyed:
                                        d.arguments.call_arguments.arguments.append(
                                            grammar.KeyArgument(ia.value.text, grammar.Argument(
                                                u'expression',
                                                grammar.VariableOperand(ia.value.text, d.line, d.lexpos),
                                                d.line, d.lexpos), d.line, d.lexpos))
                                        keyed[unicode(ia.value.text)] = d.arguments.call_arguments.arguments[-1]

                            #build subgraph
                            g = self.graph_service.form_group(gr, groups, group_params, global_constants, graphs)

                        expanded.append(g)

                        red_name = name
                        if hasattr(d, "reduction") and d.reduction:
                            red_name = NodeBuilder.extend_name(red_name,
                                                               grammar.Variable("#reduction" + graph.get_new_id(),
                                                                                d.line, d.lexpos))

                        #include subgraph into graph
                        g = expanded[-1].clone(red_name)

                        #apply renamer if only one output exists
                        if len(g.outputs()) == 1:
                            renamer[unicode(red_name)] = g.outputs()[0].node_id

                        inputs = {}
                        outputs = {}
                        graph.update_nodes_and_links(g.nodes, g.links)
                        for nk in g.nodes:
                            n = g.nodes[nk]
                            n.layer = 0
                            nn = graph.nodes[unicode(n.node_id)]
                            if n.input:
                                inputs[unicode(n.node_id)] = nn
                                n.input = False
                            if n.output:
                                outputs[unicode(n.node_id)] = nn
                                n.output = False

                        #apply connections to subgraph
                        if d.group_name != grammar.STRING:
                            for a in d.arguments.call_arguments.arguments:
                                if unicode(NodeBuilder.extend_name(red_name, a.key, True)) not in inputs:
                                    raise model.GraphError(
                                        'Definition "' + d.group_name + '" does not accept key ' + repr(a.key) +
                                        '. Available keys: ' + ','.join(inputs.iterkeys()))
                                if isinstance(a.value, grammar.VariableOperand):
                                    if unicode(NodeBuilder.extend_name(red_name, a.key)) != unicode(a.value.text):
                                        missed.append((a.value.text, NodeBuilder.extend_name(red_name, a.key), 1.0, 1.0, 0))
                                else:
                                    cname = grammar.Variable(u'value' + graph.get_new_id(), a.line, a.lexpos)
                                    graph.nodes[unicode(cname)] = model.Node(0, cname, 0, float(a.value.text))
                                    missed.append((cname, NodeBuilder.extend_name(red_name, a.key), 1.0, 1.0, 0))
                        #apply template arguments
                        if has_template_arguments(d):
                            for a in d.arguments.template_arguments.arguments:
                                if gr is not None:
                                    nas = [t for t in gr.templates
                                           if renamed_template[unicode(a.key)] in self.get_template_dependencies([t])]
                                else:
                                    nas = []
                                for n in nas:
                                    if isinstance(n, grammar.Generator):
                                        if unicode(NodeBuilder.extend_name(
                                                red_name, renamed_template[unicode(a.key)])) not in inputs:
                                            raise model.GraphError('Definition "' + d.group_name + '" does not accept '
                                                                   'key ' + repr(a.key) + '. Available keys: ' +
                                                                   ','.join(inputs.iterkeys()))
                                        if isinstance(a.value, grammar.VariableOperand):
                                            missed.append((a.value.text,
                                                           NodeBuilder.extend_name(red_name,
                                                                                   '.' + renamed_template[a.key]),
                                                           1.0, 1.0, 0))
                                        else:
                                            cname = grammar.Variable(u'value' + graph.get_new_id(), a.line, a.lexpos)
                                            graph.nodes[cname] = model.Node(0, cname, 0, float(a.value.text))
                                            missed.append((cname, NodeBuilder.extend_name(
                                                red_name, renamed_template[unicode(a.key)]), 1.0, 1.0, 0))
                        #match upcoming template names
                        if isinstance(d, grammar.TemplateNamedDefinition) or isinstance(d, grammar.TemplateDefinition):
                            names = [unicode(a.value.text) for a in d.template_vars.arguments]
                            for nk in g.nodes:
                                n = g.nodes[nk]
                                nn = graph.nodes[unicode(n.node_id)]
                                if nn.node_id.subvar is not None:
                                    for p in nn.node_id.subvar.indexvars:
                                        if unicode(p) in graph.nodes and unicode(p) in names:
                                            missed.append((p, nn.node_id, 0, 0, 1.0))

        #reconnect missed links
        self.apply_connections(missed, renamer, graph, relax)

        #count synchronization through graph
        sync_graphs = []
        for def_sync, graph_sync in delay_sync:
            if hasattr(def_sync, "result_name"):
                if def_sync.group_name == grammar.ASYNC:
                    for ns in graph_sync.nodes.values():  # all async internal nodes
                        cn = NodeBuilder.extend_name(def_sync.result_name, ns.node_id)  # expanded name
                        for n in graph.nodes.values():  # all current nodes
                            if unicode(n.node_id) == unicode(ns.node_id):  # found asynced name
                                for l in graph.links[unicode(cn)]:  # check all outgoing links from expanded
                                    nf = True
                                    nl = []
                                    for ol in graph.links[unicode(n.node_id)]:
                                        if unicode(ol.node_to) == unicode(l.node_to):
                                            nf = False
                                        if unicode(ol.node_to) != unicode(cn):
                                            nl.append(ol)
                                    graph.links[unicode(n.node_id)] = nl
                                    if nf:
                                        l.node_from = n  # connect from original node
                                        graph.links[unicode(n.node_id)].append(l)
                                del graph.nodes[unicode(cn)]  # delete expanded node totally
                                del graph.links[unicode(cn)]
                else:
                    nodes = []
                    for ns in graph_sync.nodes.values():
                        cn = NodeBuilder.extend_name(def_sync.result_name, ns.node_id)
                        if ns.output:
                            nodes.append(graph.nodes[unicode(cn)])
                    paths = self.graph_service.find_simplest_paths_to_inputs(graph, nodes)
                    crosses = []
                    for p in paths:
                        for k in p.keys():
                            if not len(p[k]):
                                del p[k]
                            else:
                                for n in p[k]:
                                    for ns in graph_sync.nodes.values():  # all async internal nodes
                                        cn = NodeBuilder.extend_name(def_sync.result_name, ns.node_id)  # expanded name
                                        if unicode(n.node_id) == unicode(cn):
                                            crosses.append(cn)
                    sync_graphs.append({"definition": def_sync, "subgraph": graph_sync,
                                        "input_paths": paths, "crosses": crosses})
        for sg in sorted(sync_graphs, key=lambda x: len(x["crosses"])):
            base_level = max(*[len(ii) for i in sg["input_paths"] for ii in i.values()])
            for ip in sg["input_paths"]:
                k = max(ip.keys(), key=lambda x: len(ip[x]))
                delta = base_level - len(ip[k])
                target = ip[k][0]
                for d in range(delta):
                    new_node_name = NodeBuilder.extend_name(ip[k][0].node_id,
                                                            grammar.Variable(u"#delay" + unicode(d), 0, 0))
                    new_node = model.Node(0, new_node_name, 0.0, 0.0)
                    graph.nodes[unicode(new_node_name)] = new_node
                    for ls in graph.links.itervalues():
                        for l in ls:
                            if unicode(l.node_to) == unicode(target.node_id):
                                l.node_to = new_node_name
                    graph.links[unicode(new_node_name)] = [model.Link(new_node_name, target.node_id, 0.0, 1.0, 0.0)]
                    target = new_node
            #TODO: fix other crossed nodes

    def build_output_neurons(self, outs, renamer, graph, relax=True):
        for d in outs:
            names = []
            rename = {}
            if d.arguments.call_arguments is not None:
                for a in d.arguments.call_arguments.arguments:
                    nn = self.util.get_out_name(a)
                    if isinstance(a, grammar.RenamedArgument):
                        for n in nn:
                            rename[unicode(n)] = a.name
                    names += nn
                for o in names:
                    name = renamer.get(unicode(o), o)
                    if unicode(name) not in graph.nodes:
                        if relax:
                            graph.set_node(name, model.Node(0, name, 0, 0, model.EXPECTED))
                        else:
                            raise model.GraphError('Definition "' + grammar.OUT +
                                                   '" cannot link with argument ' + repr(o))
                    graph.nodes[unicode(name)].output = True
                    new_name = rename.get(unicode(o), None)
                    if new_name:
                        if unicode(new_name) in graph.nodes:
                            raise model.GraphError(
                                'Argument renaming "' + repr(name) + "'->'" +
                                repr(new_name) + '" conflicts another name in group')
                        graph.nodes[unicode(new_name)] = graph.nodes[unicode(name)]
                        graph.nodes[unicode(new_name)].node_id = new_name
                        del graph.nodes[unicode(name)]
                        if unicode(name) in graph.links:
                            graph.links[unicode(new_name)] = graph.links[unicode(name)]
                            del graph.links[unicode(name)]
                        for ls in graph.links.itervalues():
                            for l in ls:
                                if unicode(l.node_to) == unicode(name):
                                    l.node_to = new_name

    @staticmethod
    def prepare_sync(definition):
        """
        :type definition: grammar.Definition
        :rtype: model.Graph
        """
        res = model.Graph()
        na = []
        for a in definition.arguments.call_arguments.arguments:
            if not isinstance(a.value, grammar.VariableOperand):
                raise model.GraphError(u"Complex argument to " + definition.group_name + ": " + repr(a.value))
            out_v = a.value.text.clone()
            res.nodes[unicode(out_v)] = model.Node(0, out_v, 0, 0, model.ADDITIVE, False, is_input=True, is_output=True)

            na.append(grammar.KeyArgument(out_v, a, a.line, a.lexpos))

        definition.arguments.call_arguments = grammar.MultiArgument(definition.line, definition.lexpos, na)
        return res

    def build_func(self, definition, name, missed, renamer):
        """
        :type definition: grammar.Definition
        :rtype: model.Graph
        """
        template = isinstance(definition, grammar.TemplateNamedDefinition)\
            or isinstance(definition, grammar.TemplateDefinition)

        g = model.Graph()
        g.set_node(name, model.Node(0, name, 0, 0,
                                    definition.group_name, template=template,
                                    is_reducable=(definition.reduction if hasattr(definition, "reduction") else False)))

        if template:
            for tv in definition.template_vars.arguments:
                missed.append((renamer.get(unicode(tv.value.text), tv.value.text), name, 0, 0, 1, False))

        if definition.arguments.call_arguments is None:
            return g

        for i, a in enumerate(definition.arguments.call_arguments.arguments):
            if not isinstance(a.value, grammar.VariableOperand):
                raise model.GraphError(u"Complex argument to " + definition.group_name + ": " + repr(a.value))
            f = a.value.text.clone()
            g.links[unicode(f)] = [model.Link(f, name, 1 if i == 0 else 0, 1 if i == 1 else 0, 1 if i == 2 else 0)]

        return g

    def build_string(self, s):
        """
        :type s: str | unicode
        :rtype: model.Graph
        """
        if s is None:
            s = ""
        res = model.Graph()
        res.nodes[u"GET"] = model.Node(0, grammar.Variable(u"GET", 0, 0), 0.0, 0.0, model.ADDITIVE, False,
                                       is_input=True, is_output=False, is_reducable=True)
        res.nodes[u"LEN"] = model.Node(0, grammar.Variable(u"LEN", 0, 0), 0, len(s), model.ADDITIVE, False,
                                       is_input=False, is_output=True)
        res.nodes[u"STREAM"] = model.Node(0, grammar.Variable(u"STREAM", 0, 0), 0, 0, model.ADDITIVE, False,
                                          is_input=False, is_output=True)
        for i in range(len(s)):
            c_name = u"C[" + str(i + 1) + "]"
            c_var = grammar.Variable(u"C", 0, 0, [i + 1])
            o_name = u"O[" + str(i + 1) + "]"
            o_var = grammar.Variable(u"O", 0, 0, [i + 1])
            res.nodes[c_name] = model.Node(0, c_var, 0, ord(s[i]), model.ADDITIVE, False,
                                           is_input=False, is_output=True)
            res.nodes[o_name] = model.Node(0, o_var, 1, 0, model.ADDITIVE, False, is_input=False, is_output=False)
            res.links[c_name] = [model.Link(c_var, o_var, 0, 1, 0)]
            res.links[o_name] = [model.Link(o_var, res.nodes[u"STREAM"].node_id, 0, 1, 0)]
            if i < len(s) - 1:
                res.links[o_name].append(model.Link(o_var, res.nodes[u"GET"].node_id, self.NETWORK_MIN, 0, 0))
        for i in range(len(s) - 1):
            o_name = u"O[" + str(i + 1) + "]"
            o2_name = u"O[" + str(i + 2) + "]"
            res.links[o_name].append(model.Link(res.nodes[o_name].node_id, res.nodes[o2_name].node_id, 1, 0, 0))
        if len(s):
            res.links[u"GET"] = [model.Link(res.nodes[u"GET"].node_id, res.nodes[u"O[1]"].node_id, 1, 0, 0)]
            res.links[u"O[1]"].append(model.Link(res.nodes[u"O[1]"].node_id, res.nodes[u"O[1]"].node_id,
                                                 self.NETWORK_MIN, 0, 0))
        return res

    def link_additive(self, node, expression, graph, parent_graph, renamer, missed, get_new_name, setter,
                      template_arguments=None):
        if NodeBuilder.is_operand_constant(expression):
            return
        if isinstance(expression, grammar.Operand):
            expression = grammar.Expression(u'*', grammar.ConstantOperand(1.0, expression.line, expression.lexpos),
                                            expression, expression.line, expression.lexpos)
        if expression.action == u'+':
            was_mod = False
            if isinstance(expression, grammar.SumExpression):
                ops = expression.operands
            else:
                ops = [expression.loperand, expression.roperand]
            for op in ops:
                #already linkmodded
                if not NodeBuilder.is_operand_constant(op) and isinstance(op, grammar.Operand):
                    if unicode(renamer.get(unicode(op.text), op.text)) in parent_graph.nodes and \
                            parent_graph.nodes[renamer.get(unicode(op.text), op.text)].kind == model.LINKMOD:
                        missed.append(setter((renamer.get(unicode(op.text), op.text), node.node_id, 0, 0, 0), 1))
                        was_mod = True

            if not was_mod:
                for op in ops:
                    self.link_additive(node, op, graph, parent_graph, renamer, missed, get_new_name, setter,
                                       template_arguments)
        if expression.action == u'*':
            #weighted link
            if self.is_operand_constant(expression.loperand):
                if self.is_operand_constant(expression.roperand):
                    if self.is_operand_number(expression.loperand) and self.is_operand_number(expression.roperand):
                        #zero source
                        if float(expression.loperand.text) * float(expression.roperand.text) == 0:
                            return
                    #constant source
                    if self.is_operand_number(expression.roperand):
                        cname = grammar.Variable(u'constant_source#' + fix_name(unicode(expression.roperand.text)),
                                                 expression.line, expression.lexpos)
                        if unicode(cname) not in graph.nodes:
                            graph.nodes[unicode(cname)] = model.Node(0, cname, 0, expression.roperand.text,
                                                                     is_reducable=True)
                        else:
                            graph.nodes[unicode(cname)].ntype = model.ADDITIVE
                    else:
                        cname = grammar.Variable(u'constant_source#', expression.line, expression.lexpos,
                                                 indexvars=[expression.roperand.text])
                        missed.append((renamer.get(unicode(expression.roperand.text), expression.roperand.text),
                                       cname, 0, 0, 1))
                        if unicode(cname) not in graph.nodes:
                            graph.nodes[unicode(cname)] = model.Node(0, cname, 0, expression.roperand.text,
                                                                     is_reducable=True)
                        else:
                            graph.nodes[unicode(cname)].ntype = model.ADDITIVE
                else:
                    cname = expression.roperand.text

                neg = cname.negative
                cname.negative = False
                try:
                    v = float(expression.loperand.text) * (-1 if neg else 1)
                except TypeError:
                    v = expression.loperand.text
                    v.negative = v.negative != neg

                missed.append(setter((renamer.get(unicode(cname), cname), node.node_id, 0, 0, 0), v))
                return
            raise model.GraphError("deprecated action")

    @staticmethod
    def is_operand_lonely_constant(operand):
        """
        :type operand: grammar.Operand
        :rtype: bool
        """
        return isinstance(operand, grammar.VariableOperand) and operand.text.is_runtime_constant

    @staticmethod
    def is_operand_constant(operand):
        """
        :type operand: grammar.Operand
        :rtype: bool
        """
        return isinstance(operand, grammar.ConstantOperand) or\
            (isinstance(operand, grammar.VariableOperand) and
             (operand.text.is_constant or operand.text.is_runtime_constant or operand.text.eval_constant is not None))

    @staticmethod
    def is_operand_number(operand):
        """
        :type operand: grammar.Operand
        :rtype: bool
        """
        return isinstance(operand, grammar.ConstantOperand) or\
            (isinstance(operand, grammar.VariableOperand) and operand.text.eval_constant is not None)

    @staticmethod
    def extract_constant(expression):
        res = 0.0
        res_ops = []
        if isinstance(expression, grammar.SumExpression):
            nops = []
            for op in expression.operands:
                if NodeBuilder.is_operand_constant(op):
                    if NodeBuilder.is_operand_number(op):
                        res += op.text
                    else:
                        res_ops.append(op)
                elif isinstance(op, grammar.Expression) and NodeBuilder.is_operand_constant(op.roperand):
                    if NodeBuilder.is_operand_number(op.loperand) and NodeBuilder.is_operand_number(op.roperand):
                        res += float(op.loperand.text) * float(op.roperand.text)
                    else:
                        try:
                            f = float(op.loperand.text)
                            if f == 0.0:
                                pass
                            elif f == 1.0:
                                res_ops.append(op.roperand)
                            else:
                                res_ops.append(op)
                        except TypeError:
                            if NodeBuilder.is_operand_lonely_constant(op.loperand) and \
                                    NodeBuilder.is_operand_lonely_constant(op.roperand):
                                raise model.GraphError("Ambiguous expression " + unicode(op) +
                                                       ": dynamic constants cannot be used in expressions")
                            res_ops.append(op)
                else:
                    nops.append(op)
            expression.operands = nops
            expression.heights = [0] * len(nops)
        res_ops.insert(0, grammar.ConstantOperand(res, expression.line, expression.lexpos))
        return res_ops

    @staticmethod
    def get_template_dependencies(template_arguments):
        """
        :type template_arguments: list[grammar.Argument | grammar.Operand | grammar.Sequence | grammar.Generator |
                grammar.Equation]
        :rtype: list[grammar.Variable]
        """
        return list(set([v for a in template_arguments
                         if isinstance(a, grammar.Generator) or isinstance(a, grammar.Sequence)
                         or isinstance(a, grammar.Operand) or isinstance(a, grammar.Argument)
                         for v in get_expression_vars(a)]))

    @staticmethod
    def get_template_constants(template_arguments, global_constants):
        """
        :type template_arguments: list[grammar.Argument | grammar.Operand | grammar.Sequence | grammar.Generator |
                grammar.Equation]
        :type global_constants: dict[unicode, grammar.Set]
        :rtype: list[grammar.Variable]
        """
        c1 = [v for a in template_arguments if isinstance(a, grammar.Generator) for v in get_expression_vars(a)]
        for c in c1:
            c.is_runtime_constant = True
        c2 = [v for a in template_arguments
              if isinstance(a, grammar.Sequence)
              for v in get_expression_vars(a) + [a.var_name]]
        for c in c2:
            c.is_constant = True
        c3 = filter(lambda x: len([gc for gc in global_constants.values() if x.match(gc.var_name)]),
                    [v for a in template_arguments if isinstance(a, grammar.Argument) and
                     isinstance(a.value, grammar.VariableOperand) for v in get_expression_vars(a)])
        for c in c3:
            c.is_constant = True
        return c1 + c2 + c3