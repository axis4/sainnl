# coding=UTF-8
"""
   Copyright 2014 IoS (progsdi@gmail.com)

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""
from itertools import chain
import threading
import types
from ply import lex, yacc
import re

__author__ = 'sergeionov'

IN = 'in'
OUT = 'out'
SYNC = 'sync'
ASYNC = 'async'
UNIT = 'additive'
LINKMOD = 'multiplicator'
STRING = 'string'

EXIT = 'exit'
TICK = 'tick'
TIME = 'time'
STDIN = 'stdin'
STDOUT = 'stdout'
LISTEN = 'listen'
SOCKET = 'socket'

INFINITY = 'INF'


class GrammarError(Exception):
    def __init__(self, *args, **kwargs):
        super(GrammarError, self).__init__(*args, **kwargs)
        self.source = None

    def __unicode__(self):
        return (self.source if self.source is not None else "unknown") + ": " + super(GrammarError, self).__unicode__()


class ParseError(Exception):
    def __init__(self, node, line, lexpos, reason):
        super(Exception, self).__init__()
        self.node = node
        self.line = line
        self.lexpos = lexpos
        self.reason = reason

    def __repr__(self):
        return "Line %s Symbol %s: %s - %s" % (repr(self.line), repr(self.lexpos), repr(self.node), repr(self.reason))


class LexYaccRules(object):
    errors = []
    lock = threading.Lock()
    text = None
    lexer = None
    parser = None

    reserved = {
        'as': 'AS',
        'from': 'FROM',
        'reduction': 'REDUCTION',
        'of': 'OF',
        'is': 'IS',
        IN: 'IN',
        OUT: 'OUT',
        SYNC: 'SYNC',
        ASYNC: 'ASYNC',
        STRING: 'STRING',
        INFINITY: 'INF',
    }

    def p_error(self, p):
        self.errors.append(ParseError(p, self.find_line(p), self.find_column(p), "illegal token"))

    # whole program
    @staticmethod
    def p_nl_program(p):
        """
        nl_program : groups
        """
        p[0] = p[1]

    @staticmethod
    def p_empty(p):
        """
        empty :
        """
        pass

    ## [N=3,M=4]
    def p_global_template_arguments(self, p):
        """
        groups : LBRAKET global_template_arguments RBRAKET groups
        """
        p[0] = [GlobalArgs(p[2], self.find_line(p), self.find_column(p))] + p[4]

    ## name [i=1..N, i < 2, j=G] PARAM, PARAM2 { ... }
    def p_groups(self, p):
        """
        groups : group_header LCURLY group_body RCURLY groups
        """
        p[0] = [Group(p[1], p[3], self.find_line(p[1]), self.find_column(p[1]))] + p[5]

    ## \n
    @staticmethod
    def p_groups_newline(p):
        """
        groups : NEWLINE groups
        """
        p[0] = p[2]

    ##
    @staticmethod
    def p_groups_empty(p):
        """
        groups : empty
        """
        p[0] = []

    ## trouble { trouble }
    def p_groups_error(self, p):
        """
        groups : error LCURLY group_body RCURLY groups
        groups : group_header LCURLY error RCURLY groups
        groups : error LCURLY error RCURLY groups
        """
        p[0] = [Group(None, None, self.find_line(p, 2), self.find_column(p, 2))] + p[5]

    ### N=3,M=4
    @staticmethod
    def p_global_template_arguments_def(p):
        """
        global_template_arguments : global_template_arguments_list
        """
        p[0] = p[1]

    #### N=3,M=4
    @staticmethod
    def p_global_template_arguments_list(p):
        """
        global_template_arguments_list : global_template_arguments_list_item COMMA global_template_arguments_list
        """
        p[0] = p[1]
        p[0].merge(p[3])

    @staticmethod
    def p_global_template_arguments_newline(p):
        """
        global_template_arguments_list : global_template_arguments_list_item COMMA NEWLINE global_template_arguments_list
        """
        p[0] = p[1]
        p[0].merge(p[4])

    #### N=3
    @staticmethod
    def p_global_template_arguments_list_one(p):
        """
        global_template_arguments_list : global_template_arguments_list_item
        """
        p[0] = p[1]

    ####
    def p_global_template_arguments_list_empty(self, p):
        """
        global_template_arguments_list : empty
        """
        p[0] = MultiArgument(self.find_line(p), self.find_column(p))

    ##### N=3
    def p_global_template_arguments_list_item(self, p):
        """
        global_template_arguments_list_item : set_constant
        """
        p[0] = MultiArgument(self.find_line(p[1]), self.find_column(p[1]), [p[1]])

    ### name [i=1..N, i < 2, j=G] PARAM, PARAM2
    def p_group_header(self, p):
        """
        group_header : packed_name template_arguments arguments
        """
        p[1] = unicode(p[1])
        if p[1] in self.reserved:
            self.errors.append(ParseError(p, self.find_line(p), self.find_column(p), "reserved name"))
        p[0] = GroupHeader(p[1], p[2], p[3], self.find_line(p), self.find_column(p))

    ### name trouble trouble
    def p_group_header_error(self, p):
        """
        group_header : packed_name template_arguments error
        group_header : packed_name error arguments
        group_header : packed_name error error
        """
        p[1] = unicode(p[1])
        p[0] = GroupHeader(p[1],
                           MultiArgument(self.find_line(p, 2), self.find_column(p, 2)),
                           MultiArgument(self.find_line(p, 3), self.find_column(p, 3)),
                           self.find_line(p), self.find_column(p))

    #### PARAM, PARAM2
    @staticmethod
    def p_arguments(p):
        """
        arguments : argument_list
        """
        p[0] = p[1]

    def p_argument_list_newline(self, p):
        """
        argument_list : complex_name COMMA NEWLINE argument_list
        """
        p[0] = MultiArgument(self.find_line(p), self.find_column(p),
                             [Argument(u'expression', VariableOperand(p[1], self.find_line(p), self.find_column(p)),
                                       self.find_line(p), self.find_column(p))])
        p[0].merge(p[4])

    ##### PARAM, PARAM2
    def p_argument_list(self, p):
        """
        argument_list : complex_name COMMA argument_list
        """
        p[0] = MultiArgument(self.find_line(p), self.find_column(p),
                             [Argument(u'expression', VariableOperand(p[1], self.find_line(p), self.find_column(p)),
                                       self.find_line(p), self.find_column(p))])
        p[0].merge(p[3])

    ##### PARAM
    def p_argument_list_one(self, p):
        """
        argument_list : complex_name
        """
        p[0] = MultiArgument(self.find_line(p), self.find_column(p),
                             [Argument(u'expression', VariableOperand(p[1], self.find_line(p), self.find_column(p)),
                                       self.find_line(p), self.find_column(p))])

    #####
    def p_argument_list_empty(self, p):
        """
        argument_list : empty
        """
        p[0] = MultiArgument(self.find_line(p), self.find_column(p))

    #### [i=1..N, i < 2, j=G]
    @staticmethod
    def p_template_arguments(p):
        """
        template_arguments : LBRAKET template_argument_list RBRAKET template_arguments
        """
        p[0] = p[2]
        p[0].merge(p[4])

    ####
    def p_template_arguments_empty(self, p):
        """
        template_arguments : empty
        """
        p[0] = MultiArgument(self.find_line(p), self.find_column(p))

    #### [trouble]
    @staticmethod
    def p_template_arguments_error(p):
        """
        template_arguments : LBRAKET error RBRAKET template_arguments
        """
        p[0] = p[4]

    @staticmethod
    def p_template_argument_newline(p):
        """
        template_argument_list : template_argument_list_item COMMA NEWLINE template_argument_list
        """
        p[0] = p[1]
        p[0].merge(p[4])

    ##### i=1..N, i < 2, j=G
    @staticmethod
    def p_template_argument_list(p):
        """
        template_argument_list : template_argument_list_item COMMA template_argument_list
        """
        p[0] = p[1]
        p[0].merge(p[3])

    ##### i=1..N
    @staticmethod
    def p_template_argument_list_one(p):
        """
        template_argument_list : template_argument_list_item
        """
        p[0] = p[1]

    #####
    def p_template_argument_list_empty(self, p):
        """
        template_argument_list : empty
        """
        p[0] = MultiArgument(self.find_line(p), self.find_column(p))

    ###### i=1..N
    ###### j=G
    ###### i < 2
    def p_template_argument_list_item(self, p):
        """
        template_argument_list_item : sequence
        template_argument_list_item : set
        template_argument_list_item : condition
        """
        p[0] = MultiArgument(self.find_line(p[1]), self.find_column(p[1]), [p[1]])

    ###### K
    def p_template_argument_list_item_simple(self, p):
        """
        template_argument_list_item : complex_name
        """
        p[0] = MultiArgument(self.find_line(p), self.find_column(p),
                             [Argument(u"expression", VariableOperand(p[1], self.find_line(p), self.find_column(p)),
                                       self.find_line(p), self.find_column(p))])

    def p_sequence(self, p):
        """
        sequence : complex_name SET sequence_border RANGE sequence_border
        """
        p[0] = Sequence(p[1], p[3], p[5], self.find_line(p), self.find_column(p))

    def p_sequence_newline(self, p):
        """
        sequence : complex_name SET NEWLINE sequence_border RANGE sequence_border
        """
        p[0] = Sequence(p[1], p[4], p[6], self.find_line(p), self.find_column(p))

    def p_set(self, p):
        """
        set : complex_name SET complex_name
        """
        p[0] = Generator(p[1], p[3], self.find_line(p), self.find_column(p))

    def p_set_newline(self, p):
        """
        set : complex_name SET NEWLINE complex_name
        """
        p[0] = Generator(p[1], p[4], self.find_line(p), self.find_column(p))

    def p_set_constant(self, p):
        """
        set_constant : complex_name SET CONSTANT
        """
        p[0] = Set(p[1], p[3], self.find_line(p), self.find_column(p))

    def p_set_constant_newline(self, p):
        """
        set_constant : complex_name SET NEWLINE CONSTANT
        """
        p[0] = Set(p[1], p[4], self.find_line(p), self.find_column(p))

    @staticmethod
    def p_sequence_border(p):
        """
        sequence_border : expression
        """
        p[0] = p[1]

    def p_condition(self, p):
        """
        condition : expression equation expression
        """
        p[0] = Equation(p[2], p[1], p[3], self.find_line(p[1]), self.find_column(p[1]))

    def p_condition_newline(self, p):
        """
        condition : expression equation NEWLINE expression
        """
        p[0] = Equation(p[2], p[1], p[4], self.find_line(p[1]), self.find_column(p[1]))

    def p_group_body(self, p):
        """
        group_body : template_named_call_definition SEMI group_body
        group_body : template_named_call_definition NEWLINE group_body
        group_body : template_definition SEMI group_body
        group_body : template_definition NEWLINE group_body
        group_body : named_string_definition SEMI group_body
        group_body : named_string_definition NEWLINE group_body
        group_body : named_call_definition SEMI group_body
        group_body : named_call_definition NEWLINE group_body
        group_body : call_definition SEMI group_body
        group_body : call_definition NEWLINE group_body
        group_body : definition SEMI group_body
        group_body : definition NEWLINE group_body
        """
        p[0] = GroupBody([p[1]] if isinstance(p[1], Definition) else [], self.find_line(p[1]), self.find_column(p[1]))
        p[0].add(p[3])

    def p_group_body_one(self, p):
        """
        group_body : template_named_call_definition
        group_body : template_definition
        group_body : named_string_definition
        group_body : named_call_definition
        group_body : call_definition
        group_body : definition
        """
        p[0] = GroupBody([p[1]] if isinstance(p[1], Definition) else [], self.find_line(p), self.find_column(p))

    @staticmethod
    def p_group_nl(p):
        """
        group_body : NEWLINE group_body
        """
        p[0] = p[2]

    @staticmethod
    def p_empty_definition(p):
        """
        definition : empty
        """
        pass

    def p_input_definition(self, p):
        """
        definition : IN in_argument_list
        """
        p[0] = Definition(IN, p[2], self.find_line(p), self.find_column(p))

    def p_output_definition(self, p):
        """
        definition : OUT definition_argument_list
        """
        p[0] = Definition(OUT, p[2], self.find_line(p), self.find_column(p))

    def p_call_definition(self, p):
        """
        call_definition : SYNC definition_argument_list
        call_definition : ASYNC definition_argument_list
        call_definition : packed_name definition_argument_list
        call_definition : UNIT definition_argument_list
        call_definition : LINKMOD definition_argument_list
        """
        p[1] = unicode(p[1])
        if p[1] == '#':
            p[1] = UNIT
        if p[1] == '&':
            p[1] = LINKMOD
        p[0] = Definition(p[1], p[2], self.find_line(p), self.find_column(p))

    def p_string_definition(self, p):
        """
        string_definition : STRING_FULL
        """
        p[0] = Definition(STRING,
                          DefinitionArguments(None,
                                              MultiArgument(self.find_line(p), self.find_column(p),
                                                            [Argument(u"expression", StringOperand(p[1][1:-1],
                                                                                                   self.find_line(p),
                                                                                                   self.find_column(p)),
                                                                      self.find_line(p), self.find_column(p))]),
                                              self.find_line(p), self.find_column(p)),
                          self.find_line(p), self.find_column(p))

    def p_string_definition_strict_newline(self, p):
        """
        string_definition : STRING NEWLINE STRING_FULL
        string_definition : STRING_SIGN NEWLINE STRING_FULL
        """
        p[0] = Definition(STRING,
                          DefinitionArguments(None,
                                              MultiArgument(self.find_line(p), self.find_column(p),
                                                            [Argument(u"expression", StringOperand(p[3][1:-1],
                                                                                                   self.find_line(p),
                                                                                                   self.find_column(p)),
                                                                      self.find_line(p), self.find_column(p))]),
                                              self.find_line(p), self.find_column(p)),
                          self.find_line(p), self.find_column(p))

    def p_string_definition_strict(self, p):
        """
        string_definition : STRING STRING_FULL
        string_definition : STRING_SIGN STRING_FULL
        """
        p[0] = Definition(STRING,
                          DefinitionArguments(None,
                                              MultiArgument(self.find_line(p), self.find_column(p),
                                                            [Argument(u"expression", StringOperand(p[2][1:-1],
                                                                                                   self.find_line(p),
                                                                                                   self.find_column(p)),
                                                                      self.find_line(p), self.find_column(p))]),
                                              self.find_line(p), self.find_column(p)),
                          self.find_line(p), self.find_column(p))

    def p_template_named_call_definition(self, p):
        """
        template_named_call_definition : LBRAKET argument_list RBRAKET named_call_definition
        """
        p[0] = TemplateNamedDefinition(p[4], p[2], self.find_line(p), self.find_column(p))

    def p_template_definition(self, p):
        """
        template_definition : LBRAKET argument_list RBRAKET definition
        """
        p[0] = TemplateDefinition(p[4], p[2], self.find_line(p), self.find_column(p))

    def p_named_reduction_call_definition_newline(self, p):
        """
        named_call_definition : complex_name IS REDUCTION OF NEWLINE call_definition
        """
        p[0] = NamedDefinition(p[1], p[6], True, self.find_line(p), self.find_column(p))

    def p_named_reduction_call_definition(self, p):
        """
        named_call_definition : complex_name IS REDUCTION OF call_definition
        """
        p[0] = NamedDefinition(p[1], p[5], True, self.find_line(p), self.find_column(p))

    def p_named_reduction_sign_call_definition_newline(self, p):
        """
        named_call_definition : complex_name IS_REDUCTION_SIGN NEWLINE call_definition
        """
        p[0] = NamedDefinition(p[1], p[4], True, self.find_line(p), self.find_column(p))

    def p_named_reduction_sign_call_definition(self, p):
        """
        named_call_definition : complex_name IS_REDUCTION_SIGN call_definition
        """
        p[0] = NamedDefinition(p[1], p[3], True, self.find_line(p), self.find_column(p))

    def p_named_call_definition_newline(self, p):
        """
        named_call_definition : complex_name IS NEWLINE call_definition
        named_call_definition : complex_name SET NEWLINE call_definition
        """
        p[0] = NamedDefinition(p[1], p[4], False, self.find_line(p), self.find_column(p))

    def p_named_call_definition(self, p):
        """
        named_call_definition : complex_name IS call_definition
        named_call_definition : complex_name SET call_definition
        """
        p[0] = NamedDefinition(p[1], p[3], False, self.find_line(p), self.find_column(p))

    def p_named_string_definition_newline(self, p):
        """
        named_string_definition : complex_name IS NEWLINE string_definition
        named_string_definition : complex_name SET NEWLINE string_definition
        """
        p[0] = NamedDefinition(p[1], p[4], False, self.find_line(p), self.find_column(p))

    def p_named_string_definition(self, p):
        """
        named_string_definition : complex_name IS string_definition
        named_string_definition : complex_name SET string_definition
        """
        p[0] = NamedDefinition(p[1], p[3], False, self.find_line(p), self.find_column(p))

    def p_named_call_expression_newline(self, p):
        """
        named_call_definition : complex_name IS NEWLINE expression
        named_call_definition : complex_name SET NEWLINE expression
        """
        p[0] = NamedDefinition(p[1], Definition(
            UNIT, DefinitionArguments(None, MultiArgument(
                self.find_line(p), self.find_column(p),
                [Argument(u"condition",
                          Equation(u'>=',
                                   ConstantOperand(0.0, self.find_line(p, 2), self.find_column(p, 2)),
                                   ConstantOperand(0.0, self.find_line(p, 2), self.find_column(p, 2)),
                                   self.find_line(p, 2), self.find_column(p, 2)),
                          self.find_line(p), self.find_column(p)),
                 Argument(u"expression", p[4], self.find_line(p[4]), self.find_column(p[4]))]
            ), self.find_line(p), self.find_column(p)), self.find_line(p), self.find_column(p)
        ), False, self.find_line(p), self.find_column(p))

    def p_named_call_expression(self, p):
        """
        named_call_definition : complex_name IS expression
        named_call_definition : complex_name SET expression
        """
        p[0] = NamedDefinition(p[1], Definition(
            UNIT, DefinitionArguments(None, MultiArgument(
                self.find_line(p), self.find_column(p),
                [Argument(u"condition",
                          Equation(u'>=',
                                   ConstantOperand(0.0, self.find_line(p, 2), self.find_column(p, 2)),
                                   ConstantOperand(0.0, self.find_line(p, 2), self.find_column(p, 2)),
                                   self.find_line(p, 2), self.find_column(p, 2)),
                          self.find_line(p), self.find_column(p)),
                 Argument(u"expression", p[3], self.find_line(p[3]), self.find_column(p[3]))]
            ), self.find_line(p), self.find_column(p)), self.find_line(p), self.find_column(p)
        ), False, self.find_line(p), self.find_column(p))

    def p_in_argument_list(self, p):
        """
        in_argument_list : complex_name COMMA in_argument_list
        """
        p[0] = DefinitionArguments(None, MultiArgument(self.find_line(p), self.find_column(p),
                                                       [Argument(u"expression",
                                                                 VariableOperand(p[1], self.find_line(p),
                                                                                 self.find_column(p)),
                                                                 self.find_line(p), self.find_column(p))]),
                                   self.find_line(p), self.find_column(p))
        p[0].call_arguments.merge(p[3].call_arguments)

    def p_in_argument_list_newline(self, p):
        """
        in_argument_list : complex_name COMMA NEWLINE in_argument_list
        """
        p[0] = DefinitionArguments(None, MultiArgument(self.find_line(p), self.find_column(p),
                                                       [Argument(u"expression",
                                                                 VariableOperand(p[1], self.find_line(p),
                                                                                 self.find_column(p)),
                                                                 self.find_line(p), self.find_column(p))]),
                                   self.find_line(p), self.find_column(p))
        p[0].call_arguments.merge(p[4].call_arguments)

    def p_in_argument_list_one(self, p):
        """
        in_argument_list : complex_name
        """
        p[0] = DefinitionArguments(None, MultiArgument(self.find_line(p), self.find_column(p),
                                                       [Argument(u"expression",
                                                                 VariableOperand(p[1], self.find_line(p),
                                                                                 self.find_column(p)),
                                                                 self.find_line(p), self.find_column(p))]),
                                   self.find_line(p), self.find_column(p))

    def p_definition_argument_list_newline(self, p):
        """
        definition_argument_list : definition_argument_list_position COMMA NEWLINE definition_argument_list_key
        """
        if isinstance(p[1], MultiArgument) and isinstance(p[4], MultiArgument):
            p[1].merge(p[4])
        if isinstance(p[1], MultiArgument):
            p[0] = DefinitionArguments(None, p[1], self.find_line(p[1]), self.find_column(p[1]))
        else:
            if isinstance(p[4], MultiArgument):
                p[0] = DefinitionArguments(None, p[4], self.find_line(p[1]), self.find_column(p[1]))
            else:
                p[0] = DefinitionArguments(None, None, self.find_line(p[1]), self.find_column(p[1]))

    def p_definition_argument_list(self, p):
        """
        definition_argument_list : definition_argument_list_position COMMA definition_argument_list_key
        """
        if isinstance(p[1], MultiArgument) and isinstance(p[3], MultiArgument):
            p[1].merge(p[3])
        if isinstance(p[1], MultiArgument):
            p[0] = DefinitionArguments(None, p[1], self.find_line(p[1]), self.find_column(p[1]))
        else:
            if isinstance(p[3], MultiArgument):
                p[0] = DefinitionArguments(None, p[3], self.find_line(p[1]), self.find_column(p[1]))
            else:
                p[0] = DefinitionArguments(None, None, self.find_line(p[1]), self.find_column(p[1]))

    def p_definition_argument_list_template(self, p):
        """
        definition_argument_list : LBRAKET definition_argument_list_key RBRAKET definition_argument_list
        """
        if isinstance(p[4], DefinitionArguments):
            p[0] = p[4]
            p[0].template_arguments = p[2]
        else:
            p[0] = DefinitionArguments(p[2], None, self.find_line(p), self.find_column(p))

    def p_definition_argument_list_one(self, p):
        """
        definition_argument_list : definition_argument_list_position
        definition_argument_list : definition_argument_list_key
        """
        if isinstance(p[1], MultiArgument):
            p[0] = DefinitionArguments(None, p[1], self.find_line(p[1]), self.find_column(p[1]))
        else:
            p[0] = DefinitionArguments(None, None, self.find_line(p[1]), self.find_column(p[1]))

    def p_definition_argument_list_empty(self, p):
        """
        definition_argument_list : empty
        """
        p[0] = DefinitionArguments(None, None, self.find_line(p), self.find_column(p))

    def p_definition_argument_list_position_newline(self, p):
        """
        definition_argument_list_position : definition_argument_list_item COMMA NEWLINE definition_argument_list_position
        """
        p[0] = MultiArgument(self.find_line(p[1]), self.find_column(p[1]), [p[1]])
        p[0].merge(p[4])

    def p_definition_argument_list_position(self, p):
        """
        definition_argument_list_position : definition_argument_list_item COMMA definition_argument_list_position
        """
        p[0] = MultiArgument(self.find_line(p[1]), self.find_column(p[1]), [p[1]])
        p[0].merge(p[3])

    def p_definition_argument_list_position_one(self, p):
        """
        definition_argument_list_position : definition_argument_list_item
        """
        p[0] = MultiArgument(self.find_line(p[1]), self.find_column(p[1]), [p[1]])

    def p_definition_argument_list_key_newline(self, p):
        """
        definition_argument_list_key : complex_name KEY_SIGN definition_argument_list_item COMMA NEWLINE definition_argument_list_key
        """
        p[0] = MultiArgument(self.find_line(p), self.find_column(p),
                             [KeyArgument(p[1], p[3], self.find_line(p), self.find_column(p))])
        p[0].merge(p[6])

    def p_definition_argument_list_key(self, p):
        """
        definition_argument_list_key : complex_name KEY_SIGN definition_argument_list_item COMMA definition_argument_list_key
        """
        p[0] = MultiArgument(self.find_line(p), self.find_column(p),
                             [KeyArgument(p[1], p[3], self.find_line(p), self.find_column(p))])
        p[0].merge(p[5])

    def p_definition_argument_list_key_one(self, p):
        """
        definition_argument_list_key : complex_name KEY_SIGN definition_argument_list_item
        """
        p[0] = MultiArgument(self.find_line(p), self.find_column(p),
                             [KeyArgument(p[1], p[3], self.find_line(p), self.find_column(p))])

    @staticmethod
    def p_definition_argument_list_item(p):
        """
        definition_argument_list_item : definition_argument_list_item_expression
        """
        p[0] = p[1]

    def p_definition_argument_list_item_rename(self, p):
        """
        definition_argument_list_item : complex_name AS definition_argument_list_item_expression
        definition_argument_list_item : complex_name AS_SIGN definition_argument_list_item_expression
        """
        p[0] = RenamedArgument(p[1], p[3], self.find_line(p), self.find_column(p))

    def p_definition_argument_list_item_expression(self, p):
        """
        definition_argument_list_item_expression : expression
        """
        p[0] = Argument(u"expression", p[1], self.find_line(p[1]), self.find_column(p[1]))

    def p_definition_argument_list_item_condition(self, p):
        """
        definition_argument_list_item_expression : condition
        """
        p[0] = Argument(u"condition", p[1], self.find_line(p[1]), self.find_column(p[1]))

    def p_definition_argument_list_item_invokation(self, p):
        """
        definition_argument_list_item_expression : LPAREN definition_argument_list_item_named RPAREN
        """
        p[0] = Argument(u"invokation", p[2], self.find_line(p), self.find_column(p))

    def p_definition_argument_list_item_invokated_named_definition(self, p):
        """
        definition_argument_list_item_named : complex_name FROM named_call_definition
        definition_argument_list_item_named : complex_name FROM call_definition
        definition_argument_list_item_named : complex_name FROM_SIGN named_call_definition
        definition_argument_list_item_named : complex_name FROM_SIGN call_definition
        """
        p[0] = ExposeDefinition(p[1], p[3], self.find_line(p), self.find_column(p))

    @staticmethod
    def p_definition_argument_list_item_invokated_expression(p):
        """
        definition_argument_list_item_named : complex_name FROM complex_name
        definition_argument_list_item_named : complex_name FROM_SIGN complex_name
        """
        if p[1].glob:
            p[0] = p[1]
        else:
            p[3].add_subvar(p[1])
            p[0] = p[3]

    @staticmethod
    def p_definition_argument_list_item_invokated_definition(p):
        """
        definition_argument_list_item_named : named_call_definition
        definition_argument_list_item_named : call_definition
        """
        p[0] = p[1]

    @staticmethod
    def p_equation(p):
        """
        equation : EQUALS
        equation : NEQUALS
        equation : GREATER
        equation : LESSER
        equation : G_OR_EQ
        equation : L_OR_EQ
        """
        p[0] = p[1]

    def p_expression_newline(self, p):
        """
        expression : expression PLUS NEWLINE term
        expression : expression MINUS NEWLINE term
        """
        p[0] = Expression(unicode(p[2]), p[1], p[4], self.find_line(p[1]), self.find_column(p[1]))

    def p_expression(self, p):
        """
        expression : expression PLUS term
        expression : expression MINUS term
        """
        p[0] = Expression(unicode(p[2]), p[1], p[3], self.find_line(p[1]), self.find_column(p[1]))

    @staticmethod
    def p_expression_term(p):
        """
        expression : term
        """
        p[0] = p[1]

    def p_term_newline(self, p):
        """
        term : term TIMES NEWLINE factor
        term : term DIVIDE NEWLINE factor
        """
        p[0] = Expression(unicode(p[2]), p[1], p[4], self.find_line(p[1]), self.find_column(p[1]))

    def p_term(self, p):
        """
        term : term TIMES factor
        term : term DIVIDE factor
        """
        p[0] = Expression(unicode(p[2]), p[1], p[3], self.find_line(p[1]), self.find_column(p[1]))

    @staticmethod
    def p_term_factor(p):
        """
        term : factor
        """
        p[0] = p[1]

    def p_factor_num(self, p):
        """
        factor : CONSTANT
        """
        p[0] = ConstantOperand(float(p[1]), self.find_line(p), self.find_column(p))

    def p_factor_inf(self, p):
        """
        factor : INF
        """
        p[0] = VariableOperand(p[1], self.find_line(p), self.find_column(p))
        p[0].is_constant = True

    def p_factor_num_minus(self, p):
        """
        factor : MINUS CONSTANT
        """
        p[0] = ConstantOperand(float(p[2]) * -1, self.find_line(p), self.find_column(p))

    def p_factor_inf_minus(self, p):
        """
        factor : MINUS INF
        """
        p[0] = VariableOperand(p[2], self.find_line(p), self.find_column(p))
        p[0].is_constant = True
        p[0].negative = True

    def p_factor_var(self, p):
        """
        factor : complex_name
        """
        p[0] = VariableOperand(p[1], self.find_line(p), self.find_column(p))

    @staticmethod
    def p_factor_expr(p):
        """
        factor : LPAREN expression RPAREN
        """
        p[0] = p[2]

    def p_packed_name(self, p):
        """
        packed_name : NAME
        """
        p[0] = Variable(p[1], self.find_line(p), self.find_column(p))

    def p_packed_name_long(self, p):
        """
        packed_name : NAME DOT packed_name
        """
        p[0] = Variable(p[1], self.find_line(p), self.find_column(p), subvar=p[3])

    def p_complex_name_empty(self, p):
        """
        ex_complex_name : empty
        """
        p[0] = Variable(None, self.find_line(p), self.find_column(p))

    def p_complex_name_more(self, p):
        """
        ex_complex_name : text_complex_name
        """
        if not p[1].glob:
            raise ParseError(p, self.find_line(p), self.find_column(p), "name parts must be dot-separated")
        p[1].glob = False
        p[0] = Variable(None, self.find_line(p), self.find_column(p), subvar=p[1])

    @staticmethod
    def p_complex_name(p):
        """
        complex_name : text_complex_name
        """
        p[0] = p[1]

    @staticmethod
    def p_complex_name_minus(p):
        """
        complex_name : MINUS text_complex_name
        """
        p[0] = p[2]
        p[0].toggle_negative()

    @staticmethod
    def p_complex_name_long(p):
        """
        text_complex_name : packed_name rest_complex ex_complex_name
        """
        p[0] = p[1]
        sv = p[1]
        while sv.subvar is not None:
            sv = sv.subvar
        sv.merge(p[2])
        sv.merge(p[3])

    @staticmethod
    def p_complex_name_global(p):
        """
        text_complex_name : DOT packed_name rest_complex ex_complex_name
        """
        p[0] = p[2]
        p[0].glob = True
        sv = p[2]
        while sv.subvar is not None:
            sv = sv.subvar
        sv.merge(p[3])
        sv.merge(p[4])

    def p_rest_complex_empty(self, p):
        """
        rest_complex : empty
        """
        p[0] = Variable(None, self.find_line(p), self.find_column(p))

    def p_rest_complex_recursive(self, p):
        """
        rest_complex : LBRAKET index_complex RBRAKET rest_complex
        """
        p[0] = Variable(None, self.find_line(p), self.find_column(p))
        p[0].merge(p[2])
        p[0].merge(p[4])

    def p_index_complex(self, p):
        """
        index_complex : text_complex_name
        """
        p[0] = Variable(None, self.find_line(p), self.find_column(p), [p[1]])

    def p_index_constant(self, p):
        """
        index_complex : CONSTANT
        """
        p[0] = Variable(None, self.find_line(p), self.find_column(p), [int(p[1])])

    @staticmethod
    def p_index_complex_newline(p):
        """
        index_complex : index_complex COMMA NEWLINE index_complex
        """
        p[0] = p[1]
        p[0].merge(p[4])

    @staticmethod
    def p_index_complex_recursive(p):
        """
        index_complex : index_complex COMMA index_complex
        """
        p[0] = p[1]
        p[0].merge(p[3])

    @staticmethod
    def t_MULTICOMMENT(t):
        r"""\"\"\"(.|\n)*?\"\"\""""
        t.lexer.lineno += t.value.count("\n")
        pass

    @staticmethod
    def t_COMMENT(t):
        r"""\"[^\"\n]*\""""
        pass

    t_EQUALS = r'=='
    t_KEY_SIGN = r':='
    t_AS_SIGN = r'~'
    t_FROM_SIGN = r'@'
    t_IS_REDUCTION_SIGN = r'=\+'
    t_PLUS = r'\+'
    t_UNIT = r'\#'
    t_LINKMOD = r'\&'
    t_STRING_SIGN = r'\$'
    t_MINUS = r'-'
    t_TIMES = r'\*'
    t_DIVIDE = r'/'
    t_NEQUALS = r'!=|=/=|<>'
    t_G_OR_EQ = r'>='
    t_L_OR_EQ = r'<='
    t_SET = r'='
    t_RANGE = r'\.\.'
    t_SEMI = r';'
    t_GREATER = r'>'
    t_LESSER = r'<'
    t_LPAREN = r'\('
    t_RPAREN = r'\)'
    t_LBRAKET = r'\['
    t_RBRAKET = r'\]'
    t_LCURLY = r'\{'
    t_RCURLY = r'\}'
    t_COMMA = r','
    t_STRING_FULL = r'\'[^\'\n\r]*\''
    t_DOT = r'\.'

    def t_NAME(self, t):
        r"""[^\W]*[^\W\d]+([^\W])*"""
        t.type = self.reserved.get(t.value, 'NAME')
        return t

    @staticmethod
    def t_CONSTANT(t):
        r"""[1-9]*[0-9]+(\.[1-9][0-9]*)?"""
        t.value = float(t.value)
        return t

    # Ignored characters
    t_ignore = " \t"

    @staticmethod
    def t_NEWLINE(t):
        r"""\n+"""
        t.lexer.lineno += t.value.count("\n")
        return t

    def t_error(self, t):
        self.errors.append(ParseError(t, self.find_line(t), self.find_column(t),
                                      u"Illegal character '{0:s}'".format(t.value[0])))
        t.lexer.skip(1)

    tokens = (
        "KEY_SIGN",
        "AS_SIGN", "FROM_SIGN", "IS_REDUCTION_SIGN",
        "PLUS", "MINUS", "TIMES", "DIVIDE",
        "EQUALS", "NEQUALS", "GREATER", "LESSER", "G_OR_EQ", "L_OR_EQ",
        "LPAREN", "RPAREN", "LBRAKET", "RBRAKET", "LCURLY", "RCURLY",
        "COMMA", "STRING_FULL", "DOT",
        "SET", "RANGE", "SEMI", "NEWLINE",
        "NAME", "CONSTANT",
        "UNIT", "LINKMOD", "STRING_SIGN"
    ) + tuple(reserved.values())

    @staticmethod
    def find_line(token, index=1):
        return (token.lineno(index) if isinstance(getattr(token, 'lineno'), types.MethodType) else token.lineno)\
            if hasattr(token, 'lineno') else token.line

    def find_column(self, token, index=1):
        pos = token.lexpos(index) if isinstance(getattr(token, 'lexpos'), types.MethodType) else token.lexpos
        last_cr = self.text.rfind('\n', 0, pos)
        if last_cr < 0:
            last_cr = 0
        column = (pos - last_cr) + 1
        return column

    def build(self, **kwargs):
        self.lexer = lex.lex(object=self, reflags=re.UNICODE, **kwargs)
        self.parser = yacc.yacc(module=self, start='nl_program')

    def parse(self, text):
        self.lock.acquire()
        try:
            self.text = text
            return self.parser.parse(text, lexer=self.lexer)
        finally:
            self.lock.release()


class AppliedPermutation(object):
    def __init__(self):
        super(AppliedPermutation, self).__init__()
        self.permutation_state = {}
        """:type: dict[str|unicode, long]"""


class LineItem(AppliedPermutation):
    def __init__(self, line, lexpos):
        """
        :type line: long
        :type lexpos: long
        """
        super(LineItem, self).__init__()
        self.line = line
        self.lexpos = lexpos


class Variable(LineItem):
    def __init__(self, name, line, lexpos, indexvars=None, subvar=None, glob=False, is_constant=False,
                 is_runtime_constant=False, eval_constant=None, negative=False, mark_renamed=False):
        """
        :type name: unicode | None
        :type line: long
        :type lexpos: long
        :type indexvars: list[Variable | long]
        :type subvar: Variable
        :type glob: bool
        :type is_constant: bool
        :type is_runtime_constant: bool
        :type eval_constant: float | None
        :type negative: bool
        :type mark_renamed: bool
        """
        super(Variable, self).__init__(line, lexpos)
        if name is not None and not isinstance(name, (str, unicode)):
            raise Exception("Unexpected parameter state")
        if indexvars is not None and not isinstance(indexvars, (list, tuple)):
            raise Exception("Unexpected parameter state")
        self.name = name
        """:type: str | unicode | None"""
        self.indexvars = indexvars if indexvars is not None else []
        """:type: list[Variable | long]"""
        self.subvar = subvar
        """:type: Variable"""
        self.glob = glob
        """:type: bool"""
        self.is_constant = is_constant
        """:type: bool"""
        self.is_runtime_constant = is_runtime_constant
        """:type: bool"""
        self.eval_constant = eval_constant
        """:type: float | None"""
        self.negative = negative
        """:type: bool"""
        self.renamed_mark = mark_renamed
        """:type: bool"""

    def match(self, var):
        """
        :type var: Variable | None
        :rtype: bool
        """
        c1 = self.clone()
        c1.negative = False
        c2 = var.clone()
        c2.negative = False
        return unicode(c1).startswith(unicode(c2))

    def find(self, var):
        """
        :type var: Variable | None
        :rtype: list[Variable]
        """
        res = []
        if self.match(var):
            res.append(self)
        res += chain(*[iv.find(var) for iv in self.indexvars if isinstance(iv, Variable)])
        if self.subvar is not None:
            res += self.subvar.find(var)
        return res

    def toggle_negative(self):
        self.negative = not self.negative

    @staticmethod
    def build(text):
        """
        :type text: str | unicode
        :rtype: Variable | int | long
        """
        name = unicode(text)

        try:
            return int(name)
        except ValueError:
            pass  # well, not a number

        neg = False
        if name[0] == u"-":
            neg = True
            name = name[1:]
        s = 0
        t = u""
        var = Variable(u"", 0, 0, [], negative=neg)
        start = True
        filling = var
        for c in name:
            if s:
                if c == u"[":
                    s += 1
                elif c == u"]":
                    s -= 1
                if s < 0:
                    raise GrammarError("Missing opening bracket")
                if (c == u"," and s == 1) or not s:
                    if len(t):
                        filling.indexvars.append(Variable.build(t))
                    t = u""
                elif s:
                    t += c
            else:
                if c == u".":
                    if start:
                        filling.glob = True
                    else:
                        if len(t):
                            filling.name = t
                        if not len(filling.name):
                            raise GrammarError("Missing name in subvar")
                        t = u""
                        filling.subvar = Variable(u"", 0, 0, [])
                        filling = filling.subvar
                elif c == u"[":
                    s += 1
                    if len(t):
                        filling.name = t
                    t = u""
                elif c == u"]":
                    raise GrammarError("Missing opening bracket")
                else:
                    t += c
            if start:
                start = False
        if s:
            raise GrammarError("Missing closing bracket")
        if len(t):
            filling.name = t

        return var


    @staticmethod
    def replace(var, from_part, to_part, mark_renamed=False):
        """
        :type var: Variable | None
        :type from_part: Variable | None
        :type to_part: Variable | None
        :rtype: Variable | None
        """
        if not isinstance(var, Variable) or var.name != from_part.name:
            return var
        s_var = unicode(var)
        if var.negative:
            s_var = s_var[1:]
        s_from = unicode(from_part)
        s_to = unicode(to_part)
        s_res = s_var
        if s_var.startswith(s_from):
            s_res = s_to + s_res[len(s_from):]
        res = Variable.build(s_res)
        if isinstance(res, Variable):
            res.is_constant = var.is_constant
            res.is_runtime_constant = var.is_runtime_constant
            res.eval_constant = var.eval_constant
            res.negative = var.negative
            res.renamed_mark = mark_renamed
        return res

    def merge(self, var_part):
        """
        :type var_part: Variable
        """
        if self.name is not None and var_part.name is not None and self.name != var_part.name:
            raise GrammarError("Conflict in variable merge: " + repr(self.name) + " and " + repr(var_part.name))
        if self.name is None:
            self.name = var_part.name
            self.glob = var_part.glob
        add = []
        """:type: list[Variable | long]"""
        for vi in var_part.indexvars:
            p = False
            for mi in self.indexvars:
                if repr(vi) == repr(mi):
                    p = True
            if not p:
                add.append(vi)
        self.indexvars += add
        if self.subvar is not None and var_part.subvar is not None and repr(self.subvar) != repr(var_part.subvar):
            raise GrammarError("Conflict in variable merge: " + repr(self.subvar) + " and " + repr(var_part.subvar))
        if self.subvar is None:
            self.subvar = var_part.subvar

    def add_subvar(self, var_part):
        t = self
        while t.subvar is not None:
            t = t.subvar
        t.merge(Variable(None, self.line, self.lexpos, subvar=var_part))

    def _as_repr(self, out_format):
        """
        :type out_format: unicode
        """
        return out_format.format(
            "." if self.glob else "",
            repr(self.name)[2:-1] if isinstance(self.name, unicode) else repr(self.name)[1:-1],
            "[" + ",".join(repr(i) for i in self.indexvars) + "]" if len(self.indexvars) else "",
            "." + repr(self.subvar) if self.subvar is not None else "",
            "C" if self.is_constant else "",
            "RC" if self.is_runtime_constant else "",
            unicode(self.eval_constant),
            "-" if self.negative else "")

    def _as_str(self, out_format):
        """
        :type out_format: unicode
        """
        return out_format.format(
            "." if self.glob else "",
            unicode(self.name),
            "[" + ",".join(unicode(i) for i in self.indexvars) + "]" if len(self.indexvars) else "",
            "." + unicode(self.subvar) if self.subvar is not None else "",
            "-" if self.negative else "")

    def __repr__(self):
        return self._as_repr(u"\"Variable{4:s}{5:s}:negative\"{7:s}\"glob\"{0:s}\"name\"{1:s}"
                             u"\"indexing\"{2:s}\"subvar\"{3:s}")

    def __unicode__(self):
        if self.eval_constant is not None:
            return unicode(int(self.eval_constant))
        return self._as_str(u"{4:s}{0:s}{1:s}{2:s}{3:s}")

    def __float__(self):
        if self.eval_constant is not None:
            return self.eval_constant * (-1.0 if self.negative else 1.0)
        raise TypeError("Not evaluable variable " + repr(self))

    def clone(self):
        return Variable(self.name, self.line, self.lexpos,
                        [i.clone() if isinstance(i, Variable) else i for i in self.indexvars],
                        self.subvar.clone() if self.subvar is not None else None, self.glob, self.is_constant,
                        self.is_runtime_constant, self.eval_constant, self.negative)


class Operand(LineItem):
    def __init__(self, kind, text, line, lexpos):
        """
        :type kind: unicode
        :type text: long | float | unicode | Variable
        :type line: long
        :type lexpos: long
        """
        super(Operand, self).__init__(line, lexpos)
        if text is None:
            raise Exception("Unexpected parameter state")
        self.kind = kind
        self.text = text

    def __repr__(self):
        return u"\"Operand {0:s}\" {1:s}".format(
            self.kind, repr(self.text) if self.kind != u'string' else "'" + self.text + "'",)

    def clone(self):
        return Operand(self.kind, self.text.clone() if hasattr(self.text, "clone") else self.text,
                       self.line, self.lexpos)


class ConstantOperand(Operand):
    def __init__(self, text, line, lexpos):
        """
        :type text: long | float
        :type line: long
        :type lexpos: long
        """
        super(ConstantOperand, self).__init__(u"value", text, line, lexpos)

    def clone(self):
        return ConstantOperand(self.text, self.line, self.lexpos)


class VariableOperand(Operand):
    def __init__(self, text, line, lexpos):
        """
        :type text: unicode | Variable
        :type line: long
        :type lexpos: long
        """
        super(VariableOperand, self).__init__(u"var",
                                              text if isinstance(text, Variable) else Variable(text, line, lexpos),
                                              line, lexpos)

    def clone(self):
        return VariableOperand(self.text, self.line, self.lexpos)


class StringOperand(Operand):
    def __init__(self, text, line, lexpos):
        """
        :type text: unicode
        :type line: long
        :type lexpos: long
        """
        super(StringOperand, self).__init__(u"string", unicode(text), line, lexpos)

    def clone(self):
        return StringOperand(self.text, self.line, self.lexpos)


class Sequence(LineItem):
    def __init__(self, var_name, start, end, line, lexpos):
        """
        :type var_name: Variable
        :type start: Expression
        :type end: Expression
        :type line: long
        :type lexpos: long
        """
        super(Sequence, self).__init__(line, lexpos)
        self.var_name = var_name
        self.start = start
        self.end = end

    def __repr__(self):
        return u"\"Sequence\" {0:s} = {1:s} .. {2:s}".format(self.var_name, repr(self.start), repr(self.end))


class Generator(LineItem):
    def __init__(self, var_name, linked_name, line, lexpos):
        """
        :type var_name: Variable
        :type linked_name: Variable
        :type line: long
        :type lexpos: long
        """
        super(Generator, self).__init__(line, lexpos)
        self.var_name = var_name
        self.linked_name = linked_name

    def __repr__(self):
        return u"\"Generator\" {0:s} = {1:s}".format(self.var_name, repr(self.linked_name))


class Set(LineItem):
    def __init__(self, var_name, value, line, lexpos):
        """
        :type var_name: Variable
        :type value: float | Variable
        :type line: long
        :type lexpos: long
        """
        super(Set, self).__init__(line, lexpos)
        self.var_name = var_name
        self.value = value

    def __repr__(self):
        return u"\"Set\" {0:s} = {1:s}".format(self.var_name, repr(self.value))


class Equation(LineItem):
    def __init__(self, action, loperand, roperand, line, lexpos):
        """
        :type action: unicode
        :type loperand: Operand | Expression | Equation
        :type roperand: Operand | Expression | Equation
        :type line: long
        :type lexpos: long
        """
        super(Equation, self).__init__(line, lexpos)
        if action == u'!=' or action == u'<>' or action == u'=/=':
            action = u'!='
        self.action = action
        self.loperand = loperand
        self.roperand = roperand

    def __repr__(self):
        return u"\"Equation\" {0:s} {1:s} {2:s}".format(repr(self.loperand), self.action, repr(self.roperand))

    def clone(self):
        return Equation(self.action, self.loperand.clone(), self.roperand.clone(), self.line, self.lexpos)


class Expression(LineItem):
    def __init__(self, action, loperand, roperand, line, lexpos):
        """
        :type action: unicode
        :type loperand: Operand | Expression | Equation
        :type roperand: Operand | Expression | Equation
        :type line: long
        :type lexpos: long
        """
        super(Expression, self).__init__(line, lexpos)
        self.action = action
        self.loperand = loperand
        self.roperand = roperand

    def __repr__(self):
        return u"\"Expression\" ({0:s} {1:s} {2:s})".format(repr(self.loperand), self.action, repr(self.roperand))

    def clone(self):
        return Expression(self.action, self.loperand.clone(), self.roperand.clone(), self.line, self.lexpos)


class SumExpression(LineItem):
    def __init__(self, line, lexpos):
        """
        :type line: long
        :type lexpos: long
        """
        super(SumExpression, self).__init__(line, lexpos)
        self.action = "+"
        self.operands = []
        """:type: list[Operand|Expression]"""
        self.heights = []
        """:type: list[long]"""

    def add_item(self, operand, height):
        """
        :type operand: Operand | Expression | Equation
        :type height: long
        """
        self.operands.append(operand)
        self.heights.append(height)

    def __repr__(self):
        return u"\"SumExpression\" ({0:s})".format((" " + self.action + " ").join([repr(o) for o in self.operands]))

    def clone(self):
        res = SumExpression(self.line, self.lexpos)
        res.operands = [o.clone() for o in self.operands]
        res.heights = self.heights[:]
        return res


class GlobalArgs(LineItem):
    def __init__(self, sequences, line, lexpos):
        """
        :type sequences: MultiArgument
        :type line: long
        :type lexpos: long
        """
        super(GlobalArgs, self).__init__(line, lexpos)
        self.sequences = sequences

    def __repr__(self):
        return u"\"GlobalArgs\" [{0:s}]".format(repr(self.sequences))


class GroupHeader(LineItem):
    def __init__(self, name, templates, arguments, line, lexpos):
        """
        :type name: unicode
        :type templates: MultiArgument
        :type arguments: MultiArgument
        :type line: long
        :type lexpos: long
        """
        super(GroupHeader, self).__init__(line, lexpos)
        self.name = name
        self.arguments = [a for a in arguments.arguments if a is not None] if arguments.arguments is not None else []
        """:type: list[Argument]"""
        self.templates = [a for a in templates.arguments if a is not None] if templates.arguments is not None else []
        """:type: list[Argument | Operand | Sequence | Generator | Equation]"""

    def __repr__(self):
        return u"\"GroupHeader\" {0:s} [{1:s}]{2:s}".format(
            self.name, ",".join([repr(a) for a in self.templates]),
            " " + ",".join([repr(a) for a in self.arguments]) if len(self.arguments) else "")


class GroupBody(LineItem):
    def __init__(self, definitions, line, lexpos):
        """
        :type definitions: list[Definition]
        :type line: long
        :type lexpos: long
        """
        super(GroupBody, self).__init__(line, lexpos)
        self.definitions = definitions

    def add(self, group_body):
        """
        :type group_body: GroupBody
        """
        self.definitions += group_body.definitions

    def __repr__(self):
        return u"\"GroupBody\" {0:s}".format(";".join([repr(d) for d in self.definitions]),)


class Group(LineItem):
    def __init__(self, group_header, group_body, line, lexpos):
        """
        :type group_header: GroupHeader|None
        :type group_body: GroupBody|None
        :type line: long
        :type lexpos: long
        """
        super(Group, self).__init__(line, lexpos)
        self.name = "UNDEFINED"
        """:type: unicode"""
        self.arguments = []
        """:type: list[Argument]"""
        self.templates = []
        """:type: list[Argument | Operand | Sequence | Generator | Equation]"""
        self.definitions = []
        """:type: list[Definition]"""
        if group_header is not None:
            self.name = group_header.name
            self.arguments = [a for a in group_header.arguments if a is not None]
            self.templates = [a for a in group_header.templates if a is not None]
        if group_body is not None:
            self.definitions = group_body.definitions

    def __repr__(self):
        return u"\"Group\" {0:s} [{1:s}]{2:s} {{ {3:s} }}".format(self.name,
                                                                  ",".join([repr(a) for a in self.templates]),
                                                                  " " + ",".join([repr(a) for a in self.arguments])
                                                                  if len(self.arguments) else "",
                                                                  ";".join([repr(d) for d in self.definitions]))


class Argument(LineItem):
    def __init__(self, kind, value, line, lexpos):
        """
        :type kind: unicode
        :type value: Expression | CallDefinition | Condition
        :type line: long
        :type lexpos: long
        """
        super(Argument, self).__init__(line, lexpos)
        self.kind = kind
        self.value = value
        """:type: Expression | Definition | Equation | Operand"""

    def __repr__(self):
        if self.kind == u"expression":
            return u"\"Argument\" {0:s}".format(repr(self.value),)
        if self.kind == u"invokation":
            return u"\"Argument\" ({0:s})".format(repr(self.value),)
        if self.kind == u"condition":
            return u"\"Argument\" {0:s}".format(repr(self.value),)
        return u"\"Argument of unknown type {0:s}\"".format(repr(self.kind))


class RenamedArgument(Argument):
    def __init__(self, name, argument, line, lexpos):
        """
        :type name: unicode
        :type argument: Argument
        :type line: long
        :type lexpos: long
        """
        super(RenamedArgument, self).__init__(argument.kind, argument.value, line, lexpos)
        self.name = name

    def __repr__(self):
        if self.kind == u"expression":
            return u"\"Reargument\" {0:s} as {1:s}".format(repr(self.name), repr(self.value),)
        if self.kind == u"invokation":
            return u"\"Reargument\" {0:s} as ({1:s})".format(repr(self.name), repr(self.value),)
        if self.kind == u"condition":
            return u"\"Reargument\" {0:s} as {1:s}".format(repr(self.name), repr(self.value),)
        return u"\"Reargument of unknown type {0:s}\"".format(repr(self.kind))


class MultiArgument(LineItem):
    def __init__(self, line, lexpos, arguments=None):
        """
        :type line: long
        :type lexpos: long
        :type arguments: list[Argument | Sequence | Generator | Equation] | None
        """
        super(MultiArgument, self).__init__(line, lexpos)
        if arguments is None:
            self.arguments = []
        else:
            self.arguments = arguments

    def add(self, argument):
        self.arguments.append(argument)

    def merge(self, multi_argument):
        self.arguments += multi_argument.arguments

    def __repr__(self):
        return u"\"MultiArgument\" {0:s}".format(",".join([repr(a) for a in self.arguments]),)


class KeyArgument(Argument):
    def __init__(self, key, argument, line, lexpos):
        """
        :type key: Variable
        :type argument: Argument
        :type line: long
        :type lexpos: long
        """
        super(KeyArgument, self).__init__(argument.kind, argument.value, line, lexpos)
        self.key = key
        self.argument = argument

    def __repr__(self):
        return u"\"KeyArgument\" {0:s} = {1:s}".format(repr(self.key), repr(self.argument),)


class DefinitionArguments(LineItem):
    def __init__(self, template_arguments, call_arguments, line, lexpos):
        """
        :type template_arguments: MultiArgument | None
        :type call_arguments: MultiArgument | None
        :type line: long
        :type lexpos: long
        """
        super(DefinitionArguments, self).__init__(line, lexpos)
        self.template_arguments = template_arguments
        self.call_arguments = call_arguments

    def __repr__(self):
        if self.template_arguments is not None:
            return u"\"DefinitionArguments\" [{0:s}] {1:s}".format(repr(self.template_arguments),
                                                                   repr(self.call_arguments)
                                                                   if self.call_arguments is not None else "",)
        else:
            return u"\"DefinitionArguments\" {0:s}".format(repr(self.call_arguments)
                                                           if self.call_arguments is not None else "",)


class Definition(LineItem):
    def __init__(self, group_name, arguments, line, lexpos):
        """
        :type group_name: str | unicode
        :type arguments: DefinitionArguments
        :type line: long
        :type lexpos: long
        """
        super(Definition, self).__init__(line, lexpos)
        self.group_name = group_name
        self.arguments = arguments
        self.pristine = True

    def __repr__(self):
        return u"\"Definition\" {0:s} {1:s}".format(self.group_name, repr(self.arguments))


class NamedDefinition(Definition):
    def __init__(self, result_name, definition, reduction, line, lexpos):
        """
        :type result_name: Variable
        :type definition: Definition
        :type reduction: bool
        :type line: long
        :type lexpos: long
        """
        super(NamedDefinition, self).__init__(definition.group_name, definition.arguments, line, lexpos)
        self.result_name = result_name
        self.chain_to = definition
        self.reduction = reduction

    def __repr__(self):
        return u"\"NamedDefinition\" {0:s} {1:s} {2:s}".format(
            repr(self.result_name), u"is reduction of" if self.reduction else u"is", repr(self.chain_to))


class TemplateNamedDefinition(NamedDefinition):
    def __init__(self, definition, template_vars, line, lexpos):
        """
        :type definition: NamedDefinition
        :type template_vars: MultiArgument
        :type line: long
        :type lexpos: long
        """
        super(TemplateNamedDefinition, self).__init__(definition.result_name, definition, definition.reduction,
                                                      line, lexpos)
        self.template_vars = template_vars
        self.named = definition

    def __repr__(self):
        return u"\"TemplateNamedDefinition\" [{0:s}] {1:s}".format(repr(self.template_vars), repr(self.named))


class TemplateDefinition(Definition):
    def __init__(self, definition, template_vars, line, lexpos):
        """
        :type definition: Definition
        :type template_vars: MultiArgument
        :type line: long
        :type lexpos: long
        """
        super(TemplateDefinition, self).__init__(definition.group_name, definition.arguments, line, lexpos)
        self.template_vars = template_vars
        self.templated = definition

    def __repr__(self):
        return u"\"TemplateDefinition\" [{0:s}] {1:s}".format(repr(self.template_vars), repr(self.templated))


class ExposeDefinition(Definition):
    def __init__(self, field_name, definition, line, lexpos):
        """
        :type field_name: Variable
        :type definition: Definition
        :type line: long
        :type lexpos: long
        """
        super(ExposeDefinition, self).__init__(definition.group_name, definition.arguments, line, lexpos)
        self.field_name = field_name
        self.chain_to = definition

    def __repr__(self):
        return u"\"ExposeDefinition\" {0:s} from {1:s}".format(repr(self.field_name), repr(self.chain_to))


class GrammarService(object):
    def __init__(self, **lexer_args):
        self.parser = LexYaccRules()
        self.lock = threading.Lock()
        self.parser.build(**lexer_args)

    def parse(self, text):
        """
        :type text: unicode
        :raise GrammarError:
        :rtype: list[GlobalArgs | Group]
        """
        self.lock.acquire()
        try:
            self.parser.errors = []
            res = self.parser.parse(text)
            if len(self.parser.errors):
                raise GrammarError("\n".join([repr(p) for p in self.parser.errors]))
            return res
        finally:
            self.lock.release()
