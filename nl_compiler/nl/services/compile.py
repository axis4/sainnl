# coding=UTF-8
"""
   Copyright 2014 IoS (progsdi@gmail.com)

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""
import os
from nl.services import grammar
from nl.services.grammar import GrammarService
from nl.services.graph import GraphService
from nl.services.graph_util.graph_layout import NetworkXLayout
from nl.services.graph_util.model import GraphError
from nl.services.package import PackageService
from nl.services.transform import TransformService
from nl_compiler import settings

__author__ = 'sergeionov'


class CompileService(object):
    @staticmethod
    def build_groups(text, package):
        """
        :type text: unicode
        :type package: unicode
        :rtype: dict[unicode, nl.services.graph_util.model.Graph]
        """
        try:
            return GraphService(
                PackageService([os.path.join(settings.STATIC_ROOT, 'lib')])).\
                construct(package, GrammarService().parse(text))
        except grammar.GrammarError, e:
            if e.source is None:
                e.source = package
            raise

    @staticmethod
    def compile(text, package):
        """
        :type text: unicode
        :type package: unicode
        :raise GraphError:
        :rtype: unicode
        """
        g = CompileService.build_groups(text, ".".join(package.split(".")[:-1]))
        if package in g:
            return NetworkXLayout().layout(g[package]).to_xml()
        raise GraphError("Start group '" + package + "' is not found")

    @staticmethod
    def transform(xml, context):
        """
        :type xml: unicode
        :rtype: tuple(unicode, unicode)
        """
        ts = TransformService()
        return ts.to_perl(xml, context), ts.to_runtime(xml, context)