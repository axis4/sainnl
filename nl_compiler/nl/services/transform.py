# coding=UTF-8
"""
   Copyright 2014 IoS (progsdi@gmail.com)

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""
from lxml import etree

__author__ = 'sergeionov'


class TransformService(object):
    perl = etree.XSLT(etree.XML("""
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output encoding="UTF-8" disable-output-escaping="yes" method="text"/>
    <xsl:template match="node">
        <xsl:if test="type = 'neuron' or type = 'multiplicator' or type = 'expected'"><xsl:value-of select="id/text()" />(<xsl:value-of select="threshold/text()" /><xsl:if test="not(threshold)">0</xsl:if>,<xsl:value-of select="constant/text()" /><xsl:if test="not(constant)">0</xsl:if>-<xsl:choose><xsl:when test="input = 1">i</xsl:when><xsl:when test="output = 1">o</xsl:when><xsl:otherwise></xsl:otherwise></xsl:choose><xsl:choose><xsl:when test="type = 'expected'">e</xsl:when><xsl:when test="type = 'multiplicator'">l</xsl:when><xsl:otherwise></xsl:otherwise></xsl:choose><xsl:if test="template = 1">-t</xsl:if>):<xsl:apply-templates select="./links/link"/><xsl:text>
</xsl:text>
        </xsl:if>
    </xsl:template>
    <xsl:template match="link"><xsl:text> </xsl:text><xsl:value-of select="to/text()" />(<xsl:value-of select="activation/text()" /><xsl:if test="not(activation)">0</xsl:if>,<xsl:value-of select="stream/text()" /><xsl:if test="not(stream)">0</xsl:if>,<xsl:value-of select="generation/text()" /><xsl:if test="not(generation)">0</xsl:if>)</xsl:template>
</xsl:stylesheet>
"""))
    runtime = etree.XSLT(etree.XML("""
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output encoding="UTF-8" disable-output-escaping="yes" method="text"/>
    <xsl:template match="node">{"blockName":"<xsl:value-of select="id/text()" />","type":"<xsl:choose><xsl:when test="type = 'expected'">additive</xsl:when><xsl:otherwise><xsl:value-of select="type/text()" /></xsl:otherwise></xsl:choose>","parameters":{<xsl:if test="input = 1">"input":{"type":0,"value":1},</xsl:if><xsl:if test="output = 1">"output":{"type":0,"value":1},</xsl:if><xsl:if test="threshold">"threshold":<xsl:choose><xsl:when test="number(threshold/text()) = threshold/text()">{"type":2,"value":<xsl:value-of select="threshold/text()" />}</xsl:when><xsl:otherwise>{"type":3,"value":"<xsl:value-of select="threshold/text()" />"}</xsl:otherwise></xsl:choose>,</xsl:if><xsl:if test="constant">"bias":<xsl:choose><xsl:when test="number(constant/text()) = constant/text()">{"type":2,"value":<xsl:value-of select="constant/text()" />}</xsl:when><xsl:otherwise>{"type":3,"value":"<xsl:value-of select="constant/text()" />"}</xsl:otherwise></xsl:choose>,</xsl:if><xsl:if test="type = 'expected'">"expected":{"type":0,"value":1},</xsl:if><xsl:if test="template = 1">"template":{"type":0,"value":1},</xsl:if>},"items":[<xsl:apply-templates select="./links/link"/>]},</xsl:template>
    <xsl:template match="link">{"itemName":"<xsl:value-of select="to/text()" />","parameters":{<xsl:if test="activation">"activation":<xsl:choose><xsl:when test="number(activation/text()) = activation/text()">{"type":2,"value":<xsl:value-of select="activation/text()" />}</xsl:when><xsl:otherwise>{"type":3,"value":"<xsl:value-of select="activation/text()" />"}</xsl:otherwise></xsl:choose>,</xsl:if><xsl:if test="stream">"stream":<xsl:choose><xsl:when test="number(stream/text()) = stream/text()">{"type":2,"value":<xsl:value-of select="stream/text()" />}</xsl:when><xsl:otherwise>{"type":3,"value":"<xsl:value-of select="stream/text()" />"}</xsl:otherwise></xsl:choose>,</xsl:if><xsl:if test="generation">"generation":<xsl:choose><xsl:when test="number(generation/text()) = generation/text()">{"type":2,"value":<xsl:value-of select="generation/text()" />}</xsl:when><xsl:otherwise>{"type":3,"value":"<xsl:value-of select="generation/text()" />"}</xsl:otherwise></xsl:choose>,</xsl:if>}},</xsl:template>
</xsl:stylesheet>
"""))
    server = etree.XSLT(etree.XML("""
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output encoding="UTF-8"/>
</xsl:stylesheet>
"""))

    def to_perl(self, xml, context):
        print "converting xml to perl"
        return unicode(self.perl(etree.XML(xml)))

    def to_server(self, xml, context):
        #TODO: return unicode(self.server(etree.XML(xml)))
        return ""

    def to_runtime(self, xml, context):
        return unicode(self.runtime(etree.XML(xml)))\
            .replace(",}", "}").replace(", }", "}")\
            .replace(",]", "]").replace(", ]", "]")
