# coding=UTF-8
"""
   Copyright 2014 IoS (progsdi@gmail.com)

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""
import os

__author__ = 'sergeionov'


class PackageError(Exception):
    pass


class PackageService(object):
    def __init__(self, libdirs):
        """
        :type libdirs: list[unicode]
        """
        self.libdirs = libdirs

    def find_package(self, package_name):
        """
        :type package_name: unicode
        :raise PackageError:
        :rtype: unicode, unicode
        """
        f = None
        for lib in self.libdirs:
            lp = os.path.join(lib, package_name.replace('.', '/')) + ".nl"
            if os.path.isfile(lp):
                f = lp
        if f is None:
            raise PackageError("Package " + package_name + " not found")
        with open(f) as fi:
            return f, fi.read()
