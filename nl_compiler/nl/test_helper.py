# coding=UTF-8
"""
   Copyright 2014 IoS (progsdi@gmail.com)

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""
from django.test import TestCase

__author__ = 'sergeionov'

from django.test.runner import DiscoverRunner


class DatabaselessTestRunner(DiscoverRunner):
    """A test suite runner that does not set up and tear down a database."""

    def setup_databases(self):
        """Overrides DiscoverRunner"""
        pass

    def teardown_databases(self, *args):
        """Overrides DiscoverRunner"""
        pass


class DatabaselessTestCase(TestCase):
    def _pre_setup(self):
        """Overrides TestCase"""
        pass

    def _post_teardown(self):
        """Overrides TestCase"""
        pass