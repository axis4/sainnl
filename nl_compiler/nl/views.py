# coding=UTF-8
"""
   Copyright 2014 IoS (progsdi@gmail.com)

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""
import traceback
from django.conf import settings
from django.core.urlresolvers import reverse
from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from django.template.context import RequestContext
from nl.services.compile import CompileService


def index(request, *parts):
    t = loader.get_template("index.html")
    c = RequestContext(request)
    return HttpResponse(t.render(c))


def compiler(request, *parts):
    package = request.POST.get("package", None)
    text = request.POST.get("text")
    data = {
        "package": package
    }
    if text is not None:
        c = CompileService()
        ok = True
        try:
            nnl = c.compile(text.replace('\r\n', '\n'), package)
        except Exception, e:
            ok = False
            traceback.print_exc()
            nnl = unicode(e)
        if ok:
            try:
                (perl, run) = c.transform(nnl, None)
            except Exception, e:
                traceback.print_exc()
                (perl, run) = (unicode(e), unicode(e))
        else:
            (perl, run) = ("", "")
        data["text"] = text
        data["nnl"] = nnl.replace('<node>', '\n<node>').replace('</nodes>', '\n</nodes>')
        data["perl"] = perl
        data["run"] = run
    t = loader.get_template("form.html")
    c = RequestContext(request, data)
    return HttpResponse(t.render(c))


class ValidateHostMiddleware(object):
    def process_request(self, request):
        if not request.META['HTTP_HOST'].endswith(settings.APP_URL):
            add = ""
            if request.META['HTTP_HOST'] in settings.URL_REDIRECTS:
                add = reverse(settings.URL_REDIRECTS[request.META['HTTP_HOST']])
            return HttpResponseRedirect('http://' + settings.APP_URL + (add + request.path).replace('//', '/'))


class ValidateSlashMiddleware(object):
    def process_request(self, request):
        if not request.path.endswith("/"):
            return HttpResponseRedirect(request.path + "/" + request.META.get("QUERY_STRING", ""))
