# coding=UTF-8
"""
   Copyright 2014 IoS (progsdi@gmail.com)

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""
import traceback
from nl.services import grammar
from nl.services.grammar import GrammarService, GrammarError
from nl.services.graph import GraphService
from nl.services.graph_util import model
from nl.services.graph_util.graph_layout import SquareLayout
from nl.services.graph_util.model import Graph, Node, Link
from nl.services.package import PackageError, PackageService
from nl.services.transform import TransformService
from nl.test_helper import DatabaselessTestCase


class SimpleTest(DatabaselessTestCase):
    def test_basic_addition(self):
        """
        Tests that 1 + 1 always equals 2.
        """
        self.assertEqual(1 + 1, 2)


class GrammarTest(DatabaselessTestCase):

    def test_simple(self):
        t = {
            'name': 'simple',
            'input': """
                test { }
            """,
            'output': ["\"Group\" test [] {  }"],
            'exception': None
        }
        self.do_parsing_tree(t)

    def test_simple_with_bad_argument(self):
        t = {
            'name': 'simple with bad argument',
            'input': """
                test (A) { }
            """,
            'output': None,
            'exception': GrammarError
        }
        self.do_parsing_tree(t)

    def test_simple_with_argument(self):
        t = {
            'name': 'simple with argument',
            'input': """
                test A { }
            """,
            'output': ["\"Group\" test [] "
                       "\"Argument\" \"Operand var\" "
                       "\"Variable:negative\"\"glob\"\"name\"A\"indexing\"\"subvar\" {  }"],
            'exception': None
        }
        self.do_parsing_tree(t)

    def test_comment(self):
        t = {
            'name': 'comment',
            'input': """
                test {
                    \"\"\"
                    q
                    \"\"\"
                }
            """,
            'output': ["\"Group\" test [] {  }"],
            'exception': None
        }
        self.do_parsing_tree(t)

    def test_top_comment(self):
        t = {
            'name': 'top comment',
            'input': """
                \"\"\"
                q
                \"\"\"
                test { }
            """,
            'output': ["\"Group\" test [] {  }"],
            'exception': None
        }
        self.do_parsing_tree(t)

    def test_diff_global(self):
        t = {
            'name': 'diff_global',
            'input': """
                test {
                    A = call B
                    C = call (.D)
                }
            """,
            'output': ["\"Group\" test [] { "
                       "\"NamedDefinition\" \"Variable:negative\"\"glob\"\"name\"A\"indexing\"\"subvar\""
                       " is \"Definition\" call \"DefinitionArguments\" \"MultiArgument\" "
                       "\"Argument\" \"Operand var\" \"Variable:negative\"\"glob\"\"name\"B\"indexing\"\"subvar\";"
                       "\"NamedDefinition\" \"Variable:negative\"\"glob\"\"name\"C\"indexing\"\"subvar\""
                       " is \"Definition\" call \"DefinitionArguments\" \"MultiArgument\" "
                       "\"Argument\" \"Operand var\" \"Variable:negative\"\"glob\".\"name\"D\"indexing\"\"subvar\" }"],
            'exception': None
        }
        self.do_parsing_tree(t)

    def test_plugins(self):
        t = {
            'name': 'plugins',
            'input': """
                network {
                    I = stdin
                    T = turing I
                    [.PROCESS.MEMORY_SIZE,.PROCESS.TERMINATE] O.RESULT.RES[.PROCESS.MEMORY_SIZE][.PROCESS.TERMINATE] = stdout (.RESULT.RES[.PROCESS.MEMORY_SIZE][.PROCESS.TERMINATE])
                }

                turing I {}
            """,
            'output': [
                "\"Group\" network [] { \"NamedDefinition\" \"Variable:negative\"\"glob\"\"name\"I\"indexing\"\"subvar\""
                " is \"Definition\" stdin \"DefinitionArguments\" ;"
                "\"NamedDefinition\" \"Variable:negative\"\"glob\"\"name\"T\"indexing\"\"subvar\""
                " is \"Definition\" turing \"DefinitionArguments\" \"MultiArgument\" "
                "\"Argument\" \"Operand var\" \"Variable:negative\"\"glob\"\"name\"I\"indexing\"\"subvar\";"
                "\"TemplateNamedDefinition\" [\"MultiArgument\" "
                "\"Argument\" \"Operand var\" \"Variable:negative\"\"glob\".\"name\"PROCESS\"indexing\"\"subvar\".\"Variable:negative\"\"glob\"\"name\"MEMORY_SIZE\"indexing\"\"subvar\","
                "\"Argument\" \"Operand var\" \"Variable:negative\"\"glob\".\"name\"PROCESS\"indexing\"\"subvar\".\"Variable:negative\"\"glob\"\"name\"TERMINATE\"indexing\"\"subvar\"] "
                "\"NamedDefinition\" \"Variable:negative\"\"glob\"\"name\"O\"indexing\"\"subvar\".\"Variable:negative\"\"glob\"\"name\"RESULT\"indexing\"\"subvar\".\"Variable:negative\"\"glob\"\"name\"RES\"indexing\"[\"Variable:negative\"\"glob\".\"name\"PROCESS\"indexing\"\"subvar\".\"Variable:negative\"\"glob\"\"name\"MEMORY_SIZE\"indexing\"\"subvar\","
                "\"Variable:negative\"\"glob\".\"name\"PROCESS\"indexing\"\"subvar\".\"Variable:negative\"\"glob\"\"name\"TERMINATE\"indexing\"\"subvar\"]\"subvar\""
                " is \"Definition\" stdout \"DefinitionArguments\" \"MultiArgument\" "
                "\"Argument\" \"Operand var\" \"Variable:negative\"\"glob\".\"name\"RESULT\"indexing\"\"subvar\".\"Variable:negative\"\"glob\"\"name\"RES\"indexing\"[\"Variable:negative\"\"glob\".\"name\"PROCESS\"indexing\"\"subvar\".\"Variable:negative\"\"glob\"\"name\"MEMORY_SIZE\"indexing\"\"subvar\","
                "\"Variable:negative\"\"glob\".\"name\"PROCESS\"indexing\"\"subvar\".\"Variable:negative\"\"glob\"\"name\"TERMINATE\"indexing\"\"subvar\"]\"subvar\" }",
                "\"Group\" turing [] \"Argument\" \"Operand var\" \"Variable:negative\"\"glob\"\"name\"I\"indexing\"\"subvar\" {  }"
            ],
            'exception': None
        }
        self.do_parsing_tree(t)

    def test_self(self):
        t = {
            'name': 'self',
            'input': """
                \"Group\"
                test [] \"Argument\" \"Operand var\" \"Variable:negative\"\"glob\"\"name\"A\"indexing\"\"subvar\" {  }
            """,
            'output': ["\"Group\" test [] "
                       "\"Argument\" \"Operand var\" "
                       "\"Variable:negative\"\"glob\"\"name\"A\"indexing\"\"subvar\" {  }"],
            'exception': None
        }
        self.do_parsing_tree(t)

    def test_self_complex(self):
        t = {
            'name': 'self complex',
            'input': "\"Group\" test [] \"Argument\" \"Operand var\" "
                     "\"Variable:negative\"\"glob\"\"name\"A\"indexing\""
                     "[\"Variable:negative\"\"glob\"\"name\"B\"indexing\"\"subvar\"]\"subvar\"."
                     "\"Variable:negative\"\"glob\"\"name\"C\"indexing\"\"subvar\" {  }",
            'output': ["\"Group\" test [] \"Argument\" \"Operand var\" "
                       "\"Variable:negative\"\"glob\"\"name\"A\"indexing\""
                       "[\"Variable:negative\"\"glob\"\"name\"B\"indexing\"\"subvar\"]\"subvar\"."
                       "\"Variable:negative\"\"glob\"\"name\"C\"indexing\"\"subvar\" {  }"],
            'exception': None
        }
        self.do_parsing_tree(t)

    def test_single_definition(self):
        t = {
            'name': 'single definition',
            'input': """
                test {
                    some V
                }
            """,
            'output': [
                "\"Group\" test [] { "
                "\"Definition\" some \"DefinitionArguments\" \"MultiArgument\" "
                "\"Argument\" \"Operand var\" \"Variable:negative\"\"glob\"\"name\"V\"indexing\"\"subvar\" "
                "}"],
            'exception': None
        }
        self.do_parsing_tree(t)

    def test_in_definition(self):
        t = {
            'name': 'in definition',
            'input': """
                test {
                    in I
                }
            """,
            'output': ["\"Group\" test [] { "
                       "\"Definition\" in \"DefinitionArguments\" "
                       "\"MultiArgument\" \"Argument\" "
                       "\"Operand var\" \"Variable:negative\"\"glob\"\"name\"I\"indexing\"\"subvar\" "
                       "}"],
            'exception': None
        }
        self.do_parsing_tree(t)

    def test_in_bad_definition(self):
        t = {
            'name': 'in bad definition',
            'input': """
                test {
                    in I + Y
                }
            """,
            'output': None,
            'exception': GrammarError
        }
        self.do_parsing_tree(t)

    def test_out_definition(self):
        t = {
            'name': 'out definition',
            'input': """
                test {
                    out O
                }
            """,
            'output': [
                "\"Group\" test [] { "
                "\"Definition\" out \"DefinitionArguments\" "
                "\"MultiArgument\" \"Argument\" \"Operand var\" "
                "\"Variable:negative\"\"glob\"\"name\"O\"indexing\"\"subvar\" "
                "}"],
            'exception': None
        }
        self.do_parsing_tree(t)

    def test_sync_definition(self):
        t = {
            'name': 'sync definition',
            'input': """
                test {
                    sync O
                }
            """,
            'output': [
                "\"Group\" test [] { "
                "\"Definition\" sync \"DefinitionArguments\" "
                "\"MultiArgument\" \"Argument\" \"Operand var\" "
                "\"Variable:negative\"\"glob\"\"name\"O\"indexing\"\"subvar\" "
                "}"],
            'exception': None
        }
        self.do_parsing_tree(t)

    def test_sync_variable(self):
        t = {
            'name': 'sync variable',
            'input': """
                test {
                    S = sync O
                }
            """,
            'output': [
                "\"Group\" test [] { "
                "\"NamedDefinition\" \"Variable:negative\"\"glob\"\"name\"S\"indexing\"\"subvar\" is "
                "\"Definition\" sync \"DefinitionArguments\" \"MultiArgument\" "
                "\"Argument\" \"Operand var\" \"Variable:negative\"\"glob\"\"name\"O\"indexing\"\"subvar\" "
                "}"],
            'exception': None
        }
        self.do_parsing_tree(t)

    def test_async_variable(self):
        t = {
            'name': 'async variable',
            'input': """
                test {
                    AS = async O
                }
            """,
            'output': [
                "\"Group\" test [] { "
                "\"NamedDefinition\" \"Variable:negative\"\"glob\"\"name\"AS\"indexing\"\"subvar\" is "
                "\"Definition\" async \"DefinitionArguments\" \"MultiArgument\" \"Argument\" \"Operand var\" "
                "\"Variable:negative\"\"glob\"\"name\"O\"indexing\"\"subvar\" "
                "}"],
            'exception': None
        }
        self.do_parsing_tree(t)

    def test_string_variable(self):
        t = {
            'name': 'string variable',
            'input': """
                test {
                    S = 'ABC'
                    S is string 'ABC'
                    S = $ 'ABC'
                }
            """,
            'output': [
                "\"Group\" test [] { "
                "\"NamedDefinition\" \"Variable:negative\"\"glob\"\"name\"S\"indexing\"\"subvar\" is "
                "\"Definition\" string \"DefinitionArguments\" \"MultiArgument\" \"Argument\" "
                "\"Operand string\" 'ABC';"
                "\"NamedDefinition\" \"Variable:negative\"\"glob\"\"name\"S\"indexing\"\"subvar\" is "
                "\"Definition\" string \"DefinitionArguments\" \"MultiArgument\" \"Argument\" "
                "\"Operand string\" 'ABC';"
                "\"NamedDefinition\" \"Variable:negative\"\"glob\"\"name\"S\"indexing\"\"subvar\" is "
                "\"Definition\" string \"DefinitionArguments\" \"MultiArgument\" \"Argument\" "
                "\"Operand string\" 'ABC' "
                "}"],
            'exception': None
        }
        self.do_parsing_tree(t)

    def test_string_error(self):
        t = {
            'name': 'string error',
            'input': """
                test {
                    S = 'ABC
                }
            """,
            'exception': GrammarError
        }
        self.do_parsing_tree(t)

    def test_single_definition_multiple_arguments(self):
        t = {
            'name': 'single definition multiple arguments',
            'input': """
                test {
                    some I2, I3
                }
            """,
            'output': [
                "\"Group\" test [] { "
                "\"Definition\" some \"DefinitionArguments\" \"MultiArgument\" "
                "\"Argument\" \"Operand var\" \"Variable:negative\"\"glob\"\"name\"I2\"indexing\"\"subvar\","
                "\"Argument\" \"Operand var\" \"Variable:negative\"\"glob\"\"name\"I3\"indexing\"\"subvar\" "
                "}"],
            'exception': None
        }
        self.do_parsing_tree(t)

    def test_multiline_definition(self):
        t = {
            'name': 'multiline definition',
            'input': """
                test {
                    in I
                    in I2
                }
            """,
            'output': [
                "\"Group\" test [] { "
                "\"Definition\" in \"DefinitionArguments\" "
                "\"MultiArgument\" \"Argument\" \"Operand var\" "
                "\"Variable:negative\"\"glob\"\"name\"I\"indexing\"\"subvar\";"
                "\"Definition\" in \"DefinitionArguments\" "
                "\"MultiArgument\" \"Argument\" \"Operand var\" "
                "\"Variable:negative\"\"glob\"\"name\"I2\"indexing\"\"subvar\" "
                "}"],
            'exception': None
        }
        self.do_parsing_tree(t)

    def test_large_definition(self):
        t = {
            'name': 'large definition',
            'input': """
                test {
                    in I
                    in I2, I3
                    O is additive I+I2+I3>=0, 1
                    sync O
                    out O
                }
            """,
            'output': [
                "\"Group\" test [] { "
                "\"Definition\" in \"DefinitionArguments\" "
                "\"MultiArgument\" \"Argument\" \"Operand var\" "
                "\"Variable:negative\"\"glob\"\"name\"I\"indexing\"\"subvar\";"
                "\"Definition\" in \"DefinitionArguments\" "
                "\"MultiArgument\" \"Argument\" \"Operand var\" "
                "\"Variable:negative\"\"glob\"\"name\"I2\"indexing\"\"subvar\","
                "\"Argument\" \"Operand var\" \"Variable:negative\"\"glob\"\"name\"I3\"indexing\"\"subvar\";"
                "\"NamedDefinition\" \"Variable:negative\"\"glob\"\"name\"O\"indexing\"\"subvar\" is "
                "\"Definition\" additive "
                "\"DefinitionArguments\" \"MultiArgument\" \"Argument\" \"Equation\" \"Expression\" (\"Expression\" "
                "(\"Operand var\" \"Variable:negative\"\"glob\"\"name\"I\"indexing\"\"subvar\" + "
                "\"Operand var\" \"Variable:negative\"\"glob\"\"name\"I2\"indexing\"\"subvar\") + "
                "\"Operand var\" \"Variable:negative\"\"glob\"\"name\"I3\"indexing\"\"subvar\") >= "
                "\"Operand value\" 0.0,\"Argument\" \"Operand value\" 1.0;"
                "\"Definition\" sync \"DefinitionArguments\" "
                "\"MultiArgument\" \"Argument\" \"Operand var\" "
                "\"Variable:negative\"\"glob\"\"name\"O\"indexing\"\"subvar\";"
                "\"Definition\" out \"DefinitionArguments\" "
                "\"MultiArgument\" \"Argument\" \"Operand var\" "
                "\"Variable:negative\"\"glob\"\"name\"O\"indexing\"\"subvar\" "
                "}"],
            'exception': None
        }
        self.do_parsing_tree(t)

    def test_chained_definition(self):
        t = {
            'name': 'chained definition',
            'input': """
                test {
                    V is plus I1, I2
                    O is plus V.RES, I3
                    out O
                }
            """,
            'output': [
                "\"Group\" test [] { "
                "\"NamedDefinition\" \"Variable:negative\"\"glob\"\"name\"V\"indexing\"\"subvar\" is "
                "\"Definition\" plus \"DefinitionArguments\" \"MultiArgument\" "
                "\"Argument\" \"Operand var\" \"Variable:negative\"\"glob\"\"name\"I1\"indexing\"\"subvar\","
                "\"Argument\" \"Operand var\" \"Variable:negative\"\"glob\"\"name\"I2\"indexing\"\"subvar\";"
                "\"NamedDefinition\" \"Variable:negative\"\"glob\"\"name\"O\"indexing\"\"subvar\" is "
                "\"Definition\" plus \"DefinitionArguments\" \"MultiArgument\" "
                "\"Argument\" \"Operand var\" "
                "\"Variable:negative\"\"glob\"\"name\"V\"indexing\"\"subvar\"."
                "\"Variable:negative\"\"glob\"\"name\"RES\"indexing\"\"subvar\","
                "\"Argument\" \"Operand var\" \"Variable:negative\"\"glob\"\"name\"I3\"indexing\"\"subvar\";"
                "\"Definition\" out \"DefinitionArguments\" "
                "\"MultiArgument\" \"Argument\" \"Operand var\" "
                "\"Variable:negative\"\"glob\"\"name\"O\"indexing\"\"subvar\" "
                "}"],
            'exception': None
        }
        self.do_parsing_tree(t)

    def test_inner_definition(self):
        t = {
            'name': 'inner definition',
            'input': """
                test {
                    O is plus (RES from V is plus I1, I2), I3
                    out O
                }
            """,
            'output': [
                "\"Group\" test [] { "
                "\"NamedDefinition\" \"Variable:negative\"\"glob\"\"name\"O\"indexing\"\"subvar\" is "
                "\"Definition\" plus \"DefinitionArguments\" \"MultiArgument\" \"Argument\" "
                "(\"ExposeDefinition\" \"Variable:negative\"\"glob\"\"name\"RES\"indexing\"\"subvar\" from "
                "\"NamedDefinition\" \"Variable:negative\"\"glob\"\"name\"V\"indexing\"\"subvar\" is "
                "\"Definition\" plus \"DefinitionArguments\" "
                "\"MultiArgument\" \"Argument\" \"Operand var\" "
                "\"Variable:negative\"\"glob\"\"name\"I1\"indexing\"\"subvar\","
                "\"Argument\" \"Operand var\" \"Variable:negative\"\"glob\"\"name\"I2\"indexing\"\"subvar\"),"
                "\"Argument\" \"Operand var\" \"Variable:negative\"\"glob\"\"name\"I3\"indexing\"\"subvar\";"
                "\"Definition\" out \"DefinitionArguments\" "
                "\"MultiArgument\" \"Argument\" \"Operand var\" "
                "\"Variable:negative\"\"glob\"\"name\"O\"indexing\"\"subvar\" "
                "}"],
            'exception': None
        }
        self.do_parsing_tree(t)

    def test_inner_definition_no_variable(self):
        t = {
            'name': 'inner definition no variable',
            'input': """
                test {
                    O is plus (RES from plus I1, I2), I3
                    out O
                }
            """,
            'output': [
                "\"Group\" test [] { "
                "\"NamedDefinition\" \"Variable:negative\"\"glob\"\"name\"O\"indexing\"\"subvar\" is "
                "\"Definition\" plus \"DefinitionArguments\" \"MultiArgument\" \"Argument\" "
                "(\"ExposeDefinition\" \"Variable:negative\"\"glob\"\"name\"RES\"indexing\"\"subvar\" from "
                "\"Definition\" plus \"DefinitionArguments\" "
                "\"MultiArgument\" \"Argument\" \"Operand var\" "
                "\"Variable:negative\"\"glob\"\"name\"I1\"indexing\"\"subvar\","
                "\"Argument\" \"Operand var\" \"Variable:negative\"\"glob\"\"name\"I2\"indexing\"\"subvar\"),"
                "\"Argument\" \"Operand var\" \"Variable:negative\"\"glob\"\"name\"I3\"indexing\"\"subvar\";"
                "\"Definition\" out \"DefinitionArguments\" "
                "\"MultiArgument\" \"Argument\" \"Operand var\" "
                "\"Variable:negative\"\"glob\"\"name\"O\"indexing\"\"subvar\" "
                "}"],
            'exception': None
        }
        self.do_parsing_tree(t)

    def test_inner_definition_renamed(self):
        t = {
            'name': 'inner definition renamed',
            'input': """
                test {
                    O is plus TT as (RES from plus I1, I2), I3
                    out O
                }
            """,
            'output': [
                "\"Group\" test [] { "
                "\"NamedDefinition\" \"Variable:negative\"\"glob\"\"name\"O\"indexing\"\"subvar\" is "
                "\"Definition\" plus \"DefinitionArguments\" \"MultiArgument\" "
                "\"Reargument\" \"Variable:negative\"\"glob\"\"name\"TT\"indexing\"\"subvar\" as "
                "(\"ExposeDefinition\" \"Variable:negative\"\"glob\"\"name\"RES\"indexing\"\"subvar\" from "
                "\"Definition\" plus \"DefinitionArguments\" "
                "\"MultiArgument\" \"Argument\" \"Operand var\" "
                "\"Variable:negative\"\"glob\"\"name\"I1\"indexing\"\"subvar\","
                "\"Argument\" \"Operand var\" \"Variable:negative\"\"glob\"\"name\"I2\"indexing\"\"subvar\"),"
                "\"Argument\" \"Operand var\" \"Variable:negative\"\"glob\"\"name\"I3\"indexing\"\"subvar\";"
                "\"Definition\" out \"DefinitionArguments\" "
                "\"MultiArgument\" \"Argument\" \"Operand var\" "
                "\"Variable:negative\"\"glob\"\"name\"O\"indexing\"\"subvar\" }"],
            'exception': None
        }
        self.do_parsing_tree(t)

    def test_templating(self):
        t = {
            'name': 'templating',
            'input': """
                test [i=1..N, i<10, j=K] T {
                    O is reduction of plus I[i], T
                    out O
                }
            """,
            'output': [
                "\"Group\" test [\"Sequence\" i = \"Operand value\" 1.0 .. "
                "\"Operand var\" \"Variable:negative\"\"glob\"\"name\"N\"indexing\"\"subvar\","
                "\"Equation\" \"Operand var\" "
                "\"Variable:negative\"\"glob\"\"name\"i\"indexing\"\"subvar\" < \"Operand value\" 10.0,"
                "\"Generator\" j = \"Variable:negative\"\"glob\"\"name\"K\"indexing\"\"subvar\"] "
                "\"Argument\" \"Operand var\" \"Variable:negative\"\"glob\"\"name\"T\"indexing\"\"subvar\" { "
                "\"NamedDefinition\" \"Variable:negative\"\"glob\"\"name\"O\"indexing\"\"subvar\" is reduction of "
                "\"Definition\" plus \"DefinitionArguments\" \"MultiArgument\" \"Argument\" \"Operand var\" "
                "\"Variable:negative\"\"glob\"\"name\"I"
                "\"indexing\"[\"Variable:negative\"\"glob\"\"name\"i\"indexing\"\"subvar\"]\"subvar\","
                "\"Argument\" \"Operand var\" \"Variable:negative\"\"glob\"\"name\"T\"indexing\"\"subvar\";"
                "\"Definition\" out \"DefinitionArguments\" \"MultiArgument\" "
                "\"Argument\" \"Operand var\" \"Variable:negative\"\"glob\"\"name\"O\"indexing\"\"subvar\" "
                "}"],
            'exception': None
        }
        self.do_parsing_tree(t)

    def test_globals(self):
        t = {
            'name': 'globals',
            'input': """
                [N=10]
                test [i=1..N, i<10] T {
                    O is reduction of plus I[i], T
                    out O
                }
            """,
            'output': [
                "\"GlobalArgs\" [\"MultiArgument\" \"Set\" N = 10.0]",
                "\"Group\" test [\"Sequence\" i = \"Operand value\" 1.0 .. "
                "\"Operand var\" \"Variable:negative\"\"glob\"\"name\"N\"indexing\"\"subvar\","
                "\"Equation\" \"Operand var\" "
                "\"Variable:negative\"\"glob\"\"name\"i\"indexing\"\"subvar\" < \"Operand value\" 10.0] "
                "\"Argument\" \"Operand var\" \"Variable:negative\"\"glob\"\"name\"T\"indexing\"\"subvar\" { "
                "\"NamedDefinition\" \"Variable:negative\"\"glob\"\"name\"O\"indexing\"\"subvar\" is reduction of "
                "\"Definition\" plus \"DefinitionArguments\" \"MultiArgument\" \"Argument\" \"Operand var\" "
                "\"Variable:negative\"\"glob\"\"name\"I"
                "\"indexing\"[\"Variable:negative\"\"glob\"\"name\"i\"indexing\"\"subvar\"]\"subvar\","
                "\"Argument\" \"Operand var\" \"Variable:negative\"\"glob\"\"name\"T\"indexing\"\"subvar\";"
                "\"Definition\" out \"DefinitionArguments\" "
                "\"MultiArgument\" \"Argument\" \"Operand var\" "
                "\"Variable:negative\"\"glob\"\"name\"O\"indexing\"\"subvar\" "
                "}"],
            'exception': None
        }
        self.do_parsing_tree(t)

    def test_symbols(self):
        t = {
            'name': 'symbols',
            'input': """
                test {
                    in I
                    in I2, I3
                    O=#I+I2+I3>=0,1
                    C=plus(RES@plus I1,I2),I3
                    C2=+plus TT~(RES@plus I1,I2),I3
                    sync O
                    out O
                }
            """,
            'output': [
                "\"Group\" test [] { "
                "\"Definition\" in \"DefinitionArguments\" \"MultiArgument\" "
                "\"Argument\" \"Operand var\" \"Variable:negative\"\"glob\"\"name\"I\"indexing\"\"subvar\";"
                "\"Definition\" in \"DefinitionArguments\" \"MultiArgument\" "
                "\"Argument\" \"Operand var\" \"Variable:negative\"\"glob\"\"name\"I2\"indexing\"\"subvar\","
                "\"Argument\" \"Operand var\" \"Variable:negative\"\"glob\"\"name\"I3\"indexing\"\"subvar\";"
                "\"NamedDefinition\" \"Variable:negative\"\"glob\"\"name\"O\"indexing\"\"subvar\" is "
                "\"Definition\" additive \"DefinitionArguments\" \"MultiArgument\" "
                "\"Argument\" \"Equation\" \"Expression\" (\"Expression\" "
                "(\"Operand var\" \"Variable:negative\"\"glob\"\"name\"I\"indexing\"\"subvar\" + "
                "\"Operand var\" \"Variable:negative\"\"glob\"\"name\"I2\"indexing\"\"subvar\") + "
                "\"Operand var\" \"Variable:negative\"\"glob\"\"name\"I3\"indexing\"\"subvar\") >= "
                "\"Operand value\" 0.0,\"Argument\" \"Operand value\" 1.0;"
                "\"NamedDefinition\" \"Variable:negative\"\"glob\"\"name\"C\"indexing\"\"subvar\" is "
                "\"Definition\" plus \"DefinitionArguments\" \"MultiArgument\" \"Argument\" "
                "(\"ExposeDefinition\" \"Variable:negative\"\"glob\"\"name\"RES\"indexing\"\"subvar\" from "
                "\"Definition\" plus \"DefinitionArguments\" \"MultiArgument\" "
                "\"Argument\" \"Operand var\" \"Variable:negative\"\"glob\"\"name\"I1\"indexing\"\"subvar\","
                "\"Argument\" \"Operand var\" \"Variable:negative\"\"glob\"\"name\"I2\"indexing\"\"subvar\"),"
                "\"Argument\" \"Operand var\" \"Variable:negative\"\"glob\"\"name\"I3\"indexing\"\"subvar\";"
                "\"NamedDefinition\" \"Variable:negative\"\"glob\"\"name\"C2\"indexing\"\"subvar\" is reduction of "
                "\"Definition\" plus \"DefinitionArguments\" \"MultiArgument\" "
                "\"Reargument\" \"Variable:negative\"\"glob\"\"name\"TT\"indexing\"\"subvar\" as "
                "(\"ExposeDefinition\" \"Variable:negative\"\"glob\"\"name\"RES\"indexing\"\"subvar\" from "
                "\"Definition\" plus \"DefinitionArguments\" \"MultiArgument\" "
                "\"Argument\" \"Operand var\" \"Variable:negative\"\"glob\"\"name\"I1\"indexing\"\"subvar\","
                "\"Argument\" \"Operand var\" \"Variable:negative\"\"glob\"\"name\"I2\"indexing\"\"subvar\"),"
                "\"Argument\" \"Operand var\" \"Variable:negative\"\"glob\"\"name\"I3\"indexing\"\"subvar\";"
                "\"Definition\" sync \"DefinitionArguments\" \"MultiArgument\" "
                "\"Argument\" \"Operand var\" \"Variable:negative\"\"glob\"\"name\"O\"indexing\"\"subvar\";"
                "\"Definition\" out \"DefinitionArguments\" \"MultiArgument\" "
                "\"Argument\" \"Operand var\" \"Variable:negative\"\"glob\"\"name\"O\"indexing\"\"subvar\" "
                "}"],
            'exception': None
        }
        self.do_parsing_tree(t)

    def test_special_name(self):
        t = {
            'name': 'special name',
            'input': """
                test {
                    .O = 1
                }
            """,
            'output': [
                "\"Group\" test [] { "
                "\"NamedDefinition\" \"Variable:negative\"\"glob\".\"name\"O\"indexing\"\"subvar\" is "
                "\"Definition\" additive \"DefinitionArguments\" \"MultiArgument\" \"Argument\" "
                "\"Equation\" \"Operand value\" 0.0 >= \"Operand value\" 0.0,\"Argument\" \"Operand value\" 1.0 "
                "}"],
            'exception': None
        }
        self.do_parsing_tree(t)

    def test_internal_name(self):
        t = {
            'name': 'internal name',
            'input': """
                test {
                    O#delay = 1
                }
            """,
            'output': None,
            'exception': GrammarError
        }
        self.do_parsing_tree(t)

    def test_symbols_complex_name(self):
        t = {
            'name': 'symbols complex name',
            'input': """
                test {
                    B[A].C[X][Y].N = # 1
                }
            """,
            'output': [
                "\"Group\" test [] { "
                "\"NamedDefinition\" "
                "\"Variable:negative\"\"glob\"\"name\"B"
                "\"indexing\"[\"Variable:negative\"\"glob\"\"name\"A\"indexing\"\"subvar\"]\"subvar\"."
                "\"Variable:negative\"\"glob\"\"name\"C"
                "\"indexing\"[\"Variable:negative\"\"glob\"\"name\"X\"indexing\"\"subvar\","
                "\"Variable:negative\"\"glob\"\"name\"Y\"indexing\"\"subvar\"]\"subvar\"."
                "\"Variable:negative\"\"glob\"\"name\"N\"indexing\"\"subvar\" is "
                "\"Definition\" additive \"DefinitionArguments\" \"MultiArgument\" \"Argument\" \"Operand value\" 1.0 "
                "}"],
            'exception': None
        }
        self.do_parsing_tree(t)

    def test_symbols_crappy_complex_name(self):
        t = {
            'name': 'symbols complex name',
            'input': """
                test {
                    B[A]."abc"C[X][Y]"def".N = # 1
                }
            """,
            'output': [
                "\"Group\" test [] { "
                "\"NamedDefinition\" "
                "\"Variable:negative\"\"glob\"\"name\"B"
                "\"indexing\"[\"Variable:negative\"\"glob\"\"name\"A\"indexing\"\"subvar\"]\"subvar\"."
                "\"Variable:negative\"\"glob\"\"name\"C"
                "\"indexing\"[\"Variable:negative\"\"glob\"\"name\"X\"indexing\"\"subvar\","
                "\"Variable:negative\"\"glob\"\"name\"Y\"indexing\"\"subvar\"]\"subvar\"."
                "\"Variable:negative\"\"glob\"\"name\"N\"indexing\"\"subvar\" is "
                "\"Definition\" additive \"DefinitionArguments\" \"MultiArgument\" \"Argument\" \"Operand value\" 1.0 "
                "}"],
            'exception': None
        }
        self.do_parsing_tree(t)

    def test_symbols_with_generator(self):
        t = {
            'name': 'symbols with generator',
            'input': """
                test {
                    in I
                    A = # I>=0, I
                    [A] B[A] = # 1, A
                    [A] out B[A]
                }
            """,
            'output': [
                "\"Group\" test [] { "
                "\"Definition\" in \"DefinitionArguments\" \"MultiArgument\" "
                "\"Argument\" \"Operand var\" \"Variable:negative\"\"glob\"\"name\"I\"indexing\"\"subvar\";"
                "\"NamedDefinition\" \"Variable:negative\"\"glob\"\"name\"A\"indexing\"\"subvar\" is "
                "\"Definition\" additive \"DefinitionArguments\" \"MultiArgument\" \"Argument\" \"Equation\" "
                "\"Operand var\" \"Variable:negative\"\"glob\"\"name\"I\"indexing\"\"subvar\" >= \"Operand value\" 0.0,"
                "\"Argument\" \"Operand var\" \"Variable:negative\"\"glob\"\"name\"I\"indexing\"\"subvar\";"
                "\"TemplateNamedDefinition\" [\"MultiArgument\" "
                "\"Argument\" \"Operand var\" \"Variable:negative\"\"glob\"\"name\"A\"indexing\"\"subvar\"] "
                "\"NamedDefinition\" "
                "\"Variable:negative\"\"glob\"\"name\"B"
                "\"indexing\"[\"Variable:negative\"\"glob\"\"name\"A\"indexing\"\"subvar\"]\"subvar\" is "
                "\"Definition\" additive \"DefinitionArguments\" \"MultiArgument\" \"Argument\" "
                "\"Operand value\" 1.0,"
                "\"Argument\" \"Operand var\" \"Variable:negative\"\"glob\"\"name\"A\"indexing\"\"subvar\";"
                "\"TemplateDefinition\" [\"MultiArgument\" "
                "\"Argument\" \"Operand var\" \"Variable:negative\"\"glob\"\"name\"A\"indexing\"\"subvar\"] "
                "\"Definition\" out \"DefinitionArguments\" \"MultiArgument\" \"Argument\" \"Operand var\" "
                "\"Variable:negative\"\"glob\"\"name\"B"
                "\"indexing\"[\"Variable:negative\"\"glob\"\"name\"A\"indexing\"\"subvar\"]\"subvar\" "
                "}"],
            'exception': None
        }
        self.do_parsing_tree(t)

    def test_symbols_with_big_generator(self):
        t = {
            'name': 'symbols with big generator',
            'input': """
                test {
                    in I, II
                    [I,II] B[I][II] = # 1, 1
                    [I,II] out B[I][II]
                }
            """,
            'output': [
                "\"Group\" test [] { "
                "\"Definition\" in \"DefinitionArguments\" \"MultiArgument\" "
                "\"Argument\" \"Operand var\" \"Variable:negative\"\"glob\"\"name\"I\"indexing\"\"subvar\","
                "\"Argument\" \"Operand var\" \"Variable:negative\"\"glob\"\"name\"II\"indexing\"\"subvar\";"
                "\"TemplateNamedDefinition\" [\"MultiArgument\" "
                "\"Argument\" \"Operand var\" \"Variable:negative\"\"glob\"\"name\"I\"indexing\"\"subvar\","
                "\"Argument\" \"Operand var\" \"Variable:negative\"\"glob\"\"name\"II\"indexing\"\"subvar\"] "
                "\"NamedDefinition\" "
                "\"Variable:negative\"\"glob\"\"name\"B"
                "\"indexing\"[\"Variable:negative\"\"glob\"\"name\"I\"indexing\"\"subvar\","
                "\"Variable:negative\"\"glob\"\"name\"II\"indexing\"\"subvar\"]\"subvar\" is \"Definition\" additive "
                "\"DefinitionArguments\" "
                "\"MultiArgument\" \"Argument\" \"Operand value\" 1.0,\"Argument\" \"Operand value\" 1.0;"
                "\"TemplateDefinition\" [\"MultiArgument\" "
                "\"Argument\" \"Operand var\" \"Variable:negative\"\"glob\"\"name\"I\"indexing\"\"subvar\","
                "\"Argument\" \"Operand var\" \"Variable:negative\"\"glob\"\"name\"II\"indexing\"\"subvar\"] "
                "\"Definition\" out \"DefinitionArguments\" \"MultiArgument\" \"Argument\" \"Operand var\" "
                "\"Variable:negative\"\"glob\"\"name\"B"
                "\"indexing\"[\"Variable:negative\"\"glob\"\"name\"I\"indexing\"\"subvar\","
                "\"Variable:negative\"\"glob\"\"name\"II\"indexing\"\"subvar\"]\"subvar\" "
                "}"],
            'exception': None
        }
        self.do_parsing_tree(t)

    def test_expression(self):
        t = {
            'name': 'expression',
            'input': """
                test {
                    O=I+I2+I3
                }
            """,
            'output': [
                "\"Group\" test [] { "
                "\"NamedDefinition\" \"Variable:negative\"\"glob\"\"name\"O\"indexing\"\"subvar\" is "
                "\"Definition\" additive \"DefinitionArguments\" \"MultiArgument\" "
                "\"Argument\" \"Equation\" \"Operand value\" 0.0 >= \"Operand value\" 0.0,"
                "\"Argument\" \"Expression\" "
                "(\"Expression\" (\"Operand var\" \"Variable:negative\"\"glob\"\"name\"I\"indexing\"\"subvar\" + "
                "\"Operand var\" \"Variable:negative\"\"glob\"\"name\"I2\"indexing\"\"subvar\") + "
                "\"Operand var\" \"Variable:negative\"\"glob\"\"name\"I3\"indexing\"\"subvar\") "
                "}"],
            'exception': None
        }
        self.do_parsing_tree(t)

    def test_no_newline_beginning(self):
        t = {
            'name': 'no newline beginning',
            'input': """network X, Y {
                     V = # 1, X + Y
                     out V
                }""",
            'output': [
                "\"Group\" network [] \"Argument\" \"Operand var\" "
                "\"Variable:negative\"\"glob\"\"name\"X\"indexing\"\"subvar\","
                "\"Argument\" \"Operand var\" \"Variable:negative\"\"glob\"\"name\"Y\"indexing\"\"subvar\" { "
                "\"NamedDefinition\" \"Variable:negative\"\"glob\"\"name\"V\"indexing\"\"subvar\" is "
                "\"Definition\" additive \"DefinitionArguments\" \"MultiArgument\" "
                "\"Argument\" \"Operand value\" 1.0,\"Argument\" "
                "\"Expression\" (\"Operand var\" \"Variable:negative\"\"glob\"\"name\"X\"indexing\"\"subvar\" + "
                "\"Operand var\" \"Variable:negative\"\"glob\"\"name\"Y\"indexing\"\"subvar\");"
                "\"Definition\" out \"DefinitionArguments\" "
                "\"MultiArgument\" \"Argument\" \"Operand var\" "
                "\"Variable:negative\"\"glob\"\"name\"V\"indexing\"\"subvar\" "
                "}"],
            'exception': None
        }
        self.do_parsing_tree(t)

    def test_multiplicator(self):
        t = {
            'name': 'multiplicator',
            'input': """
                test {
                    in I
                    A = & I*2
                    B = # 1, 2 * A
                    out B
                }
            """,
            'output': [
                "\"Group\" test [] { "
                "\"Definition\" in \"DefinitionArguments\" "
                "\"MultiArgument\" \"Argument\" \"Operand var\" "
                "\"Variable:negative\"\"glob\"\"name\"I\"indexing\"\"subvar\";"
                "\"NamedDefinition\" \"Variable:negative\"\"glob\"\"name\"A\"indexing\"\"subvar\" is "
                "\"Definition\" multiplicator "
                "\"DefinitionArguments\" \"MultiArgument\" \"Argument\" \"Expression\" "
                "(\"Operand var\" \"Variable:negative\"\"glob\"\"name\"I\"indexing\"\"subvar\" * "
                "\"Operand value\" 2.0);"
                "\"NamedDefinition\" \"Variable:negative\"\"glob\"\"name\"B\"indexing\"\"subvar\" is "
                "\"Definition\" additive \"DefinitionArguments\" \"MultiArgument\" "
                "\"Argument\" \"Operand value\" 1.0,\"Argument\" "
                "\"Expression\" (\"Operand value\" 2.0 * "
                "\"Operand var\" \"Variable:negative\"\"glob\"\"name\"A\"indexing\"\"subvar\");"
                "\"Definition\" out \"DefinitionArguments\" "
                "\"MultiArgument\" \"Argument\" \"Operand var\" "
                "\"Variable:negative\"\"glob\"\"name\"B\"indexing\"\"subvar\" "
                "}"],
            'exception': None
        }
        self.do_parsing_tree(t)

    def test_newlines(self):
        t = {
            'name': 'newlines',
            'input': """
                test [i=1..2,
                        j=1..2,
                        i<j] {
                    in A,
                        B,
                        I
                    [A,
                        B] C[A][B] =
                            # A*I >= 1, B*I
                    [A, B] R = # C[A,
                            B] +
                            I +
                        B
                    out R
                }
            """,
            'output': [
                "\"Group\" test "
                "[\"Sequence\" i = \"Operand value\" 1.0 .. \"Operand value\" 2.0,"
                "\"Sequence\" j = \"Operand value\" 1.0 .. \"Operand value\" 2.0,"
                "\"Equation\" \"Operand var\" \"Variable:negative\"\"glob\"\"name\"i\"indexing\"\"subvar\" < "
                "\"Operand var\" \"Variable:negative\"\"glob\"\"name\"j\"indexing\"\"subvar\"] { "
                "\"Definition\" in \"DefinitionArguments\" \"MultiArgument\" "
                "\"Argument\" \"Operand var\" \"Variable:negative\"\"glob\"\"name\"A\"indexing\"\"subvar\","
                "\"Argument\" \"Operand var\" \"Variable:negative\"\"glob\"\"name\"B\"indexing\"\"subvar\","
                "\"Argument\" \"Operand var\" \"Variable:negative\"\"glob\"\"name\"I\"indexing\"\"subvar\";"
                "\"TemplateNamedDefinition\" [\"MultiArgument\" "
                "\"Argument\" \"Operand var\" \"Variable:negative\"\"glob\"\"name\"A\"indexing\"\"subvar\","
                "\"Argument\" \"Operand var\" \"Variable:negative\"\"glob\"\"name\"B\"indexing\"\"subvar\"] "
                "\"NamedDefinition\" \"Variable:negative\"\"glob\"\"name\"C\"indexing\"["
                "\"Variable:negative\"\"glob\"\"name\"A\"indexing\"\"subvar\","
                "\"Variable:negative\"\"glob\"\"name\"B\"indexing\"\"subvar\"]\"subvar\" is \"Definition\" additive "
                "\"DefinitionArguments\" \"MultiArgument\" \"Argument\" \"Equation\" "
                "\"Expression\" (\"Operand var\" \"Variable:negative\"\"glob\"\"name\"A\"indexing\"\"subvar\" * "
                "\"Operand var\" \"Variable:negative\"\"glob\"\"name\"I\"indexing\"\"subvar\") >= "
                "\"Operand value\" 1.0,\"Argument\" \"Expression\" ("
                "\"Operand var\" \"Variable:negative\"\"glob\"\"name\"B\"indexing\"\"subvar\" * "
                "\"Operand var\" \"Variable:negative\"\"glob\"\"name\"I\"indexing\"\"subvar\");"
                "\"TemplateNamedDefinition\" [\"MultiArgument\" "
                "\"Argument\" \"Operand var\" \"Variable:negative\"\"glob\"\"name\"A\"indexing\"\"subvar\","
                "\"Argument\" \"Operand var\" \"Variable:negative\"\"glob\"\"name\"B\"indexing\"\"subvar\"] "
                "\"NamedDefinition\" \"Variable:negative\"\"glob\"\"name\"R\"indexing\"\"subvar\" is \"Definition\" "
                "additive \"DefinitionArguments\" \"MultiArgument\" \"Argument\" \"Expression\" (\"Expression\" "
                "(\"Operand var\" \"Variable:negative\"\"glob\"\"name\"C\"indexing\"["
                "\"Variable:negative\"\"glob\"\"name\"A\"indexing\"\"subvar\","
                "\"Variable:negative\"\"glob\"\"name\"B\"indexing\"\"subvar\"]\"subvar\" + "
                "\"Operand var\" \"Variable:negative\"\"glob\"\"name\"I\"indexing\"\"subvar\") + "
                "\"Operand var\" \"Variable:negative\"\"glob\"\"name\"B\"indexing\"\"subvar\");"
                "\"Definition\" out \"DefinitionArguments\" \"MultiArgument\" "
                "\"Argument\" \"Operand var\" \"Variable:negative\"\"glob\"\"name\"R\"indexing\"\"subvar\" "
                "}"],
            'exception': None
        }
        self.do_parsing_tree(t)

    def do_parsing_tree(self, t):
        if t.get('exception') is not None:
            with self.assertRaises(t['exception']):
                GrammarService().parse(t['input'])
        else:
            try:
                res = GrammarService().parse(t['input'])
            except Exception:
                self.fail("Exception in " + t['name'] + ": " + traceback.format_exc())
            self.assertEqual(len(res), len(t['output']),
                             "%s: length %d NOT %d" % (t['name'], len(res), len(t['output'])))
            for r in range(len(t['output'])):
                self.assertEqual(repr(res[r]), t['output'][r],
                                 "%s: %s NOT %s" % (t['name'], repr(res[r]), t['output'][r]))


class GraphTest(DatabaselessTestCase):
    def test_single_expression(self):
        t = {
            'name': 'single expression',
            'prefix': 'test',
            'input': """
                network X, Y, Z {
                    "X * X + 2 * Y - Z"
                    out X * X + 2 * Y - Z
                }
            """,
            'output': "<nodes><node><id>1_0_X_X_2_0_Y_m1_0_Z.#100001.#delay100002.#delay100003.#100004</id><type>neuron</type><x>0</x><y>0</y><output>1</output><links></links></node><node><id>X</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>X_X.#100001</to><activation>1.000000</activation><stream>1.000000</stream></link><link><to>X.#neg</to><stream>-1.000000</stream></link></links></node><node><id>X.#neg</id><type>neuron</type><x>0</x><y>0</y><links><link><to>X_X.#100001</to><activation>1.000000</activation></link></links></node><node><id>X_X.#100001</id><type>multiplicator</type><x>0</x><y>0</y><links><link><to>1_0_X_X_2_0_Y_m1_0_Z.#100001.#delay100002.#delay100003.#100004</to><stream>1.000000</stream></link></links></node><node><id>Y</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>Y.#delay100002</to><stream>1.000000</stream></link></links></node><node><id>Y.#delay100002</id><type>neuron</type><x>0</x><y>0</y><links><link><to>1_0_X_X_2_0_Y_m1_0_Z.#100001.#delay100002.#delay100003.#100004</to><stream>2.000000</stream></link></links></node><node><id>Z</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>Z.#delay100003</to><stream>1.000000</stream></link></links></node><node><id>Z.#delay100003</id><type>neuron</type><x>0</x><y>0</y><links><link><to>1_0_X_X_2_0_Y_m1_0_Z.#100001.#delay100002.#delay100003.#100004</to><stream>-1.000000</stream></link></links></node></nodes>"
        }
        self.do_init(t)
        self.do_graph_builder(t)

    def test_argument_error(self):
        t = {
            'name': 'argument error',
            'prefix': 'test',
            'input': """
                plus A, B {
                    C is additive 0 >= 0, A + B
                    out C
                }
                network X, Y {
                    I = plus A:=X, C:=Y
                    out III.C
                }
            """,
            'output': "",
            'exception': model.GraphError
        }
        self.do_init(t)
        self.do_graph_builder(t)

    def test_simple_expression(self):
        t = {
            'name': 'simple expression',
            'prefix': 'test',
            'input': """
                plus A, B {
                    C is additive 0 >= 0, A + B
                    out C
                }
                minus A, B {
                    C is additive 0 >= 0, A - B
                    out C
                }
                multiply A, B {
                    C is additive 0 >= 0, A * B
                    out C
                }
                network X, Y, Z {
                    "X * X + 2 * Y - Z"
                    I = multiply A:=X, B:=X
                    II = multiply 2, Y
                    Z1 = # Z
                    Z2 = # Z1
                    Z3 = # Z2
                    Z4 = # Z3
                    Z5 = # Z4
                    III = minus (C@plus I, II), Z5
                    III2 = minus (C@F=plus I, II), Z5
                    III3 = minus (F2=plus I, II), Z5
                    III4 = minus (plus I, II), Z5
                    out III.C
                }
            """,
            'output': "<nodes><node><id>F.A</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>F.C</to><stream>1.000000</stream></link></links></node><node><id>F.B</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>F.C</to><stream>1.000000</stream></link></links></node><node><id>F.C</id><type>neuron</type><x>0</x><y>0</y><links><link><to>III2.A</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>F2.A</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>F2.C</to><stream>1.000000</stream></link></links></node><node><id>F2.B</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>F2.C</to><stream>1.000000</stream></link></links></node><node><id>F2.C</id><type>neuron</type><x>0</x><y>0</y><links><link><to>III3.A</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>I.A</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>I.A_B.#100001</to><activation>1.000000</activation></link><link><to>I.A.#neg</to><stream>-1.000000</stream></link></links></node><node><id>I.A.#neg</id><type>neuron</type><x>0</x><y>0</y><links><link><to>I.A_B.#100001</to><activation>1.000000</activation></link></links></node><node><id>I.A_B.#100001</id><type>multiplicator</type><x>0</x><y>0</y><links><link><to>I.C</to><stream>1.000000</stream></link></links></node><node><id>I.B</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>I.A_B.#100001</to><stream>1.000000</stream></link></links></node><node><id>I.C</id><type>neuron</type><x>0</x><y>0</y><links><link><to>exec100002.A</to><activation>1.000000</activation><stream>1.000000</stream></link><link><to>F.A</to><activation>1.000000</activation><stream>1.000000</stream></link><link><to>F2.A</to><activation>1.000000</activation><stream>1.000000</stream></link><link><to>exec100003.A</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>II.A</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>II.A_B.#100001</to><activation>1.000000</activation></link><link><to>II.A.#neg</to><stream>-1.000000</stream></link></links></node><node><id>II.A.#neg</id><type>neuron</type><x>0</x><y>0</y><links><link><to>II.A_B.#100001</to><activation>1.000000</activation></link></links></node><node><id>II.A_B.#100001</id><type>multiplicator</type><x>0</x><y>0</y><links><link><to>II.C</to><stream>1.000000</stream></link></links></node><node><id>II.B</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>II.A_B.#100001</to><stream>1.000000</stream></link></links></node><node><id>II.C</id><type>neuron</type><x>0</x><y>0</y><links><link><to>exec100002.B</to><activation>1.000000</activation><stream>1.000000</stream></link><link><to>F.B</to><activation>1.000000</activation><stream>1.000000</stream></link><link><to>F2.B</to><activation>1.000000</activation><stream>1.000000</stream></link><link><to>exec100003.B</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>III.A</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>III.C</to><stream>1.000000</stream></link></links></node><node><id>III.B</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>III.C</to><stream>-1.000000</stream></link></links></node><node><id>III.C</id><type>neuron</type><x>0</x><y>0</y><output>1</output><links></links></node><node><id>III2.A</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>III2.C</to><stream>1.000000</stream></link></links></node><node><id>III2.B</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>III2.C</to><stream>-1.000000</stream></link></links></node><node><id>III2.C</id><type>neuron</type><x>0</x><y>0</y><links></links></node><node><id>III3.A</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>III3.C</to><stream>1.000000</stream></link></links></node><node><id>III3.B</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>III3.C</to><stream>-1.000000</stream></link></links></node><node><id>III3.C</id><type>neuron</type><x>0</x><y>0</y><links></links></node><node><id>III4.A</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>III4.C</to><stream>1.000000</stream></link></links></node><node><id>III4.B</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>III4.C</to><stream>-1.000000</stream></link></links></node><node><id>III4.C</id><type>neuron</type><x>0</x><y>0</y><links></links></node><node><id>X</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>I.A</to><activation>1.000000</activation><stream>1.000000</stream></link><link><to>I.B</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>Y</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>II.B</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>Z</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>Z1</to><stream>1.000000</stream></link></links></node><node><id>Z1</id><type>neuron</type><x>0</x><y>0</y><links><link><to>Z2</to><stream>1.000000</stream></link></links></node><node><id>Z2</id><type>neuron</type><x>0</x><y>0</y><links><link><to>Z3</to><stream>1.000000</stream></link></links></node><node><id>Z3</id><type>neuron</type><x>0</x><y>0</y><links><link><to>Z4</to><stream>1.000000</stream></link></links></node><node><id>Z4</id><type>neuron</type><x>0</x><y>0</y><links><link><to>Z5</to><stream>1.000000</stream></link></links></node><node><id>Z5</id><type>neuron</type><x>0</x><y>0</y><links><link><to>III.B</to><activation>1.000000</activation><stream>1.000000</stream></link><link><to>III2.B</to><activation>1.000000</activation><stream>1.000000</stream></link><link><to>III3.B</to><activation>1.000000</activation><stream>1.000000</stream></link><link><to>III4.B</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>exec100002.A</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>exec100002.C</to><stream>1.000000</stream></link></links></node><node><id>exec100002.B</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>exec100002.C</to><stream>1.000000</stream></link></links></node><node><id>exec100002.C</id><type>neuron</type><x>0</x><y>0</y><links><link><to>III.A</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>exec100003.A</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>exec100003.C</to><stream>1.000000</stream></link></links></node><node><id>exec100003.B</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>exec100003.C</to><stream>1.000000</stream></link></links></node><node><id>exec100003.C</id><type>neuron</type><x>0</x><y>0</y><links><link><to>III4.A</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>value100001</id><type>neuron</type><x>0</x><y>0</y><constant>2.000000</constant><links><link><to>II.A</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node></nodes>"
        }
        self.do_init(t)
        self.do_graph_builder(t)

    def test_constant(self):
        t = {
            'name': 'unit',
            'prefix': 'test',
            'input': """
                network X {
                    V = # 2 + X - 4 + 8
                    out V
                }
            """,
            'output': "<nodes><node><id>V</id><type>neuron</type><x>0</x><y>0</y><constant>6.000000</constant><output>1</output><links></links></node><node><id>X</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>V</to><stream>1.000000</stream></link></links></node></nodes>"
        }
        self.do_init(t)
        self.do_graph_builder(t)

    def test_unit(self):
        t = {
            'name': 'unit',
            'prefix': 'test',
            'input': """
                network X, Y {
                    V = # 1, X + Y
                    out V
                }
            """,
            'output': "<nodes><node><id>V</id><type>neuron</type><x>0</x><y>0</y><threshold>-1.000000</threshold><output>1</output><links></links></node><node><id>V.#condition_helper1</id><type>neuron</type><x>0</x><y>0</y><links><link><to>V.#condition_helper3</to><activation>-1.000000</activation></link><link><to>V.#condition_helper4</to><activation>1.000000</activation></link></links></node><node><id>V.#condition_helper2</id><type>neuron</type><x>0</x><y>0</y><links><link><to>V.#condition_helper5</to><stream>1.000000</stream></link></links></node><node><id>V.#condition_helper3</id><type>neuron</type><x>0</x><y>0</y><constant>1.000000</constant><links><link><to>V</to><activation>-1.000000</activation></link></links></node><node><id>V.#condition_helper4</id><type>neuron</type><x>0</x><y>0</y><constant>1.000000</constant><links><link><to>V</to><activation>-1.000000</activation></link></links></node><node><id>V.#condition_helper5</id><type>neuron</type><x>0</x><y>0</y><links><link><to>V</to><stream>1.000000</stream></link></links></node><node><id>X</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>V.#condition_helper2</to><stream>1.000000</stream></link></links></node><node><id>Y</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>V.#condition_helper2</to><stream>1.000000</stream></link></links></node><node><id>constant_source#1_0</id><type>neuron</type><x>0</x><y>0</y><constant>1.000000</constant><links><link><to>V.#condition_helper1</to><stream>1.000000</stream></link></links></node></nodes>"
        }
        self.do_init(t)
        self.do_graph_builder(t)

    def test_cyrillic(self):
        t = {
            'name': 'unit',
            'prefix': 'test',
            'input': u"""
                network А, Б {
                    РЕЗ = # А + Б
                    out РЕЗ
                }
            """,
            'output': u"<nodes><node><id>А</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>РЕЗ</to><stream>1.000000</stream></link></links></node><node><id>Б</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>РЕЗ</to><stream>1.000000</stream></link></links></node><node><id>РЕЗ</id><type>neuron</type><x>0</x><y>0</y><output>1</output><links></links></node></nodes>"
        }
        self.do_init(t)
        self.do_graph_builder(t)

    def test_rename(self):
        t = {
            'name': 'rename',
            'prefix': 'test',
            'input': """
                network X, Y {
                    V = # X + Y
                    out RES~V
                }
            """,
            'output': "<nodes><node><id>RES</id><type>neuron</type><x>0</x><y>0</y><output>1</output><links></links></node><node><id>X</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>RES</to><stream>1.000000</stream></link></links></node><node><id>Y</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>RES</to><stream>1.000000</stream></link></links></node></nodes>"
        }
        self.do_init(t)
        self.do_graph_builder(t)

    def test_stack_breaker(self):
        t = {
            'name': 'stack breaker',
            'prefix': 'test',
            'input': """
                g I {
                    .V = # I >= 0, 1
                }
                network X, Y {
                    g X
                    V = # X + Y + .V
                    out V
                }
            """,
            'output': "<nodes><node><id>.V</id><type>neuron</type><x>0</x><y>0</y><constant>1.000000</constant><links><link><to>V</to><stream>1.000000</stream></link></links></node><node><id>V</id><type>neuron</type><x>0</x><y>0</y><output>1</output><links></links></node><node><id>X</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>noresult100001.I</to><activation>1.000000</activation><stream>1.000000</stream></link><link><to>V</to><stream>1.000000</stream></link></links></node><node><id>Y</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>V</to><stream>1.000000</stream></link></links></node><node><id>noresult100001.I</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>.V</to><activation>1.000000</activation></link></links></node></nodes>"
        }
        self.do_init(t)
        self.do_graph_builder(t)

    def test_multiplicated_unit_with_error(self):
        t = {
            'name': 'multiplicated unit with error',
            'prefix': 'test',
            'input': """
                network X, Y {
                    M = & Y * 2
                    V = # 1, X + M
                    out V
                }
            """,
            'output': "",
            'exception': model.GraphError
        }
        self.do_init(t)
        self.do_graph_builder(t)

    def test_multiplicated_unit(self):
        t = {
            'name': 'multiplicated unit',
            'prefix': 'test',
            'input': """
                network X, Y {
                    X1 = # X
                    M = & 0, Y, 2
                    V = X1 + M
                    out V
                }
            """,
            'output': "<nodes><node><id>M</id><type>multiplicator</type><x>0</x><y>0</y><threshold>2.000000</threshold><links><link><to>V</to><stream>1.000000</stream></link></links></node><node><id>V</id><type>neuron</type><x>0</x><y>0</y><output>1</output><links></links></node><node><id>X</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>X1</to><stream>1.000000</stream></link></links></node><node><id>X1</id><type>neuron</type><x>0</x><y>0</y><links><link><to>V</to><stream>1.000000</stream></link></links></node><node><id>Y</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>M</to><stream>1.000000</stream></link></links></node></nodes>"
        }
        self.do_init(t)
        self.do_graph_builder(t)

    def test_multiplicated_modified_unit(self):
        t = {
            'name': 'multiplicated modified unit',
            'prefix': 'test',
            'input': """
                network X, Y, Z {
                    M = & X + Z, Y, 2
                    V = # 2 + M
                    out V
                }
            """,
            'output': "<nodes><node><id>M</id><type>multiplicator</type><x>0</x><y>0</y><threshold>2.000000</threshold><links><link><to>V</to><stream>1.000000</stream></link></links></node><node><id>V</id><type>neuron</type><x>0</x><y>0</y><constant>2.000000</constant><output>1</output><links></links></node><node><id>X</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>M</to><activation>1.000000</activation></link></links></node><node><id>Y</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>M</to><stream>1.000000</stream></link></links></node><node><id>Z</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>M</to><activation>1.000000</activation></link></links></node></nodes>"
        }
        self.do_init(t)
        self.do_graph_builder(t)

    def test_templated_unit(self):
        t = {
            'name': 'templated unit',
            'prefix': 'test',
            'input': """
                network X, Y {
                    [Y] V[Y] = # 1, X + Y
                    [Y] out V[Y]
                }
            """,
            'output': "<nodes><node><id>V[Y]</id><type>neuron</type><x>0</x><y>0</y><threshold>-1.000000</threshold><output>1</output><template>1</template><links></links></node><node><id>V[Y].#condition_helper1</id><type>neuron</type><x>0</x><y>0</y><template>1</template><links><link><to>V[Y].#condition_helper3</to><activation>-1.000000</activation></link><link><to>V[Y].#condition_helper4</to><activation>1.000000</activation></link></links></node><node><id>V[Y].#condition_helper2</id><type>neuron</type><x>0</x><y>0</y><template>1</template><links><link><to>V[Y].#condition_helper5</to><stream>1.000000</stream></link></links></node><node><id>V[Y].#condition_helper3</id><type>neuron</type><x>0</x><y>0</y><constant>1.000000</constant><template>1</template><links><link><to>V[Y]</to><activation>-1.000000</activation></link></links></node><node><id>V[Y].#condition_helper4</id><type>neuron</type><x>0</x><y>0</y><constant>1.000000</constant><template>1</template><links><link><to>V[Y]</to><activation>-1.000000</activation></link></links></node><node><id>V[Y].#condition_helper5</id><type>neuron</type><x>0</x><y>0</y><template>1</template><links><link><to>V[Y]</to><stream>1.000000</stream></link></links></node><node><id>X</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>V[Y].#condition_helper2</to><stream>1.000000</stream></link></links></node><node><id>Y</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>V[Y].#condition_helper1</to><generation>1.000000</generation></link><link><to>constant_source#[Y]</to><generation>1.000000</generation></link><link><to>V[Y].#condition_helper2</to><generation>1.000000</generation></link><link><to>V[Y].#condition_helper3</to><generation>1.000000</generation></link><link><to>V[Y].#condition_helper4</to><generation>1.000000</generation></link><link><to>V[Y].#condition_helper5</to><generation>1.000000</generation></link><link><to>V[Y]</to><generation>1.000000</generation></link></links></node><node><id>constant_source#1_0</id><type>neuron</type><x>0</x><y>0</y><constant>1.000000</constant><links><link><to>V[Y].#condition_helper1</to><stream>1.000000</stream></link></links></node><node><id>constant_source#[Y]</id><type>neuron</type><x>0</x><y>0</y><constant>Y</constant><links><link><to>V[Y].#condition_helper2</to><stream>1.000000</stream></link></links></node></nodes>"
        }
        self.do_init(t)
        self.do_graph_builder(t)

    def test_templated_dynamic_group(self):
        t = {
            'name': 'templated dynamic group',
            'prefix': 'test',
            'input': """
                g [i=I] X, Y {
                    V[i] = # X + Y >= 0, i
                    out V[i]
                }
                network N, X, Y {
                    out (g [I:=N] X, Y)
                }
            """,
            'output': "<nodes><node><id>N</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>exec100001.V[N]</to><generation>1.000000</generation></link></links></node><node><id>X</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>exec100001.X</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>Y</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>exec100001.Y</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>exec100001.V[N]</id><type>neuron</type><x>0</x><y>0</y><constant>N</constant><output>1</output><template>1</template><links></links></node><node><id>exec100001.X</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>exec100001.V[N]</to><activation>1.000000</activation></link></links></node><node><id>exec100001.Y</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>exec100001.V[N]</to><activation>1.000000</activation></link></links></node></nodes>"
        }
        self.do_init(t)
        self.do_graph_builder(t)

    def test_templated_dynamic_group_out(self):
        t = {
            'name': 'templated dynamic group out',
            'prefix': 'test',
            'input': """
                g [i=I] X, Y {
                    V[i] = # X + Y >= 0, i
                    out V[1]
                }
                network N, X, Y {
                    out (g [I:=N] X, Y)
                }
            """,
            'output': "<nodes><node><id>N</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>exec100001.V[N]</to><generation>1.000000</generation></link></links></node><node><id>X</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>exec100001.X</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>Y</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>exec100001.Y</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>exec100001.V[1]</id><type>expected</type><x>0</x><y>0</y><output>1</output><links></links></node><node><id>exec100001.V[N]</id><type>neuron</type><x>0</x><y>0</y><constant>N</constant><template>1</template><links></links></node><node><id>exec100001.X</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>exec100001.V[N]</to><activation>1.000000</activation></link></links></node><node><id>exec100001.Y</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>exec100001.V[N]</to><activation>1.000000</activation></link></links></node></nodes>"
        }
        self.do_init(t)
        self.do_graph_builder(t)

    def test_templated_dynamic_stack(self):
        t = {
            'name': 'templated dynamic stack',
            'prefix': 'test',
            'input': """
                g X, Y {
                    V[Y][N] = # X >= 0, Y
                    out V[Y][N]
                }
                network N, X, Y {
                    RES.N = # N
                    [RES.Y,RES.N] RES = g X, Y
                    out RES
                }
            """,
            'output': "<nodes><node><id>N</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>RES.N</to><stream>1.000000</stream></link></links></node><node><id>RES.N</id><type>neuron</type><x>0</x><y>0</y><links><link><to>RES.V[RES.Y,RES.N]</to><generation>1.000000</generation></link></links></node><node><id>RES.V[RES.Y,RES.N]</id><type>neuron</type><x>0</x><y>0</y><output>1</output><links></links></node><node><id>RES.X</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>RES.V[RES.Y,RES.N]</to><activation>1.000000</activation></link></links></node><node><id>RES.Y</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>RES.V[RES.Y,RES.N]</to><stream>1.000000</stream><generation>1.000000</generation></link></links></node><node><id>X</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>RES.X</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>Y</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>RES.Y</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node></nodes>"
        }
        self.do_init(t)
        self.do_graph_builder(t)

    def test_templated_static_group_error(self):
        t = {
            'name': 'templated static group error',
            'prefix': 'test',
            'input': """
                g [i=1..I] X, Y {
                    V[i] = # X + Y >= 0, i
                    out V[i]
                }
                network N, X, Y {
                    out (g [I:=N] X, Y)
                }
            """,
            'output': "",
            'exception': model.GraphError
        }
        self.do_init(t)
        self.do_graph_builder(t)

    def test_templated_static_group(self):
        t = {
            'name': 'templated static group',
            'prefix': 'test',
            'input': """
                g [i=1..I] X, Y {
                    V[i] = # X + Y >= 0, i
                    out V[i]
                }
                network X, Y {
                    out (V[1] @ g [I:=3] X, Y)
                }
            """,
            'output': "<nodes><node><id>X</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>exec100001.X</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>Y</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>exec100001.Y</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>exec100001.V[1]</id><type>neuron</type><x>0</x><y>0</y><constant>1.000000</constant><output>1</output><links></links></node><node><id>exec100001.V[2]</id><type>neuron</type><x>0</x><y>0</y><constant>2.000000</constant><links></links></node><node><id>exec100001.V[3]</id><type>neuron</type><x>0</x><y>0</y><constant>3.000000</constant><links></links></node><node><id>exec100001.X</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>exec100001.V[1]</to><activation>1.000000</activation></link><link><to>exec100001.V[2]</to><activation>1.000000</activation></link><link><to>exec100001.V[3]</to><activation>1.000000</activation></link></links></node><node><id>exec100001.Y</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>exec100001.V[1]</to><activation>1.000000</activation></link><link><to>exec100001.V[2]</to><activation>1.000000</activation></link><link><to>exec100001.V[3]</to><activation>1.000000</activation></link></links></node></nodes>",
        }
        self.do_init(t)
        self.do_graph_builder(t)

    def test_templated_big_static_group(self):
        t = {
            'name': 'templated big static group',
            'prefix': 'test',
            'input': """
                g [i=1..I, k=2..I] X, Y {
                    in N[i][k]
                    V[i] = # X + Y >= 0, i
                    B[i] = # N[i][k] >= 0, i
                    C =+ # V[i]
                    out V[i], B[i], C
                }
                network X, Y {
                    out (C@g [I:=3] X, Y)
                }
            """,
            'output': "<nodes><node><id>N[1,2]</id><type>expected</type><x>0</x><y>0</y><links><link><to>exec100001.N[1,2]</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>N[1,3]</id><type>expected</type><x>0</x><y>0</y><links><link><to>exec100001.N[1,3]</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>N[2,2]</id><type>expected</type><x>0</x><y>0</y><links><link><to>exec100001.N[2,2]</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>N[2,3]</id><type>expected</type><x>0</x><y>0</y><links><link><to>exec100001.N[2,3]</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>N[3,2]</id><type>expected</type><x>0</x><y>0</y><links><link><to>exec100001.N[3,2]</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>N[3,3]</id><type>expected</type><x>0</x><y>0</y><links><link><to>exec100001.N[3,3]</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>X</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>exec100001.X</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>Y</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>exec100001.Y</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>exec100001.B[1]</id><type>neuron</type><x>0</x><y>0</y><constant>1.000000</constant><links></links></node><node><id>exec100001.B[2]</id><type>neuron</type><x>0</x><y>0</y><constant>2.000000</constant><links></links></node><node><id>exec100001.B[3]</id><type>neuron</type><x>0</x><y>0</y><constant>3.000000</constant><links></links></node><node><id>exec100001.C</id><type>neuron</type><x>0</x><y>0</y><output>1</output><links></links></node><node><id>exec100001.N[1,2]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>exec100001.B[1]</to><activation>1.000000</activation></link></links></node><node><id>exec100001.N[1,3]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>exec100001.B[1]</to><activation>1.000000</activation></link></links></node><node><id>exec100001.N[2,2]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>exec100001.B[2]</to><activation>1.000000</activation></link></links></node><node><id>exec100001.N[2,3]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>exec100001.B[2]</to><activation>1.000000</activation></link></links></node><node><id>exec100001.N[3,2]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>exec100001.B[3]</to><activation>1.000000</activation></link></links></node><node><id>exec100001.N[3,3]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>exec100001.B[3]</to><activation>1.000000</activation></link></links></node><node><id>exec100001.V[1]</id><type>neuron</type><x>0</x><y>0</y><constant>1.000000</constant><links><link><to>exec100001.C</to><stream>1.000000</stream></link></links></node><node><id>exec100001.V[2]</id><type>neuron</type><x>0</x><y>0</y><constant>2.000000</constant><links><link><to>exec100001.C</to><stream>1.000000</stream></link></links></node><node><id>exec100001.V[3]</id><type>neuron</type><x>0</x><y>0</y><constant>3.000000</constant><links><link><to>exec100001.C</to><stream>1.000000</stream></link></links></node><node><id>exec100001.X</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>exec100001.V[1]</to><activation>1.000000</activation></link><link><to>exec100001.V[2]</to><activation>1.000000</activation></link><link><to>exec100001.V[3]</to><activation>1.000000</activation></link></links></node><node><id>exec100001.Y</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>exec100001.V[1]</to><activation>1.000000</activation></link><link><to>exec100001.V[2]</to><activation>1.000000</activation></link><link><to>exec100001.V[3]</to><activation>1.000000</activation></link></links></node></nodes>",
        }
        self.do_init(t)
        self.do_graph_builder(t)

    def test_templated_global_static_group(self):
        t = {
            'name': 'templated global static group',
            'prefix': 'test',
            'input': """
                [I=3]

                g [i=1..I, k=2..I] X, Y {
                    in N[i][k]
                    V[i] = # X + Y >= 0, i
                    B[i] = # N[i][k] >= 0, i
                    C =+ # V[i]
                    out V[i], B[i], C
                }
                network X, Y {
                    out (C@g X, Y)
                }
            """,
            'output': "<nodes><node><id>N[1,2]</id><type>expected</type><x>0</x><y>0</y><links><link><to>exec100001.N[1,2]</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>N[1,3]</id><type>expected</type><x>0</x><y>0</y><links><link><to>exec100001.N[1,3]</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>N[2,2]</id><type>expected</type><x>0</x><y>0</y><links><link><to>exec100001.N[2,2]</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>N[2,3]</id><type>expected</type><x>0</x><y>0</y><links><link><to>exec100001.N[2,3]</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>N[3,2]</id><type>expected</type><x>0</x><y>0</y><links><link><to>exec100001.N[3,2]</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>N[3,3]</id><type>expected</type><x>0</x><y>0</y><links><link><to>exec100001.N[3,3]</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>X</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>exec100001.X</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>Y</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>exec100001.Y</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>exec100001.B[1]</id><type>neuron</type><x>0</x><y>0</y><constant>1.000000</constant><links></links></node><node><id>exec100001.B[2]</id><type>neuron</type><x>0</x><y>0</y><constant>2.000000</constant><links></links></node><node><id>exec100001.B[3]</id><type>neuron</type><x>0</x><y>0</y><constant>3.000000</constant><links></links></node><node><id>exec100001.C</id><type>neuron</type><x>0</x><y>0</y><output>1</output><links></links></node><node><id>exec100001.N[1,2]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>exec100001.B[1]</to><activation>1.000000</activation></link></links></node><node><id>exec100001.N[1,3]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>exec100001.B[1]</to><activation>1.000000</activation></link></links></node><node><id>exec100001.N[2,2]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>exec100001.B[2]</to><activation>1.000000</activation></link></links></node><node><id>exec100001.N[2,3]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>exec100001.B[2]</to><activation>1.000000</activation></link></links></node><node><id>exec100001.N[3,2]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>exec100001.B[3]</to><activation>1.000000</activation></link></links></node><node><id>exec100001.N[3,3]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>exec100001.B[3]</to><activation>1.000000</activation></link></links></node><node><id>exec100001.V[1]</id><type>neuron</type><x>0</x><y>0</y><constant>1.000000</constant><links><link><to>exec100001.C</to><stream>1.000000</stream></link></links></node><node><id>exec100001.V[2]</id><type>neuron</type><x>0</x><y>0</y><constant>2.000000</constant><links><link><to>exec100001.C</to><stream>1.000000</stream></link></links></node><node><id>exec100001.V[3]</id><type>neuron</type><x>0</x><y>0</y><constant>3.000000</constant><links><link><to>exec100001.C</to><stream>1.000000</stream></link></links></node><node><id>exec100001.X</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>exec100001.V[1]</to><activation>1.000000</activation></link><link><to>exec100001.V[2]</to><activation>1.000000</activation></link><link><to>exec100001.V[3]</to><activation>1.000000</activation></link></links></node><node><id>exec100001.Y</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>exec100001.V[1]</to><activation>1.000000</activation></link><link><to>exec100001.V[2]</to><activation>1.000000</activation></link><link><to>exec100001.V[3]</to><activation>1.000000</activation></link></links></node></nodes>",
        }
        self.do_init(t)
        self.do_graph_builder(t)

    def test_templated_not_variable_static_group(self):
        t = {
            'name': 'templated not variable static group',
            'prefix': 'test',
            'input': """
                g [i=1..3, k=2..3] X, Y {
                    in N[i][k]
                    V[i] = # X + Y >= 0, i
                    B[i] = # N[i][k] >= 0, i
                    C =+ # V[i]
                    out V[i], B[i], C
                }
                network X, Y {
                    out (C@g X, Y)
                }
            """,
            'output': "<nodes><node><id>N[1,2]</id><type>expected</type><x>0</x><y>0</y><links><link><to>exec100001.N[1,2]</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>N[1,3]</id><type>expected</type><x>0</x><y>0</y><links><link><to>exec100001.N[1,3]</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>N[2,2]</id><type>expected</type><x>0</x><y>0</y><links><link><to>exec100001.N[2,2]</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>N[2,3]</id><type>expected</type><x>0</x><y>0</y><links><link><to>exec100001.N[2,3]</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>N[3,2]</id><type>expected</type><x>0</x><y>0</y><links><link><to>exec100001.N[3,2]</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>N[3,3]</id><type>expected</type><x>0</x><y>0</y><links><link><to>exec100001.N[3,3]</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>X</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>exec100001.X</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>Y</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>exec100001.Y</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>exec100001.B[1]</id><type>neuron</type><x>0</x><y>0</y><constant>1.000000</constant><links></links></node><node><id>exec100001.B[2]</id><type>neuron</type><x>0</x><y>0</y><constant>2.000000</constant><links></links></node><node><id>exec100001.B[3]</id><type>neuron</type><x>0</x><y>0</y><constant>3.000000</constant><links></links></node><node><id>exec100001.C</id><type>neuron</type><x>0</x><y>0</y><output>1</output><links></links></node><node><id>exec100001.N[1,2]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>exec100001.B[1]</to><activation>1.000000</activation></link></links></node><node><id>exec100001.N[1,3]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>exec100001.B[1]</to><activation>1.000000</activation></link></links></node><node><id>exec100001.N[2,2]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>exec100001.B[2]</to><activation>1.000000</activation></link></links></node><node><id>exec100001.N[2,3]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>exec100001.B[2]</to><activation>1.000000</activation></link></links></node><node><id>exec100001.N[3,2]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>exec100001.B[3]</to><activation>1.000000</activation></link></links></node><node><id>exec100001.N[3,3]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>exec100001.B[3]</to><activation>1.000000</activation></link></links></node><node><id>exec100001.V[1]</id><type>neuron</type><x>0</x><y>0</y><constant>1.000000</constant><links><link><to>exec100001.C</to><stream>1.000000</stream></link></links></node><node><id>exec100001.V[2]</id><type>neuron</type><x>0</x><y>0</y><constant>2.000000</constant><links><link><to>exec100001.C</to><stream>1.000000</stream></link></links></node><node><id>exec100001.V[3]</id><type>neuron</type><x>0</x><y>0</y><constant>3.000000</constant><links><link><to>exec100001.C</to><stream>1.000000</stream></link></links></node><node><id>exec100001.X</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>exec100001.V[1]</to><activation>1.000000</activation></link><link><to>exec100001.V[2]</to><activation>1.000000</activation></link><link><to>exec100001.V[3]</to><activation>1.000000</activation></link></links></node><node><id>exec100001.Y</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>exec100001.V[1]</to><activation>1.000000</activation></link><link><to>exec100001.V[2]</to><activation>1.000000</activation></link><link><to>exec100001.V[3]</to><activation>1.000000</activation></link></links></node></nodes>",
        }
        self.do_init(t)
        self.do_graph_builder(t)

    def test_templated_conditional_static_group(self):
        t = {
            'name': 'templated conditional static group',
            'prefix': 'test',
            'input': """
                g [i=1..4, k=2..4, i<4, k<4] X, Y {
                    in N[i][k]
                    V[i] = # X + Y >= 0, i
                    B[i] = # N[i][k] >= 0, i
                    C =+ # V[i]
                    out V[i], B[i], C
                }
                network X, Y {
                    out (C@g X, Y)
                }
            """,
            'output': "<nodes><node><id>N[1,2]</id><type>expected</type><x>0</x><y>0</y><links><link><to>exec100001.N[1,2]</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>N[1,3]</id><type>expected</type><x>0</x><y>0</y><links><link><to>exec100001.N[1,3]</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>N[2,2]</id><type>expected</type><x>0</x><y>0</y><links><link><to>exec100001.N[2,2]</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>N[2,3]</id><type>expected</type><x>0</x><y>0</y><links><link><to>exec100001.N[2,3]</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>N[3,2]</id><type>expected</type><x>0</x><y>0</y><links><link><to>exec100001.N[3,2]</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>N[3,3]</id><type>expected</type><x>0</x><y>0</y><links><link><to>exec100001.N[3,3]</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>X</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>exec100001.X</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>Y</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>exec100001.Y</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>exec100001.B[1]</id><type>neuron</type><x>0</x><y>0</y><constant>1.000000</constant><links></links></node><node><id>exec100001.B[2]</id><type>neuron</type><x>0</x><y>0</y><constant>2.000000</constant><links></links></node><node><id>exec100001.B[3]</id><type>neuron</type><x>0</x><y>0</y><constant>3.000000</constant><links></links></node><node><id>exec100001.C</id><type>neuron</type><x>0</x><y>0</y><output>1</output><links></links></node><node><id>exec100001.N[1,2]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>exec100001.B[1]</to><activation>1.000000</activation></link></links></node><node><id>exec100001.N[1,3]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>exec100001.B[1]</to><activation>1.000000</activation></link></links></node><node><id>exec100001.N[2,2]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>exec100001.B[2]</to><activation>1.000000</activation></link></links></node><node><id>exec100001.N[2,3]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>exec100001.B[2]</to><activation>1.000000</activation></link></links></node><node><id>exec100001.N[3,2]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>exec100001.B[3]</to><activation>1.000000</activation></link></links></node><node><id>exec100001.N[3,3]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>exec100001.B[3]</to><activation>1.000000</activation></link></links></node><node><id>exec100001.V[1]</id><type>neuron</type><x>0</x><y>0</y><constant>1.000000</constant><links><link><to>exec100001.C</to><stream>1.000000</stream></link></links></node><node><id>exec100001.V[2]</id><type>neuron</type><x>0</x><y>0</y><constant>2.000000</constant><links><link><to>exec100001.C</to><stream>1.000000</stream></link></links></node><node><id>exec100001.V[3]</id><type>neuron</type><x>0</x><y>0</y><constant>3.000000</constant><links><link><to>exec100001.C</to><stream>1.000000</stream></link></links></node><node><id>exec100001.X</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>exec100001.V[1]</to><activation>1.000000</activation></link><link><to>exec100001.V[2]</to><activation>1.000000</activation></link><link><to>exec100001.V[3]</to><activation>1.000000</activation></link></links></node><node><id>exec100001.Y</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>exec100001.V[1]</to><activation>1.000000</activation></link><link><to>exec100001.V[2]</to><activation>1.000000</activation></link><link><to>exec100001.V[3]</to><activation>1.000000</activation></link></links></node></nodes>",
        }
        self.do_init(t)
        self.do_graph_builder(t)

    def test_templated_linked_conditional_static_group(self):
        t = {
            'name': 'templated linked conditional static group',
            'prefix': 'test',
            'input': """
                network [iii=1..3, ii=1..3, i=2..3, ii==i-1] INP {
                    N[1] = # INP
                    N[i] = # N[ii]>=1, 1
                    O = # N[iii]
                    out O
                }
            """,
            'output': "<nodes><node><id>INP</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>N[1]</to><stream>1.000000</stream></link></links></node><node><id>N[1]</id><type>neuron</type><x>0</x><y>0</y><links><link><to>N[2]</to><activation>1.000000</activation></link><link><to>O</to><stream>1.000000</stream></link></links></node><node><id>N[2]</id><type>neuron</type><x>0</x><y>0</y><threshold>1.000000</threshold><constant>1.000000</constant><links><link><to>N[3]</to><activation>1.000000</activation></link><link><to>O</to><stream>1.000000</stream></link></links></node><node><id>N[3]</id><type>neuron</type><x>0</x><y>0</y><threshold>1.000000</threshold><constant>1.000000</constant><links><link><to>O</to><stream>1.000000</stream></link></links></node><node><id>O</id><type>neuron</type><x>0</x><y>0</y><output>1</output><links></links></node></nodes>",
        }
        self.do_init(t)
        self.do_graph_builder(t)

    def test_templated_not_implemented_conditional(self):
        t = {
            'name': 'templated not implemented conditional',
            'prefix': 'test',
            'input': """
                g [k=G, k<4] X, Y {
                    C = X + Y
                    out C
                }
                network X, Y {
                    out (C@g [G:=X] X, Y)
                }
            """,
            'output': "",
            'exception': model.GraphError
        }
        self.do_init(t)
        self.do_graph_builder(t)

    def test_templated_not_implemented_conditional_error(self):
        t = {
            'name': 'templated not implemented conditional error',
            'prefix': 'test',
            'input': """
                g [k=G, k<4] X, Y {
                    C[k] = X + Y
                    out C[k]
                }
                network X, Y {
                    out (C@g [G:=X] X, Y)
                }
            """,
            'output': "",
            'exception': model.GraphError
        }
        self.do_init(t)
        self.do_graph_builder(t)

    def test_templated_not_implemented_conditional_mix(self):
        t = {
            'name': 'templated not implemented conditional mix',
            'prefix': 'test',
            'input': """
                g [i=1..4, k=G, i<k] X, Y {
                    C = X + Y
                    out C
                }
                network X, Y {
                    out (C@g [G:=V] X, Y)
                }
            """,
            'output': "",
            'exception': model.GraphError
        }
        self.do_init(t)
        self.do_graph_builder(t)

    def test_templated_not_implemented_conditional_mix_error(self):
        t = {
            'name': 'templated not implemented conditional mix error',
            'prefix': 'test',
            'input': """
                g [i=1..4, k=G, i<k] X, Y {
                    C[i][k] = X + Y
                    out Y
                }
                network X, Y {
                    out (g [G:=X] X, Y)
                }
            """,
            'output': "",
            'exception': model.GraphError
        }
        self.do_init(t)
        self.do_graph_builder(t)

    def test_templated_renamed_static_group(self):
        t = {
            'name': 'templated renamed static group',
            'prefix': 'test',
            'input': """
                g [i=1..3, k=2..3, N, V] X, Y {
                    in N[i][k]
                    V[i] = # X + Y >= 0, i
                    B[i] = # N[i][k] >= 0, i
                    C =+ # V[i]
                    out V[i], B[i], C
                }
                network X, Y {
                    out (C@g [N:=G, V:=K] X, Y)
                }
            """,
            'output': "<nodes><node><id>G[1,2]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>exec100001.B[1]</to><activation>1.000000</activation></link></links></node><node><id>G[1,3]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>exec100001.B[1]</to><activation>1.000000</activation></link></links></node><node><id>G[2,2]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>exec100001.B[2]</to><activation>1.000000</activation></link></links></node><node><id>G[2,3]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>exec100001.B[2]</to><activation>1.000000</activation></link></links></node><node><id>G[3,2]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>exec100001.B[3]</to><activation>1.000000</activation></link></links></node><node><id>G[3,3]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>exec100001.B[3]</to><activation>1.000000</activation></link></links></node><node><id>K[1]</id><type>neuron</type><x>0</x><y>0</y><constant>1.000000</constant><links><link><to>exec100001.C</to><stream>1.000000</stream></link></links></node><node><id>K[2]</id><type>neuron</type><x>0</x><y>0</y><constant>2.000000</constant><links><link><to>exec100001.C</to><stream>1.000000</stream></link></links></node><node><id>K[3]</id><type>neuron</type><x>0</x><y>0</y><constant>3.000000</constant><links><link><to>exec100001.C</to><stream>1.000000</stream></link></links></node><node><id>X</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>exec100001.X</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>Y</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>exec100001.Y</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>exec100001.B[1]</id><type>neuron</type><x>0</x><y>0</y><constant>1.000000</constant><links></links></node><node><id>exec100001.B[2]</id><type>neuron</type><x>0</x><y>0</y><constant>2.000000</constant><links></links></node><node><id>exec100001.B[3]</id><type>neuron</type><x>0</x><y>0</y><constant>3.000000</constant><links></links></node><node><id>exec100001.C</id><type>neuron</type><x>0</x><y>0</y><output>1</output><links></links></node><node><id>exec100001.X</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>K[1]</to><activation>1.000000</activation></link><link><to>K[2]</to><activation>1.000000</activation></link><link><to>K[3]</to><activation>1.000000</activation></link></links></node><node><id>exec100001.Y</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>K[1]</to><activation>1.000000</activation></link><link><to>K[2]</to><activation>1.000000</activation></link><link><to>K[3]</to><activation>1.000000</activation></link></links></node></nodes>",
        }
        self.do_init(t)
        self.do_graph_builder(t)

    def test_templated_array_static_group(self):
        t = {
            'name': 'templated array static group',
            'prefix': 'test',
            'input': """
                sum [i=1..I, N] {
                    in N[i]
                    C = # N[i]
                    out C
                }
                network X[1], X[2], X[3] {
                    out (C@sum [N:=X, I:=3])
                }
            """,
            'output': "<nodes><node><id>X[1]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>exec100001.C</to><stream>1.000000</stream></link></links></node><node><id>X[2]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>exec100001.C</to><stream>1.000000</stream></link></links></node><node><id>X[3]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>exec100001.C</to><stream>1.000000</stream></link></links></node><node><id>exec100001.C</id><type>neuron</type><x>0</x><y>0</y><output>1</output><links></links></node></nodes>",
        }
        self.do_init(t)
        self.do_graph_builder(t)

    def test_simple_reduction(self):
        t = {
            'name': 'simple reduction',
            'prefix': 'test',
            'input': """
                network X, Y {
                    I =+ # X
                    I =+ # Y
                    out I
                }
            """,
            'output': "<nodes><node><id>I</id><type>neuron</type><x>0</x><y>0</y><output>1</output><links></links></node><node><id>X</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>I</to><stream>1.000000</stream></link></links></node><node><id>Y</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>I</to><stream>1.000000</stream></link></links></node></nodes>",
        }
        self.do_init(t)
        self.do_graph_builder(t)

    def test_conflict_reduction(self):
        t = {
            'name': 'conflict reduction',
            'prefix': 'test',
            'input': """
                network X, Y {
                    I =+ # X >= 0, X
                    I =+ # Y >= 0, Y
                    out I
                }
            """,
            'output': "<nodes><node><id>I</id><type>neuron</type><x>0</x><y>0</y><output>1</output><links></links></node><node><id>I.#reduction100001</id><type>neuron</type><x>0</x><y>0</y><links><link><to>I</to><stream>1.000000</stream></link></links></node><node><id>I.#reduction100002</id><type>neuron</type><x>0</x><y>0</y><links><link><to>I</to><stream>1.000000</stream></link></links></node><node><id>X</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>I.#reduction100001</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>Y</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>I.#reduction100002</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node></nodes>",
        }
        self.do_init(t)
        self.do_graph_builder(t)

    def test_deep_reduction(self):
        t = {
            'name': 'deep reduction',
            'prefix': 'test',
            'input': """
                g X {
                    Y = X + P
                    P =+ # 2
                    out Y
                }
                network X {
                    R = g X
                    R.P =+ # X
                    out R
                }
            """,
            'output': "<nodes><node><id>R.P</id><type>neuron</type><x>0</x><y>0</y><constant>2.000000</constant><links><link><to>R.Y</to><stream>1.000000</stream></link></links></node><node><id>R.X</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>R.Y</to><stream>1.000000</stream></link></links></node><node><id>R.Y</id><type>neuron</type><x>0</x><y>0</y><output>1</output><links></links></node><node><id>X</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>R.X</to><activation>1.000000</activation><stream>1.000000</stream></link><link><to>R.P</to><stream>1.000000</stream></link></links></node></nodes>",
        }
        self.do_init(t)
        self.do_graph_builder(t)

    def test_deep_conflict_reduction(self):
        t = {
            'name': 'deep conflict reduction',
            'prefix': 'test',
            'input': """
                g X {
                    Y = X + P
                    C = # 2
                    P =+ # C
                    out Y
                }
                network X {
                    R = g X
                    R.P =+ # X + 4 * R.C
                    out R
                }
            """,
            'output': "<nodes><node><id>R.C</id><type>neuron</type><x>0</x><y>0</y><constant>2.000000</constant><links><link><to>R.P</to><stream>5.000000</stream></link></links></node><node><id>R.P</id><type>neuron</type><x>0</x><y>0</y><links><link><to>R.Y</to><stream>1.000000</stream></link></links></node><node><id>R.X</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>R.Y</to><stream>1.000000</stream></link></links></node><node><id>R.Y</id><type>neuron</type><x>0</x><y>0</y><output>1</output><links></links></node><node><id>X</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>R.X</to><activation>1.000000</activation><stream>1.000000</stream></link><link><to>R.P</to><stream>1.000000</stream></link></links></node></nodes>",
        }
        self.do_init(t)
        self.do_graph_builder(t)

    def test_reduction_error(self):
        t = {
            'name': 'reduction error',
            'prefix': 'test',
            'input': """
                network X, Y {
                    I = # X >= 0, X
                    I = # Y >= 0, Y
                    out I
                }
            """,
            'output': "",
            'exception': model.GraphError
        }
        self.do_init(t)
        self.do_graph_builder(t)

    def test_reduction_mix_error(self):
        t = {
            'name': 'reduction mix error',
            'prefix': 'test',
            'input': """
                network X, Y {
                    I = # X >= 0, X
                    I =+ # Y >= 0, Y
                    out I
                }
            """,
            'output': "",
            'exception': model.GraphError
        }
        self.do_init(t)
        self.do_graph_builder(t)

    def test_indirect_input(self):
        t = {
            'name': 'indirect input',
            'prefix': 'test',
            'input': """
                sum A, B {
                    out A + B
                }
                network A, B {
                    out (sum)
                }
            """,
            'output': "<nodes><node><id>A</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>exec100002.A</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>B</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>exec100002.B</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>exec100002.1_0_A_1_0_B.#100001</id><type>neuron</type><x>0</x><y>0</y><output>1</output><links></links></node><node><id>exec100002.A</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>exec100002.1_0_A_1_0_B.#100001</to><stream>1.000000</stream></link></links></node><node><id>exec100002.B</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>exec100002.1_0_A_1_0_B.#100001</to><stream>1.000000</stream></link></links></node></nodes>",
        }
        self.do_init(t)
        self.do_graph_builder(t)

    def test_dotted_group(self):
        t = {
            'name': 'dotted group',
            'prefix': 'test',
            'input': """
                library.sum A, B {
                    out A + B
                }
                network A, B {
                    out (library.sum)
                }
            """,
            'output': "<nodes><node><id>A</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>exec100002.A</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>B</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>exec100002.B</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>exec100002.1_0_A_1_0_B.#100001</id><type>neuron</type><x>0</x><y>0</y><output>1</output><links></links></node><node><id>exec100002.A</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>exec100002.1_0_A_1_0_B.#100001</to><stream>1.000000</stream></link></links></node><node><id>exec100002.B</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>exec100002.1_0_A_1_0_B.#100001</to><stream>1.000000</stream></link></links></node></nodes>",
        }
        self.do_init(t)
        self.do_graph_builder(t)

    def test_library_group(self):
        t = {
            'name': 'library group',
            'prefix': 'test',
            'input': """
                network A, B {
                    out (sgl.sum)
                }
            """,
            'libraries': {
                'sgl': ('somefilepath', "sum A, B { out A + B }")
            },
            'output': "<nodes><node><id>A</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>exec100001.A</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>B</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>exec100001.B</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>exec100001.1_0_A_1_0_B.#100001</id><type>neuron</type><x>0</x><y>0</y><output>1</output><links></links></node><node><id>exec100001.A</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>exec100001.1_0_A_1_0_B.#100001</to><stream>1.000000</stream></link></links></node><node><id>exec100001.B</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>exec100001.1_0_A_1_0_B.#100001</to><stream>1.000000</stream></link></links></node></nodes>",
        }
        self.do_init(t)
        self.do_graph_builder(t)

    def test_missing_library(self):
        t = {
            'name': 'missing library',
            'prefix': 'test',
            'input': """
                network A, B {
                    out (anotherlib.sum)
                }
            """,
            'libraries': {
                'sgl': ('somefilepath', "sum A, B { out A + B }")
            },
            'output': "",
            'exception': PackageError
        }
        self.do_init(t)
        self.do_graph_builder(t)

    def test_bug_extra_links(self):
        t = {
            'name': 'bug extra links',
            'prefix': 'test',
            'input': """
                network I[1], I[2] {
                    M[1] = # I[1] - I[2] >= 0, I[1]
                }
            """,
            'output': "<nodes><node><id>I[1]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>M[1]</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>I[2]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>M[1]</to><activation>-1.000000</activation></link></links></node><node><id>M[1]</id><type>neuron</type><x>0</x><y>0</y><links></links></node></nodes>",
        }
        self.do_init(t)
        self.do_graph_builder(t)

    def test_bug_not_generated_network(self):
        t = {
            'name': 'bug not generated network',
            'prefix': 'test',
            'input': """
                network [r=1..3] {
                    in I[r]
                    O[r] = 2*I[r]
                    out O[r]
                }
            """,
            'output': "<nodes><node><id>I[1]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>O[1]</to><stream>2.000000</stream></link></links></node><node><id>I[2]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>O[2]</to><stream>2.000000</stream></link></links></node><node><id>I[3]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>O[3]</to><stream>2.000000</stream></link></links></node><node><id>O[1]</id><type>neuron</type><x>0</x><y>0</y><output>1</output><links></links></node><node><id>O[2]</id><type>neuron</type><x>0</x><y>0</y><output>1</output><links></links></node><node><id>O[3]</id><type>neuron</type><x>0</x><y>0</y><output>1</output><links></links></node></nodes>"
        }
        self.do_init(t)
        self.do_graph_builder(t)

    def test_bug_generated_arguments_not_counted(self):
        t = {
            'name': 'bug generated arguments not counted',
            'prefix': 'test',
            'input': """
                gr [r=1..3] I[r] {
                    O[r] = 2*I[r]
                    out O[r]
                }
                network {
                    in I[1],I[2],I[3]
                    O = gr I[1],I[2],I[3]
                    out O.O[1]
                    out O.O[2]
                    out O.O[3]
                }
            """,
            'output': "<nodes><node><id>I[1]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>O.I[1]</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>I[2]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>O.I[2]</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>I[3]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>O.I[3]</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>O.I[1]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>O.O[1]</to><stream>2.000000</stream></link></links></node><node><id>O.I[2]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>O.O[2]</to><stream>2.000000</stream></link></links></node><node><id>O.I[3]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>O.O[3]</to><stream>2.000000</stream></link></links></node><node><id>O.O[1]</id><type>neuron</type><x>0</x><y>0</y><output>1</output><links></links></node><node><id>O.O[2]</id><type>neuron</type><x>0</x><y>0</y><output>1</output><links></links></node><node><id>O.O[3]</id><type>neuron</type><x>0</x><y>0</y><output>1</output><links></links></node></nodes>"
        }
        self.do_init(t)
        self.do_graph_builder(t)

    def test_bug_growing_links_not_appended(self):
        t = {
            'name': 'bug growing links not appended',
            'prefix': 'test',
            'input': """
                gr [r=1..3,I] {
                    O[r] = 2*I[r]
                    out O[r]
                }
                network {
                    in I[1],I[2],I[3]
                    O = gr
                    out O.O[1]
                    out O.O[2]
                    out O.O[3]
                }
            """,
            'output': "<nodes><node><id>I[1]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links></links></node><node><id>I[2]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links></links></node><node><id>I[3]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links></links></node><node><id>O.I[1]</id><type>expected</type><x>0</x><y>0</y><links><link><to>O.O[1]</to><stream>2.000000</stream></link></links></node><node><id>O.I[2]</id><type>expected</type><x>0</x><y>0</y><links><link><to>O.O[2]</to><stream>2.000000</stream></link></links></node><node><id>O.I[3]</id><type>expected</type><x>0</x><y>0</y><links><link><to>O.O[3]</to><stream>2.000000</stream></link></links></node><node><id>O.O[1]</id><type>neuron</type><x>0</x><y>0</y><output>1</output><links></links></node><node><id>O.O[2]</id><type>neuron</type><x>0</x><y>0</y><output>1</output><links></links></node><node><id>O.O[3]</id><type>neuron</type><x>0</x><y>0</y><output>1</output><links></links></node></nodes>"
        }
        self.do_init(t)
        self.do_graph_builder(t)

    def test_bug_conflict_expected_and_existing(self):
        t = {
            'name': 'bug conflict expected and existing',
            'prefix': 'test',
            'input': """
                gr [r=1..3,I] {
                    O[r] = 2*I[r]
                    out O[r]
                }
                network {
                    in .I[1],.I[2],.I[3]
                    O = gr [I:=.I]
                    out O.O[1]
                    out O.O[2]
                    out O.O[3]
                }
            """,
            'output': "<nodes><node><id>.I[1]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>O.O[1]</to><stream>2.000000</stream></link></links></node><node><id>.I[2]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>O.O[2]</to><stream>2.000000</stream></link></links></node><node><id>.I[3]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>O.O[3]</to><stream>2.000000</stream></link></links></node><node><id>O.O[1]</id><type>neuron</type><x>0</x><y>0</y><output>1</output><links></links></node><node><id>O.O[2]</id><type>neuron</type><x>0</x><y>0</y><output>1</output><links></links></node><node><id>O.O[3]</id><type>neuron</type><x>0</x><y>0</y><output>1</output><links></links></node></nodes>"
        }
        self.do_init(t)
        self.do_graph_builder(t)

    def test_bug_definition_override(self):
        t = {
            'name': 'bug definition override',
            'prefix': 'test',
            'input': """
                gr [r=1..3,I] {
                    O[r] = 2*I[r]
                    out O[r]
                }
                network X, Y, Z {
                    .I[1] = # X + 1
                    .I[2] = # Y + 2
                    .I[3] = # Z + 3
                    O = gr [I:=.I]
                    out O.O[1]
                    out O.O[2]
                    out O.O[3]
                }
            """,
            'output': "<nodes><node><id>.I[1]</id><type>neuron</type><x>0</x><y>0</y><constant>1.000000</constant><links><link><to>O.O[1]</to><stream>2.000000</stream></link></links></node><node><id>.I[2]</id><type>neuron</type><x>0</x><y>0</y><constant>2.000000</constant><links><link><to>O.O[2]</to><stream>2.000000</stream></link></links></node><node><id>.I[3]</id><type>neuron</type><x>0</x><y>0</y><constant>3.000000</constant><links><link><to>O.O[3]</to><stream>2.000000</stream></link></links></node><node><id>O.O[1]</id><type>neuron</type><x>0</x><y>0</y><output>1</output><links></links></node><node><id>O.O[2]</id><type>neuron</type><x>0</x><y>0</y><output>1</output><links></links></node><node><id>O.O[3]</id><type>neuron</type><x>0</x><y>0</y><output>1</output><links></links></node><node><id>X</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>.I[1]</to><stream>1.000000</stream></link></links></node><node><id>Y</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>.I[2]</to><stream>1.000000</stream></link></links></node><node><id>Z</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>.I[3]</to><stream>1.000000</stream></link></links></node></nodes>"
        }
        self.do_init(t)
        self.do_graph_builder(t)

    def test_sync_async_behaviour(self):
        t = {
            'name': 'sync async behaviour',
            'prefix': 'test',
            'input': """
                network X, Y, SYNC_X, SYNC_Y, COMPLEX_SYNC_X, COMPLEX_SYNC_Y, ASYNC_X, ASYNC_Y {
                    I = # X + 1
                    DEFAULT = I + Y

                    SYNC_I = # SYNC_X + 1
                    S = sync SYNC_I, SYNC_Y
                    SYNCED = S.SYNC_I + S.SYNC_Y

                    COMPLEX_SYNC_I = # SYNCED + COMPLEX_SYNC_X
                    COMPLEX_S = sync COMPLEX_SYNC_I, COMPLEX_SYNC_Y
                    COMPLEX_SYNCED = COMPLEX_S.COMPLEX_SYNC_I + COMPLEX_S.COMPLEX_SYNC_Y

                    ASYNC_I = # ASYNC_X + 1
                    AS = async ASYNC_I, ASYNC_Y
                    ASYNCED = AS.ASYNC_I + AS.ASYNC_Y
                }
            """,
            'output': "<nodes><node><id>ASYNCED</id><type>neuron</type><x>0</x><y>0</y><links></links></node><node><id>ASYNC_I</id><type>neuron</type><x>0</x><y>0</y><constant>1.000000</constant><links><link><to>ASYNCED</to><stream>1.000000</stream></link></links></node><node><id>ASYNC_X</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>ASYNC_I</to><stream>1.000000</stream></link></links></node><node><id>ASYNC_Y</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>ASYNCED</to><stream>1.000000</stream></link></links></node><node><id>COMPLEX_S.COMPLEX_SYNC_I</id><type>neuron</type><x>0</x><y>0</y><links><link><to>COMPLEX_SYNCED</to><stream>1.000000</stream></link></links></node><node><id>COMPLEX_S.COMPLEX_SYNC_Y</id><type>neuron</type><x>0</x><y>0</y><links><link><to>COMPLEX_SYNCED</to><stream>1.000000</stream></link></links></node><node><id>COMPLEX_S.COMPLEX_SYNC_Y.#delay0</id><type>neuron</type><x>0</x><y>0</y><links><link><to>COMPLEX_S.COMPLEX_SYNC_Y</to><stream>1.000000</stream></link></links></node><node><id>COMPLEX_S.COMPLEX_SYNC_Y.#delay1</id><type>neuron</type><x>0</x><y>0</y><links><link><to>COMPLEX_S.COMPLEX_SYNC_Y.#delay0</to><stream>1.000000</stream></link></links></node><node><id>COMPLEX_S.COMPLEX_SYNC_Y.#delay2</id><type>neuron</type><x>0</x><y>0</y><links><link><to>COMPLEX_S.COMPLEX_SYNC_Y.#delay1</to><stream>1.000000</stream></link></links></node><node><id>COMPLEX_S.COMPLEX_SYNC_Y.#delay3</id><type>neuron</type><x>0</x><y>0</y><links><link><to>COMPLEX_S.COMPLEX_SYNC_Y.#delay2</to><stream>1.000000</stream></link></links></node><node><id>COMPLEX_SYNCED</id><type>neuron</type><x>0</x><y>0</y><links></links></node><node><id>COMPLEX_SYNC_I</id><type>neuron</type><x>0</x><y>0</y><links><link><to>COMPLEX_S.COMPLEX_SYNC_I</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>COMPLEX_SYNC_X</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>COMPLEX_SYNC_I</to><stream>1.000000</stream></link></links></node><node><id>COMPLEX_SYNC_Y</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>COMPLEX_S.COMPLEX_SYNC_Y.#delay3</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>DEFAULT</id><type>neuron</type><x>0</x><y>0</y><links></links></node><node><id>I</id><type>neuron</type><x>0</x><y>0</y><constant>1.000000</constant><links><link><to>DEFAULT</to><stream>1.000000</stream></link></links></node><node><id>S.SYNC_I</id><type>neuron</type><x>0</x><y>0</y><links><link><to>SYNCED</to><stream>1.000000</stream></link></links></node><node><id>S.SYNC_Y</id><type>neuron</type><x>0</x><y>0</y><links><link><to>SYNCED</to><stream>1.000000</stream></link></links></node><node><id>S.SYNC_Y.#delay0</id><type>neuron</type><x>0</x><y>0</y><links><link><to>S.SYNC_Y</to><stream>1.000000</stream></link></links></node><node><id>SYNCED</id><type>neuron</type><x>0</x><y>0</y><links><link><to>COMPLEX_SYNC_I</to><stream>1.000000</stream></link></links></node><node><id>SYNC_I</id><type>neuron</type><x>0</x><y>0</y><constant>1.000000</constant><links><link><to>S.SYNC_I</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>SYNC_X</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>SYNC_I</to><stream>1.000000</stream></link></links></node><node><id>SYNC_Y</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>S.SYNC_Y.#delay0</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>X</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>I</to><stream>1.000000</stream></link></links></node><node><id>Y</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>DEFAULT</to><stream>1.000000</stream></link></links></node></nodes>"
        }
        self.do_init(t)
        self.do_graph_builder(t)

    def test_string(self):
        t = {
            'name': 'string',
            'prefix': 'test',
            'input': """
                network {
                    S = $'test'
                }
            """,
            'output': "<nodes><node><id>S.C[1]</id><type>neuron</type><x>0</x><y>0</y><constant>116.000000</constant><links><link><to>S.O[1]</to><stream>1.000000</stream></link></links></node><node><id>S.C[2]</id><type>neuron</type><x>0</x><y>0</y><constant>101.000000</constant><links><link><to>S.O[2]</to><stream>1.000000</stream></link></links></node><node><id>S.C[3]</id><type>neuron</type><x>0</x><y>0</y><constant>115.000000</constant><links><link><to>S.O[3]</to><stream>1.000000</stream></link></links></node><node><id>S.C[4]</id><type>neuron</type><x>0</x><y>0</y><constant>116.000000</constant><links><link><to>S.O[4]</to><stream>1.000000</stream></link></links></node><node><id>S.GET</id><type>neuron</type><x>0</x><y>0</y><links><link><to>S.O[1]</to><activation>1.000000</activation></link></links></node><node><id>S.LEN</id><type>neuron</type><x>0</x><y>0</y><constant>4.000000</constant><links></links></node><node><id>S.O[1]</id><type>neuron</type><x>0</x><y>0</y><threshold>1.000000</threshold><links><link><to>S.STREAM</to><stream>1.000000</stream></link><link><to>S.GET</to><activation>-255.000000</activation></link><link><to>S.O[2]</to><activation>1.000000</activation></link><link><to>S.O[1]</to><activation>-255.000000</activation></link></links></node><node><id>S.O[2]</id><type>neuron</type><x>0</x><y>0</y><threshold>1.000000</threshold><links><link><to>S.STREAM</to><stream>1.000000</stream></link><link><to>S.GET</to><activation>-255.000000</activation></link><link><to>S.O[3]</to><activation>1.000000</activation></link></links></node><node><id>S.O[3]</id><type>neuron</type><x>0</x><y>0</y><threshold>1.000000</threshold><links><link><to>S.STREAM</to><stream>1.000000</stream></link><link><to>S.GET</to><activation>-255.000000</activation></link><link><to>S.O[4]</to><activation>1.000000</activation></link></links></node><node><id>S.O[4]</id><type>neuron</type><x>0</x><y>0</y><threshold>1.000000</threshold><links><link><to>S.STREAM</to><stream>1.000000</stream></link></links></node><node><id>S.STREAM</id><type>neuron</type><x>0</x><y>0</y><links></links></node></nodes>"
        }
        self.do_init(t)
        self.do_graph_builder(t)

    def test_bug_complex_names(self):
        t = {
            'name': 'bug complex names',
            'prefix': 'test',
            'input': """
                g [k=G] {
                    N[k] = # 1
                }
                network I[1], I[2] {
                    M[2] = g [G:=I[1]]
                    out M[2].N[1]
                }
            """,
            'output': "<nodes><node><id>I[1]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>M[2].N[I[1]]</to><generation>1.000000</generation></link></links></node><node><id>I[2]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links></links></node><node><id>M[2].N[1]</id><type>expected</type><x>0</x><y>0</y><output>1</output><links></links></node><node><id>M[2].N[I[1]]</id><type>neuron</type><x>0</x><y>0</y><constant>1.000000</constant><template>1</template><links></links></node></nodes>",
        }
        self.do_init(t)
        self.do_graph_builder(t)

    def test_static_indexes_one(self):
        t = {
            'name': 'static indexes one',
            'prefix': 'test',
            'input': """
                network [r=1..3,k=1..3,k<r] I[r] {
                    R[r]=k+I[k]
                    out R[r]
                }
            """,
            'output': "<nodes><node><id>I[1]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>R[2]</to><stream>1.000000</stream></link><link><to>R[3]</to><stream>1.000000</stream></link></links></node><node><id>I[2]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>R[3]</to><stream>1.000000</stream></link></links></node><node><id>I[3]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links></links></node><node><id>R[1]</id><type>neuron</type><x>0</x><y>0</y><output>1</output><links></links></node><node><id>R[2]</id><type>neuron</type><x>0</x><y>0</y><constant>1.000000</constant><output>1</output><links></links></node><node><id>R[3]</id><type>neuron</type><x>0</x><y>0</y><constant>3.000000</constant><output>1</output><links></links></node></nodes>",
        }
        self.do_init(t)
        self.do_graph_builder(t)

    def test_static_indexes_two(self):
        t = {
            'name': 'deep reduction',
            'prefix': 'static indexes two',
            'input': """
                network [k=1..3,r=1..3,r<k] I[k] {
                    R[k]=r+I[r]
                    out R[k]
                }
            """,
            'output': "<nodes><node><id>I[1]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>R[2]</to><stream>1.000000</stream></link><link><to>R[3]</to><stream>1.000000</stream></link></links></node><node><id>I[2]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>R[3]</to><stream>1.000000</stream></link></links></node><node><id>I[3]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links></links></node><node><id>R[1]</id><type>neuron</type><x>0</x><y>0</y><output>1</output><links></links></node><node><id>R[2]</id><type>neuron</type><x>0</x><y>0</y><constant>1.000000</constant><output>1</output><links></links></node><node><id>R[3]</id><type>neuron</type><x>0</x><y>0</y><constant>3.000000</constant><output>1</output><links></links></node></nodes>",
        }
        self.do_init(t)
        self.do_graph_builder(t)

    def test_static_indexes_three(self):
        t = {
            'name': 'deep reduction',
            'prefix': 'static indexes three',
            'input': """
                network [r=1..3,k=1..3,r<k] I[k] {
                    R[k]=r+I[r]
                    out R[k]
                }
            """,
            'output': "<nodes><node><id>I[1]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>R[2]</to><stream>1.000000</stream></link><link><to>R[3]</to><stream>1.000000</stream></link></links></node><node><id>I[2]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>R[3]</to><stream>1.000000</stream></link></links></node><node><id>I[3]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links></links></node><node><id>R[1]</id><type>neuron</type><x>0</x><y>0</y><output>1</output><links></links></node><node><id>R[2]</id><type>neuron</type><x>0</x><y>0</y><constant>1.000000</constant><output>1</output><links></links></node><node><id>R[3]</id><type>neuron</type><x>0</x><y>0</y><constant>3.000000</constant><output>1</output><links></links></node></nodes>",
        }
        self.do_init(t)
        self.do_graph_builder(t)

    def test_static_indexes_four(self):
        t = {
            'name': 'static indexes four',
            'prefix': 'test',
            'input': """
                network [k=1..3,r=1..3,k<r] I[r] {
                    R[r]=k+I[k]
                    out R[r]
                }
            """,
            'output': "<nodes><node><id>I[1]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>R[2]</to><stream>1.000000</stream></link><link><to>R[3]</to><stream>1.000000</stream></link></links></node><node><id>I[2]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>R[3]</to><stream>1.000000</stream></link></links></node><node><id>I[3]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links></links></node><node><id>R[1]</id><type>neuron</type><x>0</x><y>0</y><output>1</output><links></links></node><node><id>R[2]</id><type>neuron</type><x>0</x><y>0</y><constant>1.000000</constant><output>1</output><links></links></node><node><id>R[3]</id><type>neuron</type><x>0</x><y>0</y><constant>3.000000</constant><output>1</output><links></links></node></nodes>",
        }
        self.do_init(t)
        self.do_graph_builder(t)

    def test_static_index_contant(self):
        t = {
            'name': 'static index constant',
            'prefix': 'test',
            'input': """
                [LEN = 3]
                network [k=1..LEN] I[k] {
                    R = # I[k] >= LEN, 1
                    out R
                }
            """,
            'output': "<nodes><node><id>I[1]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>R</to><activation>1.000000</activation></link></links></node><node><id>I[2]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>R</to><activation>1.000000</activation></link></links></node><node><id>I[3]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>R</to><activation>1.000000</activation></link></links></node><node><id>R</id><type>neuron</type><x>0</x><y>0</y><threshold>3.000000</threshold><constant>1.000000</constant><output>1</output><links></links></node></nodes>",
        }
        self.do_init(t)
        self.do_graph_builder(t)

    def test_dynamic_index_contant(self):
        t = {
            'name': 'dynamic index constant',
            'prefix': 'test',
            'input': """
                network LEN, K {
                    [LEN] in I[LEN]
                    [LEN] R[LEN] = # I[LEN] >= LEN, 1 + LEN * K
                    [LEN] out R[LEN]
                }
            """,
            'output': "<nodes><node><id>I[LEN]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><template>1</template><links><link><to>R[LEN]</to><activation>1.000000</activation></link></links></node><node><id>K</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>R[LEN]</to><stream>LEN</stream></link></links></node><node><id>LEN</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>I[LEN]</to><generation>1.000000</generation></link><link><to>R[LEN]</to><generation>1.000000</generation></link></links></node><node><id>R[LEN]</id><type>neuron</type><x>0</x><y>0</y><threshold>LEN</threshold><constant>1.000000</constant><output>1</output><template>1</template><links></links></node></nodes>",
        }
        self.do_init(t)
        self.do_graph_builder(t)

    def test_perceptron(self):
        t = {
            'name': 'perceptron',
            'prefix': 'test',
            'input': """
                [N=2]
                network [i=1..N,k=1..N] {
                    in I[i]
                    M[i][k] is multiplicator 0.1 * LEARN[i][k], I[k], 1
                    Y[i] is additive M[i][k]
                    out Y[i]

                    in D[i]
                    DELAY[i] is additive D[i]
                    DELAY2[i] is additive I[i]
                    DELAY3[i] is additive DELAY2[i]
                    DIFF[i] is additive DELAY[i] - M[i][k]
                    LEARN[i][k] is additive DIFF[i] * DELAY3[k]
                }
            """,
            'output': "<nodes><node><id>DELAY2[1]</id><type>neuron</type><x>0</x><y>0</y><links><link><to>DELAY3[1]</to><stream>1.000000</stream></link></links></node><node><id>DELAY2[2]</id><type>neuron</type><x>0</x><y>0</y><links><link><to>DELAY3[2]</to><stream>1.000000</stream></link></links></node><node><id>DELAY3[1]</id><type>neuron</type><x>0</x><y>0</y><links><link><to>DIFF_DELAY3[1,1].#100001</to><stream>1.000000</stream></link><link><to>DIFF_DELAY3[2,1].#100001</to><stream>1.000000</stream></link></links></node><node><id>DELAY3[2]</id><type>neuron</type><x>0</x><y>0</y><links><link><to>DIFF_DELAY3[1,2].#100001</to><stream>1.000000</stream></link><link><to>DIFF_DELAY3[2,2].#100001</to><stream>1.000000</stream></link></links></node><node><id>DELAY[1]</id><type>neuron</type><x>0</x><y>0</y><links><link><to>DIFF[1]</to><stream>1.000000</stream></link></links></node><node><id>DELAY[2]</id><type>neuron</type><x>0</x><y>0</y><links><link><to>DIFF[2]</to><stream>1.000000</stream></link></links></node><node><id>DIFF[1]</id><type>neuron</type><x>0</x><y>0</y><links><link><to>DIFF_DELAY3[1,1].#100001</to><activation>1.000000</activation></link><link><to>DIFF_DELAY3[1,2].#100001</to><activation>1.000000</activation></link><link><to>DIFF[1].#neg</to><stream>-1.000000</stream></link></links></node><node><id>DIFF[1].#neg</id><type>neuron</type><x>0</x><y>0</y><links><link><to>DIFF_DELAY3[1,1].#100001</to><activation>1.000000</activation></link><link><to>DIFF_DELAY3[1,2].#100001</to><activation>1.000000</activation></link></links></node><node><id>DIFF[2]</id><type>neuron</type><x>0</x><y>0</y><links><link><to>DIFF_DELAY3[2,1].#100001</to><activation>1.000000</activation></link><link><to>DIFF_DELAY3[2,2].#100001</to><activation>1.000000</activation></link><link><to>DIFF[2].#neg</to><stream>-1.000000</stream></link></links></node><node><id>DIFF[2].#neg</id><type>neuron</type><x>0</x><y>0</y><links><link><to>DIFF_DELAY3[2,1].#100001</to><activation>1.000000</activation></link><link><to>DIFF_DELAY3[2,2].#100001</to><activation>1.000000</activation></link></links></node><node><id>DIFF_DELAY3[1,1].#100001</id><type>multiplicator</type><x>0</x><y>0</y><links><link><to>LEARN[1,1]</to><stream>1.000000</stream></link></links></node><node><id>DIFF_DELAY3[1,2].#100001</id><type>multiplicator</type><x>0</x><y>0</y><links><link><to>LEARN[1,2]</to><stream>1.000000</stream></link></links></node><node><id>DIFF_DELAY3[2,1].#100001</id><type>multiplicator</type><x>0</x><y>0</y><links><link><to>LEARN[2,1]</to><stream>1.000000</stream></link></links></node><node><id>DIFF_DELAY3[2,2].#100001</id><type>multiplicator</type><x>0</x><y>0</y><links><link><to>LEARN[2,2]</to><stream>1.000000</stream></link></links></node><node><id>D[1]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>DELAY[1]</to><stream>1.000000</stream></link></links></node><node><id>D[2]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>DELAY[2]</to><stream>1.000000</stream></link></links></node><node><id>I[1]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>M[1,1]</to><stream>1.000000</stream></link><link><to>M[2,1]</to><stream>1.000000</stream></link><link><to>DELAY2[1]</to><stream>1.000000</stream></link></links></node><node><id>I[2]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>M[1,2]</to><stream>1.000000</stream></link><link><to>M[2,2]</to><stream>1.000000</stream></link><link><to>DELAY2[2]</to><stream>1.000000</stream></link></links></node><node><id>LEARN[1,1]</id><type>neuron</type><x>0</x><y>0</y><links><link><to>M[1,1]</to><activation>0.100000</activation></link></links></node><node><id>LEARN[1,2]</id><type>neuron</type><x>0</x><y>0</y><links><link><to>M[1,2]</to><activation>0.100000</activation></link></links></node><node><id>LEARN[2,1]</id><type>neuron</type><x>0</x><y>0</y><links><link><to>M[2,1]</to><activation>0.100000</activation></link></links></node><node><id>LEARN[2,2]</id><type>neuron</type><x>0</x><y>0</y><links><link><to>M[2,2]</to><activation>0.100000</activation></link></links></node><node><id>M[1,1]</id><type>multiplicator</type><x>0</x><y>0</y><threshold>1.000000</threshold><links><link><to>Y[1]</to><stream>1.000000</stream></link><link><to>DIFF[1]</to><stream>-1.000000</stream></link></links></node><node><id>M[1,2]</id><type>multiplicator</type><x>0</x><y>0</y><threshold>1.000000</threshold><links><link><to>Y[1]</to><stream>1.000000</stream></link><link><to>DIFF[1]</to><stream>-1.000000</stream></link></links></node><node><id>M[2,1]</id><type>multiplicator</type><x>0</x><y>0</y><threshold>1.000000</threshold><links><link><to>Y[2]</to><stream>1.000000</stream></link><link><to>DIFF[2]</to><stream>-1.000000</stream></link></links></node><node><id>M[2,2]</id><type>multiplicator</type><x>0</x><y>0</y><threshold>1.000000</threshold><links><link><to>Y[2]</to><stream>1.000000</stream></link><link><to>DIFF[2]</to><stream>-1.000000</stream></link></links></node><node><id>Y[1]</id><type>neuron</type><x>0</x><y>0</y><output>1</output><links></links></node><node><id>Y[2]</id><type>neuron</type><x>0</x><y>0</y><output>1</output><links></links></node></nodes>",
        }
        self.do_init(t)
        self.do_graph_builder(t)

    def test_max_static(self):
        t = {
            'name': 'max static',
            'prefix': 'test',
            'input': """
                [LEN=3]
                network [i=1..LEN, j=1..LEN, i!=j, k=1..LEN, i>k, I] I[i] {
                    DELAY[i] = # I[i]
                    CMP[i][j] = # I[i] - I[j] >= 0, 1
                    MAXS[i] = # CMP[i][j] + 1 >= LEN, DELAY[i]
                    BLOCK[i] = # 0 - MAXS[k] >= 0, MAXS[i]
                    RES = # BLOCK[i]
                    out RES
                }
            """,
            'output': "<nodes><node><id>BLOCK[1]</id><type>neuron</type><x>0</x><y>0</y><links><link><to>RES</to><stream>1.000000</stream></link></links></node><node><id>BLOCK[2]</id><type>neuron</type><x>0</x><y>0</y><links><link><to>RES</to><stream>1.000000</stream></link></links></node><node><id>BLOCK[3]</id><type>neuron</type><x>0</x><y>0</y><links><link><to>RES</to><stream>1.000000</stream></link></links></node><node><id>CMP[1,2]</id><type>neuron</type><x>0</x><y>0</y><constant>1.000000</constant><links><link><to>MAXS[1]</to><activation>1.000000</activation></link></links></node><node><id>CMP[1,3]</id><type>neuron</type><x>0</x><y>0</y><constant>1.000000</constant><links><link><to>MAXS[1]</to><activation>1.000000</activation></link></links></node><node><id>CMP[2,1]</id><type>neuron</type><x>0</x><y>0</y><constant>1.000000</constant><links><link><to>MAXS[2]</to><activation>1.000000</activation></link></links></node><node><id>CMP[2,3]</id><type>neuron</type><x>0</x><y>0</y><constant>1.000000</constant><links><link><to>MAXS[2]</to><activation>1.000000</activation></link></links></node><node><id>CMP[3,1]</id><type>neuron</type><x>0</x><y>0</y><constant>1.000000</constant><links><link><to>MAXS[3]</to><activation>1.000000</activation></link></links></node><node><id>CMP[3,2]</id><type>neuron</type><x>0</x><y>0</y><constant>1.000000</constant><links><link><to>MAXS[3]</to><activation>1.000000</activation></link></links></node><node><id>DELAY[1]</id><type>neuron</type><x>0</x><y>0</y><links><link><to>MAXS[1]</to><stream>1.000000</stream></link></links></node><node><id>DELAY[2]</id><type>neuron</type><x>0</x><y>0</y><links><link><to>MAXS[2]</to><stream>1.000000</stream></link></links></node><node><id>DELAY[3]</id><type>neuron</type><x>0</x><y>0</y><links><link><to>MAXS[3]</to><stream>1.000000</stream></link></links></node><node><id>I[1]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>DELAY[1]</to><stream>1.000000</stream></link><link><to>CMP[1,2]</to><activation>1.000000</activation></link><link><to>CMP[1,3]</to><activation>1.000000</activation></link><link><to>CMP[2,1]</to><activation>-1.000000</activation></link><link><to>CMP[3,1]</to><activation>-1.000000</activation></link></links></node><node><id>I[2]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>DELAY[2]</to><stream>1.000000</stream></link><link><to>CMP[1,2]</to><activation>-1.000000</activation></link><link><to>CMP[2,1]</to><activation>1.000000</activation></link><link><to>CMP[2,3]</to><activation>1.000000</activation></link><link><to>CMP[3,2]</to><activation>-1.000000</activation></link></links></node><node><id>I[3]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>DELAY[3]</to><stream>1.000000</stream></link><link><to>CMP[1,3]</to><activation>-1.000000</activation></link><link><to>CMP[2,3]</to><activation>-1.000000</activation></link><link><to>CMP[3,1]</to><activation>1.000000</activation></link><link><to>CMP[3,2]</to><activation>1.000000</activation></link></links></node><node><id>MAXS[1]</id><type>neuron</type><x>0</x><y>0</y><threshold>3.000000</threshold><links><link><to>BLOCK[1]</to><stream>1.000000</stream></link><link><to>BLOCK[2]</to><activation>-1.000000</activation></link><link><to>BLOCK[3]</to><activation>-1.000000</activation></link></links></node><node><id>MAXS[2]</id><type>neuron</type><x>0</x><y>0</y><threshold>3.000000</threshold><links><link><to>BLOCK[2]</to><stream>1.000000</stream></link><link><to>BLOCK[3]</to><activation>-1.000000</activation></link></links></node><node><id>MAXS[3]</id><type>neuron</type><x>0</x><y>0</y><threshold>3.000000</threshold><links><link><to>BLOCK[3]</to><stream>1.000000</stream></link></links></node><node><id>RES</id><type>neuron</type><x>0</x><y>0</y><output>1</output><links></links></node></nodes>",
        }
        self.do_init(t)
        self.do_graph_builder(t)

    def test_bug_substitute_global(self):
        t = {
            'name': 'bug substitute global',
            'prefix': 'test',
            'input': """
                [II=2]
                network I {
                    O = II*I
                    out O
                }
            """,
            'output': "<nodes><node><id>I</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>II_I.#100001</to><stream>1.000000</stream></link></links></node><node><id>II</id><type>expected</type><x>0</x><y>0</y><links><link><to>II_I.#100001</to><activation>1.000000</activation></link><link><to>II.#neg</to><stream>-1.000000</stream></link></links></node><node><id>II.#neg</id><type>neuron</type><x>0</x><y>0</y><links><link><to>II_I.#100001</to><activation>1.000000</activation></link></links></node><node><id>II_I.#100001</id><type>multiplicator</type><x>0</x><y>0</y><links><link><to>O</to><stream>1.000000</stream></link></links></node><node><id>O</id><type>neuron</type><x>0</x><y>0</y><output>1</output><links></links></node></nodes>",
        }
        self.do_init(t)
        self.do_graph_builder(t)

    def test_bug_substitute_global_template(self):
        t = {
            'name': 'bug substitute global template',
            'prefix': 'test',
            'input': """
                [II=2]
                network [II] I {
                    O = II*I
                    out O
                }
            """,
            'output': "<nodes><node><id>I</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>O</to><stream>2.000000</stream></link></links></node><node><id>O</id><type>neuron</type><x>0</x><y>0</y><output>1</output><links></links></node></nodes>",
        }
        self.do_init(t)
        self.do_graph_builder(t)

    def test_bug_unary_minus(self):
        t = {
            'name': 'bug unary minus',
            'prefix': 'test',
            'input': """
                network X {
                    R = # X >= 0, -10
                    out R
                }
            """,
            'output': "<nodes><node><id>R</id><type>neuron</type><x>0</x><y>0</y><constant>-10.000000</constant><output>1</output><links></links></node><node><id>X</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>R</to><activation>1.000000</activation></link></links></node></nodes>",
        }
        self.do_init(t)
        self.do_graph_builder(t)

    def test_bug_unary_minus_var(self):
        t = {
            'name': 'bug unary minus var',
            'prefix': 'test',
            'input': """
                network X {
                    R = # -X >= 0, 1
                    out R
                }
            """,
            'output': "<nodes><node><id>R</id><type>neuron</type><x>0</x><y>0</y><constant>1.000000</constant><output>1</output><links></links></node><node><id>X</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>R</to><activation>-1.000000</activation></link></links></node></nodes>",
        }
        self.do_init(t)
        self.do_graph_builder(t)

    def test_bug_runtime_constant_minus(self):
        t = {
            'name': 'bug runtime constant minus',
            'prefix': 'test',
            'input': """
                network I, X {
                    [I] R[I] = # 10 - I * X
                    [I] out R[I]
                }
            """,
            'output': "<nodes><node><id>I</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>R[I]</to><generation>1.000000</generation></link></links></node><node><id>R[I]</id><type>neuron</type><x>0</x><y>0</y><constant>10.000000</constant><output>1</output><template>1</template><links></links></node><node><id>X</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>R[I]</to><stream>-I</stream></link></links></node></nodes>",
        }
        self.do_init(t)
        self.do_graph_builder(t)

    def test_bug_multiple_runtime_constants(self):
        t = {
            'name': 'bug multiple runtime constants',
            'prefix': 'test',
            'input': """
                network I, X {
                    [I] R[I] = # I * I + X
                    [I] out R[I]
                }
            """,
            'output': "",
            'exception': model.GraphError
        }
        self.do_init(t)
        self.do_graph_builder(t)

    def test_bug_group_reduction(self):
        t = {
            'name': 'bug group reduction',
            'prefix': 'test',
            'input': """
                [SZ=3]
                func [i=1..SZ] V[i] {
                    M =# V[i]
                    out M
                }
                network [i=1..SZ] I[i] {
                    M = func I[i]
                    out M
                }
            """,
            'output': "<nodes><node><id>I[1]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>M.V[1]</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>I[2]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>M.V[2]</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>I[3]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>M.V[3]</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>M.M</id><type>neuron</type><x>0</x><y>0</y><output>1</output><links></links></node><node><id>M.V[1]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>M.M</to><stream>1.000000</stream></link></links></node><node><id>M.V[2]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>M.M</to><stream>1.000000</stream></link></links></node><node><id>M.V[3]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>M.M</to><stream>1.000000</stream></link></links></node></nodes>"
        }
        self.do_init(t)
        self.do_graph_builder(t)

    def test_bug_lost_sign_and_constant(self):
        t = {
            'name': 'bug lost sign and constant',
            'prefix': 'test',
            'input': """
                [SZ=3]
                pmax [V,i=1..SZ,j=1..SZ,i<j,k=1..SZ] {
                    GE[i,j] =# V[i]-V[j]>=0,1
                    LT[i,j] =# V[j]-V[i]>=1,1
                    M[i] =+ # 0>=0, GE[i,j]
                    M[j] =+ # 0>=0, LT[i,j]
                    MAX = # 0
                    out MAX
                }
                network [i=1..SZ] {
                    S = 'afd'
                    .C[i] = S.C[i]
                    MM = pmax [V:=.C]
                    out MM
                }
            """,
            'output': "<nodes><node><id>.C[1]</id><type>neuron</type><x>0</x><y>0</y><links><link><to>MM.GE[1,2]</to><activation>1.000000</activation></link><link><to>MM.GE[1,3]</to><activation>1.000000</activation></link><link><to>MM.LT[1,2]</to><activation>-1.000000</activation></link><link><to>MM.LT[1,3]</to><activation>-1.000000</activation></link></links></node><node><id>.C[2]</id><type>neuron</type><x>0</x><y>0</y><links><link><to>MM.GE[1,2]</to><activation>-1.000000</activation></link><link><to>MM.GE[2,3]</to><activation>1.000000</activation></link><link><to>MM.LT[1,2]</to><activation>1.000000</activation></link><link><to>MM.LT[2,3]</to><activation>-1.000000</activation></link></links></node><node><id>.C[3]</id><type>neuron</type><x>0</x><y>0</y><links><link><to>MM.GE[1,3]</to><activation>-1.000000</activation></link><link><to>MM.GE[2,3]</to><activation>-1.000000</activation></link><link><to>MM.LT[1,3]</to><activation>1.000000</activation></link><link><to>MM.LT[2,3]</to><activation>1.000000</activation></link></links></node><node><id>MM.GE[1,2]</id><type>neuron</type><x>0</x><y>0</y><constant>1.000000</constant><links><link><to>MM.M[1]</to><stream>1.000000</stream></link></links></node><node><id>MM.GE[1,3]</id><type>neuron</type><x>0</x><y>0</y><constant>1.000000</constant><links><link><to>MM.M[1]</to><stream>1.000000</stream></link></links></node><node><id>MM.GE[2,3]</id><type>neuron</type><x>0</x><y>0</y><constant>1.000000</constant><links><link><to>MM.M[2]</to><stream>1.000000</stream></link></links></node><node><id>MM.LT[1,2]</id><type>neuron</type><x>0</x><y>0</y><threshold>1.000000</threshold><constant>1.000000</constant><links><link><to>MM.M[2]</to><stream>1.000000</stream></link></links></node><node><id>MM.LT[1,3]</id><type>neuron</type><x>0</x><y>0</y><threshold>1.000000</threshold><constant>1.000000</constant><links><link><to>MM.M[3]</to><stream>1.000000</stream></link></links></node><node><id>MM.LT[2,3]</id><type>neuron</type><x>0</x><y>0</y><threshold>1.000000</threshold><constant>1.000000</constant><links><link><to>MM.M[3]</to><stream>1.000000</stream></link></links></node><node><id>MM.MAX</id><type>neuron</type><x>0</x><y>0</y><output>1</output><links></links></node><node><id>MM.M[1]</id><type>neuron</type><x>0</x><y>0</y><links></links></node><node><id>MM.M[2]</id><type>neuron</type><x>0</x><y>0</y><links></links></node><node><id>MM.M[3]</id><type>neuron</type><x>0</x><y>0</y><links></links></node><node><id>S.C[1]</id><type>neuron</type><x>0</x><y>0</y><constant>97.000000</constant><links><link><to>S.O[1]</to><stream>1.000000</stream></link><link><to>.C[1]</to><stream>1.000000</stream></link></links></node><node><id>S.C[2]</id><type>neuron</type><x>0</x><y>0</y><constant>102.000000</constant><links><link><to>S.O[2]</to><stream>1.000000</stream></link><link><to>.C[2]</to><stream>1.000000</stream></link></links></node><node><id>S.C[3]</id><type>neuron</type><x>0</x><y>0</y><constant>100.000000</constant><links><link><to>S.O[3]</to><stream>1.000000</stream></link><link><to>.C[3]</to><stream>1.000000</stream></link></links></node><node><id>S.GET</id><type>neuron</type><x>0</x><y>0</y><links><link><to>S.O[1]</to><activation>1.000000</activation></link></links></node><node><id>S.LEN</id><type>neuron</type><x>0</x><y>0</y><constant>3.000000</constant><links></links></node><node><id>S.O[1]</id><type>neuron</type><x>0</x><y>0</y><threshold>1.000000</threshold><links><link><to>S.STREAM</to><stream>1.000000</stream></link><link><to>S.GET</to><activation>-255.000000</activation></link><link><to>S.O[2]</to><activation>1.000000</activation></link><link><to>S.O[1]</to><activation>-255.000000</activation></link></links></node><node><id>S.O[2]</id><type>neuron</type><x>0</x><y>0</y><threshold>1.000000</threshold><links><link><to>S.STREAM</to><stream>1.000000</stream></link><link><to>S.GET</to><activation>-255.000000</activation></link><link><to>S.O[3]</to><activation>1.000000</activation></link></links></node><node><id>S.O[3]</id><type>neuron</type><x>0</x><y>0</y><threshold>1.000000</threshold><links><link><to>S.STREAM</to><stream>1.000000</stream></link></links></node><node><id>S.STREAM</id><type>neuron</type><x>0</x><y>0</y><links></links></node></nodes>"
        }
        self.do_init(t)
        self.do_graph_builder(t)

    def test_bug_deep_rename(self):
        t = {
            'name': 'bug deep rename',
            'prefix': 'test',
            'input': """
                [SZ=3]
                pmax [V,i=1..SZ,j=1..SZ,i<j,k=1..SZ] V[k] {
                    GE[i,j] =# V[i]-V[j]>=0,1
                    LT[i,j] =# V[j]-V[i]>=1,1
                    M[i] =+ # 0>=0, GE[i,j]
                    M[j] =+ # 0>=0, LT[i,j]
                    MAX = # 0
                    out MAX
                }
                network [i=1..SZ] {
                    S = 'afd'
                    MM = pmax [V:=S.C]
                    out MM
                }
            """,
            'output': "<nodes><node><id>MM.GE[1,2]</id><type>neuron</type><x>0</x><y>0</y><constant>1.000000</constant><links><link><to>MM.M[1]</to><stream>1.000000</stream></link></links></node><node><id>MM.GE[1,3]</id><type>neuron</type><x>0</x><y>0</y><constant>1.000000</constant><links><link><to>MM.M[1]</to><stream>1.000000</stream></link></links></node><node><id>MM.GE[2,3]</id><type>neuron</type><x>0</x><y>0</y><constant>1.000000</constant><links><link><to>MM.M[2]</to><stream>1.000000</stream></link></links></node><node><id>MM.LT[1,2]</id><type>neuron</type><x>0</x><y>0</y><threshold>1.000000</threshold><constant>1.000000</constant><links><link><to>MM.M[2]</to><stream>1.000000</stream></link></links></node><node><id>MM.LT[1,3]</id><type>neuron</type><x>0</x><y>0</y><threshold>1.000000</threshold><constant>1.000000</constant><links><link><to>MM.M[3]</to><stream>1.000000</stream></link></links></node><node><id>MM.LT[2,3]</id><type>neuron</type><x>0</x><y>0</y><threshold>1.000000</threshold><constant>1.000000</constant><links><link><to>MM.M[3]</to><stream>1.000000</stream></link></links></node><node><id>MM.MAX</id><type>neuron</type><x>0</x><y>0</y><output>1</output><links></links></node><node><id>MM.M[1]</id><type>neuron</type><x>0</x><y>0</y><links></links></node><node><id>MM.M[2]</id><type>neuron</type><x>0</x><y>0</y><links></links></node><node><id>MM.M[3]</id><type>neuron</type><x>0</x><y>0</y><links></links></node><node><id>S.C[1]</id><type>neuron</type><x>0</x><y>0</y><constant>97.000000</constant><links><link><to>S.O[1]</to><stream>1.000000</stream></link><link><to>MM.GE[1,2]</to><activation>1.000000</activation></link><link><to>MM.GE[1,3]</to><activation>1.000000</activation></link><link><to>MM.LT[1,2]</to><activation>-1.000000</activation></link><link><to>MM.LT[1,3]</to><activation>-1.000000</activation></link></links></node><node><id>S.C[2]</id><type>neuron</type><x>0</x><y>0</y><constant>102.000000</constant><links><link><to>S.O[2]</to><stream>1.000000</stream></link><link><to>MM.GE[1,2]</to><activation>-1.000000</activation></link><link><to>MM.GE[2,3]</to><activation>1.000000</activation></link><link><to>MM.LT[1,2]</to><activation>1.000000</activation></link><link><to>MM.LT[2,3]</to><activation>-1.000000</activation></link></links></node><node><id>S.C[3]</id><type>neuron</type><x>0</x><y>0</y><constant>100.000000</constant><links><link><to>S.O[3]</to><stream>1.000000</stream></link><link><to>MM.GE[1,3]</to><activation>-1.000000</activation></link><link><to>MM.GE[2,3]</to><activation>-1.000000</activation></link><link><to>MM.LT[1,3]</to><activation>1.000000</activation></link><link><to>MM.LT[2,3]</to><activation>1.000000</activation></link></links></node><node><id>S.GET</id><type>neuron</type><x>0</x><y>0</y><links><link><to>S.O[1]</to><activation>1.000000</activation></link></links></node><node><id>S.LEN</id><type>neuron</type><x>0</x><y>0</y><constant>3.000000</constant><links></links></node><node><id>S.O[1]</id><type>neuron</type><x>0</x><y>0</y><threshold>1.000000</threshold><links><link><to>S.STREAM</to><stream>1.000000</stream></link><link><to>S.GET</to><activation>-255.000000</activation></link><link><to>S.O[2]</to><activation>1.000000</activation></link><link><to>S.O[1]</to><activation>-255.000000</activation></link></links></node><node><id>S.O[2]</id><type>neuron</type><x>0</x><y>0</y><threshold>1.000000</threshold><links><link><to>S.STREAM</to><stream>1.000000</stream></link><link><to>S.GET</to><activation>-255.000000</activation></link><link><to>S.O[3]</to><activation>1.000000</activation></link></links></node><node><id>S.O[3]</id><type>neuron</type><x>0</x><y>0</y><threshold>1.000000</threshold><links><link><to>S.STREAM</to><stream>1.000000</stream></link></links></node><node><id>S.STREAM</id><type>neuron</type><x>0</x><y>0</y><links></links></node></nodes>"
        }
        self.do_init(t)
        self.do_graph_builder(t)

    def test_bug_constant_threshold(self):
        t = {
            'name': 'bug constant threshold',
            'prefix': 'test',
            'input': """
                network [i=1..3,j=1..3,i<j] {
                    V[i,j] =# 0>=j-i,1
                    out V[i,j]
                }
            """,
            'output': "<nodes><node><id>V[1,2]</id><type>neuron</type><x>0</x><y>0</y><threshold>1.000000</threshold><constant>1.000000</constant><output>1</output><links></links></node><node><id>V[1,3]</id><type>neuron</type><x>0</x><y>0</y><threshold>2.000000</threshold><constant>1.000000</constant><output>1</output><links></links></node><node><id>V[2,3]</id><type>neuron</type><x>0</x><y>0</y><threshold>1.000000</threshold><constant>1.000000</constant><output>1</output><links></links></node></nodes>"
        }
        self.do_init(t)
        self.do_graph_builder(t)

    def test_bug_named_weights(self):
        t = {
            'name': 'bug named weights',
            'prefix': 'test',
            'input': """
                network I {
                    .COMMANDS = command_storage I
                    .PROCESS = process [CMD := .COMMANDS]
                }

                command_storage INPUT {
                    KEY1 = # INPUT
                    [KEY1] MEMORY[KEY1] = # 1
                }

                process [CMD] {
                    [CMD.KEY1] RES = # 2 - CMD.KEY1 * CMD.MEMORY[CMD.KEY1]
                }
            """,
            'output': "<nodes><node><id>.COMMANDS.INPUT</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><links><link><to>.COMMANDS.KEY1</to><stream>1.000000</stream></link></links></node><node><id>.COMMANDS.KEY1</id><type>neuron</type><x>0</x><y>0</y><links><link><to>.COMMANDS.MEMORY[.COMMANDS.KEY1]</to><generation>1.000000</generation></link><link><to>.PROCESS.RES</to><generation>1.000000</generation></link></links></node><node><id>.COMMANDS.MEMORY[.COMMANDS.KEY1]</id><type>neuron</type><x>0</x><y>0</y><constant>1.000000</constant><template>1</template><links><link><to>.PROCESS.RES</to><stream>-.COMMANDS.KEY1</stream></link></links></node><node><id>.PROCESS.RES</id><type>neuron</type><x>0</x><y>0</y><constant>2.000000</constant><template>1</template><links></links></node><node><id>I</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>.COMMANDS.INPUT</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node></nodes>",
        }
        self.do_init(t)
        self.do_graph_builder(t)

    def test_bug_expected_template_links(self):
        t = {
            'name': 'bug expected template links',
            'prefix': 'test',
            'input': """
                network A {
                    CA = # A
                    NA = # A + 1
                    [CA, NA] T[CA] = # S[CA] - S[NA]
                    [A] S[A] = # A
                }
            """,
            'output': "<nodes><node><id>A</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links><link><to>CA</to><stream>1.000000</stream></link><link><to>NA</to><stream>1.000000</stream></link><link><to>S[A]</to><generation>1.000000</generation></link></links></node><node><id>CA</id><type>neuron</type><x>0</x><y>0</y><links><link><to>S[CA]</to><generation>1.000000</generation></link><link><to>S[NA]</to><generation>1.000000</generation></link><link><to>T[CA]</to><generation>1.000000</generation></link></links></node><node><id>NA</id><type>neuron</type><x>0</x><y>0</y><constant>1.000000</constant><links><link><to>S[NA]</to><generation>1.000000</generation></link><link><to>T[CA]</to><generation>1.000000</generation></link></links></node><node><id>S[A]</id><type>neuron</type><x>0</x><y>0</y><constant>A</constant><template>1</template><links></links></node><node><id>S[CA]</id><type>expected</type><x>0</x><y>0</y><template>1</template><links><link><to>T[CA]</to><stream>1.000000</stream></link></links></node><node><id>S[NA]</id><type>expected</type><x>0</x><y>0</y><template>1</template><links><link><to>T[CA]</to><stream>-1.000000</stream></link></links></node><node><id>T[CA]</id><type>neuron</type><x>0</x><y>0</y><template>1</template><links></links></node></nodes>",
        }
        self.do_init(t)
        self.do_graph_builder(t)

    def do_init(self, t):
        try:
            t['tree'] = GrammarService().parse(t['input'])
        except Exception:
            self.fail("Exception in " + t['name'] + ": " + traceback.format_exc())

    def do_graph_builder(self, t):
        if t.get('exception') is not None:
            with self.assertRaises(t['exception']):
                GraphService(TestPackager(t.get('libraries', {}))).construct(t['prefix'], t['tree'])
        else:
            try:
                g = GraphService(TestPackager(t.get('libraries', {}))).construct(t['prefix'], t['tree'])
                res = g[t['prefix'] + ".network"].to_xml()
            except Exception:
                self.fail("Exception in " + t['name'] + ": " + traceback.format_exc())
            self.assertEqual(res, t['output'], u"{0:s}: {1:s} NOT {2:s}".format(t['name'], res, t['output']))


class TestPackager(PackageService):
    def __init__(self, packages):
        """
        :type packages: list[unicode]
        """
        super(PackageService, self).__init__()
        self.packages = packages

    def find_package(self, package_name):
        if package_name in self.packages:
            return self.packages[package_name]
        raise PackageError("Package " + package_name + " not found")


class TransformTest(DatabaselessTestCase):
    def test_bug_growing_links_not_appended(self):
        t = {
            'name': 'bug growing links not appended',
            'prefix': 'text',
            'input': "<nodes><node><id>I[1]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links></links></node><node><id>I[2]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links></links></node><node><id>I[3]</id><type>neuron</type><x>0</x><y>0</y><threshold>-255.000000</threshold><input>1</input><links></links></node><node><id>O.I[1]</id><type>expected</type><x>0</x><y>0</y><links><link><to>O.O[1]</to><stream>2.000000</stream></link></links></node><node><id>O.I[2]</id><type>expected</type><x>0</x><y>0</y><links><link><to>O.O[2]</to><stream>2.000000</stream></link></links></node><node><id>O.I[3]</id><type>expected</type><x>0</x><y>0</y><links><link><to>O.O[3]</to><stream>2.000000</stream></link></links></node><node><id>O.O[1]</id><type>neuron</type><x>0</x><y>0</y><output>1</output><links></links></node><node><id>O.O[2]</id><type>neuron</type><x>0</x><y>0</y><output>1</output><links></links></node><node><id>O.O[3]</id><type>neuron</type><x>0</x><y>0</y><output>1</output><links></links></node></nodes>",
            'output': {"perl": """I[1](-255.000000,0-i):
I[2](-255.000000,0-i):
I[3](-255.000000,0-i):
O.I[1](0,0-e): O.O[1](0,2.000000,0)
O.I[2](0,0-e): O.O[2](0,2.000000,0)
O.I[3](0,0-e): O.O[3](0,2.000000,0)
O.O[1](0,0-o):
O.O[2](0,0-o):
O.O[3](0,0-o):
""", "server": ""}
        }
        self.do_transform(t)

    def test_simple(self):
        t = {
            'name': 'simple transform',
            'input': "<nodes><node><id>I.C#linkmod_on_A_TVHQNEKCIUGFCBOA</id><type>multiplicator</type><x>0</x><y>0</y>"
                     "<threshold>1.000000</threshold><constant>1.000000</constant><links>"
                     "<link><to>I.C#delay_on_B_SOKCZGSGPFVNTHXC</to></link><link><to>I.C</to></link></links></node>"
                     "<node><id>DFOHACZIKESXEEYM</id><type>neuron</type><x>100</x><y>0</y><constant>2.000000</constant>"
                     "<links><link><to>II.A</to><activation>1.000000</activation><stream>1.000000</stream></link>"
                     "</links></node>"
                     "<node><id>III3.B</id><type>neuron</type><x>200</x><y>0</y><threshold>-256.000000</threshold>"
                     "<links><link><to>III3.C</to><stream>-1.000000</stream></link></links></node>"
                     "<node><id>III3.C</id><type>neuron</type><x>300</x><y>0</y><links></links></node>"
                     "<node><id>SHYDWENZQVEWHFFH.A</id><type>neuron</type><x>400</x><y>0</y>"
                     "<threshold>-256.000000</threshold><links>"
                     "<link><to>SHYDWENZQVEWHFFH.C</to><stream>1.000000</stream></link></links></node>"
                     "<node><id>III3.A</id><type>neuron</type><x>500</x><y>0</y><threshold>-256.000000</threshold>"
                     "<links><link><to>III3.C</to><stream>1.000000</stream></link></links></node>"
                     "<node><id>III2.C</id><type>neuron</type><x>600</x><y>0</y><links></links></node>"
                     "<node><id>III2.B</id><type>neuron</type><x>700</x><y>0</y><threshold>-256.000000</threshold>"
                     "<links><link><to>III2.C</to><stream>-1.000000</stream></link></links></node>"
                     "<node><id>III2.A</id><type>neuron</type><x>0</x><y>100</y><threshold>-256.000000</threshold>"
                     "<links><link><to>III2.C</to><stream>1.000000</stream></link></links></node>"
                     "<node><id>I.C#delay_on_B_SOKCZGSGPFVNTHXC</id><type>neuron</type><x>100</x><y>100</y>"
                     "<threshold>-256.000000</threshold><links><link><to>I.C</to></link></links></node>"
                     "<node><id>III4.A</id><type>neuron</type><x>200</x><y>100</y><threshold>-256.000000</threshold>"
                     "<links><link><to>III4.C</to><stream>1.000000</stream></link></links></node>"
                     "<node><id>III4.C</id><type>neuron</type><x>300</x><y>100</y><links></links></node>"
                     "<node><id>III4.B</id><type>neuron</type><x>400</x><y>100</y><threshold>-256.000000</threshold>"
                     "<links><link><to>III4.C</to><stream>-1.000000</stream></link></links></node>"
                     "<node><id>I.A</id><type>neuron</type><x>500</x><y>100</y><threshold>-256.000000</threshold>"
                     "<links><link><to>I.C#linkmod_on_A_TVHQNEKCIUGFCBOA</to><stream>1.000000</stream></link>"
                     "<link><to>I.C#delay_on_A_DCAQWUTTPFNJGUDR</to><activation>-1.000000</activation>"
                     "<stream>-1.000000</stream></link></links></node>"
                     "<node><id>I.B</id><type>neuron</type><x>600</x><y>100</y><threshold>-256.000000</threshold>"
                     "<links><link><to>I.C#delay_on_B_SOKCZGSGPFVNTHXC</to><activation>1.000000</activation>"
                     "<stream>1.000000</stream></link></links></node>"
                     "<node><id>I.C</id><type>neuron</type><x>700</x><y>100</y><links>"
                     "<link><to>NAXPSMYTXIZLEGMU.A</to><activation>1.000000</activation><stream>1.000000</stream>"
                     "</link><link><to>F.A</to><activation>1.000000</activation><stream>1.000000</stream></link>"
                     "<link><to>F2.A</to><activation>1.000000</activation><stream>1.000000</stream></link>"
                     "<link><to>SHYDWENZQVEWHFFH.A</to><activation>1.000000</activation><stream>1.000000</stream>"
                     "</link></links></node>"
                     "<node><id>X_X#LZMZIIPVAYNVINHX#delay_on_X_WGLGNORAJFQZONDF</id><type>neuron</type><x>0</x>"
                     "<y>200</y><threshold>-256.000000</threshold><links><link><to>X_X#LZMZIIPVAYNVINHX</to></link>"
                     "</links></node>"
                     "<node><id>II.C#delay_on_B_SOKCZGSGPFVNTHXC</id><type>neuron</type><x>100</x><y>200</y>"
                     "<threshold>-256.000000</threshold><links><link><to>II.C</to></link></links></node>"
                     "<node><id>I.C#delay_on_A_DCAQWUTTPFNJGUDR</id><type>neuron</type><x>200</x><y>200</y>"
                     "<threshold>-256.000000</threshold><links>"
                     "<link><to>I.C#linkmod_off_on_A_MGQRFEUDHOQFQYRV</to><stream>1.000000</stream></link></links>"
                     "</node>"
                     "<node><id>X_X#LZMZIIPVAYNVINHX#linkmod_off_on_X_YUSUUXNZIZZVTGMR</id><type>multiplicator</type>"
                     "<x>300</x><y>200</y><threshold>1.000000</threshold><constant>1.000000</constant><links>"
                     "<link><to>X_X#LZMZIIPVAYNVINHX#delay_on_X_WGLGNORAJFQZONDF</to></link>"
                     "<link><to>X_X#LZMZIIPVAYNVINHX</to></link></links></node>"
                     "<node><id>SHYDWENZQVEWHFFH.C</id><type>neuron</type><x>400</x><y>200</y><links>"
                     "<link><to>III4.A</to><activation>1.000000</activation><stream>1.000000</stream></link>"
                     "</links></node>"
                     "<node><id>F.C</id><type>neuron</type><x>500</x><y>200</y><links>"
                     "<link><to>III2.A</to><activation>1.000000</activation><stream>1.000000</stream></link>"
                     "</links></node>"
                     "<node><id>F.B</id><type>neuron</type><x>600</x><y>200</y><threshold>-256.000000</threshold>"
                     "<links><link><to>F.C</to><stream>1.000000</stream></link></links></node>"
                     "<node><id>F.A</id><type>neuron</type><x>700</x><y>200</y><threshold>-256.000000</threshold>"
                     "<links><link><to>F.C</to><stream>1.000000</stream></link></links></node>"
                     "<node><id>SHYDWENZQVEWHFFH.B</id><type>neuron</type><x>0</x><y>300</y>"
                     "<threshold>-256.000000</threshold><links>"
                     "<link><to>SHYDWENZQVEWHFFH.C</to><stream>1.000000</stream></link></links></node>"
                     "<node><id>X_X#LZMZIIPVAYNVINHX</id><type>neuron</type><x>100</x><y>300</y><links>"
                     "<link><to>X_X_20_Y#YUUEMWJUVBNHJSYG</to><stream>1.000000</stream></link></links></node>"
                     "<node><id>X_X#LZMZIIPVAYNVINHX#delay_on_X_FQYMJOJQEPVDEDRL</id><type>neuron</type><x>200</x>"
                     "<y>300</y><threshold>-256.000000</threshold><links>"
                     "<link><to>X_X#LZMZIIPVAYNVINHX#linkmod_off_on_X_YUSUUXNZIZZVTGMR</to><stream>1.000000</stream>"
                     "</link></links></node>"
                     "<node><id>II.C#linkmod_off_on_A_MGQRFEUDHOQFQYRV</id><type>multiplicator</type><x>300</x>"
                     "<y>300</y><threshold>1.000000</threshold><constant>1.000000</constant><template>1</template>"
                     "<links>"
                     "<link><to>II.C#delay_on_B_SOKCZGSGPFVNTHXC</to></link><link><to>II.C</to></link></links></node>"
                     "<node><id>X_X_20_Y_Z</id><type>neuron</type><x>400</x><y>300</y><output>1</output>"
                     "<links></links></node>"
                     "<node><id>X_X_20_Y#YUUEMWJUVBNHJSYG</id><type>neuron</type><x>500</x><y>300</y><links>"
                     "<link><to>X_X_20_Y_Z</to><stream>1.000000</stream></link></links></node>"
                     "<node><id>II.A</id><type>neuron</type><x>600</x><y>300</y><threshold>-256.000000</threshold>"
                     "<links><link><to>II.C#linkmod_on_A_TVHQNEKCIUGFCBOA</to><stream>1.000000</stream></link>"
                     "<link><to>II.C#delay_on_A_DCAQWUTTPFNJGUDR</to><activation>-1.000000</activation>"
                     "<stream>-1.000000</stream></link></links></node>"
                     "<node><id>II.B</id><type>neuron</type><x>700</x><y>300</y><threshold>-256.000000</threshold>"
                     "<links><link><to>II.C#delay_on_B_SOKCZGSGPFVNTHXC</to><activation>1.000000</activation>"
                     "<stream>1.000000</stream></link></links></node>"
                     "<node><id>II.C</id><type>neuron</type><x>0</x><y>400</y><links>"
                     "<link><to>NAXPSMYTXIZLEGMU.B</to><activation>1.000000</activation><stream>1.000000</stream>"
                     "</link><link><to>F.B</to><activation>1.000000</activation><stream>1.000000</stream></link>"
                     "<link><to>F2.B</to><activation>1.000000</activation><stream>1.000000</stream></link>"
                     "<link><to>SHYDWENZQVEWHFFH.B</to><activation>1.000000</activation><stream>1.000000</stream>"
                     "</link></links></node>"
                     "<node><id>NAXPSMYTXIZLEGMU.A</id><type>neuron</type><x>100</x><y>400</y>"
                     "<threshold>-256.000000</threshold><links>"
                     "<link><to>NAXPSMYTXIZLEGMU.C</to><stream>1.000000</stream></link></links></node>"
                     "<node><id>NAXPSMYTXIZLEGMU.C</id><type>neuron</type><x>200</x><y>400</y><links>"
                     "<link><to>III.A</to><activation>1.000000</activation><stream>1.000000</stream></link>"
                     "</links></node>"
                     "<node><id>NAXPSMYTXIZLEGMU.B</id><type>neuron</type><x>300</x><y>400</y>"
                     "<threshold>-256.000000</threshold><links>"
                     "<link><to>NAXPSMYTXIZLEGMU.C</to><stream>1.000000</stream></link></links></node>"
                     "<node><id>Y</id><type>neuron</type><x>400</x><y>400</y><threshold>-256.000000</threshold>"
                     "<input>1</input>"
                     "<links><link><to>II.B</to><activation>1.000000</activation><stream>1.000000</stream></link>"
                     "<link><to>20_Y#BPYTSNIHKPTEENNJ</to><stream>2.000000</stream></link></links></node>"
                     "<node><id>X</id><type>neuron</type><x>500</x><y>400</y><threshold>-256.000000</threshold>"
                     "<input>1</input>"
                     "<links><link><to>I.A</to><activation>1.000000</activation><stream>1.000000</stream></link>"
                     "<link><to>I.B</to><activation>1.000000</activation><stream>1.000000</stream></link>"
                     "<link><to>X_X#LZMZIIPVAYNVINHX#delay_on_X_WGLGNORAJFQZONDF</to><activation>1.000000</activation>"
                     "<stream>1.000000</stream></link>"
                     "<link><to>X_X#LZMZIIPVAYNVINHX#linkmod_on_X_XXXPNGISVNEHBVHR</to><stream>1.000000</stream></link>"
                     "<link><to>X_X#LZMZIIPVAYNVINHX#delay_on_X_FQYMJOJQEPVDEDRL</to><activation>-1.000000</activation>"
                     "<stream>-1.000000</stream></link></links></node>"
                     "<node><id>Z</id><type>neuron</type><x>600</x><y>400</y><threshold>-256.000000</threshold>"
                     "<input>1</input>"
                     "<links><link><to>III.B</to><activation>1.000000</activation><stream>1.000000</stream></link>"
                     "<link><to>III2.B</to><activation>1.000000</activation><stream>1.000000</stream></link>"
                     "<link><to>III3.B</to><activation>1.000000</activation><stream>1.000000</stream></link>"
                     "<link><to>III4.B</to><activation>1.000000</activation><stream>1.000000</stream></link>"
                     "<link><to>X_X_20_Y_Z</to><stream>-1.000000</stream></link></links></node>"
                     "<node><id>20_Y#BPYTSNIHKPTEENNJ</id><type>neuron</type><x>700</x><y>400</y><links>"
                     "<link><to>X_X_20_Y#YUUEMWJUVBNHJSYG</to><stream>1.000000</stream></link></links></node>"
                     "<node><id>F2.B</id><type>neuron</type><x>0</x><y>500</y><threshold>-256.000000</threshold><links>"
                     "<link><to>F2.C</to><stream>1.000000</stream></link></links></node>"
                     "<node><id>F2.C</id><type>neuron</type><x>100</x><y>500</y><links>"
                     "<link><to>III3.A</to><activation>1.000000</activation><stream>1.000000</stream></link>"
                     "</links></node>"
                     "<node><id>F2.A</id><type>neuron</type><x>200</x><y>500</y><threshold>-256.000000</threshold>"
                     "<links><link><to>F2.C</to><stream>1.000000</stream></link></links></node>"
                     "<node><id>I.C#linkmod_off_on_A_MGQRFEUDHOQFQYRV</id><type>multiplicator</type><x>300</x>"
                     "<y>500</y><threshold>1.000000</threshold><constant>1.000000</constant><links>"
                     "<link><to>I.C#delay_on_B_SOKCZGSGPFVNTHXC</to></link><link><to>I.C</to></link></links></node>"
                     "<node><id>II.C#delay_on_A_DCAQWUTTPFNJGUDR</id><type>neuron</type><x>400</x><y>500</y>"
                     "<threshold>-256.000000</threshold><links>"
                     "<link><to>II.C#linkmod_off_on_A_MGQRFEUDHOQFQYRV</to><stream>1.000000</stream></link>"
                     "</links></node>"
                     "<node><id>X_X#LZMZIIPVAYNVINHX#linkmod_on_X_XXXPNGISVNEHBVHR</id><type>multiplicator</type>"
                     "<x>500</x><y>500</y><threshold>1.000000</threshold><constant>1.000000</constant><links>"
                     "<link><to>X_X#LZMZIIPVAYNVINHX#delay_on_X_WGLGNORAJFQZONDF</to></link>"
                     "<link><to>X_X#LZMZIIPVAYNVINHX</to></link></links></node>"
                     "<node><id>II.C#linkmod_on_A_TVHQNEKCIUGFCBOA</id><type>multiplicator</type><x>600</x><y>500</y>"
                     "<threshold>1.000000</threshold><constant>1.000000</constant><links>"
                     "<link><to>II.C#delay_on_B_SOKCZGSGPFVNTHXC</to></link><link><to>II.C</to></link></links></node>"
                     "<node><id>III.A</id><type>neuron</type><x>700</x><y>500</y><threshold>-256.000000</threshold>"
                     "<links><link><to>III.C</to><stream>1.000000</stream></link></links></node>"
                     "<node><id>III.B</id><type>neuron</type><x>0</x><y>600</y><threshold>-256.000000</threshold>"
                     "<links><link><to>III.C</to><stream>-1.000000</stream></link></links></node>"
                     "<node><id>III.C</id><type>neuron</type><x>100</x><y>600</y><output>1</output>"
                     "<links></links></node></nodes>",
            'output': {"perl": """I.C#linkmod_on_A_TVHQNEKCIUGFCBOA(1.000000,1.000000-l): I.C#delay_on_B_SOKCZGSGPFVNTHXC(0,0,0) I.C(0,0,0)
DFOHACZIKESXEEYM(0,2.000000-): II.A(1.000000,1.000000,0)
III3.B(-256.000000,0-): III3.C(0,-1.000000,0)
III3.C(0,0-):
SHYDWENZQVEWHFFH.A(-256.000000,0-): SHYDWENZQVEWHFFH.C(0,1.000000,0)
III3.A(-256.000000,0-): III3.C(0,1.000000,0)
III2.C(0,0-):
III2.B(-256.000000,0-): III2.C(0,-1.000000,0)
III2.A(-256.000000,0-): III2.C(0,1.000000,0)
I.C#delay_on_B_SOKCZGSGPFVNTHXC(-256.000000,0-): I.C(0,0,0)
III4.A(-256.000000,0-): III4.C(0,1.000000,0)
III4.C(0,0-):
III4.B(-256.000000,0-): III4.C(0,-1.000000,0)
I.A(-256.000000,0-): I.C#linkmod_on_A_TVHQNEKCIUGFCBOA(0,1.000000,0) I.C#delay_on_A_DCAQWUTTPFNJGUDR(-1.000000,-1.000000,0)
I.B(-256.000000,0-): I.C#delay_on_B_SOKCZGSGPFVNTHXC(1.000000,1.000000,0)
I.C(0,0-): NAXPSMYTXIZLEGMU.A(1.000000,1.000000,0) F.A(1.000000,1.000000,0) F2.A(1.000000,1.000000,0) SHYDWENZQVEWHFFH.A(1.000000,1.000000,0)
X_X#LZMZIIPVAYNVINHX#delay_on_X_WGLGNORAJFQZONDF(-256.000000,0-): X_X#LZMZIIPVAYNVINHX(0,0,0)
II.C#delay_on_B_SOKCZGSGPFVNTHXC(-256.000000,0-): II.C(0,0,0)
I.C#delay_on_A_DCAQWUTTPFNJGUDR(-256.000000,0-): I.C#linkmod_off_on_A_MGQRFEUDHOQFQYRV(0,1.000000,0)
X_X#LZMZIIPVAYNVINHX#linkmod_off_on_X_YUSUUXNZIZZVTGMR(1.000000,1.000000-l): X_X#LZMZIIPVAYNVINHX#delay_on_X_WGLGNORAJFQZONDF(0,0,0) X_X#LZMZIIPVAYNVINHX(0,0,0)
SHYDWENZQVEWHFFH.C(0,0-): III4.A(1.000000,1.000000,0)
F.C(0,0-): III2.A(1.000000,1.000000,0)
F.B(-256.000000,0-): F.C(0,1.000000,0)
F.A(-256.000000,0-): F.C(0,1.000000,0)
SHYDWENZQVEWHFFH.B(-256.000000,0-): SHYDWENZQVEWHFFH.C(0,1.000000,0)
X_X#LZMZIIPVAYNVINHX(0,0-): X_X_20_Y#YUUEMWJUVBNHJSYG(0,1.000000,0)
X_X#LZMZIIPVAYNVINHX#delay_on_X_FQYMJOJQEPVDEDRL(-256.000000,0-): X_X#LZMZIIPVAYNVINHX#linkmod_off_on_X_YUSUUXNZIZZVTGMR(0,1.000000,0)
II.C#linkmod_off_on_A_MGQRFEUDHOQFQYRV(1.000000,1.000000-l-t): II.C#delay_on_B_SOKCZGSGPFVNTHXC(0,0,0) II.C(0,0,0)
X_X_20_Y_Z(0,0-o):
X_X_20_Y#YUUEMWJUVBNHJSYG(0,0-): X_X_20_Y_Z(0,1.000000,0)
II.A(-256.000000,0-): II.C#linkmod_on_A_TVHQNEKCIUGFCBOA(0,1.000000,0) II.C#delay_on_A_DCAQWUTTPFNJGUDR(-1.000000,-1.000000,0)
II.B(-256.000000,0-): II.C#delay_on_B_SOKCZGSGPFVNTHXC(1.000000,1.000000,0)
II.C(0,0-): NAXPSMYTXIZLEGMU.B(1.000000,1.000000,0) F.B(1.000000,1.000000,0) F2.B(1.000000,1.000000,0) SHYDWENZQVEWHFFH.B(1.000000,1.000000,0)
NAXPSMYTXIZLEGMU.A(-256.000000,0-): NAXPSMYTXIZLEGMU.C(0,1.000000,0)
NAXPSMYTXIZLEGMU.C(0,0-): III.A(1.000000,1.000000,0)
NAXPSMYTXIZLEGMU.B(-256.000000,0-): NAXPSMYTXIZLEGMU.C(0,1.000000,0)
Y(-256.000000,0-i): II.B(1.000000,1.000000,0) 20_Y#BPYTSNIHKPTEENNJ(0,2.000000,0)
X(-256.000000,0-i): I.A(1.000000,1.000000,0) I.B(1.000000,1.000000,0) X_X#LZMZIIPVAYNVINHX#delay_on_X_WGLGNORAJFQZONDF(1.000000,1.000000,0) X_X#LZMZIIPVAYNVINHX#linkmod_on_X_XXXPNGISVNEHBVHR(0,1.000000,0) X_X#LZMZIIPVAYNVINHX#delay_on_X_FQYMJOJQEPVDEDRL(-1.000000,-1.000000,0)
Z(-256.000000,0-i): III.B(1.000000,1.000000,0) III2.B(1.000000,1.000000,0) III3.B(1.000000,1.000000,0) III4.B(1.000000,1.000000,0) X_X_20_Y_Z(0,-1.000000,0)
20_Y#BPYTSNIHKPTEENNJ(0,0-): X_X_20_Y#YUUEMWJUVBNHJSYG(0,1.000000,0)
F2.B(-256.000000,0-): F2.C(0,1.000000,0)
F2.C(0,0-): III3.A(1.000000,1.000000,0)
F2.A(-256.000000,0-): F2.C(0,1.000000,0)
I.C#linkmod_off_on_A_MGQRFEUDHOQFQYRV(1.000000,1.000000-l): I.C#delay_on_B_SOKCZGSGPFVNTHXC(0,0,0) I.C(0,0,0)
II.C#delay_on_A_DCAQWUTTPFNJGUDR(-256.000000,0-): II.C#linkmod_off_on_A_MGQRFEUDHOQFQYRV(0,1.000000,0)
X_X#LZMZIIPVAYNVINHX#linkmod_on_X_XXXPNGISVNEHBVHR(1.000000,1.000000-l): X_X#LZMZIIPVAYNVINHX#delay_on_X_WGLGNORAJFQZONDF(0,0,0) X_X#LZMZIIPVAYNVINHX(0,0,0)
II.C#linkmod_on_A_TVHQNEKCIUGFCBOA(1.000000,1.000000-l): II.C#delay_on_B_SOKCZGSGPFVNTHXC(0,0,0) II.C(0,0,0)
III.A(-256.000000,0-): III.C(0,1.000000,0)
III.B(-256.000000,0-): III.C(0,-1.000000,0)
III.C(0,0-o):
""", "server": ""}
        }
        self.do_transform(t)

    def do_transform(self, t):
        perl = TransformService().to_perl(t['input'], None)
        server = TransformService().to_server(t['input'], None)
        self.assertEqual(perl, t['output']['perl'], "%s perl: %s NOT %s" % (t['name'], perl, t['output']['perl']))
        self.assertEqual(server, t['output']['server'],
                         "%s server: %s NOT %s" % (t['name'], server, t['output']['server']))


class LayoutTest(DatabaselessTestCase):
    def test_square(self):
        g = Graph()
        n1 = Node(0, grammar.Variable(u"A", 0, 0), 0, 0)
        n2 = Node(0, grammar.Variable(u"B", 0, 0), 0, 0)
        n3 = Node(0, grammar.Variable(u"C", 0, 0), 0, 0)
        g.nodes[unicode(n1.node_id)] = n1
        g.nodes[unicode(n2.node_id)] = n2
        g.nodes[unicode(n3.node_id)] = n3
        l1 = Link(n1.node_id, n2.node_id, 0, 0, 0)
        l2 = Link(n2.node_id, n3.node_id, 0, 0, 0)
        l3 = Link(n3.node_id, n1.node_id, 0, 0, 0)
        g.links[unicode(l1.node_from)] = [l1]
        g.links[unicode(l2.node_from)] = [l2]
        g.links[unicode(l3.node_from)] = [l3]
        self.assertEqual(u"<nodes><node><id>A</id><type>neuron</type><x>0</x><y>100</y>"
                         u"<links><link><to>B</to></link></links></node>"
                         u"<node><id>B</id><type>neuron</type><x>100</x><y>0</y>"
                         u"<links><link><to>C</to></link></links></node>"
                         u"<node><id>C</id><type>neuron</type><x>0</x><y>0</y>"
                         u"<links><link><to>A</to></link></links></node></nodes>",
                         SquareLayout().layout(g).to_xml())

    def test_square_large(self):
        g = GraphService(TestPackager([u'.'])).construct(u'layouttest', GrammarService().parse(u"""
                plus A, B {
                    C is additive 0 >= 0, A + B
                    out C
                }
                minus A, B {
                    C is additive 0 >= 0, A - B
                    out C
                }
                multiply A, B {
                    C is additive 0 >= 0, A * B
                    out C
                }
                network X, Y, Z {
                    "X * X + 2 * Y - Z"
                    I = multiply A:=X, B:=X
                    II = multiply 2, Y
                    III = minus (C@plus I, II), Z
                    III2 = minus (C@F=plus I, II), Z
                    III3 = minus (F2=plus I, II), Z
                    III4 = minus (plus I, II), Z
                    out III.C
                    out X * X + 2 * Y - Z
                }
            """))["layouttest.network"]
        self.assertEqual(u"<nodes><node><id>1_0_X_X_2_0_Y_m1_0_Z.#100004.#delay100005.#delay100006.#100007</id><type>neuron</type><x>0</x><y>600</y><output>1</output><links></links></node><node><id>F.A</id><type>neuron</type><x>600</x><y>500</y><threshold>-255.000000</threshold><links><link><to>F.C</to><stream>1.000000</stream></link></links></node><node><id>F.B</id><type>neuron</type><x>500</x><y>500</y><threshold>-255.000000</threshold><links><link><to>F.C</to><stream>1.000000</stream></link></links></node><node><id>F.C</id><type>neuron</type><x>400</x><y>500</y><links><link><to>III2.A</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>F2.A</id><type>neuron</type><x>300</x><y>500</y><threshold>-255.000000</threshold><links><link><to>F2.C</to><stream>1.000000</stream></link></links></node><node><id>F2.B</id><type>neuron</type><x>200</x><y>500</y><threshold>-255.000000</threshold><links><link><to>F2.C</to><stream>1.000000</stream></link></links></node><node><id>F2.C</id><type>neuron</type><x>100</x><y>500</y><links><link><to>III3.A</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>I.A</id><type>neuron</type><x>0</x><y>500</y><threshold>-255.000000</threshold><links><link><to>I.A_B.#100001</to><activation>1.000000</activation></link><link><to>I.A.#neg</to><stream>-1.000000</stream></link></links></node><node><id>I.A.#neg</id><type>neuron</type><x>600</x><y>400</y><links><link><to>I.A_B.#100001</to><activation>1.000000</activation></link></links></node><node><id>I.A_B.#100001</id><type>multiplicator</type><x>500</x><y>400</y><links><link><to>I.C</to><stream>1.000000</stream></link></links></node><node><id>I.B</id><type>neuron</type><x>400</x><y>400</y><threshold>-255.000000</threshold><links><link><to>I.A_B.#100001</to><stream>1.000000</stream></link></links></node><node><id>I.C</id><type>neuron</type><x>300</x><y>400</y><links><link><to>exec100002.A</to><activation>1.000000</activation><stream>1.000000</stream></link><link><to>F.A</to><activation>1.000000</activation><stream>1.000000</stream></link><link><to>F2.A</to><activation>1.000000</activation><stream>1.000000</stream></link><link><to>exec100003.A</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>II.A</id><type>neuron</type><x>200</x><y>400</y><threshold>-255.000000</threshold><links><link><to>II.A_B.#100001</to><activation>1.000000</activation></link><link><to>II.A.#neg</to><stream>-1.000000</stream></link></links></node><node><id>II.A.#neg</id><type>neuron</type><x>100</x><y>400</y><links><link><to>II.A_B.#100001</to><activation>1.000000</activation></link></links></node><node><id>II.A_B.#100001</id><type>multiplicator</type><x>0</x><y>400</y><links><link><to>II.C</to><stream>1.000000</stream></link></links></node><node><id>II.B</id><type>neuron</type><x>600</x><y>300</y><threshold>-255.000000</threshold><links><link><to>II.A_B.#100001</to><stream>1.000000</stream></link></links></node><node><id>II.C</id><type>neuron</type><x>500</x><y>300</y><links><link><to>exec100002.B</to><activation>1.000000</activation><stream>1.000000</stream></link><link><to>F.B</to><activation>1.000000</activation><stream>1.000000</stream></link><link><to>F2.B</to><activation>1.000000</activation><stream>1.000000</stream></link><link><to>exec100003.B</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>III.A</id><type>neuron</type><x>400</x><y>300</y><threshold>-255.000000</threshold><links><link><to>III.C</to><stream>1.000000</stream></link></links></node><node><id>III.B</id><type>neuron</type><x>300</x><y>300</y><threshold>-255.000000</threshold><links><link><to>III.C</to><stream>-1.000000</stream></link></links></node><node><id>III.C</id><type>neuron</type><x>200</x><y>300</y><output>1</output><links></links></node><node><id>III2.A</id><type>neuron</type><x>100</x><y>300</y><threshold>-255.000000</threshold><links><link><to>III2.C</to><stream>1.000000</stream></link></links></node><node><id>III2.B</id><type>neuron</type><x>0</x><y>300</y><threshold>-255.000000</threshold><links><link><to>III2.C</to><stream>-1.000000</stream></link></links></node><node><id>III2.C</id><type>neuron</type><x>600</x><y>200</y><links></links></node><node><id>III3.A</id><type>neuron</type><x>500</x><y>200</y><threshold>-255.000000</threshold><links><link><to>III3.C</to><stream>1.000000</stream></link></links></node><node><id>III3.B</id><type>neuron</type><x>400</x><y>200</y><threshold>-255.000000</threshold><links><link><to>III3.C</to><stream>-1.000000</stream></link></links></node><node><id>III3.C</id><type>neuron</type><x>300</x><y>200</y><links></links></node><node><id>III4.A</id><type>neuron</type><x>200</x><y>200</y><threshold>-255.000000</threshold><links><link><to>III4.C</to><stream>1.000000</stream></link></links></node><node><id>III4.B</id><type>neuron</type><x>100</x><y>200</y><threshold>-255.000000</threshold><links><link><to>III4.C</to><stream>-1.000000</stream></link></links></node><node><id>III4.C</id><type>neuron</type><x>0</x><y>200</y><links></links></node><node><id>X</id><type>neuron</type><x>600</x><y>100</y><threshold>-255.000000</threshold><input>1</input><links><link><to>X_X.#100004</to><activation>1.000000</activation><stream>1.000000</stream></link><link><to>I.A</to><activation>1.000000</activation><stream>1.000000</stream></link><link><to>I.B</to><activation>1.000000</activation><stream>1.000000</stream></link><link><to>X.#neg</to><stream>-1.000000</stream></link></links></node><node><id>X.#neg</id><type>neuron</type><x>500</x><y>100</y><links><link><to>X_X.#100004</to><activation>1.000000</activation></link></links></node><node><id>X_X.#100004</id><type>multiplicator</type><x>400</x><y>100</y><links><link><to>1_0_X_X_2_0_Y_m1_0_Z.#100004.#delay100005.#delay100006.#100007</to><stream>1.000000</stream></link></links></node><node><id>Y</id><type>neuron</type><x>300</x><y>100</y><threshold>-255.000000</threshold><input>1</input><links><link><to>II.B</to><activation>1.000000</activation><stream>1.000000</stream></link><link><to>Y.#delay100005</to><stream>1.000000</stream></link></links></node><node><id>Y.#delay100005</id><type>neuron</type><x>200</x><y>100</y><links><link><to>1_0_X_X_2_0_Y_m1_0_Z.#100004.#delay100005.#delay100006.#100007</to><stream>2.000000</stream></link></links></node><node><id>Z</id><type>neuron</type><x>100</x><y>100</y><threshold>-255.000000</threshold><input>1</input><links><link><to>III.B</to><activation>1.000000</activation><stream>1.000000</stream></link><link><to>III2.B</to><activation>1.000000</activation><stream>1.000000</stream></link><link><to>III3.B</to><activation>1.000000</activation><stream>1.000000</stream></link><link><to>III4.B</to><activation>1.000000</activation><stream>1.000000</stream></link><link><to>Z.#delay100006</to><stream>1.000000</stream></link></links></node><node><id>Z.#delay100006</id><type>neuron</type><x>0</x><y>100</y><links><link><to>1_0_X_X_2_0_Y_m1_0_Z.#100004.#delay100005.#delay100006.#100007</to><stream>-1.000000</stream></link></links></node><node><id>exec100002.A</id><type>neuron</type><x>600</x><y>0</y><threshold>-255.000000</threshold><links><link><to>exec100002.C</to><stream>1.000000</stream></link></links></node><node><id>exec100002.B</id><type>neuron</type><x>500</x><y>0</y><threshold>-255.000000</threshold><links><link><to>exec100002.C</to><stream>1.000000</stream></link></links></node><node><id>exec100002.C</id><type>neuron</type><x>400</x><y>0</y><links><link><to>III.A</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>exec100003.A</id><type>neuron</type><x>300</x><y>0</y><threshold>-255.000000</threshold><links><link><to>exec100003.C</to><stream>1.000000</stream></link></links></node><node><id>exec100003.B</id><type>neuron</type><x>200</x><y>0</y><threshold>-255.000000</threshold><links><link><to>exec100003.C</to><stream>1.000000</stream></link></links></node><node><id>exec100003.C</id><type>neuron</type><x>100</x><y>0</y><links><link><to>III4.A</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node><node><id>value100001</id><type>neuron</type><x>0</x><y>0</y><constant>2.000000</constant><links><link><to>II.A</to><activation>1.000000</activation><stream>1.000000</stream></link></links></node></nodes>",
                         SquareLayout().layout(g).to_xml())


class VariableTest(DatabaselessTestCase):
    def test_build(self):
        v1 = grammar.Variable.build(u"A")
        self.assertFalse(v1.glob)
        self.assertEqual(v1.name, u"A")
        self.assertEqual(len(v1.indexvars), 0)
        self.assertIsNone(v1.subvar)

        v1 = grammar.Variable.build(u"A.B")
        self.assertFalse(v1.glob)
        self.assertEqual(v1.name, u"A")
        self.assertEqual(len(v1.indexvars), 0)
        self.assertIsNotNone(v1.subvar)
        self.assertFalse(v1.subvar.glob)
        self.assertEqual(v1.subvar.name, u"B")
        self.assertEqual(len(v1.subvar.indexvars), 0)
        self.assertIsNone(v1.subvar.subvar)

        v1 = grammar.Variable.build(u".A.B")
        self.assertTrue(v1.glob)
        self.assertEqual(v1.name, u"A")
        self.assertEqual(len(v1.indexvars), 0)
        self.assertIsNotNone(v1.subvar)
        self.assertFalse(v1.subvar.glob)
        self.assertEqual(v1.subvar.name, u"B")
        self.assertEqual(len(v1.subvar.indexvars), 0)
        self.assertIsNone(v1.subvar.subvar)

        v1 = grammar.Variable.build(u"A[X].B")
        self.assertFalse(v1.glob)
        self.assertEqual(v1.name, u"A")
        self.assertEqual(len(v1.indexvars), 1)
        self.assertFalse(v1.indexvars[0].glob)
        self.assertEqual(v1.indexvars[0].name, u"X")
        self.assertEqual(len(v1.indexvars[0].indexvars), 0)
        self.assertIsNone(v1.indexvars[0].subvar)
        self.assertIsNotNone(v1.subvar)
        self.assertFalse(v1.subvar.glob)
        self.assertEqual(v1.subvar.name, u"B")
        self.assertEqual(len(v1.subvar.indexvars), 0)
        self.assertIsNone(v1.subvar.subvar)

        v1 = grammar.Variable.build(u"A[X.X].B")
        self.assertEqual(v1.name, u"A")
        self.assertEqual(len(v1.indexvars), 1)
        self.assertEqual(v1.indexvars[0].name, u"X")
        self.assertEqual(len(v1.indexvars[0].indexvars), 0)
        self.assertIsNotNone(v1.indexvars[0].subvar)
        self.assertEqual(v1.indexvars[0].subvar.name, u"X")
        self.assertEqual(len(v1.indexvars[0].subvar.indexvars), 0)
        self.assertIsNone(v1.indexvars[0].subvar.subvar)
        self.assertIsNotNone(v1.subvar)
        self.assertEqual(v1.subvar.name, u"B")
        self.assertEqual(len(v1.subvar.indexvars), 0)
        self.assertIsNone(v1.subvar.subvar)

        v1 = grammar.Variable.build(u"A[X[Y].X[I]].B")
        self.assertEqual(v1.name, u"A")
        self.assertEqual(len(v1.indexvars), 1)
        self.assertEqual(v1.indexvars[0].name, u"X")
        self.assertEqual(len(v1.indexvars[0].indexvars), 1)
        self.assertEqual(v1.indexvars[0].indexvars[0].name, u"Y")
        self.assertEqual(len(v1.indexvars[0].indexvars[0].indexvars), 0)
        self.assertIsNone(v1.indexvars[0].indexvars[0].subvar)
        self.assertIsNotNone(v1.indexvars[0].subvar)
        self.assertEqual(v1.indexvars[0].subvar.name, u"X")
        self.assertEqual(len(v1.indexvars[0].subvar.indexvars), 1)
        self.assertEqual(v1.indexvars[0].subvar.indexvars[0].name, u"I")
        self.assertEqual(len(v1.indexvars[0].subvar.indexvars[0].indexvars), 0)
        self.assertIsNone(v1.indexvars[0].subvar.indexvars[0].subvar)
        self.assertIsNone(v1.indexvars[0].subvar.subvar)
        self.assertIsNotNone(v1.subvar)
        self.assertEqual(v1.subvar.name, u"B")
        self.assertEqual(len(v1.subvar.indexvars), 0)
        self.assertIsNone(v1.subvar.subvar)

        try:
            grammar.Variable.build(u"A[X[Y.X[I.B")
            self.fail("Expecting bracket error")
        except GrammarError:
            pass

        try:
            grammar.Variable.build(u"A]")
            self.fail("Expecting bracket error")
        except GrammarError:
            pass

        try:
            grammar.Variable.build(u"A..C")
            self.fail("Expecting naming error")
        except GrammarError:
            pass

        v1 = grammar.Variable.build(u"1")
        self.assertEqual(v1, 1)

        v1 = grammar.Variable.build(u"A[1]")
        self.assertEqual(v1.name, u"A")
        self.assertEqual(len(v1.indexvars), 1)
        self.assertEqual(v1.indexvars[0], 1)
        self.assertIsNone(v1.subvar)

    def test_replace(self):
        #A = (A -> B) = B
        self.assertEqual(u"B", unicode(grammar.Variable.replace(
            grammar.Variable.build(u"A"), grammar.Variable.build(u"A"), grammar.Variable.build(u"B"))))
        #A.C = (A -> B) = B.C
        self.assertEqual(u"B.C", unicode(grammar.Variable.replace(
            grammar.Variable.build(u"A.C"), grammar.Variable.build(u"A"), grammar.Variable.build(u"B"))))

        #A.C = (A.C -> A.B) = A.B
        self.assertEqual(u"A.B", unicode(grammar.Variable.replace(
            grammar.Variable.build(u"A.C"), grammar.Variable.build(u"A.C"), grammar.Variable.build(u"A.B"))))

        #A.C = (A -> A.B) = A.B.C
        self.assertEqual(u"A.B.C", unicode(grammar.Variable.replace(
            grammar.Variable.build(u"A.C"), grammar.Variable.build(u"A"), grammar.Variable.build(u"A.B"))))

        #A.C = (A -> D.B) = D.B.C
        self.assertEqual(u"D.B.C", unicode(grammar.Variable.replace(
            grammar.Variable.build(u"A.C"), grammar.Variable.build(u"A"), grammar.Variable.build(u"D.B"))))

        #A.B.C = (A.B -> A.D) = A.D.C
        self.assertEqual(u"A.D.C", unicode(grammar.Variable.replace(
            grammar.Variable.build(u"A.B.C"), grammar.Variable.build(u"A.B"), grammar.Variable.build(u"A.D"))))

        #A.B.C = (A.B.C -> A.D.C.E) = A.D.C.E
        self.assertEqual(u"A.D.C.E", unicode(grammar.Variable.replace(
            grammar.Variable.build(u"A.B.C"), grammar.Variable.build(u"A.B.C"), grammar.Variable.build(u"A.D.C.E"))))

        #A.C[N] = (A.C[N] -> D.A.C[N]) = D.A.C[N]
        self.assertEqual(u"D.A.C[N]", unicode(grammar.Variable.replace(
            grammar.Variable.build(u"A.C[N]"), grammar.Variable.build(u"A.C[N]"), grammar.Variable.build(u"D.A.C[N]"))))

        #A[M].C[N] = (A[M].C -> D.A[M].B) = D.A[M].B[N]
        self.assertEqual(u"D.A[M].B[N]", unicode(grammar.Variable.replace(
            grammar.Variable.build(u"A[M].C[N]"), grammar.Variable.build(u"A[M].C"), grammar.Variable.build(u"D.A[M].B"))))

        #A[N].C = (A -> D.A) = D.A[N].C
        self.assertEqual(u"D.A[N].C", unicode(grammar.Variable.replace(
            grammar.Variable.build(u"A[N].C"), grammar.Variable.build(u"A"), grammar.Variable.build(u"D.A"))))

        #A.B[N].C = (A.B[N].C -> A.D.C.E) = A.D.C.E
        self.assertEqual(u"A.D.C.E", unicode(grammar.Variable.replace(
            grammar.Variable.build(u"A.B[N].C"), grammar.Variable.build(u"A.B[N].C"), grammar.Variable.build(u"A.D.C.E"))))

        #A.B[N].A.B = (A.B -> A.D) = A.D[N].A.B
        self.assertEqual(u"A.D[N].A.B", unicode(grammar.Variable.replace(
            grammar.Variable.build(u"A.B[N].A.B"), grammar.Variable.build(u"A.B"), grammar.Variable.build(u"A.D"))))
