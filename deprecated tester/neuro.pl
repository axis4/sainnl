#CopyLeft (c) 2008 IoS progsdi@gmail.com
#
#Command line:
#	debug - maybe, but only the first - create detailed logs
#
#SHOULD BE SET (NO DEFAULT VALUE PRESENT)
#	maintype - type of network to apply
#	filein - filein value for Input::Image - input image file
#	fileout - fileout value for Output::Image - output image file
#	width - width value for Generator::Oscillator - width of neurons' matrix
#	height - height value for Generator::Oscillator - height of neurons' matrix
#
#MAY BE SET AS CONFIGURATION (NOT SET = DEFAULT)
#	countereps - EPS value for Counter::Oscillator - epsilon for winners' estimation
#	neuronv - v value for Neuron::Oscillator - v potencial = the value neuron oscillates with
#	neuronw - w value for Neuron::Oscillator - w gate voltage = the value to modify v
#	neuronthreshold - threshold value for Neuron::Oscillator - threshold for neuron to oscillate
#	generatorsat - SAT value for Generator::Oscillator - saturation
#	generatorchargerate - CR value for Generator::Oscillator - charge rate of neurons
#	generatordischargerate - DR value for Generator::Oscillator - discharge rate of neurons
#	animate - animate value for Network::Oscillator and Output::Image - force system to create a frame in image on each step
#
#SHOULD NOT BE SET (SHOULD STAY DEFAULT)
#	counterbeta - BETA value for Counter::Oscillator - BETA coefficient
#	generatordist - DIST value for Generator::Oscillator - radius of neighbours
#	maximumsteps - maximumsteps value for Counter::Oscillator - number of steps before break of oscillator
#(ni)	classnumber - classnumber value for Counter::Oscillator - number of classes required to break oscillating
#	neuronalpha - ALPHA value for Neuron::Oscillator - ALPHA coefficient
#	neuronbeta - BETA value for Neuron::Oscillator - BETA coefficient
#	neuronc - C value for Neuron::Oscillator - C coefficient

use NN::Generator;
use NN::Input;
use NN::Output;
use NN::Neuron;
use NN::Link;
use NN::Network;

#use Image::Magick;

$|=1;

my $deb=$ARGV[0];
if($deb=~/^debug|trace/)
{
	shift @ARGV;
	open LL,">>log.sew";
	print LL "Log Started\n";
	close LL;
}else{
	$deb=0;
}

my %h=();
for(@ARGV)
{
	if(/^(.*?)=(.*)$/)
	{
		$h{$1}=$2;
	}
}

my $net;
eval "use NN::Network::".$h{maintype}.";\n\$net=new NN::Network::".$h{maintype}.";";
if($@)
{
	die "Network create failed. $@";
}
my $fh;

$net->{debug}=$deb;
$net->{params}=\%h;

if($deb=~/^debug/)
{
	open LL,">>log.sew";
	print LL "Network $net\n";
	close LL;
}

print STDOUT localtime()." Started\n";

my $created=0;

if(open $fh,"config.sew")
{
	$nnet=$net->readstructure($fh);
	if(!$nnet->{build})
	{
		testnetwork($net);
		$created=1;
	}else{
		$net=$nnet;
	}
}else{
	testnetwork($net);
	$created=1;
}

$net->{debug}=$deb;
$net->{params}=\%h;

if($deb=~/^debug/)
{
	open LL,">>log.sew";
	print LL "Structure ready\n";
	close LL;
}

print STDOUT localtime()." Created\n";

if(0 && $created)
{
	if(open $fh,">config.sew")
	{
		$net->writestructure($fh);
		close $fh;
	}else{
		print STDERR "Network cannot save it's state to file 'config.sew': $!";
	}

	if($deb=~/^debug|trace/)
	{
		open LL,">>log.sew";
		print LL "Structure saved\n";
		close LL;
	}
}

print STDOUT localtime()." Can break\n";

$net->perform;

print STDOUT "\n";

if($deb=~/^debug|trace/)
{
	open LL,">>log.sew";
	print LL "Performance applied\n";
	close LL;
}

=merger
if(!$h{animate})
{
	$h{fileout}=~/(.*[\\\/])(.+)/;
	my $newname=$1.'merge.'.$2;
	my $nimage=new Image::Magick();
	$nimage->Read($h{fileout});
	$nimage->Quantize(colorspace=>'gray');
	my $iimage=new Image::Magick();
	$iimage->Read($h{filein});
	$iimage->Quantize(colorspace=>'gray');
	$iimage->Composite(image=>$nimage,compose=>"Over",gravity=>"Center",dissolve=>30,opacity=>40);
	$iimage->Write($newname);
	undef $nimage;
	undef $iimage;

	if($deb=~/^debug|trace/)
	{
		open LL,">>log.sew";
		print LL "Images merged\n";
		close LL;
	}
}
=cut

=selfkeep
if(open $fh,">config.sew")
{
	$net->writestructure($fh);
	close $fh;
}else{
	print STDERR "Network cannot save it's state to file 'config.sew': $!";
}
=cut

if($deb=~/^debug|trace/)
{
	open LL,">>log.sew";
	print LL "Structure saved\n";
	print LL "Log stopped\n";
	close LL;
}

sub testnetwork
{
	my $net=shift;
	my $gen;
	eval "use NN::Generator::".$h{maintype}.";\n\$gen=new NN::Generator::".$h{maintype}."(params=>\$net->{params});";
	if($@)
	{
		die "Failed to generate network structure. $@";
	}
	$gen->{debug}=$deb;
	$gen->perform($net);
	if($deb=~/^debug|trace/)
	{
		open LL,">>log.sew";
		print LL "Generating done\n";
		close LL;
	}
}