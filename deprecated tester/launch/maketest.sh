cd ..
if [ 1 -lt "$#" ]
then
	if [ "stdin" = "$2" ]
	then
		echo "starting $1 stdin"
		perl call.pl $1 launch/conf.txt
	else
		echo "starting $1 file $2"
		perl call.pl $1 launch/conf.txt < launch/$2
	fi
else
	if [ "stdin" = "$1" ]
	then
		echo "starting stdin"
		perl call.pl launch/conf.txt
	else
		echo "starting file $1"
		perl call.pl launch/conf.txt < launch/$1
	fi
fi
