my $name="";

my $tv=undef;
my $debug="";
if(@ARGV > 0)
{
	if($ARGV[0]=~/^debug|trace/)
	{
		$debug=shift @ARGV;
	}
	if(@ARGV > 1)
    {
		my $ethalon=shift @ARGV;

		if(open FF,$ethalon)
        {
			binmode(FF);
			my @e=<FF>;
			$tv="";
			for(@e)
            {
				s/^\s+|\s+$//g;
				$tv.="$_";
			}
			close FF;
		}
		if(@ARGV > 1)
        {
			$name=shift @ARGV;
		}
	}
	$conf=shift @ARGV;
} else {
	die "Usage: call.pl [debug|trace] [ethalonfile [testname]] configurationfile [< input]\n";
}

open FF,"$conf" or die "$conf $!";
my @a=<FF>;
close FF;
my $a="@a";
$a=~s/\n/ \| /g;

#if was ethalon then it is testing mode
if(defined $tv)
{
    my @res=split(/\n/,`perl neuro.pl $debug maintype=Flow "neurons=$a"`);
    my $res="";
    for(@res)
    {
        #cut logging
        if(!/^\w{3}\s+\w{3}\s+\d+\s+\d{2}\:\d{2}\:\d{2}\s+\d{4}\s+/)
        {
            s/^\s+|\s+$//g;
            if(!/^$/)
            {
                $res.="$_";
            }
        }
    }

	if($res eq $tv)
    {
		print "$name passed\n";
	} else {
		open V1,">v1.res" or die "$!";
		print V1 $res;
		close V1;
		open V2,">v2.res" or die "$!";
		print V2 $tv;
		close V2;
		$res=~s/(.)/sprintf("%X",ord($1))/eg;
		$tv=~s/(.)/sprintf("%X",ord($1))/eg;
		print "$name failed\nExpected:\n$tv\nActual:\n$res\n";
	}
} else {
	exec "perl neuro.pl $debug maintype=Flow \"neurons=$a\"";
}
