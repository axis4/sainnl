package NN::Network::Simple;

use strict;
use NN::Neuron;
use NN::Link;

BEGIN{
	use NN::Network;
	our ($VERSION,@ISA);

	$VERSION=1.0;

	@ISA=qw(NN::Network);
}

sub new
{
	my $class=shift;
	my %params=@_;
	my $self={};
	bless $self,$class;
	$self->{neurons}={};
	$self->{links}={};
	for(keys %params)
	{
		$self->{$_}=$params{$_};
	}
	$self;
}

sub perform
{
	my $self=shift;
	if($self->{input})
	{
		$self->{input}->perform($self->neurons,$self->links);
	}
	if($self->{counter})
	{
		$self->{counter}->perform($self->neurons,$self->links);
	}
	if($self->{output})
	{
		$self->{output}->perform($self->neurons,$self->links);
	}
}

1;