package NN::Network::Flow;

use strict;
use NN::Neuron;
use NN::Link;
use IO::Uncompress::Gunzip qw(gunzip $GunzipError);

our $DATA=[];

BEGIN{
	use NN::Network;
	our ($VERSION,@ISA);

	$VERSION=1.0;

	@ISA=qw(NN::Network);
}

sub new
{
	my $class=shift;
	my %params=@_;
	my $self={};
	bless $self,$class;
	$self->{neurons}={};
	$self->{links}={};
	for(keys %params)
	{
		$self->{$_}=$params{$_};
	}
	$self;
}

sub perform
{
	my $self=shift;
	my $counterres=0;
	my $deb=$self->{debug};

	if($deb=~/^debug/)
	{
		print STDERR localtime()." Performance initiated\n";
	}

	if($deb=~/^debug/)
	{
		open LL,">>log.sew";
		print LL "Network started\n";
		print LL "DATA: self=$self\n";
		close LL;
	}

	if($deb=~/^debug/)
	{
		print STDERR localtime()." N".scalar(keys %{$self->neurons})."\n";
	}

	my $n=0;
	$self->{counter}->{debug}=$deb;
	while(!$counterres)
	{
		$n++;
		if($deb=~/^debug/)
		{
			print STDERR localtime()." Counting step $n\n";
		}
		$self->{input}->perform($self->neurons,$self->links);
		$counterres=$self->{counter}->perform($self->neurons,$self->links);
		$self->{output}->perform($self->neurons,$self->links);
		if($deb=~/^debug/)
		{
			if($deb=~/^debug:(.*)$/)
			{
				my $dia=$1;
				my $output;
				gunzip $dia => \$output or die "unzip failed: $GunzipError\n";
				for my $k (sort {$a cmp $b} keys %{$self->{neurons}})
				{
					my $n=$self->{neurons}->{$k};
					my $id=$n->{id};
					my $as=$n->{activesum};
					my $fs=$n->{flowsum};
					my $gs=$n->{generatesum};
					$output=~s/##$id(\s+)/#$as\|$fs\|$gs$1/sg;
				}
				$dia=~/^(.*\\)/;
				my $f="${1}step_$n.dia";
				my $fi="${1}step_$n.png";
				open TMP,">$f" or die "$f error: $!";
				print TMP $output;
				close TMP;
				if(-e "C:\\Program Files\\Dia\\bin\\dia.exe") {#win
					`"C:\\Program Files\\Dia\\bin\\dia.exe" -e $fi -t cairo-alpha-png -n -l $f`;
				} elsif(-e "C:\\Program Files (x86)\\Dia\\bin\\dia.exe") {#win x64
					`"C:\\Program Files (x86)\\Dia\\bin\\dia.exe" -e $fi -t cairo-alpha-png -n -l $f`;
				} elsif(-e "/Applications/Dia.app/Contents/Resources/bin/dia") {#mac
					`/Applications/Dia.app/Contents/Resources/bin/dia -e $fi -t cairo-alpha-png -n -l $f`;
				} else {
					#Well... nothing
				}
			}
		}
	}

	if($deb=~/^debug/)
	{
		open LL,">>log.sew";
		print LL "Finished in $n steps\n";
		close LL;
	}

	if($deb=~/^debug/)
	{
		print STDERR localtime()." Performance finished\n";
	}
}

1;