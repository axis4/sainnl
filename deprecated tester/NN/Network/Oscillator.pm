package NN::Network::Oscillator;

use strict;
use NN::Neuron;
use NN::Link;

our $DATA=[];

BEGIN{
	use NN::Network;
	our ($VERSION,@ISA);

	$VERSION=1.0;

	@ISA=qw(NN::Network);
}

sub new
{
	my $class=shift;
	my %params=@_;
	my $self={};
	bless $self,$class;
	$self->{neurons}={};
	$self->{links}={};
	for(keys %params)
	{
		$self->{$_}=$params{$_};
	}
	$self;
}

sub perform
{
	my $self=shift;
	my $counterres=0;
	my $deb=$self->{debug};

	print STDOUT localtime()." Performance initiated\n";

	if($deb=~/^debug/)
	{
		open LL,">>log.sew";
		print LL "Network started\n";
		print LL "DATA: self=$self\n";
		close LL;
	}

	print STDOUT localtime()." N".scalar(keys %{$self->neurons})."\n";

	$self->{input}->{debug}=$deb;
	$self->{input}->perform($self->neurons,$self->links);


	print STDOUT localtime()." N".scalar(keys %{$self->neurons})."\n";

	if($deb=~/^debug/)
	{
		open LL,">>log.sew";
		print LL "Image received\n";
		close LL;
	}

	$DATA=[];
	for(keys %{$self->neurons})
	{
		push @$DATA,{who=>$_,value=>$self->neurons->{$_}->{sum}};
	}

	print STDOUT localtime()." Image loaded\n";
	print STDOUT localtime()." N".scalar(keys %{$self->neurons})."\n";

	my $n=0;
	$self->{counter}->{debug}=$deb;
	while(!$counterres)
	{
		$n++;
		print STDOUT localtime()." Counting step $n\n";
		for(@$DATA)
		{
			$self->neurons->{$_->{'who'}}->{sum}=$_->{'value'};
		}
		$counterres=$self->{counter}->perform($self->neurons,$self->links);
		if($self->{params}->{animate})
		{
			$self->draw($n-1);
		}
	}

	if($deb=~/^debug/)
	{
		open LL,">>log.sew";
		print LL "Oscillated in $n steps\n";
		close LL;
	}

	$self->draw($n-1);

	print STDOUT localtime()." Performance finished\n";
}

sub draw
{
	my $self=shift;
	my $deb=$self->{debug};
	my $frameindex=shift;

	my %mark=();
	my $i=0;
	for(keys %{$self->neurons})
	{
		if(!$mark{$self->neurons->{$_}->{mark}})
		{
			$mark{$self->neurons->{$_}->{mark}}=$i;
			$i++;
		}
	}
	$i--;

	for(keys %{$self->neurons})
	{
		if($i > 0)
		{
			$self->neurons->{$_}->{sum}=$mark{$self->neurons->{$_}->{mark}}/$i;
		}else{
			$self->neurons->{$_}->{sum}=0;
		}
	}
	if($i <= 0)
	{
		print STDOUT localtime()." Strange answer, all black\n";
	}

	print STDOUT localtime()." Image prepared\n";

	$self->{output}->{debug}=$deb;
	$self->{output}->perform($self->neurons,$self->links,$frameindex);

	if($deb=~/^debug/)
	{
		open LL,">>log.sew";
		print LL "Image saved\n";
		close LL;
	}
}

sub readstructure
{
	my $self=shift;
	my $filehandle=shift||0;
	if($filehandle)
	{
		my $myclass=0;
		eval{
			my @res=<$filehandle>;
			my $res="@res";
			my $dump=new NN::XML::Dumper;
			if($self->{debug}=~/^debug/)
			{
				open LL,'>>log.sew';
				print LL "XML::Dumper prepared to read XML\n";
				close LL;
			}
			$myclass=$dump->xml2pl($res);
			my $n={};
			my @k=sort {$a <=> $b} keys %{$myclass->neurons};
			my $p=0;
			for(@k)
			{
				if(($myclass->neurons->{$_}->{v} || $myclass->neurons->{$_}->{w} || $myclass->neurons->{$_}->{sum}) && $_ == $p+1)
				{
					$n->{$_}=$myclass->neurons->{$_};
				}
				$p=$_;
			}
			$myclass->{neurons}=$n;
			print STDOUT "Received ".scalar(keys %{$myclass->neurons})." neurons\n";
			if($self->{debug}=~/^debug/)
			{
				open LL,'>>log.sew';
				print LL "XML::Dumper read XML\n";
				print LL "Received ".scalar(keys %{$myclass->neurons})." neurons\n";
				close LL;
			}
			my $m=0;
			for(keys %{$myclass->{neurons}})
			{
				if($m < $_)
				{
					$m=$_;
				}
			}
			NN::Neuron::setstartid($m);
			$myclass->{build}=1;
		};
		return $myclass;
	}else{
		return NN::Network->new();
	}
}

sub writestructure
{
	my $self=shift;
	my $filehandle=shift||0;
	if($self->{debug}=~/^debug/)
	{
		open LL,'>>log.sew';
		print LL "Creating XML::Dumper\n";
		close LL;
	}
	my $dump=new NN::XML::Dumper;
	$dump->dtd;
	if($self->{debug}=~/^debug/)
	{
		open LL,'>>log.sew';
		print LL "XML::Dumper prepared to create XML\n";
		close LL;
	}
	my $res=$dump->pl2xml($self);
	if($self->{debug}=~/^debug/)
	{
		open LL,'>>log.sew';
		print LL "XML::Dumper created XML\n";
		close LL;
	}
	if($filehandle)
	{
		print $filehandle $res;
	}else{
		print $res;
	}
}

1;