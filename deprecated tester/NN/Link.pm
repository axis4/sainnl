package NN::Link;

use strict;
use NN::Network;
use NN::Neuron;
use NN::Link::Simple;

BEGIN{
	our ($VERSION,@ISA);

	$VERSION=1.0;

	@ISA=qw();
}

my $ids=0;

sub new
{
	my $class=shift;
	my $from=shift;
	my $to=shift;
	my %params=@_;
	my $self={};
	bless $self,"NN::Link::Simple";
	$self->{from}=$from;
	$self->{to}=$to;
	$self->{id}=$self->getid();
	for(keys %params)
	{
		$self->{$_}=$params{$_};
	}
	$self;
}

sub setstartid
{
	$ids=shift;
}

sub getid
{
	$ids++;
	return $ids;
}

sub stream
{
	return;
}

1;