package NN::Output::Image;

use strict;
use NN::Neuron;
use NN::Link;
use Image::Magick;

BEGIN{
	use NN::Output;
	our ($VERSION,@ISA);

	$VERSION=1.0;

	@ISA=qw(NN::Output);
}

sub new
{
	my $class=shift;
	my %params=@_;
	my $self={};
	bless $self,$class;
	$self->{id}=$self->getid();
	for(keys %params)
	{
		$self->{$_}=$params{$_};
	}
	$self;
}

sub perform
{
	my $self=shift;
	my $neurons=shift;
	my $links=shift;
	my $frameindex=shift||0;
	if(!$self->{params}->{animate})
	{
		$frameindex=0;
	}
	
	my $im=new Image::Magick();
	$im->Set(size=>'100x100');
	my $last=new Image::Magick();
	$last->Set(size=>'100x100');
	$last->ReadImage('xc:white');
	if($frameindex > 0)
	{
		$im->ReadImage($self->{params}->{fileout});
		print localtime(time).' '.scalar(@$im)." frames found\n";
	}
	push @$im,$last;
	for(keys %$neurons)
	{
		my $x=$neurons->{$_}->{x};
		my $y=$neurons->{$_}->{y};
		my $v=$neurons->{$_}->{sum};
		if($self->{debug})
		{
			open LL,'>>log.sew';	
			print LL "$x,$y: $v\n";
			close LL;
		}
		$last->SetPixel(x=>$x,y=>$y,channel=>'RGB',color=>[$v,$v,$v]);
	}
	$last->Quantize(colorspace=>'gray');
	$last->Set('magick'=>'GIF');
	$im->Set(delay=>100);
	$im->Set(loop=>0);
	my $res=$im->Coalesce();
	$res->Write($self->{params}->{fileout});
	undef $res;
	if($last != $im)
	{
		undef $last;
	}
	undef $im;
}

1;