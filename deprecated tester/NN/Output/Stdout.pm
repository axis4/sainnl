package NN::Output::Stdout;

use strict;
use NN::Neuron;
use NN::Link;
require NN::Neuron::Flow;

BEGIN{
	use NN::Output;
	our ($VERSION,@ISA);

	$VERSION=1.0;

	@ISA=qw(NN::Output);
}

sub new
{
	my $class=shift;
	my %params=@_;
	my $self={};
	bless $self,$class;
	$self->{id}=$self->getid();
	for(keys %params)
	{
		$self->{$_}=$params{$_};
	}
	binmode(STDOUT);
	$self;
}

sub compare_indexes
{
	my ($v1,$v2)=@_;
	my $res=0;
	for my $i (0..($#{$v1},$#{$v2})[$#{$v1} < $#{$v2}])
	{
		if($i > $#{$v1})
		{
			$res=1;
		}
		if($i > $#{$v2})
		{
			$res=-1;
		}
		if(!$res)
		{
			if($v1->[$i]=~/^\d+$/)
			{
				if($v2->[$i]=~/^\d+$/)
				{
					$res=int($v1->[$i])<=>int($v2->[$i]);
				}
			}
		}
		if(!$res)
		{
			$res=int($v1->[$i]) cmp int($v2->[$i]);
		}
		if($res)
		{
			last;
		}
	}
	return $res;
}

sub compare_vars
{
	my ($v1,$v2)=@_;
	if(ref $v1)
	{
		if(ref $v2)
		{
			my $res=$v1->{glob} <=> $v2->{glob};
			if(!$res)
			{
				$res=$v1->{name} cmp $v2->{name};
			}
			if(!$res)
			{
				$res=compare_indexes($v1->{indexes},$v2->{indexes});
			}
			if(!$res)
			{
				$res=compare_vars($v1->{subvar},$v2->{subvar});
			}
			return $res;
		}
		return -1;
	}
	if(ref $v2)
	{
		return 1;
	}
	return 0;
}

sub compare
{
	return compare_vars(NN::Neuron::Flow::extract_name($_[0]), NN::Neuron::Flow::extract_name($_[1]));
}

sub perform
{
	my $self=shift;
	my $neurons=shift;
	my $links=shift;
	if($self->{debug}=~/^debug/)
	{
		print STDERR "Output vector\n";
	}
	for(sort {compare($a,$b)} keys %$neurons)
	{
		if(defined($self->{neurons}->{$neurons->{$_}}))
		{
			if(!$neurons->{$_}->{expected} && !$neurons->{$_}->{template})
			{
				if($self->{debug}=~/^debug/)
				{
					print STDERR "$_=".$neurons->{$_}->{sum}." ";
				}
				my $o=int($neurons->{$_}->{sum});
				if($o < 0)
				{
					$o+=256;
				}
				print STDOUT chr($o);
            }
		}
	}
	if($self->{debug}=~/^debug/)
	{
		print STDERR "\n";
	}
}

1;