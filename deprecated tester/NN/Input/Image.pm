package NN::Input::Image;

use strict;
use NN::Neuron;
use NN::Link;
use Image::Magick;

BEGIN{
	use NN::Input;
	our ($VERSION,@ISA);

	$VERSION=1.0;

	@ISA=qw(NN::Input);
}

sub new
{
	my $class=shift;
	my %params=@_;
 	my $self={};
	bless $self,$class;
	$self->{id}=$self->getid();
	for(keys %params)
	{
		$self->{$_}=$params{$_};
	}
	$self;
}

sub perform
{
	my $self=shift;
	my $neurons=shift;
	my $links=shift;
	my $i=0;
	my $im=new Image::Magick();
	$im->Read($self->{params}->{filein});
	$im->Quantize(colorspace=>'gray');
	my $w=$im->Get('columns');
	my $h=$im->Get('rows');
	if($self->{debug}=~/^debug/)
	{
		open LL,">>log.sew";
		print LL "$w,$h: ".$im->Get('magick')."\n";
		close LL;
	}
	my @k=();
	my @qwe=keys %$neurons;
	@qwe=sort {$a <=> $b} @qwe;
	for(@qwe)
	{
		push @k,$neurons->{$_};
	}
	for my $x (0..$w-1)
	{
		for my $y (0..$h-1)
		{
			$,='|';
			if($self->{debug}=~/^debug/)
			{
				open LL,">>log.sew";
				print LL "$x,$y: ".$im->GetPixel(channel=>'Gray',x=>$x,y=>$y)."\n";
				close LL;
			}
			$k[$x*$h+$y]->{sum}=$im->GetPixel(channel=>'Gray',x=>$x,y=>$y);
			$k[$x*$h+$y]->{start}=$k[$x*$h+$y]->{sum};
		}
	}
	undef $im;
}

1;