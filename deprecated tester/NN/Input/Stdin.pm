package NN::Input::Stdin;

use strict;
use NN::Neuron;
use NN::Link;

BEGIN{
	use NN::Input;
	our ($VERSION,@ISA);

	$VERSION=1.0;

	@ISA=qw(NN::Input);
}

sub new
{
	my $class=shift;
	my %params=@_;
	my $self={};
	bless $self,$class;
	$self->{id}=$self->getid();
	for(keys %params)
	{
		$self->{$_}=$params{$_};
	}
	if($self->{debug}=~/^debug/)
	{
		print STDERR "INPUT VECTOR=".(defined($self->{params}->{input}) ? $self->{params}->{input} : "STDIN")."\n";
		for(keys %{$self->{neurons}})
		{
			print STDERR "INPUT NODE=".$_."\n";
		}
	}
	if (!defined($self->{params}->{input}))
	{
		binmode(STDIN);
	}
	$self;
}

sub perform
{
	my $self=shift;
	my $neurons=shift;
	my $links=shift;
	my $i=0;
	my $str="";
	for(sort {$a cmp $b} keys %$neurons)
	{
		$i++;
		if(defined($self->{neurons}->{$neurons->{$_}}))
		{
			my $v;
			if (defined $self->{params}->{input})
			{
				if(length($self->{params}->{input}))
				{
					($v,$self->{params}->{input})=($self->{params}->{input}=~/(.)(.*)/);
				}
			} else {
				if($self->{debug}=~/^debug/)
				{
					print STDERR "Reading input handler\n";
				}
				my $fh = $self->{params}->{input_handler} || \*STDIN;
				if (read($fh,$v,1) < 1)
				{
					undef $v;
				} else {
					$self->{params}->{stop}=1;
				}
			}
			if(!defined $v)
			{
				$v="\0";
			}
			$str.=sprintf(" $_=%04d",ord($v));
			$neurons->{$_}->{sum}=ord($v);
		}else{
			$str.=sprintf(" $_=%04d",0);
		}
	}
	if($self->{debug}=~/^debug/)
	{
		print STDERR "Input: $str\n";
	}
}

1;