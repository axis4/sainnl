package NN::Neuron;

use strict;
use NN::Network;
use NN::Link;
use NN::Neuron::Simple;

BEGIN{
	our ($VERSION,@ISA);

	$VERSION=1.0;

	@ISA=qw();
}

my $ids=0;

sub new
{
	my $class=shift;
	my %params=@_;
	my $self={};
	bless $self,"NN::Neuron::Simple";
	$self->{id}=$self->getid();
	for(keys %params)
	{
		$self->{$_}=$params{$_};
	}
	$self;
}

sub setstartid
{
	$ids=shift;
}

sub getid
{
	$ids++;
	return $ids;
}

sub addneuron
{
	return;
}

sub deleteneuron
{
	return;
}

sub resum
{
	my $self=shift;
	$self->{sum}=$self->{function}($self->{sum});
	return;
}

1;