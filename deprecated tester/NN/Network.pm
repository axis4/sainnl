package NN::Network;

use strict;
use NN::Network::Simple;
use NN::Neuron;
use NN::Link;
use NN::XML::Dumper;

BEGIN{
	our ($VERSION,@ISA);

	$VERSION=1.0;

	@ISA=qw();
}

sub new
{
	my $class=shift;
	my %params=@_;
	my $self={};
	bless $self,"NN::Network::Simple";
	$self->{neurons}={};
	$self->{links}={};
	for(keys %params)
	{
		$self->{$_}=$params{$_};
	}
	$self;
}

sub links
{
	my $self=shift;
	$self->{links};
}

sub neurons
{
	my $self=shift;
	$self->{neurons};
}

sub addneuron
{
	my $self=shift;
	my $who=shift;
	$self->neurons->{$who->{id}}=$who;
}

sub deleteneuron
{
	my $self=shift;
	my $who=shift;
	undef $self->neurons->{$who->{id}};
}

sub addlink
{
	my $self=shift;
	my @links=@_;
	for(@links)
	{
		$self->links->{$_->{id}}=$_;
	}
}

sub deletelink
{
	my $self=shift;
	my @links=@_;
	for(@links)
	{
		undef $self->links->{$_->{id}};
	}
}

sub find_incomes
{
    my $self=shift;
    my $nid=shift;
    my @res=();
    for(keys %{$self->links})
    {
        if($self->{links}->{$_}->{to}->{id} eq $nid)
        {
            push @res,$self->{links}->{$_};
        }
    }
    return @res;
}

sub readstructure
{
}

sub writestructure
{
}

sub perform
{
	return;
}

1;