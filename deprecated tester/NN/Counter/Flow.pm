package NN::Counter::Flow;

use strict;
use NN::Neuron;
use NN::Link;

BEGIN{
	use NN::Counter;
	our ($VERSION,@ISA);

	$VERSION=1.0;

	@ISA=qw(NN::Counter);
}

sub new
{
	my $class=shift;
	my %params=@_;
	my $self={};
	bless $self,$class;
	$self->{id}=$self->getid();
	for(keys %params)
	{
		$self->{$_}=$params{$_};
	}
	if (!defined $self->{stop})
	{
		$self->{input_stopped}=1;
	}
	$self;
}

sub perform
{
	my $self=shift;
	my $neurons=shift;
	my $links=shift;
	if ($self->{input_stopped})
	{
		if ($self->{params}->{stop})
		{
			$self->{stop}=$self->{params}->{stop};
			$self->{params}->{stop}=0;
		} else {
			$self->{stop}--;
		}
	} else {
		$self->{stop}--;
	}
	if($self->{debug}=~/^debug/)
	{
		open LL,'>>log.sew';
		print LL "Counter step started\n";
		print LL scalar(keys %{$neurons})." neurons in network\n";
		close LL;
	}
	#first run
	for(keys %{$links})
	{
        if(defined $links->{$_}->{to} && defined $links->{$_}->{from})
        {
            for my $a ('active','flow','generate')
            {
                $links->{$_}->{to}->{$a.'sum'}=0;
                $links->{$_}->{to}->{$a.'source'}={};
            }
        }
	}
	for(sort {($links->{$a}->{to}->{id} cmp $links->{$b}->{to}->{id}) or ($links->{$a}->{from}->{id} cmp $links->{$b}->{from}->{id})} keys %{$links})
	{
		$links->{$_}->{debug}=$self->{debug};
		$links->{$_}->stream();
	}

	#second run
	my $statestr="";
	for(keys %{$neurons})
	{
		$neurons->{$_}->{sum}=0;
	}
	for(sort {$a cmp $b} keys %{$neurons})
	{
		$neurons->{$_}->{debug}=$self->{debug};
		$neurons->{$_}->stream();
		if($neurons->{$_}->{sum} != 0) {
			$statestr.=sprintf(" $_=%04d",$neurons->{$_}->{sum});
		}
	}
	if (ref($self->{params}->{hook}) eq "CODE")
	{
		$self->{params}->{hook}->($self, $neurons, $links);
	}
	if($self->{debug}=~/^trace/)
	{
		print STDERR "State (".$self->{stop}."): $statestr\n";
    }
	if($self->{debug}=~/^debug/)
	{
		print STDERR "State: $statestr\n";
		print STDERR "Stop: ".$self->{stop}."<=".((scalar(keys %$neurons) + 1)*(-1))."\n";
	}

	if($self->{debug}=~/^debug/)
	{
		open LL,'>>log.sew';
		print LL "Counter step finished\n";
		close LL;
	}
	return $self->{stop} <= (scalar(keys %$neurons) + 1)*(-1);
}

1;