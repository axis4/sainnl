package NN::Counter::Simple;

use strict;
use NN::Neuron;
use NN::Link;

BEGIN{
	use NN::Counter;
	our ($VERSION,@ISA);

	$VERSION=1.0;

	@ISA=qw(NN::Counter);
}

sub new
{
	my $class=shift;
	my %params=@_;
	my $self={};
	bless $self,$class;
	$self->{id}=$self->getid();
	for(keys %params)
	{
		$self->{$_}=$params{$_};
	}
	$self;
}

sub perform
{
	my $self=shift;
	my $neurons=shift;
	my $links=shift;
	my $m=1;
	while($m)
	{
		$m=0;
		for(keys %{$neurons})
		{
			if($_->{sum})
			{
				$_->resum();
				for(keys %{$_->{links}})
				{
					$_->stream();
					$m=1;
				}
			}
		}
	}
	return;
}

1;