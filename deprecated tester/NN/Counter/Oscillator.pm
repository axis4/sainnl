package NN::Counter::Oscillator;

use strict;
use NN::Neuron;
use NN::Link;

my $EPS=3;
my $BETA=10;

BEGIN{
	use NN::Counter;
	our ($VERSION,@ISA);

	$VERSION=1.0;

	@ISA=qw(NN::Counter);
}

my $mark=1;

sub new
{
	my $class=shift;
	my %params=@_;
	my $self={};
	bless $self,$class;
	$self->{id}=$self->getid();
	$mark=1;
	for(keys %params)
	{
		$self->{$_}=$params{$_};
	}
	$EPS=$self->{params}->{countereps}||3;
	$BETA=$self->{params}->{counterbeta}||10;
	$self->{maximumsteps}=$self->{params}->{maximumsteps}||30;
	$self->{classnumber}=$self->{params}->{classnumber}||7;
	$self;
}

sub lastmark
{
	my $self=shift;
	return $mark-1;
}

sub perform
{
	my $self=shift;
	my $neurons=shift;
	my $links=shift;
	my $stop=0;
	if($self->{debug}=~/^debug/)
	{
		open LL,'>>log.sew';
		print LL "Counter step started\n";
		print LL scalar(keys %{$neurons})." neurons in network\n";
		close LL;
	}
	#first run
	my $m=0;
	for(keys %{$neurons})
	{
		if($_ > 0)
		{
			$neurons->{$_}->{debug}=$self->{debug};
			$neurons->{$_}->stream;
			if($m < $neurons->{$_}->{sum})
			{
#				$m=1;
				$m=$neurons->{$_}->{sum};
			}
		}
	}

	if($self->{debug}=~/^debug/)
	{
		open LL,'>>log.sew';
		print LL "Neurons finished recounting\n";
		print LL "Current state: ".$self->{charge}."\n";
		close LL;
	}

	my @winners=();
	if($m > 0)
	{
		for(keys %{$neurons})
		{
			if($m <= $neurons->{$_}->{sum} + $EPS)
			{
				push @winners,$_;
				$neurons->{$_}->{charge}=1;
			}
		}
	}

	if($self->{debug}=~/^debug/)
	{
		open LL,'>>log.sew';
		print LL "Detected ".scalar(@winners)." winners:\n";
		for(@winners)
		{
			print LL "\t$_\n";
		}
		close LL;
	}

	#coupling
	if($self->{debug}=~/^debug/)
	{
		open LL,'>>log.sew';
		print LL "Working through ".scalar(keys %{$neurons})."^2 neurons checking ".scalar(@$NN::Network::Oscillator::DATA)." input neurons a step\n";
		close LL;
	}
	for my $n1 (keys %{$neurons})
	{
		for my $n2 (keys %{$neurons->{$n1}->{neighbours}})
		{
			my ($xi,$xj,$si,$sj);
			$xi=$neurons->{$n1}->{v};
			$xj=$neurons->{$n2}->{v};
			$si=$neurons->{$n1}->{start};
			$sj=$neurons->{$n2}->{start};
			$neurons->{$n1}->{v}+=exp((($si-$sj)*($sj-$si))/($BETA*$BETA))*($xj-$xi);
		}
	}

	if($self->{debug}=~/^debug/)
	{
		open LL,'>>log.sew';
		print LL "Coupling applied\n";
		close LL;
	}

	for(@winners)
	{
		$neurons->{$_}->{mark}=$mark;
	}
	$mark++;

	my %chek=();
	for(keys %{$neurons})
	{
		$neurons->{$_}->{mark}=$neurons->{$_}->{mark}||0;
		$chek{$neurons->{$_}->{mark}}=($chek{$neurons->{$_}->{mark}}||0)+1;
	}

	print STDOUT localtime(time).' ';
	for(keys %chek)
	{
		print STDOUT $_."(".$chek{$_}."),";
	}
	print STDOUT "\n";

	if($mark > $self->{maximumsteps} || (0 && !$chek{0}))
	{
		print STDOUT $mark.' > '.$self->{maximumsteps}.' '.scalar(keys %chek).' < '.$self->{classnumber}.' '.defined($chek{0})."\n";
		$stop=1;
	}

	if($self->{debug}=~/^debug/)
	{
		open LL,'>>log.sew';
		print LL "Counter step finished\n";
		close LL;
	}
	return $stop;
}

1;