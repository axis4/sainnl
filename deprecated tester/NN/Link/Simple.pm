package NN::Link::Simple;

use strict;
use NN::Network;
use NN::Neuron;

BEGIN{
	use NN::Link;
	our ($VERSION,@ISA);

	$VERSION=1.0;

	@ISA=qw(NN::Link);
}

sub new
{
	my $class=shift;
	my $from=shift;
	my $to=shift;
	my %params=@_;
	my $self={};
	bless $self,$class;
	$self->{from}=$from;
	$self->{to}=$to;
	$self->{id}=$self->getid();
	for(keys %params)
	{
		$self->{$_}=$params{$_};
	}
	$self;
}

sub stream
{
	my $self=shift;
	$self->{to}->{sum}+=$self->{from}->{sum}*$self->{weight};
}

1;