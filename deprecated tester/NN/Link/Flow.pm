package NN::Link::Flow;

use strict;
use NN::Network;
use NN::Neuron;

BEGIN{
	use NN::Link;
	our ($VERSION,@ISA);

	$VERSION=1.0;

	@ISA=qw(NN::Link);
}

sub new
{
	my $class=shift;
	my $from=shift;
	my $to=shift;
	my %params=@_;
	my $self={};
	bless $self,$class;
	$self->{from}=$from;
	$self->{to}=$to;
    $self->{fromid}=$from->{id} if(defined $from);
    $self->{toid}=$to->{id} if(defined $to);
	$self->{id}=$self->getid();
	for(keys %params)
	{
		$self->{$_}=$params{$_};
	}
	$self;
}

sub stream
{
	my $self=shift;
    if (defined $self->{from} && defined $self->{to} && !$self->{from}->{template})
    {
        for my $a ('active','flow','generate')
        {
        	if($self->{$a.'weight'}=~/^\-?[\d\.]+$/) {
	            my $v=$self->{from}->{sum}*$self->{$a.'weight'};
				$self->{to}->{$a.'sum'}+=$v;
				$self->{to}->{$a.'source'}->{$self->{from}->{id}}=$v;
        	} else {
				if($self->{debug}=~/^debug/)
				{
					print STDERR "!NOT A NUMBER IN WEIGHT ".$self->{$a.'weight'}." BETWEEN ".$self->{fromid}." AND ".$self->{toid}."\n";
				}
        	}
        }
        if($self->{debug}=~/^debug/ and ($self->{to}->{activesum} or $self->{to}->{flowsum} or $self->{to}->{generatesum}))
        {
            print STDERR "FROM=".$self->{from}->{id}."(".$self->{from}."), TO=".$self->{to}->{id}."(".$self->{to}."), ID=".$self->{id}.", SUM=".$self->{from}->{sum}.", AW=".$self->{activeweight}.", FW=".$self->{flowweight}.", GW=".$self->{generateweight}.", ACTIVERESULT=".$self->{to}->{activesum}."(".scalar(keys %{$self->{to}->{activesource}})."), FLOWRESULT=".$self->{to}->{flowsum}."(".scalar(keys %{$self->{to}->{flowsource}})."), GENRESULT=".$self->{to}->{generatesum}."(".scalar(keys %{$self->{to}->{generatesource}}).")\n";
        }
    }
}

1;