package NN::Input;

use strict;
use NN::Neuron;
use NN::Link;
use NN::Input::Stdin;

BEGIN{
	our ($VERSION,@ISA);

	$VERSION=1.0;

	@ISA=qw();
}

my $ids=0;

sub new
{
	my $class=shift;
	my %params=@_;
	my $self={};
	bless $self,"NN::Input::Stdin";
	$self->{id}=$self->getid();
	for(keys %params)
	{
		$self->{$_}=$params{$_};
	}
	$self;
}

sub setstartid
{
	$ids=shift;
}

sub getid
{
	$ids++;
	return $ids;
}

sub addneuron
{
	my $self=shift;
	my @who=@_;
	for(@who)
	{
		$self->{neurons}->{$_}=1;
	}
}

sub deleteneuron
{
	my $self=shift;
	my @who=@_;
	for(@who)
	{
		undef $self->{neurons}->{$_};
	}
}

sub perform
{
	return;
}

1;