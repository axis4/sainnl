package NN::Neuron::Oscillator;

use strict;
use NN::Network;
use NN::Link;
use Math::Trig;

my $ALPHA=12;
my $BETA=4;
my $C=0.04;

BEGIN{
	use NN::Neuron;
	our ($VERSION,@ISA);

	$VERSION=1.0;

	@ISA=qw(NN::Neuron);
}

sub new
{
	my $class=shift;
	my %params=@_;
	my $self={};
	bless $self,$class;
	$self->{id}=$self->getid();
	$self->{charge}=0;
	$self->{chargerate}=1;
	$self->{dischargerate}=1;
	$self->{saturation}=10;
	for(keys %params)
	{
		$self->{$_}=$params{$_};
	}
	$self->{z}=$self->{saturation};
	$ALPHA=$self->{params}->{neuronalpha}||12;
	$BETA=$self->{params}->{neuronbeta}||4;
	$C=$self->{params}->{neuronc}||0.04;
	$self->{threshold}=$self->{params}->{neuronthreshold}||10;
	$self->{v}=$self->{params}->{neuronv}?(eval "".$self->{params}->{neuronv}):0.2;
	$self->{w}=$self->{params}->{neuronw}?(eval "".$self->{params}->{neuronw}):0.2;
	$self;
}

sub addneuron
{
	return;
}

sub deleteneuron
{
	return;
}

sub stream
{
	my $self=shift;
	my $v=$self->{v};
	if($self->{debug}=~/^debug/)
	{
		open TT,'>>',"./NN/trace/".$self->{id}.".log";
		print TT localtime(time)."\tSUM=".$self->{sum}."\tV=".$self->{v}."\tW=".$self->{w}."\tCHARGE=".$self->{charge}."\tZ=".$self->{z}."\n";
		close TT;
	}
	$self->{z}=($self->{charge}?(-$self->{chargerate})*($self->{z}-$self->{saturation}):(-$self->{dischargerate})*$self->{z});
	#3*v-v^3-v^7+2-w+I-z
	$self->{v}=3*$v+2-$self->{w}+$self->{sum}-$self->{z};#-$v*$v*$v-$v*$v*$v*$v*$v*$v*$v
	$self->{w}=$C*($ALPHA*(1+tanh($BETA*$v))-$self->{w});
	if($self->{v} > $self->{threshold})
	{
		$self->{sum}=$self->{v};
	}else{
		$self->{sum}=0;
	}
	if($self->{z} <= $self->{saturation})
	{
		$self->{charge}=0;
	}
	if($self->{debug}=~/^debug/)
	{
		open TT,'>>',"./NN/trace/".$self->{id}.".log";
		print TT localtime(time)."\tSUM=".$self->{sum}."\tV=".$self->{v}."\tW=".$self->{w}."\tCHARGE=".$self->{charge}."\tZ=".$self->{z}."\n";
		print TT localtime(time)."\t--------------------------------------------------------------------------------------------------------------\n";
		close TT;
	}
}

1;