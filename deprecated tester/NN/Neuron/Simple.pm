package NN::Neuron::Simple;

use strict;
use NN::Network;
use NN::Link;
use constant ALPHA => 2;

BEGIN{
	use NN::Neuron;
	our ($VERSION,@ISA);

	$VERSION=1.0;

	@ISA=qw(NN::Neuron);
}

sub new
{
	my $class=shift;
	my %params=@_;
	my $self={};
	bless $self,$class;
	$self->{id}=$self->getid();
	$self->{function}=sub{
						my $s=shift;
						return 1/(1+exp($s*ALPHA));
					};
	for(keys %params)
	{
		$self->{$_}=$params{$_};
	}
	$self;
}

sub addneuron
{
	my $self=shift;
	my $who=shift;
	my %connection=@_;
	$self->{links}->{$who}=new Link($self,$who,%connection);
	$who->{backlinks}->{$self}=$self->{links}->{$who};
	$self->{network}->addlink($self->{links}->{$who});
}

sub deleteneuron
{
	my $self=shift;
	my $who=shift;
	$self->{network}->deletelink($self->{links}->{$who});
	undef $who->{backlinks}->{$self};
	undef $self->{links}->{$who};
}

1;