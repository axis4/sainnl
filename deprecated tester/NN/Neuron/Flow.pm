package NN::Neuron::Flow;

use strict;
use NN::Network;
use NN::Link;
use Math::Trig;

BEGIN{
	use NN::Neuron;
	our ($VERSION,@ISA);

	$VERSION=1.0;

	@ISA=qw(NN::Neuron);
}

sub new
{
	my $class=shift;
	my %params=@_;
	my $self={};
	bless $self,$class;
	$self->{id}=$self->getid();
    for my $a ('active','flow','generate')
    {
        $self->{$a.'sum'}=0;
        $self->{$a.'source'}={};
    }
	$self->{sum}=0;
	$self->{constant}=0;
	$self->{threshold}=1;
	$self->{template}=0;
    $self->{input}=0;
    $self->{output}=0;
	$self->{expected}=0;
	$self->{links}=[];
	for(keys %params)
	{
		$self->{$_}=$params{$_};
	}
	$self;
}

sub addlink
{
	my $self=shift;
	my $link=shift;
	push @{$self->{links}},$link;
	return;
}

sub addneuron
{
	return;
}

sub deleteneuron
{
	return;
}

sub match
{
    my $pattern=shift;
    my $name=shift;
    if($pattern!~/\*/)
    {
        return 0;
    }
    $pattern=quotemeta($pattern);
    $pattern=~s/\\\*/[^\\\]]*/g;
    return $name=~/$pattern/;
};

sub extract_name
{
    my $name=shift;
    my $neg=substr($name, 0, 1) eq '-';
    if($neg) {
        $name=substr($name, 1);
    }
    my $s=0;
    my $t="";
    my $var={
        "neg"=>$neg,
        "name"=>"",
        "glob"=>0,
        "indexes"=>[],
        "subvar"=>undef
    };
    my $start=1;
    my $filling=$var;
    for my $c (split(//,$name))
    {
        if($s)
        {
            if($c eq "[")
            {
                $s++;
            } elsif($c eq "]") {
                $s--;
            }
            if($s < 0) {
                die "Missing opening bracket";
            }
            if(($c eq "," && $s==1) || !$s) {
                if(length($t)) {
                    push @{$filling->{"indexes"}},$t;
                }
                $t="";
            } elsif($s) {
                $t.=$c;
            }
        } else {
            if($c eq ".")
            {
                if($start) {
                    $filling->{"glob"}=1;
                } else {
                    if(length($t)) {
                        $filling->{"name"}=$t;
                    }
                    if(!length($filling->{"name"})) {
                        die "Name is not filled - multiple dots?";
                    }
                    $t="";
                    $filling->{"subvar"}={
                        "name"=>undef,
                        "glob"=>0,
                        "indexes"=>[],
                        "subvar"=>undef
                    };
                    $filling=$filling->{"subvar"};
                }
            } elsif($c eq "[") {
                $s++;
                if(length($t)) {
                    $filling->{"name"}=$t;
                }
                $t="";
            } elsif($c eq "]") {
                die "Missing opening bracket";
            } else {
                $t.=$c;
            }
        }
        if($start) {
            $start=0;
        }
    }
    if($s) {
        die "Missing closing bracket";
    }

    if(length($t)) {
        $filling->{"name"}=$t;
    }
    return $var;
};

sub combine_name
{
    my $var=shift @_;
    return sprintf("%s%s%s%s%s",
                    ($var->{"neg"}?"-":""),
                    ($var->{"glob"}?".":""),
                    $var->{"name"},
                    (scalar(@{$var->{"indexes"}})?"[".join(",",@{$var->{"indexes"}})."]":""),
                    ($var->{"subvar"}?".".combine_name($var->{"subvar"}):""));
};

sub transform_name
{
    my ($sid,$vars,$generate_rename)=@_;
    my $nt=0;
    my $var=extract_name($sid);
    my $cv=$var;
    while(defined $cv) {
        for(@{$cv->{"indexes"}})
        {
            if(defined $vars->{$_})
            {
                $_=$vars->{$_};
            } elsif(defined $generate_rename->{$_}) {
                $nt=1;
            }
        }
        $cv=$cv->{"subvar"};
    }
    my $nid=combine_name($var);
    return ($nid,$nt);
};

sub templated
{
    my ($sid,$tname)=@_;
    my $var=extract_name($sid);
    for(@{$var->{"indexes"}})
    {
        if($_ eq $tname)
        {
            return 1;
        }
    }
    return 0;
};

sub merge_links
{
    my ($from,$to)=@_;
    # append links from expected state into new neuron
    for my $el (@{$from->{links}})
    {
        my $f;
        for my $l (@{$to->{links}})
        {
            if($el->{fromid} eq $l->{fromid} && $el->{toid} eq $l->{toid})
            {
                $f=$l;
            }
        }
        if(!defined $f)
        {
            $el->{from}=$to;
            push @{$to->{links}},$el;
        }
    }
};

sub stream
{
	my $self=shift;
	if($self->{debug}=~/^debug/)
	{
		open TT,'>>',"./NN/trace/".$self->{id}.".log";
		print TT localtime(time)."\tASUM=".$self->{activesum}."\tFSUM=".$self->{flowsum}."\tGSUM=".$self->{generatesum}."\tSUM=".$self->{sum}."\n";
		close TT;
	}
    if($self->{template})
    {
        my $generate_rename=$self->{generatesource};
        
        my $neu=NN::Neuron::Flow->new(params=>$self->{params},network=>$self->{network},threshold=>$self->{threshold},constant=>$self->{constant},linkmod=>$self->{linkmod},input=>$self->{input},output=>$self->{output},template=>0,expected=>$self->{expected});

        if($self->{debug}=~/^debug/)
        {
            print STDERR "\t\t\tGENERATING SYGNALS (".scalar(keys %{$generate_rename})."): ";
            for(keys %{$generate_rename})
            {
                print STDERR $_."=".$generate_rename->{$_}." ";
            }
            print STDERR "\n";
        }
        
        my %vars=();
        for my $g (keys %{$generate_rename})
        {
            if(int($generate_rename->{$g}) > 0)
            {
                $vars{$g}=int($generate_rename->{$g});
            }
        }
        
        if(scalar(keys %vars))
        {
            if($self->{debug}=~/^debug/)
            {
                print STDERR "\t\t\tGENERATING VARS (".scalar(keys %vars)."): ";
                for(keys %vars)
                {
                    print STDERR $_."=".$vars{$_}." ";
                }
                print STDERR "\n";
            }

            ($neu->{id},$neu->{template})=transform_name($self->{id},\%vars,$generate_rename);
            
            if($self->{debug}=~/^debug/)
            {
                print STDERR "\t\t\tGENERATING NEW NODE: oldname=".$self->{id}.", newname=".$neu->{id}.", vars=".join(",", map { sprintf("%s=%s", $_, $vars{$_}) } keys %vars).", templated_name=".$neu->{template}."\n";
            }
            
            my $useless;

            my $neg;
            ($neg)=$neu->{threshold}=~s/^(\-)//;
            $neu->{threshold}=(defined $vars{$neu->{threshold}.''}?($neg?-1:1)*$vars{$neu->{threshold}.''}:($neg?"-":"").$neu->{threshold});
            ($neg)=$neu->{constant}=~s/^(\-)//;
            $neu->{constant}=(defined $vars{$neu->{constant}.''}?($neg?-1:1)*$vars{$neu->{constant}.''}:($neg?"-":"").$neu->{constant});
            #links from current name
            #"name(a,b-): target(c,d,e)"
            for my $sl (@{$self->{links}})
            {
                my $tln;
                ($tln,$useless)=transform_name($sl->{to}->{id},\%vars,$generate_rename);
                if(!defined $self->{network}->{neurons}->{$tln})
                {
                    my $tneu=NN::Neuron::Flow->new(params=>$self->{params},network=>$self->{network},expected=>1,id=>$tln);
                    $self->{network}->addneuron($tneu);
                }

                my $lin=NN::Link::Flow->new($neu,$self->{network}->{neurons}->{$tln},activeweight=>$sl->{activeweight},flowweight=>$sl->{flowweight},generateweight=>$sl->{generateweight});
                my $ch=0;
                for my $el (keys %{$self->{network}->{links}})
                {
                    if($self->{network}->{links}->{$el}->{fromid} eq $neu->{id} && $self->{network}->{links}->{$el}->{toid} eq $tln)
                    {
                        $lin=$self->{network}->{links}->{$el};
                        $ch=1;
                    }
                }
                if(!$ch)
                {
                    $self->{network}->addlink($lin);
                    push @{$neu->{links}},$lin;
                }
                ($neg)=$lin->{activeweight}=~s/^(\-)//;
                $lin->{activeweight}=(defined $vars{$lin->{activeweight}.''}?($neg?-1:1)*$vars{$lin->{activeweight}.''}:($neg?"-":"").$lin->{activeweight});
                ($neg)=$lin->{flowweight}=~s/^(\-)//;
                $lin->{flowweight}=(defined $vars{$lin->{flowweight}.''}?($neg?-1:1)*$vars{$lin->{flowweight}.''}:($neg?"-":"").$lin->{flowweight});
                ($neg)=$lin->{generateweight}=~s/^(\-)//;
                $lin->{generateweight}=(defined $vars{$lin->{generateweight}.''}?($neg?-1:1)*$vars{$lin->{generateweight}.''}:($neg?"-":"").$lin->{generateweight});

                if($self->{debug}=~/^debug/ && !$ch)
                {
                    print STDERR "\t\t\tGENERATING OUTGOING LINK: from=".$lin->{fromid}.", to=".$lin->{toid}." weighted: ".$lin->{activeweight}.",".$lin->{flowweight}.",".$lin->{generateweight}."\n";
                }
            }
            #links from equal current name[x]
            #"name[*](a,b-): target(c,d,e)"
            for(keys %{$self->{network}->{neurons}})
            {
                if(match($_, $neu->{id}))
                {
                    for my $l (@{$self->{network}->{neurons}->{$_}->{links}})
                    {
						my $tln;
						($tln,$useless)=transform_name($l->{to}->{id},\%vars,$generate_rename);
						if(!defined $self->{network}->{neurons}->{$tln})
						{
							my $tneu=NN::Neuron::Flow->new(params=>$self->{params},network=>$self->{network},expected=>1,id=>$tln);
							$self->{network}->addneuron($tneu);
						}

                        my $lin=NN::Link::Flow->new($neu,$self->{network}->{neurons}->{$tln},activeweight=>$l->{activeweight},flowweight=>$l->{flowweight},generateweight=>$l->{generateweight});
                        my $ch=0;
                        for my $el (keys %{$self->{network}->{links}})
                        {
                            if($self->{network}->{links}->{$el}->{fromid} eq $neu->{id} && $self->{network}->{links}->{$el}->{toid} eq $l->{toid})
                            {
                                $lin=$self->{network}->{links}->{$el};
                                $ch=1;
                            }
                        }
                        if(!$ch)
                        {
                            $self->{network}->addlink($lin);
                            push @{$neu->{links}},$lin;
                            
                            ($neg)=$lin->{activeweight}=~s/^(\-)//;
                            $lin->{activeweight}=(defined $vars{$lin->{activeweight}.''}?($neg?-1:1)*$vars{$lin->{activeweight}.''}:($neg?"-":"").$lin->{activeweight});
                            ($neg)=$lin->{flowweight}=~s/^(\-)//;
                            $lin->{flowweight}=(defined $vars{$lin->{flowweight}.''}?($neg?-1:1)*$vars{$lin->{flowweight}.''}:($neg?"-":"").$lin->{flowweight});
                            ($neg)=$lin->{generateweight}=~s/^(\-)//;
                            $lin->{generateweight}=(defined $vars{$lin->{generateweight}.''}?($neg?-1:1)*$vars{$lin->{generateweight}.''}:($neg?"-":"").$lin->{generateweight});
                            
                            if($self->{debug}=~/^debug/)
                            {
                                print STDERR "\t\t\tGENERATING OUTGOING MASKED LINK: from=".$lin->{fromid}.", to=".$lin->{toid}." weighted: ".$lin->{activeweight}.",".$lin->{flowweight}.",".$lin->{generateweight}."\n";
                            }
                        }
                    }
                }
            }
            #links to current name
            #"target(a,b-): name(c,d,e)"
            for(keys %{$self->{network}->{links}})
            {
                if(!defined $self->{network}->{links}->{$_}->{from} && $self->{network}->{links}->{$_}->{fromid} eq $neu->{id}) #impossible state when target is a nonexistingname
                {
                    $self->{network}->{links}->{$_}->{from}=$neu;
                }
                if(!defined $self->{network}->{links}->{$_}->{to} && $self->{network}->{links}->{$_}->{toid} eq $neu->{id}) #"target(a,b-): nonexistingname(c,d,e)" and "nonexistingname==name"
                {
                    $self->{network}->{links}->{$_}->{to}=$neu;
                }
                if(defined $self->{network}->{links}->{$_}->{to} && $self->{network}->{links}->{$_}->{toid} eq $self->{id}) #"target(a,b-): templatename(c,d,e)"
                {
                    #check if in same templating state
                    if(defined $self->{network}->{links}->{$_}->{from} && $self->{network}->{links}->{$_}->{from}->{template})
                    {
                        my $state_match=0;
                        for my $k (keys %vars)
                        {
                            if(defined $self->{network}->{links}->{$_}->{from}->{generatesource}->{$k} && $self->{network}->{links}->{$_}->{from}->{generatesource}->{$k} > 0)
                            {
                                $state_match=1;
                            }
                        }
                        if($state_match)
                        {
                            next;
                        }
                    }

					my $tln;
					($tln,$useless)=transform_name($self->{network}->{links}->{$_}->{fromid},\%vars,$generate_rename);
					if(!defined $self->{network}->{neurons}->{$tln})
					{
						my $tneu=NN::Neuron::Flow->new(params=>$self->{params},network=>$self->{network},expected=>1,id=>$tln);
						$self->{network}->addneuron($tneu);
					}

                    my $lin=NN::Link::Flow->new($self->{network}->{neurons}->{$tln},$neu,fromid=>$tln,toid=>$neu->{id},activeweight=>$self->{network}->{links}->{$_}->{activeweight},flowweight=>$self->{network}->{links}->{$_}->{flowweight},generateweight=>$self->{network}->{links}->{$_}->{generateweight});
                    my $ch=0;
                    for my $el (keys %{$self->{network}->{links}})
                    {
                        if($self->{network}->{links}->{$el}->{fromid} eq $tln && $self->{network}->{links}->{$el}->{toid} eq $neu->{id})
                        {
                            $lin=$self->{network}->{links}->{$el};
                            $ch=1;
                        }
                    }
                    if(!$ch)
                    {
                        ($neg)=($lin->{activeweight}=~s/^(\-)//);
                        $lin->{activeweight}=(defined $vars{$lin->{activeweight}.''}?($neg?-1:1)*$vars{$lin->{activeweight}.''}:($neg?"-":"").$lin->{activeweight});
                        ($neg)=($lin->{flowweight}=~s/^(\-)//);
                        $lin->{flowweight}=(defined $vars{$lin->{flowweight}.''}?($neg?-1:1)*$vars{$lin->{flowweight}.''}:($neg?"-":"").$lin->{flowweight});
                        ($neg)=$lin->{generateweight}=~s/^(\-)//;
                        $lin->{generateweight}=(defined $vars{$self->{network}->{links}->{$_}->{fromid}.''}?0:(defined $vars{$lin->{generateweight}.''}?($neg?-1:1)*$vars{$lin->{generateweight}.''}:($neg?"-":"").$lin->{generateweight}));

                        if($lin->{activeweight} || $lin->{flowweight} || $lin->{generateweight})
                        {
                            $self->{network}->addlink($lin);
							push @{$self->{network}->{neurons}->{$tln}->{links}},$lin;

                            if($self->{debug}=~/^debug/)
                            {
                                print STDERR "\t\t\tGENERATING INCOMING LINK: from=".$lin->{fromid}.", to=".$lin->{toid}." weighted: ".$lin->{activeweight}.",".$lin->{flowweight}.",".$lin->{generateweight}."\n";
                            }
                        }
                    }
                }
            }

            if(defined $self->{network}->{neurons}->{$neu->{id}})
            {
				$neu->{input} = $neu->{input} or $self->{network}->{neurons}->{$neu->{id}}->{input};
				$neu->{output} = $neu->{output} or $self->{network}->{neurons}->{$neu->{id}}->{output};
                    
                if($self->{network}->{neurons}->{$neu->{id}}->{expected})
                {
                    merge_links($self->{network}->{neurons}->{$neu->{id}}, $neu);
                    
                    for my $el (keys %{$self->{network}->{links}})
                    {
                        if($self->{network}->{links}->{$el}->{toid} eq $neu->{id})
                        {
                            $self->{network}->{links}->{$el}->{to}=$neu;
                        }
                    }
                    
                    delete $self->{network}->{neurons}->{$neu->{id}};
                } else {
                    merge_links($neu, $self->{network}->{neurons}->{$neu->{id}});
                }
            }
            
            my $working=0;

            if(!defined $self->{network}->{neurons}->{$neu->{id}})
            {
                $self->{network}->addneuron($neu);
            
                if(!$neu->{template})
                {
                    $working=1;
                }
            } else {
                if($self->{template} && !$neu->{template} && $self->{id} eq $neu->{id})
                {
                    $self->{template}=$neu->{template};
                    for my $nl (@{$neu->{links}})
                    {
                        my $ch=0;
                        for my $ol (@{$self->{links}})
                        {
                            if($ol->{fromid} eq $nl->{fromid} and $ol->{toid} eq $nl->{toid})
                            {
                                $ch=1;
                            }
                        }
                        if(!$ch)
                        {
                        	$nl->{from}=$self;
                            $self->addlink($nl);
                        }
                    }
                    $working=1;
                }
            }
            if($working)
            {
                if($neu->{input})
                {
                    $self->{network}->{input}->{neurons}->{$neu}=1;
                }
                if($neu->{output})
                {
                    $self->{network}->{output}->{neurons}->{$neu}=1;
                }
                
                if($self->{debug}=~/^debug/)
                {
                    for my $k (sort {$a cmp $b} keys %{$self->{network}->{neurons}})
                    {
                        my $n=$self->{network}->{neurons}->{$k};
                        printf STDERR "\t\t\t%s(%s,%s-%s):",$n->{id},$n->{threshold},$n->{constant},($n->{expected}?'e':'').($n->{input}?'i':'').($n->{output}?'o':'').($n->{linkmod}?'l':'').($n->{template}?'-t':'');
                        for my $l (sort {$a->{to}->{id} cmp $b->{to}->{id}} @{$n->{links}})
                        {
                            printf STDERR " %s(%s,%s,%s)",$l->{to}->{id},$l->{activeweight},$l->{flowweight},$l->{generateweight};
                        }
                        print STDERR "\n";
                    }
                }
            }
        }
    } else {
		if($self->{expected})
        {
            if(0 and $self->{debug}=~/^debug/)
            {
                print STDERR $self->{id}." NOOP\n";
            }
            return;#no-op
        }
        if($self->{linkmod})
        {
            $self->{threshold}+=$self->{activesum};
            $self->{sum}=($self->{flowsum}+$self->{constant})*$self->{threshold};
        } else {
            $self->{sum}=($self->{flowsum}+$self->{constant})*($self->{activesum}>=$self->{threshold}?1:0);
        }
        $self->{sum}=($self->{sum}, 255)[$self->{sum} > 255];
        $self->{sum}=($self->{sum}, -255)[$self->{sum} < -255];
        $self->{sum}=int($self->{sum});
        if($self->{debug}=~/^debug/ && $self->{sum})
        {
            print STDERR $self->{id}."(".$self.") ACTIVE=".$self->{activesum}."(".join(",", map { sprintf("%s=%s",$_,$self->{activesource}->{$_}) } keys %{$self->{activesource}})."), FLOW=".$self->{flowsum}."(".join(",", map { sprintf("%s=%s",$_,$self->{flowsource}->{$_}) } keys %{$self->{flowsource}})."), C=".$self->{constant}.", LEVEL=".$self->{threshold}." => RESULT=".$self->{sum}."\n";
        }
    }
	if($self->{debug}=~/^debug/)
	{
		open TT,'>>',"./NN/trace/".$self->{id}.".log";
		print TT localtime(time)."\tASUM=".$self->{activesum}."\tFSUM=".$self->{flowsum}."\tGSUM=".$self->{generatesum}."\tSUM=".$self->{sum}."\n";
		print TT localtime(time)."\t--------------------------------------------------------------------------------------------------------------\n";
		close TT;
	}
}

1;