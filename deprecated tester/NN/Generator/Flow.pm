package NN::Generator::Flow;

use strict;
use NN::Input::Stdin;
use NN::Output::Stdout;
use NN::Counter::Flow;
use NN::Network::Flow;
use NN::Neuron::Flow;
use NN::Link::Flow;

BEGIN{
	use NN::Generator;
	our ($VERSION,@ISA);

	$VERSION=1.0;

	@ISA=qw(NN::Generator);
}

sub new
{
	my $class=shift;
	my %params=@_;
	my $self={};
	bless $self,$class;
	$self->{id}=$self->getid();
	for(keys %params)
	{
		$self->{$_}=$params{$_};
	}
	
	$self;
}

sub perform
{
	my $self=shift;
	my $net=shift;
	my $deb=$self->{debug};
	my $desc=$self->{params}->{neurons};

	if($deb=~/^debug/)
	{
		open LL,">>log.sew";
		print LL "Generator started\n";
		print LL "DATA: self=$self net=$net\n";
		close LL;
	}

	$net->{build}=1;
	NN::Neuron::setstartid(0);
	if($deb eq 'debug')
	{
		print STDERR "Generating neurons\n";
	}

	my @ns=split(/\|/,$desc);
	for(0..$#ns)
	{
		$ns[$_]=~s/^\s+|\s+$//g;
		if(length($ns[$_]))
		{
			my ($cur_num, $cur,$link)=($ns[$_]=~/([^(]*)\(([^)]*)\):\s*(.*)/);
			if (!length($cur_num)) {
				$cur_num = $_+1;
			}
			my $neu=new NN::Neuron::Flow(params=>$self->{params},network=>$net,id=>$cur_num);
			$net->addneuron($neu);
		}
	}

	my %ineu=();
	my %oneu=();

	for(0..$#ns)
	{
		next if(!length($ns[$_]));
		my ($cur_num, $cur,$link)=($ns[$_]=~/([^(]*)\(([^)]*)\):\s*(.*)/);
		if (!length($cur_num)) {
			$cur_num = $_+1;
		}
		my ($t,$c,$f,$g)=($cur=~/^(.*),\s*(.*)\-([eiol]+)?(\-t)?$/);
        if($f =~ /e/)
        {
            $net->neurons->{$cur_num}->{expected}=1;
        }
		if($f =~ /i/)
		{
			$net->neurons->{$cur_num}->{input}=1;
			if(!$g)
			{
				$ineu{$net->neurons->{$cur_num}}=1;
			}
		}
		if($f =~ /o/)
		{
			$net->neurons->{$cur_num}->{output}=1;
			if(!$g)
			{
				$oneu{$net->neurons->{$cur_num}}=1;
			}
		}
		if($f =~ /l/)
		{
			$net->neurons->{$cur_num}->{linkmod}=1;
		}
		$net->neurons->{$cur_num}->{threshold}=$t;
		$net->neurons->{$cur_num}->{constant}=$c;
 		$net->neurons->{$cur_num}->{template}=($g?1:0);
		if($deb=~/^debug/)
		{
			print STDERR "SETUP NEURON: ".($cur_num).", THRESHOLD=".$t.", CONSTANT=".$c.", TEMPLATE=".$net->neurons->{$cur_num}->{template}."\n";
		}
		while($link=~s/([\w\[\]\:\.\,\*\#]+)\((.*?)\)\s*//)
		{
			my $n=$1;
			my $cort=$2;
			my ($aw,$sw,$gw)=($cort=~/(.*),\s*(.*),\s*(.*)/);
			my $lin=new NN::Link::Flow($net->neurons->{$cur_num},$net->neurons->{$n},fromid=>$cur_num,toid=>$n,activeweight=>$aw,flowweight=>$sw,generateweight=>$gw);
			$net->neurons->{$cur_num}->addlink($lin);
			$net->addlink($lin);
			if($deb=~/^debug/)
			{
				print STDERR "SETUP LINK: FROM=".($cur_num).", TO=".$n.", ACTIVE=".$aw.", FLOW=".$sw.", GEN=".$gw."\n";
			}
		}
	}
    
    #preprocess templated incomes
#    for(keys %{$net->neurons})
#    {
#        if($net->neurons->{$_}->{template})
#        {
#            my %gen_names=();
#            for my $nl ($net->find_incomes($_))
#            {
#                if($nl->{generateweight} != 0 && !NN::Neuron::Flow::templated($_,$nl->{fromid}))
#                {
#                    $gen_names{$nl->{fromid}}=1;
#                }
#            }
#            if(scalar(keys %gen_names)) {
#                for my $nl ($net->find_incomes($_))
#                {
#                    my $vars={};
#                    for my $name (keys %gen_names)
#                    {
#                        $vars->{$name}='*';
#                    }
#                    my ($nlid,$ul)=NN::Neuron::Flow::transform_name($nl->{fromid},$vars,{});
#                    if($nl->{fromid} ne $nlid)
#                    {
#                        my $tneu;
#                        if(!defined $net->{neurons}->{$nlid})
#                        {
#                            $tneu=NN::Neuron::Flow->new(params=>$nl->{from}->{params},network=>$self->{network},expected=>1,id=>$nlid);
#                            $net->addneuron($tneu);
#                        } else {
#                            $tneu=$net->{neurons}->{$nlid};
#                        }
#                        # clean old link source
#                        my @del_indexes = reverse(grep { ${$nl->{from}->{links}}[$_]->{id} eq $nl->{id} } 0..$#{$nl->{from}->{links}});
#                        for my $i (@del_indexes)
#                        {
#                            splice(@{$nl->{from}->{links}},$i,1);
#                        }
#                        # set new link source
#                        $nl->{fromid}=$nlid;
#                        $nl->{from}=$tneu;
#                        $tneu->addlink($nl);
#
#                        if($deb=~/^debug/)
#                        {
#                            print STDERR "SETUP ABSTRACT LINK: FROM=".($nlid).", TO=".$_."\n";
#                        }
#                    }
#                }
#            }
#        }
#    }

	$net->{input}=new NN::Input::Stdin(params=>$self->{params},neurons=>\%ineu,debug=>$deb);
	$net->{output}=new NN::Output::Stdout(params=>$self->{params},neurons=>\%oneu,debug=>$deb);
	$net->{counter}=new NN::Counter::Flow(params=>$self->{params},debug=>$deb,stop=>(defined $self->{params}->{input} ? length($self->{params}->{input}) : undef));

	if($deb=~/^debug/)
	{
		print STDERR "Created ".scalar(keys %{$net->neurons})." neurons\n";
	}
	
	if($deb=~/^debug/)
	{
		open LL,">>log.sew";
		print LL "Generator ended\n";
		close LL;
	}
}

1;