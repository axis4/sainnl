package NN::Generator::Simple;

use strict;
use NN::Input;
use NN::Output;
use NN::Counter;
use NN::Network;
use NN::Neuron;
use NN::Link;

BEGIN{
	use NN::Generator;
	our ($VERSION,@ISA);

	$VERSION=1.0;

	@ISA=qw(NN::Generator);
}

sub new
{
	my $class=shift;
	my %params=@_;
	my $self={};
	bless $self,$class;
	$self->{id}=$self->getid();
	for(keys %params)
	{
		$self->{$_}=$params{$_};
	}
	$self;
}

sub perform
{
	my $self=shift;
	my $net=shift;
	my $in=new NN::Input;
	my $out=new NN::Output;
	my $con=new NN::Counter;
	$net->{input}=$in;
	$net->{output}=$out;
	$net->{counter}=$con;
	my $neu=new NN::Neuron;
}

1;