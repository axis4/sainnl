package NN::Generator::Oscillator;

use strict;
use NN::Input::Image;
use NN::Output::Image;
use NN::Counter::Oscillator;
use NN::Network::Oscillator;
use NN::Neuron::Oscillator;

my $SAT=10;
my $DIST=5;
my $CR=1;
my $DR=0.05;

BEGIN{
	use NN::Generator;
	our ($VERSION,@ISA);

	$VERSION=1.0;

	@ISA=qw(NN::Generator);
}

sub new
{
	my $class=shift;
	my %params=@_;
	my $self={};
	bless $self,$class;
	$self->{id}=$self->getid();
	for(keys %params)
	{
		$self->{$_}=$params{$_};
	}
	$SAT=$self->{params}->{generatorsat}||10;
	$DIST=$self->{params}->{generatordist}||5;
	$CR=$self->{params}->{generatorchargerate}||1;
	$DR=$self->{params}->{generatordischargerate}||1;
	$self;
}

sub perform
{
	my $self=shift;
	my $net=shift;
	my $deb=$self->{debug};
	my $width=$self->{params}->{width};
	my $height=$self->{params}->{height};

	if($deb=~/^debug/)
	{
		open LL,">>log.sew";
		print LL "Generator started\n";
		print LL "DATA: self=$self net=$net width=$width height=$height\n";
		close LL;
	}

	my $in=new NN::Input::Image(params=>$self->{params});
	my $out=new NN::Output::Image(params=>$self->{params});
	my $con=new NN::Counter::Oscillator(params=>$self->{params});
	$in->{debug}=$deb;
	$out->{debug}=$deb;
	$con->{debug}=$deb;
	$net->{input}=$in;
	$net->{output}=$out;
	$net->{counter}=$con;
	$net->{build}=1;
	NN::Neuron::setstartid(0);
	print STDOUT "Generating neurons\n";
	for my $x (0..$width-1)
	{
		for my $y (0..$height-1)
		{
			print STDOUT ($x*$height+$y)*100/($width*$height)."%\r";
			my $neu=new NN::Neuron::Oscillator(x=>$x,y=>$y,saturation=>$SAT,chargerate=>$CR,dischargerate=>$DR,params=>$self->{params});
			$net->addneuron($neu);
		}
	}

	print STDOUT "\n";
	print STDOUT "Created ".scalar(keys %{$net->neurons})." neurons\n";
	print STDOUT "Generating neighbours\n";
	my $x=0;
	print STDOUT $x*100/($width*$height)."%\r";
	my @k=keys %{$net->neurons};
	@k=sort {$net->neurons->{$a}->{x}*$height+$net->neurons->{$a}->{y} <=> $net->neurons->{$b}->{x}*$height+$net->neurons->{$b}->{y}} @k;
	for my $n1 (@k)
	{
		for(my $i=(-1)*$DIST;$i<$DIST;$i++)
		{
			for(my $j=(-1)*$DIST;$j<$DIST;$j++)
			{
				if($i != 0 || $j != 0)
				{
					my $cx=$net->neurons->{$n1}->{x}+$i;
					my $cy=$net->neurons->{$n1}->{y}+$j;
					if($cx >= 0 && $cx < $width && $cy >= 0 && $cy < $height)
					{
						my $in=$cx*$height+$cy+1;
						my ($x1,$y1);
						$x1=$net->neurons->{$n1}->{x}-$net->neurons->{$in}->{x};
						$y1=$net->neurons->{$n1}->{y}-$net->neurons->{$in}->{y};
						my $c=$x1*$x1+$y1*$y1;
						if($c < $DIST*$DIST)
						{
							$net->neurons->{$n1}->{neighbours}->{$net->neurons->{$in}->{id}}=1;
						}
					}
				}
			}
		}
		$x++;
		print STDOUT $x*100/($width*$height)."%\r";
	}
	print STDOUT "\n";
	
	if($deb=~/^debug/)
	{
		open LL,">>log.sew";
		print LL "Generator ended\n";
		close LL;
	}
}

1;