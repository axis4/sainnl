package NN::Counter;

use strict;
use NN::Neuron;
use NN::Link;
use NN::Counter::Simple;

BEGIN{
	our ($VERSION,@ISA);

	$VERSION=1.0;

	@ISA=qw();
}

my $ids=0;

sub new
{
	my $class=shift;
	my %params=@_;
	my $self={};
	bless $self,"NN:Counter::Simple";
	$self->{id}=$self->getid();
	for(keys %params)
	{
		$self->{$_}=$params{$_};
	}
	$self;
}

sub setstartid
{
	$ids=shift;
}

sub getid
{
	$ids++;
	return $ids;
}

sub perform
{
	return;
}

1;