@echo off
rem echo "Oscillator test"
rem cd NN\test1
rem call maketest.bat
rem cd NN\test2
rem call maketest.bat
rem cd NN\test3
rem call maketest.bat
echo "Flow test"
cd NN\flowtest1
call maketest.bat
cd NN\flowtest2
call maketest.bat
cd NN\flowtest3
call maketest.bat
cd NN\flowtest4
call maketest.bat
cd NN\flowtest5
call maketest.bat
cd NN\flowtest6
call maketest.bat
cd NN\flowtest7memory_cell
call maketest.bat
cd NN\flowtest8switch
call maketest.bat
cd NN\flowtest9compare_block
call maketest.bat
cd NN\flowtest10maximum
call maketest.bat
cd NN\flowtest11linkmod
call maketest.bat
cd NN\flowtest11linkmod_neurononly
call maketest.bat
cd NN\flowtest12generator
call maketest.bat
cd NN\flowtest13inputkeeper
call maketest.bat
cd NN\flowtest14association
call maketest.bat
cd NN\flowtest_perceptron
call maketest.bat
