#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo "Oscillator test"
cd $DIR/NN/test1
bash maketest.sh
cd $DIR/NN/test2
bash maketest.sh
cd $DIR/NN/test3
bash maketest.sh
echo "Flow test"
cd $DIR/NN/flowtest1
bash maketest.sh
cd $DIR/NN/flowtest2
bash maketest.sh
cd $DIR/NN/flowtest3
bash maketest.sh
cd $DIR/NN/flowtest4
bash maketest.sh
cd $DIR/NN/flowtest5
bash maketest.sh
cd $DIR/NN/flowtest6
bash maketest.sh
cd $DIR/NN/flowtest7memory_cell
bash maketest.sh
cd $DIR/NN/flowtest8switch
bash maketest.sh
cd $DIR/NN/flowtest9compare_block
bash maketest.sh
cd $DIR/NN/flowtest10maximum
bash maketest.sh
cd $DIR/NN/flowtest11linkmod
bash maketest.sh
cd $DIR/NN/flowtest11linkmod_neurononly
bash maketest.sh
cd $DIR/NN/flowtest12generator
bash maketest.sh
cd $DIR/NN/flowtest13inputkeeper
bash maketest.sh
cd $DIR/NN/flowtest14association
bash maketest.sh
cd $DIR/NN/flowtest_perceptron
bash maketest.sh
