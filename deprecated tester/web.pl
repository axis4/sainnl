if(@ARGV != 1)
{
	die "Usage: web.pl port\n";
}

use NN::Generator;
use NN::Input;
use NN::Output;
use NN::Neuron;
use NN::Link;
use NN::Network;

$|=1;

my $network_cache = {};

{
	package MyWebServer;

	use HTTP::Server::Simple::CGI;
	use Net::Server::Fork;
	use base qw(HTTP::Server::Simple::CGI);

	sub rnd_str
	{
		return join('', @_[ map{ rand @_ } 1 .. shift ]);
	}

	sub rnd_id
	{
		return rnd_str(8, 'a'..'z', 0..9);
	}

	my %dispatch = (
		'/' => \&resp_main,
		'/start' => \&resp_start,
		'/step' => \&resp_step
	);

	sub handle_request
	{
		my $self = shift;
		my $cgi = shift;
		my $path = $cgi->path_info();
		my $handler = $dispatch{$path};

		$|=1;

		if (ref($handler) eq "CODE")
		{
			$handler->($cgi);
		} else {
			print "HTTP/1.0 404 Not found\r\n";
			print $cgi->header,
			$cgi->start_html('Not found'),
			$cgi->h1('Not found'),
			$cgi->end_html;
		}
	}
	
	sub resp_main
	{
		my $cgi=shift;
		print "HTTP/1.0 200 OK\r\n";
		print $cgi->header;
		print <<DATA;
<!DOCTYPE html>
<form action="start" method="post">
	<table>
	<tr><td>Config:</td><td><textarea name="config"></textarea></td></tr>
	<tr><td>Input(hex, comma-separated):</td><td><input type="text" name="input"></td></tr>
	<tr><td colspan="2"><input type="submit" value="OK"></td></tr>
	</table>
</form>
DATA
	}
	
	sub record
	{
		my $counter = shift;
		my $neurons = shift;
		my $links = shift;
		my $s = sprintf("{\"stop\": %d, \"neurons\": {", $counter->{stop});
		my @n = ();
		for(sort {$a cmp $b} keys %{$neurons})
		{
			push @n, sprintf("\"%s\": %04d", $_, $neurons->{$_}->{sum});
		}
		push @{$counter->{params}->{recordings}}, $s.join(", ", @n)."}}";
	}

	sub resp_start
	{
		my $cgi = shift;
		print "HTTP/1.0 200 OK\r\n";
		print $cgi->header;
		if($cgi->request_method ne "POST")
		{
			print "Only POST allowed";
		}

		my $conf = $cgi->param('config');
		$conf=~s/\n/ \| /g;
		my $input = $cgi->param('input');
		my $jsonp = $cgi->param('jsonp');
		my $script = $cgi->param('script');
		
		use NN::Network::Flow;
		use NN::Generator::Flow;
		my $id = rnd_id();
		my $net = new NN::Network::Flow;
		#$net->{debug}="debug";
		$network_cache->{$id} = $net;
		$net->{params} = {neurons => $conf, hook => \&record, cache_id => $id, recordings => []};
		if(defined $input)
		{
			$net->{params}->{input} = pack('H*', $input);
		} else {
			print "Not implemented";
			return;
			#TODO: interactive shell
			#open $net->{params}->{input_handler}
		}
		my $gen = new NN::Generator::Flow(params=>$net->{params});
		#$gen->{debug}="debug";
		$gen->perform($net);
		
		my $res = "";
		if(defined $input)
		{
			$net->perform;
			$res = "[\n".join(",\n", @{$net->{params}->{recordings}})."\n]";
		} else {
			$res = "{\"id\": \"".$id."\"}";
		}
		if(defined $jsonp)
		{
			$res = $jsonp."(".$res.");";
		}
		if(defined $script)
		{
			$res = "<script>".$res."</script>";
		}
		print $res;
	}
	
	sub resp_step
	{
		print "Not implemented";
		return;

		my $cgi = shift;
		return if !ref $cgi;
		if($cgi->method ne "POST")
		{
			print "Only POST allowed";
		}

		my $id = $cgi->param('id');
		my $input = $cgi->param('input');
		#TODO: call network step in interactive step
		$network_cache->{$id};
		#TODO: print network output
	}
	
	sub net_server
	{
		return Net::Server::Fork;
	}
} 

my $srv = MyWebServer->new($ARGV[0]);
my $pid = $srv->background();
print "Use 'kill $pid' to stop server.\n";
