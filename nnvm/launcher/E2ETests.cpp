/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#include "E2ETests.hpp"
#include <map>
#include <iostream>
#include <sstream>
#include <ByteCodeParserException.hpp>
#include <ByteCodeException.hpp>
#include <ByteCodeLoader.hpp>
#include <ByteCodeUtils.hpp>
#include <BinaryUtils.hpp>
#include "../nnvm/src/writer/ByteCodeWriter.hpp"
#include "../nnvm/src/utils/Utils.hpp"
#include "../nnvm/src/processor/Processor.hpp"

namespace nnvm {
    namespace tests {
        std::map<icu::UnicodeString, testCase> cases = {
                {USS("01. one byte offset"), []() {
                    return E2ETests::runFile(USS("Test1.network"));
                }},
                {USS("02. generated output"), []() {
                    return E2ETests::runFile(USS("Test2.network"));
                }},
                {USS("03. filtered output"), []() {
                    return E2ETests::runFile(USS("Test3.network"));
                }},
                {USS("04. foreach block in functions package"), []() {
                    return E2ETests::runFile(USS("Test4.network"));
                }},
                {USS("05. factorial block in functions package"), []() {
                    return E2ETests::runFile(USS("Test5.network"));
                }},
        };

        std::map<icu::UnicodeString, std::map<icu::UnicodeString, testCase>> e2e = {
                {USS("01. simple cases"), cases},
        };

        int8_t E2ETests::matchStreams(std::string actual, std::string expected)
        {
            fast_assert(!actual.empty(), StringUtils::spf(std::string("Expected: {0}\nActual: "), expected), actual);
            fast_assert(actual.size() >= expected.size(),
                        StringUtils::spf(std::string("Expected: {0}\nActual: "), expected), actual);

            // match substream ignoring extra "zero" signals of network execution
            std::vector<int32_t> av;
            for (char & i : actual) {
                av.push_back((int32_t) i);
            }
            std::vector<int32_t> ev;
            for (char & i : expected) {
                ev.push_back((int32_t) i);
            }

            auto pos = std::search(av.begin(), av.end(), ev.begin(), ev.end());

            auto prefix = av[0];
            auto suffix = av[av.size() - 1];

            auto exists = pos != av.end();
            auto atstart = exists && pos == av.begin();
            auto atsignificantstart = exists && pos != av.begin() && *(pos - 1) == prefix;
            auto fullend = exists && ((pos + ev.size() == av.end()) || *(pos + ev.size()) == suffix);

            fast_assert(exists && (atstart || atsignificantstart) && fullend,
                        StringUtils::spf(std::string("Expected: {0}\nActual: "), expected), actual);

            return 1;

        }

        int8_t E2ETests::runFile(icu::UnicodeString packageName)
        {
            std::vector<icu::UnicodeString> parts;
            StringUtils::split(parts, packageName, USS("\\") + BinaryUtils::packageSeparatorUnicode);
            if (parts.size() < 2) {
                std::cout << "Incorrect package name " << StringConvUtils::utou8(packageName) << std::endl;
                return 0;
            }

            auto file = parts[parts.size() - 2];
            auto path = StringUtils::joinArray(std::vector<icu::UnicodeString> {TEST_PATH, LIBRARY_CASES_PATH, file},
                                               BinaryUtils::pathSeparatorUnicode);

            auto streams = PerformanceUtils::timeit<std::tuple<icu::UnicodeString, icu::UnicodeString>>([&path]() {
                int64_t err;
                auto in = BinaryUtils::openTextStream(path + USS(".in"), err);
                std::cout << StringConvUtils::utou8(path + USS(".in")) << std::endl;
                std::vector<int8_t> chars;

                auto inStream = USS("");
                auto outStream = USS("");

                if (BinaryUtils::readBinary(*in, chars, 1024 * 1024 * 1024) >= 0) {
                    inStream = StringConvUtils::u8tou(std::string(chars.begin(), chars.end()));
                }
                auto out = BinaryUtils::openTextStream(path + USS(".out"), err);
                chars.clear();
                if (BinaryUtils::readBinary(*out, chars, 1024 * 1024 * 1024) >= 0) {
                    outStream = StringConvUtils::u8tou(std::string(chars.begin(), chars.end()));
                }
                return std::tuple<icu::UnicodeString, icu::UnicodeString>(inStream, outStream);
            }, [](int64_t v) { std::cout << "Test streams loaded [" << double(v) / 1000000 << "ms]" << std::endl; });

            auto inStream = std::get<0>(streams);
            auto outStream = std::get<1>(streams);

            std::stringstream redirectedIn(StringConvUtils::utou8(inStream));
            return runStream(packageName, redirectedIn, inStream.countChar32(), outStream, file);
        }

        int8_t E2ETests::runStream(icu::UnicodeString packageName, std::istream& redirectedIn, int64_t inSize, icu::UnicodeString outStream, icu::UnicodeString testFileName)
        {
            std::stringstream redirectedOut;

            std::shared_ptr<Context> ctx = nullptr;
            std::shared_ptr<ByteCode> net = nullptr;
            int8_t prepareRes = 1;
            auto t = PerformanceUtils::timeit<std::tuple<std::shared_ptr<Context>, std::shared_ptr<ByteCode>>>(
                    [&redirectedIn, &redirectedOut, &testFileName, &packageName, &prepareRes]() {
                        std::shared_ptr<ByteCode> tnet = nullptr;
                        auto tctx = prepareContext(redirectedIn, redirectedOut);
                        auto loader = tctx->byteLoader;
                        try {
                            auto package = LIBRARY_CASES_PATH + BinaryUtils::packageSeparatorUnicode + packageName;
                            tnet = loader->load(package, tctx);
                            if (tnet->getName() != package) {
                                prepareRes = 0;
                                return std::tuple<std::shared_ptr<Context>, std::shared_ptr<ByteCode>>(tctx, tnet);
                            }
                            ByteCodeUtils::finalizeCode(tnet);
                        }
                        catch (ByteCodeParserException &e) {
                            std::cout << "Failed on " << StringConvUtils::utou8(testFileName) << ": " << e.what() << std::endl;
                            prepareRes = 0;
                            return std::tuple<std::shared_ptr<Context>, std::shared_ptr<ByteCode>>(tctx, nullptr);
                        }
                        return std::tuple<std::shared_ptr<Context>, std::shared_ptr<ByteCode>>(tctx, tnet);
                    }, [](int64_t v) {
                        std::cout << "Network compiled/parsed [" << double(v) / 1000000 << "ms]" << std::endl;
                    });
            if (!prepareRes) {
                return 0;
            }

            ctx = std::get<0>(t);
            net = std::get<1>(t);
            ctx->useByteCode(net);

            auto vc = ctx->setup->getProcessorSetup()->getCodes();
            auto streamLen = (int64_t) vc->size() + inSize + 1;
            auto stepCount = std::max(std::max((int64_t) streamLen, (int64_t) outStream.countChar32()), (int64_t)1);

            Processor p(ctx->setup, ctx);
            ctx->setup->getProcessorSetup()->running = 1;
            PerformanceUtils::timeit([&p, ctx, &stepCount, &testFileName, net]() {
                for (auto i = 0; i < stepCount; ++i) {
                    ctx->setup->getProcessorSetup()->tick++;
                    p.processStep();

                    auto fileName = StringUtils::spf(USS("{0}{1}.txt"), StringConvUtils::utow(testFileName),
                                                      ctx->setup->getProcessorSetup()->tick);
                    int64_t err;
                    auto s = BinaryUtils::openWriteTextStream(fileName, err);
                    if (!s->is_open()) {
                        throw ByteCodeException(
                                StringUtils::spf(_i("err.dump.file.failed"), StringConvUtils::utow(fileName)));
                    }
                    ByteCodeWriter(s, ctx).writeConjugate(net);
                }

                for (auto & i : *ctx->setup->getProcessorSetup()->getCodes()) {
                    ByteCodeUtils::reveal(i.second);
                }
            }, [&stepCount](int64_t v) { std::cout << "Test steps finished [" << double(v) / 1000000 << "ms] " << " having " << double(v) / 1000000 / stepCount << "ms per step" << std::endl; });

            p.terminate();

            return matchStreams(redirectedOut.str(), StringConvUtils::utou8(outStream));
        }

        icu::UnicodeString E2ETests::name()
        {
            return USS("e2e");
        }

        std::map<icu::UnicodeString, std::map<icu::UnicodeString, testCase>> &E2ETests::categories()
        {
            return e2e;
        }
    }
}
