/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#pragma once

namespace nnvm {
    namespace tests {
        class Tests;

        class TestFailureException;
    }
}

#include <LibraryDescriptor.hpp>
#include <predef.h>
#include <inttypes.h>
#include <map>
#include <unicode/unistr.h>
#include <exception>
#include <Logger.hpp>
#include <ProcessorSetup.hpp>
#include <MachineSetup.hpp>
#include <StringConv.hpp>
#include <ByteCodeLoader.hpp>

namespace nnvm {
    namespace tests {
        const icu::UnicodeString TEST_PATH = USS("tests");
        const icu::UnicodeString LIBRARY_CASES_PATH = USS("cases");

        class TestFailureException : public std::exception {
        };

#define fast_assert_less(condition, text) if (!(condition)) { std::cout << (text) << std::endl; return 0; }
#define fast_assert(condition, text, value) if (!(condition)) { std::cout << (text) << (value) << std::endl; return 0; }
#define fast_assert2(condition, text, value1, value2) if (!(condition)) { std::cout << (text) << (value1) << " and " << (value2) << std::endl; return 0; }
#define fast_assert_equals(value1, value2, text) if ((value1) != (value2)) { std::cout << (text) << (value1) << " and " << (value2) << std::endl; return 0; }
#define ref_assert_less(ref_v, condition, text) if (!(condition)) { std::cout << (text) << std::endl; ref_v &= 0; }
#define ref_assert(ref_v, condition, text, value) if (!(condition)) { std::cout << (text) << (value) << std::endl; ref_v &= 0; }
#define ref_assert2(ref_v, condition, text, value1, value2) if (!(condition)) { std::cout << (text) << (value1) << " and " << (value2) << std::endl; ref_v &= 0; }
#define ref_assert_equals(ref_v, value1, value2, text) if ((value1) != (value2)) { std::cout << (text) << (value1) << " and " << (value2) << std::endl; ref_v &= 0; }

        typedef int8_t(*testCase)();

        class Tests {
        public:
            virtual int8_t start();

            virtual int8_t start(int64_t categoryIndex);

            virtual int8_t start(int64_t categoryIndex, int64_t testIndex);

            virtual icu::UnicodeString name() = 0;

            virtual std::map<icu::UnicodeString, std::map<icu::UnicodeString, testCase>> &categories() = 0;

            static std::shared_ptr<MachineSetup> prepareSetup(LogLevel level, std::istream &stdIn,
                                                              std::ostream &stdOut);

            static std::shared_ptr<MachineSetup> prepareSetup(std::istream &stdIn, std::ostream &stdOut);

            static std::shared_ptr<Context> prepareContext(int64_t &i, ProcessorParallelMode mode, std::istream &stdIn,
                                                           std::ostream &stdOut);

            static std::shared_ptr<Context> prepareContext(int64_t &i, ProcessorParallelMode mode, std::istream &stdIn,
                                                           std::ostream &stdOut, int32_t port);

            static std::shared_ptr<Context> prepareContext(int64_t &i, ProcessorParallelMode mode, LogLevel level,
                                                           std::istream &stdIn, std::ostream &stdOut);

            static std::shared_ptr<Context> prepareContext(int64_t &i, ProcessorParallelMode mode, LogLevel level,
                                                           std::istream &stdIn, std::ostream &stdOut, int32_t port);

            static std::shared_ptr<Context> prepareContext(int64_t &i, std::istream &stdIn = std::cin,
                                                           std::ostream &stdOut = std::cout);

            static std::shared_ptr<Context> prepareContext(int64_t &i, LogLevel level, std::istream &stdIn = std::cin,
                                                           std::ostream &stdOut = std::cout);

            static std::shared_ptr<Context> prepareContext(ProcessorParallelMode mode, std::istream &stdIn = std::cin,
                                                           std::ostream &stdOut = std::cout);

            static std::shared_ptr<Context> prepareContext(ProcessorParallelMode mode, LogLevel level,
                                                           std::istream &stdIn = std::cin,
                                                           std::ostream &stdOut = std::cout);

            static std::shared_ptr<Context> prepareContext(std::istream &stdIn = std::cin,
                                                           std::ostream &stdOut = std::cout);

            static std::shared_ptr<Context> prepareContext(LogLevel level, std::istream &stdIn = std::cin,
                                                           std::ostream &stdOut = std::cout);

            static std::shared_ptr<Context> prepareContext(int32_t port, std::istream &stdIn = std::cin,
                                                           std::ostream &stdOut = std::cout);

            static int8_t waitForCondition(const std::function<int8_t()>& condition, int64_t timeoutNs = 2 * 1000 * 1000 * 1000);

            std::vector<std::tuple<int64_t, int64_t, int8_t>> runStat;
        };

        class RedirectedMachineSetup : public MachineSetup {
        public:
            RedirectedMachineSetup(std::shared_ptr<ProcessorSetup> processorSetup,
                                   std::shared_ptr<ByteCodeLoaderSetup> loaderSetup,
                                   std::shared_ptr<ByteCodeParserSetup> parserSetup,
                                   std::shared_ptr<ByteCodeWriterSetup> writerSetup,
                                   std::shared_ptr<RemotingSetup> remotingSetup,
                                   std::vector<icu::UnicodeString> paths,
                                   std::istream &stdIn, std::ostream &stdOut);

            std::istream &stdIn;
            std::ostream &stdOut;
        };
    }
}
