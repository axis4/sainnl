network {
	BYTES = stdin
	RES = + BYTES, 1
	N = # (BYTES >= 52), 1
	[N] G[N] = stdout RES
}
