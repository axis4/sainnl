network {
	BYTES = stdin
	N = # (BYTES >= 49), (- BYTES, 49)
    RES = sbl.functions.simple.foreach N:N
	FILTER = # (RES.COUNTING >= 1), (+ RES.INDEX, 49)
	stdout FILTER
}
