network {
	BYTES = stdin
	N = # (BYTES >= 49), (- BYTES, 49)
    RES = sbl.functions.simple.factorial N:N
	FILTER = # (RES >= 1), RES"(+ RES, 49)"
	stdout FILTER
}
