/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#pragma once

namespace nnvm {
    namespace tests {
        class E2ETests;
    }
}

#include <LibraryDescriptor.hpp>
#include <predef.h>
#include "Tests.hpp"
#include <string>
#include <inttypes.h>
#include <unicode/unistr.h>

namespace nnvm {
    namespace tests {
        class E2ETests : public Tests {
        public:
            virtual icu::UnicodeString name();

            virtual std::map<icu::UnicodeString, std::map<icu::UnicodeString, testCase>> &categories();

            static int8_t runFile(icu::UnicodeString file);

            static int8_t runStream(icu::UnicodeString packageName, std::istream& redirectedIn, int64_t inSize, icu::UnicodeString outStream, icu::UnicodeString testFileName);

            static int8_t matchStreams(std::string actual, std::string expected);
        };
    }
}
