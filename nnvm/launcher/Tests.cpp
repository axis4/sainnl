/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#include "Tests.hpp"
#include <DefaultNeurons.hpp>
#include <utility>
#include "../nnvm/src/utils/Utils.hpp"
#include "../nnvm/src/utils/ThreadUtils.hpp"

namespace nnvm {
    namespace tests {
        RedirectedMachineSetup::RedirectedMachineSetup(std::shared_ptr<ProcessorSetup> processorSetup,
                                                       std::shared_ptr<ByteCodeLoaderSetup> loaderSetup,
                                                       std::shared_ptr<ByteCodeParserSetup> parserSetup,
                                                       std::shared_ptr<ByteCodeWriterSetup> writerSetup,
                                                       std::shared_ptr<RemotingSetup> remotingSetup,
                                                       std::vector<icu::UnicodeString> paths,
                                                       std::istream &stdIn, std::ostream &stdOut)
                : MachineSetup(
                        std::move(processorSetup), std::move(loaderSetup), std::move(parserSetup),
                        std::move(writerSetup), std::move(remotingSetup), std::move(paths), stdIn, stdOut),
                  stdIn(stdIn), stdOut(stdOut) { }

        int8_t Tests::start()
        {
            int8_t res = 0;
            PerformanceUtils::timeit([this, &res]() {
                for (int64_t i = 1; i <= categories().size(); i++) {
                    res += start(i);
                }
                std::cout << "All " << name() << " tests took ";
            });
            if (!res) {
                std::cout << "All " << name() << " tests passed" << std::endl;
            }
            else {
                std::cout << "Failed " << name() << " tests " << (int) res << std::endl;
                std::cout << "Indexes:" << std::endl;
                for (auto &i : runStat) {
                    if (!std::get<2>(i)) {
                        std::cout << "(" << std::get<0>(i) << "," << std::get<1>(i) << ") ";
                    }
                }
                std::cout << std::endl;
            }
            return res;
        }

        int8_t Tests::start(int64_t categoryIndex)
        {
            auto cats = categories();
            if (categoryIndex == 0 || categoryIndex > cats.size()) {
                std::cout << "- Category #" << categoryIndex << " doesn't exist" << std::endl;
                return 2;
            }
            auto it = cats.begin();
            int8_t res = 0;
            for (auto k = 0; k < categoryIndex - 1; k++) { it++; }
            std::cout << "Category " << it->first << " tests" << std::endl;
            PerformanceUtils::timeit([this, &res, &it, &categoryIndex]() {
                for (int64_t i = 1; i <= it->second.size(); i++) {
                    res += start(categoryIndex, i);
                }
                std::cout << "Category took ";
            });
            if (!res) {
                std::cout << "Category " << it->first << " tests passed" << std::endl;
            }
            else {
                std::cout << "Failed " << it->first << " tests " << (int) res << std::endl;
            }
            return res;
        }

        int8_t Tests::start(int64_t categoryIndex, int64_t testIndex)
        {
            auto cats = categories();
            if (categoryIndex == 0 || categoryIndex > cats.size()) {
                std::cout << "- Category #" << categoryIndex << " doesn't exist" << std::endl;
                return 2;
            }
            auto cit = cats.begin();
            for (auto k = 0; k < categoryIndex - 1; k++) { cit++; }

            if (testIndex == 0 || testIndex > cit->second.size()) {
                std::cout << "- Test #" << testIndex << " doesn't exist in category #" << categoryIndex << std::endl;
                return 2;
            }
            int8_t res = 0;
            auto it = cit->second.begin();
            for (auto k = 0; k < testIndex - 1; k++) { it++; }
            int64_t tm = 0;
            finally f([&tm]() {
                std::cout << "[" << double(tm) / 1000000 << "ms]" << std::endl;
            });
            try {
                PerformanceUtils::timeit([&it]() {
                    if (!it->second()) {
                        throw TestFailureException();
                    }
                }, [&tm](int64_t t) { tm = t; });
            }
            catch (TestFailureException &e) {
                std::cout << "- Test " << it->first << " failed ";
                res++;
            }
            catch (std::exception &e) {
                std::cout << "- Test " << it->first << " threw exception: " << e.what() << " ";
                res++;
            }
            catch (...) {
                std::cout << "- Test " << it->first << " threw unexpected exception ";
                res++;
            }
            if (!res) {
                runStat.emplace_back(categoryIndex, testIndex, 1);
                std::cout << "Test " << it->first << " passed ";
            }
            else {
                runStat.emplace_back(categoryIndex, testIndex, 0);
            }
            return res;
        }

        std::shared_ptr<MachineSetup> Tests::prepareSetup(LogLevel level, std::istream &stdIn, std::ostream &stdOut)
        {
            std::vector<icu::UnicodeString> libPaths = {TEST_PATH, _i("default.libs")};
            std::vector<icu::UnicodeString> nativePaths = {_i("default.natives")};
            std::shared_ptr<MachineSetup> res = std::make_shared<MachineSetup>(
                    std::make_shared<ProcessorSetup>(), std::make_shared<ByteCodeLoaderSetup>(nativePaths),
                    std::make_shared<ByteCodeParserSetup>(), std::make_shared<ByteCodeWriterSetup>(),
                    std::make_shared<RemotingSetup>(), libPaths, stdIn, stdOut);

            res->getByteCodeParserSetup()->primitiveNeuron = NeuronNames::ADDMULT;
            res->getByteCodeParserSetup()->additionNeuron = NeuronNames::ADDITIVE;
            res->getByteCodeParserSetup()->multiplicationNeuron = NeuronNames::MULTIPLICATIVE;
            res->getByteCodeParserSetup()->libraryNeuron = NeuronNames::BLOCKCALL;
            res->getByteCodeParserSetup()->aliases[USS("#")] = NeuronNames::ADDITIVE;
            res->getByteCodeParserSetup()->aliases[USS("&")] = NeuronNames::MULTIPLICATIVE;
            res->getByteCodeParserSetup()->aliases[USS("$")] = NeuronNames::STRING;
            res->getByteCodeParserSetup()->aliases[USS("+")] = NeuronNames::PLUS;
            res->getByteCodeParserSetup()->aliases[USS("-")] = NeuronNames::MINUS;
            res->getByteCodeParserSetup()->aliases[USS("*")] = NeuronNames::MULTIPLY;
            res->getByteCodeParserSetup()->aliases[USS("~")] = NeuronNames::BRIDGE;
            res->getByteCodeParserSetup()->arithmeticNeuronNames.insert(NeuronNames::PLUS);
            res->getByteCodeParserSetup()->arithmeticNeuronNames.insert(NeuronNames::MINUS);
            res->getByteCodeParserSetup()->arithmeticNeuronNames.insert(NeuronNames::MULTIPLY);

            res->getLogger()->setLevel(level);

            return res;
        }

        std::shared_ptr<MachineSetup> Tests::prepareSetup(std::istream &stdIn, std::ostream &stdOut)
        {
            return prepareSetup(LogLevel::Warn, stdIn, stdOut);
        }

        std::shared_ptr<Context> Tests::prepareContext(int64_t &i, ProcessorParallelMode mode, std::istream &stdIn,
                                                       std::ostream &stdOut)
        {
            auto setup = prepareSetup(stdIn, stdOut);
            setup->getProcessorSetup()->parallelMode = mode;
            auto lib = std::make_shared<LibraryLoader>(setup);
            auto load = std::make_shared<ByteCodeLoader>(setup, lib);
            auto res = std::make_shared<Context>(setup, lib, load);
            res->getNewIndex = [&i]() { return ++i; };
            return res;
        }

        std::shared_ptr<Context> Tests::prepareContext(int64_t &i, ProcessorParallelMode mode, std::istream &stdIn,
                                                       std::ostream &stdOut, int32_t port)
        {
            auto setup = prepareSetup(stdIn, stdOut);
            setup->getProcessorSetup()->parallelMode = mode;
            setup->getRemotingSetup()->listenAddress = USS("127.0.0.1");
            setup->getRemotingSetup()->listenPort = port;
            setup->getRemotingSetup()->poolSize = 10;
            auto lib = std::make_shared<LibraryLoader>(setup);
            auto load = std::make_shared<ByteCodeLoader>(setup, lib);
            auto res = std::make_shared<Context>(setup, lib, load);
            res->getNewIndex = [&i]() { return ++i; };
            return res;
        }

        std::shared_ptr<Context> Tests::prepareContext(int64_t &i, ProcessorParallelMode mode, LogLevel level,
                                                       std::istream &stdIn, std::ostream &stdOut, int32_t port)
        {
            auto setup = prepareSetup(level, stdIn, stdOut);
            setup->getProcessorSetup()->parallelMode = mode;
            setup->getRemotingSetup()->listenAddress = USS("127.0.0.1");
            setup->getRemotingSetup()->listenPort = port;
            setup->getRemotingSetup()->poolSize = 10;
            auto lib = std::make_shared<LibraryLoader>(setup);
            auto load = std::make_shared<ByteCodeLoader>(setup, lib);
            auto res = std::make_shared<Context>(setup, lib, load);
            res->getNewIndex = [&i]() { return ++i; };
            return res;
        }

        std::shared_ptr<Context> Tests::prepareContext(int64_t &i, ProcessorParallelMode mode, LogLevel level,
                                                       std::istream &stdIn, std::ostream &stdOut)
        {
            auto setup = prepareSetup(level, stdIn, stdOut);
            setup->getProcessorSetup()->parallelMode = mode;
            auto lib = std::make_shared<LibraryLoader>(setup);
            auto load = std::make_shared<ByteCodeLoader>(setup, lib);
            auto res = std::make_shared<Context>(setup, lib, load);
            res->getNewIndex = [&i]() { return ++i; };
            return res;
        }

        std::shared_ptr<Context> Tests::prepareContext(int64_t &i, std::istream &stdIn, std::ostream &stdOut)
        {
            return prepareContext(i, ProcessorParallelMode::NONE, stdIn, stdOut);
        }

        std::shared_ptr<Context> Tests::prepareContext(int64_t &i, LogLevel level, std::istream &stdIn,
                                                       std::ostream &stdOut)
        {
            return prepareContext(i, ProcessorParallelMode::NONE, level, stdIn, stdOut);
        }

        std::shared_ptr<Context> Tests::prepareContext(ProcessorParallelMode mode, std::istream &stdIn,
                                                       std::ostream &stdOut)
        {
            int64_t i;
            return prepareContext(i, mode, stdIn, stdOut);
        }

        std::shared_ptr<Context> Tests::prepareContext(ProcessorParallelMode mode, LogLevel level, std::istream &stdIn,
                                                       std::ostream &stdOut)
        {
            int64_t i;
            return prepareContext(i, mode, level, stdIn, stdOut);
        }

        std::shared_ptr<Context> Tests::prepareContext(std::istream &stdIn, std::ostream &stdOut)
        {
            int64_t i;
            return prepareContext(i, ProcessorParallelMode::NONE, stdIn, stdOut);
        }

        std::shared_ptr<Context> Tests::prepareContext(LogLevel level, std::istream &stdIn, std::ostream &stdOut)
        {
            int64_t i;
            return prepareContext(i, level, stdIn, stdOut);
        }

        std::shared_ptr<Context> Tests::prepareContext(int32_t port, std::istream &stdIn, std::ostream &stdOut)
        {
            int64_t i;
            return prepareContext(i, ProcessorParallelMode::NONE, stdIn, stdOut, port);
        }

        int8_t Tests::waitForCondition(const std::function<int8_t()>& condition, int64_t timeoutNs)
        {
            ThreadUtils::wait(1);
            auto start = PerformanceUtils::now_nanoseconds();
            int8_t res;
            while(!(res = condition()) && PerformanceUtils::now_nanoseconds() - start < timeoutNs) {
                ThreadUtils::wait(1);
            }

            return res;
        }
    }
}
