/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#include "UnitTests.hpp"
#include <NNProtocol.hpp>
#include <BinaryUtils.hpp>

namespace nnvm {
    namespace tests {
        template<typename T, int64_t value, int64_t divider, int8_t writer(const T, const std::shared_ptr<std::ostream>&), int8_t reader(std::istream&, T&, std::vector<uint8_t>&)>
        int8_t serialization()
        {
            auto ss = std::make_shared<std::stringstream>();
            T exp = (T)value / (T)divider;
            writer(exp, ss);
            T act;
            std::vector<uint8_t> tmp;
            reader(*ss, act, tmp);
            fast_assert_equals(exp, act, "Mismatched value ");

            return (int8_t) 1;
        }

        template<int64_t value>
        int8_t integerSerialization()
        {
            return serialization<int64_t, value, 1, BinaryUtils::writeInteger, BinaryUtils::readInteger>();
        }

        template<int64_t value, int64_t divider>
        int8_t doubleSerialization()
        {
            return serialization<double, value, divider, BinaryUtils::writeDouble, BinaryUtils::readDouble>();
        }

        int8_t testNNProtocolEnvelope()
        {
            auto ctx = Tests::prepareContext();
            nnvm::grid::NNProtocol p(ctx);
            nnvm::grid::Socket connection("localhost", 10002, nnvm::grid::ProtocolType::TCP);
            nnvm::grid::NNPayload data(USS("test"));
            std::vector<int8_t> b;
            fast_assert_less(p.doSerialize(connection, data, b), "Failed to serialize");
            nnvm::grid::NNPayload data2;
            fast_assert_less(p.doDeserialize(connection, b, data2), "Failed to deserialize");
            fast_assert_equals(data.type, data2.type, "Mismatched data type ");
            fast_assert_equals(data.pointer1, data2.pointer1, "Mismatched data pointer1 ");
            fast_assert_equals(data.pointer2, data2.pointer2, "Mismatched data pointer2 ");
            fast_assert_equals(data.majorVersion, data2.majorVersion, "Mismatched data majorVersion ");
            fast_assert_equals(data.minorVersion, data2.minorVersion, "Mismatched data minorVersion ");
            fast_assert2(data.message == data2.message, "Mismatched data message ", StringConvUtils::utou8(data.message), StringConvUtils::utou8(data2.message));

            return (int8_t) 1;
        }

        int8_t testNNProtocolStatus()
        {
            auto ctx = Tests::prepareContext();
            nnvm::grid::NNProtocol p(ctx);
            nnvm::grid::Socket connection("localhost", 10002, nnvm::grid::ProtocolType::TCP);
            nnvm::grid::NNStatus status;
            status.nodeCount = 2;
            status.edgeCount = 1;
            status.stepCount = 1;
            status.node.min = 1.23;
            status.node.max = 12.3;
            status.node.avg = 5;
            status.node.sum = 45.123412335;
            status.node.quantiles[0.5] = 6;
            status.node.quantiles[0.9] = 10;
            status.node.quantiles[0.95] = 11;
            status.node.quantiles[0.99] = 12.1;
            status.uptime = 10000;
            status.nodes[USS("test1")].max = 100;
            status.nodes[USS("test2")].max = 200;
            nnvm::grid::NNPayload data(status);
            std::vector<int8_t> b;
            fast_assert_less(p.doSerialize(connection, data, b), "Failed to serialize");
            nnvm::grid::NNPayload data2;
            fast_assert_less(p.doDeserialize(connection, b, data2), "Failed to deserialize");
            fast_assert_equals(nnvm::grid::NNPayloadType::STATUS_RESPONSE, data2.type, "Mismatched data type ");
            fast_assert_equals(data.type, data2.type, "Mismatched data type ");
            fast_assert2(data.message == data2.message, "Mismatched data message ", StringConvUtils::utou8(data.message), StringConvUtils::utou8(data2.message));
            fast_assert_equals(data.status.uptime, data2.status.uptime, "Mismatched data uptime ");
            fast_assert_equals(data.status.nodeCount, data2.status.nodeCount, "Mismatched data nodeCount ");
            fast_assert_equals(data.status.edgeCount, data2.status.edgeCount, "Mismatched data edgeCount ");
            fast_assert_equals(data.status.stepCount, data2.status.stepCount, "Mismatched data stepCount ");
            fast_assert_equals(data.status.node.max, data2.status.node.max, "Mismatched data node.max ");
            fast_assert_equals(data.status.node.min, data2.status.node.min, "Mismatched data node.min ");
            fast_assert_equals(data.status.node.avg, data2.status.node.avg, "Mismatched data node.avg ");
            fast_assert_equals(data.status.node.sum, data2.status.node.sum, "Mismatched data node.sum ");
            for (auto i = data.status.node.quantiles.begin(), j = data2.status.node.quantiles.begin(); i != data.status.node.quantiles.end() && j != data2.status.node.quantiles.end(); ++i, ++j) {
                fast_assert_equals(i->first, j->first, "Mismatched data node.quantiles key ");
                fast_assert_equals(i->second, j->second, "Mismatched data node.quantiles value ");
            }
            fast_assert_equals(100, data2.status.nodes[USS("test1")].max, "Mismatched data node[USS(\"test1\")].max ");
            fast_assert_equals(data.status.nodes[USS("test1")].max, data2.status.nodes[USS("test1")].max, "Mismatched data node[USS(\"test1\")].max ");
            fast_assert_equals(200, data2.status.nodes[USS("test2")].max, "Mismatched data node[USS(\"test2\")].max ");
            fast_assert_equals(data.status.nodes[USS("test2")].max, data2.status.nodes[USS("test2")].max, "Mismatched data node[USS(\"test2\")].max ");

            return (int8_t) 1;
        }

        int8_t testNNProtocolState()
        {
            auto ctx = Tests::prepareContext();
            nnvm::grid::NNProtocol p(ctx);
            nnvm::grid::Socket connection("localhost", 10002, nnvm::grid::ProtocolType::TCP);
            auto state = std::make_shared<ByteCode>(USS("test"), ctx->byteLoader);
            auto block = std::make_shared<ByteCodeBlock>(USS("testBlock"), USS("testType"));
            auto block2 = std::make_shared<ByteCodeBlock>(USS("testBlock2"), USS("testType"));
            auto item = std::make_shared<ByteCodeBlockItem>(block2->getName(), block);
            block->addItem(item);
            state->addBlock(block);
            state->addBlock(block2);
            nnvm::grid::NNPayload data(state);
            std::vector<int8_t> b;
            fast_assert_less(p.doSerialize(connection, data, b), "Failed to serialize");
            nnvm::grid::NNPayload data2;
            fast_assert_less(p.doDeserialize(connection, b, data2), "Failed to deserialize");
            fast_assert_equals(nnvm::grid::NNPayloadType::STATE_RESPONSE, data2.type, "Mismatched data type ");
            fast_assert_equals(data.type, data2.type, "Mismatched data type ");
            fast_assert_equals(data.state->getName(), data2.state->getName(), "Mismatched data package name ");
            auto b1 = data.state->getBlock(USS("testBlock"));
            auto b2 = data2.state->getBlock(USS("testBlock"));
            fast_assert_less(std::get<1>(data2.state->getBlocks()) != b2, "Mismatched data block not found");
            fast_assert_equals((*b1)->getName(), (*b2)->getName(), "Mismatched data block name ");
            auto i1 = (*b1)->getLastItem();
            auto i2 = (*b2)->getLastItem();
            fast_assert_less(i2 != nullptr, "Mismatched data block's item not found");
            fast_assert_equals(i1->getName(), i2->getName(), "Mismatched data block's item name ");
            fast_assert_less(i2->getOwner().lock() == *b2, "Mismatched data block's item owner ");
            b1 = data.state->getBlock(USS("testBlock2"));
            b2 = data2.state->getBlock(USS("testBlock2"));
            fast_assert_less(std::get<1>(data2.state->getBlocks()) != b2, "Mismatched data block2 not found");
            fast_assert_equals((*b1)->getName(), (*b2)->getName(), "Mismatched data block2 name ");
            fast_assert_less(i2->getName() == (*b2)->getName(), "Mismatched data block's item name and block2 ");

            return (int8_t) 1;
        }

        int8_t testNNProtocolFlow()
        {
            auto na = USS("A");
            auto nb = USS("B");
            auto nc = USS("C");
            auto ctx = Tests::prepareContext();
            nnvm::grid::NNProtocol p(ctx);
            nnvm::grid::Socket connection("localhost", 10002, nnvm::grid::ProtocolType::TCP);
            nnvm::grid::NNFlow flow;
            flow.destinations[na][nc].activation = 0.5;
            flow.destinations[na][nc].stream = 1.5;
            flow.destinations[na][nc].generation = -0.5;
            flow.destinations[nb][nc].activation = 10.5;
            flow.destinations[nb][nc].stream = 1.5678;
            flow.destinations[nb][nc].generation = -0.5678;
            nnvm::grid::NNPayload data(flow);
            std::vector<int8_t> b;
            fast_assert_less(p.doSerialize(connection, data, b), "Failed to serialize");
            nnvm::grid::NNPayload data2;
            fast_assert_less(p.doDeserialize(connection, b, data2), "Failed to deserialize");
            fast_assert_equals(nnvm::grid::NNPayloadType::FLOW, data2.type, "Mismatched data type ");
            fast_assert_equals(data.type, data2.type, "Mismatched data type ");
            fast_assert_less(data2.flow.destinations.find(USS("A")) != data2.flow.destinations.end(), "Mismatched data flow destinations key A");
            fast_assert_less(data2.flow.destinations.find(USS("B")) != data2.flow.destinations.end(), "Mismatched data flow destinations key B");
            for (auto &i : data.flow.destinations) {
                for (auto &k : i.second) {
                    fast_assert_equals(k.second.activation, data2.flow.destinations[i.first][k.first].activation, "Mismatched data destination " + StringConvUtils::utou8(i.first) + " from " + StringConvUtils::utou8(k.first) + " activation ");
                    fast_assert_equals(k.second.stream, data2.flow.destinations[i.first][k.first].stream, "Mismatched data destination " + StringConvUtils::utou8(i.first) + " from " + StringConvUtils::utou8(k.first) + " stream ");
                    fast_assert_equals(k.second.generation, data2.flow.destinations[i.first][k.first].generation, "Mismatched data destination " + StringConvUtils::utou8(i.first) + " from " + StringConvUtils::utou8(k.first) + " generation ");
                }
            }

            return (int8_t) 1;
        }

        std::map<icu::UnicodeString, testCase> simple = {
                {USS("01. one byte offset"), []() { return (int8_t) 1; }},
        };

        std::map<icu::UnicodeString, testCase> serializer = {
                {USS("01. integer serializer"), integerSerialization<100>},
                {USS("02. integer negative serializer"), integerSerialization<-100>},
                {USS("03. integer 2 byte serializer"), integerSerialization<0x1234>},
                {USS("04. integer 2 byte negative serializer"), integerSerialization<-0x1234>},
                {USS("05. big integer serializer"), integerSerialization<1000LL*1000*1000*1000*1000>},
                {USS("06. big integer negative serializer"), integerSerialization<-1000LL*1000*1000*1000*1000>},
                {USS("07. double serializer"), doubleSerialization<1234567, 1000000>},
                {USS("08. double zero serializer"), doubleSerialization<0, 1>},
                {USS("09. double one serializer"), doubleSerialization<1, 1>},
                {USS("10. double one negative serializer"), doubleSerialization<-1, 1>},
                {USS("11. double special serializer"), doubleSerialization<5, 10>},
                {USS("12. double special negative serializer"), doubleSerialization<-5, 10>},
        };

        std::map<icu::UnicodeString, testCase> protocol = {
                {USS("01. nnprotocol serialization basic"), testNNProtocolEnvelope},
                {USS("02. nnprotocol serialization status"), testNNProtocolStatus},
                {USS("03. nnprotocol serialization state"), testNNProtocolState},
                {USS("04. nnprotocol serialization flow"), testNNProtocolFlow},
        };

        std::map<icu::UnicodeString, std::map<icu::UnicodeString, testCase>> units = {
                {USS("01. simple cases"), simple},
                {USS("02. serializer"), serializer},
                {USS("03. grid protocol"), protocol},
        };

        icu::UnicodeString UnitTests::name() {
            return USS("unit");
        }

        std::map<icu::UnicodeString, std::map<icu::UnicodeString, testCase>> &UnitTests::categories() {
            return units;
        }
    }
}