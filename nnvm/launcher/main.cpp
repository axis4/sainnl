/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

// main.cpp : Defines the entry point for the console application.

#include <string>
#include <iostream>
#include <map>
#include <unicode/unistr.h>
#if U_PLATFORM_HAS_WIN32_API > 0
#include <direct.h>
#define getcwd _getcwd
#else
#include <unistd.h>
#endif
#include <Internationalization.hpp>
#include <StringConv.hpp>
#include "../nnvm/src/VM.hpp"
#include "../nnvm/src/utils/Utils.hpp"
#include "UnitTests.hpp"
#include "IntegrationTests.hpp"
#include "E2ETests.hpp"

void parseArgs(int argc, char* argv[], std::map<icu::UnicodeString, icu::UnicodeString>& args)
{
    for (auto i = 1; i < argc; i++) {
        if (std::char_traits<char>::length(argv[i]) > 2 && argv[i][0] == '-' && argv[i][1] == '-') {
            std::string s(argv[i]);
            auto vstart = s.find('=');
            if (vstart != std::string::npos) {
                auto key = nnvm::StringConvUtils::u8tou(s.substr(2, vstart - 2));
                args[key] = nnvm::StringConvUtils::u8tou(s.substr(vstart + 1));
            }
            else {
                auto key = nnvm::StringConvUtils::u8tou(s.substr(2));
                args[key] = USS("1");
            }
        }
        else {
            args[_i("key.package")] = nnvm::StringConvUtils::u8tou(std::string(argv[i]));
        }
    }
}

std::shared_ptr<nnvm::VM> buildVM(std::map<icu::UnicodeString, icu::UnicodeString>& args)
{
    return std::make_shared<nnvm::VM>(args);
}

std::string cwd()
{
    auto c = getcwd(nullptr, 0);
    if (c != nullptr) {
        std::string res(c);
        free(c);
        return res;
    }
    char buf[4096];
    if (getcwd(buf, sizeof(buf)*sizeof(char)) == nullptr) {
        throw std::runtime_error("CWD failed");
    }
    return std::string(buf);
}

int main(int argc, char* argv[], char* envp[])
{
#if U_PLATFORM_HAS_WIN32_API > 0
    setlocale(LC_ALL, "english_us.65001");
    std::wcin.imbue(std::locale(std::locale("english_us.65001"), new nnvm::no_separator()));
    std::wcout.imbue(std::locale(std::locale("english_us.65001"), new nnvm::no_separator()));
    std::cin.imbue(std::locale(std::locale("english_us.65001"), new nnvm::no_separator()));
    std::cout.imbue(std::locale(std::locale("english_us.65001"), new nnvm::no_separator()));
    std::locale::global(std::locale(std::locale("english_us.65001"), new nnvm::no_separator()));
#else
    setlocale(LC_ALL, "en_US.UTF-8");
    std::cin.imbue(std::locale(std::locale("en_US.UTF-8"), new nnvm::no_separator()));
    std::cout.imbue(std::locale(std::locale("en_US.UTF-8"), new nnvm::no_separator()));
    std::locale::global(std::locale(std::locale("en_US.UTF-8"), new nnvm::no_separator()));
#endif
    auto t1 = _i("language.probe");
    auto t2 = USS("true");
    if (t1 != t2) {
        std::cerr << "Internationalization file not found";
        auto c = cwd();
        if (c.size()) {
            std::cerr << ": " << c;
        }
        std::cerr << std::endl;
        return 1;
    }

    if (argc > 1 && std::string(argv[1]) == nnvm::StringConvUtils::utou8(_i("key.unit.test"))) {
        if (argc > 2)
        {
            if (argc > 3)
            {
                return nnvm::tests::UnitTests().start(atoi(argv[2]), atoi(argv[3]));
            }
            return nnvm::tests::UnitTests().start(atoi(argv[2]));
        }
        return nnvm::tests::UnitTests().start();
    }
    if (argc > 1 && std::string(argv[1]) == nnvm::StringConvUtils::utou8(_i("key.integration.test"))) {
        if (argc > 2)
        {
            if (argc > 3)
            {
                return nnvm::tests::IntegrationTests().start(atoi(argv[2]), atoi(argv[3]));
            }
            return nnvm::tests::IntegrationTests().start(atoi(argv[2]));
        }
        return nnvm::tests::IntegrationTests().start();
    }
    if (argc > 1 && std::string(argv[1]) == nnvm::StringConvUtils::utou8(_i("key.library.test"))) {
        if (argc > 2)
        {
            if (argc > 3)
            {
                return nnvm::tests::E2ETests().start(atoi(argv[2]), atoi(argv[3]));
            }
            return nnvm::tests::E2ETests().start(atoi(argv[2]));
        }
        return nnvm::tests::E2ETests().start();
    }
    if (argc > 1 && std::string(argv[1]) == nnvm::StringConvUtils::utou8(_i("key.test"))) {
        nnvm::tests::UnitTests ut;
        nnvm::tests::IntegrationTests it;
        nnvm::tests::E2ETests et;
        auto unit = ut.start();
        auto integration = it.start();
        auto libraries = et.start();
        if (unit || integration || libraries) {
            std::cout << "As a failure summary:" << std::endl;
            if (unit) {
                std::cout << "Unit: ";
                for (auto & i : ut.runStat) {
                    if (!std::get<2>(i)) {
                        std::cout << "(" << std::get<0>(i) << "," << std::get<1>(i) << ") ";
                    }
                }
                std::cout << std::endl;
            }
            if (integration) {
                std::cout << "Integration: ";
                for (auto & i : it.runStat) {
                    if (!std::get<2>(i)) {
                        std::cout << "(" << std::get<0>(i) << "," << std::get<1>(i) << ") ";
                    }
                }
                std::cout << std::endl;
            }
            if (libraries) {
                std::cout << "E2E: ";
                for (auto & i : et.runStat) {
                    if (!std::get<2>(i)) {
                        std::cout << "(" << std::get<0>(i) << "," << std::get<1>(i) << ") ";
                    }
                }
                std::cout << std::endl;
            }
        }
        return !(!unit && !integration && !libraries);
    }

    std::map<icu::UnicodeString, icu::UnicodeString> args;
    parseArgs(argc, argv, args);
    if (args.empty()) {
        std::wcerr << nnvm::StringConvUtils::utow(_i("usage")) << std::endl;
        return 1;
    }
    int8_t res = 1;
    std::shared_ptr<nnvm::VM> vm = nullptr;
    try {
        vm = buildVM(args);
        res = vm->execute();
    }
    catch (std::exception& e) {
        std::wcerr << e.what() << std::endl;
    }
    return res;
}

