/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#include "IntegrationTests.hpp"
#include <cmath>
#include <iostream>
#include <sstream>
#include <unicode/uchar.h>
#include <unicode/numfmt.h>
#include <unicode/ustream.h>
#include <cinttypes>
#include <asio.hpp>
#include <NameParts.hpp>
#include <ByteCode.hpp>
#include <ByteCodeParserException.hpp>
#include <NameParserException.hpp>
#include <StateMachine.hpp>
#include <MachineSetup.hpp>
#include <ByteCodeParser.hpp>
#include <ByteCodeUtils.hpp>
#include <BinaryUtils.hpp>
#include <BaseNeurons.hpp>
#include <DefaultNeurons.hpp>
#include <StringUtils.hpp>
#include <Server.hpp>
#include <NNProtocol.hpp>
#include "../nnvm/src/writer/ByteCodeWriter.hpp"
#include "../nnvm/src/processor/Processor.hpp"
#include "../nnvm/src/parser/DefinitionBuilder.hpp"
#include "../nnvm/src/utils/ThreadUtils.hpp"

namespace nnvm {
    namespace tests {
        const double EPSILON = DBL_EPSILON;
        const icu::UnicodeString BLOCKCALL = USS("blockcall");
        const icu::UnicodeString ADDMULT = USS("addmult");
        const icu::UnicodeString ADDITIVE = USS("additive");
        const icu::UnicodeString MULTIPLICATIVE = USS("multiplicative");
        const icu::UnicodeString EXIT = USS("exit");
        const icu::UnicodeString TICK = USS("tick");
        const icu::UnicodeString TIME = USS("time");
        const icu::UnicodeString STRING = USS("string");

        void shoutError(const icu::UnicodeString &paramName, const std::shared_ptr<ByteCodeParameter> &resparam,
                        double expected, const std::shared_ptr<ByteCodeBlock> &b3, int64_t tick) {
            std::cout << "Error value for " << StringConvUtils::utou8(b3->getName()) << " on " << tick << " step" <<
            std::endl;
            if (resparam) {
                std::cout << "Value is " << resparam->value.doubleValue << " but expected " << expected << std::endl;
            }
            else {
                std::cout << "No " << StringConvUtils::utou8(paramName) << " param" << std::endl;
            }
        }

        class TestProtocol : public grid::NNProtocol
        {
        public:
            explicit TestProtocol(const std::shared_ptr<Context> &ctx) : NNProtocol(ctx), sequence(0) {}

            ~TestProtocol() override
            {
                enableSyncs.clear();
                syncs.clear();
                terminates.clear();
                flows.clear();
                acks.clear();
                statuses.clear();
                states.clear();
                initialStates.clear();
            }

            void onReceiveError(const grid::Socket &connection, const std::error_code &error) override
            {
                //std::cout<<"Receive failed!";
            }

            void onSendEnd(const std::error_code &error, int64_t bytes_transferred,
                           std::shared_ptr<std::vector<int8_t>> data) override
            {
                //std::cout<<"Sent!";
            }

            void onConnectError(const grid::Socket &connection, const std::error_code &error) override
            {
                //std::cout<<"Connect failed!";
            }

            int64_t sequence;

            std::deque<int64_t> enableSyncs;
            std::deque<int64_t> syncs;
            std::deque<int64_t> terminates;
            std::deque<std::tuple<int64_t, grid::NNFlow>> flows;
            std::deque<std::tuple<int64_t, icu::UnicodeString>> acks;
            std::deque<std::tuple<int64_t, grid::NNStatus>> statuses;
            std::deque<std::tuple<int64_t, std::shared_ptr<ByteCode>>> states;
            std::deque<std::tuple<int64_t, std::shared_ptr<ByteCode>>> initialStates;

        protected:
            int8_t acceptEnableSync() override {
                ctx->setup->getProcessorSetup()->remoteSync = 1;
                enableSyncs.push_back(++sequence);
                return 1;
            }

            int8_t acceptSync() override {
                syncs.push_back(++sequence);
                return 1;
            }

            int8_t acceptTerminate() override {
                terminates.push_back(++sequence);
                return 1;
            }

            int8_t acceptFlow(const grid::NNFlow &flow) override {
                flows.emplace_back(++sequence, flow);
                return 1;
            }

            int8_t acceptAck(const icu::UnicodeString &message) override {
                acks.emplace_back(++sequence, message);
                return 1;
            }

            int8_t acceptStatus(const grid::NNStatus &status) override {
                statuses.emplace_back(++sequence, status);
                return 1;
            }

            int8_t acceptState(std::shared_ptr<ByteCode> state) override {
                states.emplace_back(++sequence, state);
                return 1;
            }

            int8_t acceptInitialState(std::shared_ptr<ByteCode> state) override {
                initialStates.emplace_back(++sequence, state);
                return 1;
            }
        };

        std::string catStrings(char *chars, int32_t count) {
            std::string res(chars, chars + count);
            return res;
        }

        std::string catStrings(std::string *stringList, int32_t count) {
            std::string res;
            for (int i = 0; i < count; i++) {
                res += stringList[i];
            }
            return res;
        }

        std::string makeString(const icu::UnicodeString &string) {
            auto tmpss = std::make_shared<std::stringstream>();
            BinaryUtils::writeString(string, tmpss);
            return tmpss->str();
        }

        int8_t wideToUnicode() {
            int64_t l = 10;
            std::wstring etalon(L"test\u0422\u0435\u0441\u044290");
            std::wstring res;
            int64_t n = 1000000;
            int64_t tm;
            PerformanceUtils::timeit([&n, &res, &etalon]() {
                for (auto i = 0; i < n; ++i) {
                    res = StringConvUtils::utow(StringConvUtils::wtou(etalon));
                }
            }, [&tm](int64_t t) { tm = t; });
            std::cout << "Performance " << double(tm) / n / l << "ns/symbol" << std::endl;
            return (int8_t) (etalon == res ? 1 : 0);
        }

        int8_t wideToUtf() {
            int64_t l = 10;
            std::wstring etalon(L"test\u0422\u0435\u0441\u044290");
            std::wstring res;
            int64_t n = 1000000;
            int64_t tm;
            PerformanceUtils::timeit([&n, &res, &etalon]() {
                for (auto i = 0; i < n; ++i) {
                    res = StringConvUtils::u8tow(StringConvUtils::wtou8(etalon));
                }
            }, [&tm](int64_t t) { tm = t; });
            std::cout << "Performance " << double(tm) / n / l << "ns/symbol" << std::endl;
            return (int8_t) (etalon == res ? 1 : 0);
        }

        int8_t utfToUnicode() {
            int64_t l = 10;
            auto etalon = StringConvUtils::wtou8(L"test\u0422\u0435\u0441\u044290");
            std::string res;
            int64_t n = 1000000;
            int64_t tm;
            PerformanceUtils::timeit([&n, &res, &etalon]() {
                for (auto i = 0; i < n; ++i) {
                    res = StringConvUtils::utou8(StringConvUtils::u8tou(etalon));
                }
            }, [&tm](int64_t t) { tm = t; });
            std::cout << "Performance " << double(tm) / n / l << "ns/symbol" << std::endl;
            return (int8_t) (etalon == res ? 1 : 0);
        }

        int8_t constantStringUnicode() {
            int64_t l = 10;
            auto etalon = StringConvUtils::wtou8(L"test\u0422\u0435\u0441\u044290");
            std::string res;
            int64_t n = 1000000;
            int64_t tm;
            PerformanceUtils::timeit([&n, &res, &etalon]() {
                for (auto i = 0; i < n; ++i) {
                    res = StringConvUtils::utou8(USS("test\u0422\u0435\u0441\u044290"));
                }
            }, [&tm](int64_t t) { tm = t; });
            std::cout << "Performance " << double(tm) / n / l << "ns/symbol" << std::endl;
            return (int8_t) (etalon == res ? 1 : 0);
        }

        int8_t constantStringUnicodeStandard() {
            int64_t l = 10;
            auto etalon = StringConvUtils::wtou8(L"test\u0422\u0435\u0441\u044290");
            std::string res;
            int64_t n = 1000000;
            int64_t tm;
            PerformanceUtils::timeit([&n, &res, &etalon]() {
                for (auto i = 0; i < n; ++i) {
                    res = StringConvUtils::utou8(UNICODE_STRING_SIMPLE("test\u0422\u0435\u0441\u044290"));
                }
            }, [&tm](int64_t t) { tm = t; });
            std::cout << "Performance " << double(tm) / n / l << "ns/symbol" << std::endl;
            return (int8_t) (etalon == res ? 1 : 0);
        }

        int8_t spfWide() {
            int64_t l = 10;
            std::wstring etalon(L"test\u0422\u0435\u0441\u044290");
            std::wstring res;
            int64_t n = 1000000;
            int64_t tm;
            PerformanceUtils::timeit([&n, &res, &etalon]() {
                for (auto i = 0; i < n; ++i) {
                    res = StringUtils::spf(std::wstring(L"test{0}"), std::wstring(L"\u0422\u0435\u0441\u044290"));
                }
            }, [&tm](int64_t t) { tm = t; });
            std::cout << "Performance " << double(tm) / n / l << "ns/symbol" << std::endl;
            return (int8_t) (etalon == res ? 1 : 0);
        }

        int8_t spfUnicode() {
            int64_t l = 10;
            auto etalon = USS("test\u0422\u0435\u0441\u044290");
            icu::UnicodeString res;
            int64_t n = 1000000;
            int64_t tm;
            PerformanceUtils::timeit([&n, &res, &etalon]() {
                for (auto i = 0; i < n; ++i) {
                    res = StringUtils::spf(USS("test{0}"), StringConvUtils::utow(USS("\u0422\u0435\u0441\u044290")));
                }
            }, [&tm](int64_t t) { tm = t; });
            std::cout << "Performance " << double(tm) / n / l << "ns/symbol" << std::endl;
            return (int8_t) (etalon == res ? 1 : 0);
        }

        int8_t spfVsConcatUnicode() {
            auto pattern = USS("{0}.[{1}]");
            auto etalon = USS("test.[10]");
            auto name = USS("test");
            int64_t v = 10;
            int64_t l = etalon.countChar32();
            int64_t n = 1000000;
            int64_t ts;
            int64_t tc;
            icu::UnicodeString rs;
            icu::UnicodeString rc;
            PerformanceUtils::timeit([&n, &name, &v, &rs, &pattern]() {
                for (auto i = 0; i < n; ++i) {
                    rs = StringUtils::spf(pattern, StringConvUtils::utow(name), v);
                }
            }, [&ts](int64_t t) { ts = t; });
            PerformanceUtils::timeit([&n, &name, &v, &rc, &pattern]() {
                for (auto i = 0; i < n; ++i) {
                    rc = name + USS(".[") + ByteCodeUtils::asString(v) + USS("]");
                }
            }, [&tc](int64_t t) { tc = t; });
            std::cout << "SPF=" << double(ts) / n / l << "ns/symbol, CONCAT=" << double(tc) / n / l <<  "ns/symbol, Latency is " << tc / ts << " times" << std::endl;
            return (int8_t) (etalon == rs && etalon == rc ? 1 : 0);
        }

        int8_t performanceUnicode() {
            int64_t l = 6;
            int64_t n = 1000000;
            int64_t tw;
            int64_t tu;
            std::vector<std::wstring> sw((unsigned long) n);
            std::vector<icu::UnicodeString> su((unsigned long) n);
            PerformanceUtils::timeit([&n, &sw]() {
                for (auto i = 0; i < n; ++i) {
                    sw[i] = L"SIMPLE";
                }
            }, [&tw](int64_t t) { tw = t; });
            PerformanceUtils::timeit([&n, &su]() {
                for (auto i = 0; i < n; ++i) {
                    su[i] = USS("SIMPLE");
                }
            }, [&tu](int64_t t) { tu = t; });
            std::cout << "WS=" << double(tw) / n / l << "ns/symbol, UN=" << double(tu) / n / l << "ns/symbol, Latency is " << tu / tw << " times" << std::endl;
            return 1;
        }

        int8_t performanceGenerator() {
            int64_t n = 1000000;
            int64_t tw;
            int64_t tg;
            std::wstring start = L"SIMPLE TEST STRING";
            std::wstring end;
            auto l = (int64_t) start.length();
            PerformanceUtils::timeit([&n, &start, &end]() {
                for (auto i = 0; i < n; ++i) {
                    end = start;
                }
            }, [&tw](int64_t t) { tw = t; });
            PerformanceUtils::timeit([&n, &start, &end]() {
                auto k = std::make_shared<int64_t>(0);
                auto g = Generator<wchar_t, int64_t>::create([&start](std::shared_ptr<int64_t> i, wchar_t &res) {
                    if (start.length() >= *i) { return 0; }
                    res = start[*i];
                    ++(*i);
                    return 1;
                }, k);
                for (auto i = 0; i < n; ++i) {
                    *k = 0;
                    auto r = g->range();
                    std::basic_ostringstream<wchar_t> os;
                    for (auto si = std::get<0>(r); si != std::get<1>(r); ++si) {
                        os << *si;
                    }
                    end = os.str();
                }
            }, [&tg](int64_t t) { tg = t; });
            std::cout << "WS=" << double(tw) / n / l << "ns/symbol, GEN=" << double(tg) / n / l <<
            "ns/symbol, Latency is " << tg / tw << " times" << std::endl;
            return 1;
        }

        int8_t performanceFormatter() {
            int64_t n = 1000000;
            int64_t ts = 0;
            int64_t tf = 0;
            int64_t tif = 0;
            int64_t tpf = 0;
            double rs = 0;
            double rf = 0;
            double rif = 0;
            double rpf = 0;
            icu::UnicodeString u = USS("1234567890.1234567890");
            int64_t l = u.countChar32();
            PerformanceUtils::timeit([&n, &u, &rs]() {
                for (auto i = 0; i < n; ++i) {
                    std::wstringstream ss;
                    ss << StringConvUtils::utow(u).c_str();
                    ss >> rs;
                }
            }, [&ts](int64_t t) { ts = t; });
            PerformanceUtils::timeit([&n, &u, &rf]() {
                for (auto i = 0; i < n; ++i) {
                    icu::Formattable f;
                    UErrorCode success = U_ZERO_ERROR;
                    auto nf = icu::NumberFormat::createInstance(success);
                    nf->parse(u, f, success);
                    if (success <= U_ZERO_ERROR) {
                        auto res = f.getDouble(success);
                        rf = success <= U_ZERO_ERROR ? res : 0;
                    }
                }
            }, [&tf](int64_t t) { tf = t; });
            UErrorCode successCreate = U_ZERO_ERROR;
            auto nf = icu::NumberFormat::createInstance(successCreate);
            if (successCreate <= U_ZERO_ERROR) {
                PerformanceUtils::timeit([&n, &u, &rif, &nf]() {
                    for (auto i = 0; i < n; ++i) {
                        icu::Formattable f;
                        UErrorCode success = U_ZERO_ERROR;
                        nf->parse(u, f, success);
                        if (success <= U_ZERO_ERROR) {
                            auto res = f.getDouble(success);
                            rif = success <= U_ZERO_ERROR ? res : 0;
                        }
                    }
                }, [&tif](int64_t t) { tif = t; });
            }
            icu::Formattable f;
            if (successCreate <= U_ZERO_ERROR) {
                PerformanceUtils::timeit([&n, &u, &rpf, &nf, &f]() {
                    for (auto i = 0; i < n; ++i) {
                        UErrorCode success = U_ZERO_ERROR;
                        nf->parse(u, f, success);
                        if (success <= U_ZERO_ERROR) {
                            auto res = f.getDouble(success);
                            rpf = success <= U_ZERO_ERROR ? res : 0;
                        }
                    }
                }, [&tpf](int64_t t) { tpf = t; });
            }
            std::cout << "STREAM=" << double(ts) / n / l << "ns/symbol, ";
            std::cout << "FORMATTER=" << double(tf) / n / l << "ns/symbol, ";
            std::cout << "INIT_FORMATTER=" << double(tif) / n / l << "ns/symbol, ";
            std::cout << "PREP_FORMATTER=" << double(tpf) / n / l << "ns/symbol, ";
            std::cout << "Latency is " << tf / ts << ", " << tif / ts << " and " << tpf / ts << " times" << std::endl;
            return (int8_t) ((rs == rf) && (rf == rif) && (rif == rpf) ? 1 : 0);
        }

        char seq[]{0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09};
        auto seqLen = (int32_t) sizeof(seq);
        const int32_t functionalLen = 19;
        std::string functionalBytes[][functionalLen] = {
                {std::string(1, '\2'), "NVM",
                        makeString(USS("bigpack")),
                        makeString(USS("block1")),
                        makeString(USS("additive")),
                        makeString(USS("parameter1")),
                        catStrings(seq, seqLen),
                        makeString(USS("")),
                        makeString(USS("item1")),
                        makeString(USS("parameter1")),
                        catStrings(seq, seqLen),
                        makeString(USS("parameter2")),
                        std::string(1, '\0'), "0",
                        makeString(USS("parameter3")),
                        std::string(1, '\3'), makeString(USS("stringvalue3")),
                        makeString(USS("")),
                        makeString(USS(""))},
        };
        auto functionalJson = USS(
                "{\"packageName\": \"bigjsonpack\", \"blocks\": [{\"blockName\": \"block1\", \"type\": \"additive\", \"parameters\": {\"parameter1\": {\"type\": 1, \"value\": 650777868590383874}}, \"items\": [{\"itemName\": \"item1\", \"parameters\": {\"parameter1\": {\"type\": 1, \"value\": 650777868590383874}, \"parameter2\": {\"type\": 0, \"value\": 48}, \"parameter3\": {\"type\": 3, \"value\": \"stringvalue3\"}}}]}]}");
        auto functionalJsonPretty = USS("{\
    \"packageName\": \"bigjsonpack\",\
    \"blocks\": [\
        {\
            \"blockName\": \"block1\",\
            \"type\": \"additive\",\
            \"parameters\": {\
                \"parameter1\": {\
                    \"type\": 1,\
                    \"value\": 650777868590383874\
                },\
                \"parameter4\": {\
                    \"type\": 1,\
                    \"value\": 2259522249032450\
                }\
            },\
            \"items\": [\
                {\
                    \"itemName\": \"item1\",\
                    \"parameters\": {\
                        \"parameter1\": {\
                            \"type\": 1,\
                            \"value\": 650777868590383874\
                        },\
                        \"parameter2\": {\
                            \"type\": 0,\
                            \"value\": 48\
                        },\
                        \"parameter3\": {\
                            \"type\": 3,\
                            \"value\": \"stringvalue3\"\
                        }\
                    }\
                }\
            ]\
        }\
    ]\
}");

        int8_t byteCodeParserHeadedTest() {
            auto ss = std::make_shared<std::istringstream>();
            const int8_t n = 2;
            std::string tests[n] = {std::string(1, '\2'), "NVM"};
            ss->str(catStrings(tests, n));
            nnvm::ByteCodeParser parser(ss, USS(""), TEST_PATH,
                                        IntegrationTests::prepareContext(ProcessorParallelMode::NONE));
            auto res = parser.read();
            return (int8_t) (res ? 0 : 1);
        }

        int8_t byteCodeParserNonHeadedTest() {
            auto ss = std::make_shared<std::istringstream>();
            const int8_t n = 2;
            std::string tests[n] = {std::string(1, '\2'), makeString(USS("N"))};
            ss->str(catStrings(tests, n));
            nnvm::ByteCodeParser parser(ss, USS(""), TEST_PATH, IntegrationTests::prepareContext());
            try {
                parser.read();
                return 0;
            }
            catch (ByteCodeParserException &e) {
                return 1;
            }
        }

        int8_t byteCodeParserPackagedTest() {
            int8_t ok = 1;
            const int8_t n = 3;
            std::string tests[][n] = {
                    {std::string(1, '\2'), "NVM", makeString(USS("onlypack"))},
                    {std::string(1, '\2'), "NVM", makeString(USS("onlypack123"))},
                    {std::string(1, '\2'), "NVM", makeString(USS("onlypack.with.path"))}
            };
            icu::UnicodeString results[] = {
                    USS("onlypack"),
                    USS("onlypack123"),
                    USS("onlypack.with.path")
            };
            for (auto i = 0; i < sizeof(tests) / sizeof(std::string) / n; i++) {
                auto ss = std::make_shared<std::istringstream>();
                ss->str(catStrings(tests[i], n));
                nnvm::ByteCodeParser parser(ss, USS(""), StringConvUtils::u8tou(tests[i][1]),
                                            IntegrationTests::prepareContext());
                try {
                    auto res = parser.read();
                    ok &= res->front()->getName() == results[i] ? 1 : 0;
                }
                catch (ByteCodeParserException &e) {
                    ok &= 0;
                    std::cout << "Failed on " << tests[i][0] << ": " << e.what() << std::endl;
                }
            }
            return ok;
        }

        int8_t byteCodeParserWrongPackagedTest() {
            int8_t ok = 1;
            const int8_t n = 3;
            std::string tests[][n] = {
                    {std::string(1, '\2'), "NVM", makeString(USS("errorpa,ck"))},
                    {std::string(1, '\2'), "NVM", makeString(USS("errorpa/ck"))},
                    {std::string(1, '\2'), "NVM", makeString(USS("errorpa!ck"))},
                    {std::string(1, '\2'), "NVM", makeString(USS("errorpa#ck"))},
                    {std::string(1, '\2'), "NVM", makeString(USS("errorpa$ck"))},
                    {std::string(1, '\2'), "NVM", makeString(USS("errorpa%ck"))},
                    {std::string(1, '\2'), "NVM", makeString(USS("errorpa(ck"))},
                    {std::string(1, '\2'), "NVM", makeString(USS("errorpa)ck"))},
                    {std::string(1, '\2'), "NVM", makeString(USS("errorpa\"ck"))},
                    {std::string(1, '\2'), "NVM", makeString(USS("errorpa'ck"))}
            };
            auto m = sizeof(tests) / sizeof(std::string) / n;
            for (auto i = 0; i < m; i++) {
                auto ss = std::make_shared<std::istringstream>();
                ss->str(catStrings(tests[i], n));
                nnvm::ByteCodeParser parser(ss, USS(""), StringConvUtils::u8tou(tests[i][1]),
                                            IntegrationTests::prepareContext());
                try {
                    parser.read();
                    ok &= 0;
                    std::cout << "Failed on " << tests[i][0] << std::endl;
                }
                catch (ByteCodeParserException &e) {
                }
            }
            return ok;
        }

        int8_t byteCodeParserBlockedTest() {
            int8_t ok = 1;
            const int8_t n = 5;
            std::string tests[][n] = {
                    {std::string(1, '\2'), "NVM", makeString(USS("packwithblock")),
                            makeString(USS("block")),
                            makeString(USS("additive"))},
                    {std::string(1, '\2'), "NVM", makeString(USS("packwithblock")),
                            makeString(USS("bl.ock")),
                            makeString(USS("additive"))},
                    {std::string(1, '\2'), "NVM", makeString(USS("packwithblock")),
                            makeString(USS("block[a]")),
                            makeString(USS("additive"))},
                    {std::string(1, '\2'), "NVM", makeString(USS("packwithblock")),
                            makeString(USS("bl,ock")),
                            makeString(USS("additive"))},
                    {std::string(1, '\2'), "NVM", makeString(USS("packwithblock")),
                            makeString(USS("blo#ck")),
                            makeString(USS("additive"))},
                    {std::string(1, '\2'), "NVM", makeString(USS("packwithblock")),
                            makeString(USS("blo1ck")),
                            makeString(USS("additive"))},
            };
            icu::UnicodeString results[] = {
                    USS("block"),
                    USS("bl.ock"),
                    USS("block[a]"),
                    USS("bl,ock"),
                    USS("blo#ck"),
                    USS("blo1ck")
            };
            auto m = sizeof(tests) / sizeof(std::string) / n;
            for (auto i = 0; i < m; i++) {
                auto ss = std::make_shared<std::istringstream>();
                ss->str(catStrings(tests[i], n));
                nnvm::ByteCodeParser parser(ss, USS(""), StringConvUtils::u8tou(tests[i][1]),
                                            IntegrationTests::prepareContext());
                try {
                    auto res = parser.read();
                    auto r = res->front()->getBlocks();
                    if (std::get<0>(r) != std::get<1>(r)) {
                        ok &= (*res->front()->getBlock(0))->getName() == results[i] ? 1 : 0;
                        ok &= (*res->front()->getBlock(0))->getType() == USS("additive") ? 1 : 0;
                    }
                    else {
                        ok &= 0;
                    }
                }
                catch (ByteCodeParserException &e) {
                    std::cout << "Failed on " << tests[i][0] << ": " << e.what() << std::endl;
                }
            }
            return ok;
        }

        int8_t byteCodeParserWrongBlockedTest() {
            int8_t ok = 1;
            const int8_t n = 5;
            std::string tests[][n] = {
                    {std::string(1, '\2'), "NVM", makeString(USS("packwithwrongblock")),
                            makeString(USS("bl(ock")),
                            makeString(USS("additive"))},
                    {std::string(1, '\2'), "NVM", makeString(USS("packwithwrongblock")),
                            makeString(USS("bl/ock")),
                            makeString(USS("additive"))},
                    {std::string(1, '\2'), "NVM", makeString(USS("packwithwrongblock")),
                            makeString(USS("bl!ock")),
                            makeString(USS("additive"))},
            };
            auto m = sizeof(tests) / sizeof(std::string) / n;
            for (auto i = 0; i < m; i++) {
                auto ss = std::make_shared<std::istringstream>();
                ss->str(catStrings(tests[i], n));
                nnvm::ByteCodeParser parser(ss, USS(""), StringConvUtils::u8tou(tests[i][1]),
                                            IntegrationTests::prepareContext());
                try {
                    parser.read();
                    ok &= 0;
                    std::cout << "Failed on " << tests[i][0] << std::endl;
                }
                catch (ByteCodeParserException &e) {
                }
            }
            return ok;
        }

        int8_t byteCodeParserBlockWithParameterTest() {
            const int8_t n = 10;
            std::string tests[][n] = {
                    {std::string(1, '\2'), "NVM", makeString(USS("packwithblock")),
                            makeString(USS("block")),
                            makeString(USS("additive")),
                            makeString(USS("parameter1")),
                            std::string(1, '\0'), std::string(1, '\0'),
                            makeString(USS("")),
                            makeString(USS(""))},
            };
            auto ss = std::make_shared<std::istringstream>();
            ss->str(catStrings(tests[0], n));
            nnvm::ByteCodeParser parser(ss, USS(""), StringConvUtils::u8tou(tests[0][1]),
                                        IntegrationTests::prepareContext());
            try {
                auto res = parser.read();
                auto r = res->front()->getBlocks();
                if (std::get<0>(r) != std::get<1>(r)) {
                    if ((*res->front()->getBlock(0))->getParameter(USS("parameter1"))->value.byteValue == 0) {
                        return 1;
                    }
                }
            }
            catch (ByteCodeParserException &e) {
                std::cout << "Failed on " << tests[0][0] << ": " << e.what() << std::endl;
            }
            return 0;
        }

        int8_t byteCodeParserFunctionalTest() {
            auto ss = std::make_shared<std::istringstream>();
            ss->str(catStrings(functionalBytes[0], functionalLen));
            nnvm::ByteCodeParser parser(ss, USS(""), StringConvUtils::u8tou(functionalBytes[0][2]),
                                        IntegrationTests::prepareContext());
            try {
                auto res = parser.read();
                if (res->front()->getName() != USS("bigpack")) {
                    return 0;
                }
                auto r = res->front()->getBlocks();
                if (std::get<0>(r) == std::get<1>(r)) {
                    return 0;
                }
                auto b = *res->front()->getBlock(0);
                if (b->getName() != USS("block1")) {
                    return 0;
                }
                if (b->getType() != USS("additive")) {
                    return 0;
                }
                auto p = b->getParameter(USS("parameter1"));
                if (!p || p->getName() != USS("parameter1") || p->getType() != ByteCodeParameterTypes::INTEGER ||
                    p->value.integerValue != 0x0908070605040302) {
                    return 0;
                }
                auto r2 = b->getItems();
                if (std::get<0>(r2) == std::get<1>(r2)) {
                    return 0;
                }
                auto bi = *b->getItem(0);
                p = bi->getParameter(USS("parameter2"));
                if (!p || p->getName() != USS("parameter2") || p->getType() != ByteCodeParameterTypes::BYTE ||
                    p->value.byteValue != 0x30) {
                    return 0;
                }
            }
            catch (ByteCodeParserException &e) {
                std::cout << "Failed on " << functionalBytes[0][0] << ": " << e.what() << std::endl;
                return 0;
            }
            return 1;
        }

        int8_t byteCodeParserJsonFunctionalTest() {
            auto ss = std::make_shared<std::istringstream>();
            ss->str(StringConvUtils::utou8(functionalJsonPretty));
            nnvm::ByteCodeParser parser(ss, USS(""), USS("bigjsonpack"), IntegrationTests::prepareContext());
            try {
                auto res = parser.read();
                if (res->front()->getName() != USS("bigjsonpack")) {
                    return 0;
                }
                auto r = res->front()->getBlocks();
                if (std::get<0>(r) == std::get<1>(r)) {
                    return 0;
                }
                auto b = *res->front()->getBlock(0);
                if (b->getName() != USS("block1")) {
                    return 0;
                }
                if (b->getType() != USS("additive")) {
                    return 0;
                }
                auto p = b->getParameter(USS("parameter1"));
                if (!p || p->getName() != USS("parameter1") || p->getType() != ByteCodeParameterTypes::INTEGER ||
                    p->value.integerValue == 0x0908070605040302) {
                    return 0;
                }
                p = b->getParameter(USS("parameter4"));
                if (!p || p->getName() != USS("parameter4") || p->getType() != ByteCodeParameterTypes::INTEGER ||
                    p->value.integerValue != 0x08070605040302) {
                    return 0;
                }
                auto r2 = b->getItems();
                if (std::get<0>(r2) == std::get<1>(r2)) {
                    return 0;
                }
                auto bi = *b->getItem(0);
                p = bi->getParameter(USS("parameter2"));
                if (!p || p->getName() != USS("parameter2") || p->getType() != ByteCodeParameterTypes::BYTE ||
                    p->value.byteValue != 0x30) {
                    return 0;
                }
            }
            catch (ByteCodeParserException &e) {
                std::cout << "Failed on " << StringConvUtils::utou8(functionalJsonPretty.tempSubString(18, 11)) <<
                    ": " << e.what() << std::endl;
                return 0;
            }
            return 1;
        }

        int8_t byteCodeParserJsonEscapeTest() {
            std::string j = R"({"packageName": "str\"ing\bthen\tand\r\nexperiment\rwith\n", "blocks": []})";
            auto ss = std::make_shared<std::istringstream>();
            ss->str(j);
            nnvm::ByteCodeParser parser(ss, USS(""), USS("str\"ing\bthen\tand\r\nexperiment\rwith\n"),
                                        IntegrationTests::prepareContext());
            try {
                auto res = parser.read();
                if (res->front()->getName() != USS("str\"ing\bthen\tand\r\nexperiment\rwith\n")) {
                    return 0;
                }
            }
            catch (ByteCodeParserException &e) {
                std::cout << "Failed on json escaped: " << e.what() << std::endl;
                return 0;
            }
            return 1;
        }

        int8_t byteCodeParserJsonUnicodeTest() {
            std::string j = R"({"packageName": "string\u0001 ab \u0144", "blocks": []})";
            auto ss = std::make_shared<std::istringstream>();
            ss->str(j);
            nnvm::ByteCodeParser parser(ss, USS(""), USS("string\1 ab \u0144"), IntegrationTests::prepareContext());
            try {
                auto res = parser.read();
                if (res->front()->getName() != USS("string\1 ab \u0144")) {
                    return 0;
                }
            }
            catch (ByteCodeParserException &e) {
                std::cout << "Failed on json with unicode: " << e.what() << std::endl;
                return 0;
            }
            return 1;
        }

        int8_t byteCodeParserJsonWrongUnicodeTest() {
            std::string j = R"({"packageName": "string\uD800", "blocks": []})";
            auto ss = std::make_shared<std::istringstream>();
            ss->str(j);
            nnvm::ByteCodeParser parser(ss, USS(""), USS("string"), IntegrationTests::prepareContext());
            try {
                parser.read();
            }
            catch (ByteCodeParserException &e) {
                return 1;
            }
            return 0;
        }

        int8_t byteCodeParserJsonBOMTest() {
            std::string j = "\xEF\xBB\xBF{\"packageName\": \"string\", \"blocks\": []}";
            auto ss = std::make_shared<std::istringstream>();
            ss->str(j);
            nnvm::ByteCodeParser parser(ss, USS(""), USS("string"), IntegrationTests::prepareContext());
            try {
                auto res = parser.read();
                if (res->front()->getName() != USS("string")) {
                    return 0;
                }
            }
            catch (ByteCodeParserException &e) {
                std::cout << "Failed on json with bom: " << e.what() << std::endl;
                return 0;
            }
            return 1;
        }

        int8_t byteCodeParserWriterJsonUnicodeTest() {
            icu::UnicodeString j = USS(R"({"packageName": "string\u0001 ab \u0144", "blocks": []})");
            auto ss = std::make_shared<std::istringstream>();
            ss->str(StringConvUtils::utou8(j));
            nnvm::ByteCodeParser parser(ss, USS(""), USS("string\u0001 ab \u0144"), IntegrationTests::prepareContext());
            try {
                auto res = parser.read();
                auto oss = std::make_shared<std::ostringstream>();
                ByteCodeWriter bw(oss, IntegrationTests::prepareContext());
                if (bw.writeJson(res->front())) {
                    return 0;
                }
                auto a = std::string(oss->str());
                auto e = StringConvUtils::utou8(j);
                return a == e;
            }
            catch (ByteCodeParserException &e) {
                std::cout << "Failed on json with unicode: " << e.what() << std::endl;
                return 0;
            }
        }

        int8_t byteCodeWriterFunctionalTest() {
            auto ctx = IntegrationTests::prepareContext();
            auto c = std::make_shared<ByteCode>(USS("bigpack"), ctx->byteLoader);
            auto b = std::make_shared<ByteCodeBlock>(USS("block1"), USS("additive"));
            c->addBlock(b);
            auto bi = std::make_shared<ByteCodeBlockItem>(USS("item1"), b);
            b->addItem(bi);
            auto bp = std::make_shared<ByteCodeParameter>(USS("parameter1"), (int64_t) 0x0908070605040302);
            b->setParameter(bp);
            auto bip1 = std::make_shared<ByteCodeParameter>(USS("parameter1"), (int64_t) 0x0908070605040302);
            bi->setParameter(bip1);
            auto bip2 = std::make_shared<ByteCodeParameter>(USS("parameter2"), (int8_t) 0x30);
            bi->setParameter(bip2);
            auto bip3 = std::make_shared<ByteCodeParameter>(USS("parameter3"), USS("stringvalue3"));
            bi->setParameter(bip3);
            auto ss = std::make_shared<std::ostringstream>();
            nnvm::ByteCodeWriter bw(ss, ctx);
            if (bw.write(c)) {
                return 0;
            }
            auto actual = ss->str();
            auto expected = catStrings(functionalBytes[0], functionalLen);
            return actual == expected;
        }

        int8_t byteCodeWriterJsonFunctionalTest() {
            auto ctx = IntegrationTests::prepareContext();
            auto c = std::make_shared<ByteCode>(USS("bigjsonpack"), ctx->byteLoader);
            auto b = std::make_shared<ByteCodeBlock>(USS("block1"), USS("additive"));
            c->addBlock(b);
            auto bi = std::make_shared<ByteCodeBlockItem>(USS("item1"), b);
            b->addItem(bi);
            auto bp = std::make_shared<ByteCodeParameter>(USS("parameter1"), (int64_t) 0x0908070605040302);
            b->setParameter(bp);
            auto bip1 = std::make_shared<ByteCodeParameter>(USS("parameter1"), (int64_t) 0x0908070605040302);
            bi->setParameter(bip1);
            auto bip2 = std::make_shared<ByteCodeParameter>(USS("parameter2"), (int8_t) 0x30);
            bi->setParameter(bip2);
            auto bip3 = std::make_shared<ByteCodeParameter>(USS("parameter3"), USS("stringvalue3"));
            bi->setParameter(bip3);
            auto ss = std::make_shared<std::ostringstream>();
            nnvm::ByteCodeWriter bw(ss, ctx);
            if (bw.writeJson(c)) {
                return 0;
            }
            auto s = ss->str();
            return StringConvUtils::u8tou(s) == functionalJson;
        }

        int8_t byteCodeLoaderFunctionalTest() {
            auto ctx = IntegrationTests::prepareContext();
            auto loader = ctx->byteLoader;
            try {
                auto res = loader->load(USS("Test"), ctx);

                fast_assert_less((bool) res, "No res");
                fast_assert(res->getName() == USS("Test"), "res not Test but ", StringConvUtils::utou8(res->getName()));
                auto r = res->getBlocks();
                fast_assert_less(std::get<0>(r) != std::get<1>(r), "No blocks");
                auto b = *res->getBlock(0);
                fast_assert(b->getName() == USS("block1"), "res[0] not block1 but ",
                            StringConvUtils::utou8(b->getName()));
                fast_assert(b->getType() == USS("additive"), "res[0] type not additive but ",
                            StringConvUtils::utou8(b->getType()));
                auto p = b->getParameter(USS("parameter1"));
                fast_assert_less(p && p->getName() == USS("parameter1"), "res[0] has no parameter1");
                fast_assert(
                        p->getType() == ByteCodeParameterTypes::INTEGER && p->value.integerValue == 0x0908070605040302,
                        "res[0] parameter1 is not integer 0x0908070605040302 but ", p->value.integerValue);
                auto r2 = b->getItems();
                fast_assert_less(std::get<0>(r2) != std::get<1>(r2), "res[0] has no items");
                auto bi = *b->getItem(0);
                p = bi->getParameter(USS("parameter2"));
                fast_assert_less(p && p->getName() == USS("parameter2"), "res[0] item[0] has no parameter2");
                fast_assert(p->getType() == ByteCodeParameterTypes::BYTE && p->value.byteValue == 0x30,
                            "res[0] item[0] parameter2 is not byte 0x30 but ", p->value.byteValue);
            }
            catch (ByteCodeParserException &e) {
                std::cout << "Failed on " << functionalBytes[0][1] << std::endl;
                return 0;
            }
            return 1;
        }

        int8_t byteCodeLoaderJSONFunctionalTest() {
            auto ctx = IntegrationTests::prepareContext();
            auto loader = ctx->byteLoader;
            try {
                auto res = loader->load(USS("TestNNJ"), ctx);

                fast_assert_less((bool) res, "No res");
                fast_assert(res->getName() == USS("TestNNJ"), "res not TestNNJ but ",
                            StringConvUtils::utou8(res->getName()));
                auto r = res->getBlocks();
                fast_assert_less(std::get<0>(r) != std::get<1>(r), "No blocks");
                auto b = *res->getBlock(0);
                fast_assert(b->getName() == USS("block1"), "res[0] not block1 but ",
                            StringConvUtils::utou8(b->getName()));
                fast_assert(b->getType() == USS("additive"), "res[0] type not additive but ",
                            StringConvUtils::utou8(b->getType()));
                auto p = b->getParameter(USS("input"));
                fast_assert_less(p && p->getName() == USS("input"), "res[0] has no input");
                fast_assert(p->getType() == ByteCodeParameterTypes::BYTE && p->value.byteValue == 1,
                            "res[0] input is not byte 1 but ", p->value.byteValue);
                auto r2 = b->getItems();
                fast_assert_less(std::get<0>(r2) != std::get<1>(r2), "res[0] has no items");
                auto bi = *b->getItem(0);
                fast_assert(bi->getName() == USS("block2"), "res[0] item[0] not block2 but ",
                            StringConvUtils::utou8(bi->getName()));
                p = bi->getParameter(USS("activation"));
                fast_assert_less(p && p->getName() == USS("activation"), "res[0] item[0] has no activation");
                fast_assert(p->getType() == ByteCodeParameterTypes::BYTE && p->value.byteValue == 1,
                            "res[0] item[0] activation is not byte 1 but ", p->value.byteValue);
            }
            catch (ByteCodeParserException &e) {
                std::cout << "Failed on " << functionalBytes[0][1] << std::endl;
                return 0;
            }
            return 1;
        }

        int8_t byteCodeLoaderSourceFunctionalTest() {
            auto ctx = IntegrationTests::prepareContext();
            auto loader = ctx->byteLoader;
            try {
                auto res = loader->load(USS("TestNN.network"), ctx);

                fast_assert_less((bool) res, "No res");
                fast_assert(res->getName() == USS("TestNN.network"), "res not TestNN.network but ",
                            StringConvUtils::utou8(res->getName()));
                auto r = res->getBlocks();
                fast_assert_less(std::get<0>(r) != std::get<1>(r), "No blocks");
                auto b = *res->getBlock(0);
                fast_assert(b->getName() == USS("block1"), "res[0] not block1 but ",
                            StringConvUtils::utou8(b->getName()));
                fast_assert(b->getType() == USS("additive"), "res[0] type not additive but ",
                            StringConvUtils::utou8(b->getType()));
                auto p = b->getParameter(ElementParams::INPUT);
                fast_assert_less(p && p->getName() == ElementParams::INPUT,
                                 "res[0] has no " + StringConvUtils::utou8(ElementParams::INPUT));
                fast_assert(p->getType() == ByteCodeParameterTypes::BYTE && p->value.byteValue == 1,
                            "res[0] " + StringConvUtils::utou8(ElementParams::INPUT) + " is not byte 1 but ",
                            p->value.byteValue);
                auto r2 = b->getItems();
                fast_assert_less(std::get<0>(r2) != std::get<1>(r2), "res[0] has no items");
                auto bi = *b->getItem(0);
                fast_assert(bi->getName() == USS("block2"), "res[0] item[0] not block2 but ",
                            StringConvUtils::utou8(bi->getName()));
                p = bi->getParameter(LinkParams::ACTIVATION);
                fast_assert_less(p && p->getName() == LinkParams::ACTIVATION,
                                 "res[0] item[0] has no " + StringConvUtils::utou8(LinkParams::ACTIVATION));
                fast_assert(p->getType() == ByteCodeParameterTypes::DOUBLE && p->value.doubleValue == 30.0,
                            "res[0] item[0] " + StringConvUtils::utou8(LinkParams::ACTIVATION) +
                            " is not double 30 but ", p->value.doubleValue);
            }
            catch (ByteCodeParserException &e) {
                std::cout << "Failed on " << functionalBytes[0][1] << std::endl;
                return 0;
            }
            return 1;
        }

        int8_t processorSimpleTest() {
            auto ctx = IntegrationTests::prepareContext();
            auto c = std::make_shared<ByteCode>(USS("TestNNSimple"), ctx->byteLoader);
            auto b = std::make_shared<ByteCodeBlock>(USS("A"), ADDITIVE);
            c->addBlock(b);
            auto bi = std::make_shared<ByteCodeBlockItem>(USS("B"), b);
            b->addItem(bi);
            auto bip1 = std::make_shared<ByteCodeParameter>(LinkParams::ACTIVATION, (int8_t) 1);
            bi->setParameter(bip1);
            auto bip2 = std::make_shared<ByteCodeParameter>(LinkParams::STREAM, (int8_t) 2);
            bi->setParameter(bip2);
            auto b2 = std::make_shared<ByteCodeBlock>(USS("B"), ADDITIVE);
            c->addBlock(b2);
            ByteCodeUtils::finalizeCode(c);
            ctx->useByteCode(c);
            Processor p(ctx->setup, ctx);
            p.processStep();
            std::shared_ptr<ByteCodeParameter> param;
            std::shared_ptr<ByteCodeParameter> resparam;
            if (!b2->tryGetParameter(ElementParams::SUM, resparam) || fabs(resparam->value.doubleValue) > EPSILON) {
                shoutError(ElementParams::SUM, resparam, 0.0, b2, 1);
                return 0;
            }
            if (!b->tryGetParameter(ElementParams::SUM, param)) {
                shoutError(ElementParams::SUM, param, 0.0, b, 1);
                return 0;
            }
            param->value.doubleValue = 10;
            p.processStep();
            if (!b2->tryGetParameter(ElementParams::SUM, resparam) ||
                fabs(resparam->value.doubleValue - 20.) > EPSILON) {
                shoutError(ElementParams::SUM, resparam, 20.0, b2, 2);
                return 0;
            }

            return 1;
        }

        int8_t processorIfTest() {
            auto ctx = IntegrationTests::prepareContext();
            auto c = std::make_shared<ByteCode>(USS("TestNNIf"), ctx->byteLoader);
            // A(0,0) -(1,0,0)-> C
            auto b = std::make_shared<ByteCodeBlock>(USS("A"), ADDITIVE);
            c->addBlock(b);
            auto bi = std::make_shared<ByteCodeBlockItem>(USS("C"), b);
            b->addItem(bi);
            auto bip = std::make_shared<ByteCodeParameter>(LinkParams::ACTIVATION, (int8_t) 1);
            bi->setParameter(bip);
            // B(0,0) -(0,1,0)-> C
            auto b2 = std::make_shared<ByteCodeBlock>(USS("B"), ADDITIVE);
            c->addBlock(b2);
            auto bi2 = std::make_shared<ByteCodeBlockItem>(USS("C"), b2);
            b2->addItem(bi2);
            auto bi2p = std::make_shared<ByteCodeParameter>(LinkParams::STREAM, (int8_t) 1);
            bi2->setParameter(bi2p);
            // C(2,0) ->
            auto b3 = std::make_shared<ByteCodeBlock>(USS("C"), ADDITIVE);
            c->addBlock(b3);
            auto b3p = std::make_shared<ByteCodeParameter>(ElementParams::THRESHOLD, (int8_t) 2);
            b3->setParameter(b3p);
            ByteCodeUtils::finalizeCode(c);
            ctx->useByteCode(c);
            Processor p(ctx->setup, ctx);

            // default (0)
            p.processStep();
            std::shared_ptr<ByteCodeParameter> param;
            std::shared_ptr<ByteCodeParameter> resparam;
            if (!b3->tryGetParameter(ElementParams::SUM, resparam) || fabs(resparam->value.doubleValue) > EPSILON) {
                shoutError(ElementParams::SUM, resparam, 0.0, b3, 1);
                return 0;
            }

            // < 2
            if (!b->tryGetParameter(ElementParams::SUM, param)) {
                shoutError(ElementParams::SUM, param, 0.0, b, 1);
                return 0;
            }
            param->value.doubleValue = 1;
            if (!b2->tryGetParameter(ElementParams::SUM, param)) {
                shoutError(ElementParams::SUM, param, 0.0, b2, 1);
                return 0;
            }
            param->value.doubleValue = 20;
            p.processStep();
            if (!b3->tryGetParameter(ElementParams::SUM, resparam) || fabs(resparam->value.doubleValue) > EPSILON) {
                shoutError(ElementParams::SUM, resparam, 0.0, b3, 2);
                return 0;
            }

            // >= 2
            if (!b->tryGetParameter(ElementParams::SUM, param)) {
                shoutError(ElementParams::SUM, param, 0.0, b, 2);
                return 0;
            }
            param->value.doubleValue = 2;
            if (!b2->tryGetParameter(ElementParams::SUM, param)) {
                shoutError(ElementParams::SUM, param, 0.0, b2, 2);
                return 0;
            }
            param->value.doubleValue = 30;
            p.processStep();
            if (!b3->tryGetParameter(ElementParams::SUM, resparam) ||
                fabs(resparam->value.doubleValue - 30.) > EPSILON) {
                shoutError(ElementParams::SUM, resparam, 30.0, b3, 3);
                return 0;
            }

            return 1;
        }

        int8_t processorSqrTest() {
            auto ctx = IntegrationTests::prepareContext();
            auto c = std::make_shared<ByteCode>(USS("TestNNSqr"), ctx->byteLoader);
            // A(0,0) -(1,1,0)-> C
            // A(0,0) -(0,-1,0)-> B
            auto b = std::make_shared<ByteCodeBlock>(USS("A"), ADDITIVE);
            c->addBlock(b);
            auto bi = std::make_shared<ByteCodeBlockItem>(USS("C"), b);
            b->addItem(bi);
            auto bip = std::make_shared<ByteCodeParameter>(LinkParams::ACTIVATION, (int8_t) 1);
            bi->setParameter(bip);
            auto bip2 = std::make_shared<ByteCodeParameter>(LinkParams::STREAM, (int8_t) 1);
            bi->setParameter(bip2);
            auto bi2 = std::make_shared<ByteCodeBlockItem>(USS("B"), b);
            b->addItem(bi2);
            auto bi2p = std::make_shared<ByteCodeParameter>(LinkParams::STREAM, (int8_t) -1);
            bi2->setParameter(bi2p);
            // B(0,0) -(1,0,0)-> C
            auto b2 = std::make_shared<ByteCodeBlock>(USS("B"), ADDITIVE);
            c->addBlock(b2);
            auto b2i = std::make_shared<ByteCodeBlockItem>(USS("C"), b2);
            b2->addItem(b2i);
            auto b2ip = std::make_shared<ByteCodeParameter>(LinkParams::ACTIVATION, (int8_t) 1);
            b2i->setParameter(b2ip);
            // C(0,0) ->
            auto b3 = std::make_shared<ByteCodeBlock>(USS("C"), MULTIPLICATIVE);
            c->addBlock(b3);
            ByteCodeUtils::finalizeCode(c);
            ctx->useByteCode(c);
            Processor p(ctx->setup, ctx);

            // default (0)
            p.processStep();
            std::shared_ptr<ByteCodeParameter> param;
            std::shared_ptr<ByteCodeParameter> resparam;
            if (!b3->tryGetParameter(ElementParams::SUM, resparam) || fabs(resparam->value.doubleValue) > EPSILON) {
                shoutError(ElementParams::SUM, resparam, 0.0, b3, 1);
                return 0;
            }

            // 2^2
            if (!b->tryGetParameter(ElementParams::SUM, param)) {
                shoutError(ElementParams::SUM, param, 0.0, b, 1);
                return 0;
            }
            param->value.doubleValue = 2;
            p.processStep();
            if (!b3->tryGetParameter(ElementParams::SUM, resparam) ||
                fabs(resparam->value.doubleValue - 4.) > EPSILON) {
                shoutError(ElementParams::SUM, resparam, 4.0, b3, 2);
                return 0;
            }

            // 3^2
            if (!b->tryGetParameter(ElementParams::SUM, param)) {
                shoutError(ElementParams::SUM, param, 0.0, b, 2);
                return 0;
            }
            param->value.doubleValue = 3;
            p.processStep();
            if (!b3->tryGetParameter(ElementParams::SUM, resparam) ||
                fabs(resparam->value.doubleValue - 9.) > EPSILON) {
                shoutError(ElementParams::SUM, resparam, 9.0, b3, 3);
                return 0;
            }

            // 10^2
            if (!b->tryGetParameter(ElementParams::SUM, param)) {
                shoutError(ElementParams::SUM, param, 0.0, b, 3);
                return 0;
            }
            param->value.doubleValue = 10;
            p.processStep();
            if (!b3->tryGetParameter(ElementParams::SUM, resparam) ||
                fabs(resparam->value.doubleValue - 100.) > EPSILON) {
                shoutError(ElementParams::SUM, resparam, 100.0, b3, 4);
                return 0;
            }

            // 4^2
            if (!b->tryGetParameter(ElementParams::SUM, param)) {
                shoutError(ElementParams::SUM, param, 0.0, b, 4);
                return 0;
            }
            param->value.doubleValue = 4;
            p.processStep();
            if (!b3->tryGetParameter(ElementParams::SUM, resparam) ||
                fabs(resparam->value.doubleValue - 16.) > EPSILON) {
                shoutError(ElementParams::SUM, resparam, 16.0, b3, 5);
                return 0;
            }
            return 1;
        }

        int8_t processorTemplateTest() {
            auto ctx = IntegrationTests::prepareContext();
            auto c = std::make_shared<ByteCode>(USS("TestNNTemplate"), ctx->byteLoader);
            // A(0,0) -(0,0,1)-> B[A]
            auto b = std::make_shared<ByteCodeBlock>(USS("A"), ADDITIVE);
            c->addBlock(b);
            auto bi = std::make_shared<ByteCodeBlockItem>(USS("B[A]"), b);
            b->addItem(bi);
            auto bip = std::make_shared<ByteCodeParameter>(LinkParams::GENERATION, (int8_t) 1);
            bi->setParameter(bip);
            // B[A](0,A) -(0,1,0)-> C
            auto b2 = std::make_shared<ByteCodeBlock>(USS("B[A]"), ADDITIVE);
            c->addBlock(b2);
            auto b2p = std::make_shared<ByteCodeParameter>(ElementParams::BIAS, USS("A"));
            b2->setParameter(b2p);
            auto b2p2 = std::make_shared<ByteCodeParameter>(ElementParams::TEMPLATE, (int8_t) 1);
            b2->setParameter(b2p2);
            auto bi2 = std::make_shared<ByteCodeBlockItem>(USS("C"), b2);
            b2->addItem(bi2);
            auto bi2p = std::make_shared<ByteCodeParameter>(LinkParams::STREAM, (int8_t) 1);
            bi2->setParameter(bi2p);
            // C
            auto b3 = std::make_shared<ByteCodeBlock>(USS("C"), ADDITIVE);
            c->addBlock(b3);
            ByteCodeUtils::finalizeCode(c);
            ctx->useByteCode(c);
            Processor p(ctx->setup, ctx);

            // default (0)
            p.processStep();
            std::shared_ptr<ByteCodeParameter> param;
            std::shared_ptr<ByteCodeParameter> resparam;
            if (!b3->tryGetParameter(ElementParams::SUM, resparam) || fabs(resparam->value.doubleValue) > EPSILON) {
                shoutError(ElementParams::SUM, resparam, 0.0, b3, 1);
                return 0;
            }

            // +1
            if (!b->tryGetParameter(ElementParams::SUM, param)) {
                shoutError(ElementParams::SUM, param, 0.0, b, 1);
                return 0;
            }
            param->value.doubleValue = 1;
            p.processStep();
            if (!b3->tryGetParameter(ElementParams::SUM, resparam) || fabs(resparam->value.doubleValue) > EPSILON) {
                shoutError(ElementParams::SUM, resparam, 0.0, b3, 2);
                return 0;
            }
            p.processStep();
            if (!b3->tryGetParameter(ElementParams::SUM, resparam) ||
                fabs(resparam->value.doubleValue - 1.) > EPSILON) {
                shoutError(ElementParams::SUM, resparam, 1.0, b3, 3);
                return 0;
            }

            // +2
            if (!b->tryGetParameter(ElementParams::SUM, param)) {
                shoutError(ElementParams::SUM, param, 0.0, b, 3);
                return 0;
            }
            param->value.doubleValue = 2;
            p.processStep();
            if (!b3->tryGetParameter(ElementParams::SUM, resparam) ||
                fabs(resparam->value.doubleValue - 1.) > EPSILON) {
                shoutError(ElementParams::SUM, resparam, 1.0, b3, 4);
                return 0;
            }
            // +4
            if (!b->tryGetParameter(ElementParams::SUM, param)) {
                shoutError(ElementParams::SUM, param, 0.0, b, 4);
                return 0;
            }
            param->value.doubleValue = 4;
            p.processStep();
            if (!b3->tryGetParameter(ElementParams::SUM, resparam) ||
                fabs(resparam->value.doubleValue - 3.) > EPSILON) {
                shoutError(ElementParams::SUM, resparam, 3.0, b3, 5);
                return 0;
            }
            // +0
            p.processStep();
            if (!b3->tryGetParameter(ElementParams::SUM, resparam) ||
                fabs(resparam->value.doubleValue - 7.) > EPSILON) {
                shoutError(ElementParams::SUM, resparam, 7.0, b3, 6);
                return 0;
            }

            return 1;
        }

        int8_t processorMemoryManagementTest() {
            auto ctx = IntegrationTests::prepareContext();
            auto c = std::make_shared<ByteCode>(USS("TestNNMemoryManagement"), ctx->byteLoader);
            // A(0,0) -(0,0,1)-> B[A]
            auto b = std::make_shared<ByteCodeBlock>(USS("A"), ADDITIVE);
            c->addBlock(b);
            auto bi = std::make_shared<ByteCodeBlockItem>(USS("B[A]"), b);
            b->addItem(bi);
            auto bip = std::make_shared<ByteCodeParameter>(LinkParams::GENERATION, (int8_t) 1);
            bi->setParameter(bip);
            // B[A](0,A) -(0,1,0)-> C
            auto b2 = std::make_shared<ByteCodeBlock>(USS("B[A]"), ADDITIVE);
            c->addBlock(b2);
            auto b2p = std::make_shared<ByteCodeParameter>(ElementParams::BIAS, USS("A"));
            b2->setParameter(b2p);
            auto b2p2 = std::make_shared<ByteCodeParameter>(ElementParams::TEMPLATE, (int8_t) 1);
            b2->setParameter(b2p2);
            auto bi2 = std::make_shared<ByteCodeBlockItem>(USS("C"), b2);
            b2->addItem(bi2);
            auto bi2p = std::make_shared<ByteCodeParameter>(LinkParams::STREAM, (int8_t) 1);
            bi2->setParameter(bi2p);
            // C
            auto b3 = std::make_shared<ByteCodeBlock>(USS("C"), ADDITIVE);
            c->addBlock(b3);
            // B[1](0,1) -(0,1,0)-> C
            auto b4 = std::make_shared<ByteCodeBlock>(USS("B[1]"), ADDITIVE);
            c->addBlock(b4);
            auto b4p = std::make_shared<ByteCodeParameter>(ElementParams::BIAS, (int8_t) 1);
            b4->setParameter(b4p);
            auto bi4 = std::make_shared<ByteCodeBlockItem>(USS("C"), b4);
            b4->addItem(bi4);
            auto bi4p = std::make_shared<ByteCodeParameter>(LinkParams::STREAM, (int8_t) 1);
            bi4->setParameter(bi4p);
            ByteCodeUtils::finalizeCode(c);
            ctx->useByteCode(c);
            Processor p(ctx->setup, ctx);

            // 0 => 1
            p.processStep();
            std::shared_ptr<ByteCodeParameter> param;
            std::shared_ptr<ByteCodeParameter> resparam;
            if (!b3->tryGetParameter(ElementParams::SUM, resparam) || fabs(resparam->value.doubleValue) > EPSILON) {
                shoutError(ElementParams::SUM, resparam, 0.0, b3, 1);
                return 0;
            }
            p.processStep();
            if (!b3->tryGetParameter(ElementParams::SUM, resparam) ||
                fabs(resparam->value.doubleValue - 1.) > EPSILON) {
                shoutError(ElementParams::SUM, resparam, 1.0, b3, 2);
                return 0;
            }

            // +1 => 1
            if (!b->tryGetParameter(ElementParams::SUM, param)) {
                shoutError(ElementParams::SUM, param, 0.0, b, 2);
                return 0;
            }
            param->value.doubleValue = 1;
            p.processStep();
            if (!b3->tryGetParameter(ElementParams::SUM, resparam) ||
                fabs(resparam->value.doubleValue - 1.) > EPSILON) {
                shoutError(ElementParams::SUM, resparam, 1.0, b3, 3);
                return 0;
            }
            p.processStep();
            if (!b3->tryGetParameter(ElementParams::SUM, resparam) ||
                fabs(resparam->value.doubleValue - 1.) > EPSILON) {
                shoutError(ElementParams::SUM, resparam, 1.0, b3, 4);
                return 0;
            }

            // -1 => 0
            if (!b->tryGetParameter(ElementParams::SUM, param)) {
                shoutError(ElementParams::SUM, param, 0.0, b, 4);
                return 0;
            }
            param->value.doubleValue = -1;
            p.processStep();
            if (!b3->tryGetParameter(ElementParams::SUM, resparam) ||
                fabs(resparam->value.doubleValue - 1.) > EPSILON) {
                shoutError(ElementParams::SUM, resparam, 1.0, b3, 5);
                return 0;
            }
            p.processStep();
            if (!b3->tryGetParameter(ElementParams::SUM, resparam) || fabs(resparam->value.doubleValue) > EPSILON) {
                shoutError(ElementParams::SUM, resparam, 0.0, b3, 6);
                return 0;
            }

            // +1 => 1
            if (!b->tryGetParameter(ElementParams::SUM, param)) {
                shoutError(ElementParams::SUM, param, 0.0, b, 6);
                return 0;
            }
            param->value.doubleValue = 1;
            p.processStep();
            if (!b3->tryGetParameter(ElementParams::SUM, resparam) || fabs(resparam->value.doubleValue) > EPSILON) {
                shoutError(ElementParams::SUM, resparam, 0.0, b3, 7);
                return 0;
            }
            p.processStep();
            if (!b3->tryGetParameter(ElementParams::SUM, resparam) ||
                fabs(resparam->value.doubleValue - 1.) > EPSILON) {
                shoutError(ElementParams::SUM, resparam, 1.0, b3, 8);
                return 0;
            }

            return 1;
        }

        int8_t processorExitTest() {
            auto ctx = IntegrationTests::prepareContext();
            auto c = std::make_shared<ByteCode>(USS("TestNNExit"), ctx->byteLoader);
            // A(0,0) -(1,1,0)-> E
            auto b = std::make_shared<ByteCodeBlock>(USS("A"), ADDITIVE);
            c->addBlock(b);
            auto bp = std::make_shared<ByteCodeParameter>(ElementParams::BIAS, (int8_t) 1);
            b->setParameter(bp);
            auto bi = std::make_shared<ByteCodeBlockItem>(USS("E"), b);
            b->addItem(bi);
            auto bip = std::make_shared<ByteCodeParameter>(LinkParams::ACTIVATION, (int8_t) 1);
            bi->setParameter(bip);
            auto bip2 = std::make_shared<ByteCodeParameter>(LinkParams::STREAM, (int8_t) 3);
            bi->setParameter(bip2);
            // E
            auto b2 = std::make_shared<ByteCodeBlock>(USS("E"), EXIT);
            c->addBlock(b2);
            ByteCodeUtils::finalizeCode(c);
            ctx->useByteCode(c);
            Processor p(ctx->setup, ctx);
            ctx->setup->getProcessorSetup()->running = true;

            // default (0)
            p.processStep();
            if (!ctx->setup->getProcessorSetup()->running) {
                std::cout << "Process should be running" << std::endl;
                return 0;
            }
            p.processStep();
            if (ctx->setup->getProcessorSetup()->running) {
                std::cout << "Process should not be running" << std::endl;
                return 0;
            }
            if (ctx->setup->getProcessorSetup()->exitCode != 3) {
                std::cout << "Process should return 3 and was " << ctx->setup->getProcessorSetup()->exitCode <<
                std::endl;
                return 0;
            }
            return 1;
        }

        int8_t processorTickTest() {
            auto ctx = IntegrationTests::prepareContext();
            auto c = std::make_shared<ByteCode>(USS("TestNNTick"), ctx->byteLoader);
            // A(0,0) -(1,0,0)-> T
            auto b = std::make_shared<ByteCodeBlock>(USS("A"), ADDITIVE);
            c->addBlock(b);
            auto bp = std::make_shared<ByteCodeParameter>(ElementParams::BIAS, (int8_t) 1);
            b->setParameter(bp);
            auto bi = std::make_shared<ByteCodeBlockItem>(USS("T"), b);
            b->addItem(bi);
            auto bip = std::make_shared<ByteCodeParameter>(LinkParams::ACTIVATION, (int8_t) 1);
            bi->setParameter(bip);
            // T -(0,1,0)-> B
            auto b2 = std::make_shared<ByteCodeBlock>(USS("T"), TICK);
            c->addBlock(b2);
            auto bi2 = std::make_shared<ByteCodeBlockItem>(USS("B"), b2);
            b2->addItem(bi2);
            auto bi2p = std::make_shared<ByteCodeParameter>(LinkParams::STREAM, (int8_t) 1);
            bi2->setParameter(bi2p);
            // B
            auto b3 = std::make_shared<ByteCodeBlock>(USS("B"), ADDITIVE);
            c->addBlock(b3);
            ByteCodeUtils::finalizeCode(c);
            ctx->useByteCode(c);
            Processor p(ctx->setup, ctx);
            ctx->setup->getProcessorSetup()->tick = 10;

            // default (0)
            p.processStep();
            std::shared_ptr<ByteCodeParameter> param;
            std::shared_ptr<ByteCodeParameter> resparam;
            if (!b3->tryGetParameter(ElementParams::SUM, resparam) || fabs(resparam->value.doubleValue) > EPSILON) {
                shoutError(ElementParams::SUM, resparam, 0.0, b3, 1);
                return 0;
            }
            p.processStep();
            if (!b3->tryGetParameter(ElementParams::SUM, resparam) || fabs(resparam->value.doubleValue) > EPSILON) {
                shoutError(ElementParams::SUM, resparam, 0.0, b3, 2);
                return 0;
            }
            ctx->setup->getProcessorSetup()->tick++;
            p.processStep();
            if (!b3->tryGetParameter(ElementParams::SUM, resparam) ||
                fabs(resparam->value.doubleValue - 10.) > EPSILON) {
                shoutError(ElementParams::SUM, resparam, 10.0, b3, 3);
                return 0;
            }
            p.processStep();
            if (!b3->tryGetParameter(ElementParams::SUM, resparam) ||
                fabs(resparam->value.doubleValue - 11.) > EPSILON) {
                shoutError(ElementParams::SUM, resparam, 11.0, b3, 3);
                return 0;
            }
            return 1;
        }

        int8_t processorTimeTest() {
            auto mspernode = .5;
            auto nnode = 3;
            auto steps = 1000;

            auto ctx = IntegrationTests::prepareContext();
            auto c = std::make_shared<ByteCode>(USS("TestNNTime"), ctx->byteLoader);
            // A(0,0) -(1,0,0)-> T
            auto b = std::make_shared<ByteCodeBlock>(USS("A"), ADDITIVE);
            c->addBlock(b);
            auto bp = std::make_shared<ByteCodeParameter>(ElementParams::BIAS, (int8_t) 1);
            b->setParameter(bp);
            auto bi = std::make_shared<ByteCodeBlockItem>(USS("T"), b);
            b->addItem(bi);
            auto bip = std::make_shared<ByteCodeParameter>(LinkParams::ACTIVATION, (int8_t) 1);
            bi->setParameter(bip);
            // T -(0,1,0)-> B
            auto b2 = std::make_shared<ByteCodeBlock>(USS("T"), TIME);
            c->addBlock(b2);
            auto bi2 = std::make_shared<ByteCodeBlockItem>(USS("B"), b2);
            b2->addItem(bi2);
            auto bi2p = std::make_shared<ByteCodeParameter>(LinkParams::STREAM, (int8_t) 1);
            bi2->setParameter(bi2p);
            // B
            auto b3 = std::make_shared<ByteCodeBlock>(USS("B"), ADDITIVE);
            c->addBlock(b3);
            ByteCodeUtils::finalizeCode(c);
            Processor p(ctx->setup, ctx);
            ctx->useByteCode(c);

            // default (0)
            p.processStep();
            std::shared_ptr<ByteCodeParameter> param;
            std::shared_ptr<ByteCodeParameter> resparam;
            if (!b3->tryGetParameter(ElementParams::SUM, resparam) || fabs(resparam->value.doubleValue) > EPSILON) {
                shoutError(ElementParams::SUM, resparam, 0.0, b3, 1);
                return 0;
            }
            p.processStep();
            if (!b3->tryGetParameter(ElementParams::SUM, resparam) || fabs(resparam->value.doubleValue) > EPSILON) {
                shoutError(ElementParams::SUM, resparam, 0.0, b3, 2);
                return 0;
            }
            p.processStep();
            if (!b3->tryGetParameter(ElementParams::SUM, resparam)) {
                shoutError(ElementParams::SUM, resparam, 0.0, b3, 3);
                return 0;
            }
            auto prev = resparam->value.doubleValue;
            p.processStep();
            if (!b3->tryGetParameter(ElementParams::SUM, resparam) || fabs(resparam->value.doubleValue - prev) > 100) {
                shoutError(ElementParams::SUM, resparam, prev + 100, b3, 4);
                return 0;
            }
            auto t = (resparam->value.doubleValue - prev) / nnode;
            if (t > mspernode) {
                std::cout << "Too low single performance. Was " << t << "ms/tick/node but expected at most " <<
                mspernode << std::endl;
                return 0;
            }
            prev = resparam->value.doubleValue;
            for (auto i = 0; i < steps; i++) {
                p.processStep();
            }
            if (!b3->tryGetParameter(ElementParams::SUM, resparam) || fabs(resparam->value.doubleValue - prev) > 3000) {
                shoutError(ElementParams::SUM, resparam, prev + 3000, b3, steps + 4);
                return 0;
            }
            t = (resparam->value.doubleValue - prev) / steps / nnode;
            if (t > mspernode) {
                std::cout << "Too low average performance. Was " << t << "ms/tick/node but expected at most " <<
                mspernode << std::endl;
                return 0;
            }
            std::cout << "Performance is " << t << "ms/tick/node or " << t * 14000000000 / 1000 / 60 / 60 / 24 <<
            "d/tick/14bln" << std::endl;
            return 1;
        }

        int8_t processorPerformanceTest(ProcessorParallelMode mode) {
            auto mspernode = .5;
            auto w = 50;
            auto h = 50;
            auto nnode = w * h + 2;
            auto steps = 2;

            auto ctx = IntegrationTests::prepareContext(mode);
            auto c = std::make_shared<ByteCode>(USS("TestNNPerformance"), ctx->byteLoader);

            // T -(0,1,0)-> B
            auto b2 = std::make_shared<ByteCodeBlock>(USS("T"), TIME);
            c->addBlock(b2);
            auto bi2 = std::make_shared<ByteCodeBlockItem>(USS("B"), b2);
            b2->addItem(bi2);
            auto bi2p = std::make_shared<ByteCodeParameter>(LinkParams::STREAM, (int8_t) 1);
            bi2->setParameter(bi2p);
            // B
            auto b3 = std::make_shared<ByteCodeBlock>(USS("B"), ADDITIVE);
            c->addBlock(b3);

            std::vector<std::shared_ptr<ByteCodeBlock>> bs(static_cast<unsigned long>(w * h));
            for (auto i = 0; i < w; i++) {
                for (auto k = 0; k < h; k++) {
                    auto name = StringUtils::spf(USS("A_{0}_{1}"), i, k);
                    auto b = std::make_shared<ByteCodeBlock>(name, ADDITIVE);
                    auto bp = std::make_shared<ByteCodeParameter>(ElementParams::BIAS, (int8_t) 1);
                    b->setParameter(bp);
                    c->addBlock(b);
                    bs[i * w + k] = b;
                }
            }
            for (auto i = 1; i < w; i++) {
                for (auto k = 0; k < h; k++) {
                    // A_i-1_k -(1,0,0)-> A_i_k
                    // A_i-1_k -(1,0,0)-> T
                    auto from = StringUtils::spf(USS("A_{0}_{1}"), i - 1, k);
                    auto to = StringUtils::spf(USS("A_{0}_{1}"), i, k);
                    auto bi = std::make_shared<ByteCodeBlockItem>(to, bs[(i - 1) * w + k]);
                    bs[(i - 1) * w + k]->addItem(bi);
                    auto bip = std::make_shared<ByteCodeParameter>(LinkParams::ACTIVATION, (int8_t) 1);
                    bi->setParameter(bip);
                    auto bii2 = std::make_shared<ByteCodeBlockItem>(USS("T"), bs[(i - 1) * w + k]);
                    bs[(i - 1) * w + k]->addItem(bii2);
                    auto bii2p = std::make_shared<ByteCodeParameter>(LinkParams::ACTIVATION, (int8_t) 1);
                    bii2->setParameter(bii2p);
                }
            }
            ByteCodeUtils::finalizeCode(c);
            ctx->useByteCode(c);
            Processor p(ctx->setup, ctx);

            // default (0)
            p.processStep();
            std::shared_ptr<ByteCodeParameter> param;
            std::shared_ptr<ByteCodeParameter> resparam;
            if (!b3->tryGetParameter(ElementParams::SUM, resparam) || fabs(resparam->value.doubleValue) > EPSILON) {
                shoutError(ElementParams::SUM, resparam, 0.0, b3, 1);
                return 0;
            }
            p.processStep();
            if (!b3->tryGetParameter(ElementParams::SUM, resparam) || fabs(resparam->value.doubleValue) > EPSILON) {
                shoutError(ElementParams::SUM, resparam, 0.0, b3, 2);
                return 0;
            }
            p.processStep();
            if (!b3->tryGetParameter(ElementParams::SUM, resparam)) {
                shoutError(ElementParams::SUM, resparam, 0.0, b3, 3);
                return 0;
            }
            auto prev = resparam->value.doubleValue;
            for (auto i = 0; i < steps; i++) {
                p.processStep();
            }
            if (!b3->tryGetParameter(ElementParams::SUM, resparam) ||
                fabs(resparam->value.doubleValue - prev) > 30000) {
                shoutError(ElementParams::SUM, resparam, prev + 3000, b3, steps + 3);
                return 0;
            }
            auto t = (resparam->value.doubleValue - prev) / steps / nnode;
            if (t > mspernode) {
                std::cout << "Too low average performance. Was " << t << "ms/tick/node but expected at most " <<
                mspernode << std::endl;
                return 0;
            }
            std::cout << "Performance is " << t << "ms/tick/node" << std::endl;
            return 1;
        }

        int8_t processorPerformanceSequentialTest() {
            return processorPerformanceTest(ProcessorParallelMode::NONE);
        }

        int8_t processorPerformanceOMPTest() {
#ifdef _OPENMP
            return processorPerformanceTest(ProcessorParallelMode::OMP);
#else
            std::cout << "OpenMP is not supported" << std::endl;
            return 1;
#endif
        }

        int8_t processorPerformanceOCLTest() {
            std::cout << "OpenCL is not supported" << std::endl;
            return 1;
        }

        int8_t nameParserSimpleTest() {
            auto etalon = USS("a");
            auto res = NameParts::parseName(etalon);
            return static_cast<int8_t>(res->name == etalon && !res->subname && !res->isGlobal && res->indexes.empty() ? 1 : 0);
        }

        int8_t nameParserGlobalTest() {
            auto res = NameParts::parseName(USS(".a"));
            return static_cast<int8_t>(res->name == USS("a") && !res->subname && res->isGlobal && res->indexes.empty() ? 1 : 0);
        }

        int8_t nameParserWrongTest() {
            try {
                NameParts::parseName(USS("."));
                std::cout << "Not failed on " << "'.'" << std::endl;
                return 0;
            }
            catch (NameParserException &e) {
                return 1;
            }
        }

        int8_t nameParserWrongCommaTest() {
            try {
                NameParts::parseName(USS("a,a"));
                std::cout << "Not failed on " << "'a,a'" << std::endl;
                return 0;
            }
            catch (NameParserException &e) {
                return 1;
            }
        }

        int8_t nameParserLongTest() {
            auto etalon = USS("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_#");
            std::shared_ptr<NameParts> res;
            auto n = 100;
            int64_t tm;
            int64_t td;
            PerformanceUtils::timeit([&n, &res, &etalon]() {
                for (auto i = 0; i < n; ++i) {
                    res = NameParts::parseName(etalon);
                }
            }, [&tm](int64_t t) { tm = t; });
            PerformanceUtils::timeit([&n, &res, &etalon]() {
                for (auto i = 0; i < n; ++i) {
                    res = std::make_shared<NameParts>();
                    for (auto k = 0; k < etalon.length(); ++k) {
                        auto c = etalon.char32At(k);
                        if (u_isalpha(c) || u_isdigit(c) || c == '.' || c == ',' || c == '[' || c == ']' ||
                            u_isWhitespace(c) || c == '_' || c == '#') {
                            res->name.append(etalon.char32At(k));
                        }
                    }
                }
            }, [&td](int64_t t) { td = t; });
            std::cout << "Performance Parser=" << tm / n << "ns, Direct=" << td / n << "ns, Latency " << tm / td <<
            " times" << std::endl;
            return static_cast<int8_t>(res->name == etalon && !res->subname && !res->isGlobal && res->indexes.empty() ? 1 : 0);
        }

        int8_t nameParserIndexTest() {
            auto etalon = USS("a");
            auto res = NameParts::parseName(USS("a[a]"));
            return static_cast<int8_t>(res->name == etalon && !res->subname && !res->isGlobal && res->indexes.size() == 1 &&
                                       res->indexes[0]->name == etalon && !res->indexes[0]->subname &&
                                       !res->indexes[0]->isGlobal && res->indexes[0]->indexes.empty() ? 1 : 0);
        }

        int8_t nameParserSubTest() {
            auto res = NameParts::parseName(USS("a.a"));
            return static_cast<int8_t>(res->name == USS("a") && res->subname && res->subname->name == USS("a") &&
                                       res->subname->isGlobal && !res->subname->subname &&
                                       res->subname->indexes.empty() && !res->isGlobal &&
                                       res->indexes.empty() ? 1 : 0);
        }

        int8_t compilerEmptyTest() {
            icu::UnicodeString j = USS("");
            auto ss = std::make_shared<std::istringstream>();
            ss->str(StringConvUtils::utou8(j));
            nnvm::ByteCodeParser parser(ss, TEST_PATH, USS("test"), IntegrationTests::prepareContext());
            auto res = parser.read();
            return res == nullptr;
        }

        int8_t compilerConstTest() {
            icu::UnicodeString j = USS("[C:2, LEN:50]");
            auto ss = std::make_shared<std::istringstream>();
            ss->str(StringConvUtils::utou8(j));
            nnvm::ByteCodeParser parser(ss, TEST_PATH, USS("test"), IntegrationTests::prepareContext());
            auto res = parser.read();
            return res && res->empty();
        }

        int8_t compilerSingleTest() {
            icu::UnicodeString j = USS("single {  }");
            auto ss = std::make_shared<std::istringstream>();
            ss->str(StringConvUtils::utou8(j));
            nnvm::ByteCodeParser parser(ss, TEST_PATH, USS("single"),
                                                       IntegrationTests::prepareContext());
            auto res = parser.read();

            auto b = true;
            b = b && (bool) res;
            if (!b) {
                std::cout << "No res" << std::endl;
                return b;
            }
            b = b && res->size() == 1;
            if (!b) {
                std::cout << "res not 1" << std::endl;
                return b;
            }
            b = b && res->front()->getName() == USS("tests.single");
            if (!b) {
                std::cout << "res[0] package not tests.single but " <<
                StringConvUtils::utou8(res->front()->getName()) << std::endl;
                return b;
            }
            auto r = res->front()->getBlocks();
            b = b && std::get<0>(r) == std::get<1>(r);
            if (!b) {
                std::cout << "res[0][0] has blocks" << std::endl;
                return b;
            }
            return b;
        }

        int8_t compilerMainArgumentsTest() {
            icu::UnicodeString j = USS("single B, C { }");
            auto ss = std::make_shared<std::istringstream>();
            ss->str(StringConvUtils::utou8(j));
            nnvm::ByteCodeParser parser(ss, TEST_PATH, USS("single"),
                                                       IntegrationTests::prepareContext());
            auto res = parser.read();
            auto r0 = res->front()->getBlocks();
            auto r1 = res->front()->getBlocks();
            auto tr1 = std::get<0>(r1);
            std::advance(tr1, 1);
            r1 = std::tuple<std::deque<std::shared_ptr<ByteCodeBlock>>::iterator, std::deque<std::shared_ptr<ByteCodeBlock>>::iterator>(
                    tr1, std::get<1>(r1));

            auto b = true;
            b = b && (bool) res;
            if (!b) {
                std::cout << "No res" << std::endl;
                return b;
            }
            b = b && res->size() == 1;
            if (!b) {
                std::cout << "res not 1" << std::endl;
                return b;
            }
            b = b && std::get<0>(r0) != std::get<1>(r0);
            if (!b) {
                std::cout << "res[0][0] no blocks" << std::endl;
                return b;
            }
            b = b && std::get<0>(r1) != std::get<1>(r1);
            if (!b) {
                std::cout << "res[0][1] no blocks" << std::endl;
                return b;
            }
            b = b && (*std::get<0>(r0))->getName() == USS("B");
            if (!b) {
                std::cout << "res[0][0] not B but " << StringConvUtils::utou8((*std::get<0>(r0))->getName()) <<
                std::endl;
                return b;
            }
            b = b && (*std::get<0>(r1))->getName() == USS("C");
            if (!b) {
                std::cout << "res[0][1] not C but " << StringConvUtils::utou8((*std::get<0>(r1))->getName()) <<
                std::endl;
                return b;
            }
            return b;
        }

        int8_t compilerCommentsTest() {
            icu::UnicodeString j = USS(
                    "\"test\"[L:10]\n\"\"\"\nsimple \"single\" block with arguments\n\"\"\"\nsingle \"that's bee\"B, \"that's see\"C { \"\"\"empty\"\"\" }");
            auto ss = std::make_shared<std::istringstream>();
            ss->str(StringConvUtils::utou8(j));
            nnvm::ByteCodeParser parser(ss, TEST_PATH, USS("single"), IntegrationTests::prepareContext());
            auto res = parser.read();

            auto r0 = res->front()->getBlocks();
            auto r1 = res->front()->getBlocks();
            auto tr1 = std::get<0>(r1);
            std::advance(tr1, 1);
            r1 = std::tuple<std::deque<std::shared_ptr<ByteCodeBlock>>::iterator, std::deque<std::shared_ptr<ByteCodeBlock>>::iterator>(
                    tr1, std::get<1>(r1));

            auto b = true;
            b = b && (bool) res;
            if (!b) {
                std::cout << "No res" << std::endl;
                return b;
            }
            b = b && res->size() == 1;
            if (!b) {
                std::cout << "res not 1" << std::endl;
                return b;
            }
            b = b && std::get<0>(r0) != std::get<1>(r0);
            if (!b) {
                std::cout << "res[0][0] no blocks" << std::endl;
                return b;
            }
            b = b && std::get<0>(r1) != std::get<1>(r1);
            if (!b) {
                std::cout << "res[0][1] no blocks" << std::endl;
                return b;
            }
            b = b && (*std::get<0>(r0))->getName() == USS("B");
            if (!b) {
                std::cout << "res[0][0] not B but " << StringConvUtils::utou8((*std::get<0>(r0))->getName()) <<
                std::endl;
                return b;
            }
            b = b && (*std::get<0>(r1))->getName() == USS("C");
            if (!b) {
                std::cout << "res[0][1] not C but " << StringConvUtils::utou8((*std::get<0>(r1))->getName()) <<
                std::endl;
                return b;
            }
            return b;
        }

        int8_t compilerSingleVariableTest() {
            icu::UnicodeString j = USS("single B, C { A =\"or 'is'\" #\"or 'additive'\" (+ B, C), 0 }");
            auto ss = std::make_shared<std::istringstream>();
            ss->str(StringConvUtils::utou8(j));
            nnvm::ByteCodeParser parser(ss, TEST_PATH, USS("single"), IntegrationTests::prepareContext());
            auto res = parser.read();

            auto r0 = res->front()->getBlocks();
            auto r1 = res->front()->getBlocks();
            auto tr1 = std::get<0>(r1);
            std::advance(tr1, 1);
            r1 = std::tuple<std::deque<std::shared_ptr<ByteCodeBlock>>::iterator, std::deque<std::shared_ptr<ByteCodeBlock>>::iterator>(
                    tr1, std::get<1>(r1));
            auto r2 = res->front()->getBlocks();
            auto tr2 = std::get<0>(r2);
            std::advance(tr2, 2);
            r2 = std::tuple<std::deque<std::shared_ptr<ByteCodeBlock>>::iterator, std::deque<std::shared_ptr<ByteCodeBlock>>::iterator>(
                    tr2, std::get<1>(r2));

            for (auto &i : *res) {
                auto r = i->getBlocks();
                for (auto k = std::get<0>(r); k != std::get<1>(r); ++k) {
                    ByteCodeUtils::reveal(*k);
                }
            }

            std::shared_ptr<ByteCodeParameter> p;
            auto b = true;

            b = b && (bool) res;
            if (!b) {
                std::cout << "No res" << std::endl;
                return b;
            }
            b = b && res->size() == 1;
            if (!b) {
                std::cout << "res not 1" << std::endl;
                return b;
            }
            b = b && std::get<0>(r0) != std::get<1>(r0);
            if (!b) {
                std::cout << "res[0][0] no blocks" << std::endl;
                return b;
            }
            b = b && std::get<0>(r1) != std::get<1>(r1);
            if (!b) {
                std::cout << "res[0][1] no blocks" << std::endl;
                return b;
            }
            b = b && std::get<0>(r2) != std::get<1>(r2);
            if (!b) {
                std::cout << "res[0][2] no blocks" << std::endl;
                return b;
            }
            b = b && (*std::get<0>(r0))->getName() == USS("A");
            if (!b) {
                std::cout << "res[0][0] not A but " << StringConvUtils::utou8((*std::get<0>(r0))->getName()) <<
                std::endl;
                return b;
            }
            b = b && (*std::get<0>(r1))->getName() == USS("B");
            if (!b) {
                std::cout << "res[0][2] not B but " << StringConvUtils::utou8((*std::get<0>(r1))->getName()) <<
                std::endl;
                return b;
            }
            b = b && (*std::get<0>(r2))->getName() == USS("C");
            if (!b) {
                std::cout << "res[0][3] not C but " << StringConvUtils::utou8((*std::get<0>(r2))->getName()) <<
                std::endl;
                return b;
            }
            b = b && !(*std::get<0>(r0))->tryGetParameter(ElementParams::EXPECTED, p);
            if (!b) {
                std::cout << "res[0][0] shouldn't have Expected" << std::endl;
                return b;
            }
            b = b && !(*std::get<0>(r1))->tryGetParameter(ElementParams::EXPECTED, p);
            if (!b) {
                std::cout << "res[0][1] shouldn't have Expected" << std::endl;
                return b;
            }
            b = b && !(*std::get<0>(r2))->tryGetParameter(ElementParams::EXPECTED, p);
            if (!b) {
                std::cout << "res[0][2] shouldn't have Expected" << std::endl;
                return b;
            }
            b = b && !(*std::get<0>(r0))->tryGetParameter(ElementParams::INPUT, p);
            if (!b) {
                std::cout << "res[0][0] shouldn't have Input" << std::endl;
                return b;
            }
            b = b && (*std::get<0>(r1))->tryGetParameter(ElementParams::INPUT, p) && p->value.byteValue;
            if (!b) {
                std::cout << "res[0][1] Input not 1 but " << p->value.byteValue << std::endl;
                return b;
            }
            b = b && (*std::get<0>(r2))->tryGetParameter(ElementParams::INPUT, p) && p->value.byteValue;
            if (!b) {
                std::cout << "res[0][2] Input not 1 but " << p->value.byteValue << std::endl;
                return b;
            }
            b = b && !(*std::get<0>(r0))->tryGetParameter(ElementParams::INDEX, p);
            if (!b) {
                std::cout << "res[0][0] shouldn't have Index" << std::endl;
                return b;
            }
            b = b && (*std::get<0>(r1))->tryGetParameter(ElementParams::INDEX, p) && p->value.integerValue == (int64_t) 1;
            if (!b) {
                std::cout << "res[0][1] Index not 1 but " << p->value.integerValue << std::endl;
                return b;
            }
            b = b && (*std::get<0>(r2))->tryGetParameter(ElementParams::INDEX, p) && p->value.integerValue == (int64_t) 2;
            if (!b) {
                std::cout << "res[0][2] Index not 2 but " << p->value.integerValue << std::endl;
                return b;
            }
            auto ri = (*std::get<0>(r0))->getIncomes();
            b = b && std::get<0>(ri) != std::get<1>(ri);
            if (!b) {
                std::cout << "res[0][0] Incomes empty" << std::endl;
                return b;
            }
            b = b && (*(*std::get<0>(r0))->getIncome(0))->getOwner().lock()->getName() == USS("B");
            if (!b) {
                std::cout << "res[0][0] in[0] not B but " <<
                StringConvUtils::utou8((*(*std::get<0>(r0))->getIncome(0))->getOwner().lock()->getName()) << std::endl;
                return b;
            }
            b = b && ByteCodeUtils::asDouble(*(*(*std::get<0>(r0))->getIncome(0))->getParameter(LinkParams::ACTIVATION)) ==
                 1.0;
            if (!b) {
                std::cout << "res[0][0] in[0] activation not 1 but " << ByteCodeUtils::asDouble(
                        *(*(*std::get<0>(r0))->getIncome(0))->getParameter(LinkParams::ACTIVATION)) << std::endl;
                return b;
            }
            b = b && !(*(*std::get<0>(r0))->getIncome(0))->tryGetParameter(LinkParams::STREAM, p);
            if (!b) {
                std::cout << "res[0][0] in[0] stream not 0 but " <<
                ByteCodeUtils::asDouble(*(*(*std::get<0>(r0))->getIncome(0))->getParameter(LinkParams::STREAM)) <<
                std::endl;
                return b;
            }
            b = b && (*(*std::get<0>(r0))->getIncome(1))->getOwner().lock()->getName() == USS("C");
            if (!b) {
                std::cout << "res[0][0] in[1] not C but " <<
                StringConvUtils::utou8((*(*std::get<0>(r0))->getIncome(1))->getOwner().lock()->getName()) << std::endl;
                return b;
            }
            b = b && ByteCodeUtils::asDouble(*(*(*std::get<0>(r0))->getIncome(1))->getParameter(LinkParams::ACTIVATION)) ==
                 1.0;
            if (!b) {
                std::cout << "res[0][0] in[1] activation not 1 but " << ByteCodeUtils::asDouble(
                        *(*(*std::get<0>(r0))->getIncome(1))->getParameter(LinkParams::ACTIVATION)) << std::endl;
                return b;
            }
            b = b && !(*(*std::get<0>(r0))->getIncome(1))->tryGetParameter(LinkParams::STREAM, p);
            if (!b) {
                std::cout << "res[0][0] in[1] stream not 0 but " <<
                ByteCodeUtils::asDouble(*(*(*std::get<0>(r0))->getIncome(1))->getParameter(LinkParams::STREAM)) <<
                std::endl;
                return b;
            }
            return b;
        }

        int8_t compilerExpectedVariableTest() {
            icu::UnicodeString j = USS("single { A = # (+ B, C), 0 }");
            auto ss = std::make_shared<std::istringstream>();
            ss->str(StringConvUtils::utou8(j));
            nnvm::ByteCodeParser parser(ss, TEST_PATH, USS("single"), IntegrationTests::prepareContext());
            auto res = parser.read();

            auto r0 = res->front()->getBlocks();
            auto r1 = res->front()->getBlocks();
            auto tr1 = std::get<0>(r1);
            std::advance(tr1, 1);
            r1 = std::tuple<std::deque<std::shared_ptr<ByteCodeBlock>>::iterator, std::deque<std::shared_ptr<ByteCodeBlock>>::iterator>(
                    tr1, std::get<1>(r1));
            auto r2 = res->front()->getBlocks();
            auto tr2 = std::get<0>(r2);
            std::advance(tr2, 2);
            r2 = std::tuple<std::deque<std::shared_ptr<ByteCodeBlock>>::iterator, std::deque<std::shared_ptr<ByteCodeBlock>>::iterator>(
                    tr2, std::get<1>(r2));

            std::shared_ptr<ByteCodeParameter> p;

            fast_assert_less((bool) res, "No res");
            fast_assert(res->size() == 1, "res not 1 but ", res->size());
            fast_assert_less(std::get<0>(r0) != std::get<1>(r0), "res[0][0] no blocks");
            fast_assert_less(std::get<0>(r1) != std::get<1>(r1), "res[0][1] no blocks");
            fast_assert_less(std::get<0>(r2) != std::get<1>(r2), "res[0][2] no blocks");
            fast_assert((*std::get<0>(r0))->getName() == USS("A"), "res[0][0] not A but ",
                        StringConvUtils::utou8((*std::get<0>(r0))->getName()));
            fast_assert((*std::get<0>(r1))->getName() == USS("B"), "res[0][1] not B but ",
                        StringConvUtils::utou8((*std::get<0>(r1))->getName()));
            fast_assert((*std::get<0>(r2))->getName() == USS("C"), "res[0][2] not C but ",
                        StringConvUtils::utou8((*std::get<0>(r2))->getName()));
            fast_assert(!(*std::get<0>(r0))->tryGetParameter(ElementParams::EXPECTED, p),
                        "res[0][0] shouldn't have Expected but ", p->value.byteValue);
            fast_assert((*std::get<0>(r1))->tryGetParameter(ElementParams::EXPECTED, p) && p->value.byteValue,
                        "res[0][1] Expected not 1 but ", p->value.byteValue);
            fast_assert((*std::get<0>(r2))->tryGetParameter(ElementParams::EXPECTED, p) && p->value.byteValue,
                        "res[0][2] Expected not 1 but ", p->value.byteValue);
            auto ri = (*std::get<0>(r0))->getIncomes();
            fast_assert_less(std::get<0>(ri) != std::get<1>(ri), "res[0][0] Incomes empty");
            fast_assert((*(*std::get<0>(r0))->getIncome(0))->getOwner().lock()->getName() == USS("B"),
                        "res[0][0] in[0] not B but ",
                        StringConvUtils::utou8((*(*std::get<0>(r0))->getIncome(0))->getOwner().lock()->getName()));
            fast_assert(ByteCodeUtils::asDouble(
                    *(*(*std::get<0>(r0))->getIncome(0))->getParameter(LinkParams::ACTIVATION)) == 1.0,
                        "res[0][0] in[0] activation not 1 but ", ByteCodeUtils::asDouble(
                    *(*(*std::get<0>(r0))->getIncome(0))->getParameter(LinkParams::ACTIVATION)));
            fast_assert(!(*(*std::get<0>(r0))->getIncome(0))->tryGetParameter(LinkParams::STREAM, p),
                        "res[0][0] in[0] stream not 0 but ", ByteCodeUtils::asDouble(
                    *(*(*std::get<0>(r0))->getIncome(0))->getParameter(LinkParams::STREAM)));
            return 1;
        }

        int8_t compilerOneAdditiveTest() {
            icu::UnicodeString j = USS("single { A = # (+ B, C) }");
            auto ss = std::make_shared<std::istringstream>();
            ss->str(StringConvUtils::utou8(j));
            nnvm::ByteCodeParser parser(ss, TEST_PATH, USS("single"), IntegrationTests::prepareContext());
            auto res = parser.read();

            auto r0 = res->front()->getBlocks();
            auto r1 = res->front()->getBlocks();
            auto tr1 = std::get<0>(r1);
            std::advance(tr1, 1);
            r1 = std::tuple<std::deque<std::shared_ptr<ByteCodeBlock>>::iterator, std::deque<std::shared_ptr<ByteCodeBlock>>::iterator>(
                    tr1, std::get<1>(r1));
            auto r2 = res->front()->getBlocks();
            auto tr2 = std::get<0>(r2);
            std::advance(tr2, 2);
            r2 = std::tuple<std::deque<std::shared_ptr<ByteCodeBlock>>::iterator, std::deque<std::shared_ptr<ByteCodeBlock>>::iterator>(
                    tr2, std::get<1>(r2));

            std::shared_ptr<ByteCodeParameter> p;

            fast_assert_less((bool) res, "No res");
            fast_assert(res->size() == 1, "res not 1 but ", res->size());
            fast_assert_less(std::get<0>(r0) != std::get<1>(r0), "res[0][0] no blocks");
            fast_assert_less(std::get<0>(r1) != std::get<1>(r1), "res[0][1] no blocks");
            fast_assert_less(std::get<0>(r2) != std::get<1>(r2), "res[0][2] no blocks");
            fast_assert((*std::get<0>(r0))->getName() == USS("A"), "res[0][0] not A but ",
                        StringConvUtils::utou8((*std::get<0>(r0))->getName()));
            fast_assert((*std::get<0>(r1))->getName() == USS("B"), "res[0][1] not B but ",
                        StringConvUtils::utou8((*std::get<0>(r1))->getName()));
            fast_assert((*std::get<0>(r2))->getName() == USS("C"), "res[0][2] not C but ",
                        StringConvUtils::utou8((*std::get<0>(r2))->getName()));
            fast_assert(!(*std::get<0>(r0))->tryGetParameter(ElementParams::EXPECTED, p),
                        "res[0][0] shouldn't have Expected but ", p->value.byteValue);
            fast_assert((*std::get<0>(r1))->tryGetParameter(ElementParams::EXPECTED, p) && p->value.byteValue,
                        "res[0][1] Expected not 1 but ", p->value.byteValue);
            fast_assert((*std::get<0>(r2))->tryGetParameter(ElementParams::EXPECTED, p) && p->value.byteValue,
                        "res[0][2] Expected not 1 but ", p->value.byteValue);
            auto ri = (*std::get<0>(r0))->getIncomes();
            fast_assert_less(std::get<0>(ri) != std::get<1>(ri), "res[0][0] Incomes empty");
            fast_assert((*(*std::get<0>(r0))->getIncome(0))->getOwner().lock()->getName() == USS("B"),
                        "res[0][0] in[0] not B but ",
                        StringConvUtils::utou8((*(*std::get<0>(r0))->getIncome(0))->getOwner().lock()->getName()));
            fast_assert(!(*(*std::get<0>(r0))->getIncome(0))->tryGetParameter(LinkParams::ACTIVATION, p),
                        "res[0][0] in[0] activation not 0 but ", ByteCodeUtils::asDouble(
                    *(*(*std::get<0>(r0))->getIncome(0))->getParameter(LinkParams::ACTIVATION)));
            fast_assert(
                    ByteCodeUtils::asDouble(*(*(*std::get<0>(r0))->getIncome(0))->getParameter(LinkParams::STREAM)) ==
                    1.0, "res[0][0] in[0] stream not 1 but ",
                    ByteCodeUtils::asDouble(*(*(*std::get<0>(r0))->getIncome(0))->getParameter(LinkParams::STREAM)));
            return 1;
        }

        int8_t compilerExpressionTest() {
            icu::UnicodeString j = USS("single B, C { A = + B, C }");
            auto ss = std::make_shared<std::istringstream>();
            ss->str(StringConvUtils::utou8(j));
            nnvm::ByteCodeParser parser(ss, TEST_PATH, USS("single"), IntegrationTests::prepareContext());
            auto res = parser.read();

            auto r0 = res->front()->getBlocks();
            auto r1 = res->front()->getBlocks();
            auto tr1 = std::get<0>(r1);
            std::advance(tr1, 1);
            r1 = std::tuple<std::deque<std::shared_ptr<ByteCodeBlock>>::iterator, std::deque<std::shared_ptr<ByteCodeBlock>>::iterator>(
                    tr1, std::get<1>(r1));
            auto r2 = res->front()->getBlocks();
            auto tr2 = std::get<0>(r2);
            std::advance(tr2, 2);
            r2 = std::tuple<std::deque<std::shared_ptr<ByteCodeBlock>>::iterator, std::deque<std::shared_ptr<ByteCodeBlock>>::iterator>(
                    tr2, std::get<1>(r2));

            std::shared_ptr<ByteCodeParameter> p;

            fast_assert_less((bool) res, "No res");
            fast_assert(res->size() == 1, "res not 1 but ", res->size());
            fast_assert_less(std::get<0>(r0) != std::get<1>(r0), "res[0][0] no blocks");
            fast_assert_less(std::get<0>(r1) != std::get<1>(r1), "res[0][1] no blocks");
            fast_assert_less(std::get<0>(r2) != std::get<1>(r2), "res[0][2] no blocks");
            fast_assert((*std::get<0>(r0))->getName() == USS("A"), "res[0][0] not A but ",
                        StringConvUtils::utou8((*std::get<0>(r0))->getName()));
            fast_assert((*std::get<0>(r1))->getName() == USS("B"), "res[0][1] not B but ",
                        StringConvUtils::utou8((*std::get<0>(r1))->getName()));
            fast_assert((*std::get<0>(r2))->getName() == USS("C"), "res[0][2] not C but ",
                        StringConvUtils::utou8((*std::get<0>(r2))->getName()));
            fast_assert(!(*std::get<0>(r0))->tryGetParameter(ElementParams::EXPECTED, p),
                        "res[0][0] shouldn't have Expected but ", p->value.byteValue);
            fast_assert(!(*std::get<0>(r1))->tryGetParameter(ElementParams::EXPECTED, p),
                        "res[0][1] shouldn't have Expected but ", p->value.byteValue);
            fast_assert(!(*std::get<0>(r2))->tryGetParameter(ElementParams::EXPECTED, p),
                        "res[0][2] shouldn't have Expected but ", p->value.byteValue);
            fast_assert(!(*std::get<0>(r0))->tryGetParameter(ElementParams::INPUT, p),
                        "res[0][0] shouldn't have Input but ", p->value.byteValue);
            fast_assert((*std::get<0>(r1))->tryGetParameter(ElementParams::INPUT, p) && p->value.byteValue,
                        "res[0][1] Input not 1 but ", p->value.byteValue);
            fast_assert((*std::get<0>(r2))->tryGetParameter(ElementParams::INPUT, p) && p->value.byteValue,
                        "res[0][2] Input not 1 but ", p->value.byteValue);
            auto ri = (*std::get<0>(r0))->getIncomes();
            fast_assert_less(std::get<0>(ri) != std::get<1>(ri), "res[0][0] Incomes empty");
            fast_assert((*(*std::get<0>(r0))->getIncome(0))->getOwner().lock()->getName() == USS("B"),
                        "res[0][0] in[0] not B but ",
                        StringConvUtils::utou8((*(*std::get<0>(r0))->getIncome(0))->getOwner().lock()->getName()));
            fast_assert(!(*(*std::get<0>(r0))->getIncome(0))->tryGetParameter(LinkParams::ACTIVATION, p),
                        "res[0][0] in[0] activation not 0 but ", ByteCodeUtils::asDouble(
                    *(*(*std::get<0>(r0))->getIncome(0))->getParameter(LinkParams::ACTIVATION)));
            fast_assert(
                    ByteCodeUtils::asDouble(*(*(*std::get<0>(r0))->getIncome(0))->getParameter(LinkParams::STREAM)) ==
                    1.0, "res[0][0] in[0] stream not 1 but ",
                    ByteCodeUtils::asDouble(*(*(*std::get<0>(r0))->getIncome(0))->getParameter(LinkParams::STREAM)));
            return 1;
        }

        int8_t compilerComplexSumExpressionTest() {
            icu::UnicodeString j = USS("single B, C, D { A = + D, (* 2, B), C }");
            auto ss = std::make_shared<std::istringstream>();
            ss->str(StringConvUtils::utou8(j));
            nnvm::ByteCodeParser parser(ss, TEST_PATH, USS("single"), IntegrationTests::prepareContext());
            auto res = parser.read();

            auto r0 = res->front()->getBlocks();
            auto r1 = res->front()->getBlocks();
            auto tr1 = std::get<0>(r1);
            std::advance(tr1, 1);
            r1 = std::tuple<std::deque<std::shared_ptr<ByteCodeBlock>>::iterator, std::deque<std::shared_ptr<ByteCodeBlock>>::iterator>(
                    tr1, std::get<1>(r1));
            auto r2 = res->front()->getBlocks();
            auto tr2 = std::get<0>(r2);
            std::advance(tr2, 2);
            r2 = std::tuple<std::deque<std::shared_ptr<ByteCodeBlock>>::iterator, std::deque<std::shared_ptr<ByteCodeBlock>>::iterator>(
                    tr2, std::get<1>(r2));
            auto r3 = res->front()->getBlocks();
            auto tr3 = std::get<0>(r3);
            std::advance(tr3, 3);
            r3 = std::tuple<std::deque<std::shared_ptr<ByteCodeBlock>>::iterator, std::deque<std::shared_ptr<ByteCodeBlock>>::iterator>(
                    tr3, std::get<1>(r3));

            std::shared_ptr<ByteCodeParameter> p;

            fast_assert_less((bool) res, "No res");
            fast_assert(res->size() == 1, "res not 1 but ", res->size());
            fast_assert_less(std::get<0>(r0) != std::get<1>(r0), "res[0][0] no blocks");
            fast_assert_less(std::get<0>(r1) != std::get<1>(r1), "res[0][1] no blocks");
            fast_assert_less(std::get<0>(r2) != std::get<1>(r2), "res[0][2] no blocks");
            fast_assert_less(std::get<0>(r3) != std::get<1>(r3), "res[0][3] no blocks");
            fast_assert((*std::get<0>(r0))->getName() == USS("A"), "res[0][0] not A but ",
                        StringConvUtils::utou8((*std::get<0>(r0))->getName()));
            fast_assert((*std::get<0>(r1))->getName() == USS("B"), "res[0][1] not B but ",
                        StringConvUtils::utou8((*std::get<0>(r1))->getName()));
            fast_assert((*std::get<0>(r2))->getName() == USS("C"), "res[0][2] not C but ",
                        StringConvUtils::utou8((*std::get<0>(r2))->getName()));
            fast_assert((*std::get<0>(r3))->getName() == USS("D"), "res[0][3] not D but ",
                        StringConvUtils::utou8((*std::get<0>(r3))->getName()));
            fast_assert(!(*std::get<0>(r0))->tryGetParameter(ElementParams::EXPECTED, p),
                        "res[0][0] shouldn't have Expected but ", p->value.byteValue);
            fast_assert(!(*std::get<0>(r1))->tryGetParameter(ElementParams::EXPECTED, p),
                        "res[0][1] shouldn't have Expected but ", p->value.byteValue);
            fast_assert(!(*std::get<0>(r2))->tryGetParameter(ElementParams::EXPECTED, p),
                        "res[0][2] shouldn't have Expected but ", p->value.byteValue);
            fast_assert(!(*std::get<0>(r3))->tryGetParameter(ElementParams::EXPECTED, p),
                        "res[0][3] shouldn't have Expected but ", p->value.byteValue);
            fast_assert(!(*std::get<0>(r0))->tryGetParameter(ElementParams::INPUT, p),
                        "res[0][0] shouldn't have Input but ", p->value.byteValue);
            fast_assert((*std::get<0>(r1))->tryGetParameter(ElementParams::INPUT, p) && p->value.byteValue,
                        "res[0][1] Input not 1 but ", p->value.byteValue);
            fast_assert((*std::get<0>(r2))->tryGetParameter(ElementParams::INPUT, p) && p->value.byteValue,
                        "res[0][2] Input not 1 but ", p->value.byteValue);
            fast_assert((*std::get<0>(r3))->tryGetParameter(ElementParams::INPUT, p) && p->value.byteValue,
                        "res[0][3] Input not 1 but ", p->value.byteValue);
            auto ri = (*std::get<0>(r0))->getIncomes();
            fast_assert_less(std::get<0>(ri) != std::get<1>(ri), "res[0][0] Incomes empty");
            fast_assert((*(*std::get<0>(r0))->getIncome(0))->getOwner().lock()->getName() == USS("C"),
                        "res[0][0] in[0] not C but ",
                        StringConvUtils::utou8((*(*std::get<0>(r0))->getIncome(0))->getOwner().lock()->getName()));
            fast_assert(!(*(*std::get<0>(r0))->getIncome(0))->tryGetParameter(LinkParams::ACTIVATION, p),
                        "res[0][0] in[0] activation not 0 but ", ByteCodeUtils::asDouble(
                    *(*(*std::get<0>(r0))->getIncome(0))->getParameter(LinkParams::ACTIVATION)));
            fast_assert(
                    ByteCodeUtils::asDouble(*(*(*std::get<0>(r0))->getIncome(0))->getParameter(LinkParams::STREAM)) ==
                    1.0, "res[0][0] in[0] stream not 1 but ",
                    ByteCodeUtils::asDouble(*(*(*std::get<0>(r0))->getIncome(0))->getParameter(LinkParams::STREAM)));
            fast_assert((*(*std::get<0>(r0))->getIncome(1))->getOwner().lock()->getName() == USS("B"),
                        "res[0][0] in[1] not B but ",
                        StringConvUtils::utou8((*(*std::get<0>(r0))->getIncome(1))->getOwner().lock()->getName()));
            fast_assert(!(*(*std::get<0>(r0))->getIncome(1))->tryGetParameter(LinkParams::ACTIVATION, p),
                        "res[0][0] in[1] activation not 0 but ", ByteCodeUtils::asDouble(
                    *(*(*std::get<0>(r0))->getIncome(1))->getParameter(LinkParams::ACTIVATION)));
            fast_assert(
                    ByteCodeUtils::asDouble(*(*(*std::get<0>(r0))->getIncome(1))->getParameter(LinkParams::STREAM)) ==
                    2.0, "res[0][0] in[1] stream not 2 but ",
                    ByteCodeUtils::asDouble(*(*(*std::get<0>(r0))->getIncome(1))->getParameter(LinkParams::STREAM)));
            fast_assert((*(*std::get<0>(r0))->getIncome(2))->getOwner().lock()->getName() == USS("D"),
                        "res[0][0] in[2] not D but ",
                        StringConvUtils::utou8((*(*std::get<0>(r0))->getIncome(2))->getOwner().lock()->getName()));
            fast_assert(!(*(*std::get<0>(r0))->getIncome(2))->tryGetParameter(LinkParams::ACTIVATION, p),
                        "res[0][0] in[2] activation not 0 but ", ByteCodeUtils::asDouble(
                    *(*(*std::get<0>(r0))->getIncome(2))->getParameter(LinkParams::ACTIVATION)));
            fast_assert(
                    ByteCodeUtils::asDouble(*(*(*std::get<0>(r0))->getIncome(2))->getParameter(LinkParams::STREAM)) ==
                    1.0, "res[0][0] in[2] stream not 1 but ",
                    ByteCodeUtils::asDouble(*(*(*std::get<0>(r0))->getIncome(2))->getParameter(LinkParams::STREAM)));
            return 1;
        }

        int8_t compilerInAndOutTest() {
            icu::UnicodeString j = USS("single {\n in B, C; out (+ B, C)\n}");
            auto ss = std::make_shared<std::istringstream>();
            ss->str(StringConvUtils::utou8(j));
            nnvm::ByteCodeParser parser(ss, TEST_PATH, USS("single"), IntegrationTests::prepareContext());
            auto res = parser.read();

            auto r0 = res->front()->getBlocks();
            auto r1 = res->front()->getBlocks();
            auto tr1 = std::get<0>(r1);
            std::advance(tr1, 1);
            r1 = std::tuple<std::deque<std::shared_ptr<ByteCodeBlock>>::iterator, std::deque<std::shared_ptr<ByteCodeBlock>>::iterator>(
                    tr1, std::get<1>(r1));
            auto r2 = res->front()->getBlocks();
            auto tr2 = std::get<0>(r2);
            std::advance(tr2, 2);
            r2 = std::tuple<std::deque<std::shared_ptr<ByteCodeBlock>>::iterator, std::deque<std::shared_ptr<ByteCodeBlock>>::iterator>(
                    tr2, std::get<1>(r2));

            std::shared_ptr<ByteCodeParameter> p;

            fast_assert_less((bool) res, "No res");
            fast_assert(res->size() == 1, "res not 1 but ", res->size());
            fast_assert_less(std::get<0>(r0) != std::get<1>(r0), "res[0][0] no blocks");
            fast_assert_less(std::get<0>(r1) != std::get<1>(r1), "res[0][1] no blocks");
            fast_assert_less(std::get<0>(r2) != std::get<1>(r2), "res[0][2] no blocks");
            fast_assert((*std::get<0>(r0))->getName() == USS("B"), "res[0][0] not B but ",
                        StringConvUtils::utou8((*std::get<0>(r0))->getName()));
            fast_assert((*std::get<0>(r1))->getName() == USS("C"), "res[0][1] not C but ",
                        StringConvUtils::utou8((*std::get<0>(r1))->getName()));
            fast_assert((*std::get<0>(r2))->getName() == USS("plus__B__C1"), "res[0][2] not plus__B__C1 but ",
                        StringConvUtils::utou8((*std::get<0>(r2))->getName()));
            fast_assert(!(*std::get<0>(r0))->tryGetParameter(ElementParams::EXPECTED, p),
                        "res[0][0] shouldn't have Expected but ", p->value.byteValue);
            fast_assert(!(*std::get<0>(r1))->tryGetParameter(ElementParams::EXPECTED, p),
                        "res[0][1] shouldn't have Expected but ", p->value.byteValue);
            fast_assert(!(*std::get<0>(r2))->tryGetParameter(ElementParams::EXPECTED, p),
                        "res[0][2] shouldn't have Expected but ", p->value.byteValue);
            fast_assert((*std::get<0>(r0))->tryGetParameter(ElementParams::INPUT, p) && p->value.byteValue,
                        "res[0][0] Input not 1 but ", p->value.byteValue);
            fast_assert((*std::get<0>(r1))->tryGetParameter(ElementParams::INPUT, p) && p->value.byteValue,
                        "res[0][1] Input not 1 but ", p->value.byteValue);
            fast_assert(!(*std::get<0>(r2))->tryGetParameter(ElementParams::INPUT, p),
                        "res[0][2] shouldn't have Input but ", p->value.byteValue);

            auto ri = (*std::get<0>(r2))->getIncomes();
            fast_assert_less(std::get<0>(ri) != std::get<1>(ri), "res[0][2] Incomes empty");
            fast_assert((*(*std::get<0>(r2))->getIncome(0))->getOwner().lock()->getName() == USS("B"),
                        "res[0][2] in[0] not B but ",
                        StringConvUtils::utou8((*(*std::get<0>(r2))->getIncome(0))->getOwner().lock()->getName()));
            fast_assert(!(*(*std::get<0>(r2))->getIncome(0))->tryGetParameter(LinkParams::ACTIVATION, p),
                        "res[0][2] in[0] activation not 0 but ", ByteCodeUtils::asDouble(
                    *(*(*std::get<0>(r2))->getIncome(0))->getParameter(LinkParams::ACTIVATION)));
            fast_assert(
                    ByteCodeUtils::asDouble(*(*(*std::get<0>(r2))->getIncome(0))->getParameter(LinkParams::STREAM)) ==
                    1.0, "res[0][2] in[0] stream not 1 but ",
                    ByteCodeUtils::asDouble(*(*(*std::get<0>(r2))->getIncome(0))->getParameter(LinkParams::STREAM)));
            fast_assert((*(*std::get<0>(r2))->getIncome(1))->getOwner().lock()->getName() == USS("C"),
                        "res[0][2] in[1] not C but ",
                        StringConvUtils::utou8((*(*std::get<0>(r2))->getIncome(1))->getOwner().lock()->getName()));
            fast_assert(!(*(*std::get<0>(r2))->getIncome(1))->tryGetParameter(LinkParams::ACTIVATION, p),
                        "res[0][2] in[1] activation not 0 but ", ByteCodeUtils::asDouble(
                    *(*(*std::get<0>(r2))->getIncome(1))->getParameter(LinkParams::ACTIVATION)));
            fast_assert(
                    ByteCodeUtils::asDouble(*(*(*std::get<0>(r2))->getIncome(1))->getParameter(LinkParams::STREAM)) ==
                    1.0, "res[0][2] in[1] stream not 1 but ",
                    ByteCodeUtils::asDouble(*(*(*std::get<0>(r2))->getIncome(1))->getParameter(LinkParams::STREAM)));
            return 1;
        }

        int8_t compilerBlockCallTest() {
            icu::UnicodeString j = USS(
                    "called A, B {\n out (+ A, B)\n}\nsingle B, C {\n D = called A:B, B:C\n out D.plus__A__B1\n}");
            auto ss = std::make_shared<std::istringstream>();
            ss->str(StringConvUtils::utou8(j));
            nnvm::ByteCodeParser parser(ss, TEST_PATH, USS("single"), IntegrationTests::prepareContext());
            auto res = parser.read();

            auto r0 = res->front()->getBlocks();
            auto r1 = res->front()->getBlocks();
            auto tr1 = std::get<0>(r1);
            std::advance(tr1, 1);
            r1 = std::tuple<std::deque<std::shared_ptr<ByteCodeBlock>>::iterator, std::deque<std::shared_ptr<ByteCodeBlock>>::iterator>(
                    tr1, std::get<1>(r1));
            auto r2 = res->front()->getBlocks();
            auto tr2 = std::get<0>(r2);
            std::advance(tr2, 2);
            r2 = std::tuple<std::deque<std::shared_ptr<ByteCodeBlock>>::iterator, std::deque<std::shared_ptr<ByteCodeBlock>>::iterator>(
                    tr2, std::get<1>(r2));

            auto rd0 = res->back()->getBlocks();
            auto rd1 = res->back()->getBlocks();
            auto trd1 = std::get<0>(rd1);
            std::advance(trd1, 1);
            rd1 = std::tuple<std::deque<std::shared_ptr<ByteCodeBlock>>::iterator, std::deque<std::shared_ptr<ByteCodeBlock>>::iterator>(
                    trd1, std::get<1>(rd1));
            auto rd2 = res->back()->getBlocks();
            auto trd2 = std::get<0>(rd2);
            std::advance(trd2, 2);
            rd2 = std::tuple<std::deque<std::shared_ptr<ByteCodeBlock>>::iterator, std::deque<std::shared_ptr<ByteCodeBlock>>::iterator>(
                    trd2, std::get<1>(rd2));
            auto rd3 = res->back()->getBlocks();
            auto trd3 = std::get<0>(rd3);
            std::advance(trd3, 3);
            rd3 = std::tuple<std::deque<std::shared_ptr<ByteCodeBlock>>::iterator, std::deque<std::shared_ptr<ByteCodeBlock>>::iterator>(
                    trd3, std::get<1>(rd3));
            auto rd4 = res->back()->getBlocks();
            auto trd4 = std::get<0>(rd4);
            std::advance(trd4, 4);
            rd4 = std::tuple<std::deque<std::shared_ptr<ByteCodeBlock>>::iterator, std::deque<std::shared_ptr<ByteCodeBlock>>::iterator>(
                    trd4, std::get<1>(rd4));

            std::shared_ptr<ByteCodeParameter> p;

            fast_assert_less((bool) res, "No res");
            fast_assert(res->size() == 2, "res not 2 but ", res->size());
            fast_assert(res->at(0)->getName() == USS("tests.called"), "res[0] not tests.called but ",
                        StringConvUtils::utou8(res->at(0)->getName()));
            fast_assert(res->at(1)->getName() == USS("tests.single"), "res[1] not tests.single but ",
                        StringConvUtils::utou8(res->at(1)->getName()));
            fast_assert_less(std::get<0>(r0) != std::get<1>(r0), "res[0][0] no blocks");
            fast_assert_less(std::get<0>(r1) != std::get<1>(r1), "res[0][1] no blocks");
            fast_assert_less(std::get<0>(r2) != std::get<1>(r2), "res[0][2] no blocks");
            fast_assert((*std::get<0>(r0))->getName() == USS("A"), "res[0][0] not A but ",
                        StringConvUtils::utou8((*std::get<0>(r0))->getName()));
            fast_assert((*std::get<0>(r1))->getName() == USS("B"), "res[0][1] not B but ",
                        StringConvUtils::utou8((*std::get<0>(r1))->getName()));
            fast_assert((*std::get<0>(r2))->getName() == USS("plus__A__B1"), "res[0][2] not plus__A__B1 but ",
                        StringConvUtils::utou8((*std::get<0>(r2))->getName()));
            fast_assert(!(*std::get<0>(r0))->tryGetParameter(ElementParams::EXPECTED, p),
                        "res[0][0] shouldn't have Expected but ", p->value.byteValue);
            fast_assert(!(*std::get<0>(r1))->tryGetParameter(ElementParams::EXPECTED, p),
                        "res[0][1] shouldn't have Expected but ", p->value.byteValue);
            fast_assert(!(*std::get<0>(r2))->tryGetParameter(ElementParams::EXPECTED, p),
                        "res[0][2] shouldn't have Expected but ", p->value.byteValue);
            fast_assert((*std::get<0>(r0))->tryGetParameter(ElementParams::INPUT, p) && p->value.byteValue,
                        "res[0][0] Input not 1 but ", p->value.byteValue);
            fast_assert((*std::get<0>(r1))->tryGetParameter(ElementParams::INPUT, p) && p->value.byteValue,
                        "res[0][1] Input not 1 but ", p->value.byteValue);
            fast_assert(!(*std::get<0>(r2))->tryGetParameter(ElementParams::INPUT, p),
                        "res[0][2] shouldn't have Input but ", p->value.byteValue);

            auto ri = (*std::get<0>(r2))->getIncomes();
            fast_assert_less(std::get<0>(ri) != std::get<1>(ri), "res[0][2] Incomes empty");
            fast_assert((*(*std::get<0>(r2))->getIncome(0))->getOwner().lock()->getName() == USS("A"),
                        "res[0][2] in[0] not A but ",
                        StringConvUtils::utou8((*(*std::get<0>(r2))->getIncome(0))->getOwner().lock()->getName()));
            fast_assert(!(*(*std::get<0>(r2))->getIncome(0))->tryGetParameter(LinkParams::ACTIVATION, p),
                        "res[0][2] in[0] activation not 0 but ", ByteCodeUtils::asDouble(
                    *(*(*std::get<0>(r2))->getIncome(0))->getParameter(LinkParams::ACTIVATION)));
            fast_assert(
                    ByteCodeUtils::asDouble(*(*(*std::get<0>(r2))->getIncome(0))->getParameter(LinkParams::STREAM)) ==
                    1.0, "res[0][2] in[0] stream not 1 but ",
                    ByteCodeUtils::asDouble(*(*(*std::get<0>(r2))->getIncome(0))->getParameter(LinkParams::STREAM)));
            fast_assert((*(*std::get<0>(r2))->getIncome(1))->getOwner().lock()->getName() == USS("B"),
                        "res[0][2] in[1] not B but ",
                        StringConvUtils::utou8((*(*std::get<0>(r2))->getIncome(1))->getOwner().lock()->getName()));
            fast_assert(!(*(*std::get<0>(r2))->getIncome(1))->tryGetParameter(LinkParams::ACTIVATION, p),
                        "res[0][2] in[1] activation not 0 but ", ByteCodeUtils::asDouble(
                    *(*(*std::get<0>(r2))->getIncome(1))->getParameter(LinkParams::ACTIVATION)));
            fast_assert(
                    ByteCodeUtils::asDouble(*(*(*std::get<0>(r2))->getIncome(1))->getParameter(LinkParams::STREAM)) ==
                    1.0, "res[0][2] in[1] stream not 1 but ",
                    ByteCodeUtils::asDouble(*(*(*std::get<0>(r2))->getIncome(1))->getParameter(LinkParams::STREAM)));

            fast_assert_less(std::get<0>(rd0) != std::get<1>(rd0), "res[1][0] no blocks");
            fast_assert_less(std::get<0>(rd1) != std::get<1>(rd1), "res[1][1] no blocks");
            fast_assert_less(std::get<0>(rd2) != std::get<1>(rd2), "res[1][2] no blocks");
            fast_assert_less(std::get<0>(rd3) != std::get<1>(rd3), "res[1][3] no blocks");
            fast_assert_less(std::get<0>(rd4) != std::get<1>(rd4), "res[1][4] no blocks");

            auto tmp0 = StringConvUtils::utow((*std::get<0>(rd0))->getName());
            auto tmp1 = StringConvUtils::utow((*std::get<0>(rd1))->getName());
            auto tmp2 = StringConvUtils::utow((*std::get<0>(rd2))->getName());
            auto tmp3 = StringConvUtils::utow((*std::get<0>(rd3))->getName());
            auto tmp4 = StringConvUtils::utow((*std::get<0>(rd4))->getName());

            fast_assert((*std::get<0>(rd0))->getName() == USS("B"), "res[1][0] not B but ",
                        StringConvUtils::utou8((*std::get<0>(rd0))->getName()));
            fast_assert((*std::get<0>(rd1))->getName() == USS("C"), "res[1][1] not C but ",
                        StringConvUtils::utou8((*std::get<0>(rd1))->getName()));
            fast_assert((*std::get<0>(rd2))->getName() == USS("D.A"), "res[1][2] not D.A but ",
                        StringConvUtils::utou8((*std::get<0>(rd2))->getName()));
            fast_assert((*std::get<0>(rd3))->getName() == USS("D.B"), "res[1][3] not D.B but ",
                        StringConvUtils::utou8((*std::get<0>(rd3))->getName()));
            fast_assert((*std::get<0>(rd4))->getName() == USS("D.plus__A__B1"), "res[1][4] not D.plus__A__B1 but ",
                        StringConvUtils::utou8((*std::get<0>(rd4))->getName()));
            fast_assert(!(*std::get<0>(rd0))->tryGetParameter(ElementParams::EXPECTED, p),
                        "res[1][0] shouldn't have Expected but ", p->value.byteValue);
            fast_assert(!(*std::get<0>(rd1))->tryGetParameter(ElementParams::EXPECTED, p),
                        "res[1][1] shouldn't have Expected but ", p->value.byteValue);
            fast_assert(!(*std::get<0>(rd2))->tryGetParameter(ElementParams::EXPECTED, p),
                        "res[1][2] shouldn't have Expected but ", p->value.byteValue);
            fast_assert(!(*std::get<0>(rd3))->tryGetParameter(ElementParams::EXPECTED, p),
                        "res[1][3] shouldn't have Expected but ", p->value.byteValue);
            fast_assert(!(*std::get<0>(rd4))->tryGetParameter(ElementParams::EXPECTED, p),
                        "res[1][4] shouldn't have Expected but ", p->value.byteValue);
            fast_assert((*std::get<0>(rd0))->tryGetParameter(ElementParams::INPUT, p) && p->value.byteValue,
                        "res[1][0] Input not 1 but ", p->value.byteValue);
            fast_assert((*std::get<0>(rd1))->tryGetParameter(ElementParams::INPUT, p) && p->value.byteValue,
                        "res[1][1] Input not 1 but ", p->value.byteValue);
            fast_assert(!(*std::get<0>(rd2))->tryGetParameter(ElementParams::INPUT, p),
                        "res[1][2] shouldn't have Input but ", p->value.byteValue);
            fast_assert(!(*std::get<0>(rd3))->tryGetParameter(ElementParams::INPUT, p),
                        "res[1][3] shouldn't have Input but ", p->value.byteValue);
            fast_assert(!(*std::get<0>(rd4))->tryGetParameter(ElementParams::INPUT, p),
                        "res[1][4] shouldn't have Input but ", p->value.byteValue);

            auto ri2 = (*std::get<0>(rd2))->getIncomes();
            fast_assert_less(std::get<0>(ri2) != std::get<1>(ri2), "res[1][2] Incomes empty");
            fast_assert((*(*std::get<0>(rd2))->getIncome(0))->getOwner().lock()->getName() == USS("B"),
                        "res[1][2] in[0] not B but ",
                        StringConvUtils::utou8((*(*std::get<0>(rd2))->getIncome(0))->getOwner().lock()->getName()));
            fast_assert(!(*(*std::get<0>(rd2))->getIncome(0))->tryGetParameter(LinkParams::ACTIVATION, p),
                        "res[1][2] in[0] activation not 0 but ", ByteCodeUtils::asDouble(
                    *(*(*std::get<0>(rd2))->getIncome(0))->getParameter(LinkParams::ACTIVATION)));
            fast_assert(
                    ByteCodeUtils::asDouble(*(*(*std::get<0>(rd2))->getIncome(0))->getParameter(LinkParams::STREAM)) ==
                    1.0, "res[1][2] in[0] stream not 1 but ",
                    ByteCodeUtils::asDouble(*(*(*std::get<0>(rd2))->getIncome(0))->getParameter(LinkParams::STREAM)));
            auto ri3 = (*std::get<0>(rd3))->getIncomes();
            fast_assert_less(std::get<0>(ri3) != std::get<1>(ri3), "res[1][3] Incomes empty");
            fast_assert((*(*std::get<0>(rd3))->getIncome(0))->getOwner().lock()->getName() == USS("C"),
                        "res[1][3] in[0] not C but ",
                        StringConvUtils::utou8((*(*std::get<0>(rd3))->getIncome(0))->getOwner().lock()->getName()));
            fast_assert(!(*(*std::get<0>(rd3))->getIncome(0))->tryGetParameter(LinkParams::ACTIVATION, p),
                        "res[1][3] in[0] activation not 0 but ", ByteCodeUtils::asDouble(
                    *(*(*std::get<0>(rd3))->getIncome(0))->getParameter(LinkParams::ACTIVATION)));
            fast_assert(
                    ByteCodeUtils::asDouble(*(*(*std::get<0>(rd3))->getIncome(0))->getParameter(LinkParams::STREAM)) ==
                    1.0, "res[1][3] in[0] stream not 1 but ",
                    ByteCodeUtils::asDouble(*(*(*std::get<0>(rd3))->getIncome(0))->getParameter(LinkParams::STREAM)));
            auto ri4 = (*std::get<0>(rd4))->getIncomes();
            fast_assert_less(std::get<0>(ri4) != std::get<1>(ri4), "res[1][4] Incomes empty");
            fast_assert((*(*std::get<0>(rd4))->getIncome(0))->getOwner().lock()->getName() == USS("D.A"),
                        "res[1][4] in[0] not D.A but ",
                        StringConvUtils::utou8((*(*std::get<0>(rd4))->getIncome(0))->getOwner().lock()->getName()));
            fast_assert(!(*(*std::get<0>(rd4))->getIncome(0))->tryGetParameter(LinkParams::ACTIVATION, p),
                        "res[1][4] in[0] activation not 0 but ", ByteCodeUtils::asDouble(
                    *(*(*std::get<0>(rd4))->getIncome(0))->getParameter(LinkParams::ACTIVATION)));
            fast_assert(
                    ByteCodeUtils::asDouble(*(*(*std::get<0>(rd4))->getIncome(0))->getParameter(LinkParams::STREAM)) ==
                    1.0, "res[1][4] in[0] stream not 1 but ",
                    ByteCodeUtils::asDouble(*(*(*std::get<0>(rd4))->getIncome(0))->getParameter(LinkParams::STREAM)));
            fast_assert((*(*std::get<0>(rd4))->getIncome(1))->getOwner().lock()->getName() == USS("D.B"),
                        "res[1][4] in[1] not D.B but ",
                        StringConvUtils::utou8((*(*std::get<0>(rd4))->getIncome(1))->getOwner().lock()->getName()));
            fast_assert(!(*(*std::get<0>(rd4))->getIncome(1))->tryGetParameter(LinkParams::ACTIVATION, p),
                        "res[1][4] in[1] activation not 0 but ", ByteCodeUtils::asDouble(
                    *(*(*std::get<0>(rd4))->getIncome(1))->getParameter(LinkParams::ACTIVATION)));
            fast_assert(
                    ByteCodeUtils::asDouble(*(*(*std::get<0>(rd4))->getIncome(1))->getParameter(LinkParams::STREAM)) ==
                    1.0, "res[1][4] in[1] stream not 1 but ",
                    ByteCodeUtils::asDouble(*(*(*std::get<0>(rd4))->getIncome(1))->getParameter(LinkParams::STREAM)));

            return 1;
        }

        int8_t compilerBlockCallTestJsonCheck() {
            icu::UnicodeString j = USS(
                    "called A, B {\n out (+ A, B)\n}\nsingle B, C {\n D = called A:B, B:C\n out D.plus__A__B1\n}");
            auto ss = std::make_shared<std::istringstream>();
            ss->str(StringConvUtils::utou8(j));
            nnvm::ByteCodeParser parser(ss, TEST_PATH, USS("single"), IntegrationTests::prepareContext());
            auto res = parser.read();

            std::vector<icu::UnicodeString> expected{
                    USS("{\"packageName\": \"tests.called\", \"blocks\": ["
                                "{\"blockName\": \"A\", \"type\": \"additive\", \"parameters\": {\"i\": {\"type\": 0, \"value\": 1}, \"n\": {\"type\": 1, \"value\": 1}}, \"items\": ["
                                "{\"itemName\": \"plus__A__B1\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"B\", \"type\": \"additive\", \"parameters\": {\"i\": {\"type\": 0, \"value\": 1}, \"n\": {\"type\": 1, \"value\": 2}}, \"items\": ["
                                "{\"itemName\": \"plus__A__B1\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"plus__A__B1\", \"type\": \"additive\", \"parameters\": {\"o\": {\"type\": 0, \"value\": 1}}, \"items\": []}]}"),
                    USS("{\"packageName\": \"tests.single\", \"blocks\": ["
                                "{\"blockName\": \"B\", \"type\": \"additive\", \"parameters\": {\"i\": {\"type\": 0, \"value\": 1}, \"n\": {\"type\": 1, \"value\": 1}}, \"items\": ["
                                "{\"itemName\": \"D.A\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"C\", \"type\": \"additive\", \"parameters\": {\"i\": {\"type\": 0, \"value\": 1}, \"n\": {\"type\": 1, \"value\": 2}}, \"items\": ["
                                "{\"itemName\": \"D.B\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"D.A\", \"type\": \"additive\", \"parameters\": {}, \"items\": ["
                                "{\"itemName\": \"D.plus__A__B1\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"D.B\", \"type\": \"additive\", \"parameters\": {}, \"items\": ["
                                "{\"itemName\": \"D.plus__A__B1\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"D.plus__A__B1\", \"type\": \"additive\", \"parameters\": {\"o\": {\"type\": 0, \"value\": 1}}, \"items\": []}]}")};

            auto i = res->begin();
            auto l = expected.begin();
            for (; i != res->end() && l != expected.end(); ++i, ++l) {
                auto os = std::make_shared<std::ostringstream>();
                ByteCodeWriter bw(os, IntegrationTests::prepareContext());
                if (bw.writeJson(*i)) {
                    return 0;
                }
                auto s = os->str();
                fast_assert(StringConvUtils::u8tou(s) == *l,
                            StringUtils::spf(std::string("Expected: {0}\nActual: "), StringConvUtils::utou8(*l)), s)
            }

            return 1;
        }

        int8_t compilerBlockCallSingleOutputTest() {
            icu::UnicodeString j = USS(
                    "called A, B {\n out (+ A, B)\n}\nsingle B, C {\n D = called A:B, B:C\n out D\n}");
            auto ss = std::make_shared<std::istringstream>();
            ss->str(StringConvUtils::utou8(j));
            nnvm::ByteCodeParser parser(ss, TEST_PATH, USS("single"), IntegrationTests::prepareContext());
            auto res = parser.read();

            std::vector<icu::UnicodeString> expected{
                    USS("{\"packageName\": \"tests.called\", \"blocks\": ["
                                "{\"blockName\": \"A\", \"type\": \"additive\", \"parameters\": {\"i\": {\"type\": 0, \"value\": 1}, \"n\": {\"type\": 1, \"value\": 1}}, \"items\": ["
                                "{\"itemName\": \"plus__A__B1\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"B\", \"type\": \"additive\", \"parameters\": {\"i\": {\"type\": 0, \"value\": 1}, \"n\": {\"type\": 1, \"value\": 2}}, \"items\": ["
                                "{\"itemName\": \"plus__A__B1\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"plus__A__B1\", \"type\": \"additive\", \"parameters\": {\"o\": {\"type\": 0, \"value\": 1}}, \"items\": []}]}"),
                    USS("{\"packageName\": \"tests.single\", \"blocks\": ["
                                "{\"blockName\": \"B\", \"type\": \"additive\", \"parameters\": {\"i\": {\"type\": 0, \"value\": 1}, \"n\": {\"type\": 1, \"value\": 1}}, \"items\": ["
                                "{\"itemName\": \"D.A\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"C\", \"type\": \"additive\", \"parameters\": {\"i\": {\"type\": 0, \"value\": 1}, \"n\": {\"type\": 1, \"value\": 2}}, \"items\": ["
                                "{\"itemName\": \"D.B\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"D.A\", \"type\": \"additive\", \"parameters\": {}, \"items\": ["
                                "{\"itemName\": \"D.plus__A__B1\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"D.B\", \"type\": \"additive\", \"parameters\": {}, \"items\": ["
                                "{\"itemName\": \"D.plus__A__B1\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"D.plus__A__B1\", \"type\": \"additive\", \"parameters\": {\"o\": {\"type\": 0, \"value\": 1}}, \"items\": []}]}")};

            auto i = res->begin();
            auto l = expected.begin();
            for (; i != res->end() && l != expected.end(); ++i, ++l) {
                auto os = std::make_shared<std::ostringstream>();
                ByteCodeWriter bw(os, IntegrationTests::prepareContext());
                if (bw.writeJson(*i)) {
                    return 0;
                }
                auto s = os->str();
                fast_assert(StringConvUtils::u8tou(s) == *l,
                            StringUtils::spf(std::string("Expected: {0}\nActual: "), StringConvUtils::utou8(*l)), s)
            }

            return 1;
        }

        int8_t compilerBlockCallSingleOutputInExpressionTest() {
            icu::UnicodeString j = USS(
                    "called A, B {\n out (+ A, B)\n}\nsingle B, C {\n D = called B:C, A:B\n E = + D, 1\n out E\n}");
            auto ss = std::make_shared<std::istringstream>();
            ss->str(StringConvUtils::utou8(j));
            nnvm::ByteCodeParser parser(ss, TEST_PATH, USS("single"), IntegrationTests::prepareContext());
            auto res = parser.read();

            std::vector<icu::UnicodeString> expected{
                    USS("{\"packageName\": \"tests.called\", \"blocks\": ["
                                "{\"blockName\": \"A\", \"type\": \"additive\", \"parameters\": {\"i\": {\"type\": 0, \"value\": 1}, \"n\": {\"type\": 1, \"value\": 1}}, \"items\": ["
                                "{\"itemName\": \"plus__A__B1\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"B\", \"type\": \"additive\", \"parameters\": {\"i\": {\"type\": 0, \"value\": 1}, \"n\": {\"type\": 1, \"value\": 2}}, \"items\": ["
                                "{\"itemName\": \"plus__A__B1\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"plus__A__B1\", \"type\": \"additive\", \"parameters\": {\"o\": {\"type\": 0, \"value\": 1}}, \"items\": []}]}"),
                    USS("{\"packageName\": \"tests.single\", \"blocks\": ["
                                "{\"blockName\": \"B\", \"type\": \"additive\", \"parameters\": {\"i\": {\"type\": 0, \"value\": 1}, \"n\": {\"type\": 1, \"value\": 1}}, \"items\": ["
                                "{\"itemName\": \"D.A\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"C\", \"type\": \"additive\", \"parameters\": {\"i\": {\"type\": 0, \"value\": 1}, \"n\": {\"type\": 1, \"value\": 2}}, \"items\": ["
                                "{\"itemName\": \"D.B\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"D.A\", \"type\": \"additive\", \"parameters\": {}, \"items\": ["
                                "{\"itemName\": \"D.plus__A__B1\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"D.B\", \"type\": \"additive\", \"parameters\": {}, \"items\": ["
                                "{\"itemName\": \"D.plus__A__B1\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"D.plus__A__B1\", \"type\": \"additive\", \"parameters\": {}, \"items\": ["
                                "{\"itemName\": \"E\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"E\", \"type\": \"additive\", \"parameters\": {\"o\": {\"type\": 0, \"value\": 1}}, \"items\": []}, "
                                "{\"blockName\": \"E.const1.000000\", \"type\": \"additive\", \"parameters\": {\"b\": {\"type\": 2, \"value\": 1}}, \"items\": ["
                                "{\"itemName\": \"E\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}]}")};

            auto i = res->begin();
            auto l = expected.begin();
            for (; i != res->end() && l != expected.end(); ++i, ++l) {
                auto os = std::make_shared<std::ostringstream>();
                ByteCodeWriter bw(os, IntegrationTests::prepareContext());
                if (bw.writeJson(*i)) {
                    return 0;
                }
                auto s = os->str();
                fast_assert(StringConvUtils::u8tou(s) == *l,
                            StringUtils::spf(std::string("Expected: {0}\nActual: "), StringConvUtils::utou8(*l)), s)
            }

            return 1;
        }

        int8_t compilerFactorialTest() {
            icu::UnicodeString j = USS("factorial N {\n"
                                               " COUNTER = # ((+ N, COUNTER) >= 1), (+ N, COUNTER, (-1))\n"
                                               " ACC = & ACC, COUNTER\n"
                                               " FLUSH = # ((-COUNTER) >= 0), (-1)\n"
                                               " RES = # (FLUSH >= 0), ACC\n"
                                               " out RES\n"
                                               "}\n"
                                               "network N {\n"
                                               " D = factorial N:N\n"
                                               " out D\n"
                                               "}");
            auto ss = std::make_shared<std::istringstream>();
            ss->str(StringConvUtils::utou8(j));
            nnvm::ByteCodeParser parser(ss, TEST_PATH, USS("single"), IntegrationTests::prepareContext());
            auto res = parser.read();

            std::vector<icu::UnicodeString> expected{
                    USS("{\"packageName\": \"tests.factorial\", \"blocks\": ["
                                "{\"blockName\": \"ACC\", \"type\": \"multiplicative\", \"parameters\": {}, \"items\": ["
                                "{\"itemName\": \"ACC\", \"parameters\": {\"a\": {\"type\": 0, \"value\": 1}}}, "
                                "{\"itemName\": \"RES\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"COUNTER\", \"type\": \"additive\", \"parameters\": {\"p\": {\"type\": 2, \"value\": 1}}, \"items\": ["
                                "{\"itemName\": \"COUNTER\", \"parameters\": {\"a\": {\"type\": 0, \"value\": 1}, \"s\": {\"type\": 2, \"value\": 1}}}, "
                                "{\"itemName\": \"ACC\", \"parameters\": {\"s\": {\"type\": 0, \"value\": 1}}}, "
                                "{\"itemName\": \"FLUSH\", \"parameters\": {\"a\": {\"type\": 2, \"value\": -1}}}]}, "
                                "{\"blockName\": \"COUNTER.const_1.000000\", \"type\": \"additive\", \"parameters\": {\"b\": {\"type\": 2, \"value\": -1}}, \"items\": ["
                                "{\"itemName\": \"COUNTER\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"FLUSH\", \"type\": \"additive\", \"parameters\": {}, \"items\": ["
                                "{\"itemName\": \"RES\", \"parameters\": {\"a\": {\"type\": 0, \"value\": 1}}}]}, "
                                "{\"blockName\": \"FLUSH.const_1.000000\", \"type\": \"additive\", \"parameters\": {\"b\": {\"type\": 2, \"value\": -1}}, \"items\": ["
                                "{\"itemName\": \"FLUSH\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"N\", \"type\": \"additive\", \"parameters\": {\"i\": {\"type\": 0, \"value\": 1}, \"n\": {\"type\": 1, \"value\": 1}}, \"items\": ["
                                "{\"itemName\": \"COUNTER\", \"parameters\": {\"a\": {\"type\": 0, \"value\": 1}, \"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"RES\", \"type\": \"additive\", \"parameters\": {\"o\": {\"type\": 0, \"value\": 1}}, \"items\": []}]}"),

                    USS("{\"packageName\": \"tests.network\", \"blocks\": ["
                                "{\"blockName\": \"D.ACC\", \"type\": \"multiplicative\", \"parameters\": {}, \"items\": ["
                                "{\"itemName\": \"D.ACC\", \"parameters\": {\"a\": {\"type\": 0, \"value\": 1}}}, "
                                "{\"itemName\": \"D.RES\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"D.COUNTER\", \"type\": \"additive\", \"parameters\": {\"p\": {\"type\": 2, \"value\": 1}}, \"items\": ["
                                "{\"itemName\": \"D.COUNTER\", \"parameters\": {\"a\": {\"type\": 0, \"value\": 1}, \"s\": {\"type\": 2, \"value\": 1}}}, "
                                "{\"itemName\": \"D.ACC\", \"parameters\": {\"s\": {\"type\": 0, \"value\": 1}}}, "
                                "{\"itemName\": \"D.FLUSH\", \"parameters\": {\"a\": {\"type\": 2, \"value\": -1}}}]}, "
                                "{\"blockName\": \"D.COUNTER.const_1.000000\", \"type\": \"additive\", \"parameters\": {\"b\": {\"type\": 2, \"value\": -1}}, \"items\": ["
                                "{\"itemName\": \"D.COUNTER\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"D.FLUSH\", \"type\": \"additive\", \"parameters\": {}, \"items\": ["
                                "{\"itemName\": \"D.RES\", \"parameters\": {\"a\": {\"type\": 0, \"value\": 1}}}]}, "
                                "{\"blockName\": \"D.FLUSH.const_1.000000\", \"type\": \"additive\", \"parameters\": {\"b\": {\"type\": 2, \"value\": -1}}, \"items\": ["
                                "{\"itemName\": \"D.FLUSH\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"D.N\", \"type\": \"additive\", \"parameters\": {}, \"items\": ["
                                "{\"itemName\": \"D.COUNTER\", \"parameters\": {\"a\": {\"type\": 0, \"value\": 1}, \"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"D.RES\", \"type\": \"additive\", \"parameters\": {\"o\": {\"type\": 0, \"value\": 1}}, \"items\": []}, "
                                "{\"blockName\": \"N\", \"type\": \"additive\", \"parameters\": {\"i\": {\"type\": 0, \"value\": 1}, \"n\": {\"type\": 1, \"value\": 1}}, \"items\": ["
                                "{\"itemName\": \"D.N\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}]}")};

            auto i = res->begin();
            auto l = expected.begin();
            for (; i != res->end() && l != expected.end(); ++i, ++l) {
                auto os = std::make_shared<std::ostringstream>();
                ByteCodeWriter bw(os, IntegrationTests::prepareContext());
                if (bw.writeJson(*i)) {
                    return 0;
                }
                auto s = os->str();
                fast_assert(StringConvUtils::u8tou(s) == *l,
                            StringUtils::spf(std::string("Expected: {0}\nActual: "), StringConvUtils::utou8(*l)), s)
            }

            return 1;
        }

        int8_t compilerGeneratorDefinitionTest() {
            icu::UnicodeString j = USS("COUNTER[I] = # ((* 2, A) >= I), 1");

            int64_t i = 0;
            auto ctx = IntegrationTests::prepareContext(i);
            DefinitionBuilder d(ctx);
            LanguageDefinition ld(ctx->byteLoader);
            ld.templateNames.emplace_back(USS("I"));
            std::deque<LanguageDefinition> c;
            std::map<icu::UnicodeString, std::tuple<int64_t, int64_t>> ranges;
            auto res = d.parseDefinition(j, ld, c, ranges);

            fast_assert(res, "Res is not 1: ", res);
            fast_assert(ld.name && *ld.name == USS("COUNTER[I]"), "Definition is not COUNTER[I]: ",
                        StringConvUtils::utou8(*ld.name));
            fast_assert(!ld.reduction, "Definition reduction is not 0: ", ld.reduction);
            fast_assert(ld.operand.call->blockName == NeuronNames::ADDITIVE, "Definition is not additive: ",
                        StringConvUtils::utou8(ld.operand.call->blockName));
            fast_assert(ld.operand.call->arguments.size() == 2, "Definition with not 2 arguments: ",
                        ld.operand.call->arguments.size());
            fast_assert(!ld.operand.call->arguments[1].operand.expression &&
                        ld.operand.call->arguments[1].operand.literal &&
                        ld.operand.call->arguments[1].operand.literal->isOf(TokenKind::NUMBER) &&
                        ld.operand.call->arguments[1].operand.literal->value == USS("1"),
                        "Definition #2 argument is not number 1: ", ld.operand.call->arguments[1].operand.literal
                                                                    ? ld.operand.call->arguments[1].operand.literal->value
                                                                    : USS("NULL"));
            fast_assert(!ld.operand.call->arguments[0].operand.literal &&
                        ld.operand.call->arguments[0].operand.expression &&
                        ld.operand.call->arguments[0].operand.expression->middle == USS(">=") &&
                        ld.operand.call->arguments[0].operand.expression->right->literal &&
                        ld.operand.call->arguments[0].operand.expression->right->literal->value == USS("I"),
                        "Definition #1 argument has not I on the right: ",
                        StringConvUtils::utou8(LanguageModel::toString(*ld.operand.call->arguments[0].operand.expression)));

            return 1;
        }

        int8_t compilerGeneratorTest() {
            icu::UnicodeString j = USS("network I {\n"
                                               " [I] COUNTER[I] = # (A >= I), 1\n"
                                               " A = + A, 1\n"
                                               " [I] RES = # (0 >= 0), COUNTER[I]\n"
                                               " out RES\n"
                                               "}");
            auto ss = std::make_shared<std::istringstream>();
            ss->str(StringConvUtils::utou8(j));
            nnvm::ByteCodeParser parser(ss, TEST_PATH, USS("single"), IntegrationTests::prepareContext());
            auto res = parser.read();

            std::vector<icu::UnicodeString> expected{
                    USS("{\"packageName\": \"tests.network\", \"blocks\": ["
                                "{\"blockName\": \"A\", \"type\": \"additive\", \"parameters\": {}, \"items\": ["
                                "{\"itemName\": \"COUNTER[I]\", \"parameters\": {\"a\": {\"type\": 0, \"value\": 1}}}, "
                                "{\"itemName\": \"A\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"A.const1.000000\", \"type\": \"additive\", \"parameters\": {\"b\": {\"type\": 2, \"value\": 1}}, \"items\": ["
                                "{\"itemName\": \"A\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"COUNTER[I]\", \"type\": \"additive\", \"parameters\": {\"p\": {\"type\": 3, \"value\": \"I\"}, \"t\": {\"type\": 0, \"value\": 1}}, \"items\": ["
                                "{\"itemName\": \"RES\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"COUNTER[I].const1.000000\", \"type\": \"additive\", \"parameters\": {\"b\": {\"type\": 2, \"value\": 1}, \"t\": {\"type\": 0, \"value\": 1}}, \"items\": ["
                                "{\"itemName\": \"COUNTER[I]\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"I\", \"type\": \"additive\", \"parameters\": {\"i\": {\"type\": 0, \"value\": 1}, \"n\": {\"type\": 1, \"value\": 1}}, \"items\": ["
                                "{\"itemName\": \"COUNTER[I]\", \"parameters\": {\"g\": {\"type\": 0, \"value\": 1}}}, "
                                "{\"itemName\": \"COUNTER[I].const1.000000\", \"parameters\": {\"g\": {\"type\": 0, \"value\": 1}}}]}, "
                                "{\"blockName\": \"RES\", \"type\": \"additive\", \"parameters\": {\"o\": {\"type\": 0, \"value\": 1}}, \"items\": []}]}")};

            auto i = res->begin();
            auto l = expected.begin();
            for (; i != res->end() && l != expected.end(); ++i, ++l) {
                auto os = std::make_shared<std::ostringstream>();
                ByteCodeWriter bw(os, IntegrationTests::prepareContext());
                if (bw.writeJson(*i)) {
                    return 0;
                }
                auto s = os->str();
                fast_assert(StringConvUtils::u8tou(s) == *l,
                            StringUtils::spf(std::string("Expected: {0}\nActual: "), StringConvUtils::utou8(*l)), s)
            }

            return 1;
        }

        int8_t compilerStaticGeneratorTest() {
            icu::UnicodeString j = USS("network [i=8:10] B[i], I {\n"
                                               " A[i] = # (A[i] >= I), i\n"
                                               " RES += # A[i]\n"
                                               " out RES\n"
                                               "}");
            auto ss = std::make_shared<std::istringstream>();
            ss->str(StringConvUtils::utou8(j));
            nnvm::ByteCodeParser parser(ss, TEST_PATH, USS("single"), IntegrationTests::prepareContext());
            auto res = parser.read();

            std::vector<icu::UnicodeString> expected{
                    USS("{\"packageName\": \"tests.network_i_8_10\", \"blocks\": ["
                                "{\"blockName\": \"A[10]\", \"type\": \"additive\", \"parameters\": {}, \"items\": ["
                                "{\"itemName\": \"A[10]\", \"parameters\": {\"a\": {\"type\": 0, \"value\": 1}}}, "
                                "{\"itemName\": \"RES_\\u00A7i[10]\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"A[10].const10.000000\", \"type\": \"additive\", \"parameters\": {\"b\": {\"type\": 2, \"value\": 10}}, \"items\": ["
                                "{\"itemName\": \"A[10]\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"A[8]\", \"type\": \"additive\", \"parameters\": {}, \"items\": ["
                                "{\"itemName\": \"A[8]\", \"parameters\": {\"a\": {\"type\": 0, \"value\": 1}}}, "
                                "{\"itemName\": \"RES_\\u00A7i[8]\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"A[8].const8.000000\", \"type\": \"additive\", \"parameters\": {\"b\": {\"type\": 2, \"value\": 8}}, \"items\": ["
                                "{\"itemName\": \"A[8]\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"A[9]\", \"type\": \"additive\", \"parameters\": {}, \"items\": ["
                                "{\"itemName\": \"A[9]\", \"parameters\": {\"a\": {\"type\": 0, \"value\": 1}}}, "
                                "{\"itemName\": \"RES_\\u00A7i[9]\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"A[9].const9.000000\", \"type\": \"additive\", \"parameters\": {\"b\": {\"type\": 2, \"value\": 9}}, \"items\": ["
                                "{\"itemName\": \"A[9]\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"B[10]\", \"type\": \"additive\", \"parameters\": {\"i\": {\"type\": 0, \"value\": 1}, \"n\": {\"type\": 1, \"value\": 3}}, \"items\": []}, "
                                "{\"blockName\": \"B[8]\", \"type\": \"additive\", \"parameters\": {\"i\": {\"type\": 0, \"value\": 1}, \"n\": {\"type\": 1, \"value\": 1}}, \"items\": []}, "
                                "{\"blockName\": \"B[9]\", \"type\": \"additive\", \"parameters\": {\"i\": {\"type\": 0, \"value\": 1}, \"n\": {\"type\": 1, \"value\": 2}}, \"items\": []}, "
                                "{\"blockName\": \"I\", \"type\": \"additive\", \"parameters\": {\"i\": {\"type\": 0, \"value\": 1}, \"n\": {\"type\": 1, \"value\": 4}}, \"items\": ["
                                "{\"itemName\": \"A[8]\", \"parameters\": {\"a\": {\"type\": 2, \"value\": -1}}}, "
                                "{\"itemName\": \"A[9]\", \"parameters\": {\"a\": {\"type\": 2, \"value\": -1}}}, "
                                "{\"itemName\": \"A[10]\", \"parameters\": {\"a\": {\"type\": 2, \"value\": -1}}}]}, "
                                "{\"blockName\": \"RES\", \"type\": \"additive\", \"parameters\": {\"o\": {\"type\": 0, \"value\": 1}}, \"items\": []}, "
                                "{\"blockName\": \"RES_\\u00A7i[10]\", \"type\": \"additive\", \"parameters\": {}, \"items\": ["
                                "{\"itemName\": \"RES\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"RES_\\u00A7i[8]\", \"type\": \"additive\", \"parameters\": {}, \"items\": ["
                                "{\"itemName\": \"RES\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"RES_\\u00A7i[9]\", \"type\": \"additive\", \"parameters\": {}, \"items\": ["
                                "{\"itemName\": \"RES\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}]}")};

            auto i = res->begin();
            auto l = expected.begin();
            for (; i != res->end() && l != expected.end(); ++i, ++l) {
                auto os = std::make_shared<std::ostringstream>();
                ByteCodeWriter bw(os, IntegrationTests::prepareContext());
                if (bw.writeJson(*i)) {
                    return 0;
                }
                auto s = os->str();
                fast_assert(StringConvUtils::u8tou(s) == *l,
                            StringUtils::spf(std::string("Expected: {0}\nActual: "), StringConvUtils::utou8(*l)), s)
            }

            return 1;
        }

        int8_t compilerDoubleStaticGeneratorTest() {
            icu::UnicodeString j = USS("network [i=8:10, k=1:2] B[i], I {\n"
                                               " A[i] = # (A[i] >= I), k\n"
                                               " RES += # A[i]\n"
                                               " out RES\n"
                                               "}");
            auto ss = std::make_shared<std::istringstream>();
            ss->str(StringConvUtils::utou8(j));
            nnvm::ByteCodeParser parser(ss, TEST_PATH, USS("single"), IntegrationTests::prepareContext());
            auto res = parser.read();

            std::vector<icu::UnicodeString> expected{
                    USS("{\"packageName\": \"tests.network_i_8_10_k_1_2\", \"blocks\": ["
                                "{\"blockName\": \"A[10]\", \"type\": \"additive\", \"parameters\": {}, \"items\": ["
                                "{\"itemName\": \"A[10]\", \"parameters\": {\"a\": {\"type\": 0, \"value\": 1}}}, "
                                "{\"itemName\": \"RES_\\u00A7i[10]\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"A[10].const3.000000\", \"type\": \"additive\", \"parameters\": {\"b\": {\"type\": 2, \"value\": 3}}, \"items\": ["
                                "{\"itemName\": \"A[10]\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"A[8]\", \"type\": \"additive\", \"parameters\": {}, \"items\": ["
                                "{\"itemName\": \"A[8]\", \"parameters\": {\"a\": {\"type\": 0, \"value\": 1}}}, "
                                "{\"itemName\": \"RES_\\u00A7i[8]\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"A[8].const3.000000\", \"type\": \"additive\", \"parameters\": {\"b\": {\"type\": 2, \"value\": 3}}, \"items\": ["
                                "{\"itemName\": \"A[8]\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"A[9]\", \"type\": \"additive\", \"parameters\": {}, \"items\": ["
                                "{\"itemName\": \"A[9]\", \"parameters\": {\"a\": {\"type\": 0, \"value\": 1}}}, "
                                "{\"itemName\": \"RES_\\u00A7i[9]\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"A[9].const3.000000\", \"type\": \"additive\", \"parameters\": {\"b\": {\"type\": 2, \"value\": 3}}, \"items\": ["
                                "{\"itemName\": \"A[9]\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"B[10]\", \"type\": \"additive\", \"parameters\": {\"i\": {\"type\": 0, \"value\": 1}, \"n\": {\"type\": 1, \"value\": 3}}, \"items\": []}, "
                                "{\"blockName\": \"B[8]\", \"type\": \"additive\", \"parameters\": {\"i\": {\"type\": 0, \"value\": 1}, \"n\": {\"type\": 1, \"value\": 1}}, \"items\": []}, "
                                "{\"blockName\": \"B[9]\", \"type\": \"additive\", \"parameters\": {\"i\": {\"type\": 0, \"value\": 1}, \"n\": {\"type\": 1, \"value\": 2}}, \"items\": []}, "
                                "{\"blockName\": \"I\", \"type\": \"additive\", \"parameters\": {\"i\": {\"type\": 0, \"value\": 1}, \"n\": {\"type\": 1, \"value\": 4}}, \"items\": ["
                                "{\"itemName\": \"A[8]\", \"parameters\": {\"a\": {\"type\": 2, \"value\": -1}}}, "
                                "{\"itemName\": \"A[9]\", \"parameters\": {\"a\": {\"type\": 2, \"value\": -1}}}, "
                                "{\"itemName\": \"A[10]\", \"parameters\": {\"a\": {\"type\": 2, \"value\": -1}}}]}, "
                                "{\"blockName\": \"RES\", \"type\": \"additive\", \"parameters\": {\"o\": {\"type\": 0, \"value\": 1}}, \"items\": []}, "
                                "{\"blockName\": \"RES_\\u00A7i[10]\", \"type\": \"additive\", \"parameters\": {}, \"items\": ["
                                "{\"itemName\": \"RES\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"RES_\\u00A7i[8]\", \"type\": \"additive\", \"parameters\": {}, \"items\": ["
                                "{\"itemName\": \"RES\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"RES_\\u00A7i[9]\", \"type\": \"additive\", \"parameters\": {}, \"items\": ["
                                "{\"itemName\": \"RES\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}]}")};

            auto i = res->begin();
            auto l = expected.begin();
            for (; i != res->end() && l != expected.end(); ++i, ++l) {
                auto os = std::make_shared<std::ostringstream>();
                ByteCodeWriter bw(os, IntegrationTests::prepareContext());
                if (bw.writeJson(*i)) {
                    return 0;
                }
                auto s = os->str();
                fast_assert(StringConvUtils::u8tou(s) == *l,
                            StringUtils::spf(std::string("Expected: {0}\nActual: "), StringConvUtils::utou8(*l)), s)
            }

            return 1;
        }

        int8_t compilerConditionalStaticGeneratorTest() {
            icu::UnicodeString j = USS("network [i=8:10, k=8:10, i<k] B[i], I {\n"
                                               " A[i] = # (A[i] >= I), k\n"
                                               " RES += # A[i]\n"
                                               " out RES\n"
                                               "}");
            auto ss = std::make_shared<std::istringstream>();
            ss->str(StringConvUtils::utou8(j));
            nnvm::ByteCodeParser parser(ss, TEST_PATH, USS("single"), IntegrationTests::prepareContext());
            auto res = parser.read();

            std::vector<icu::UnicodeString> expected{
                    USS("{\"packageName\": \"tests.network_i_8_10_k_8_10\", \"blocks\": ["
                                "{\"blockName\": \"A[8]\", \"type\": \"additive\", \"parameters\": {}, \"items\": ["
                                "{\"itemName\": \"A[8]\", \"parameters\": {\"a\": {\"type\": 0, \"value\": 1}}}, "
                                "{\"itemName\": \"RES_\\u00A7i[8]\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"A[8].const19.000000\", \"type\": \"additive\", \"parameters\": {\"b\": {\"type\": 2, \"value\": 19}}, \"items\": ["
                                "{\"itemName\": \"A[8]\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"A[9]\", \"type\": \"additive\", \"parameters\": {}, \"items\": ["
                                "{\"itemName\": \"A[9]\", \"parameters\": {\"a\": {\"type\": 0, \"value\": 1}}}, "
                                "{\"itemName\": \"RES_\\u00A7i[9]\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"A[9].const10.000000\", \"type\": \"additive\", \"parameters\": {\"b\": {\"type\": 2, \"value\": 10}}, \"items\": ["
                                "{\"itemName\": \"A[9]\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"B[8]\", \"type\": \"additive\", \"parameters\": {\"i\": {\"type\": 0, \"value\": 1}, \"n\": {\"type\": 1, \"value\": 1}}, \"items\": []}, "
                                "{\"blockName\": \"B[9]\", \"type\": \"additive\", \"parameters\": {\"i\": {\"type\": 0, \"value\": 1}, \"n\": {\"type\": 1, \"value\": 2}}, \"items\": []}, "
                                "{\"blockName\": \"I\", \"type\": \"additive\", \"parameters\": {\"i\": {\"type\": 0, \"value\": 1}, \"n\": {\"type\": 1, \"value\": 3}}, \"items\": ["
                                "{\"itemName\": \"A[8]\", \"parameters\": {\"a\": {\"type\": 2, \"value\": -1}}}, "
                                "{\"itemName\": \"A[9]\", \"parameters\": {\"a\": {\"type\": 2, \"value\": -1}}}]}, "
                                "{\"blockName\": \"RES\", \"type\": \"additive\", \"parameters\": {\"o\": {\"type\": 0, \"value\": 1}}, \"items\": []}, "
                                "{\"blockName\": \"RES_\\u00A7i[8]\", \"type\": \"additive\", \"parameters\": {}, \"items\": ["
                                "{\"itemName\": \"RES\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"RES_\\u00A7i[9]\", \"type\": \"additive\", \"parameters\": {}, \"items\": ["
                                "{\"itemName\": \"RES\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}]}")};

            auto i = res->begin();
            auto l = expected.begin();
            for (; i != res->end() && l != expected.end(); ++i, ++l) {
                auto os = std::make_shared<std::ostringstream>();
                ByteCodeWriter bw(os, IntegrationTests::prepareContext());
                if (bw.writeJson(*i)) {
                    return 0;
                }
                auto s = os->str();
                fast_assert(StringConvUtils::u8tou(s) == *l,
                            StringUtils::spf(std::string("Expected: {0}\nActual: "), StringConvUtils::utou8(*l)), s)
            }

            return 1;
        }

        int8_t compilerContextStaticGeneratorTest() {
            icu::UnicodeString j = USS("[LEN : 10]\n"
                                               "network [i=8:LEN, k=8:LEN, i<k] B[i], I {\n"
                                               " A[i] = # (A[i] >= I), k\n"
                                               " RES += # A[i]\n"
                                               " out RES\n"
                                               "}");
            auto ss = std::make_shared<std::istringstream>();
            ss->str(StringConvUtils::utou8(j));
            nnvm::ByteCodeParser parser(ss, TEST_PATH, USS("single"), IntegrationTests::prepareContext());
            auto res = parser.read();

            std::vector<icu::UnicodeString> expected{
                    USS("{\"packageName\": \"tests.network_i_8_10_k_8_10\", \"blocks\": ["
                                "{\"blockName\": \"A[8]\", \"type\": \"additive\", \"parameters\": {}, \"items\": ["
                                "{\"itemName\": \"A[8]\", \"parameters\": {\"a\": {\"type\": 0, \"value\": 1}}}, "
                                "{\"itemName\": \"RES_\\u00A7i[8]\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"A[8].const19.000000\", \"type\": \"additive\", \"parameters\": {\"b\": {\"type\": 2, \"value\": 19}}, \"items\": ["
                                "{\"itemName\": \"A[8]\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"A[9]\", \"type\": \"additive\", \"parameters\": {}, \"items\": ["
                                "{\"itemName\": \"A[9]\", \"parameters\": {\"a\": {\"type\": 0, \"value\": 1}}}, "
                                "{\"itemName\": \"RES_\\u00A7i[9]\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"A[9].const10.000000\", \"type\": \"additive\", \"parameters\": {\"b\": {\"type\": 2, \"value\": 10}}, \"items\": ["
                                "{\"itemName\": \"A[9]\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"B[8]\", \"type\": \"additive\", \"parameters\": {\"i\": {\"type\": 0, \"value\": 1}, \"n\": {\"type\": 1, \"value\": 1}}, \"items\": []}, "
                                "{\"blockName\": \"B[9]\", \"type\": \"additive\", \"parameters\": {\"i\": {\"type\": 0, \"value\": 1}, \"n\": {\"type\": 1, \"value\": 2}}, \"items\": []}, "
                                "{\"blockName\": \"I\", \"type\": \"additive\", \"parameters\": {\"i\": {\"type\": 0, \"value\": 1}, \"n\": {\"type\": 1, \"value\": 3}}, \"items\": ["
                                "{\"itemName\": \"A[8]\", \"parameters\": {\"a\": {\"type\": 2, \"value\": -1}}}, "
                                "{\"itemName\": \"A[9]\", \"parameters\": {\"a\": {\"type\": 2, \"value\": -1}}}]}, "
                                "{\"blockName\": \"RES\", \"type\": \"additive\", \"parameters\": {\"o\": {\"type\": 0, \"value\": 1}}, \"items\": []}, "
                                "{\"blockName\": \"RES_\\u00A7i[8]\", \"type\": \"additive\", \"parameters\": {}, \"items\": ["
                                "{\"itemName\": \"RES\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"RES_\\u00A7i[9]\", \"type\": \"additive\", \"parameters\": {}, \"items\": ["
                                "{\"itemName\": \"RES\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}]}")};

            auto i = res->begin();
            auto l = expected.begin();
            for (; i != res->end() && l != expected.end(); ++i, ++l) {
                auto os = std::make_shared<std::ostringstream>();
                ByteCodeWriter bw(os, IntegrationTests::prepareContext());
                if (bw.writeJson(*i)) {
                    return 0;
                }
                auto s = os->str();
                fast_assert(StringConvUtils::u8tou(s) == *l,
                            StringUtils::spf(std::string("Expected: {0}\nActual: "), StringConvUtils::utou8(*l)), s)
            }
            fast_assert(i == res->end() && l == expected.end(), StringUtils::spf(std::string("Expected: {0}\nActual: "),
                                                                                 l == expected.end() ? "nothing"
                                                                                                     : StringConvUtils::utou8(
                                                                                         *l)),
                        i == res->end() ? "nothing" : "extra string")

            return 1;
        }

        int8_t compilerCalledContextStaticGeneratorTest() {
            icu::UnicodeString j = USS("[LEN : 10]\n"
                                               "sample [i=8:LEN] I {\n"
                                               " A[i] = # (A[i] >= I), 1\n"
                                               " RES += # A[i]\n"
                                               " out RES\n"
                                               "}\n"
                                               "network I {\n"
                                               " RES = sample\\ [LEN: 8] I:I\n"
                                               " RES2 = sample \\[LEN : 9] I:I\n"
                                               " out RES.RES\n"
                                               "}");
            auto ss = std::make_shared<std::istringstream>();
            ss->str(StringConvUtils::utou8(j));
            nnvm::ByteCodeParser parser(ss, TEST_PATH, USS("single"), IntegrationTests::prepareContext());
            auto res = parser.read();

            std::vector<icu::UnicodeString> expected{
                    USS("{\"packageName\": \"tests.network\", \"blocks\": ["
                                "{\"blockName\": \"I\", \"type\": \"additive\", \"parameters\": {\"i\": {\"type\": 0, \"value\": 1}, \"n\": {\"type\": 1, \"value\": 1}}, \"items\": ["
                                "{\"itemName\": \"RES.I\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}, "
                                "{\"itemName\": \"RES2.I\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"RES.A[8]\", \"type\": \"additive\", \"parameters\": {}, \"items\": ["
                                "{\"itemName\": \"RES.A[8]\", \"parameters\": {\"a\": {\"type\": 0, \"value\": 1}}}, "
                                "{\"itemName\": \"RES.RES_\\u00A7i[8]\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"RES.A[8].const1.000000\", \"type\": \"additive\", \"parameters\": {\"b\": {\"type\": 2, \"value\": 1}}, \"items\": ["
                                "{\"itemName\": \"RES.A[8]\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"RES.I\", \"type\": \"additive\", \"parameters\": {}, \"items\": ["
                                "{\"itemName\": \"RES.A[8]\", \"parameters\": {\"a\": {\"type\": 2, \"value\": -1}}}]}, "
                                "{\"blockName\": \"RES.RES\", \"type\": \"additive\", \"parameters\": {\"o\": {\"type\": 0, \"value\": 1}}, \"items\": []}, "
                                "{\"blockName\": \"RES.RES_\\u00A7i[8]\", \"type\": \"additive\", \"parameters\": {}, \"items\": ["
                                "{\"itemName\": \"RES.RES\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"RES2.A[8]\", \"type\": \"additive\", \"parameters\": {}, \"items\": ["
                                "{\"itemName\": \"RES2.A[8]\", \"parameters\": {\"a\": {\"type\": 0, \"value\": 1}}}, "
                                "{\"itemName\": \"RES2.RES_\\u00A7i[8]\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"RES2.A[8].const1.000000\", \"type\": \"additive\", \"parameters\": {\"b\": {\"type\": 2, \"value\": 1}}, \"items\": ["
                                "{\"itemName\": \"RES2.A[8]\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"RES2.A[9]\", \"type\": \"additive\", \"parameters\": {}, \"items\": ["
                                "{\"itemName\": \"RES2.A[9]\", \"parameters\": {\"a\": {\"type\": 0, \"value\": 1}}}, "
                                "{\"itemName\": \"RES2.RES_\\u00A7i[9]\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"RES2.A[9].const1.000000\", \"type\": \"additive\", \"parameters\": {\"b\": {\"type\": 2, \"value\": 1}}, \"items\": ["
                                "{\"itemName\": \"RES2.A[9]\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"RES2.I\", \"type\": \"additive\", \"parameters\": {}, \"items\": ["
                                "{\"itemName\": \"RES2.A[8]\", \"parameters\": {\"a\": {\"type\": 2, \"value\": -1}}}, "
                                "{\"itemName\": \"RES2.A[9]\", \"parameters\": {\"a\": {\"type\": 2, \"value\": -1}}}]}, "
                                "{\"blockName\": \"RES2.RES\", \"type\": \"additive\", \"parameters\": {}, \"items\": []}, "
                                "{\"blockName\": \"RES2.RES_\\u00A7i[8]\", \"type\": \"additive\", \"parameters\": {}, \"items\": ["
                                "{\"itemName\": \"RES2.RES\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"RES2.RES_\\u00A7i[9]\", \"type\": \"additive\", \"parameters\": {}, \"items\": ["
                                "{\"itemName\": \"RES2.RES\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}]}"),

                    USS("{\"packageName\": \"tests.sample_i_8_10\", \"blocks\": ["
                                "{\"blockName\": \"A[10]\", \"type\": \"additive\", \"parameters\": {}, \"items\": ["
                                "{\"itemName\": \"A[10]\", \"parameters\": {\"a\": {\"type\": 0, \"value\": 1}}}, "
                                "{\"itemName\": \"RES_\\u00A7i[10]\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"A[10].const1.000000\", \"type\": \"additive\", \"parameters\": {\"b\": {\"type\": 2, \"value\": 1}}, \"items\": ["
                                "{\"itemName\": \"A[10]\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"A[8]\", \"type\": \"additive\", \"parameters\": {}, \"items\": ["
                                "{\"itemName\": \"A[8]\", \"parameters\": {\"a\": {\"type\": 0, \"value\": 1}}}, "
                                "{\"itemName\": \"RES_\\u00A7i[8]\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"A[8].const1.000000\", \"type\": \"additive\", \"parameters\": {\"b\": {\"type\": 2, \"value\": 1}}, \"items\": ["
                                "{\"itemName\": \"A[8]\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"A[9]\", \"type\": \"additive\", \"parameters\": {}, \"items\": ["
                                "{\"itemName\": \"A[9]\", \"parameters\": {\"a\": {\"type\": 0, \"value\": 1}}}, "
                                "{\"itemName\": \"RES_\\u00A7i[9]\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"A[9].const1.000000\", \"type\": \"additive\", \"parameters\": {\"b\": {\"type\": 2, \"value\": 1}}, \"items\": ["
                                "{\"itemName\": \"A[9]\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"I\", \"type\": \"additive\", \"parameters\": {\"i\": {\"type\": 0, \"value\": 1}, \"n\": {\"type\": 1, \"value\": 1}}, \"items\": ["
                                "{\"itemName\": \"A[8]\", \"parameters\": {\"a\": {\"type\": 2, \"value\": -1}}}, "
                                "{\"itemName\": \"A[9]\", \"parameters\": {\"a\": {\"type\": 2, \"value\": -1}}}, "
                                "{\"itemName\": \"A[10]\", \"parameters\": {\"a\": {\"type\": 2, \"value\": -1}}}]}, "
                                "{\"blockName\": \"RES\", \"type\": \"additive\", \"parameters\": {\"o\": {\"type\": 0, \"value\": 1}}, \"items\": []}, "
                                "{\"blockName\": \"RES_\\u00A7i[10]\", \"type\": \"additive\", \"parameters\": {}, \"items\": ["
                                "{\"itemName\": \"RES\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"RES_\\u00A7i[8]\", \"type\": \"additive\", \"parameters\": {}, \"items\": ["
                                "{\"itemName\": \"RES\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"RES_\\u00A7i[9]\", \"type\": \"additive\", \"parameters\": {}, \"items\": ["
                                "{\"itemName\": \"RES\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}]}")};

            auto i = res->begin();
            auto l = expected.begin();
            for (; i != res->end() && l != expected.end(); ++i, ++l) {
                auto os = std::make_shared<std::ostringstream>();
                ByteCodeWriter bw(os, IntegrationTests::prepareContext());
                if (bw.writeJson(*i)) {
                    return 0;
                }
                auto s = os->str();
                fast_assert(StringConvUtils::u8tou(s) == *l,
                            StringUtils::spf(std::string("Expected: {0}\nActual: "), StringConvUtils::utou8(*l)), s)
            }
            fast_assert(i == res->end() && l == expected.end(), StringUtils::spf(std::string("Expected: {0}\nActual: "),
                                                                                 l == expected.end() ? "nothing"
                                                                                                     : StringConvUtils::utou8(
                                                                                         *l)),
                        i == res->end() ? "nothing" : "extra string")

            return 1;
        }

        int8_t compilerStaticGeneratorExpressionTest() {
            icu::UnicodeString j = USS("network [i=8:8,k=1:2] I {\n"
                                               " A[i] = # (A[i] >= I), (+ (* 2, k), (* k, I))\n"
                                               "}");
            auto ss = std::make_shared<std::istringstream>();
            ss->str(StringConvUtils::utou8(j));
            nnvm::ByteCodeParser parser(ss, TEST_PATH, USS("single"), IntegrationTests::prepareContext());
            auto res = parser.read();

            std::vector<icu::UnicodeString> expected{
                    USS("{\"packageName\": \"tests.network_i_8_8_k_1_2\", \"blocks\": ["
                                "{\"blockName\": \"A[8]\", \"type\": \"additive\", \"parameters\": {}, \"items\": ["
                                "{\"itemName\": \"A[8]\", \"parameters\": {\"a\": {\"type\": 0, \"value\": 1}}}]}, "
                                "{\"blockName\": \"A[8].const6.000000\", \"type\": \"additive\", \"parameters\": {\"b\": {\"type\": 2, \"value\": 6}}, \"items\": ["
                                "{\"itemName\": \"A[8]\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"I\", \"type\": \"additive\", \"parameters\": {\"i\": {\"type\": 0, \"value\": 1}, \"n\": {\"type\": 1, \"value\": 1}}, \"items\": ["
                                "{\"itemName\": \"A[8]\", \"parameters\": {\"a\": {\"type\": 2, \"value\": -1}, \"s\": {\"type\": 2, \"value\": 3}}}]}]}")};

            auto i = res->begin();
            auto l = expected.begin();
            for (; i != res->end() && l != expected.end(); ++i, ++l) {
                auto os = std::make_shared<std::ostringstream>();
                ByteCodeWriter bw(os, IntegrationTests::prepareContext());
                if (bw.writeJson(*i)) {
                    return 0;
                }
                auto s = os->str();
                fast_assert(StringConvUtils::u8tou(s) == *l,
                            StringUtils::spf(std::string("Expected: {0}\nActual: "), StringConvUtils::utou8(*l)), s)
            }

            return 1;
        }

        int8_t compilerStaticGeneratorExpressionIndexTest() {
            icu::UnicodeString j = USS("[LEN:10]\n"
                                               "network [i=8:LEN-2] I {\n"
                                               " A[i] = # (A[i] >= I), 1\n"
                                               "}");
            auto ss = std::make_shared<std::istringstream>();
            ss->str(StringConvUtils::utou8(j));
            nnvm::ByteCodeParser parser(ss, TEST_PATH, USS("single"), IntegrationTests::prepareContext());
            auto res = parser.read();

            std::vector<icu::UnicodeString> expected{
                    USS("{\"packageName\": \"tests.network_i_8_8\", \"blocks\": ["
                                "{\"blockName\": \"A[8]\", \"type\": \"additive\", \"parameters\": {}, \"items\": ["
                                "{\"itemName\": \"A[8]\", \"parameters\": {\"a\": {\"type\": 0, \"value\": 1}}}]}, "
                                "{\"blockName\": \"A[8].const1.000000\", \"type\": \"additive\", \"parameters\": {\"b\": {\"type\": 2, \"value\": 1}}, \"items\": ["
                                "{\"itemName\": \"A[8]\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"I\", \"type\": \"additive\", \"parameters\": {\"i\": {\"type\": 0, \"value\": 1}, \"n\": {\"type\": 1, \"value\": 1}}, \"items\": ["
                                "{\"itemName\": \"A[8]\", \"parameters\": {\"a\": {\"type\": 2, \"value\": -1}}}]}]}")};

            auto i = res->begin();
            auto l = expected.begin();
            for (; i != res->end() && l != expected.end(); ++i, ++l) {
                auto os = std::make_shared<std::ostringstream>();
                ByteCodeWriter bw(os, IntegrationTests::prepareContext());
                if (bw.writeJson(*i)) {
                    return 0;
                }
                auto s = os->str();
                fast_assert(StringConvUtils::u8tou(s) == *l,
                            StringUtils::spf(std::string("Expected: {0}\nActual: "), StringConvUtils::utou8(*l)), s)
            }

            return 1;
        }

        int8_t compilerComplexExpressionBalanceTest() {
            icu::UnicodeString j = USS("sum A, B {\n"
                                               " out (+ A, B)"
                                               "}\n"
                                               "network A, B, C, D {\n"
                                               " E = + A, (* 2, B), (* 3, (+ C, D)), (sum A:A, B:B)\n"
                                               "}");
            auto ss = std::make_shared<std::istringstream>();
            ss->str(StringConvUtils::utou8(j));
            nnvm::ByteCodeParser parser(ss, TEST_PATH, USS("single"), IntegrationTests::prepareContext());
            auto res = parser.read();

            /*
network A, B, C, D {
    result__blockcall__sum__A__A__B__B2 is blockcall 'sum', (A : A), (B : B) "1-2"
    result__plus__C__D3 is plus C, D "1"
    result__multiply__2__B4 is multiply B, 2 "1"
    result__result__multiply__2__B45 is result__multiply__2__B4 "2"
    result__multiply__3__result__plus__C__D36 is multiply result__plus__C__D3, 3 "2"
    result__A7 is A "1"
    result__result__A78 is result__A7 "2"
    E is plus result__result__multiply__2__B45, result__multiply__3__result__plus__C__D36, result__result__A78, result__blockcall__sum__A__A__B__B2
}
     */

            std::vector<icu::UnicodeString> expected{
                    USS("{\"packageName\": \"tests.network\", \"blocks\": ["
                                "{\"blockName\": \"A\", \"type\": \"additive\", \"parameters\": {\"i\": {\"type\": 0, \"value\": 1}, \"n\": {\"type\": 1, \"value\": 1}}, \"items\": ["
                                "{\"itemName\": \"result__blockcall__sum__A__A__B__B2.A\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}, "
                                "{\"itemName\": \"result__A7\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"B\", \"type\": \"additive\", \"parameters\": {\"i\": {\"type\": 0, \"value\": 1}, \"n\": {\"type\": 1, \"value\": 2}}, \"items\": ["
                                "{\"itemName\": \"result__blockcall__sum__A__A__B__B2.B\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}, "
                                "{\"itemName\": \"result__multiply__2__B4\", \"parameters\": {\"a\": {\"type\": 2, \"value\": 2}}}]}, "
                                "{\"blockName\": \"C\", \"type\": \"additive\", \"parameters\": {\"i\": {\"type\": 0, \"value\": 1}, \"n\": {\"type\": 1, \"value\": 3}}, \"items\": ["
                                "{\"itemName\": \"result__plus__C__D3\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"D\", \"type\": \"additive\", \"parameters\": {\"i\": {\"type\": 0, \"value\": 1}, \"n\": {\"type\": 1, \"value\": 4}}, \"items\": ["
                                "{\"itemName\": \"result__plus__C__D3\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"E\", \"type\": \"additive\", \"parameters\": {}, \"items\": []}, "
                                "{\"blockName\": \"result__A7\", \"type\": \"additive\", \"parameters\": {}, \"items\": ["
                                "{\"itemName\": \"result__result__A78\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"result__blockcall__sum__A__A__B__B2.A\", \"type\": \"additive\", \"parameters\": {}, \"items\": ["
                                "{\"itemName\": \"result__blockcall__sum__A__A__B__B2.plus__A__B1\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"result__blockcall__sum__A__A__B__B2.B\", \"type\": \"additive\", \"parameters\": {}, \"items\": ["
                                "{\"itemName\": \"result__blockcall__sum__A__A__B__B2.plus__A__B1\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"result__blockcall__sum__A__A__B__B2.plus__A__B1\", \"type\": \"additive\", \"parameters\": {}, \"items\": ["
                                "{\"itemName\": \"E\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"result__multiply__2__B4\", \"type\": \"multiplicative\", \"parameters\": {}, \"items\": ["
                                "{\"itemName\": \"result__result__multiply__2__B45\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"result__multiply__3__result__plus__C__D36\", \"type\": \"multiplicative\", \"parameters\": {}, \"items\": ["
                                "{\"itemName\": \"E\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"result__plus__C__D3\", \"type\": \"additive\", \"parameters\": {}, \"items\": ["
                                "{\"itemName\": \"result__multiply__3__result__plus__C__D36\", \"parameters\": {\"a\": {\"type\": 2, \"value\": 3}}}]}, "
                                "{\"blockName\": \"result__result__A78\", \"type\": \"additive\", \"parameters\": {}, \"items\": ["
                                "{\"itemName\": \"E\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}, "
                                "{\"blockName\": \"result__result__multiply__2__B45\", \"type\": \"additive\", \"parameters\": {}, \"items\": ["
                                "{\"itemName\": \"E\", \"parameters\": {\"s\": {\"type\": 2, \"value\": 1}}}]}]}")};

            auto i = res->begin();
            auto l = expected.begin();
            for (; i != res->end() && l != expected.end(); ++i, ++l) {
                auto os = std::make_shared<std::ostringstream>();
                ByteCodeWriter bw(os, IntegrationTests::prepareContext());
                if (bw.writeJson(*i)) {
                    return 0;
                }
                auto s = os->str();
                fast_assert(StringConvUtils::u8tou(s) == *l,
                            StringUtils::spf(std::string("Expected: {0}\nActual: "), StringConvUtils::utou8(*l)), s)
            }

            return 1;
        }

        int8_t decompilerSimpleTest() {
            auto ctx = IntegrationTests::prepareContext();
            auto c = std::make_shared<ByteCode>(USS("TestNNSimpleDecompile"),ctx->byteLoader);
            // A(0,0) -(0,0,1)-> B[A]
            auto b = std::make_shared<ByteCodeBlock>(USS("A"), ADDITIVE);
            c->addBlock(b);
            auto bi = std::make_shared<ByteCodeBlockItem>(USS("B[A]"), b);
            b->addItem(bi);
            auto bip = std::make_shared<ByteCodeParameter>(LinkParams::GENERATION, (int8_t) 1);
            bi->setParameter(bip);
            // B[A](0,A) -(0,1,0)-> C
            auto b2 = std::make_shared<ByteCodeBlock>(USS("B[A]"), ADDITIVE);
            c->addBlock(b2);
            auto b2p = std::make_shared<ByteCodeParameter>(ElementParams::BIAS, USS("A"));
            b2->setParameter(b2p);
            auto b2p2 = std::make_shared<ByteCodeParameter>(ElementParams::TEMPLATE, (int8_t) 1);
            b2->setParameter(b2p2);
            auto bi2 = std::make_shared<ByteCodeBlockItem>(USS("C"), b2);
            b2->addItem(bi2);
            auto bi2p = std::make_shared<ByteCodeParameter>(LinkParams::STREAM, (int8_t) 1);
            bi2->setParameter(bi2p);
            // C
            auto b3 = std::make_shared<ByteCodeBlock>(USS("C"), ADDITIVE);
            c->addBlock(b3);
            // B[1](0,1) -(0,1,0)-> C
            auto b4 = std::make_shared<ByteCodeBlock>(USS("B[1]"), ADDITIVE);
            c->addBlock(b4);
            auto b4p = std::make_shared<ByteCodeParameter>(ElementParams::BIAS, (int8_t) 1);
            b4->setParameter(b4p);
            auto bi4 = std::make_shared<ByteCodeBlockItem>(USS("C"), b4);
            b4->addItem(bi4);
            auto bi4p = std::make_shared<ByteCodeParameter>(LinkParams::STREAM, (int8_t) 1);
            bi4->setParameter(bi4p);
            ByteCodeUtils::finalizeCode(c);
            auto ss = std::make_shared<std::ostringstream>();
            ByteCodeWriter bw(ss, ctx);
            if (bw.writeSource(c)) {
                return 0;
            }
            auto actual = StringConvUtils::u8tou(ss->str());
            ss = std::make_shared<std::ostringstream>();
            *ss << "\"package TestNNSimpleDecompile\"" << std::endl
            << "network {" << std::endl
            << "\tA is additive (+ 0) >= 0, (+ 0)" << std::endl
            << "\tB[1] is additive (+ 0) >= 0, (+ 0, 1)" << std::endl
            << "\t[A] B[A] is additive (+ 0) >= 0, (+ 0, A)" << std::endl
            << "\tC is additive (+ 0) >= 0, (+ (* 1, B[1]), (* 1, B[A]))" << std::endl
            << "}" << std::endl;
            auto expected = StringConvUtils::u8tou(ss->str());
            fast_assert(actual == expected,
                        StringUtils::spf(std::string("Expected: {0}\nActual: "), StringConvUtils::utou8(expected)),
                        StringConvUtils::utou8(actual));
            return 1;
        }

        int8_t decompilerComplexTest() {
            auto ctx = IntegrationTests::prepareContext();
            auto c = std::make_shared<ByteCode>(USS("TestNNComplexDecompile"), ctx->byteLoader);
            // A(0,0) -(0,0,1)-> B[A]
            auto b = std::make_shared<ByteCodeBlock>(USS("A"), ADDITIVE);
            c->addBlock(b);
            auto bi = std::make_shared<ByteCodeBlockItem>(USS("B[A]"), b);
            b->addItem(bi);
            auto bip = std::make_shared<ByteCodeParameter>(LinkParams::GENERATION, (int8_t) 1);
            bi->setParameter(bip);
            // B[A](0,A) -(0,1,0)-> C
            auto b2 = std::make_shared<ByteCodeBlock>(USS("B[A]"), ADDITIVE);
            c->addBlock(b2);
            auto b2p = std::make_shared<ByteCodeParameter>(ElementParams::BIAS, USS("A"));
            b2->setParameter(b2p);
            auto b2p2 = std::make_shared<ByteCodeParameter>(ElementParams::TEMPLATE, (int8_t) 1);
            b2->setParameter(b2p2);
            auto bi2 = std::make_shared<ByteCodeBlockItem>(USS("C"), b2);
            b2->addItem(bi2);
            auto bi2p = std::make_shared<ByteCodeParameter>(LinkParams::STREAM, (int8_t) 1);
            bi2->setParameter(bi2p);
            // C
            auto b3 = std::make_shared<ByteCodeBlock>(USS("C"), ADDITIVE);
            c->addBlock(b3);
            auto bi3 = std::make_shared<ByteCodeBlockItem>(USS("A"), b3);
            b3->addItem(bi3);
            auto bi3p = std::make_shared<ByteCodeParameter>(LinkParams::STREAM, (int8_t) 10);
            bi3->setParameter(bi3p);
            auto bi3_2 = std::make_shared<ByteCodeBlockItem>(USS("C"), b3);
            b3->addItem(bi3_2);
            auto bi3_2p = std::make_shared<ByteCodeParameter>(LinkParams::STREAM, (int8_t) -2);
            bi3_2->setParameter(bi3_2p);
            auto bi3_3 = std::make_shared<ByteCodeBlockItem>(USS("E"), b3);
            b3->addItem(bi3_3);
            auto bi3_3p = std::make_shared<ByteCodeParameter>(LinkParams::ACTIVATION, (int8_t) 1);
            bi3_3->setParameter(bi3_3p);
            auto bi3_3p2 = std::make_shared<ByteCodeParameter>(LinkParams::STREAM, (int8_t) 1);
            bi3_3->setParameter(bi3_3p2);
            // B[1](0,1) -(0,1,0)-> C
            auto b4 = std::make_shared<ByteCodeBlock>(USS("B[1]"), ADDITIVE);
            c->addBlock(b4);
            auto b4p = std::make_shared<ByteCodeParameter>(ElementParams::BIAS, (int8_t) 1);
            b4->setParameter(b4p);
            auto bi4 = std::make_shared<ByteCodeBlockItem>(USS("C"), b4);
            b4->addItem(bi4);
            auto bi4p = std::make_shared<ByteCodeParameter>(LinkParams::STREAM, (int8_t) 1);
            bi4->setParameter(bi4p);
            // E
            auto b5 = std::make_shared<ByteCodeBlock>(USS("E"), EXIT);
            c->addBlock(b5);
            ByteCodeUtils::finalizeCode(c);
            auto ss = std::make_shared<std::ostringstream>();
            nnvm::ByteCodeWriter bw(ss, ctx);
            if (bw.writeSource(c)) {
                return 0;
            }
            auto actual = StringConvUtils::u8tou(ss->str());
            ss = std::make_shared<std::ostringstream>();
            *ss << "\"package TestNNComplexDecompile\"" << std::endl
            << "network {" << std::endl
            << "\tA is additive (+ 0) >= 0, (+ (* 10, C))" << std::endl
            << "\tB[1] is additive (+ 0) >= 0, (+ 0, 1)" << std::endl
            << "\t[A] B[A] is additive (+ 0) >= 0, (+ 0, A)" << std::endl
            << "\tC is additive (+ 0) >= 0, (+ (* 1, B[1]), (* 1, B[A]), (* -2, C))" << std::endl
            << "\tE is exit (+ (* 1, C)), (+ (* 1, C))" << std::endl
            << "}" << std::endl;
            auto expected = StringConvUtils::u8tou(ss->str());
            fast_assert(actual == expected,
                        StringUtils::spf(std::string("Expected: {0}\nActual: "), StringConvUtils::utou8(expected)),
                        StringConvUtils::utou8(actual));
            return 1;
        }

        int8_t automataSimpleTest() {
            int64_t pos = 0;
            auto target = std::make_shared<std::string>("simpleTestString");
            std::vector<State<std::string, int8_t, std::string, std::string>> nameStates = {
                    {
                            [&pos, &target](std::shared_ptr<std::string> dataSource,
                                            std::vector<int8_t> &result) -> int64_t {
                                if (pos >= target->size()) { return 0; }
                                result.resize(2);
                                result[0] = target->at(static_cast<unsigned long>(pos));
                                pos++;
                                return 1;
                            },
                            [](std::vector<int8_t> &data, int64_t size) -> int8_t {
                                return static_cast<int8_t>(size == 0 ? 0 : 1);
                            },
                            [](std::vector<int8_t> &data, int64_t size, std::shared_ptr<std::string> &carried,
                               std::shared_ptr<std::string> &result) -> int8_t {
                                if (!result) { result = std::make_shared<std::string>(""); }
                                if (size > 0) { (*result) += data[0]; }
                                return 1;
                            },
                            [](std::vector<int8_t> &data, int64_t size, std::shared_ptr<std::string> c,
                               int16_t &state) -> int8_t {
                                if (!size) { state++; }
                                return 1;
                            },
                            [](std::vector<int8_t> &data, int16_t state, int64_t symbol) -> icu::UnicodeString {
                                return USS("");
                            },
                            1
                    }, //0 .|\w
            };
            ErrorHandlers<std::string, int8_t, std::string, std::string> handlers;
            handlers.noParse = [](int16_t stateIndex, std::shared_ptr<std::vector<int16_t> > states) { };
            handlers.noRead = [](
                    std::function<icu::UnicodeString(std::vector<int8_t> & , int16_t, int64_t)> &errorDetails,
                    std::vector<int8_t> &buffer, int16_t stateIndex, std::shared_ptr<std::vector<int16_t> > states) { };
            handlers.noMatch = [](
                    std::function<icu::UnicodeString(std::vector<int8_t> & , int16_t, int64_t)> &errorDetails,
                    std::vector<int8_t> &buffer, int16_t stateIndex, std::shared_ptr<std::vector<int16_t> > states) { };
            handlers.noFill = [](
                    std::function<icu::UnicodeString(std::vector<int8_t> & , int16_t, int64_t)> &errorDetails,
                    std::vector<int8_t> &buffer, int16_t stateIndex, std::shared_ptr<std::vector<int16_t> > states) { };
            StateMachine<std::string, int8_t, std::string, std::string> a(nameStates, handlers);
            std::shared_ptr<std::string> res;
            int8_t ok = 1;
            int64_t n = 10000;
            int64_t tm;
            int64_t td;
            PerformanceUtils::timeit([&n, &a, &pos, &target, &res]() {
                for (auto i = 0; i < n; ++i) {
                    pos = 0;
                    res = a.run(target);
                }
            }, [&tm](int64_t t) { tm = t; });
            ok = ok && (*res) == (*target);
            PerformanceUtils::timeit([&n, &a, &pos, &target, &res]() {
                for (auto i = 0; i < n; ++i) {
                    res = std::make_shared<std::string>("");
                    for (pos = 0; pos < target->size(); ++pos) {
                        (*res) += target->at((unsigned long) pos);
                    }
                }
            }, [&td](int64_t t) { td = t; });
            ok = ok && (*res) == (*target);
            std::cout << "Performance Automata=" << tm / n << "ns, Direct=" << td / n << "ns, Latency " << tm / td <<
            " times" << std::endl;
            return ok;
        }

        // each test should use another port, as previous run could reach CLOSE_WAIT state
        int32_t testPortStart = 10000;

        int8_t withGrid(int32_t index, const std::function<int8_t(std::shared_ptr<Context>, std::shared_ptr<TestProtocol>,
                                                                  std::shared_ptr<Context>, std::shared_ptr<TestProtocol>)> &test)
        {
            // launch two servers of TestProtocol
            auto step = PerformanceUtils::now_nanoseconds();
            auto start = step;
            auto ctx1 = IntegrationTests::prepareContext(testPortStart + index * 2 + 1);
            auto ctx2 = IntegrationTests::prepareContext(testPortStart + index * 2 + 2);
            auto p1 = std::make_shared<TestProtocol>(ctx1);
            auto p2 = std::make_shared<TestProtocol>(ctx2);
            auto s1 = std::make_shared<grid::Server>(ctx1, p1);
            auto s2 = std::make_shared<grid::Server>(ctx2, p2);
            if (!Tests::waitForCondition([s1]() { return s1->isRunning() ? 1 : 0; }))
            {
                ctx1->setup->getLogger()->info(USS("Running condition not reached for server 1 in 2s"));
                return 0;
            }
            if (!Tests::waitForCondition([s2]() { return s2->isRunning() ? 1 : 0; }))
            {
                ctx2->setup->getLogger()->info(USS("Running condition not reached for server 2 in 2s"));
                return 0;
            }

            step = PerformanceUtils::now_nanoseconds();
            auto reached = step - start;
            ctx1->setup->getLogger()->info(StringUtils::spf(USS("Running condition reached in {0} ms"), reached/1000000.0));

            auto res = test(ctx1, p1, ctx2, p2);

            step = PerformanceUtils::now_nanoseconds();
            auto tested = step - reached - start;
            ctx1->setup->getLogger()->info(StringUtils::spf(USS("Testing finished in {0} ms"), tested/1000000.0));

            s1->stop();
            s2->stop();
            Tests::waitForCondition([s1, s2]() { return !s1->isRunning() && !s2->isRunning() ? 1 : 0; });

            step = PerformanceUtils::now_nanoseconds();
            auto stopped = step - tested - reached - start;
            ctx1->setup->getLogger()->info(StringUtils::spf(USS("Stopping condition reached in {0} ms"), stopped/1000000.0));

            return res;
        }

        int8_t withGrid(const std::function<int8_t(std::shared_ptr<Context>, std::shared_ptr<TestProtocol>,
                                                   std::shared_ptr<Context>, std::shared_ptr<TestProtocol>)> &test)
        {
            return withGrid(0, test);
        }

        int8_t withAsio(int32_t index, const std::function<int8_t(int32_t, int32_t)> &test)
        {
            // launch two servers of Echo
            asio::io_service io_service1;
            asio::io_service io_service2;
            auto port1 = testPortStart + index * 2 + 1;
            auto port2 = testPortStart + index * 2 + 2;
            std::cout<<"Starting port "<<port1<<std::endl;
            asio::ip::tcp::acceptor acceptor1(io_service1,
                                              asio::ip::tcp::endpoint(asio::ip::tcp::v4(),
                                                                      static_cast<unsigned short>(port1)),
                                              true);
            std::cout<<"Starting port "<<port2<<std::endl;
            asio::ip::tcp::acceptor acceptor2(io_service2,
                                              asio::ip::tcp::endpoint(asio::ip::tcp::v4(),
                                                                      static_cast<unsigned short>(port2)),
                                              true);
            std::function<void()> accept1 = [&accept1, &acceptor1, &io_service1]() {
                auto socket = std::make_shared<asio::ip::tcp::socket>(io_service1);
                acceptor1.async_accept(
                        *socket,
                        [&accept1, socket](const asio::error_code &error) {
                            asio::ip::tcp::no_delay option(true);
                            socket->set_option(option);
                            auto data = std::make_shared<std::vector<int8_t>>(1024, 0);
                            socket->async_receive(
                                    asio::buffer(*data),
                                    [data, socket](const asio::error_code &error, size_t bytes_transferred) {
                                        auto v = *data;
                                        asio::async_write(*socket,
                                                          asio::buffer(*data, bytes_transferred),
                                                          [data, socket](const asio::error_code &error, size_t bytes_transferred) {
                                                              auto v2 = *data;
                                                              socket->close();
                                                          });
                                    });
                            accept1();
                        });
            };
            std::function<void()> accept2 = [&accept2, &acceptor2, &io_service2]() {
                auto socket = std::make_shared<asio::ip::tcp::socket>(io_service2);
                acceptor2.async_accept(
                        *socket,
                        [&accept2, socket](const asio::error_code &error) {
                            asio::ip::tcp::no_delay option(true);
                            socket->set_option(option);
                            auto data = std::make_shared<std::vector<int8_t>>(1024, 0);
                            socket->async_receive(
                                    asio::buffer(*data),
                                    [data, socket](const asio::error_code &error, size_t bytes_transferred) {
                                        auto v = *data;
                                        asio::async_write(*socket,
                                                          asio::buffer(*data, bytes_transferred),
                                                          [data, socket](const asio::error_code &error, size_t bytes_transferred) {
                                                              auto v2 = *data;
                                                              socket->close();
                                                          });
                                    });
                            accept2();
                        });
            };
            accept1();
            accept2();
            std::thread thread1([&io_service1]() { io_service1.run(); });
            std::thread thread2([&io_service2]() { io_service2.run(); });
            auto res = test(testPortStart + index * 2 + 1, testPortStart + index * 2 + 2);
            io_service1.stop();
            io_service2.stop();
            std::cout<<"Stopping port "<<port1<<std::endl;
            std::cout<<"Stopping port "<<port2<<std::endl;
            thread1.join();
            thread2.join();
            return res;
        }

        int8_t gridStartTest()
        {
            return withGrid([](std::shared_ptr<Context> ctx1, std::shared_ptr<TestProtocol> p1,
                               std::shared_ptr<Context> ctx2, std::shared_ptr<TestProtocol> p2) {
                grid::NNPayload data;
                p1->sendStructure(grid::ProtocolType::TCP, ctx2->setup->getRemotingSetup()->listenAddress,
                                  ctx2->setup->getRemotingSetup()->listenPort, data);
                fast_assert_less(Tests::waitForCondition([p2]() { return !p2->acks.empty() ? 1 : 0; }), "Acks timeout");
                fast_assert_equals(p2->acks.size(), 1, "Acks ");
                fast_assert_equals(std::get<1>(p2->acks[0]), USS("OK"), "Ack ");
                return 1;
            });
        }

        int8_t gridConnectAndAckTest()
        {
            return withGrid([](std::shared_ptr<Context> ctx1, std::shared_ptr<TestProtocol> p1,
                               std::shared_ptr<Context> ctx2, std::shared_ptr<TestProtocol> p2) {
                grid::NNPayload data(USS("Test"));
                p1->sendStructure(grid::ProtocolType::TCP, ctx2->setup->getRemotingSetup()->listenAddress,
                                  ctx2->setup->getRemotingSetup()->listenPort, data);
                fast_assert_less(Tests::waitForCondition([p2]() { return p2->acks.size() > 1 ? 1 : 0; }), "Acks timeout");
                fast_assert_equals(p2->acks.size(), 2, "Acks ");
                fast_assert_equals(std::get<1>(p2->acks[0]), USS("OK"), "Ack 0 ");
                fast_assert_equals(std::get<1>(p2->acks[1]), USS("Test"), "Ack 1 ");
                return 1;
            });
        }

        int8_t gridReliabilityTest()
        {
            // use n of 1000 and more to reach enough of runs for race conditions to start occurring
            const int16_t n = 1000;
            int16_t success = 0;
            for (auto i = 0; i < n; ++i) {
                auto f = [i](std::shared_ptr<Context> ctx1, std::shared_ptr<TestProtocol> p1,
                            std::shared_ptr<Context> ctx2, std::shared_ptr<TestProtocol> p2) {
                    grid::NNPayload data(USS("Test"));
                    auto port = testPortStart + i * 2 + 1;
                    p1->sendStructure(grid::ProtocolType::TCP, ctx2->setup->getRemotingSetup()->listenAddress,
                                      ctx2->setup->getRemotingSetup()->listenPort, data);
                    fast_assert_less(Tests::waitForCondition([p2]() { return p2->acks.size() > 1 ? 1 : 0; }, 1000000000),
                                     "Acks timeout");
                    fast_assert_equals(p2->acks.size(), 2, "Acks ");
                    fast_assert_equals(std::get<1>(p2->acks[0]), USS("OK"), "Ack 0 ");
                    fast_assert_equals(std::get<1>(p2->acks[1]), USS("Test"), "Ack 1 ");
                    return 1;
                };
                success += withGrid(i, f);
                ThreadUtils::wait(1000);
            }

            fast_assert_equals(success, n, "Successful starts ");

            return 1;
        }

        int8_t gridProtocolServerTest()
        {
            // use n of 1000 and more to reach enough of runs for race conditions to start occurring
            const int16_t n = 1000;
            int16_t success = 0;
            for (auto i = 0; i < n; ++i) {
                auto f = [i](std::shared_ptr<Context> ctx1, std::shared_ptr<TestProtocol> p1,
                             std::shared_ptr<Context> ctx2, std::shared_ptr<TestProtocol> p2) {
                    auto port = testPortStart + i * 2 + 1;
                    std::string address("127.0.0.1");
                    asio::io_service io_service;
                    asio::ip::tcp::socket tcpSocket(io_service);
                    asio::ip::tcp::endpoint e(asio::ip::address::from_string(address), (unsigned short)port);
                    tcpSocket.connect(e);
                    asio::ip::tcp::no_delay option(true);
                    tcpSocket.set_option(option);
                    std::vector<int8_t> data;
                    grid::NNPayload dataPayload(USS("Test"));
                    nnvm::grid::Socket dummy(address, port, nnvm::grid::ProtocolType::TCP);
                    p1->doSerialize(dummy, dataPayload, data);
                    tcpSocket.write_some(asio::buffer(data));
                    auto bytes_transferred = tcpSocket.receive(asio::buffer(data));
                    p1->doDeserialize(dummy, data, dataPayload);
                    fast_assert_equals(bytes_transferred, 33, "Bytes ");
                    fast_assert_equals(dataPayload.pointer1, (int8_t)0xA4, "P1 ");
                    fast_assert_equals(dataPayload.pointer2, (int32_t)0x24344454, "P2 ");
                    fast_assert_equals(dataPayload.majorVersion, 1, "Major ");
                    fast_assert_equals(dataPayload.minorVersion, 1, "Minor ");
                    fast_assert_equals(dataPayload.type, nnvm::grid::NNPayloadType::ACK, "Type ");
                    fast_assert_equals(dataPayload.message, USS("OK"), "Message ");
                    return 1;
                };
                success += withGrid(i, f);
                ThreadUtils::wait(1000);
            }

            fast_assert_equals(success, n, "Successful starts ");

            return 1;
        }

        int8_t gridSimplifiedServerTest()
        {
            // use n of 1000 and more to reach enough of runs for race conditions to start occurring
            const int16_t n = 1000;
            int16_t success = 0;
            for (auto i = 0; i < n; ++i) {
                auto f = [](int32_t port1, int32_t port2) {
                    asio::io_service io_service;
                    asio::ip::tcp::socket tcpSocket(io_service);
                    asio::ip::tcp::endpoint e(asio::ip::address::from_string("127.0.0.1"), (unsigned short)port1);
                    tcpSocket.connect(e);
                    asio::ip::tcp::no_delay option(true);
                    tcpSocket.set_option(option);
                    std::vector<int8_t> data(1024, 0);
                    data[0] = 'T';
                    tcpSocket.write_some(asio::buffer(data, 1));
                    auto bytes_transferred = tcpSocket.receive(asio::buffer(data));
                    fast_assert_equals(bytes_transferred, 1, "Bytes ");
                    fast_assert_equals(data[0], 'T', "Data ");
                    return 1;
                };
                success += withAsio(i, f);
                ThreadUtils::wait(1000);
            }

            fast_assert_equals(success, n, "Successful starts ");

            return 1;
        }

        int8_t gridSyncTest()
        {
            return withGrid([](std::shared_ptr<Context> ctx1, std::shared_ptr<TestProtocol> p1,
                               std::shared_ptr<Context> ctx2, std::shared_ptr<TestProtocol> p2) {
                grid::NNPayload data(grid::NNPayloadType::ENABLE_SYNC);
                p1->sendStructure(grid::ProtocolType::TCP, ctx2->setup->getRemotingSetup()->listenAddress,
                                  ctx2->setup->getRemotingSetup()->listenPort, data);
                fast_assert_less(Tests::waitForCondition([p1]() { return p1->acks.size() > 1 ? 1 : 0; }), "Acks 1 timeout");
                Tests::waitForCondition([p1]() { return p1->acks.size() > 1 ? 1 : 0; });
                fast_assert_equals(p1->acks.size(), 2, "Acks ");
                fast_assert_equals(std::get<1>(p1->acks[1]), _i("err.grid.remote_sync_prohibited"), "Ack 1 ");

                data = grid::NNPayload(grid::NNPayloadType::SYNC_STEP);
                p1->sendStructure(grid::ProtocolType::TCP, ctx2->setup->getRemotingSetup()->listenAddress,
                                  ctx2->setup->getRemotingSetup()->listenPort, data);
                fast_assert_less(Tests::waitForCondition([p1]() { return p1->acks.size() > 2 ? 1 : 0; }), "Acks 2 timeout");
                fast_assert_equals(p1->acks.size(), 3, "Acks ");
                fast_assert_equals(std::get<1>(p1->acks[2]), _i("err.grid.remote_sync_disabled"), "Ack 2 ");

                ctx2->setup->getProcessorSetup()->allowSync = 1;
                data = grid::NNPayload(grid::NNPayloadType::ENABLE_SYNC);
                p1->sendStructure(grid::ProtocolType::TCP, ctx2->setup->getRemotingSetup()->listenAddress,
                                  ctx2->setup->getRemotingSetup()->listenPort, data);
                fast_assert_less(Tests::waitForCondition([p1]() { return p1->acks.size() > 3 ? 1 : 0; }), "Acks 3 timeout");
                fast_assert_equals(p1->acks.size(), 4, "Acks ");
                fast_assert_equals(std::get<1>(p1->acks[3]), USS("OK"), "Ack 3 ");

                data = grid::NNPayload(grid::NNPayloadType::SYNC_STEP);
                p1->sendStructure(grid::ProtocolType::TCP, ctx2->setup->getRemotingSetup()->listenAddress,
                                  ctx2->setup->getRemotingSetup()->listenPort, data);
                fast_assert_less(Tests::waitForCondition([p1]() { return p1->acks.size() > 4 ? 1 : 0; }), "Acks 4 timeout");
                fast_assert_equals(p1->acks.size(), 5, "Acks ");
                fast_assert_equals(std::get<1>(p1->acks[4]), USS("OK"), "Ack 4 ");

                fast_assert_equals(p2->enableSyncs.size(), 1, "EnableSyncs ");
                fast_assert_equals(p2->syncs.size(), 1, "Syncs ");

                return 1;
            });
        }

        int8_t gridLatencyTest()
        {
            return withGrid([](std::shared_ptr<Context> ctx1, std::shared_ptr<TestProtocol> p1,
                               std::shared_ptr<Context> ctx2, std::shared_ptr<TestProtocol> p2) -> int8_t {
                int8_t res = 1;
                grid::NNPayload data(USS("Test"));
                PerformanceUtils::timeit([&data, ctx1, ctx2, p1, p2]() -> void {
                    p1->sendStructure(grid::ProtocolType::TCP, ctx2->setup->getRemotingSetup()->listenAddress,
                                      ctx2->setup->getRemotingSetup()->listenPort, data);
                    Tests::waitForCondition([p1, p2]() { return p1->acks.empty() && p2->acks.size() < 2 ? 0 : 1; });
                }, [&res](int64_t reg) {
                    ref_assert(res, reg < 1000000, "Grid Ack packet processing ms ", reg/1000000.0);
                });
                PerformanceUtils::timeit([&data, ctx1, ctx2, p1, p2]() -> void {
                    p1->sendStructure(grid::ProtocolType::TCP, ctx2->setup->getRemotingSetup()->listenAddress,
                                      ctx2->setup->getRemotingSetup()->listenPort, data);
                    Tests::waitForCondition([p1, p2]() { return p1->acks.empty() && p2->acks.size() < 2 ? 0 : 1; });
                }, [&res](int64_t reg) {
                    ref_assert(res, reg < 1000000, "Grid Ack packet reprocessing ms ", reg/1000000.0);
                });
                return res;
            });
        }

        std::map<icu::UnicodeString, testCase> utilities = {
                {USS("01. string conversion w->u->w"),           wideToUnicode},
                {USS("02. string conversion w->u8->w"),          wideToUtf},
                {USS("03. string conversion u8->u->u8"),         utfToUnicode},
                {USS("04. constant string definition"),          constantStringUnicode},
                {USS("05. constant string definition standard"), constantStringUnicodeStandard},
                {USS("06. sprintf w"),                           spfWide},
                {USS("07. sprintf u"),                           spfUnicode},
                {USS("08. u performance"),                       performanceUnicode},
                {USS("09. generator performance"),               performanceGenerator},
                {USS("10. unicode formatter performance"),       performanceFormatter},
                {USS("11. sprintf vs concat performance"),       spfVsConcatUnicode},
        };

        std::map<icu::UnicodeString, testCase> binary = {
                {USS("01. parse binary header"),                   byteCodeParserHeadedTest},
                {USS("02. parse binary wrong header"),             byteCodeParserNonHeadedTest},
                {USS("03. parse binary with package"),             byteCodeParserPackagedTest},
                {USS("04. parse binary with wrong package name"),  byteCodeParserWrongPackagedTest},
                {USS("05. parse binary with block"),               byteCodeParserBlockedTest},
                {USS("06. parse binary with wrong block name"),    byteCodeParserWrongBlockedTest},
                {USS("07. parse binary with parameterized block"), byteCodeParserBlockWithParameterTest},
                {USS("08. parse binary full test"),                byteCodeParserFunctionalTest},
        };

        std::map<icu::UnicodeString, testCase> json = {
                {USS("01. parse json full test"),                 byteCodeParserJsonFunctionalTest},
                {USS("02. parse json with escaped test"),         byteCodeParserJsonEscapeTest},
                {USS("03. parse json with unicode test"),         byteCodeParserJsonUnicodeTest},
                {USS("04. parse & write json with unicode test"), byteCodeParserWriterJsonUnicodeTest},
                {USS("05. parse json with wrong unicode test"),   byteCodeParserJsonWrongUnicodeTest},
                {USS("06. parse json with bom test"),             byteCodeParserJsonBOMTest},
        };

        std::map<icu::UnicodeString, testCase> writers = {
                {USS("01. write binary full test"), byteCodeWriterFunctionalTest},
                {USS("02. write json full test"),   byteCodeWriterJsonFunctionalTest},
        };

        std::map<icu::UnicodeString, testCase> loader = {
                {USS("01. load package test"),        byteCodeLoaderFunctionalTest},
                {USS("02. load JSON package test"),   byteCodeLoaderJSONFunctionalTest},
                {USS("03. load source package test"), byteCodeLoaderSourceFunctionalTest},
        };

        std::map<icu::UnicodeString, testCase> processor = {
                {USS("01. process simple network"),            processorSimpleTest},
                {USS("02. process if network"),                processorIfTest},
                {USS("03. process sqr network"),               processorSqrTest},
                {USS("04. process template network"),          processorTemplateTest},
                {USS("05. process memory management network"), processorMemoryManagementTest},
                {USS("06. process exit network"),              processorExitTest},
                {USS("07. process tick network"),              processorTickTest},
                {USS("08. process time network"),              processorTimeTest},
                {USS("09. sequential performance network"),    processorPerformanceSequentialTest},
                {USS("10. openmp performance network"),        processorPerformanceOMPTest},
                {USS("11. opencl performance network"),        processorPerformanceOCLTest},
        };

        std::map<icu::UnicodeString, testCase> language_name = {
                {USS("01. parse simple name"),      nameParserSimpleTest},
                {USS("02. parse global name"),      nameParserGlobalTest},
                {USS("03. parse wrong name"),       nameParserWrongTest},
                {USS("04. parse wrong comma name"), nameParserWrongCommaTest},
                {USS("05. parse long name"),        nameParserLongTest},
                {USS("06. parse indexed name"),     nameParserIndexTest},
                {USS("07. parse sub name"),         nameParserSubTest},
        };

        std::map<icu::UnicodeString, testCase> compiler = {
                {USS("01. compile empty"),                                       compilerEmptyTest},
                {USS("02. compile consts"),                                      compilerConstTest},
                {USS("03. compile single"),                                      compilerSingleTest},
                {USS("04. compile main arguments"),                              compilerMainArgumentsTest},
                {USS("05. compile comments"),                                    compilerCommentsTest},
                {USS("06. compile single variable"),                             compilerSingleVariableTest},
                {USS("07. compile expected variable"),                           compilerExpectedVariableTest},
                {USS("08. compile one additive"),                                compilerOneAdditiveTest},
                {USS("09. compile expression"),                                  compilerExpressionTest},
                {USS("10. compile complex expression"),                          compilerComplexSumExpressionTest},
                {USS("11. compile in and out"),                                  compilerInAndOutTest},
                {USS("12. compile block call"),                                  compilerBlockCallTest},
                {USS("13. compile block call using json check"),                 compilerBlockCallTestJsonCheck},
                {USS("14. compile block call with single output"),               compilerBlockCallSingleOutputTest},
                {USS("15. compile block call with single output in expression"), compilerBlockCallSingleOutputInExpressionTest},
                {USS("16. compile factorial"),                                   compilerFactorialTest},
                {USS("17. compile generator definition"),                        compilerGeneratorDefinitionTest},
                {USS("18. compile generator"),                                   compilerGeneratorTest},
                {USS("19. compile static generator"),                            compilerStaticGeneratorTest},
                {USS("20. compile double static generator"),                     compilerDoubleStaticGeneratorTest},
                {USS("21. compile conditional static generator"),                compilerConditionalStaticGeneratorTest},
                {USS("22. compile context static generator"),                    compilerContextStaticGeneratorTest},
                {USS("23. compile called context static generator"),             compilerCalledContextStaticGeneratorTest},
                {USS("24. compile static generator with expression"),            compilerStaticGeneratorExpressionTest},
                {USS("25. compile static generator with expression on index"),   compilerStaticGeneratorExpressionIndexTest},
                {USS("26. compile complex expression balance"),                  compilerComplexExpressionBalanceTest},
        };

        std::map<icu::UnicodeString, testCase> decompiler = {
                {USS("01. decompile simple"),  decompilerSimpleTest},
                {USS("02. decompile complex"), decompilerComplexTest},
        };

        std::map<icu::UnicodeString, testCase> automata = {
                {USS("01. automata simple"), automataSimpleTest},
        };

        std::map<icu::UnicodeString, testCase> grid = {
                {USS("01. grid start"), gridStartTest},
                {USS("02. grid connect and ack"), gridConnectAndAckTest},
                {USS("03. grid sync"), gridSyncTest},
                {USS("04. grid reliability"), gridReliabilityTest},
                {USS("05. grid protocol reliability"), gridProtocolServerTest},
                {USS("06. grid asio reliability"), gridSimplifiedServerTest},
                {USS("07. grid latency"), gridLatencyTest},
        };

        std::map<icu::UnicodeString, std::map<icu::UnicodeString, testCase>> integration = {
                {USS("01. utilities"),            utilities},
                {USS("02. binary parser"),        binary},
                {USS("03. json parser"),          json},
                {USS("04. writers"),              writers},
                {USS("05. loader"),               loader},
                {USS("06. processor"),            processor},
                {USS("07. language name parser"), language_name},
                {USS("08. language compiler"),    compiler},
                {USS("09. language decompiler"),  decompiler},
                {USS("10. automata performance"), automata},
                {USS("11. grid connection"),      grid},
        };

        icu::UnicodeString IntegrationTests::name() {
            return USS("integration");
        }

        std::map<icu::UnicodeString, std::map<icu::UnicodeString, testCase>> &IntegrationTests::categories() {
            return integration;
        }
    }
}
