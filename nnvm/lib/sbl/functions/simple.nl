foreach N {
    """Accepts a number n and returns 1..n locking itself during the computation.
    Only N >= 1 can trigger computation.
    Returns the current index, a flag if the index is counted and whether it is the last.
    """

    "N == 0, LOCK == 0, UNLOCK == 0 => 0"
    "N >= 1, LOCK == 0, UNLOCK == 0 => 1"
    "N >= 0, LOCK == 1, UNLOCK == 0 => 1"
    "N >= 0, LOCK >= 0, UNLOCK == 1 => 0"
    "Layer: 1,2,..."
    LOCK = # ((- (+ N, LOCK), (* __INF, END)) >= 1), 1

    "N >= 0, LOCK == 0 => N"
    "N >= 0, LOCK == 1 => 0"
    "Layer: 1"
    TRIGGER = # ((- LOCK) >= 0), N

    "Layer: 2,3,..."
    DELAY = # LOCK

    "Track number of ticks"
    "LOCK == 0 => 0"
    "LOCK == 1 => COUNTER++"
    "Layer: 2,3,..."
    COUNTER = # (LOCK >= 1), (+ COUNTER, LOCK)

    "Keep current input"
    "END == 0 => N cycle"
    "END == 1 => 0"
    "Layer: 2,3,..."
    TARGET = # ((- END) >= 0), (+ TRIGGER, TARGET)

    "Ending flag"
    "Layer: 3,4,..."
    END = # ((- COUNTER, TARGET) >= 0), 1
    "Layer: 3,4,..."
    INDEX = # COUNTER
    "Layer: 3,4,..."
    COUNTING = # DELAY

    out INDEX, COUNTING, END
}

factorial N {
    """Accepts a number n and count n! locking itself during the computation.
        Only N >= 1 can trigger computation.
    """

    COUNTER = foreach N:N

    "Accumulate the a*=i, i=n..1. When i reaches 0, a=0"
    ACCUMULATOR = & COUNTER.COUNTING, COUNTER.INDEX, 1

    "Output the result only when it is ready."
    RESULT = # (COUNTER.END >= 1), ACCUMULATOR
    out RESULT
}
