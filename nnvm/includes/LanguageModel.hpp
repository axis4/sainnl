/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#pragma once

#include "predef.h"

namespace nnvm {
    NNVM_EXPORTED class Operand;
    NNVM_EXPORTED class LanguageTemplate;
    NNVM_EXPORTED struct LanguageHeader;
    NNVM_EXPORTED class ExpressionTree;
    NNVM_EXPORTED class LanguageArgument;
    NNVM_EXPORTED struct LanguageCall;
    NNVM_EXPORTED class LanguageDefinition;
    NNVM_EXPORTED struct LanguageBlock;
    NNVM_EXPORTED struct LanguageTree;
    NNVM_EXPORTED enum class TokenKind;
    NNVM_EXPORTED struct CompileContext;
    NNVM_EXPORTED class LanguageModel;
}

#include <unicode/unistr.h>
#include <inttypes.h>
#include <deque>
#include <vector>
#include <map>
#include <list>
#include <memory>
#include <LR.hpp>
#include <ByteCode.hpp>
#include <LoaderModel.hpp>

namespace nnvm {
    class LanguageNode
    {
    public:
        LanguageNode(std::shared_ptr<ByteCodeLoader> loader);
        LanguageNode(const LanguageNode& obj);

        std::shared_ptr<ByteCodeLoader> loader;
    };

    class LanguageTemplate : public LanguageNode
    {
    public:
        LanguageTemplate(std::shared_ptr<ByteCodeLoader> loader);
        LanguageTemplate(const LanguageTemplate& obj);
        
        icu::UnicodeString name;
        std::shared_ptr<icu::UnicodeString> rename; // NULL if not dynamic generator
        std::shared_ptr<Operand> rangeStart; // NULL if not static generator
        std::shared_ptr<Operand> rangeEnd; // NULL if not static generator
        std::shared_ptr<Operand> condition; // NULL if no condition
    };

    class LanguageHeader : public LanguageNode
    {
    public:
        LanguageHeader(std::shared_ptr<ByteCodeLoader> loader);
        LanguageHeader(const LanguageHeader& obj);

        std::deque<icu::UnicodeString> inputs;
        std::deque<LanguageTemplate> templates;
    };

    enum class TokenKind
    {
        TOKEN_EOF,
        TEMPLATER,
        TEMPLATER_CONTENT,
        LITERAL,
        STRING,
        NUMBER,
        AS_NUMBER,
        COLON,
        LBRACKET,
        RBRACKET,
        OPERATOR,
        LINEAR_OPERATOR,
        FACTOR_OPERATOR,
        FROM,
        EQUAL,
        NOT,
        CONDITIONAL,
        ARG_DELIMITER,
        KEY_ARG,
        ARGS,
        ASSIGN,
        REDUCTION,
        ASSIGN_REDUCTION,
        EXPRESSION,
        CALL,
        DEFINITION,
    };

    class Operand : public LanguageNode
    {
    public:
        Operand(std::shared_ptr<ByteCodeLoader> loader);
        Operand(const Operand& obj);
        
        std::shared_ptr<LanguageCall> call;
        std::shared_ptr<Token<TokenKind>> literal;
        std::shared_ptr<ExpressionTree> expression;
    };

    class ExpressionTree : public LanguageNode
    {
    public:
        ExpressionTree(std::shared_ptr<ByteCodeLoader> loader);
        ExpressionTree(const ExpressionTree& obj);
        
        std::shared_ptr<Operand> left;
        icu::UnicodeString middle;
        std::shared_ptr<Operand> right;
    };

    class LanguageArgument : public LanguageNode
    {
    public:
        LanguageArgument(std::shared_ptr<ByteCodeLoader> loader);
        LanguageArgument(const LanguageArgument& obj);

        Operand operand;
        std::shared_ptr<std::deque<Token<TokenKind> > > rawTokens;
    };

    class LanguageCall : public LanguageNode
    {
    public:
        LanguageCall(std::shared_ptr<ByteCodeLoader> loader);
        LanguageCall(const LanguageCall& obj);

        icu::UnicodeString blockName;
        icu::UnicodeString extractedName;
        std::deque<LanguageArgument> arguments;
        std::deque<LanguageArgument> templateArguments;
        std::deque<std::deque<Token<TokenKind> > > rawTokens; //array of tokens per argument
    };

    class LanguageDefinition : public LanguageNode
    {
    public:
        LanguageDefinition(std::shared_ptr<ByteCodeLoader> loader);
        LanguageDefinition(const LanguageDefinition& obj);
        
        std::deque<icu::UnicodeString> templateNames;
        std::shared_ptr<icu::UnicodeString> name; // NULL if no "A = "
        int8_t reduction;
        Operand operand;
        
        std::map<icu::UnicodeString, int64_t> fixedIndexState;
    };

    class LanguageBlock : public LanguageNode
    {
    public:
        LanguageBlock(std::shared_ptr<ByteCodeLoader> loader);
        LanguageBlock(const LanguageBlock& obj);

        icu::UnicodeString name;
        LanguageHeader header;
        std::deque<LanguageDefinition> definitions;
    };

    class LanguageConst : public LanguageNode
    {
    public:
        LanguageConst(std::shared_ptr<ByteCodeLoader> loader);
        LanguageConst(const LanguageConst& obj);

        icu::UnicodeString name;
        double value;
    };

    class LanguageTree : public LanguageNode
    {
    public:
        LanguageTree(std::shared_ptr<ByteCodeLoader> loader);
        LanguageTree(const LanguageTree& obj);

        std::deque<LanguageConst> consts;
        std::deque<LanguageBlock> blocks;
    };

    struct CompileContext
    {
        icu::UnicodeString prefix;
        std::map<icu::UnicodeString, double>* context;
        std::map<icu::UnicodeString, icu::UnicodeString>* remapOutputs;
        std::deque<LanguageDefinition> definitions;
    };

    class LanguageModel
    {
    public:
        static Token<TokenKind> fullCopy(const Token<TokenKind>& token);
        static std::deque<Token<TokenKind>> fullCopy(const std::deque<Token<TokenKind>>& tokens);
        static Operand fullCopy(const Operand& call);
        static LanguageArgument fullCopy(const LanguageArgument& argument);
        static std::deque<LanguageArgument> fullCopy(const std::deque<LanguageArgument>& arguments);
        static LanguageCall fullCopy(const LanguageCall& call);
        static ExpressionTree fullCopy(const ExpressionTree& expression);
        static std::deque<LanguageTemplate> fullCopy(const std::deque<LanguageTemplate>& templates);
        static LanguageHeader fullCopy(const LanguageHeader& header);
        static LanguageDefinition fullCopy(const LanguageDefinition& definition);
        static LanguageBlock fullCopy(const LanguageBlock& block);

        static icu::UnicodeString toString(const Token<TokenKind>& token);
        static icu::UnicodeString toString(const std::deque<Token<TokenKind>>& tokens);
        static icu::UnicodeString toString(const Operand& o, int8_t is_top);
        static icu::UnicodeString toString(const ExpressionTree& e);
        static icu::UnicodeString toString(const LanguageArgument& a);
        static icu::UnicodeString toString(const LanguageCall& c);
        static icu::UnicodeString toString(const LanguageDefinition& d);
        static icu::UnicodeString toString(const LanguageBlock& b);

        static std::shared_ptr<ByteCode> loadBlock(icu::UnicodeString blockName, icu::UnicodeString prefix, CompileContext& cctx, std::shared_ptr<Context> ctx, std::map<icu::UnicodeString, double>& templateContext);
    };
}
