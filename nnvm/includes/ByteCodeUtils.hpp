/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#pragma once

#include "predef.h"

namespace nnvm {
    NNVM_EXPORTED class ByteCodeUtils;
}

#include <unicode/unistr.h>
#include <inttypes.h>
#include <memory>
#include <Disposable.hpp>
#include <ByteCode.hpp>
#include <ByteCodeBlock.hpp>
#include <ByteCodeBlockItem.hpp>
#include <ByteCodeParameter.hpp>

namespace nnvm {
    class ByteCodeUtils
    {
    public:
        static double asDouble(const ByteCodeParameter& param);
        static icu::UnicodeString asString(const ByteCodeParameter& param);
        static icu::UnicodeString asString(const int8_t value);
        static icu::UnicodeString asString(const int64_t value);
        static icu::UnicodeString asString(const double value);
        static double asNumber(const icu::UnicodeString& s);
        static int64_t asHexNumber(const icu::UnicodeString& s);
        static void finalizeCode(std::shared_ptr<ByteCode> code);
        static int8_t isInIncomes(std::shared_ptr<ByteCodeBlockItem> edge, std::deque<std::shared_ptr<ByteCodeBlockItem>>::iterator incomeStart, std::deque<std::shared_ptr<ByteCodeBlockItem>>::iterator incomeEnd);
        static int8_t isInItems(std::shared_ptr<ByteCodeBlockItem> edge, std::deque<std::shared_ptr<ByteCodeBlockItem>>::iterator itemStart, std::deque<std::shared_ptr<ByteCodeBlockItem>>::iterator itemEnd);
        static int8_t isInItems(std::shared_ptr<ByteCodeBlockItem> edge);
        static void reveal(std::shared_ptr<ByteCodeBlock> block);
        static void reveal(std::vector<std::shared_ptr<ByteCodeParameter> > parameters);
    };
}
