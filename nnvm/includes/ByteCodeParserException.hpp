/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#pragma once

#include "predef.h"

namespace nnvm {
    NNVM_EXPORTED class ByteCodeParserException;
}

#include <inttypes.h>
#include <exception>
#include <unicode/unistr.h>

namespace nnvm {
    class ByteCodeParserException : public std::runtime_error
    {
    public:
        ByteCodeParserException(const icu::UnicodeString& source, const icu::UnicodeString& message, int16_t state, int64_t symbol);
    private:
        icu::UnicodeString source;
        icu::UnicodeString message;
        int16_t state;
        int64_t symbol;
    };
}
