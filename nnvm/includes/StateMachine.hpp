/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#pragma once

#include "predef.h"

namespace nnvm {
    template<typename TSource, typename TRawAtom, typename TCarried, typename TResult> NNVM_EXPORTED class State;
    template<typename TSource, typename TRawAtom, typename TCarried, typename TResult> NNVM_EXPORTED struct ErrorHandlers;
    template<typename TSource, typename TRawAtom, typename TCarried, typename TResult> NNVM_EXPORTED class StateMachine;
    NNVM_EXPORTED struct DataSource;
    template<typename TCarried, typename TResult> NNVM_EXPORTED class StringMachine;
}

#include <iostream>
#include <memory>
#include <vector>
#include <functional>
#include <unicode/unistr.h>
#include <StringConv.hpp>

namespace nnvm {
    const int16_t MAX_STEP = 16960;

    template<typename TSource, typename TRawAtom, typename TCarried, typename TResult>
    class State
    {
    public:
        using stateReader = std::function<int64_t(std::shared_ptr<TSource>, std::vector<TRawAtom>&)>;
        using stateMatcher = std::function<int8_t(std::vector<TRawAtom>&, int64_t)>;
        using stateFiller = std::function<int8_t(std::vector<TRawAtom>&, int64_t&, std::shared_ptr<TCarried>&, std::shared_ptr<TResult>&)>;
        using stateStepper = std::function<int8_t(std::vector<TRawAtom>&, int64_t, std::shared_ptr<TCarried>, int16_t&)>;
        using stateError = std::function<icu::UnicodeString(std::vector<TRawAtom>&, int16_t, int64_t)>;
        
        stateReader reader;
        stateMatcher matcher;
        stateFiller filler;
        stateStepper stepper;
        stateError error;
        int8_t terminal;
        
        struct Readers {
            struct pass {
                stateReader next = [](std::shared_ptr<TSource> source, std::vector<TRawAtom>& data) -> int64_t { return 0; };
            };
        };
        
        // default matchers
        struct Matches {
            struct alwaysTrue {
                stateMatcher next =
                [](std::vector<TRawAtom>& data, int64_t size) -> int8_t { return 1; };
            };
            template<TRawAtom condition>
            struct isAtom1 {
                stateMatcher next =
                [](std::vector<TRawAtom>& data, int64_t size) -> int8_t
                {
                    return data[0] == condition && size == 1 ? 1 : -1;
                };
            };
            template<TRawAtom condition, TRawAtom condition2>
            struct isAtom2 {
                stateMatcher next =
                [](std::vector<TRawAtom>& data, int64_t size) -> int8_t
                {
                    return (data[0] == condition || data[0] == condition2) && size == 1 ? 1 : -1;
                };
            };
            template<TRawAtom condition, TRawAtom condition2, TRawAtom condition3>
            struct isAtom3 {
                stateMatcher next =
                [](std::vector<TRawAtom>& data, int64_t size) -> int8_t
                {
                    return (data[0] == condition || data[0] == condition2 || data[0] == condition3) && size == 1 ? 1 : -1;
                };
            };
        };
        
        // default fillers
        struct Fills {
            stateFiller noop =
            [](std::vector<TRawAtom>& data, int64_t& size, std::shared_ptr<TCarried>& carried, std::shared_ptr<TResult>& result) -> int8_t
            {
                return 1;
            };
        };

        // default conditional steps
        struct Steps {
            template <int64_t default_step>
            struct steps0 {
                stateStepper next =
                [](std::vector<TRawAtom>& data, int64_t size, std::shared_ptr<TCarried> carried, int16_t& state) -> int8_t
                {
                    if (size == 0) { state = MAX_STEP; }
                    else { state = default_step; }
                    return 1;
                };
            };
            struct stepsInc {
                stateStepper next =
                [](std::vector<TRawAtom>& data, int64_t size, std::shared_ptr<TCarried> carried, int16_t& state) -> int8_t
                {
                    if (size == 0) { state = MAX_STEP; }
                    else { state++; }
                    return 1;
                };
            };
            template <TRawAtom condition, int64_t step, int64_t default_step>
            struct steps1 {
                stateStepper next =
                [](std::vector<TRawAtom>& data, int64_t size, std::shared_ptr<TCarried> carried, int16_t& state) -> int8_t
                {
                    if (size == 0) { state = MAX_STEP; }
                    else { state = data[0] == condition ? step : default_step; }
                    return 1;
                };
            };
            template <TRawAtom condition, int64_t step, TRawAtom condition2, int64_t step2, int64_t default_step, int8_t require_conditions>
            struct steps2 {
                stateStepper next =
                [](std::vector<TRawAtom>& data, int64_t size, std::shared_ptr<TCarried> carried, int16_t& state) -> int8_t
                {
                    if (size == 0) { state = MAX_STEP; }
                    else {
                        if (require_conditions && data[0] != condition && data[0] != condition2) {
                            return -1;
                        }
                        state = data[0] == condition
                        ? step
                        : (data[0] == condition2 ? step2 : default_step);
                    }
                    return 1;
                };
            };
            template <TRawAtom condition, int64_t step, TRawAtom condition2, int64_t step2, TRawAtom condition3, int64_t step3, int64_t default_step, int8_t require_conditions>
            struct steps3 {
                stateStepper next =
                [](std::vector<TRawAtom>& data, int64_t size, std::shared_ptr<TCarried> carried, int16_t& state) -> int8_t
                {
                    if (size == 0) { state = MAX_STEP; }
                    else {
                        if (require_conditions && data[0] != condition && data[0] != condition2 && data[0] != condition3) {
                            return -1;
                        }
                        state = data[0] == condition
                        ? step
                        : (data[0] == condition2
                           ? step2
                           : (data[0] == condition3 ? step3 : default_step));
                    }
                    return 1;
                };
            };
            template <TRawAtom condition, int64_t step, TRawAtom condition2, int64_t step2, TRawAtom condition3, int64_t step3, TRawAtom condition4, int64_t step4, int64_t default_step, int8_t require_conditions>
            struct steps4 {
                stateStepper next =
                [](std::vector<TRawAtom>& data, int64_t size, std::shared_ptr<TCarried> carried, int16_t& state) -> int8_t
                {
                    if (size == 0) { state = MAX_STEP; }
                    else {
                        if (require_conditions && data[0] != condition && data[0] != condition2
                            && data[0] != condition3 && data[0] != condition4) {
                            return -1;
                        }
                        state = data[0] == condition
                        ? step
                        : (data[0] == condition2
                           ? step2
                           : (data[0] == condition3
                              ? step3
                              : (data[0] == condition4 ? step4 : default_step)));
                    }
                    return 1;
                };
            };
            template <TRawAtom condition, int64_t step, TRawAtom condition2, int64_t step2, TRawAtom condition3, int64_t step3, TRawAtom condition4, int64_t step4, TRawAtom condition5, int64_t step5, int64_t default_step, int8_t require_conditions>
            struct steps5 {
                stateStepper next =
                [](std::vector<TRawAtom>& data, int64_t size, std::shared_ptr<TCarried> carried, int16_t& state) -> int8_t
                {
                    if (size == 0) { state = MAX_STEP; }
                    else {
                        if (require_conditions && data[0] != condition && data[0] != condition2
                            && data[0] != condition3 && data[0] != condition4 && data[0] != condition5) {
                            return -1;
                        }
                        state = data[0] == condition
                        ? step
                        : (data[0] == condition2
                           ? step2
                           : (data[0] == condition3
                              ? step3
                              : (data[0] == condition4
                                 ? step4
                                 : (data[0] == condition5 ? step5 : default_step))));
                    }
                    return 1;
                };
            };
            template <TRawAtom condition, int64_t step, TRawAtom condition2, int64_t step2, TRawAtom condition3, int64_t step3, TRawAtom condition4, int64_t step4, TRawAtom condition5, int64_t step5, TRawAtom condition6, int64_t step6, int64_t default_step, int8_t require_conditions>
            struct steps6 {
                stateStepper next =
                [](std::vector<TRawAtom>& data, int64_t size, std::shared_ptr<TCarried> carried, int16_t& state) -> int8_t
                {
                    if (size == 0) { state = MAX_STEP; }
                    else {
                        if (require_conditions && data[0] != condition && data[0] != condition2
                            && data[0] != condition3 && data[0] != condition4 && data[0] != condition5
                            && data[0] != condition6) {
                            return -1;
                        }
                        state = data[0] == condition
                        ? step
                        : (data[0] == condition2
                           ? step2
                           : (data[0] == condition3
                              ? step3
                              : (data[0] == condition4
                                 ? step4
                                 : (data[0] == condition5
                                    ? step5
                                    : (data[0] == condition6 ? step6 : default_step)))));
                    }
                    return 1;
                };
            };
        };
    };

    template<typename TSource, typename TRawAtom, typename TCarried, typename TResult>
    struct ErrorHandlers
    {
        using stateError = std::function<icu::UnicodeString(std::vector<TRawAtom>&, int16_t, int64_t)>;

        std::function<void(std::shared_ptr<TCarried>&)> noSetup;
        std::function<void(int16_t, std::shared_ptr<std::vector<int16_t> >)> noParse;
        std::function<void(stateError&, std::vector<TRawAtom>&, int16_t, std::shared_ptr<std::vector<int16_t> >)> noRead;
        std::function<void(stateError&, std::vector<TRawAtom>&, int16_t, std::shared_ptr<std::vector<int16_t> >)> noMatch;
        std::function<void(stateError&, std::vector<TRawAtom>&, int16_t, std::shared_ptr<std::vector<int16_t> >)> noFill;
        std::function<void(stateError&, std::vector<TRawAtom>&, int16_t, std::shared_ptr<std::vector<int16_t> >)> noStep;
        std::function<void(std::shared_ptr<TCarried>&, std::shared_ptr<TResult>&, int16_t, std::shared_ptr<std::vector<int16_t> >)> noTerminal;
    };

    template<typename TSource, typename TRawAtom, typename TCarried, typename TResult>
    class StateMachine
    {
    public:
        using setupFiller = std::function<int8_t(std::shared_ptr<TCarried>&)>;
        using terminalFiller = std::function<int8_t(std::shared_ptr<TCarried>&, std::shared_ptr<TResult>&)>;
        
        StateMachine(std::vector<State<TSource, TRawAtom, TCarried, TResult> > states,
                ErrorHandlers<TSource, TRawAtom, TCarried, TResult> errorHandlers) {
            this->states = states;
            this->errorHandlers = errorHandlers;
            this->stateDebug = std::make_shared<std::vector<int16_t> >();
            this->setup = [](std::shared_ptr<TCarried>& c) { return 1; };
            this->terminal = [](std::shared_ptr<TCarried>& c, std::shared_ptr<TResult>& r) { return 1; };
        }
        StateMachine(std::vector<State<TSource, TRawAtom, TCarried, TResult> > states,
                ErrorHandlers<TSource, TRawAtom, TCarried, TResult> errorHandlers,
                setupFiller setup, terminalFiller terminal) {
            this->states = states;
            this->errorHandlers = errorHandlers;
            this->stateDebug = std::make_shared<std::vector<int16_t> >();
            this->setup = setup;
            this->terminal = terminal;
        }
        StateMachine(std::vector<State<TSource, TRawAtom, TCarried, TResult> > states,
                ErrorHandlers<TSource, TRawAtom, TCarried, TResult> errorHandlers,
                setupFiller setup, terminalFiller terminal, int64_t defaultBufferSize) {
            this->states = states;
            this->errorHandlers = errorHandlers;
            this->DEFAULT_SIZE = defaultBufferSize;
            this->stateDebug = std::make_shared<std::vector<int16_t> >();
            this->setup = setup;
            this->terminal = terminal;
        }
        StateMachine(std::vector<State<TSource, TRawAtom, TCarried, TResult> > states,
                ErrorHandlers<TSource, TRawAtom, TCarried, TResult> errorHandlers,
                setupFiller setup, terminalFiller terminal, std::shared_ptr<std::vector<int16_t> > stateDebug) {
            this->states = states;
            this->errorHandlers = errorHandlers;
            this->stateDebug = stateDebug;
            this->setup = setup;
            this->terminal = terminal;
        }
        ~StateMachine() {
            if (this->stateDebug) {
                //for (auto i : *this->stateDebug) {
                //    std::cout << i << ", ";
                //}
                //std::cout << std::endl;
            }
        }
        std::shared_ptr<TResult> run(std::shared_ptr<TSource> dataSource) {
            std::shared_ptr<TCarried> carried = nullptr;
            return run(dataSource, carried);
        }
        std::shared_ptr<TResult> run(std::shared_ptr<TSource> dataSource, std::shared_ptr<TCarried>& carried) {
            if (!this->setup(carried)) {
                this->errorHandlers.noSetup(carried);
            }
            std::shared_ptr<TResult> result = nullptr;
            int16_t stateIndex = 0;
            State<TSource, TRawAtom, TCarried, TResult>* prevState = nullptr;
            std::vector<TRawAtom> buffer(DEFAULT_SIZE);
            if (this->stateDebug) {
                this->stateDebug->push_back(stateIndex);
            }
            while (true) {
                if (stateIndex < 0) {
                    break;
                }
                if (stateIndex >= this->states.size()) {
                    if (!prevState || !prevState->terminal) {
                        this->errorHandlers.noParse(stateIndex, this->stateDebug);
                    }
                    break;
                }

                State<TSource, TRawAtom, TCarried, TResult>* state = (&this->states[0]) + stateIndex;
                buffer.clear();
                
                auto size = state->reader(dataSource, buffer);
                if (size < 0) {
                    if (stateIndex > 0) {
                        this->errorHandlers.noRead(state->error, buffer, stateIndex, this->stateDebug);
                    }
                    break;
                }
                int8_t m = state->matcher(buffer, size);
                if (m < 0) {
                    this->errorHandlers.noMatch(state->error, buffer, stateIndex, this->stateDebug);
                    break;
                }
                if (!state->filler(buffer, size, carried, result)) {
                    this->errorHandlers.noFill(state->error, buffer, stateIndex, this->stateDebug);
                    break;
                }
                if (!state->stepper(buffer, size, carried, stateIndex)) {
                    this->errorHandlers.noStep(state->error, buffer, stateIndex, this->stateDebug);
                    break;
                }
                if (this->stateDebug) {
                    this->stateDebug->push_back(stateIndex);
                }
                prevState = state;
            }
            if (!this->terminal(carried, result)) {
                this->errorHandlers.noTerminal(carried, result, stateIndex, this->stateDebug);
            }
            return result;
        }
    private:
        std::vector<State<TSource, TRawAtom, TCarried, TResult> > states;
        terminalFiller terminal;
        setupFiller setup;
        ErrorHandlers<TSource, TRawAtom, TCarried, TResult> errorHandlers;
        const int64_t DEFAULT_SIZE = 1024;
        std::shared_ptr<std::vector<int16_t> > stateDebug;
    };

    template<typename TSource, typename TRawAtom, typename TCarried, typename TResult>
    class SubMachine : public State<TSource, TRawAtom, TCarried, TResult>
    {
    public:
        SubMachine(std::vector<State<TSource, TRawAtom, TCarried, TResult> > subStates,
            ErrorHandlers<TSource, TRawAtom, TCarried, TResult> subErrorHandlers,
            int64_t nextStep, int8_t terminal)
            : State<TSource, TRawAtom, TCarried, TResult>(),
            nextStep(nextStep), subStates(subStates), subErrorHandlers(subErrorHandlers)
        {
            this->reader = std::bind(&SubMachine::stubRead, this, std::placeholders::_1, std::placeholders::_2);
            this->matcher = std::bind(&SubMachine::stubMatch, this, std::placeholders::_1, std::placeholders::_2);
            this->filler = std::bind(&SubMachine::subCall, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4);
            this->stepper = std::bind(&SubMachine::stepOut, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4);
            this->error = std::bind(&SubMachine::stubError, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
            this->terminal = terminal;
        }
    private:
        using setupFiller = std::function<int8_t(std::shared_ptr<TCarried>&)>;
        using terminalFiller = std::function<int8_t(std::shared_ptr<TCarried>&, std::shared_ptr<TResult>&)>;

        int64_t stubRead(std::shared_ptr<TSource> s, std::vector<TRawAtom>& r) { tempSource = s; return 0; }
        int8_t stubMatch(std::vector<TRawAtom>& r, int64_t s) { return 1; }
        int8_t subCall(std::vector<TRawAtom>& r, int64_t& s, std::shared_ptr<TCarried>& c, std::shared_ptr<TResult>& result)
        {
            setupFiller st = [&c](std::shared_ptr<TCarried>& ci) { ci = c; return 1; };
            terminalFiller t = [&c](std::shared_ptr<TCarried>& ci, std::shared_ptr<TResult>& r) { c = ci; return 1; };
            StateMachine<TSource, TRawAtom, TCarried, TResult> a(this->subStates, this->subErrorHandlers, st, t);
            result = a.run(this->tempSource, c);
            return 1;
        }
        int8_t stepOut(std::vector<TRawAtom>& r, int64_t s, std::shared_ptr<TCarried> c, int16_t& step) { step = nextStep; return 1; }
        icu::UnicodeString stubError(std::vector<TRawAtom>& r, int16_t s, int64_t step) { return USS(""); }
        std::shared_ptr<TSource> tempSource;
        int64_t nextStep;
        std::vector<State<TSource, TRawAtom, TCarried, TResult> > subStates;
        ErrorHandlers<TSource, TRawAtom, TCarried, TResult> subErrorHandlers;
    };

    struct DataSource
    {
        icu::UnicodeString data;
        int64_t pos;
        int64_t n;
    };

    template<typename TCarried, typename TResult>
    using StringState = State<DataSource, UChar32, TCarried, TResult>;

    template<typename TCarried, typename TResult>
    class StringMachine : public StateMachine<DataSource, UChar32, TCarried, TResult>
    {
    public:
        using setupFiller = std::function<int8_t(std::shared_ptr<TCarried>&)>;
        using terminalFiller = std::function<int8_t(std::shared_ptr<TCarried>&, std::shared_ptr<TResult>&)>;

        StringMachine(std::vector<StringState<TCarried, TResult> > states,
                      ErrorHandlers<DataSource, UChar32, TCarried, TResult> errorHandlers)
            : StateMachine<DataSource, UChar32, TCarried, TResult>(states, errorHandlers) {}
        StringMachine(std::vector<StringState<TCarried, TResult> > states,
            ErrorHandlers<DataSource, UChar32, TCarried, TResult> errorHandlers,
            setupFiller setup, terminalFiller terminal)
            : StateMachine<DataSource, UChar32, TCarried, TResult>(states, errorHandlers, setup, terminal) {}
        StringMachine(std::vector<StringState<TCarried, TResult> > states,
            ErrorHandlers<DataSource, UChar32, TCarried, TResult> errorHandlers,
            setupFiller setup, terminalFiller terminal, int64_t defaultBufferSize)
            : StateMachine<DataSource, UChar32, TCarried, TResult>(states, errorHandlers, setup, terminal, defaultBufferSize) {}
        StringMachine(std::vector<StringState<TCarried, TResult> > states,
            ErrorHandlers<DataSource, UChar32, TCarried, TResult> errorHandlers,
            setupFiller setup, terminalFiller terminal, std::shared_ptr<std::vector<int64_t> > stateDebug)
            : StateMachine<DataSource, UChar32, TCarried, TResult>(states, errorHandlers, setup, terminal, stateDebug) {}

        using stateReader = std::function<int64_t(std::shared_ptr<DataSource>, std::vector<UChar32>&)>;
        struct Readers {
            struct readChar {
                stateReader next =
                [](std::shared_ptr<DataSource> dataSource, std::vector<UChar32>& result) -> int64_t
                {
                    if (dataSource->pos >= dataSource->n) {
                        return 0;
                    }
                    auto c = dataSource->data.char32At((int32_t)dataSource->pos);
                    if (result.size() < 1) {
                        result.resize(1);
                    }
                    result[0] = c;
                    dataSource->pos++;
                    return 1;
                };
            };
            struct peekChar {
                stateReader next =
                [](std::shared_ptr<DataSource> dataSource, std::vector<UChar32>& result) -> int64_t
                {
                    if (dataSource->pos >= dataSource->n) {
                        return 0;
                    }
                    auto c = dataSource->data.char32At((int32_t)dataSource->pos);
                    if (result.size() < 1) {
                        result.resize(1);
                    }
                    result[0] = c;
                    return 1;
                };
            };
        };

        std::shared_ptr<TResult> run(const icu::UnicodeString& str) {
            std::shared_ptr<DataSource> iter(new DataSource());
            iter->data = str;
            iter->pos = 0;
            iter->n = str.countChar32();
            return this->StateMachine<DataSource, UChar32, TCarried, TResult>::run(iter);
        }
    };
}
