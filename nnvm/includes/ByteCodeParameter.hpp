/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#pragma once

#include "predef.h"

namespace nnvm {
    NNVM_EXPORTED class ByteCodeParameter;
}

#include <unicode/unistr.h>
#include <memory>
#include <inttypes.h>
#include <Disposable.hpp>

namespace nnvm {
    enum class ByteCodeParameterTypes
    {
        BYTE = 0,
        INTEGER = 1,
        DOUBLE = 2,
        STRING = 3,
        _INTERNAL_OBJECT = 4
    };

    class ByteCodeParameterValue : public Disposable
    {
    public:
        ByteCodeParameterValue(int8_t value);
        ByteCodeParameterValue(int64_t value);
        ByteCodeParameterValue(double value);
        ByteCodeParameterValue(const icu::UnicodeString& value);
        ByteCodeParameterValue(std::shared_ptr<void> value);
        ByteCodeParameterValue(ByteCodeParameterValue& obj);
        int8_t byteValue = 0;
        int64_t integerValue = 0;
        double doubleValue = 0;
        icu::UnicodeString stringValue;
        std::shared_ptr<void> objectValue = nullptr;
    };

    class ByteCodeParameter : public Disposable
    {
    public:
        ByteCodeParameter(const icu::UnicodeString& parameterName, int8_t value);
        ByteCodeParameter(const icu::UnicodeString& parameterName, int64_t value);
        ByteCodeParameter(const icu::UnicodeString& parameterName, double value);
        ByteCodeParameter(const icu::UnicodeString& parameterName, const icu::UnicodeString& value);
        ByteCodeParameter(const icu::UnicodeString& parameterName, std::shared_ptr<void> value);
        ByteCodeParameter(ByteCodeParameter& obj);
        virtual ~ByteCodeParameter();
        const icu::UnicodeString& getName() const;
        ByteCodeParameterTypes getType() const;
        ByteCodeParameterValue value;
    private:
        icu::UnicodeString name;
        ByteCodeParameterTypes type;
    };
}
