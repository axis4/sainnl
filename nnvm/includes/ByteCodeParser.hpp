/*
 Copyright 2013-2016 Sergey Ionov

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#pragma once

#include "predef.h"

namespace nnvm
{
    NNVM_EXPORTED class ByteCodeParser;
}

#include <inttypes.h>
#include <istream>
#include <memory>
#include <exception>
#include <Disposable.hpp>
#include <StateMachine.hpp>
#include <ByteCode.hpp>
#include <MachineSetup.hpp>
#include <LibraryLoader.hpp>
#include <ByteCodeLoader.hpp>

namespace nnvm {
    class ByteCodeParser : public Disposable
    {
    public:
        ByteCodeParser(std::shared_ptr<std::istream> stream, icu::UnicodeString prefix, icu::UnicodeString package, std::shared_ptr<Context> ctx);
        virtual ~ByteCodeParser();
        std::shared_ptr<std::vector<std::shared_ptr<ByteCode>>> read();
        static int8_t isWhiteSpace(const unsigned char c);

    private:
        std::shared_ptr<std::istream> stream;
        icu::UnicodeString prefix;
        icu::UnicodeString package;
        std::shared_ptr<Context> ctx;
    };
}
