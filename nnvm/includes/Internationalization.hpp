/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#pragma once

#include "predef.h"

namespace nnvm {
    NNVM_EXPORTED class InternationalizationException;
    NNVM_EXPORTED class Internationalization;
}

#include <map>
#include <unicode/unistr.h>
#include <exception>
#include <Disposable.hpp>

#define _i(x) nnvm::Internationalization::default_state()[icu::UnicodeString(x)]

namespace nnvm {
    class InternationalizationException : public std::runtime_error
    {
    public:
        InternationalizationException(const icu::UnicodeString& reason);
    protected:
        icu::UnicodeString reason;
    };

    class Internationalization : public Disposable
    {
    public:
        static Internationalization default_state();
        static Internationalization produce(const icu::UnicodeString& file);
        Internationalization();
        Internationalization(const Internationalization& obj);
        ~Internationalization() override;
        icu::UnicodeString operator[](const icu::UnicodeString& key);
    private:
        explicit Internationalization(const icu::UnicodeString& file);
        std::map<icu::UnicodeString, icu::UnicodeString> items;
        static std::map<icu::UnicodeString, Internationalization> cache;
    };
}
