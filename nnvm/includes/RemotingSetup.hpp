/*
 Copyright 2013-2016 Sergey Ionov

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#pragma once

#include "predef.h"

namespace nnvm {
    NNVM_EXPORTED class RemotingSetup;
}

#include <unicode/unistr.h>
#include <inttypes.h>
#include <Disposable.hpp>

namespace nnvm {
    class RemotingSetup : public Disposable
    {
    public:
        RemotingSetup();
        RemotingSetup(const RemotingSetup& obj);
        virtual ~RemotingSetup();

        icu::UnicodeString listenAddress;
        int32_t listenPort;
        int16_t poolSize;
    };
}
