/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#pragma once

#include "predef.h"

namespace nnvm {
    NNVM_EXPORTED enum class ProcessorMode;
    NNVM_EXPORTED enum class ProcessorParallelMode;
    NNVM_EXPORTED class ProcessorSetup;
}

#include <vector>
#include <map>
#include <unicode/unistr.h>
#include <thread>
#include <inttypes.h>
#include <Disposable.hpp>
#include <ByteCodeBlock.hpp>
#include <Logger.hpp>
#include <Templater.hpp>
#include <MachineSetup.hpp>
#include <ExecutableNeuron.hpp>

namespace nnvm {
    enum class ProcessorMode
    {
        BYTE = 0,
        INTEGER = 1,
        REAL = 2
    };

    enum class ProcessorParallelMode
    {
        NONE = 0,
        OMP = 1,
        OCL = 2,
    };

    class ProcessorSetup : public Disposable
    {
    public:
        typedef std::function<int8_t(icu::UnicodeString, double&)> getConstant;
        ProcessorSetup();
        ProcessorSetup(const ProcessorSetup& obj);
        ProcessorSetup(std::shared_ptr<Templater> templateStrategy);
        virtual ~ProcessorSetup();
        virtual void preSetup(std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock> > >& codes);
        void addNeuron(const icu::UnicodeString& name, std::shared_ptr<ExecutableNeuron> code);
        std::shared_ptr<ExecutableNeuron> getNeuron(const icu::UnicodeString& name);
        std::map<icu::UnicodeString, std::shared_ptr<ExecutableNeuron> >::iterator getNeurons();
        std::map<icu::UnicodeString, std::shared_ptr<ExecutableNeuron> >::iterator getNeuronsEnd();
        void offsetByMode(std::shared_ptr<ByteCodeParameter>& param);
        std::shared_ptr<Templater> getTemplateStrategy();
        void setConstantGetter(getConstant constants);
        std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock>>> getCodes();
        int8_t verbose;
        int8_t exitCode;
        int8_t running;
        int8_t tolerate_missing;
        ProcessorParallelMode parallelMode;
        ProcessorMode mode;
        int8_t remoteSync;
        int8_t allowSync;
        int8_t allowDebug;
        int8_t allowDistributed;
        int8_t allowRemoteInit;
        int64_t tick;
    protected:
        std::shared_ptr<Templater> templateStrategy;
        getConstant constants;
        std::map<icu::UnicodeString, std::shared_ptr<ExecutableNeuron> > neurons;
        std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock>>> running_codes;
    };
}
