/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#pragma once

#include "predef.h"

namespace nnvm {
    NNVM_EXPORTED class StringConvUtils;
}

#include <vector>
#include <unicode/unistr.h>

////#define USS(x) StringConvUtils::wtou(std::wstring(L ## x))

#define USS(x) UNICODE_STRING_SIMPLE(x)

namespace nnvm {
    class StringConvUtils
    {
    public:
        static std::u32string wtou32(const std::wstring& v);
        static std::wstring u32tow(const std::u32string& v);

        static std::string wtou8(const std::wstring& v);
        static std::wstring u8tow(const std::string& v);

        static icu::UnicodeString u8tou(const std::string& v);
        static std::string utou8(const icu::UnicodeString& v);

        static icu::UnicodeString wtou(const std::wstring& v);
        static std::wstring utow(const icu::UnicodeString& v);

        static UChar u8tou(const unsigned char& v);
        static unsigned char utou8(const UChar& v);

        static std::string utoescaped(const icu::UnicodeString& v, int8_t asciiSafe = false);

        static icu::UnicodeString asUnicodeString(std::vector<UChar32>::const_iterator begin, std::vector<UChar32>::const_iterator end);
        static icu::UnicodeString asUnicodeString(const UChar32* begin, int64_t count);
    };
}
