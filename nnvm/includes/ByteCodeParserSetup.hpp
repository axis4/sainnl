/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#pragma once

#include "predef.h"

namespace nnvm {
    NNVM_EXPORTED class ByteCodeParserSetup;
}

#include <unicode/unistr.h>
#include <map>
#include <set>
#include <memory>
#include <Disposable.hpp>
#include <ParsedNeuron.hpp>

namespace nnvm {
    class ByteCodeParserSetup : public Disposable
    {
    public:
        std::map<icu::UnicodeString, icu::UnicodeString> aliases;
        std::set<icu::UnicodeString> arithmeticNeuronNames;
        icu::UnicodeString libraryNeuron;
        icu::UnicodeString primitiveNeuron;
        icu::UnicodeString additionNeuron;
        icu::UnicodeString multiplicationNeuron;
        std::map<icu::UnicodeString, std::shared_ptr<ParsedNeuron> > neurons;
        
        ByteCodeParserSetup();
        ByteCodeParserSetup(const ByteCodeParserSetup& obj);
    };
}
