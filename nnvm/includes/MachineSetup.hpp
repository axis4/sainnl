/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#pragma once

#include "predef.h"

NNVM_EXPORTED struct MachineVersion;

namespace nnvm {
    NNVM_EXPORTED class MachineSetup;
    NNVM_EXPORTED class MachineSetupData;
    NNVM_EXPORTED enum class MachineArch;
    NNVM_EXPORTED enum class MachineBuild;
    NNVM_EXPORTED enum class MachineOS;
    NNVM_EXPORTED enum class MachineBit;
}

#include <memory>
#include <vector>
#include <iostream>
#include <inttypes.h>
#include <unicode/unistr.h>
#include <ProcessorSetup.hpp>
#include <ByteCodeLoaderSetup.hpp>
#include <ByteCodeWriterSetup.hpp>
#include <ByteCodeParserSetup.hpp>
#include <RemotingSetup.hpp>
#include <Internationalization.hpp>
#include <Counters.hpp>
#include <Setup.hpp>

struct MachineVersion
{
public:
    int64_t major;
    int64_t minor;
};

namespace nnvm {
    class MachineSetup : public Setup
    {
    public:
        MachineSetup(std::shared_ptr<ProcessorSetup> processorSetup = nullptr, std::shared_ptr<ByteCodeLoaderSetup> loaderSetup = nullptr, std::shared_ptr<ByteCodeParserSetup> parserSetup = nullptr, std::shared_ptr<ByteCodeWriterSetup> writerSetup = nullptr, std::shared_ptr<RemotingSetup> remotingSetup = nullptr, std::vector<icu::UnicodeString> paths = { _i("default.libs") }, std::istream& stdIn = std::cin, std::ostream& stdOut = std::cout);
        MachineSetup(std::shared_ptr<Logger> logger, std::shared_ptr<ProcessorSetup> processorSetup = nullptr, std::shared_ptr<ByteCodeLoaderSetup> loaderSetup = nullptr, std::shared_ptr<ByteCodeParserSetup> parserSetup = nullptr, std::shared_ptr<ByteCodeWriterSetup> writerSetup = nullptr, std::shared_ptr<RemotingSetup> remotingSetup = nullptr, std::vector<icu::UnicodeString> paths = { _i("default.libs") }, std::istream& stdIn = std::cin, std::ostream& stdOut = std::cout);
        std::shared_ptr<ProcessorSetup> getProcessorSetup();
        std::shared_ptr<ByteCodeWriterSetup> getByteCodeWriterSetup();
        std::shared_ptr<ByteCodeLoaderSetup> getByteCodeLoaderSetup();
        std::shared_ptr<ByteCodeParserSetup> getByteCodeParserSetup();
        std::shared_ptr<RemotingSetup> getRemotingSetup();
        std::vector<icu::UnicodeString> getLibPaths();
        std::istream& getStdIn();
        std::ostream& getStdOut();
        std::vector<SampleSetting> getDefaultSampling();
        int8_t getConstant(icu::UnicodeString name, double& value);
        int8_t isRuntimeConstant(icu::UnicodeString name);
        std::map<icu::UnicodeString, std::tuple<int8_t, double> >::const_iterator getConstants();
        MachineVersion version();
        int8_t versionsEqual(const MachineVersion& v1, const MachineVersion& v2);
        int8_t versionsCompatible(const MachineVersion& v1, const MachineVersion& v2);
        MachineArch arch();
        MachineBuild build();
        MachineOS os();
        MachineBit bit();
        int64_t bitMultiplier();

    private:
        std::shared_ptr<MachineSetupData> data;
        void setDefaultSampling();
        void setMachineConstants();
    };

    enum class MachineArch
    {
        NNHM,
        ARM,
        Atmel,
        PIC,
        Power,
        RISC,
        SPARC,
        X86,
        X86_64,
        X64,
        Itanium,
        Cell
    };
    enum class MachineBuild
    {
        Debug,
        Release
    };
    enum class MachineOS
    {
        AIX,
        ANDROID,
        AMIGA,
        BEOS,
        BLUE_GENE,
        BSD,
        CONVEX,
        LINUX,
        HI_UX,
        HP_UX,
        OS400,
        LYNX,
        OSX,
        IOS,
        MINIX,
        DOS,
        NUCLEUS,
        OS2,
        PALM,
        PLAN9,
        QNX,
        SOLARIS,
        SYMBIAN,
        WINDOWS,
        WINDOWS_CE,
        ZOS,
        UNIX
    };
    enum class MachineBit
    {
        X8,
        X16,
        X32,
        X64,
        X128
    };
}
