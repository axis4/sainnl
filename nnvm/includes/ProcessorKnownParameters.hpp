/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#pragma once

#include "predef.h"

namespace nnvm {
    NNVM_EXPORTED class LinkParams;
    NNVM_EXPORTED class ElementParams;
}

#include <unicode/unistr.h>

namespace nnvm {
    class LinkParams
    {
    public:
        static icu::UnicodeString ACTIVATION;
        static icu::UnicodeString STREAM;
        static icu::UnicodeString GENERATION;
    };

    class ElementParams
    {
    public:
        static icu::UnicodeString SUM_SUFFIX;
        static icu::UnicodeString SOURCE_SUFFIX;

        static icu::UnicodeString INPUT;
        static icu::UnicodeString OUTPUT;
        static icu::UnicodeString TEMPLATE;
        static icu::UnicodeString EXPECTED;
        static icu::UnicodeString INTERNAL_STATE;
        static icu::UnicodeString BIAS;
        static icu::UnicodeString INITIAL_STATE;
        static icu::UnicodeString THRESHOLD;
        static icu::UnicodeString SUM;
        static icu::UnicodeString INDEX;
    };
}
