/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#pragma once

#include "predef.h"

namespace nnvm {
    NNVM_EXPORTED class SampleSetting;
    NNVM_EXPORTED class Sample;
    NNVM_EXPORTED class Quantile;
    NNVM_EXPORTED class Counter;
    NNVM_EXPORTED class Counters;
}

#include <unicode/unistr.h>
#include <vector>
#include <map>
#include <memory>
#include <inttypes.h>

namespace nnvm {
    class SampleSetting
    {
    public:
        SampleSetting(double quantile, double error);
        double quantile;
        double error;
    };

    class Sample
    {
    public:
        Sample(double value, int64_t lowDiff, int64_t highDiff, int64_t accumulated);
        double value;
        int64_t lowDiff;
        int64_t highDiff;
        int64_t accumulated;
    };

    class Quantile
    {
    public:
        Quantile(double quantile, double value);
        double quantile;
        double value;
    };

    class Counter
    {
    public:
        Counter(std::vector<SampleSetting> sampling);
        Counter(const Counter& obj);
        double min; // ns
        double max; // ns
        double avg; // ns
        double sum; // ns sum
        int64_t sampleCount;
        std::vector<Sample> samples;
        void incCounter(double sample);
        std::vector<Quantile> quantiles();
    private:
        std::vector<SampleSetting> sampling;
        double quantileGreatest(int64_t accumulated);
        Quantile quantile(SampleSetting& setting);
    };

    class Counters
    {
    public:
        Counters(std::vector<SampleSetting> sampling);
        Counters(std::vector<SampleSetting> stepSampling, std::vector<SampleSetting> templatePhaseSampling, std::vector<SampleSetting> addedNodesSampling, std::vector<SampleSetting> removedNodesSampling, std::vector<SampleSetting> linkPhaseSampling, std::vector<SampleSetting> nodePhaseSampling, std::vector<SampleSetting> eachNodeSampling, std::vector<SampleSetting> neuronSampling);
        Counters(const Counters& obj);
        Counter step;
        Counter templatePhase;
        Counter addedNodes;
        Counter removedNodes;
        Counter linkPhase;
        Counter nodePhase;
        Counter eachNode;
        std::map<icu::UnicodeString, Counter> nodes;
        int64_t nodeCount;
        int64_t edgeCount;
        int64_t stepCount;
        int64_t uptime; //ns
    };
}
