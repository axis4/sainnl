/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#pragma once

#include "predef.h"

namespace nnvm {
    NNVM_EXPORTED class Logger;
    NNVM_EXPORTED class StdRight;
}

#include <unicode/unistr.h>
#include <Disposable.hpp>

namespace nnvm {
    enum class LogLevel
    {
        Debug,
        Info,
        Warn,
        Fatal
    };

    class Logger : public Disposable
    {
    public:
        Logger();
        Logger(const LogLevel level);
        virtual ~Logger();
        static LogLevel parseLevel(const icu::UnicodeString& level);
        void setLevel(const LogLevel level);
        void debug(const icu::UnicodeString& message);
        void info(const icu::UnicodeString& message);
        void warn(const icu::UnicodeString& message);
        void error(const icu::UnicodeString& message);
        virtual void log(const LogLevel level, const icu::UnicodeString& message) = 0;
    protected:
        LogLevel limitLevel;
    };

    class StdRight : public Logger
    {
    public:
        StdRight();
        StdRight(const LogLevel level);
        virtual void log(const LogLevel level, const icu::UnicodeString& message);
    };
}
