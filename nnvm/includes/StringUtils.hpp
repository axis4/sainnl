/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#pragma once

#include "predef.h"

namespace nnvm {
    NNVM_EXPORTED class CFormatArg;
    NNVM_EXPORTED class UnicodeIterator;
    NNVM_EXPORTED class StringUtils;
}

#include <vector>
#include <unicode/unistr.h>
#include <memory>
#include <inttypes.h>

#if defined(_WIN32) || defined(_WIN64)
// If defined, enables the use of WideCharToMultiByte and MultiByteToWideChar
// If not defined, wcstombs and mbstowcs are used instead. they always use the default code page
// and don't support surrogate pairs
#define STR_USE_WIN32_CONV

// If defined, enables the use of Windows localization functions - GetNumberFormat, GetTimeFormat, etc
// If not defined, then the numbers, currency, time and date use fixed formats
#define STR_USE_WIN32_NLS

// If defined, enables support for SYSTEMTIME, FILETIME and DATE
// If not defined, only time_t is supported as a time format
#define STR_USE_WIN32_TIME

// If defined, enables the use of IsDBCSLeadByte to support DBCS code pages
// If not defined, isleadbyte is used instead
#define STR_USE_WIN32_DBCS
#else
#ifndef _LEADBYTE
#define _LEADBYTE 0x8000
#define isleadbyte(_c) (_c & _LEADBYTE)
#endif
#endif

// enables support for STL strings and streams
#define STR_USE_STL

#define nnvm_min(a, b) (((a) < (b)) ? (a) : (b))

namespace nnvm {
    class CFormatArg
    {
    public:
        CFormatArg( void ) { type=TYPE_NONE; }
        CFormatArg( char x ) { type=TYPE_INT; i=x; }
        CFormatArg( unsigned char x ) { type=TYPE_UINT; i=x; }
        CFormatArg( short x ) { type=TYPE_INT; i=x; }
        CFormatArg( unsigned short x ) { type=TYPE_UINT; i=x; }
#ifdef _WIN64
        CFormatArg( long x ) { type=TYPE_INT; i=x; }
        CFormatArg( unsigned long x ) { type=TYPE_UINT; i=x; }
        CFormatArg( int x ) { type=TYPE_INT; i=x; }
        CFormatArg( unsigned int x ) { type=TYPE_UINT; i=x; }
        CFormatArg( const void *x ) { type=TYPE_UINT64; i=(INT_PTR)x; }
#elif defined(_WIN32)
        CFormatArg( LONG_PTR x ) { type=TYPE_INT; i=x; }
        CFormatArg( ULONG_PTR x ) { type=TYPE_UINT; i=x; }
        CFormatArg( INT_PTR x ) { type=TYPE_INT; i=x; }
        CFormatArg( UINT_PTR x ) { type=TYPE_UINT; i=x; }
        CFormatArg( const void *x ) { type=TYPE_UINT; i=(INT_PTR)x; }
#else
        CFormatArg( long x ) { type=TYPE_INT; i=(int)x; }
        CFormatArg( unsigned long x ) { type=TYPE_UINT; i=(int)x; }
        CFormatArg( int x ) { type=TYPE_INT; i=x; }
        CFormatArg( unsigned int x ) { type=TYPE_UINT; i=(int)x; }
        CFormatArg( const void *x ) { type=TYPE_UINT; i=(int)(long)x; }
#endif
        CFormatArg( int64_t x ) { type=TYPE_INT64; i64=x; }
        CFormatArg( float x ) { type=TYPE_DOUBLE; d=x; }
        CFormatArg( double x ) { type=TYPE_DOUBLE; d=x; }
        CFormatArg( const char *x ) { type=TYPE_STRING; s=x?x:"(null)"; }
        CFormatArg( const wchar_t *x ) { type=TYPE_WSTRING; ws=x?x:L"(null)"; }
#ifdef STR_USE_STL
        CFormatArg( const std::string &x ) { type=TYPE_STRING; s=x.c_str(); }
	    CFormatArg( const std::wstring &x ) { type=TYPE_WSTRING; ws=x.c_str(); }
#endif
#ifdef STR_USE_WIN32_TIME
        CFormatArg( const SYSTEMTIME &x ) { type=TYPE_TIME; t=&x; }
#endif

        enum
        {
            TYPE_NONE=0,
            TYPE_INT=1,
            TYPE_UINT=2,
            TYPE_INT64=3,
            TYPE_UINT64=4,
            TYPE_DOUBLE=5,
            TYPE_STRING=6,
            TYPE_WSTRING=7,
            TYPE_TIME=8,
        };

        union
        {
#if defined(_WIN32) || defined(_WIN64)
            INT_PTR i;
#else
            int i;
#endif
            int64_t i64;
            double d;
            const char *s;
            const wchar_t *ws;
#ifdef STR_USE_WIN32_TIME
            const SYSTEMTIME *t;
#else
            time_t t;
#endif
        };
        int type;

        static CFormatArg s_Null;
    };

    const int MAX_FORMAT_ARGUMENTS=10;
#define FORMAT_STRING_ARGS_H const CFormatArg &arg1=CFormatArg::s_Null, const CFormatArg &arg2=CFormatArg::s_Null, const CFormatArg &arg3=CFormatArg::s_Null, const CFormatArg &arg4=CFormatArg::s_Null, const CFormatArg &arg5=CFormatArg::s_Null, const CFormatArg &arg6=CFormatArg::s_Null, const CFormatArg &arg7=CFormatArg::s_Null, const CFormatArg &arg8=CFormatArg::s_Null, const CFormatArg &arg9=CFormatArg::s_Null, const CFormatArg &arg10=CFormatArg::s_Null
#define FORMAT_STRING_ARGS_CPP const CFormatArg &arg1, const CFormatArg &arg2, const CFormatArg &arg3, const CFormatArg &arg4, const CFormatArg &arg5, const CFormatArg &arg6, const CFormatArg &arg7, const CFormatArg &arg8, const CFormatArg &arg9, const CFormatArg &arg10
#define FORMAT_STRING_ARGS_PASS arg1,arg2,arg3,arg4,arg5,arg6,arg7,arg8,arg9,arg10


    class UnicodeIterator : public std::iterator<std::random_access_iterator_tag, UChar32, int32_t, UChar32*, UChar32>
    {
    public:
        UnicodeIterator(icu::UnicodeString str) : s(str), pos(0) {}
        UnicodeIterator(const UnicodeIterator& rawIterator) = default;
        ~UnicodeIterator() {}
        
        UnicodeIterator& operator=(const UnicodeIterator& rawIterator) = default;
        UnicodeIterator& operator=(icu::UnicodeString str);
        
        operator bool()const;
        
        bool operator==(const UnicodeIterator& rawIterator)const;
        bool operator!=(const UnicodeIterator& rawIterator)const;
        
        UnicodeIterator& operator+=(const int32_t& movement);
        UnicodeIterator& operator-=(const int32_t& movement);
        UnicodeIterator& operator++();
        UnicodeIterator& operator--();
        const UnicodeIterator operator++(int32_t);
        const UnicodeIterator operator--(int32_t);
        UnicodeIterator operator+(const int32_t& movement);
        UnicodeIterator operator-(const int32_t& movement);
        int32_t operator-(const UnicodeIterator& rawIterator);
        
        UChar32 operator*();
        
    protected:
        icu::UnicodeString s;
        int64_t pos;
        friend class StringUtils;
    };

    class StringUtils
    {
    public:
        static std::vector<icu::UnicodeString>& split(std::vector<icu::UnicodeString>& result, const icu::UnicodeString& s, const icu::UnicodeString& pattern);
        static std::vector<icu::UnicodeString>& split(std::vector<icu::UnicodeString>& result, const icu::UnicodeString& s, const char* pattern);
        static std::vector<icu::UnicodeString>& split(std::vector<icu::UnicodeString>& result, const icu::UnicodeString& s, const wchar_t* pattern);
        static int8_t matches(const icu::UnicodeString& s, const icu::UnicodeString& pattern);
        static icu::UnicodeString joinArray(const std::vector<icu::UnicodeString>& v, const char* delim);
        static icu::UnicodeString joinArray(const std::vector<icu::UnicodeString>& v, const wchar_t* delim);
        static icu::UnicodeString joinArray(const std::vector<icu::UnicodeString>& v, const icu::UnicodeString& delim);
        static std::string spf(const std::string fmt, FORMAT_STRING_ARGS_H);
        static std::wstring spf(const std::wstring fmt, FORMAT_STRING_ARGS_H);
        static std::u32string spf(const std::u32string fmt, FORMAT_STRING_ARGS_H);
        static icu::UnicodeString spf(const icu::UnicodeString fmt, FORMAT_STRING_ARGS_H);
        static UnicodeIterator beginIterator(icu::UnicodeString s);
        static UnicodeIterator endIterator(icu::UnicodeString s);
        static std::string timeStr(int64_t nanoseconds);
    };
}
