/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#pragma once

#include "predef.h"

namespace nnvm {
    NNVM_EXPORTED class ParsedNeuron;
}

#include <memory>
#include <vector>
#include <map>
#include <inttypes.h>
#include <unicode/unistr.h>
#include <Disposable.hpp>
#include <LanguageModel.hpp>
#include <ByteCodeBlock.hpp>
#include <MachineSetup.hpp>
#include <LoaderModel.hpp>
#include <LibraryLoader.hpp>

namespace nnvm {
    enum class ParsedNeuronArgumentType
    {
        FAIL, // incorrect state
        REQUIRED_SINGLE, // only single literals allowed
        SINGLE, // parse tokens into tree and simplify them into single literals plus generate subdefinitions
        FLAT_CONDITIONAL, // parse tokens into tree and simplify down two additions and one conditional plus generate subdefinitions
        FLAT, // parse tokens into tree and simplify down two additions plus generate subdefinitions
        TREE, // parse tokens into tree
        KEY, // a single literal with colon separated parsed tokens into tree and simplified into single literals plus generated subdefinitions
        RAW, // keep as is
    };

    class ParsedNeuron : public Disposable
    {
    public:
        ParsedNeuron(std::shared_ptr<MachineSetup> setup, std::shared_ptr<LibraryLoader> loader);
        // Type of argument preprocessing.
        virtual ParsedNeuronArgumentType argumentType(int64_t argumentIndex, int64_t argumentTotal);
        // Custom compilation
        virtual std::vector<std::shared_ptr<ByteCodeBlock> > compile(icu::UnicodeString name, LanguageCall& call, CompileContext& cctx, std::shared_ptr<Context> ctx) = 0;
        // Custom block merging
        //virtual std::shared_ptr<ByteCodeBlock> mergeBlock(std::shared_ptr<ByteCodeBlock> block) = 0;
    protected:
        virtual std::vector<std::shared_ptr<ByteCodeBlockItem> > compileExpression(Operand& operand, icu::UnicodeString name, icu::UnicodeString linkType, std::shared_ptr<ByteCodeBlock> current, std::vector<std::shared_ptr<ByteCodeBlock> >& rv, std::shared_ptr<Context> ctx);
        std::shared_ptr<MachineSetup> setup;
        std::shared_ptr<LibraryLoader> loader;
    };
}
