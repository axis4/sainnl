/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#pragma once

#include "predef.h"

namespace nnvm {
    NNVM_EXPORTED class WritableNeuron;
}

#include <ostream>
#include <memory>
#include <inttypes.h>
#include <unicode/unistr.h>
#include <Disposable.hpp>
#include <ByteCodeBlock.hpp>
#include <ByteCodeWriterSetup.hpp>
#include <MachineSetup.hpp>
#include <LibraryLoader.hpp>

namespace nnvm {
    class WritableNeuron : public Disposable
    {
    public:
        WritableNeuron(std::shared_ptr<MachineSetup> setup, std::shared_ptr<LibraryLoader> loader);

        // Serialization of a node into a SAINNL line
        virtual void writeSourceBlock(std::shared_ptr<ByteCodeBlock> code, std::shared_ptr<std::ostream> stream) = 0;
        
        int8_t dumpItems(icu::UnicodeString itemType, std::shared_ptr<ByteCodeBlock> code, std::shared_ptr<std::ostream> stream);
        std::vector<std::function<void(std::shared_ptr<ByteCodeBlock>, std::shared_ptr<std::ostream>)>> argumentWriters;

    protected:
        std::shared_ptr<MachineSetup> setup;
        std::shared_ptr<LibraryLoader> loader;
    };
}
