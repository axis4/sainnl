/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#pragma once

#ifdef _WIN32
#  define NNVM_EXPORTED __declspec(dllimport)
#else
#  define NNVM_EXPORTED
#endif

#ifdef _WIN32
#  define EXPORTED __declspec(dllexport)
#else
#  define EXPORTED
#endif

#include "predef.h"
#include <memory>
#include <unicode/unistr.h>
#include <MachineSetup.hpp>
#include <ExecutableNeuron.hpp>
#include <WritableNeuron.hpp>
#include <ParsedNeuron.hpp>

extern "C" EXPORTED int isMachineVersionCompatible(const MachineVersion& version);
/* {
    return MACHINE_MAJOR_VERSION == version.major;
 } */

extern "C" EXPORTED std::shared_ptr<nnvm::ExecutableNeuron> getExecutable(icu::UnicodeString name, std::shared_ptr<nnvm::MachineSetup> setup, std::shared_ptr<nnvm::LibraryLoader> loader);
extern "C" EXPORTED std::shared_ptr<nnvm::WritableNeuron> getWriteable(icu::UnicodeString name, std::shared_ptr<nnvm::MachineSetup> setup, std::shared_ptr<nnvm::LibraryLoader> loader);
extern "C" EXPORTED std::shared_ptr<nnvm::ParsedNeuron> getParsed(icu::UnicodeString name, std::shared_ptr<nnvm::MachineSetup> setup, std::shared_ptr<nnvm::LibraryLoader> loader);
