/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#pragma once

#include "predef.h"

namespace nnvm {
    NNVM_EXPORTED class ByteCodeBlock;
}

#include <unicode/unistr.h>
#include <inttypes.h>
#include <tuple>
#include <deque>
#include <map>
#include <vector>
#include <memory>
#include <Disposable.hpp>
#include <ByteCodeParameter.hpp>
#include <ByteCodeBlockItem.hpp>

namespace nnvm {
    class ByteCodeBlock : public Disposable
    {
    public:
        static std::vector<std::shared_ptr<ByteCodeBlock> > fullCopy(const std::vector<std::shared_ptr<ByteCodeBlock> >& src);
        static std::deque<std::shared_ptr<ByteCodeBlockItem> > fullCopy(const std::deque<std::shared_ptr<ByteCodeBlockItem> >& src);
        static std::deque<std::shared_ptr<ByteCodeBlockItem> > fullCopy(const std::deque<std::shared_ptr<ByteCodeBlockItem> >::const_iterator start, const std::deque<std::shared_ptr<ByteCodeBlockItem> >::const_iterator end);
        static std::deque<std::shared_ptr<ByteCodeParameter> > fullCopy(const std::deque<std::shared_ptr<ByteCodeParameter> >& src);
        static std::deque<std::shared_ptr<ByteCodeParameter> > fullCopy(const std::deque<std::shared_ptr<ByteCodeParameter> >::const_iterator start, const std::deque<std::shared_ptr<ByteCodeParameter> >::const_iterator end);
        
        ByteCodeBlock(const icu::UnicodeString& blockName, const icu::UnicodeString& type);
        ByteCodeBlock(ByteCodeBlock& obj);
        virtual ~ByteCodeBlock();
        std::shared_ptr<ByteCodeBlock> isolatedCopy();
        std::shared_ptr<ByteCodeBlock> isolatedCopy(const icu::UnicodeString& rename);
        void dispose();
        void addItem(std::shared_ptr<ByteCodeBlockItem> item);
        void eraseItem(std::deque<std::shared_ptr<ByteCodeBlockItem> >::iterator item);
        std::tuple<std::deque<std::shared_ptr<ByteCodeBlockItem> >::iterator, std::deque<std::shared_ptr<ByteCodeBlockItem> >::iterator> getItems();
        int64_t getItemsCount();
        std::deque<std::shared_ptr<ByteCodeBlockItem>>::iterator getItem(int64_t index);
        std::shared_ptr<ByteCodeBlockItem> getLastItem();
        void addIncome(std::shared_ptr<ByteCodeBlockItem> item);
        void eraseIncome(std::deque<std::shared_ptr<ByteCodeBlockItem> >::iterator item);
        std::tuple<std::deque<std::shared_ptr<ByteCodeBlockItem> >::iterator, std::deque<std::shared_ptr<ByteCodeBlockItem> >::iterator> getIncomes();
        int64_t getIncomesCount();
        std::deque<std::shared_ptr<ByteCodeBlockItem>>::iterator getIncome(int64_t index);
        std::shared_ptr<ByteCodeBlockItem> getLastIncome();
        std::tuple<std::deque<std::shared_ptr<ByteCodeParameter> >::iterator, std::deque<std::shared_ptr<ByteCodeParameter> >::iterator> getParameters();
        std::shared_ptr<ByteCodeParameter> getParameter(const icu::UnicodeString& name);
        int8_t tryGetParameter(const icu::UnicodeString& name, std::shared_ptr<ByteCodeParameter>& result);
        void setParameter(std::shared_ptr<ByteCodeParameter> parameter);

        std::shared_ptr<ByteCodeParameter> getFastThreshold();
        std::shared_ptr<ByteCodeParameter> getFastBias();
        std::shared_ptr<ByteCodeParameter> getFastInitial();
        std::shared_ptr<ByteCodeParameter> getFastInput();
        std::shared_ptr<ByteCodeParameter> getFastOutput();
        std::shared_ptr<ByteCodeParameter> getFastTemplate();
        std::shared_ptr<ByteCodeParameter> getFastExpected();
        std::shared_ptr<ByteCodeParameter> getFastSum();
        std::shared_ptr<ByteCodeParameter> getFastIndex();

        const icu::UnicodeString& getName();
        const icu::UnicodeString& getType();
        void changeType(icu::UnicodeString type);
        void getParams(const std::vector<icu::UnicodeString>& names, std::map<icu::UnicodeString, std::shared_ptr<ByteCodeParameter> >& result);
        void compact();

    private:
        icu::UnicodeString name;
        icu::UnicodeString type;

        std::shared_ptr<ByteCodeParameter> fastThreshold;
        std::shared_ptr<ByteCodeParameter> fastBias;
        std::shared_ptr<ByteCodeParameter> fastInitial;
        std::shared_ptr<ByteCodeParameter> fastInput;
        std::shared_ptr<ByteCodeParameter> fastOutput;
        std::shared_ptr<ByteCodeParameter> fastTemplate;
        std::shared_ptr<ByteCodeParameter> fastExpected;
        std::shared_ptr<ByteCodeParameter> fastSum;
        std::shared_ptr<ByteCodeParameter> fastIndex;

        std::deque<std::shared_ptr<ByteCodeParameter> > parameters;
        std::map<icu::UnicodeString, int64_t> parametersIndex;
        std::deque<std::shared_ptr<ByteCodeBlockItem> > items;
        std::deque<std::shared_ptr<ByteCodeBlockItem> > incomes;
    };
}