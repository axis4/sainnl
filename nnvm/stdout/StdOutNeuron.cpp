/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#include <LibraryDescriptor.hpp>
#include "StdOutNeuron.hpp"
#include <DefaultNeurons.hpp>
#include <StringConv.hpp>
#include <BinaryUtils.hpp>
#include <ByteCodeUtils.hpp>

namespace nnvm {
    namespace system {
        StdOutNeuron::StdOutNeuron(std::shared_ptr<MachineSetup> setup, std::shared_ptr<LibraryLoader> loader)
                : QueuedIONeuron<UChar32>(setup, loader), ParsedNeuron(setup, loader), WritableNeuron(setup, loader) { }
        
        void StdOutNeuron::writeSourceBlock(std::shared_ptr<ByteCodeBlock> code, std::shared_ptr<std::ostream> stream)
        {
            *stream << StringConvUtils::utou8(code->getName()) << " is " << StringConvUtils::utou8(getEligibleType());
            auto r = code->getIncomes();
            if (std::get<0>(r) != std::get<1>(r)) {
                *stream << " ";
                dumpItems(LinkParams::STREAM, code, stream);
            }
        }
        int8_t StdOutNeuron::readData(UChar32& data)
        {
            return false;
        }
        int8_t StdOutNeuron::writeData(UChar32& data)
        {
            if (data) {
                std::cout << "Outdata " << data << std::endl;
                auto tmp = StringConvUtils::utou8(StringConvUtils::asUnicodeString(&data, 1));
                auto d = std::vector<uint8_t>(tmp.begin(), tmp.end());
                BinaryUtils::writeBinary(d, this->QueuedIONeuron<UChar32>::setup->getStdOut());
            }
            return true;
        }
        icu::UnicodeString StdOutNeuron::getEligibleType()
        {
            return NeuronNames::STDOUT;
        }
        ParsedNeuronArgumentType StdOutNeuron::argumentType(int64_t argumentIndex, int64_t argumentTotal)
        {
            if (argumentIndex > 0) {
                return ParsedNeuronArgumentType::FAIL;
            }
            return ParsedNeuronArgumentType::SINGLE;
        }
        
        std::vector<std::shared_ptr<ByteCodeBlock>> StdOutNeuron::compile(icu::UnicodeString name, LanguageCall& call, CompileContext& cctx, std::shared_ptr<Context> ctx)
        {
            std::vector<std::shared_ptr<ByteCodeBlock>> rv;
            auto res = std::make_shared<ByteCodeBlock>(name, this->getEligibleType());
            rv.push_back(res);
            auto b = 0.0;
            for (auto & argument : call.arguments) {
                if (!argument.operand.literal->isOf(TokenKind::NUMBER)) {
                    auto e = std::make_shared<ByteCodeBlock>(argument.operand.literal->value, USS(""));
                    e->setParameter(std::make_shared<ByteCodeParameter>(ElementParams::EXPECTED, (int8_t)1));
                    rv.push_back(e);
                    auto ep = std::make_shared<ByteCodeBlockItem>(name, e);
                    ep->setParameter(std::make_shared<ByteCodeParameter>(LinkParams::STREAM, 1.0));
                    res->addIncome(ep);
                }
                else {
                    b += ByteCodeUtils::asNumber(argument.operand.literal->value);
                }
            }
            res->setParameter(std::make_shared<ByteCodeParameter>(ElementParams::BIAS, b));
            return rv;
        }
    }
}

extern "C" EXPORTED int isMachineVersionCompatible(const MachineVersion& version)
{
    return MACHINE_MAJOR_VERSION == version.major;
}

extern "C" EXPORTED std::shared_ptr<nnvm::ExecutableNeuron> getExecutable(icu::UnicodeString name, std::shared_ptr<nnvm::MachineSetup> setup, std::shared_ptr<nnvm::LibraryLoader> loader)
{
    return std::make_shared<nnvm::system::StdOutNeuron>(setup, loader);
}

extern "C" EXPORTED std::shared_ptr<nnvm::WritableNeuron> getWriteable(icu::UnicodeString name, std::shared_ptr<nnvm::MachineSetup> setup, std::shared_ptr<nnvm::LibraryLoader> loader)
{
    return std::make_shared<nnvm::system::StdOutNeuron>(setup, loader);
}

extern "C" EXPORTED std::shared_ptr<nnvm::ParsedNeuron> getParsed(icu::UnicodeString name, std::shared_ptr<nnvm::MachineSetup> setup, std::shared_ptr<nnvm::LibraryLoader> loader)
{
    return std::make_shared<nnvm::system::StdOutNeuron>(setup, loader);
}
