/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#include <LibraryDescriptor.hpp>
#include "InPosNeuron.hpp"
#include <DefaultNeurons.hpp>

namespace nnvm {
    namespace system {
        InPosNeuron::InPosNeuron(std::shared_ptr<MachineSetup> setup, std::shared_ptr<LibraryLoader> loader)
                : ParsedNeuron(setup, loader) { }

        ParsedNeuronArgumentType InPosNeuron::argumentType(int64_t argumentIndex, int64_t argumentTotal)
        {
            if (argumentIndex >= argumentTotal) {
                return ParsedNeuronArgumentType::FAIL;
            }
            return ParsedNeuronArgumentType::REQUIRED_SINGLE;
        }
        std::vector<std::shared_ptr<ByteCodeBlock>> InPosNeuron::compile(icu::UnicodeString name, LanguageCall& call, CompileContext& cctx, std::shared_ptr<Context> ctx)
        {
            std::vector<std::shared_ptr<ByteCodeBlock>> rv;
            if (call.arguments.empty()) {
                return rv;
            }
            int64_t id = 1;
            for (auto & argument : call.arguments) {
                id++;
                auto v = argument.operand.literal->value;
                auto res = std::make_shared<ByteCodeBlock>(v, NeuronNames::ADDITIVE);
                res->setParameter(std::make_shared<ByteCodeParameter>(ElementParams::INPUT, (int8_t)1));
                res->setParameter(std::make_shared<ByteCodeParameter>(ElementParams::INDEX, id));
                rv.push_back(res);
            }
            return rv;
        }
    }
}

extern "C" EXPORTED int isMachineVersionCompatible(const MachineVersion& version)
{
    return MACHINE_MAJOR_VERSION == version.major;
}

extern "C" EXPORTED std::shared_ptr<nnvm::ExecutableNeuron> getExecutable(icu::UnicodeString name, std::shared_ptr<nnvm::MachineSetup> setup, std::shared_ptr<nnvm::LibraryLoader> loader)
{
    return nullptr;
}

extern "C" EXPORTED std::shared_ptr<nnvm::WritableNeuron> getWriteable(icu::UnicodeString name, std::shared_ptr<nnvm::MachineSetup> setup, std::shared_ptr<nnvm::LibraryLoader> loader)
{
    return nullptr;
}

extern "C" EXPORTED std::shared_ptr<nnvm::ParsedNeuron> getParsed(icu::UnicodeString name, std::shared_ptr<nnvm::MachineSetup> setup, std::shared_ptr<nnvm::LibraryLoader> loader)
{
    return std::make_shared<nnvm::system::InPosNeuron>(setup, loader);
}
