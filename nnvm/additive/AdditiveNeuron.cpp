/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#include <LibraryDescriptor.hpp>
#include "AdditiveNeuron.hpp"
#include <DefaultNeurons.hpp>
#include <StringConv.hpp>
#include <ByteCodeUtils.hpp>

namespace nnvm {
    namespace sainn {
        AdditiveNeuron::AdditiveNeuron(std::shared_ptr<MachineSetup> setup, std::shared_ptr<LibraryLoader> loader)
                : SelfCheckedNeuron(setup, loader), ParsedNeuron(setup, loader), WritableNeuron(setup, loader) { }
        
        icu::UnicodeString AdditiveNeuron::getEligibleType()
        {
            return NeuronNames::ADDITIVE;
        }
        
        void AdditiveNeuron::writeSourceBlock(std::shared_ptr<ByteCodeBlock> code, std::shared_ptr<std::ostream> stream)
        {
            *stream << StringConvUtils::utou8(code->getName()) << " is " << StringConvUtils::utou8(getEligibleType()) << " ";
            argumentWriters[0](code, stream);
            *stream << " >= ";
            std::shared_ptr<ByteCodeParameter> source;
            auto paramName = StringConvUtils::wtou(StringConvUtils::utow(ElementParams::THRESHOLD));
            if (!code->tryGetParameter(paramName, source)) {
                *stream << "0";
            }
            else {
                *stream << StringConvUtils::utou8(ByteCodeUtils::asString(*source));
            }
            *stream << ", ";
            
            argumentWriters[1](code, stream);
        }
        
        void AdditiveNeuron::safeCall(std::shared_ptr<ByteCodeBlock> block,
                                      std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock>>> blocks)
        {
            std::map<icu::UnicodeString, std::shared_ptr<ByteCodeParameter>> params;
            block->getParams(std::vector<icu::UnicodeString>{
                LinkParams::ACTIVATION + ElementParams::SUM_SUFFIX,
                LinkParams::STREAM + ElementParams::SUM_SUFFIX,
                ElementParams::THRESHOLD,
                ElementParams::BIAS }, params);
            
            auto actSum = params[LinkParams::ACTIVATION + ElementParams::SUM_SUFFIX];
            auto strSum = params[LinkParams::STREAM + ElementParams::SUM_SUFFIX];
            auto thres = params[ElementParams::THRESHOLD];
            auto bias = params[ElementParams::BIAS];
            
            auto tmpSum = std::make_shared<ByteCodeParameter>(ElementParams::SUM, (double)0);
            auto sum = tmpSum;
            if (block->tryGetParameter(ElementParams::SUM, sum)) {
                if (sum->getType() != ByteCodeParameterTypes::DOUBLE) {
                    sum = tmpSum;
                    block->setParameter(tmpSum);
                }
            }
            else {
                block->setParameter(tmpSum);
            }

            sum->value.doubleValue += ByteCodeUtils::asDouble(*actSum) >= ByteCodeUtils::asDouble(*thres)
                ? ByteCodeUtils::asDouble(*strSum) + ByteCodeUtils::asDouble(*bias)
                : 0;
        }

        ParsedNeuronArgumentType AdditiveNeuron::argumentType(int64_t argumentIndex, int64_t argumentTotal)
        {
            if (argumentIndex > 1) {
                return ParsedNeuronArgumentType::FAIL;
            }
            if (argumentIndex > 0 || argumentTotal == 1) {
                return ParsedNeuronArgumentType::FLAT;
            }
            return ParsedNeuronArgumentType::FLAT_CONDITIONAL;
        }
        
        std::vector<std::shared_ptr<ByteCodeBlock>> AdditiveNeuron::compile(icu::UnicodeString name, LanguageCall& call, CompileContext& cctx, std::shared_ptr<Context> ctx)
        {
            std::vector<std::shared_ptr<ByteCodeBlock>> rv;
            if (call.arguments.size() < 1) {
                return rv;
            }
            std::shared_ptr<ByteCodeBlock> res(new ByteCodeBlock(name, this->getEligibleType()));
            rv.push_back(res);
            if (call.arguments.size() == 1) {
                // add always true conditional expression
                LanguageArgument a(ctx->byteLoader);
                a.operand.expression = std::make_shared<ExpressionTree>(ctx->byteLoader);
                
                a.operand.expression->left = std::make_shared<Operand>(ctx->byteLoader);
                auto t = std::make_shared<Token<TokenKind>>(ctx->byteLoader);
                t->value = USS("0");
                t->type.push_back(TokenKind::LITERAL);
                t->type.push_back(TokenKind::NUMBER);
                a.operand.expression->left->literal = t;
                
                a.operand.expression->middle = USS(">=");
                
                a.operand.expression->right = std::make_shared<Operand>(ctx->byteLoader);
                t = std::make_shared<Token<TokenKind>>(ctx->byteLoader);
                t->value = USS("0");
                t->type.push_back(TokenKind::LITERAL);
                t->type.push_back(TokenKind::NUMBER);
                a.operand.expression->right->literal = t;
                
                call.arguments.insert(call.arguments.begin(), a);
            }
            
            auto e = call.arguments[0].operand.expression;

            auto act = compileExpression(*e->left, name, LinkParams::ACTIVATION, res, rv, ctx);
            for (auto i = act.begin(); i != act.end(); ++i) {
                std::shared_ptr<ByteCodeBlockItem> ep;
                // find already defined link like current stream
                auto r = res->getIncomes();
                for (auto l = std::get<0>(r); l != std::get<1>(r); ++l) {
                    if ((*l)->getOwner().lock()->getName() == (*i)->getOwner().lock()->getName()) {
                        ep = *l;
                    }
                }
                if (!ep) {
                    res->addIncome(*i);
                }
                else {
                    std::shared_ptr<ByteCodeParameter> p;
                    if ((*i)->tryGetParameter(LinkParams::ACTIVATION, p)) {
                        auto np = std::make_shared<ByteCodeParameter>(LinkParams::ACTIVATION, 0.0);
                        std::shared_ptr<ByteCodeParameter> pp;
                        if (!ep->tryGetParameter(LinkParams::ACTIVATION, pp)) {
                            pp = std::make_shared<ByteCodeParameter>(LinkParams::ACTIVATION, 0.0);
                        }
                        np->value = ByteCodeParameterValue(ByteCodeUtils::asDouble(*pp) + ByteCodeUtils::asDouble(*p));
                        ep->setParameter(np);
                    }
                }
            }
            if (e->right->literal->isOf(TokenKind::NUMBER)) {
                res->setParameter(std::make_shared<ByteCodeParameter>(ElementParams::THRESHOLD, ByteCodeUtils::asNumber(e->right->literal->value)));
            }
            else {
                res->setParameter(std::make_shared<ByteCodeParameter>(ElementParams::THRESHOLD, e->right->literal->value));
            }
            
            if (call.arguments.size() < 2) {
                return rv;
            }
            
            auto str = compileExpression(call.arguments[1].operand, name, LinkParams::STREAM, res, rv, ctx);
            for (auto i = str.begin(); i != str.end(); ++i) {
                std::shared_ptr<ByteCodeBlockItem> ep;
                // find already defined link like current stream
                auto r = res->getIncomes();
                for (auto l = std::get<0>(r); l != std::get<1>(r); ++l) {
                    if ((*l)->getOwner().lock()->getName() == (*i)->getOwner().lock()->getName()) {
                        ep = *l;
                    }
                }
                if (!ep) {
                    ep = std::make_shared<ByteCodeBlockItem>(name, (*i)->getOwner());
                    res->addIncome(ep);
                }
                std::shared_ptr<ByteCodeParameter> p;
                if ((*i)->tryGetParameter(LinkParams::STREAM, p)) {
                    auto np = std::make_shared<ByteCodeParameter>(LinkParams::STREAM, 0.0);
                    std::shared_ptr<ByteCodeParameter> pp;
                    if (!ep->tryGetParameter(LinkParams::STREAM, pp)) {
                        pp = std::make_shared<ByteCodeParameter>(LinkParams::STREAM, 0.0);
                    }
                    np->value = ByteCodeParameterValue(ByteCodeUtils::asDouble(*pp) + ByteCodeUtils::asDouble(*p));
                    
                    ep->setParameter(np);
                }
            }
            
            //    res->setParameter(std::make_shared<ByteCodeParameter>(
            //        ElementParams::BIAS, ByteCodeUtils::asNumber(call.arguments[2].literal->value)));
            return rv;
        }
    }
}

extern "C" EXPORTED int isMachineVersionCompatible(const MachineVersion& version)
{
    return MACHINE_MAJOR_VERSION == version.major;
}

extern "C" EXPORTED std::shared_ptr<nnvm::ExecutableNeuron> getExecutable(icu::UnicodeString name, std::shared_ptr<nnvm::MachineSetup> setup, std::shared_ptr<nnvm::LibraryLoader> loader)
{
    return std::make_shared<nnvm::sainn::AdditiveNeuron>(setup, loader);
}

extern "C" EXPORTED std::shared_ptr<nnvm::WritableNeuron> getWriteable(icu::UnicodeString name, std::shared_ptr<nnvm::MachineSetup> setup, std::shared_ptr<nnvm::LibraryLoader> loader)
{
    return std::make_shared<nnvm::sainn::AdditiveNeuron>(setup, loader);
}

extern "C" EXPORTED std::shared_ptr<nnvm::ParsedNeuron> getParsed(icu::UnicodeString name, std::shared_ptr<nnvm::MachineSetup> setup, std::shared_ptr<nnvm::LibraryLoader> loader)
{
    return std::make_shared<nnvm::sainn::AdditiveNeuron>(setup, loader);
}
