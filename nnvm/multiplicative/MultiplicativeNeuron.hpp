/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#pragma once

#include <predef.h>

namespace nnvm {
    namespace sainn {
        NNVM_EXPORTED class MultiplicativeNeuron;
    }
}

#include <unicode/unistr.h>
#include <inttypes.h>
#include <vector>
#include <memory>
#include <MachineSetup.hpp>
#include <ByteCodeBlock.hpp>
#include <LanguageModel.hpp>
#include <LoaderModel.hpp>
#include <ParsedNeuron.hpp>
#include <WritableNeuron.hpp>
#include <BaseNeurons.hpp>

namespace nnvm {
    namespace sainn {
        class MultiplicativeNeuron : public FunctionNeuron
        {
        public:
            MultiplicativeNeuron(std::shared_ptr<MachineSetup> setup, std::shared_ptr<LibraryLoader> loader);
            ParsedNeuronArgumentType argumentType(int64_t argumentIndex, int64_t argumentTotal);
            std::vector<std::shared_ptr<ByteCodeBlock>> compile(icu::UnicodeString name, LanguageCall& call, CompileContext& cctx, std::shared_ptr<Context> ctx);
        protected:
            icu::UnicodeString getEligibleType();
            int64_t getArgumentCount();
            void safeCall(std::shared_ptr<ByteCodeBlock> block, std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock>>> blocks);
        };
    }
}
