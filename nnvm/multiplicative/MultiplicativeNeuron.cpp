/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#include <LibraryDescriptor.hpp>
#include "MultiplicativeNeuron.hpp"
#include <DefaultNeurons.hpp>
#include <ByteCodeUtils.hpp>

namespace nnvm {
    namespace sainn {
        MultiplicativeNeuron::MultiplicativeNeuron(std::shared_ptr<MachineSetup> setup, std::shared_ptr<LibraryLoader> loader)
                : FunctionNeuron(setup, loader) { }
        
        icu::UnicodeString MultiplicativeNeuron::getEligibleType()
        {
            return NeuronNames::MULTIPLICATIVE;
        }
        
        int64_t MultiplicativeNeuron::getArgumentCount()
        {
            return 3;
        }
        
        void MultiplicativeNeuron::safeCall(std::shared_ptr<ByteCodeBlock> block,
                                            std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock>>> blocks)
        {
            std::map<icu::UnicodeString, std::shared_ptr<ByteCodeParameter>> params;
            block->getParams(std::vector<icu::UnicodeString>{
                LinkParams::ACTIVATION + ElementParams::SUM_SUFFIX,
                LinkParams::STREAM + ElementParams::SUM_SUFFIX,
                ElementParams::INITIAL_STATE,
                ElementParams::BIAS }, params);
            
            auto actSum = params[LinkParams::ACTIVATION + ElementParams::SUM_SUFFIX];
            auto strSum = params[LinkParams::STREAM + ElementParams::SUM_SUFFIX];
            auto initState = params[ElementParams::INITIAL_STATE];
            auto bias = params[ElementParams::BIAS];
            
            auto tmpIntState = std::make_shared<ByteCodeParameter>(ElementParams::INTERNAL_STATE, initState->value.doubleValue);
            auto intState = tmpIntState;
            if (block->tryGetParameter(ElementParams::INTERNAL_STATE, intState)) {
                if (intState->getType() != ByteCodeParameterTypes::DOUBLE) {
                    intState = tmpIntState;
                    block->setParameter(tmpIntState);
                }
            }
            else {
                block->setParameter(tmpIntState);
            }
            
            auto tmpSum = std::make_shared<ByteCodeParameter>(ElementParams::SUM, (double)0);
            auto sum = tmpSum;
            if (block->tryGetParameter(ElementParams::SUM, sum)) {
                if (sum->getType() != ByteCodeParameterTypes::DOUBLE) {
                    sum = tmpSum;
                    block->setParameter(tmpSum);
                }
            }
            else {
                block->setParameter(tmpSum);
            }
            
            intState->value.doubleValue += ByteCodeUtils::asDouble(*actSum);
            sum->value.doubleValue = intState->value.doubleValue * (ByteCodeUtils::asDouble(*strSum) + ByteCodeUtils::asDouble(*bias));
        }
        
        ParsedNeuronArgumentType MultiplicativeNeuron::argumentType(int64_t argumentIndex, int64_t argumentTotal)
        {
            if (argumentIndex > 2) {
                return ParsedNeuronArgumentType::FAIL;
            }
            if (argumentIndex > 1) {
                return ParsedNeuronArgumentType::REQUIRED_SINGLE;
            }
            if (argumentIndex > 0) {
                return ParsedNeuronArgumentType::FLAT;
            }
            return ParsedNeuronArgumentType::FLAT;
        }
        
        std::vector<std::shared_ptr<ByteCodeBlock>> MultiplicativeNeuron::compile(icu::UnicodeString name, LanguageCall& call, CompileContext& cctx, std::shared_ptr<Context> ctx)
        {
            auto rv = this->FunctionNeuron::compile(name, call, cctx, ctx);
            auto res = rv[0];
            
            // if A = & B, C
            // means A = B * C
            if (call.arguments.size() < 3) {
                return rv;
            }
            
            // if A = & B, C, N
            res->setParameter(std::make_shared<ByteCodeParameter>(ElementParams::INITIAL_STATE, ByteCodeUtils::asNumber(call.arguments[2].operand.literal->value)));
            call.arguments.pop_back();
            return rv;
        }
    }
}

extern "C" EXPORTED int isMachineVersionCompatible(const MachineVersion& version)
{
    return MACHINE_MAJOR_VERSION == version.major;
}

extern "C" EXPORTED std::shared_ptr<nnvm::ExecutableNeuron> getExecutable(icu::UnicodeString name, std::shared_ptr<nnvm::MachineSetup> setup, std::shared_ptr<nnvm::LibraryLoader> loader)
{
    return std::make_shared<nnvm::sainn::MultiplicativeNeuron>(setup, loader);
}

extern "C" EXPORTED std::shared_ptr<nnvm::WritableNeuron> getWriteable(icu::UnicodeString name, std::shared_ptr<nnvm::MachineSetup> setup, std::shared_ptr<nnvm::LibraryLoader> loader)
{
    return std::make_shared<nnvm::sainn::MultiplicativeNeuron>(setup, loader);
}

extern "C" EXPORTED std::shared_ptr<nnvm::ParsedNeuron> getParsed(icu::UnicodeString name, std::shared_ptr<nnvm::MachineSetup> setup, std::shared_ptr<nnvm::LibraryLoader> loader)
{
    return std::make_shared<nnvm::sainn::MultiplicativeNeuron>(setup, loader);
}
