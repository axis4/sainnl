/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#include <LibraryDescriptor.hpp>
#include "ExitNeuron.hpp"
#include <DefaultNeurons.hpp>
#include <ByteCodeUtils.hpp>
#include <utility>

namespace nnvm {
    namespace system {
        ExitNeuron::ExitNeuron(std::shared_ptr<MachineSetup> setup, std::shared_ptr<LibraryLoader> loader)
                : FunctionNeuron(std::move(setup), std::move(loader)) { }
        
        icu::UnicodeString ExitNeuron::getEligibleType()
        {
            return NeuronNames::EXIT;
        }
        
        int64_t ExitNeuron::getArgumentCount()
        {
            return 2;
        }
        
        void ExitNeuron::safeCall(std::shared_ptr<ByteCodeBlock> block,
                                  std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock>>> blocks)
        {
            std::map<icu::UnicodeString, std::shared_ptr<ByteCodeParameter>> params;
            block->getParams(std::vector<icu::UnicodeString>{
                LinkParams::ACTIVATION + ElementParams::SUM_SUFFIX,
                LinkParams::STREAM + ElementParams::SUM_SUFFIX,
                ElementParams::BIAS }, params);
            
            auto actSum = params[LinkParams::ACTIVATION + ElementParams::SUM_SUFFIX];
            auto strSum = params[LinkParams::STREAM + ElementParams::SUM_SUFFIX];
            auto bias = params[ElementParams::BIAS];
            
            if (ByteCodeUtils::asDouble(*actSum) > 0) {
                ExecutableNeuron::setup->getProcessorSetup()->exitCode = (int8_t)(ByteCodeUtils::asDouble(*strSum) + ByteCodeUtils::asDouble(*bias));
                ExecutableNeuron::setup->getProcessorSetup()->running = 0;
            }
        }
    }
}

extern "C" EXPORTED int isMachineVersionCompatible(const MachineVersion& version)
{
    return MACHINE_MAJOR_VERSION == version.major;
}

extern "C" EXPORTED std::shared_ptr<nnvm::ExecutableNeuron> getExecutable(icu::UnicodeString name, std::shared_ptr<nnvm::MachineSetup> setup, std::shared_ptr<nnvm::LibraryLoader> loader)
{
    return std::make_shared<nnvm::system::ExitNeuron>(setup, loader);
}

extern "C" EXPORTED std::shared_ptr<nnvm::WritableNeuron> getWriteable(icu::UnicodeString name, std::shared_ptr<nnvm::MachineSetup> setup, std::shared_ptr<nnvm::LibraryLoader> loader)
{
    return std::make_shared<nnvm::system::ExitNeuron>(setup, loader);
}

extern "C" EXPORTED std::shared_ptr<nnvm::ParsedNeuron> getParsed(icu::UnicodeString name, std::shared_ptr<nnvm::MachineSetup> setup, std::shared_ptr<nnvm::LibraryLoader> loader)
{
    return std::make_shared<nnvm::system::ExitNeuron>(setup, loader);
}
