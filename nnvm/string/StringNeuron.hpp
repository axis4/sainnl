/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#pragma once

#include <predef.h>

namespace nnvm {
    namespace system {
        NNVM_EXPORTED class StringNeuron;
    }
}

#include <unicode/unistr.h>
#include <inttypes.h>
#include <vector>
#include <memory>
#include <MachineSetup.hpp>
#include <ByteCodeBlock.hpp>
#include <LanguageModel.hpp>
#include <LoaderModel.hpp>
#include <ParsedNeuron.hpp>
#include <WritableNeuron.hpp>
#include <DefaultNeurons.hpp>

namespace nnvm {
    namespace system {
        class StringNeuron : public ParsedNeuron
        {
        public:
            StringNeuron(std::shared_ptr<MachineSetup> setup, std::shared_ptr<LibraryLoader> loader);
            ParsedNeuronArgumentType argumentType(int64_t argumentIndex, int64_t argumentTotal);
            std::vector<std::shared_ptr<ByteCodeBlock>> compile(icu::UnicodeString name, LanguageCall& call, CompileContext& cctx, std::shared_ptr<Context> ctx);
        };
    }
}