/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#include <LibraryDescriptor.hpp>
#include "StringNeuron.hpp"
#include <StringUtils.hpp>
#include <ProcessorKnownParameters.hpp>
#include <ByteCodeException.hpp>

namespace nnvm {
    namespace system {
        StringNeuron::StringNeuron(std::shared_ptr<MachineSetup> setup, std::shared_ptr<LibraryLoader> loader)
                : ParsedNeuron(setup, loader) { }
        
        ParsedNeuronArgumentType StringNeuron::argumentType(int64_t argumentIndex, int64_t argumentTotal)
        {
            if (argumentIndex > 1) {
                return ParsedNeuronArgumentType::FAIL;
            }
            if (argumentIndex > 0) {
                return ParsedNeuronArgumentType::SINGLE;
            }
            return ParsedNeuronArgumentType::RAW;
        }
        std::vector<std::shared_ptr<ByteCodeBlock>> StringNeuron::compile(icu::UnicodeString name, LanguageCall& call, CompileContext& cctx, std::shared_ptr<Context> ctx)
        {
            if (call.rawTokens.empty() || call.rawTokens[0].size() != 1 || !call.rawTokens[0][0].isOf(TokenKind::STRING)) {
                throw ByteCodeException(StringUtils::spf(_i("err.compiler.wrong_string"), StringConvUtils::utow(name)));
            }
            auto string = call.rawTokens[0][0].value;
            std::vector<std::shared_ptr<ByteCodeBlock>> res;
            icu::UnicodeString trigger;
            if (call.rawTokens.size() > 1) {
                trigger = call.arguments[1].operand.literal->value;
            }
            // STRING.GET(1,1)
            auto get = std::make_shared<ByteCodeBlock>(name + USS(".GET"), NeuronNames::ADDITIVE);
            get->setParameter(std::make_shared<ByteCodeParameter>(ElementParams::THRESHOLD, (int8_t)1));
            get->setParameter(std::make_shared<ByteCodeParameter>(ElementParams::BIAS, (int8_t)1));
            res.push_back(get);
            
            // trigger -(1,0)-> GET
            if (trigger.countChar32() > 0) {
                auto et = std::make_shared<ByteCodeBlock>(trigger, USS(""));
                et->setParameter(std::make_shared<ByteCodeParameter>(ElementParams::EXPECTED, (int8_t)1));
                auto e = std::make_shared<ByteCodeBlockItem>(get->getName(), et);
                e->setParameter(std::make_shared<ByteCodeParameter>(LinkParams::ACTIVATION, (int8_t)1));
                e->setParameter(std::make_shared<ByteCodeParameter>(LinkParams::STREAM, (int8_t)0));
                get->addIncome(e);
            }
            
            // STRING.LEN(0,strlen)
            auto len = std::make_shared<ByteCodeBlock>(name + USS(".LEN"), NeuronNames::ADDITIVE);
            len->setParameter(std::make_shared<ByteCodeParameter>(ElementParams::THRESHOLD, (int8_t)0));
            len->setParameter(std::make_shared<ByteCodeParameter>(ElementParams::BIAS, (int64_t)string.countChar32()));
            res.push_back(len);
            // STRING.STREAM(0,0)
            auto stream = std::make_shared<ByteCodeBlock>(name + USS(".STREAM"), NeuronNames::ADDITIVE);
            len->setParameter(std::make_shared<ByteCodeParameter>(ElementParams::THRESHOLD, (int8_t)0));
            len->setParameter(std::make_shared<ByteCodeParameter>(ElementParams::BIAS, (int8_t)0));
            res.push_back(len);
            
            std::shared_ptr<ByteCodeBlock> pd = nullptr;
            for (auto i = 0; i < string.countChar32(); ++i) {
                // STRING.C[i](0,char)
                auto c = std::make_shared<ByteCodeBlock>(StringUtils::spf(USS("{0}.C[{1}]"), StringConvUtils::utow(name), i + 1), NeuronNames::ADDITIVE);
                c->setParameter(std::make_shared<ByteCodeParameter>(ElementParams::THRESHOLD, (int8_t)0));
                c->setParameter(std::make_shared<ByteCodeParameter>(ElementParams::BIAS, (int64_t)string.char32At(i)));
                res.push_back(c);
                
                // STRING.D[i](1,1)
                auto d = std::make_shared<ByteCodeBlock>(StringUtils::spf(USS("{0}.D[{1}]"), StringConvUtils::utow(name), i + 1), NeuronNames::ADDITIVE);
                d->setParameter(std::make_shared<ByteCodeParameter>(ElementParams::THRESHOLD, (int8_t)1));
                d->setParameter(std::make_shared<ByteCodeParameter>(ElementParams::BIAS, (int8_t)1));
                res.push_back(d);
                
                // STRING.O[i](1,0)
                auto o = std::make_shared<ByteCodeBlock>(StringUtils::spf(USS("{0}.O[{1}]"), StringConvUtils::utow(name), i + 1), NeuronNames::ADDITIVE);
                o->setParameter(std::make_shared<ByteCodeParameter>(ElementParams::THRESHOLD, (int8_t)1));
                o->setParameter(std::make_shared<ByteCodeParameter>(ElementParams::BIAS, (int8_t)0));
                res.push_back(o);
                
                // GET|D[i-1] -(1,0)-> D[i]
                auto e = std::make_shared<ByteCodeBlockItem>(d->getName(), i > 0 ? pd : get);
                e->setParameter(std::make_shared<ByteCodeParameter>(LinkParams::ACTIVATION, (int8_t)1));
                e->setParameter(std::make_shared<ByteCodeParameter>(LinkParams::STREAM, (int8_t)0));
                d->addIncome(e);
                
                // D[i] -(1,0)-> O[i]
                e = std::make_shared<ByteCodeBlockItem>(o->getName(), d);
                e->setParameter(std::make_shared<ByteCodeParameter>(LinkParams::ACTIVATION, (int8_t)1));
                e->setParameter(std::make_shared<ByteCodeParameter>(LinkParams::STREAM, (int8_t)0));
                o->addIncome(e);
                
                // C[i] -(0,1)-> O[i]
                e = std::make_shared<ByteCodeBlockItem>(o->getName(), c);
                e->setParameter(std::make_shared<ByteCodeParameter>(LinkParams::ACTIVATION, (int8_t)0));
                e->setParameter(std::make_shared<ByteCodeParameter>(LinkParams::STREAM, (int8_t)1));
                o->addIncome(e);
                
                // O[i] -(0,1)-> STREAM
                e = std::make_shared<ByteCodeBlockItem>(stream->getName(), o);
                e->setParameter(std::make_shared<ByteCodeParameter>(LinkParams::ACTIVATION, (int8_t)0));
                e->setParameter(std::make_shared<ByteCodeParameter>(LinkParams::STREAM, (int8_t)1));
                stream->addIncome(e);
                
                pd = d;
            }
            return res;
        }
    }
}

extern "C" EXPORTED int isMachineVersionCompatible(const MachineVersion& version)
{
    return MACHINE_MAJOR_VERSION == version.major;
}

extern "C" EXPORTED std::shared_ptr<nnvm::ExecutableNeuron> getExecutable(icu::UnicodeString name, std::shared_ptr<nnvm::MachineSetup> setup, std::shared_ptr<nnvm::LibraryLoader> loader)
{
    return nullptr;
}

extern "C" EXPORTED std::shared_ptr<nnvm::WritableNeuron> getWriteable(icu::UnicodeString name, std::shared_ptr<nnvm::MachineSetup> setup, std::shared_ptr<nnvm::LibraryLoader> loader)
{
    return nullptr;
}

extern "C" EXPORTED std::shared_ptr<nnvm::ParsedNeuron> getParsed(icu::UnicodeString name, std::shared_ptr<nnvm::MachineSetup> setup, std::shared_ptr<nnvm::LibraryLoader> loader)
{
    return std::make_shared<nnvm::system::StringNeuron>(setup, loader);
}
