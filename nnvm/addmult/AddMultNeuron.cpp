/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#include <LibraryDescriptor.hpp>
#include "AddMultNeuron.hpp"
#include <DefaultNeurons.hpp>

namespace nnvm {
    namespace sainn {
        AddMultNeuron::AddMultNeuron(std::shared_ptr<MachineSetup> setup, std::shared_ptr<LibraryLoader> loader)
                : ParsedNeuron(setup, loader) { }

        ParsedNeuronArgumentType AddMultNeuron::argumentType(int64_t argumentIndex, int64_t argumentTotal)
        {
            // only one argument
            if (argumentIndex > 0) {
                return ParsedNeuronArgumentType::FAIL;
            }
            return ParsedNeuronArgumentType::FLAT;
        }
        std::vector<std::shared_ptr<ByteCodeBlock>> AddMultNeuron::compile(icu::UnicodeString name, LanguageCall& call, CompileContext& cctx, std::shared_ptr<Context> ctx)
        {
            if (call.arguments[0].operand.literal || (call.arguments[0].operand.call && call.arguments[0].operand.call->blockName == NeuronNames::PLUS)) {
                LanguageArgument a(ctx->byteLoader);
                a.operand.expression = std::make_shared<ExpressionTree>(ctx->byteLoader);

                a.operand.expression->left = std::make_shared<Operand>(ctx->byteLoader);
                a.operand.expression->left->literal = std::make_shared<Token<TokenKind>>(ctx->byteLoader);
                a.operand.expression->left->literal->value = USS("0");
                a.operand.expression->left->literal->type.push_back(TokenKind::LITERAL);
                a.operand.expression->left->literal->type.push_back(TokenKind::NUMBER);

                a.operand.expression->middle = USS(">=");

                a.operand.expression->right = std::make_shared<Operand>(ctx->byteLoader);
                a.operand.expression->right->literal = std::make_shared<Token<TokenKind>>(ctx->byteLoader);
                a.operand.expression->right->literal->value = USS("0");
                a.operand.expression->right->literal->type.push_back(TokenKind::LITERAL);
                a.operand.expression->right->literal->type.push_back(TokenKind::NUMBER);

                call.arguments.push_front(a);
                auto n = ctx->getNeuron(NeuronNames::ADDITIVE);
                if (n != ctx->setup->getByteCodeParserSetup()->neurons.end()) {
                    return n->second->compile(name, call, cctx, ctx);
                }
                return std::vector<std::shared_ptr<ByteCodeBlock>>();
            }
            if (call.arguments[0].operand.call && call.arguments[0].operand.call->blockName == NeuronNames::MULTIPLY) {
                auto n = ctx->getNeuron(NeuronNames::MULTIPLICATIVE);
                if (n != ctx->setup->getByteCodeParserSetup()->neurons.end()) {
                    return n->second->compile(name, call, cctx, ctx);
                }
            }
            return std::vector<std::shared_ptr<ByteCodeBlock>>();
        }
    }
}

extern "C" EXPORTED int isMachineVersionCompatible(const MachineVersion& version)
{
    return MACHINE_MAJOR_VERSION == version.major;
}

extern "C" EXPORTED std::shared_ptr<nnvm::ExecutableNeuron> getExecutable(icu::UnicodeString name, std::shared_ptr<nnvm::MachineSetup> setup, std::shared_ptr<nnvm::LibraryLoader> loader)
{
    return nullptr;
}

extern "C" EXPORTED std::shared_ptr<nnvm::WritableNeuron> getWriteable(icu::UnicodeString name, std::shared_ptr<nnvm::MachineSetup> setup, std::shared_ptr<nnvm::LibraryLoader> loader)
{
    return nullptr;
}

extern "C" EXPORTED std::shared_ptr<nnvm::ParsedNeuron> getParsed(icu::UnicodeString name, std::shared_ptr<nnvm::MachineSetup> setup, std::shared_ptr<nnvm::LibraryLoader> loader)
{
    return std::make_shared<nnvm::sainn::AddMultNeuron>(setup, loader);
}
