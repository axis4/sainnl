##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Debug
ProjectName            :=minus
ConfigurationName      :=Debug
WorkspacePath          := "/Users/sergeionov/sources/sainnl/nnvm"
ProjectPath            := "/Users/sergeionov/sources/sainnl/nnvm/minus"
IntermediateDirectory  :=./Debug
OutDir                 := $(IntermediateDirectory)
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=Serge Ionov
Date                   :=26/03/2016
CodeLitePath           :="/Users/sergeionov/Library/Application Support/codelite"
LinkerName             :=/Applications/Xcode.app/Contents/Developer/usr/bin/g++
SharedObjectLinkerName :=/Applications/Xcode.app/Contents/Developer/usr/bin/g++ -dynamiclib -fPIC
ObjectSuffix           :=.o
DependSuffix           :=
PreprocessSuffix       :=.i
DebugSwitch            :=-g 
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=$(IntermediateDirectory)/$(ProjectName).so
Preprocessors          :=
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E
ObjectsFileList        :="minus.txt"
PCHCompileFlags        :=
MakeDirCommand         :=mkdir -p
LinkOptions            :=  -licuuc -licutu -licule -liculx -licuio -licudata -licui18n
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch). $(IncludeSwitch)/usr/local/include 
IncludePCH             := 
RcIncludePath          := 
Libs                   := 
ArLibs                 :=  
LibPath                := $(LibraryPathSwitch). $(LibraryPathSwitch)/usr/local/lib 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := /usr/bin/ar rcu
CXX      := /Applications/Xcode.app/Contents/Developer/usr/bin/g++
CC       := /Applications/Xcode.app/Contents/Developer/usr/bin/gcc
CXXFLAGS := -std=c++14 -std=c++11 -g $(Preprocessors)
CFLAGS   :=  -g $(Preprocessors)
ASFLAGS  := 
AS       := /usr/bin/as


##
## User defined environment variables
##
CodeLiteDir:=/Users/sergeionov/Downloads/codelite.app/Contents/SharedSupport/
Objects0=$(IntermediateDirectory)/ArithmeticPlugins.cpp$(ObjectSuffix) 



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild MakeIntermediateDirs
all: $(OutputFile)

$(OutputFile): $(IntermediateDirectory)/.d $(Objects) 
	@$(MakeDirCommand) $(@D)
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(SharedObjectLinkerName) $(OutputSwitch)$(OutputFile) @$(ObjectsFileList) $(LibPath) $(Libs) $(LinkOptions)
	@$(MakeDirCommand) "/Users/sergeionov/sources/sainnl/nnvm/.build-debug"
	@echo rebuilt > "/Users/sergeionov/sources/sainnl/nnvm/.build-debug/minus"

MakeIntermediateDirs:
	@test -d ./Debug || $(MakeDirCommand) ./Debug


$(IntermediateDirectory)/.d:
	@test -d ./Debug || $(MakeDirCommand) ./Debug

PreBuild:


##
## Objects
##
$(IntermediateDirectory)/ArithmeticPlugins.cpp$(ObjectSuffix): ArithmeticPlugins.cpp 
	$(CXX) $(IncludePCH) $(SourceSwitch) "/Users/sergeionov/sources/sainnl/nnvm/minus/ArithmeticPlugins.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/ArithmeticPlugins.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/ArithmeticPlugins.cpp$(PreprocessSuffix): ArithmeticPlugins.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/ArithmeticPlugins.cpp$(PreprocessSuffix) "ArithmeticPlugins.cpp"

##
## Clean
##
clean:
	$(RM) -r ./Debug/


