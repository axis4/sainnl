cmake_minimum_required(VERSION 3.12)
project(out
        VERSION 1.0
        DESCRIPTION "Compile-time Output neuron for Neural Network Virtual Machine"
        LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_POSITION_INDEPENDENT_CODE ON)
set(CMAKE_CXX_EXTENSIONS OFF)

add_subdirectory (../nnvm "${CMAKE_CURRENT_BINARY_DIR}/nnvm")

set(SOURCE_FILES
        OutNeuron.cpp
        OutNeuron.hpp)

add_library(out SHARED ${SOURCE_FILES})
target_link_libraries (out nnvm)
