/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#include <LibraryDescriptor.hpp>
#include "TickNeuron.hpp"
#include <DefaultNeurons.hpp>
#include <ByteCodeUtils.hpp>

namespace nnvm {
    namespace system {
        TickNeuron::TickNeuron(std::shared_ptr<MachineSetup> setup, std::shared_ptr<LibraryLoader> loader)
                : FunctionNeuron(setup, loader) { }
        
        icu::UnicodeString TickNeuron::getEligibleType()
        {
            return NeuronNames::TICK;
        }
        
        int64_t TickNeuron::getArgumentCount()
        {
            return 1;
        }
        
        void TickNeuron::safeCall(std::shared_ptr<ByteCodeBlock> block,
                                  std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock>>> blocks)
        {
            std::map<icu::UnicodeString, std::shared_ptr<ByteCodeParameter>> params;
            block->getParams(std::vector<icu::UnicodeString>{ LinkParams::ACTIVATION + ElementParams::SUM_SUFFIX }, params);
            
            auto actSum = params[LinkParams::ACTIVATION + ElementParams::SUM_SUFFIX];
            
            auto tmpSum = std::make_shared<ByteCodeParameter>(ElementParams::SUM, (double)0);
            auto sum = tmpSum;
            if (block->tryGetParameter(ElementParams::SUM, sum)) {
                if (sum->getType() != ByteCodeParameterTypes::DOUBLE) {
                    sum = tmpSum;
                    block->setParameter(tmpSum);
                }
            }
            else {
                block->setParameter(tmpSum);
            }
            
            sum->value.doubleValue += ByteCodeUtils::asDouble(*actSum) > 0 ? ExecutableNeuron::setup->getProcessorSetup()->tick : 0;
        }
    }
}

extern "C" EXPORTED int isMachineVersionCompatible(const MachineVersion& version)
{
    return MACHINE_MAJOR_VERSION == version.major;
}

extern "C" EXPORTED std::shared_ptr<nnvm::ExecutableNeuron> getExecutable(icu::UnicodeString name, std::shared_ptr<nnvm::MachineSetup> setup, std::shared_ptr<nnvm::LibraryLoader> loader)
{
    return std::make_shared<nnvm::system::TickNeuron>(setup, loader);
}

extern "C" EXPORTED std::shared_ptr<nnvm::WritableNeuron> getWriteable(icu::UnicodeString name, std::shared_ptr<nnvm::MachineSetup> setup, std::shared_ptr<nnvm::LibraryLoader> loader)
{
    return std::make_shared<nnvm::system::TickNeuron>(setup, loader);
}

extern "C" EXPORTED std::shared_ptr<nnvm::ParsedNeuron> getParsed(icu::UnicodeString name, std::shared_ptr<nnvm::MachineSetup> setup, std::shared_ptr<nnvm::LibraryLoader> loader)
{
    return std::make_shared<nnvm::system::TickNeuron>(setup, loader);
}
