/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#include <LibraryDescriptor.hpp>
#include "PlusNeuron.hpp"
#include <DefaultNeurons.hpp>

namespace nnvm {
    namespace arithmetic {
        PlusNeuron::PlusNeuron(std::shared_ptr<MachineSetup> setup, std::shared_ptr<LibraryLoader> loader)
                : ParsedNeuron(setup, loader) { }

        ParsedNeuronArgumentType PlusNeuron::argumentType(int64_t argumentIndex, int64_t argumentTotal)
        {
            return ParsedNeuronArgumentType::TREE;
        }

        std::vector<std::shared_ptr<ByteCodeBlock>> PlusNeuron::compile(icu::UnicodeString name, LanguageCall& call, nnvm::CompileContext& cctx, std::shared_ptr<Context> ctx)
        {
            auto pcall = std::make_shared<LanguageCall>(call);
            call.blockName = NeuronNames::ADDMULT;
            call.arguments.clear();
            LanguageArgument a(ctx->byteLoader);
            a.operand.call = pcall;
            call.arguments.push_back(a);
            auto n = ctx->getNeuron(NeuronNames::ADDMULT);
            if (n != ctx->setup->getByteCodeParserSetup()->neurons.end()) {
                return n->second->compile(name, call, cctx, ctx);
            }
            return std::vector<std::shared_ptr<ByteCodeBlock>>();
        }
    }
}

extern "C" EXPORTED int isMachineVersionCompatible(const MachineVersion& version)
{
    return MACHINE_MAJOR_VERSION == version.major;
}

extern "C" EXPORTED std::shared_ptr<nnvm::ExecutableNeuron> getExecutable(icu::UnicodeString name, std::shared_ptr<nnvm::MachineSetup> setup, std::shared_ptr<nnvm::LibraryLoader> loader)
{
    return nullptr;
}

extern "C" EXPORTED std::shared_ptr<nnvm::WritableNeuron> getWriteable(icu::UnicodeString name, std::shared_ptr<nnvm::MachineSetup> setup, std::shared_ptr<nnvm::LibraryLoader> loader)
{
    return nullptr;
}

extern "C" EXPORTED std::shared_ptr<nnvm::ParsedNeuron> getParsed(icu::UnicodeString name, std::shared_ptr<nnvm::MachineSetup> setup, std::shared_ptr<nnvm::LibraryLoader> loader)
{
    return std::make_shared<nnvm::arithmetic::PlusNeuron>(setup, loader);
}
