/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#include <LibraryDescriptor.hpp>
#include "StdInNeuron.hpp"
#include <DefaultNeurons.hpp>
#include <BinaryUtils.hpp>

namespace nnvm {
    namespace system {
        StdInNeuron::StdInNeuron(std::shared_ptr<MachineSetup> setup, std::shared_ptr<LibraryLoader> loader)
                : QueuedIONeuron<UChar32>(setup, loader), ParsedNeuron(setup, loader), WritableNeuron(setup, loader) { }
        
        void StdInNeuron::writeSourceBlock(std::shared_ptr<ByteCodeBlock> code, std::shared_ptr<std::ostream> stream)
        {
            *stream << StringConvUtils::utou8(code->getName()) << " is " << StringConvUtils::utou8(getEligibleType());
        }
        
        int8_t StdInNeuron::readData(UChar32& data)
        {
            std::vector<int8_t> tmp;
            // TODO: support UTF-8 - support multi-byte encoding
            if (BinaryUtils::readBinary(this->QueuedIONeuron<UChar32>::setup->getStdIn(), tmp, 1) <= 0 || tmp.size() == 0) {
                return false;
            }
            data = StringConvUtils::u8tou(tmp[0]);
            std::cout << "Indata " << data << std::endl;
            return true;
        }
        int8_t StdInNeuron::writeData(UChar32& data)
        {
            return true;
        }
        icu::UnicodeString StdInNeuron::getEligibleType()
        {
            return NeuronNames::STDIN;
        }
        ParsedNeuronArgumentType StdInNeuron::argumentType(int64_t argumentIndex, int64_t argumentTotal)
        {
            return ParsedNeuronArgumentType::FAIL;
        }
        std::vector<std::shared_ptr<ByteCodeBlock>> StdInNeuron::compile(icu::UnicodeString name, LanguageCall& call, CompileContext& cctx, std::shared_ptr<Context> ctx)
        {
            return { std::make_shared<ByteCodeBlock>(name, this->getEligibleType()) };
        }
    }
}

extern "C" EXPORTED int isMachineVersionCompatible(const MachineVersion& version)
{
    return MACHINE_MAJOR_VERSION == version.major;
}

extern "C" EXPORTED std::shared_ptr<nnvm::ExecutableNeuron> getExecutable(icu::UnicodeString name, std::shared_ptr<nnvm::MachineSetup> setup, std::shared_ptr<nnvm::LibraryLoader> loader)
{
    return std::make_shared<nnvm::system::StdInNeuron>(setup, loader);
}

extern "C" EXPORTED std::shared_ptr<nnvm::WritableNeuron> getWriteable(icu::UnicodeString name, std::shared_ptr<nnvm::MachineSetup> setup, std::shared_ptr<nnvm::LibraryLoader> loader)
{
    return std::make_shared<nnvm::system::StdInNeuron>(setup, loader);
}

extern "C" EXPORTED std::shared_ptr<nnvm::ParsedNeuron> getParsed(icu::UnicodeString name, std::shared_ptr<nnvm::MachineSetup> setup, std::shared_ptr<nnvm::LibraryLoader> loader)
{
    return std::make_shared<nnvm::system::StdInNeuron>(setup, loader);
}
