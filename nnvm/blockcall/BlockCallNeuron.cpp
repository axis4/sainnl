/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#include <LibraryDescriptor.hpp>
#include "BlockCallNeuron.hpp"
#include <Calculator.hpp>
#include <DefaultNeurons.hpp>
#include <ByteCodeUtils.hpp>
#include <StringUtils.hpp>
#include <ByteCodeException.hpp>

namespace nnvm {
    namespace system {
        BlockCallNeuron::BlockCallNeuron(std::shared_ptr<MachineSetup> setup, std::shared_ptr<LibraryLoader> loader)
                : ParsedNeuron(setup, loader) { }
        
        ParsedNeuronArgumentType BlockCallNeuron::argumentType(int64_t argumentIndex, int64_t argumentTotal)
        {
            if (argumentIndex == 0) {
                return ParsedNeuronArgumentType::RAW;
            }
            return ParsedNeuronArgumentType::KEY;
        }
        std::vector<std::shared_ptr<ByteCodeBlock>> BlockCallNeuron::compile(icu::UnicodeString name, LanguageCall& call, CompileContext& cctx, std::shared_ptr<Context> ctx)
        {
            std::vector<std::shared_ptr<ByteCodeBlock>> res;
            if (call.arguments.empty()) {
                // no call
                return res;
            }
            if (!call.arguments[0].rawTokens || call.arguments[0].rawTokens->empty() || !call.arguments[0].rawTokens->front().isOf(TokenKind::STRING)) {
                // wrong first argument
                return res;
            }
            auto tmp = call.arguments[0].rawTokens->front().value;
            icu::UnicodeString blockName;
            tmp.extractBetween(1, tmp.length() - 1, blockName);

            auto newCtx = *cctx.context;
            for (auto & templateArgument : call.templateArguments) {
                if (templateArgument.operand.expression->right->expression) {
                    if (!Calculator::process(*templateArgument.operand.expression->right)) {
                        throw ByteCodeException(StringUtils::spf(_i("err.compiler.non_const_template_value"), StringConvUtils::utow(blockName)));
                    }
                    newCtx[templateArgument.operand.expression->left->literal->value] = ByteCodeUtils::asNumber(templateArgument.operand.expression->right->expression->right->literal->value);
                }
                else {
                    newCtx[templateArgument.operand.expression->left->literal->value] = ByteCodeUtils::asNumber(templateArgument.operand.expression->right->literal->value);
                }
            }

            auto package = LanguageModel::loadBlock(blockName, cctx.prefix, cctx, ctx, newCtx);

            auto prefix = name + USS(".");
            auto bc = std::make_shared<ByteCode>(USS("tmp"), ctx->byteLoader);
            
            // for "single ouput" check
            auto so = 1;
            std::shared_ptr<ByteCodeBlock> soBlock = nullptr;
            
            // fill blocks
            auto r = package->getBlocks();
            for (auto p = std::get<0>(r); p != std::get<1>(r); ++p) {
                auto b = (*p)->isolatedCopy(prefix + (*p)->getName());
                std::shared_ptr<ByteCodeParameter> pr;
                if (b->tryGetParameter(ElementParams::INPUT, pr)) {
                    pr->value.byteValue = 0;
                }
                if (b->tryGetParameter(ElementParams::INDEX, pr)) {
                    pr->value.integerValue = 0;
                }
                if (b->tryGetParameter(ElementParams::OUTPUT, pr) && pr->value.byteValue) {
                    if (!soBlock) {
                        soBlock = b;
                    }
                    else {
                        so = 0;
                    }
                    pr->value.byteValue = 0;
                }
                if (b->getType() == USS("")) {
                    b->changeType(NeuronNames::ADDITIVE);
                }
                bc->addBlock(b);
            }
            
            if (so && soBlock) {
                cctx.remapOutputs->operator[](name) = soBlock->getName();
            }
            
            // fill internal items
            r = package->getBlocks();
            for (auto p = std::get<0>(r); p != std::get<1>(r); ++p) {
                auto b = bc->getBlock(prefix + (*p)->getName());
                auto be = bc->getBlocks();
                if (b == std::get<1>(be)) {
                    throw ByteCodeException(USS("Incomplete state of ByteCode"));
                }
                auto r2 = (*p)->getItems();
                for (auto i = std::get<0>(r2); i != std::get<1>(r2); ++i) {
                    auto l = (*i)->getOwner().lock();
                    if (!l) {
                        throw ByteCodeException(USS("Incomplete state of ByteCode"));
                    }
                    auto o = bc->getBlock(prefix + l->getName());
                    auto oe = bc->getBlocks();
                    if (o == std::get<1>(oe)) {
                        throw ByteCodeException(USS("Incomplete state of ByteCode"));
                    }
                    auto ni = (*i)->isolatedCopy(prefix + (*i)->getName());
                    ni->changeOwner(*o);
                    (*b)->addItem(ni);
                }
            }
            ByteCodeUtils::finalizeCode(bc);
            
            // Add links to inputs. Links from outputs are autobinded by Expected bindings.
            if (call.arguments.size() > 1) {
                auto i = call.arguments.begin();
                ++i;
                for (; i != call.arguments.end(); ++i) {
                    // arguments are expressions of A : B
                    if (i->operand.expression->middle != USS(":")) {
                        // TODO: support positional arguments later
                        return res;
                    }
                    auto block_key = i->operand.expression->left->literal->value;
                    auto input_source = i->operand.expression->right->literal->value;
                    auto b = bc->getBlock(prefix + block_key);
                    auto be = bc->getBlocks();
                    if (b == std::get<1>(be)) {
                        throw ByteCodeException(USS("Wrong key on block call: ") + block_key);
                    }
                    auto e = std::make_shared<ByteCodeBlock>(input_source, NeuronNames::ADDITIVE);
                    e->setParameter(std::make_shared<ByteCodeParameter>(ElementParams::EXPECTED, (int8_t)1));
                    bc->addBlock(e);
                    auto bi = std::make_shared<ByteCodeBlockItem>(prefix + block_key, e);
                    bi->setParameter(std::make_shared<ByteCodeParameter>(LinkParams::STREAM, 1.0));
                    (*b)->addIncome(bi);
                }
            }
            
            r = bc->getBlocks();
            res.insert(res.begin(), std::get<0>(r), std::get<1>(r));
            return res;
        }
    }
}

extern "C" EXPORTED int isMachineVersionCompatible(const MachineVersion& version)
{
    return MACHINE_MAJOR_VERSION == version.major;
}

extern "C" EXPORTED std::shared_ptr<nnvm::ExecutableNeuron> getExecutable(icu::UnicodeString name, std::shared_ptr<nnvm::MachineSetup> setup, std::shared_ptr<nnvm::LibraryLoader> loader)
{
    return nullptr;
}

extern "C" EXPORTED std::shared_ptr<nnvm::WritableNeuron> getWriteable(icu::UnicodeString name, std::shared_ptr<nnvm::MachineSetup> setup, std::shared_ptr<nnvm::LibraryLoader> loader)
{
    return nullptr;
}

extern "C" EXPORTED std::shared_ptr<nnvm::ParsedNeuron> getParsed(icu::UnicodeString name, std::shared_ptr<nnvm::MachineSetup> setup, std::shared_ptr<nnvm::LibraryLoader> loader)
{
    return std::make_shared<nnvm::system::BlockCallNeuron>(setup, loader);
}
