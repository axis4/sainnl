{
    "id": "https://bitbucket.org/axis4/sainnl/raw/default/nnvm/nnvm/ByteCodeParameterSchema.js",
    "$schema": "http://json-schema.org/draft-04/schema#",
    "type": "object",
    "title": "ByteCodeParameter",
    "patternProperties": {
        "^.+$": {
            "type": "object",
            "oneOf": [
                      { "$ref": "#/definitions/byte" },
                      { "$ref": "#/definitions/integer" },
                      { "$ref": "#/definitions/double" },
                      { "$ref": "#/definitions/string" }
                  ]
        }
    },
    "definitions": {
        "byte": {
            "properties": {
                "name": {
                    "type": "string"
                },
                "type": {
                    "type": "integer",
                    "minimum": 0,
                    "maximum": 0
                },
                "value": {
                    "type": "integer",
                    "minimum": -127,
                    "maximum": 127
                }
            }
        },
        "integer": {
            "properties": {
                "name": {
                    "type": "string"
                },
                "type": {
                    "type": "integer",
                    "minimum": 1,
                    "maximum": 1
                },
                "value": {
                    "type": "integer"
                }
            }
        },
        "double": {
            "properties": {
                "name": {
                    "type": "string"
                },
                "type": {
                    "type": "integer",
                    "minimum": 2,
                    "maximum": 2
                },
                "value": {
                    "type": "number"
                }
            }
        },
        "string": {
            "properties": {
                "name": {
                    "type": "string"
                },
                "type": {
                    "type": "integer",
                    "minimum": 3,
                    "maximum": 3
                },
                "value": {
                    "type": "string"
                }
            }
        }
    }
}