{
    "id": "https://bitbucket.org/axis4/sainnl/raw/default/nnvm/nnvm/ByteCodeSchema.js",
    "$schema": "http://json-schema.org/draft-04/schema#",
    "title": "ByteCode",
    "description": "A ByteCode configuration",
    "type": "object",
    "properties": {
        "packageName": {
            "description": "The name of loadable ByteCode",
            "type": "string"
        },
        "blocks": {
            "description": "The list of all ByeCodeBlocks",
            "type": "array",
            "items": {
                "$ref": "https://bitbucket.org/axis4/sainnl/raw/default/nnvm/nnvm/ByteCodeBlockSchema.js"
            }
        }
    },
    "required": ["packageName"],
    "additionalProperties": false
}
