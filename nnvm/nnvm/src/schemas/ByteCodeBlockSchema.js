{
    "id": "https://bitbucket.org/axis4/sainnl/raw/default/nnvm/nnvm/ByteCodeBlockSchema.js",
    "$schema": "http://json-schema.org/draft-04/schema#",
    "title": "ByteCodeBlock",
    "type": "object",
    "properties": {
        "blockName": {
            "description": "The name of each block",
            "type": "string"
        }
        "type": {
            "description": "The type of each block",
            "type": "string"
        },
        "parameters": {
            "$ref": "https://bitbucket.org/axis4/sainnl/raw/default/nnvm/nnvm/ByteCodeParameterSchema.js"
        },
        "items": {
            "description": "The list of all ByeCodeBlockItems",
            "type": "array",
            "items": {
                "$ref": "https://bitbucket.org/axis4/sainnl/raw/default/nnvm/nnvm/ByteCodeBlockItemSchema.js"
            }
        }
    },
    "required": ["blockName", "type"],
    "additionalProperties": false
}
