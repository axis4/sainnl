{
    "id": "https://bitbucket.org/axis4/sainnl/raw/default/nnvm/nnvm/ByteCodeBlockItemSchema.js",
    "$schema": "http://json-schema.org/draft-04/schema#",
    "title": "ByteCodeBlockItem",
    "type": "object",
    "properties": {
        "itemName": {
            "description": "The name of each item of block",
            "type": "string"
        }
        "parameters": {
            "$ref": "https://bitbucket.org/axis4/sainnl/raw/default/nnvm/nnvm/ByteCodeParameterSchema.js"
        }
    },
    "required": ["itemName"],
    "additionalProperties": false
}
