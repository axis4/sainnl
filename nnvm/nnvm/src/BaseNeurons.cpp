/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#include <BaseNeurons.hpp>
#include <list>
#include <vector>
#include <iostream>
#include <ByteCodeParameter.hpp>
#include <ByteCodeException.hpp>
#include <Internationalization.hpp>
#include "utils/Utils.hpp"

namespace nnvm {
    SelfCheckedNeuron::SelfCheckedNeuron(std::shared_ptr<MachineSetup> setup, std::shared_ptr<LibraryLoader> loader)
            : ExecutableNeuron(setup, loader) { }

    void SelfCheckedNeuron::call(std::shared_ptr<ByteCodeBlock> block,
                                 std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock>>> blocks)
    {
        safe();
        auto s = getEligibleType();
        if (block->getType() != s) {
            throw ByteCodeException(StringUtils::spf(_i("err.plugin.wrongstate"),
                                                     StringConvUtils::utow(s),
                                                     StringConvUtils::utow(block->getType())));
        }
        safeCall(block, blocks);
    }

    void SelfCheckedNeuron::removed(std::shared_ptr<ByteCodeBlock> code,
                                    std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock>>> codes)
    {
        // ignore
    }

    FunctionNeuron::FunctionNeuron(std::shared_ptr<MachineSetup> setup, std::shared_ptr<LibraryLoader> loader)
            : SelfCheckedNeuron(setup, loader), ParsedNeuron(setup, loader), WritableNeuron(setup, loader) { }

    void FunctionNeuron::writeSourceBlock(std::shared_ptr<ByteCodeBlock> code, std::shared_ptr<std::ostream> stream)
    {
        *stream << StringConvUtils::utou8(code->getName()) << " is " << StringConvUtils::utou8(getEligibleType());
        if (getArgumentCount() > 0) {
            *stream << " ";
            for (auto i = 0; i < getArgumentCount(); i++) {
                if (i > 0) {
                    *stream << ", ";
                }
                argumentWriters[i](code, stream);
            }
        }
    }
                
    ParsedNeuronArgumentType FunctionNeuron::argumentType(int64_t argumentIndex, int64_t argumentTotal)
    {
        if (argumentIndex >= this->getArgumentCount() || argumentIndex >= argumentTotal) {
            return ParsedNeuronArgumentType::FAIL;
        }
        return ParsedNeuronArgumentType::FLAT;
    }

    std::vector<std::shared_ptr<ByteCodeBlock>> FunctionNeuron::compile(icu::UnicodeString name, LanguageCall& call, CompileContext& cctx, std::shared_ptr<Context> ctx)
    {
        std::vector<std::shared_ptr<ByteCodeBlock>> rv;
        std::shared_ptr<ByteCodeBlock> res(new ByteCodeBlock(name, this->getEligibleType()));
        rv.push_back(res);
        if (call.arguments.size() > 0) {
            auto act = compileExpression(call.arguments[0].operand, name, LinkParams::ACTIVATION, res, rv, ctx);
            for (auto i = act.begin(); i != act.end(); ++i) {
                rv.push_back((*i)->getOwner().lock());
                res->addIncome(*i);
            }
        }
        if (call.arguments.size() > 1) {
            auto str = compileExpression(call.arguments[1].operand, name, LinkParams::STREAM, res, rv, ctx);
            for (auto i = str.begin(); i != str.end(); ++i) {
                std::shared_ptr<ByteCodeBlockItem> ep;
                auto r = res->getIncomes();
                for (auto l = std::get<0>(r); l != std::get<1>(r); ++l) {
                    if ((*l)->getOwner().lock()->getName() == (*i)->getOwner().lock()->getName()) {
                        ep = *l;
                    }
                }
                if (!ep) {
                    ep = std::make_shared<ByteCodeBlockItem>(name, (*i)->getOwner());
                    res->addIncome(ep);
                }
                ep->setParameter((*i)->getParameter(LinkParams::STREAM));
            }
        }
        return rv;
    }

    ThreadedNeuron::ThreadedNeuron(std::shared_ptr<MachineSetup> setup, std::shared_ptr<LibraryLoader> loader)
            : SelfCheckedNeuron(setup, loader), lockedSetup(setup) { }

    void ThreadedNeuron::threadFunc(std::shared_ptr<ThreadNeuronDescriptor> desc)
    {
        auto b = desc->blocks->find(desc->block);
        if (b == desc->blocks->end()) {
            return;
        }
        auto block = b->second;
        while (desc->running) {
            subprocessLogic(block);
        }
    }

    void ThreadedNeuron::safeCall(std::shared_ptr<ByteCodeBlock> block,
                                  std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock>>> blocks)
    {
        auto f = threads.find(block->getName());
        if (f == threads.end()) {
            auto t = std::make_shared<ThreadNeuronDescriptor>();
            t->running = true;
            t->block = block->getName();
            t->blocks = blocks;
            t->thread = std::make_shared<std::thread>(&ThreadedNeuron::threadFunc, this, t);
            threads[block->getName()] = t;
        }
    }

    void ThreadedNeuron::removed(std::shared_ptr<ByteCodeBlock> code,
                                 std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock>>> codes)
    {
        auto i = threads.find(code->getName());
        if (i != threads.end()) {
            auto p = i->second;
            threads.erase(i);
            p->running = false;
            if (p->thread->joinable()) {
                p->thread->join();
            }
        }
    }

    ThreadedNeuron::~ThreadedNeuron()
    {
        for (auto i = threads.begin(); i != threads.end(); ++i) {
            i->second->running = false;
            if (i->second->thread->joinable()) {
                i->second->thread->join();
            }
        }
        threads.clear();
        lockedSetup = std::shared_ptr<MachineSetup>(nullptr);
    }
}
