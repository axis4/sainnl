/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#include <LibraryLoader.hpp>
#include <dlfcn.h>
#include <ByteCodeException.hpp>
#include <BinaryUtils.hpp>
#include "../utils/Utils.hpp"

namespace nnvm {
    class LibraryHandle
    {
    public:
        LibraryHandle(std::function<void(const LibraryHandle&, icu::UnicodeString)> load, std::function<void(const LibraryHandle&)> unload,
                      std::wstring fullPath, std::shared_ptr<void> descriptor, std::shared_ptr<MachineSetup> setup)
                : load(load), unload(unload), fullPath(fullPath), descriptor(descriptor), setup(setup) { }
        ~LibraryHandle()
        {
            unload(*this);
        }

        std::wstring fullPath;
        std::shared_ptr<void> descriptor;
        std::function<void(const LibraryHandle&, icu::UnicodeString)> load;
        std::function<void(const LibraryHandle&)> unload;
        std::shared_ptr<MachineSetup> setup;
    };

    class NativeHandle
    {
    public:
        void* handle;
    };

    class LibraryLoaderState
    {
    public:
        typedef std::function<void(const std::wstring& libraryPath, const std::wstring& packagePath, std::wstring& fullpath, std::shared_ptr<LibraryHandle>& handle, icu::UnicodeString& err)> NativeProbeDelegate;

        typedef int (*versionCheck)(const MachineVersion& version);
        typedef std::shared_ptr<nnvm::ExecutableNeuron> (*executableFactory)(icu::UnicodeString name, std::shared_ptr<nnvm::MachineSetup> setup, std::shared_ptr<nnvm::LibraryLoader> loader);
        typedef std::shared_ptr<nnvm::WritableNeuron> (*writeableFactory)(icu::UnicodeString name, std::shared_ptr<nnvm::MachineSetup> setup, std::shared_ptr<nnvm::LibraryLoader> loader);
        typedef std::shared_ptr<nnvm::ParsedNeuron> (*parsableFactory)(icu::UnicodeString name, std::shared_ptr<nnvm::MachineSetup> setup, std::shared_ptr<nnvm::LibraryLoader> loader);


        static const std::wstring prefixlibrary;
        static const std::wstring library;
        static const std::wstring lua;
        static const std::wstring python;

        static const std::string versionSymbol;
        static const std::string executableSymbol;
        static const std::string writableSymbol;
        static const std::string parsableSymbol;

        std::map<icu::UnicodeString, std::shared_ptr<LibraryHandle>> handles;
        std::vector<NativeProbeDelegate> fileProbes;

        static std::shared_ptr<LibraryHandle> openLibrary(std::wstring fullPath, std::shared_ptr<MachineSetup> setup, std::shared_ptr<LibraryLoader> loader, icu::UnicodeString& err)
        {
            auto descriptor = std::make_shared<NativeHandle>();
            descriptor->handle = dlopen(StringConvUtils::wtou8(fullPath).c_str(), RTLD_NOW);
            if (!descriptor->handle) {
                err = StringConvUtils::u8tou(std::string(dlerror()));
                return nullptr;
            }
            return std::make_shared<LibraryHandle>(
                    [setup, loader](const LibraryHandle& handle, icu::UnicodeString package)
                    {
                        auto h = std::static_pointer_cast<NativeHandle>(handle.descriptor)->handle;
                        auto f = dlsym(h, versionSymbol.c_str());
                        if (f) {
                            if (!((versionCheck) f)(setup->version())) {
                                return;
                            }
                        }

                        f = dlsym(h, parsableSymbol.c_str());
                        if (f) {
                            auto p = ((parsableFactory) f)(package, setup, loader);
                            if (p) {
                                setup->getByteCodeParserSetup()->neurons[package] = p;
                            }
                        }

                        f = dlsym(h, executableSymbol.c_str());
                        if (f) {
                            auto e = ((executableFactory) f)(package, setup, loader);
                            if (e) {
                                setup->getProcessorSetup()->addNeuron(package, e);
                            }
                        }

                        f = dlsym(h, writableSymbol.c_str());
                        if (f) {
                            auto w = ((writeableFactory) f)(package, setup, loader);
                            if (w) {
                                setup->getByteCodeWriterSetup()->addNeuron(package, w);
                            }
                        }
                    },
                    [](const LibraryHandle& handle)
                    {
                        // TODO: cleanup neurons of library
                        std::wcout << L"Closed " << handle.fullPath << std::endl;
                        dlclose(std::static_pointer_cast<NativeHandle>(handle.descriptor)->handle);
                    },
                    fullPath, descriptor, setup);
        };
    };

#if U_PLATFORM_HAS_WIN32_API > 0
    const std::wstring LibraryLoaderState::library = L".dll";
    const std::wstring LibraryLoaderState::prefixlibrary = L"";
#elif U_PLATFORM_IS_DARWIN_BASED > 0
    const std::wstring LibraryLoaderState::library = L".dylib";
    const std::wstring LibraryLoaderState::prefixlibrary = L"lib";
#else
    const std::wstring LibraryLoaderState::library = L".so";
    const std::wstring LibraryLoaderState::prefixlibrary = L"lib";
#endif
    const std::wstring LibraryLoaderState::lua = L".lua";
    const std::wstring LibraryLoaderState::python = L".py";

    const std::string LibraryLoaderState::versionSymbol = "isMachineVersionCompatible";
    const std::string LibraryLoaderState::executableSymbol = "getExecutable";
    const std::string LibraryLoaderState::writableSymbol = "getWriteable";
    const std::string LibraryLoaderState::parsableSymbol = "getParsed";

    LibraryLoader::LibraryLoader(std::shared_ptr<MachineSetup> setup) : setup(setup), state(std::make_shared<LibraryLoaderState>())
    {
        state->fileProbes.push_back([this, setup](const std::wstring& libraryPath, const std::wstring& packagePath, std::wstring& fullpath, std::shared_ptr<LibraryHandle>& handle, icu::UnicodeString& err)
                             {
                                 auto l = packagePath.rfind(BinaryUtils::packageSeparatorWide);
                                 if (l != std::wstring::npos && l < packagePath.size()) {
                                     fullpath = libraryPath +
                                             std::wstring(packagePath.begin(), packagePath.begin() + l + 1) +
                                             LibraryLoaderState::prefixlibrary + std::wstring(packagePath.begin() + l + 1, packagePath.end()) +
                                             LibraryLoaderState::library;
                                 }
                                 else {
                                     fullpath = libraryPath +
                                             LibraryLoaderState::prefixlibrary +
                                             packagePath +
                                             LibraryLoaderState::library;
                                 }
                                 handle = LibraryLoaderState::openLibrary(fullpath, setup, this->shared_from_this(), err);
                             });

        state->fileProbes.push_back([setup](const std::wstring& libraryPath, const std::wstring& packagePath, std::wstring& fullpath, std::shared_ptr<LibraryHandle>& handle, icu::UnicodeString& err)
                             {
                                 fullpath = libraryPath + packagePath + LibraryLoaderState::lua;
                                 handle = nullptr;
                                 err = USS("Not implemented");
                             });

        state->fileProbes.push_back([setup](const std::wstring& libraryPath, const std::wstring& packagePath, std::wstring& fullpath, std::shared_ptr<LibraryHandle>& handle, icu::UnicodeString& err)
                             {
                                 fullpath = libraryPath + packagePath + LibraryLoaderState::python;
                                 handle = nullptr;
                                 err = USS("Not implemented");
                             });
    }

    void LibraryLoader::load(icu::UnicodeString package)
    {
        safe();

        std::shared_ptr<LibraryHandle> s;
        // a.b.c.d
        auto packagepath = StringConvUtils::utow(package);

        // a/b/c/d
        auto p = packagepath.find(BinaryUtils::packageSeparatorWide);
        while (p != std::wstring::npos) {
            packagepath = packagepath.replace(p, 1, BinaryUtils::pathSeparatorWide);
            p = packagepath.find(BinaryUtils::packageSeparatorWide);
        }

        auto lp = this->setup->getByteCodeLoaderSetup()->nativePaths;
        std::wstring fullpath;
        icu::UnicodeString err;
        for (auto libpath = lp.begin(); libpath != lp.end(); ++libpath) {
            auto path = StringConvUtils::utow(*libpath);
            if (path.back() != BinaryUtils::pathSeparatorWide[0]) {
                path += BinaryUtils::pathSeparatorWide;
            }

            // TODO: timestamp based probe selection

            for (auto probe = state->fileProbes.begin(); probe != state->fileProbes.end(); ++probe) {
                probe->operator()(path, packagepath, fullpath, s, err);
                if (s) {
                    break;
                }
                setup->getLogger()->debug(USS("Native library not linked ") + StringConvUtils::wtou(fullpath) + USS(". Error: ") + err);
            }
            if (s) {
                break;
            }
        }
        if (!s) {
            // not found, ignore - it can be block
            return;
        }

        setup->getLogger()->info(USS("Linking ") + package);
        setup->getLogger()->debug(USS("Pointing to file ") + StringConvUtils::wtou(fullpath));

        state->handles[package] = s;
        s->load(*s, package);

        setup->getLogger()->debug(USS("Neuron library linked"));
    }
}
