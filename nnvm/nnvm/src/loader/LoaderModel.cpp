/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#include <LoaderModel.hpp>
#include <ByteCodeException.hpp>

namespace nnvm {
    int8_t context_in_noop(const std::map<icu::UnicodeString, std::map<icu::UnicodeString, std::tuple<double, double, double> > >& flow)
    {
        return 0;
    }
    int8_t context_out_noop(std::map<icu::UnicodeString, std::map<icu::UnicodeString, std::tuple<double, double, double> > >& flow)
    {
        return 0;
    }

    Context::Context(std::shared_ptr<MachineSetup> setup, std::shared_ptr<LibraryLoader> loader, std::shared_ptr<ByteCodeLoader> byteLoader)
        : setup(setup), loader(loader), byteLoader(byteLoader), counters(setup->getDefaultSampling()),
        compiled(std::make_shared<std::map<icu::UnicodeString, std::shared_ptr<ByteCode> > >()),
        prepared(std::make_shared<std::map<icu::UnicodeString, LanguageBlock> >()),
        compileStarted(std::make_shared<std::set<icu::UnicodeString> >()),
        processExternalLinks(context_in_noop),
        prepareExternalLinks(context_out_noop) { }

    void Context::useByteCode(std::shared_ptr<ByteCode> code)
    {
        executed = code;
        std::vector<std::shared_ptr<ByteCode>> v{code};
        auto vc = byteLoader->mergeByteCodes(v);
        setup->getProcessorSetup()->preSetup(vc);
    }

    std::map<icu::UnicodeString, std::shared_ptr<ParsedNeuron> >::iterator Context::getNeuron(icu::UnicodeString blockName)
    {
        auto neuron = setup->getByteCodeParserSetup()->neurons.find(blockName);
        if (neuron == setup->getByteCodeParserSetup()->neurons.end()) {
            try {
                loader->load(blockName);
            }
            catch(ByteCodeException e) {
                // pass
            }
            neuron = setup->getByteCodeParserSetup()->neurons.find(blockName);
        }
        return neuron;
    }

    void Context::globalBlock()
    {
        std::unique_lock<std::mutex> lk(cvMutex);
        cv.wait(lk);
    }

    void Context::globalRelease()
    {
        cv.notify_all();
    }
}
