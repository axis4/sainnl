/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#include <ByteCodeLoader.hpp>
#include <ByteCodeParser.hpp>
#include <ByteCodeException.hpp>
#include <ByteCodeParserException.hpp>
#include <Internationalization.hpp>
#include <BinaryUtils.hpp>
#include <algorithm>
#include <utility>
#include "../utils/Utils.hpp"

namespace nnvm {
    std::wstring ByteCodeLoader::sourceExt = L".nl";
    std::wstring ByteCodeLoader::jsonExt = L".nlj";
    std::wstring ByteCodeLoader::binExt = L".nlc";

    ByteCodeLoader::ByteCodeLoader(std::shared_ptr<MachineSetup> setup, std::shared_ptr<LibraryLoader> library) : setup(std::move(setup)), library(std::move(library)), fileProbes()
    {
        fileProbes.emplace_back([](const std::wstring& libraryPath, const std::wstring& packagePath, const std::wstring& blockPath, const std::wstring& block, std::wstring& fullpath, std::shared_ptr<std::ifstream>& stream, int8_t& is_block, int64_t& err)
                             {
                                 fullpath = libraryPath + packagePath + binExt;
                                 stream = BinaryUtils::openStream(StringConvUtils::wtou(fullpath), err);
                             });
        
        fileProbes.emplace_back([](const std::wstring& libraryPath, const std::wstring& packagePath, const std::wstring& blockPath, const std::wstring& block, std::wstring& fullpath, std::shared_ptr<std::ifstream>& stream, int8_t& is_block, int64_t& err)
                             {
                                 fullpath = libraryPath + packagePath + jsonExt;
                                 stream = BinaryUtils::openTextStream(StringConvUtils::wtou(fullpath), err);
                             });
        
        fileProbes.emplace_back([](const std::wstring& libraryPath, const std::wstring& packagePath, const std::wstring& blockPath, const std::wstring& block, std::wstring& fullpath, std::shared_ptr<std::ifstream>& stream, int8_t& is_block, int64_t& err)
                             {
                                 fullpath = libraryPath + blockPath + sourceExt;
                                 stream = BinaryUtils::openTextStream(StringConvUtils::wtou(fullpath), err);
                                 is_block = 1;
                             });
    }

    ByteCodeLoader::~ByteCodeLoader() = default;

    std::shared_ptr<ByteCode> ByteCodeLoader::load(const icu::UnicodeString& package, std::shared_ptr<Context> ctx)
    {
        safe();
        
        // package is for sure the name in SAINN block library, not a neuron
        
        std::shared_ptr<std::ifstream> s;
        // a.b.c.d
        auto packagepath = StringConvUtils::utow(package);
        auto p = packagepath.rfind(BinaryUtils::packageSeparatorWide);
        // a.b.c
        auto prefix = p != std::wstring::npos ? StringConvUtils::wtou(std::wstring(packagepath.begin(), packagepath.begin() += p)) : USS("");
        
        // a/b/c/d
        p = packagepath.find(BinaryUtils::packageSeparatorWide);
        while (p != std::wstring::npos) {
            packagepath = packagepath.replace(p, 1, BinaryUtils::pathSeparatorWide);
            p = packagepath.find(BinaryUtils::packageSeparatorWide);
        }
        
        // a/b/c
        p = packagepath.rfind(BinaryUtils::pathSeparatorWide);
        auto blockpath = p != std::wstring::npos ? std::wstring(packagepath.begin(), packagepath.begin() += p) : L"";
        // d
        auto block = std::wstring(packagepath.begin() += p + 1, packagepath.end());
        int8_t is_block = 0;
        auto lp = this->setup->getLibPaths();
        std::wstring fullpath;
        int64_t err = 0;
        for (auto & libpath : lp) {
            auto path = StringConvUtils::utow(libpath);
            if (path.back() != BinaryUtils::pathSeparatorWide[0]) {
                path += BinaryUtils::pathSeparatorWide;
            }
            
            // TODO: timestamp based probe selection
            
            for (auto & fileProbe : fileProbes) {
                fileProbe.operator()(path, packagepath, blockpath, block, fullpath, s, is_block, err);
                if (s->is_open()) {
                    break;
                }
            }
            if (s->is_open()) {
                break;
            }
        }
        if (!s->is_open()) {
            auto w = StringConvUtils::utow(package);
            auto wp = StringConvUtils::utow(StringUtils::joinArray(this->setup->getLibPaths(), ","));
            auto we = StringConvUtils::u8tow(std::string(strerror((int)err)));
            throw ByteCodeException(StringUtils::spf(_i("err.package.not.found"), w, wp, we));
        }
        
        setup->getLogger()->info(USS("Loading ") + package);
        setup->getLogger()->debug(USS("Pointing to file ") + StringConvUtils::wtou(fullpath));
        
        ByteCodeParser parser(s, prefix, package, std::move(ctx));
        std::shared_ptr<std::vector<std::shared_ptr<ByteCode>>> res;
        try {
            res = parser.read();
        }
        catch (ByteCodeParserException& e) {
            throw ByteCodeException(StringUtils::spf(_i("err.package.not.found"), StringConvUtils::utow(package), StringConvUtils::utow(StringUtils::joinArray(this->setup->getLibPaths(), ",")), StringConvUtils::u8tow(e.what())));
        }
        if (!res) {
            throw ByteCodeException(StringUtils::spf(_i("err.package.not.found"), StringConvUtils::utow(package), StringConvUtils::utow(StringUtils::joinArray(this->setup->getLibPaths(), ",")), L"Package parser error"));
        }
        
        setup->getLogger()->debug(USS("File parsed"));

        std::shared_ptr<ByteCode> r = std::shared_ptr<ByteCode>(nullptr);
        if (is_block) {
            for (auto & i : *res) {
                if (i->getName() == package) {
                    r = i;
                }
            }
        }
        else {
            r = res->front();
        }
        if (!r) {
            throw ByteCodeException(StringUtils::spf(_i("err.package.not.found"), StringConvUtils::utow(package), StringConvUtils::utow(StringUtils::joinArray(this->setup->getLibPaths(), ",")), L"Package not found after parsing"));
        }
        if (r->getName() != package) {
            throw ByteCodeException(StringUtils::spf(_i("err.package.mismatch"), StringConvUtils::utow(r->getName()), StringConvUtils::utow(package), StringConvUtils::utow(StringUtils::joinArray(this->setup->getLibPaths(), ","))));
        }
        return r;
    }

    std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock>>> ByteCodeLoader::mergeByteCodes(const std::vector<std::shared_ptr<ByteCode>>& codes)
    {
        auto res = std::make_shared<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock>>>();
        for (const auto& c : codes) {
            auto r = c->getBlocks();
            for (auto k = std::get<0>(r); k != std::get<1>(r); k++) {
                auto b = *k;
                res->operator[](b->getName()) = b;
            }
        }
        return res;
    }

    std::shared_ptr<std::set<icu::UnicodeString>> ByteCodeLoader::extractNeurons(const std::vector<std::shared_ptr<ByteCode>>& codes)
    {
        auto result = std::make_shared<std::set<icu::UnicodeString>>();
        for (const auto& c : codes) {
            auto r = c->getBlocks();
            for (auto k = std::get<0>(r); k != std::get<1>(r); k++) {
                auto b = *k;
                result->insert(b->getType());
                setup->getLogger()->debug(USS("Detected neuron ") + b->getType());
            }
        }
        return result;
    }
}
