/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#include "VM.hpp"
#include <ByteCodeException.hpp>
#include <Internationalization.hpp>
#include <BinaryUtils.hpp>
#include <DefaultNeurons.hpp>
#include <ByteCodeUtils.hpp>
#include "utils/Utils.hpp"
#include "writer/ByteCodeWriter.hpp"
#include "NNProtocol.hpp"

namespace nnvm {
    icu::UnicodeString applyName(const icu::UnicodeString &name, int64_t tick)
    {
        if (tick < 0) {
            return name;
        }
        std::vector<icu::UnicodeString> r;
        StringUtils::split(r, name, L"\\.");
        if (r.size() > 1) {
            r.insert(++r.cbegin(), StringUtils::spf(USS("{0}"), tick));
        }
        else {
            r.push_back(StringUtils::spf(USS("{0}"), tick));
        }
        return StringUtils::joinArray(r, USS("."));
    }

    VM::VM(std::map<icu::UnicodeString, icu::UnicodeString>& args, std::shared_ptr<Processor> processor, std::shared_ptr<ByteCodeLoader> loader, std::shared_ptr<MachineSetup> setup)
    {
        auto l = args.find(_i("key.libs"));
        std::vector<icu::UnicodeString> libPath;
        if (l != args.end()) {
            StringUtils::split(libPath, l->second, USS(","));
        }
        else {
            libPath.push_back(_i("default.libs"));
        }

        l = args.find(_i("key.natives"));
        std::vector<icu::UnicodeString> nativePath;
        if (l != args.end()) {
            StringUtils::split(nativePath, l->second, USS(","));
        }
        else {
            nativePath.push_back(_i("default.natives"));
        }

        setup = setup ? setup : std::make_shared<MachineSetup>(
             args.find(_i("key.compile_only")) != args.end() ? nullptr : std::make_shared<ProcessorSetup>(),
             std::make_shared<ByteCodeLoaderSetup>(nativePath),
             std::make_shared<ByteCodeParserSetup>(),
             std::make_shared<ByteCodeWriterSetup>(),
             std::make_shared<RemotingSetup>(),
             libPath);
        pointerPool.track(setup);
        pointerPool.track(setup->getLogger());
        pointerPool.track(setup->getProcessorSetup());
        pointerPool.track(setup->getByteCodeLoaderSetup());
        pointerPool.track(setup->getByteCodeParserSetup());
        pointerPool.track(setup->getByteCodeWriterSetup());
        pointerPool.track(setup->getRemotingSetup());

        auto ari = args.find(_i("key.allow_remote_init"));
        if (ari != args.end()) {
            setup->getProcessorSetup()->allowRemoteInit = ari->second == USS("1");
        }

        if (!setup->getProcessorSetup()->allowRemoteInit) {
            auto p = args.find(_i("key.package"));
            if (p == args.end()) {
                throw ByteCodeException(_i("err.nopackage"));
            }
            this->mainPackage = p->second;
        }

        auto pm = args.find(_i("key.parallel"));
        if (pm != args.end()) {
            std::map<icu::UnicodeString, ProcessorParallelMode> modes
                    {
                            { USS("none"), ProcessorParallelMode::NONE },
                            { USS("omp"), ProcessorParallelMode::OMP },
                            //{ USS("ocl"), ProcessorParallelMode::OCL },
                    };
            auto pmv = modes.find(pm->second);
            if (pmv == modes.end()) {
                throw ByteCodeException(_i("err.unknown_parallel_mode"));
            }
            setup->getProcessorSetup()->parallelMode = pmv->second;
        }

        auto rs = args.find(_i("key.remote_sync"));
        if (rs != args.end()) {
            setup->getProcessorSetup()->remoteSync = rs->second == USS("1");
        }

        auto as = args.find(_i("key.allow_sync"));
        if (as != args.end()) {
            setup->getProcessorSetup()->allowSync = as->second == USS("1");
        }

        auto ad = args.find(_i("key.allow_debug"));
        if (ad != args.end()) {
            setup->getProcessorSetup()->allowDebug = ad->second == USS("1");
        }

        auto ads = args.find(_i("key.allow_distributed"));
        if (ads != args.end()) {
            setup->getProcessorSetup()->allowDistributed = ads->second == USS("1");
        }

        auto la = args.find(_i("key.address"));
        setup->getRemotingSetup()->listenAddress = la != args.end() ? la->second : USS("localhost");
        auto lp = args.find(_i("key.port"));
        setup->getRemotingSetup()->listenPort = lp != args.end() ? (int32_t)ByteCodeUtils::asNumber(lp->second) : 10555;
        auto ln = args.find(_i("key.pool"));
        setup->getRemotingSetup()->poolSize = ln != args.end() ? (int16_t)ByteCodeUtils::asNumber(ln->second) : 16;

        setup->getByteCodeParserSetup()->primitiveNeuron = NeuronNames::ADDMULT;
        setup->getByteCodeParserSetup()->additionNeuron = NeuronNames::ADDITIVE;
        setup->getByteCodeParserSetup()->multiplicationNeuron = NeuronNames::MULTIPLICATIVE;
        setup->getByteCodeParserSetup()->libraryNeuron = NeuronNames::BLOCKCALL;
        setup->getByteCodeParserSetup()->aliases[USS("#")] = NeuronNames::ADDITIVE;
        setup->getByteCodeParserSetup()->aliases[USS("&")] = NeuronNames::MULTIPLICATIVE;
        setup->getByteCodeParserSetup()->aliases[USS("$")] = NeuronNames::STRING;
        setup->getByteCodeParserSetup()->aliases[USS("+")] = NeuronNames::PLUS;
        setup->getByteCodeParserSetup()->aliases[USS("-")] = NeuronNames::MINUS;
        setup->getByteCodeParserSetup()->aliases[USS("*")] = NeuronNames::MULTIPLY;
        setup->getByteCodeParserSetup()->aliases[USS("~")] = NeuronNames::BRIDGE;
        setup->getByteCodeParserSetup()->arithmeticNeuronNames.insert(NeuronNames::PLUS);
        setup->getByteCodeParserSetup()->arithmeticNeuronNames.insert(NeuronNames::MINUS);
        setup->getByteCodeParserSetup()->arithmeticNeuronNames.insert(NeuronNames::MULTIPLY);
        
        //registerDefaults(setup);

        auto libLoader = std::make_shared<LibraryLoader>(setup);
        loader = loader ? loader : std::make_shared<ByteCodeLoader>(setup, libLoader);
        pointerPool.track(libLoader);
        pointerPool.track(loader);

        this->ctx = std::make_shared<Context>(setup, libLoader, loader);
        pointerPool.track(ctx);
        pointerPool.track(ctx->byteDumper);
        auto c = ctx;

        std::map<icu::UnicodeString, std::function<VMDumper(const icu::UnicodeString&)> > dumpKeys
                {
                        {_i("key.dump_bin"), [&c](const icu::UnicodeString &file) {
                            return VMDumper([&file, &c](std::shared_ptr<ByteCode> code, int64_t tick) {
                                auto fileName = applyName(file, tick);
                                int64_t err;
                                auto s = BinaryUtils::openWriteStream(fileName, err);
                                if (!s->is_open()) {
                                    throw ByteCodeException(StringUtils::spf(_i("err.dump.file.failed"),
                                                                             StringConvUtils::utow(fileName)));
                                }
                                return ByteCodeWriter(s, c).write(code);
                            });
                        }
                        },
                        {_i("key.dump_conjugate"), [&c](const icu::UnicodeString &file) {
                            return VMDumper([&file, &c](std::shared_ptr<ByteCode> code, int64_t tick) {
                                auto fileName = applyName(file, tick);
                                int64_t err;
                                auto s = BinaryUtils::openWriteTextStream(fileName, err);
                                if (!s->is_open()) {
                                    throw ByteCodeException(StringUtils::spf(_i("err.dump.file.failed"),
                                                                             StringConvUtils::utow(fileName)));
                                }
                                return ByteCodeWriter(s, c).writeConjugate(code);
                            });
                        }
                        },
                        {_i("key.dump_xml"), [&c](const icu::UnicodeString &file) {
                            return VMDumper([&file, &c](std::shared_ptr<ByteCode> code, int64_t tick) {
                                auto fileName = applyName(file, tick);
                                int64_t err;
                                auto s = BinaryUtils::openWriteTextStream(fileName, err);
                                if (!s->is_open()) {
                                    throw ByteCodeException(StringUtils::spf(_i("err.dump.file.failed"),
                                                                             StringConvUtils::utow(fileName)));
                                }
                                return ByteCodeWriter(s, c).writeXml(code);
                            });
                        }
                        },
                        {_i("key.dump_json"), [&c](const icu::UnicodeString &file) {
                            return VMDumper([&file, &c](std::shared_ptr<ByteCode> code, int64_t tick) {
                                auto fileName = applyName(file, tick);
                                int64_t err;
                                auto s = BinaryUtils::openWriteTextStream(fileName, err);
                                if (!s->is_open()) {
                                    throw ByteCodeException(StringUtils::spf(_i("err.dump.file.failed"),
                                                                             StringConvUtils::utow(fileName)));
                                }
                                return ByteCodeWriter(s, c).writeJson(code);
                            });
                        }
                        },
                        {_i("key.dump_source"), [&c](const icu::UnicodeString &file) {
                            return VMDumper([&file, &c](std::shared_ptr<ByteCode> code, int64_t tick) {
                                auto fileName = applyName(file, tick);
                                int64_t err;
                                auto s = BinaryUtils::openWriteTextStream(fileName, err);
                                if (!s->is_open()) {
                                    throw ByteCodeException(StringUtils::spf(_i("err.dump.file.failed"),
                                                                             StringConvUtils::utow(fileName)));
                                }
                                return ByteCodeWriter(s, c).writeSource(code);
                            });
                        }
                        }
                };
        for (auto &dumpKey : dumpKeys) {
            auto f = args.find(dumpKey.first);
            if (f != args.end()) {
                ctx->byteDumper->add(f->first, dumpKey.second(f->second));
            }
        }

        if (args.find(_i("key.compile_only")) != args.end()) {
            compiler = true;
        }
        else {
            this->processor = processor ? processor : std::make_shared<Processor>(setup, ctx);
            pointerPool.track(this->processor);
            ctx->processExternalLinks = [this](const std::map<icu::UnicodeString, std::map<icu::UnicodeString, std::tuple<double, double, double> > >& flow) {
                return this->processor->processExternalLinks(flow);
            };
        }
        auto ll = args.find(_i("key.log_level"));
        if (ll != args.end()) {
            setup->getLogger()->setLevel(Logger::parseLevel(ll->second));
        }
    }

    VM::~VM() = default;

    void VM::readNetwork()
    {
        if (!compiler) {
            if (ctx->setup->getProcessorSetup()->allowRemoteInit && mainPackage == USS("")) {
                //wait for package from network
                ctx->globalBlock();
            }
            else {
                ctx->executed = ctx->byteLoader->load(mainPackage, ctx);
            }
        }
        else {
            ctx->executed = ctx->byteLoader->load(mainPackage, ctx);
        }
        if (!ctx->executed) {
            throw ByteCodeException(_i("err.package.not.loaded"));
        }
        pointerPool.track(ctx->executed);
    }

    int8_t VM::runNetwork()
    {
        std::shared_ptr<ByteCode> prev;
        ctx->setup->getProcessorSetup()->running = 1;
        ctx->setup->getLogger()->info(USS("All blocks prepared. Starting processor."));
        while (ctx->setup->getProcessorSetup()->running) {
            // network configuration changed
            if (prev != ctx->executed) {
                prev = ctx->executed;
                std::vector<std::shared_ptr<ByteCode>> vc{ ctx->executed };
                auto blocks = ctx->byteLoader->mergeByteCodes(vc);
                ctx->setup->getProcessorSetup()->preSetup(blocks);
            }

            ctx->setup->getProcessorSetup()->tick++;
            processor->processStep();
            ctx->byteDumper->dump(ctx->executed, ctx->setup->getProcessorSetup()->tick);

            // lock if in debug or synced distributed network
            if (ctx->setup->getProcessorSetup()->remoteSync) {
                ctx->globalBlock();
            }
        }
        auto res = ctx->setup->getProcessorSetup()->exitCode;
        processor->terminate();
        return res;
    }

    int8_t VM::execute()
    {
        int8_t res = 0;
        if (!compiler) {
            auto proto = std::make_shared<grid::NNProtocol>(ctx);
            server = std::make_shared<grid::Server>(ctx, proto);
            pointerPool.track(server);
            pointerPool.track(proto);
        }
        readNetwork();
        if (!compiler) {
            res = runNetwork();
            server->stop();
        }
        else {
            ctx->byteDumper->dump(ctx->executed, -1);
        }

        return res;
    }
}
