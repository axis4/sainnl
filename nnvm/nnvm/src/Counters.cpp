/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#include <Counters.hpp>
#include <cmath>

namespace nnvm {
    const int64_t MAX_SAMPLE = 10000;
    const int64_t MAX_STORED_SAMPLE = 100;

    SampleSetting::SampleSetting(double quantile, double error) : quantile(quantile), error(error) {}

    Sample::Sample(double value, int64_t lowDiff, int64_t highDiff, int64_t accumulated)
    : value(value), lowDiff(lowDiff), highDiff(highDiff), accumulated(accumulated) {}

    Quantile::Quantile(double quantile, double value) : quantile(quantile), value(value) {}

    Counter::Counter(std::vector<SampleSetting> sampling)
    : min(0), max(0), avg(0), sum(0), sampleCount(0), sampling(sampling) {}

    Counter::Counter(const Counter& obj)
    : min(obj.min), max(obj.max), avg(obj.avg), sum(obj.sum), sampleCount(obj.sampleCount), sampling(obj.sampling), samples(obj.samples) {}

    void Counter::incCounter(double sample)
    {
        // quantiles
        // n = #items, k = asymptote
        // S = data structure, s = #samples
        int64_t r = 0;
        auto i = this->samples.begin();
        for (auto end = this->samples.end(); i != end && i->value >= sample; ++i) {
            i->accumulated = r;
            r += i->lowDiff;
        }
        this->samples.insert(i, Sample(sample, 1, (int64_t)std::floor(this->quantileGreatest(r)) - 1, r));
        
        // compress samples
        if (this->samples.size() > MAX_STORED_SAMPLE) {
            auto ni = this->samples.rbegin();
            auto l = this->samples.rbegin();
            ++ni;
            for (auto end = this->samples.rend(); ni != end; ++l, ++ni) {
                if (ni->lowDiff + l->lowDiff + l->highDiff <= this->quantileGreatest(ni->accumulated)) {
                    ni->value = l->value;
                    ni->lowDiff += l->lowDiff;
                    ni->highDiff = l->highDiff;
                    this->samples.erase(std::next(l).base());
                }
            }
        }
        
        if (this->min > sample) {
            this->min = sample;
        }
        if (this->max < sample) {
            this->max = sample;
        }
        this->sum += sample;
        this->sampleCount++;
        this->avg = this->sum / this->sampleCount;
        
        // compress on overflow
        if (this->sampleCount > MAX_SAMPLE) {
            this->sum = this->avg;
            this->sampleCount = 1;
        }
    }

    std::vector<Quantile> Counter::quantiles()
    {
        std::vector<Quantile> res;
        auto end = this->sampling.end();
        for (auto i = this->sampling.begin(); i != end; ++i) {
            res.push_back(this->quantile(*i));
        }
        return res;
    }

    Quantile Counter::quantile(SampleSetting& setting)
    {
        int64_t r = 0;
        auto ni = this->samples.begin();
        auto i = this->samples.begin();
        ++ni;
        auto b = setting.quantile * this->samples.size() + this->quantileGreatest(
                (int64_t) (setting.quantile * this->samples.size())) / 2;
        for (auto end = this->samples.end(); ni != end; ++ni, ++i) {
            r += i->lowDiff;
            if (r + ni->lowDiff + ni->highDiff > b) {
                return Quantile(setting.quantile, i->value);
            }
        }
        return Quantile(setting.quantile, 0);
    }

    double Counter::quantileGreatest(int64_t accumulated)
    {
        std::vector<double> res;
        for (auto i = this->sampling.begin(); i != this->sampling.end(); ++i) {
            if (accumulated < i->quantile * this->samples.size()) {
                res.push_back(2 * accumulated * i->error / i->quantile);
            }
            else {
                res.push_back(2 * accumulated * (1.0 - i->error) / (1.0 - i->quantile));
            }
        }
        return *std::max_element(res.begin(), res.end());
    }

    Counters::Counters(std::vector<SampleSetting> sampling) : step(sampling), templatePhase(sampling), addedNodes(sampling), removedNodes(sampling), linkPhase(sampling), nodePhase(sampling), eachNode(sampling) {}
    Counters::Counters(std::vector<SampleSetting> stepSampling, std::vector<SampleSetting> templatePhaseSampling, std::vector<SampleSetting> addedNodesSampling, std::vector<SampleSetting> removedNodesSampling, std::vector<SampleSetting> linkPhaseSampling, std::vector<SampleSetting> nodePhaseSampling, std::vector<SampleSetting> eachNodeSampling, std::vector<SampleSetting> neuronSampling) : step(stepSampling), templatePhase(templatePhaseSampling), addedNodes(addedNodesSampling), removedNodes(removedNodesSampling), linkPhase(linkPhaseSampling), nodePhase(nodePhaseSampling), eachNode(eachNodeSampling) {}

    Counters::Counters(const Counters& obj) : step(obj.step), templatePhase(obj.templatePhase), addedNodes(obj.addedNodes), removedNodes(obj.removedNodes), linkPhase(obj.linkPhase), nodePhase(obj.nodePhase), eachNode(obj.eachNode), nodes(obj.nodes) {}
}
