/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#include <ByteCode.hpp>
#include <StringConv.hpp>
#include <utility>

namespace nnvm {
    ByteCode::ByteCode(const icu::UnicodeString& packageName, std::shared_ptr<ByteCodeLoader> loader)
            : packageName(packageName), loader(std::move(loader)), blocks(std::deque<std::shared_ptr<ByteCodeBlock>>())
    {
        safe();
    }

    ByteCode::ByteCode(ByteCode& obj)
            : packageName(obj.packageName), loader(obj.loader), blocks(std::deque<std::shared_ptr<ByteCodeBlock>>(obj.blocks))
    {
        safe();
        obj.safe();
    }

    ByteCode::~ByteCode()
    {
        safe();
        dispose();
    }

    void ByteCode::dispose()
    {
        safe();
        blocks.clear();
    }

    void ByteCode::addBlock(std::shared_ptr<ByteCodeBlock> block)
    {
        safe();
        blocks.push_back(block);
    }

    std::tuple<std::deque<std::shared_ptr<ByteCodeBlock> >::iterator, std::deque<std::shared_ptr<ByteCodeBlock> >::iterator> ByteCode::getBlocks()
    {
        safe();
        return { blocks.begin(), blocks.end() };
    }

    std::deque<std::shared_ptr<ByteCodeBlock>>::iterator ByteCode::getBlock(int64_t index)
    {
        safe();
        auto res = std::get<0>(getBlocks());
        std::advance(res, index);
        return res;
    }

    std::deque<std::shared_ptr<ByteCodeBlock>>::iterator ByteCode::getBlock(icu::UnicodeString name)
    {
        safe();
        auto range = getBlocks();
        auto res = std::get<0>(range);
        auto e = std::get<1>(range);
        for (; res != e && (*res)->getName() != name; ++res);
        return res;
    }

    std::shared_ptr<ByteCodeBlock> ByteCode::getLastBlock()
    {
        safe();
        return blocks.back();
    }

    icu::UnicodeString& ByteCode::getName()
    {
        return packageName;
    }

    void ByteCode::addPackagePath(icu::UnicodeString& path) {
        packageName = path + USS(".") + packageName;
    }
}
