/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#include <ByteCodeBlock.hpp>
#include <Internationalization.hpp>
#include <ByteCodeException.hpp>
#include <ByteCodeUtils.hpp>
#include <cmath>
#include <ProcessorKnownParameters.hpp>
#include "../utils/Utils.hpp"

namespace nnvm {
    std::vector<std::shared_ptr<ByteCodeBlock>> ByteCodeBlock::fullCopy(const std::vector<std::shared_ptr<ByteCodeBlock>>& src)
    {
        std::vector<std::shared_ptr<ByteCodeBlock>> res;
        std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock>> bn;
        for (const auto &b : src) {
            auto n = std::make_shared<ByteCodeBlock>(b->getName(), b->getType());
            auto params = b->getParameters();
            auto nparams = fullCopy(std::get<0>(params), std::get<1>(params));
            for (auto &nparam : nparams) {
                n->setParameter(nparam);
            }
            auto items = b->getItems();
            auto incomes = b->getIncomes();
            auto nitems = fullCopy(std::get<0>(items), std::get<1>(items));
            // set all items and fix owner to current
            for (auto &nitem : nitems) {
                nitem->changeOwner(n);
                n->addItem(nitem);
            }
            auto nincomes = fullCopy(std::get<0>(incomes), std::get<1>(incomes));
            // set all incomes
            for (auto &nincome : nincomes) {
                n->addIncome(nincome);
            }
            res.push_back(n);
            bn[n->getName()] = n;
        }
        
        // set items as incomes
        for (auto &re : res) {
            auto items = re->getItems();
            for (auto i = std::get<0>(items); i != std::get<1>(items); ++i) {
                auto to_check = bn[(*i)->getName()]->getIncomes();
                auto k = std::get<0>(to_check);
                for (; k != std::get<1>(to_check); ++k) {
                    if ((*k)->getOwner().lock()->getName() == (*i)->getOwner().lock()->getName()) {
                        break;
                    }
                }
                // set item as income if not already set
                if (k == std::get<1>(to_check)) {
                    bn[(*i)->getName()]->addIncome(*i);
                }
            }
        }
        
        // set incomes as items
        for (auto &re : res) {
            auto incomes = re->getIncomes();
            for (auto i = std::get<0>(incomes); i != std::get<1>(incomes); ++i) {
                (*i)->changeOwner(bn[(*i)->getOwner().lock()->getName()]);
                auto to_check = (*i)->getOwner().lock()->getItems();
                auto k = std::get<0>(to_check);
                for (; k != std::get<1>(to_check); ++k) {
                    if ((*k)->getName() == (*i)->getName()) {
                        break;
                    }
                }
                // set income as item if not already set
                if (k == std::get<1>(to_check)) {
                    (*i)->getOwner().lock()->addItem(*i);
                }
            }
        }
        
        return res;
    }

    std::deque<std::shared_ptr<ByteCodeBlockItem>> ByteCodeBlock::fullCopy(const std::deque<std::shared_ptr<ByteCodeBlockItem>>& src)
    {
        return fullCopy(src.begin(), src.end());
    }

    std::deque<std::shared_ptr<ByteCodeBlockItem>> ByteCodeBlock::fullCopy(const std::deque<std::shared_ptr<ByteCodeBlockItem>>::const_iterator start, const std::deque<std::shared_ptr<ByteCodeBlockItem>>::const_iterator end)
    {
        std::deque<std::shared_ptr<ByteCodeBlockItem>> res;
        for (auto i = start; i != end; ++i) {
            if (!(*i)->getOwner().lock()) {
                continue;
            }
            auto n = std::make_shared<ByteCodeBlockItem>((*i)->getName(), (*i)->getOwner());
            auto params = (*i)->getParameters();
            auto nparams = fullCopy(std::get<0>(params), std::get<1>(params));
            for (auto &nparam : nparams) {
                n->setParameter(nparam);
            }
            res.push_back(n);
        }
        return res;
    }

    std::deque<std::shared_ptr<ByteCodeParameter>> ByteCodeBlock::fullCopy(const std::deque<std::shared_ptr<ByteCodeParameter>>& src)
    {
        return fullCopy(src.begin(), src.end());
    }

    std::deque<std::shared_ptr<ByteCodeParameter>> ByteCodeBlock::fullCopy(const std::deque<std::shared_ptr<ByteCodeParameter>>::const_iterator start, const std::deque<std::shared_ptr<ByteCodeParameter>>::const_iterator end)
    {
        std::deque<std::shared_ptr<ByteCodeParameter>> res;
        for (auto p = start; p != end; ++p) {
            switch ((*p)->getType()) {
                case ByteCodeParameterTypes::BYTE:
                    res.push_back(std::make_shared<ByteCodeParameter>((*p)->getName(), (*p)->value.byteValue));
                    break;
                    
                case ByteCodeParameterTypes::INTEGER:
                    res.push_back(std::make_shared<ByteCodeParameter>((*p)->getName(), (*p)->value.integerValue));
                    break;
                    
                case ByteCodeParameterTypes::DOUBLE:
                    res.push_back(std::make_shared<ByteCodeParameter>((*p)->getName(), (*p)->value.doubleValue));
                    break;
                    
                case ByteCodeParameterTypes::STRING:
                    res.push_back(std::make_shared<ByteCodeParameter>((*p)->getName(), (*p)->value.stringValue));
                    break;
                    
                case ByteCodeParameterTypes::_INTERNAL_OBJECT:
                    res.push_back(std::make_shared<ByteCodeParameter>((*p)->getName(), (*p)->value.objectValue));
                    break;
            }
        }
        return res;
    }

    ByteCodeBlock::ByteCodeBlock(const icu::UnicodeString& blockName, const icu::UnicodeString& type)
            : name(blockName), type(type), items(std::deque<std::shared_ptr<ByteCodeBlockItem>>()),
              parameters(std::deque<std::shared_ptr<ByteCodeParameter>>()),
              parametersIndex(std::map<icu::UnicodeString, int64_t>()),
              incomes(std::deque<std::shared_ptr<ByteCodeBlockItem>>()),
              fastBias(std::shared_ptr<ByteCodeParameter>(nullptr)),
              fastExpected(std::shared_ptr<ByteCodeParameter>(nullptr)),
              fastIndex(std::shared_ptr<ByteCodeParameter>(nullptr)),
              fastInitial(std::shared_ptr<ByteCodeParameter>(nullptr)),
              fastInput(std::shared_ptr<ByteCodeParameter>(nullptr)),
              fastOutput(std::shared_ptr<ByteCodeParameter>(nullptr)),
              fastSum(std::shared_ptr<ByteCodeParameter>(nullptr)),
              fastTemplate(std::shared_ptr<ByteCodeParameter>(nullptr)),
              fastThreshold(std::shared_ptr<ByteCodeParameter>(nullptr))
    {
        safe();
    }

    ByteCodeBlock::ByteCodeBlock(ByteCodeBlock& obj)
            : name(obj.name), type(obj.type), items(std::deque<std::shared_ptr<ByteCodeBlockItem>>(obj.items)),
              parameters(std::deque<std::shared_ptr<ByteCodeParameter>>(obj.parameters)),
              parametersIndex(std::map<icu::UnicodeString, int64_t>(obj.parametersIndex)),
              incomes(std::deque<std::shared_ptr<ByteCodeBlockItem>>(obj.incomes)),
              fastBias(obj.fastBias),
              fastExpected(obj.fastExpected),
              fastIndex(obj.fastIndex),
              fastInitial(obj.fastInitial),
              fastInput(obj.fastInput),
              fastOutput(obj.fastOutput),
              fastSum(obj.fastSum),
              fastTemplate(obj.fastTemplate),
              fastThreshold(obj.fastThreshold)
    {
        safe();
        obj.safe();
    }

    ByteCodeBlock::~ByteCodeBlock()
    {
        safe();
        incomes.clear();
        items.clear();
        parametersIndex.clear();
        parameters.clear();
    }

    std::shared_ptr<ByteCodeBlock> ByteCodeBlock::isolatedCopy()
    {
        return isolatedCopy(name);
    }

    std::shared_ptr<ByteCodeBlock> ByteCodeBlock::isolatedCopy(const icu::UnicodeString& rename)
    {
        safe();
        auto copy = std::make_shared<ByteCodeBlock>(rename, type);
        for (auto &parameter : parameters) {
            copy->setParameter(std::make_shared<ByteCodeParameter>(*parameter));
        }
        copy->fastBias = fastBias;
        copy->fastExpected = fastExpected;
        copy->fastIndex = fastIndex;
        copy->fastInitial = fastInitial;
        copy->fastInput = fastInput;
        copy->fastOutput = fastOutput;
        copy->fastSum = fastSum;
        copy->fastTemplate = fastTemplate;
        copy->fastThreshold = fastThreshold;
        return copy;
    }

    void ByteCodeBlock::dispose()
    {
        safe();
        parameters.clear();
        items.clear();
        incomes.clear();
    }

    void ByteCodeBlock::addItem(std::shared_ptr<ByteCodeBlockItem> item)
    {
        safe();
        if (!item->getOwner().lock()) {
            throw ByteCodeException(StringUtils::spf(_i("err.bytecode.blockitem.owner.null"), StringConvUtils::utow(item->getName())));
        }
        if (item->getOwner().lock().get() != this) {
            throw ByteCodeException(StringUtils::spf(_i("err.bytecode.blockitem.owner.null"), StringConvUtils::utow(item->getName())));
        }
        items.push_back(item);
    }

    void ByteCodeBlock::eraseItem(std::deque<std::shared_ptr<ByteCodeBlockItem>>::iterator item)
    {
        safe();
        items.erase(item);
    }

    void ByteCodeBlock::addIncome(std::shared_ptr<ByteCodeBlockItem> item)
    {
        safe();
        incomes.push_back(item);
    }

    void ByteCodeBlock::eraseIncome(std::deque<std::shared_ptr<ByteCodeBlockItem>>::iterator item)
    {
        safe();
        incomes.erase(item);
    }

    std::tuple<std::deque<std::shared_ptr<ByteCodeBlockItem>>::iterator, std::deque<std::shared_ptr<ByteCodeBlockItem>>::iterator> ByteCodeBlock::getItems()
    {
        safe();
        return { items.begin(), items.end() };
    }

    int64_t ByteCodeBlock::getItemsCount()
    {
        safe();
        return static_cast<int64_t>(items.size());
    }

    std::deque<std::shared_ptr<ByteCodeBlockItem>>::iterator ByteCodeBlock::getItem(int64_t index)
    {
        safe();
        auto res = std::get<0>(getItems());
        std::advance(res, index);
        return res;
    }

    std::shared_ptr<ByteCodeBlockItem> ByteCodeBlock::getLastItem()
    {
        safe();
        return items.back();
    }

    std::tuple<std::deque<std::shared_ptr<ByteCodeBlockItem>>::iterator, std::deque<std::shared_ptr<ByteCodeBlockItem>>::iterator> ByteCodeBlock::getIncomes()
    {
        safe();
        return { incomes.begin(), incomes.end() };
    }

    int64_t ByteCodeBlock::getIncomesCount()
    {
        safe();
        return static_cast<int64_t>(incomes.size());
    }

    std::deque<std::shared_ptr<ByteCodeBlockItem>>::iterator ByteCodeBlock::getIncome(int64_t index)
    {
        safe();
        auto res = std::get<0>(getIncomes());
        std::advance(res, index);
        return res;
    }

    std::shared_ptr<ByteCodeBlockItem> ByteCodeBlock::getLastIncome()
    {
        safe();
        return incomes.back();
    }

    std::tuple<std::deque<std::shared_ptr<ByteCodeParameter>>::iterator, std::deque<std::shared_ptr<ByteCodeParameter>>::iterator> ByteCodeBlock::getParameters()
    {
        safe();
        return { parameters.begin(), parameters.end() };
    }

    std::shared_ptr<ByteCodeParameter> ByteCodeBlock::getParameter(const icu::UnicodeString& name)
    {
        safe();
        auto res = parametersIndex.find(name);
        if (res == parametersIndex.end())
        {
            std::vector<icu::UnicodeString> keys;
            for (auto &parameter : parameters) {
                keys.push_back(parameter->getName());
            }
            throw ByteCodeException(StringUtils::spf(_i("err.bytecode.block.parameter.missing"), StringConvUtils::utow(name), StringConvUtils::utow(StringUtils::joinArray(keys, USS(", ")))));
        }
        return parameters[res->second];
    }

    int8_t ByteCodeBlock::tryGetParameter(const icu::UnicodeString& name, std::shared_ptr<ByteCodeParameter>& result)
    {
        safe();
        auto res = parametersIndex.find(name);
        if (res == parametersIndex.end())
        {
            return 0;
        }
        result = parameters[res->second];
        return 1;
    }

    void ByteCodeBlock::setParameter(std::shared_ptr<ByteCodeParameter> parameter)
    {
        safe();
        auto s = parameter->getName();
        auto fp = parametersIndex.find(s);
        if (fp != parametersIndex.end()) {
            parameters[fp->second] = parameter;
        }
        else {
            auto p = parameters.size();
            parameters.push_back(parameter);
            parametersIndex[s] = (int64_t) p;
        }

        // fast update
        if (s == ElementParams::BIAS) {
            fastBias = parameter;
        }
        if (s == ElementParams::EXPECTED) {
            fastExpected = parameter;
        }
        if (s == ElementParams::INDEX) {
            fastIndex = parameter;
        }
        if (s == ElementParams::INITIAL_STATE) {
            fastInitial = parameter;
        }
        if (s == ElementParams::INPUT) {
            fastInput = parameter;
        }
        if (s == ElementParams::OUTPUT) {
            fastOutput = parameter;
        }
        if (s == ElementParams::SUM) {
            fastSum = parameter;
        }
        if (s == ElementParams::TEMPLATE) {
            fastTemplate = parameter;
        }
        if (s == ElementParams::THRESHOLD) {
            fastThreshold = parameter;
        }
    }

    std::shared_ptr<ByteCodeParameter> ByteCodeBlock::getFastThreshold()
    {
        return fastThreshold;
    }

    std::shared_ptr<ByteCodeParameter> ByteCodeBlock::getFastBias()
    {
        return fastBias;
    }

    std::shared_ptr<ByteCodeParameter> ByteCodeBlock::getFastInitial()
    {
        return fastInitial;
    }

    std::shared_ptr<ByteCodeParameter> ByteCodeBlock::getFastInput()
    {
        return fastInput;
    }

    std::shared_ptr<ByteCodeParameter> ByteCodeBlock::getFastOutput()
    {
        return fastOutput;
    }

    std::shared_ptr<ByteCodeParameter> ByteCodeBlock::getFastTemplate()
    {
        return fastTemplate;
    }

    std::shared_ptr<ByteCodeParameter> ByteCodeBlock::getFastExpected()
    {
        return fastExpected;
    }

    std::shared_ptr<ByteCodeParameter> ByteCodeBlock::getFastSum()
    {
        return fastSum;
    }

    std::shared_ptr<ByteCodeParameter> ByteCodeBlock::getFastIndex()
    {
        return fastIndex;
    }

    const icu::UnicodeString& ByteCodeBlock::getName()
    {
        safe();
        return name;
    }

    const icu::UnicodeString& ByteCodeBlock::getType()
    {
        safe();
        return type;
    }

    void ByteCodeBlock::changeType(icu::UnicodeString type)
    {
        safe();
        this->type = type;
    }

    void ByteCodeBlock::getParams(const std::vector<icu::UnicodeString>& names, std::map<icu::UnicodeString, std::shared_ptr<ByteCodeParameter>>& result)
    {
        for (const auto &name : names) {
            std::shared_ptr<ByteCodeParameter> p = std::shared_ptr<ByteCodeParameter>(nullptr);
            if (!this->tryGetParameter(name, p)) {
                p = std::make_shared<ByteCodeParameter>(name, (double)0);
                this->setParameter(p);
            }

            result[name] = p;
        }
    }

    void ByteCodeBlock::compact()
    {
        safe();
        parameters.erase(std::remove_if(parameters.begin(), parameters.end(), [](std::shared_ptr<ByteCodeParameter> p) {
            return (p->getType() == ByteCodeParameterTypes::BYTE || p->getType() == ByteCodeParameterTypes::INTEGER || p->getType() == ByteCodeParameterTypes::DOUBLE) && fabs(ByteCodeUtils::asDouble(*p)) < 1e-8;
        }), parameters.end());
        parametersIndex.clear();
        auto n = 0;
        for (auto &item : parameters) {
            parametersIndex[item->getName()] = n++;
        }
        for (auto &item : items) {
            item->compact();
        }
    }
}
