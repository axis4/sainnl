/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#include <ByteCodeParameter.hpp>
#include <iostream>

namespace nnvm {
    ByteCodeParameterValue::ByteCodeParameterValue(int8_t value) : byteValue(value)
    {
        safe();
    }

    ByteCodeParameterValue::ByteCodeParameterValue(int64_t value) : integerValue(value)
    {
        safe();
    }

    ByteCodeParameterValue::ByteCodeParameterValue(double value) : doubleValue(value)
    {
        safe();
    }

    ByteCodeParameterValue::ByteCodeParameterValue(const icu::UnicodeString& value) : stringValue(value)
    {
        safe();
    }

    ByteCodeParameterValue::ByteCodeParameterValue(std::shared_ptr<void> value) : objectValue(value)
    {
        safe();
    }

    ByteCodeParameterValue::ByteCodeParameterValue(ByteCodeParameterValue& obj)
            : byteValue(obj.byteValue), doubleValue(obj.doubleValue), integerValue(obj.integerValue),
              objectValue(obj.objectValue), stringValue(obj.stringValue)
    {
        safe();
    }

    ByteCodeParameter::ByteCodeParameter(const icu::UnicodeString& parameterName, int8_t value)
            : name(parameterName), type(ByteCodeParameterTypes::BYTE), value(value)
    {
        safe();
    }

    ByteCodeParameter::ByteCodeParameter(const icu::UnicodeString& parameterName, int64_t value)
            : name(parameterName), type(ByteCodeParameterTypes::INTEGER), value(value)
    {
        safe();
    }

    ByteCodeParameter::ByteCodeParameter(const icu::UnicodeString& parameterName, double value)
            : name(parameterName), type(ByteCodeParameterTypes::DOUBLE), value(value)
    {
        safe();
    }

    ByteCodeParameter::ByteCodeParameter(const icu::UnicodeString& parameterName, const icu::UnicodeString& value)
            : name(parameterName), type(ByteCodeParameterTypes::STRING), value(value)
    {
        safe();
    }

    ByteCodeParameter::ByteCodeParameter(const icu::UnicodeString& parameterName, std::shared_ptr<void> value)
            : name(parameterName), type(ByteCodeParameterTypes::_INTERNAL_OBJECT), value(value)
    {
        safe();
    }

    ByteCodeParameter::ByteCodeParameter(ByteCodeParameter& obj)
            : name(obj.name), type(obj.type), value(obj.value)
    {
        safe();
        obj.safe();
    }

    ByteCodeParameter::~ByteCodeParameter()
    {
        safe();
        value.objectValue = nullptr;
    }

    const icu::UnicodeString& ByteCodeParameter::getName() const
    {
        safe();
        return name;
    }

    ByteCodeParameterTypes ByteCodeParameter::getType() const
    {
        safe();
        return type;
    }
}
