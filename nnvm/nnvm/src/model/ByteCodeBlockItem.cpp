/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#include <ByteCodeBlockItem.hpp>
#include <Internationalization.hpp>
#include <ByteCodeException.hpp>
#include <ByteCodeUtils.hpp>
#include <cmath>
#include <ProcessorKnownParameters.hpp>
#include "../utils/Utils.hpp"

namespace nnvm {
    ByteCodeBlockItem::ByteCodeBlockItem(const icu::UnicodeString& itemName, std::weak_ptr<ByteCodeBlock> owner)
            : name(itemName), owner(owner),
              fastActivation(std::shared_ptr<ByteCodeParameter>(nullptr)),
              fastStream(std::shared_ptr<ByteCodeParameter>(nullptr)),
              fastGeneration(std::shared_ptr<ByteCodeParameter>(nullptr)),
              parameters(std::deque<std::shared_ptr<ByteCodeParameter>>()),
              parametersIndex(std::map<icu::UnicodeString, int64_t>())
    {
        safe();
        if (!owner.lock()) {
            throw ByteCodeException(StringUtils::spf(_i("err.bytecode.blockitem.owner.null"), StringConvUtils::utow(name)));
        }
    }

    ByteCodeBlockItem::ByteCodeBlockItem(ByteCodeBlockItem& obj)
            : name(obj.name), owner(obj.owner),
              fastActivation(obj.fastActivation), fastStream(obj.fastStream), fastGeneration(obj.fastGeneration),
              parameters(std::deque<std::shared_ptr<ByteCodeParameter>>(obj.parameters)),
              parametersIndex(std::map<icu::UnicodeString, int64_t>(obj.parametersIndex))
    {
        safe();
        obj.safe();
        if (!owner.lock()) {
            throw ByteCodeException(StringUtils::spf(_i("err.bytecode.blockitem.owner.null"), StringConvUtils::utow(name)));
        }
    }

    ByteCodeBlockItem::~ByteCodeBlockItem()
    {
        safe();
        owner = std::weak_ptr<ByteCodeBlock>();
        parametersIndex.clear();
        parameters.clear();
    }

    std::shared_ptr<ByteCodeBlockItem> ByteCodeBlockItem::isolatedCopy()
    {
        return isolatedCopy(name);
    }

    std::shared_ptr<ByteCodeBlockItem> ByteCodeBlockItem::isolatedCopy(const icu::UnicodeString& rename)
    {
        safe();
        auto copy = std::make_shared<ByteCodeBlockItem>(rename, owner);
        for (auto i = parameters.begin(); i != parameters.end(); ++i) {
            copy->setParameter(std::make_shared<ByteCodeParameter>(**i));
        }
        return copy;
    }

    std::tuple<std::deque<std::shared_ptr<ByteCodeParameter>>::iterator, std::deque<std::shared_ptr<ByteCodeParameter>>::iterator> ByteCodeBlockItem::getParameters()
    {
        safe();
        return std::tuple<std::deque<std::shared_ptr<ByteCodeParameter>>::iterator, std::deque<std::shared_ptr<ByteCodeParameter>>::iterator>(parameters.begin(), parameters.end());
    }

    std::shared_ptr<ByteCodeParameter> ByteCodeBlockItem::getParameter(const icu::UnicodeString& name)
    {
        safe();
        auto res = parametersIndex.find(name);
        if (res == parametersIndex.end())
        {
            throw ByteCodeException(StringUtils::spf(_i("err.bytecode.blockitem.parameter.missing"), StringConvUtils::utow(name)));
        }
        return parameters[res->second];
    }

    int8_t ByteCodeBlockItem::tryGetParameter(const icu::UnicodeString& name, std::shared_ptr<ByteCodeParameter>& result)
    {
        safe();
        auto res = parametersIndex.find(name);
        if (res == parametersIndex.end())
        {
            return 0;
        }
        result = parameters[res->second];
        return 1;
    }

    void ByteCodeBlockItem::setParameter(std::shared_ptr<ByteCodeParameter> parameter)
    {
        safe();
        auto s = parameter->getName();
        auto fp = parametersIndex.find(s);
        if (fp != parametersIndex.end()) {
            parameters[fp->second] = parameter;
        }
        else {
            auto p = parameters.size();
            parameters.push_back(parameter);
            parametersIndex[s] = (int64_t) p;
        }

        // fast update
        if (s == LinkParams::ACTIVATION) {
            fastActivation = parameter;
        }
        if (s == LinkParams::STREAM) {
            fastStream = parameter;
        }
        if (s == LinkParams::GENERATION) {
            fastGeneration = parameter;
        }
    }

    std::shared_ptr<ByteCodeParameter> ByteCodeBlockItem::getFastActivation()
    {
        return fastActivation;
    }

    std::shared_ptr<ByteCodeParameter> ByteCodeBlockItem::getFastStream()
    {
        return fastStream;
    }

    std::shared_ptr<ByteCodeParameter> ByteCodeBlockItem::getFastGeneration()
    {
        return fastGeneration;
    }

    icu::UnicodeString& ByteCodeBlockItem::getName()
    {
        safe();
        return name;
    }

    std::weak_ptr<ByteCodeBlock> ByteCodeBlockItem::getOwner()
    {
        safe();
        return owner;
    }

    void ByteCodeBlockItem::changeOwner(std::weak_ptr<ByteCodeBlock> owner)
    {
        safe();
        if (!owner.lock()) {
            throw ByteCodeException(StringUtils::spf(_i("err.bytecode.blockitem.owner.null"), StringConvUtils::utow(name)));
        }
        this->owner = owner;
    }

    void ByteCodeBlockItem::compact()
    {
        safe();
        parameters.erase(std::remove_if(parameters.begin(), parameters.end(), [](std::shared_ptr<ByteCodeParameter> p) {
            return (p->getType() == ByteCodeParameterTypes::BYTE || p->getType() == ByteCodeParameterTypes::INTEGER || p->getType() == ByteCodeParameterTypes::DOUBLE) && fabs(ByteCodeUtils::asDouble(*p)) < 1e-8;
        }), parameters.end());
        parametersIndex.clear();
        auto n = 0;
        for (auto i = parameters.begin(); i != parameters.end(); ++i, ++n) {
            parametersIndex[(*i)->getName()] = n;
        }
    }
}
