/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#include <DefaultNeurons.hpp>
#include <list>
#include <vector>
#include <ostream>
#include <iostream>
#include <ByteCodeParameter.hpp>
#include <ByteCodeException.hpp>
#include <Internationalization.hpp>
#include "ByteCodeLoader.hpp"
#include "parser/DefinitionBuilder.hpp"
#include "writer/ByteCodeWriter.hpp"

namespace nnvm {
    icu::UnicodeString NeuronNames::IN = USS("in");
    icu::UnicodeString NeuronNames::INPOS = USS("inpos");
    icu::UnicodeString NeuronNames::OUT = USS("out");
    icu::UnicodeString NeuronNames::ADDMULT = USS("addmult");
    icu::UnicodeString NeuronNames::BLOCKCALL = USS("blockcall");
    icu::UnicodeString NeuronNames::BRIDGE = USS("bridge");
    icu::UnicodeString NeuronNames::STRING = USS("string");
    icu::UnicodeString NeuronNames::ADDITIVE = USS("additive");
    icu::UnicodeString NeuronNames::MULTIPLICATIVE = USS("multiplicative");
    icu::UnicodeString NeuronNames::EXIT = USS("exit");
    icu::UnicodeString NeuronNames::TICK = USS("tick");
    icu::UnicodeString NeuronNames::TIME = USS("time");
    icu::UnicodeString NeuronNames::STDIN = USS("stdin");
    icu::UnicodeString NeuronNames::STDOUT = USS("stdout");
    icu::UnicodeString NeuronNames::PLUS = USS("plus");
    icu::UnicodeString NeuronNames::MINUS = USS("minus");
    icu::UnicodeString NeuronNames::MULTIPLY = USS("multiply");


    void registerDefaults(std::shared_ptr<nnvm::MachineSetup> setup) {
        /*
        auto in = std::make_shared<InPlugin>(setup);
        auto ip = std::make_shared<InPosPlugin>(setup);
        auto ot = std::make_shared<OutPlugin>(setup);
        auto am = std::make_shared<AddMultPlugin>(setup);
        auto bc = std::make_shared<BlockCallPlugin>(setup);
        auto st = std::make_shared<StringPlugin>(setup);
        auto ad = std::make_shared<AdditivePlugin>(setup);
        auto mu = std::make_shared<MultiplicativePlugin>(setup);
        auto ex = std::make_shared<ExitPlugin>(setup);
        auto tc = std::make_shared<TickPlugin>(setup);
        auto tm = std::make_shared<TimePlugin>(setup);
        auto si = std::make_shared<StdInPlugin>(setup);
        auto so = std::make_shared<StdOutPlugin>(setup);

        setup->getProcessorSetup()->addPlugin(NeuronNames::ADDITIVE, ad);
        setup->getProcessorSetup()->addPlugin(NeuronNames::MULTIPLICATIVE, mu);
        setup->getProcessorSetup()->addPlugin(NeuronNames::EXIT, ex);
        setup->getProcessorSetup()->addPlugin(NeuronNames::TICK, tc);
        setup->getProcessorSetup()->addPlugin(NeuronNames::TIME, tm);
        setup->getProcessorSetup()->addPlugin(NeuronNames::STDIN, si);
        setup->getProcessorSetup()->addPlugin(NeuronNames::STDOUT, so);

        setup->getByteCodeWriterSetup()->addPlugin(NeuronNames::ADDITIVE, ad);
        setup->getByteCodeWriterSetup()->addPlugin(NeuronNames::MULTIPLICATIVE, mu);
        setup->getByteCodeWriterSetup()->addPlugin(NeuronNames::EXIT, ex);
        setup->getByteCodeWriterSetup()->addPlugin(NeuronNames::TICK, tc);
        setup->getByteCodeWriterSetup()->addPlugin(NeuronNames::TIME, tm);
        setup->getByteCodeWriterSetup()->addPlugin(NeuronNames::STDIN, si);
        setup->getByteCodeWriterSetup()->addPlugin(NeuronNames::STDOUT, so);

        setup->getByteCodeParserSetup()->neurons[NeuronNames::IN] = in;
        setup->getByteCodeParserSetup()->neurons[NeuronNames::INPOS] = ip;
        setup->getByteCodeParserSetup()->neurons[NeuronNames::OUT] = ot;
        setup->getByteCodeParserSetup()->neurons[NeuronNames::ADDMULT] = am;
        setup->getByteCodeParserSetup()->neurons[NeuronNames::BLOCKCALL] = bc;
        setup->getByteCodeParserSetup()->neurons[NeuronNames::STRING] = st;
        setup->getByteCodeParserSetup()->neurons[NeuronNames::ADDITIVE] = ad;
        setup->getByteCodeParserSetup()->neurons[NeuronNames::MULTIPLICATIVE] = mu;
        setup->getByteCodeParserSetup()->neurons[NeuronNames::EXIT] = ex;
        setup->getByteCodeParserSetup()->neurons[NeuronNames::TICK] = tc;
        setup->getByteCodeParserSetup()->neurons[NeuronNames::TIME] = tm;
        setup->getByteCodeParserSetup()->neurons[NeuronNames::STDIN] = si;
        setup->getByteCodeParserSetup()->neurons[NeuronNames::STDOUT] = so;
         */
    }
}