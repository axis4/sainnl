/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#include "ByteCodeWriter.hpp"
#include <inttypes.h>
#include <unicode/unistr.h>
#include <unicode/ustream.h>
#include <list>
#include <algorithm>
#include <ByteCodeLoader.hpp>
#include <ByteCodeException.hpp>
#include <Internationalization.hpp>
#include <ProcessorKnownParameters.hpp>
#include <BinaryUtils.hpp>
#include <ByteCodeUtils.hpp>
#include "../utils/Utils.hpp"

namespace nnvm {
    ByteCodeWriter::ByteCodeWriter(std::shared_ptr<std::ostream> stream, std::shared_ptr<Context> ctx) : stream(stream), ctx(ctx) { safe(); }

    ByteCodeWriter::~ByteCodeWriter() { safe(); }

    class Writer
    {
    public:
        Writer(std::shared_ptr<Context> ctx) : ctx(ctx) { };
        virtual int8_t writePackage(std::shared_ptr<ByteCode> b, std::shared_ptr<std::ostream> stream) = 0;
        virtual int8_t writeBlock(std::shared_ptr<ByteCodeBlock> b, std::shared_ptr<std::ostream> stream) = 0;
        virtual int8_t writeItem(std::shared_ptr<ByteCodeBlockItem> b, std::shared_ptr<std::ostream> stream) = 0;
        virtual int8_t writeParameter(std::shared_ptr<ByteCodeParameter> b, std::shared_ptr<std::ostream> stream) = 0;
        
    protected:
        std::shared_ptr<Context> ctx;
    };

    class BinaryWriter : public Writer
    {
    public:
        BinaryWriter(std::shared_ptr<Context> ctx) : Writer(ctx) { };

        int8_t writeParameter(std::shared_ptr<ByteCodeParameter> p, std::shared_ptr<std::ostream> stream)
        {
            if (!p) {
                return 0;
            }

            if (p->getType() == ByteCodeParameterTypes::_INTERNAL_OBJECT) {
                return 0;
            }

            if (!BinaryUtils::writeString(p->getName(), stream)) {
                return 0;
            }

            BinaryUtils::writeByte((int8_t)p->getType(), stream);
            switch (p->getType()) {
            case ByteCodeParameterTypes::BYTE:
                if (!BinaryUtils::writeByte(p->value.byteValue, stream)) {
                    return 0;
                }
                break;
            case ByteCodeParameterTypes::INTEGER:
                if (!BinaryUtils::writeInteger(p->value.integerValue, stream)) {
                    return 0;
                }
                break;
            case ByteCodeParameterTypes::DOUBLE:
                if (!BinaryUtils::writeDouble(p->value.doubleValue, stream)) {
                    return 0;
                }
                break;
            case ByteCodeParameterTypes::STRING:
                if (!BinaryUtils::writeString(p->value.stringValue, stream)) {
                    return 0;
                }
                break;
            default:
                throw ByteCodeException(StringUtils::spf(_i("err.bytecode.parameter.type.notimplemeted"), (int)p->getType()));
            }

            return 1;
        }

        int8_t writeItem(std::shared_ptr<ByteCodeBlockItem> bi, std::shared_ptr<std::ostream> stream)
        {
            if (!bi) {
                return 0;
            }

            if (!BinaryUtils::writeString(bi->getName(), stream)) {
                return 0;
            }

            auto r = bi->getParameters();
            for (auto k = std::get<0>(r); k != std::get<1>(r); ++k) {
                if (!writeParameter(*k, stream)) {
                    return 0;
                }
            }

            if (!BinaryUtils::writeInteger((int64_t)0, stream)) {
                return 0;
            }

            return 1;
        }

        int8_t writeBlock(std::shared_ptr<ByteCodeBlock> b, std::shared_ptr<std::ostream> stream)
        {
            if (!b) {
                return 0;
            }

            if (!BinaryUtils::writeString(b->getName(), stream)) {
                return 0;
            }

            if (!BinaryUtils::writeString(b->getType(), stream)) {
                return 0;
            }

            auto r = b->getParameters();
            for (auto k = std::get<0>(r); k != std::get<1>(r); ++k) {
                if (!writeParameter(*k, stream)) {
                    return 0;
                }
            }

            if (!BinaryUtils::writeInteger((int64_t)0, stream)) {
                return 0;
            }

            auto r2 = b->getItems();
            for (auto l = std::get<0>(r2); l != std::get<1>(r2); ++l) {
                if (!writeItem(*l, stream)) {
                    return 0;
                }
            }

            if (!BinaryUtils::writeInteger((int64_t)0, stream)) {
                return 0;
            }

            return 1;
        }

        int8_t writePackage(std::shared_ptr<ByteCode> c, std::shared_ptr<std::ostream> stream)
        {
            if (!c) {
                return 0;
            }

            uint8_t t = 2;
            if (!BinaryUtils::writeBinary(&t, *stream, sizeof(int8_t))) {
                return 0;
            }
            t = 'N';
            if (!BinaryUtils::writeBinary(&t, *stream, sizeof(int8_t))) {
                return 0;
            }
            t = 'V';
            if (!BinaryUtils::writeBinary(&t, *stream, sizeof(int8_t))) {
                return 0;
            }
            t = 'M';
            if (!BinaryUtils::writeBinary(&t, *stream, sizeof(int8_t))) {
                return 0;
            }

            if (!BinaryUtils::writeString(c->getName(), stream)) {
                return 0;
            }

            auto r = c->getBlocks();
            for (auto i = std::get<0>(r); i != std::get<1>(r); ++i) {
                if (!writeBlock(*i, stream)) {
                    return 0;
                }
            }

            return 1;
        }
    };

    class ConjugateWriter : public Writer
    {
    public:
        ConjugateWriter(std::shared_ptr<Context> ctx) : Writer(ctx) { };

        int8_t writeParameter(std::shared_ptr<ByteCodeParameter> p, std::shared_ptr<std::ostream> stream)
        {
            throw ByteCodeException(_i("err.bytecode.parameter_method.notimplemeted"));
        }
        
        int8_t writeItem(std::shared_ptr<ByteCodeBlockItem> bi, std::shared_ptr<std::ostream> stream)
        {
            std::shared_ptr<ByteCodeParameter> ap;
            if (!bi->tryGetParameter(LinkParams::ACTIVATION, ap)) {
                ap = std::make_shared<ByteCodeParameter>(LinkParams::ACTIVATION, 0.0);
            }
            std::shared_ptr<ByteCodeParameter> sp;
            if (!bi->tryGetParameter(LinkParams::STREAM, sp)) {
                sp = std::make_shared<ByteCodeParameter>(LinkParams::STREAM, 0.0);
            }
            std::shared_ptr<ByteCodeParameter> gp;
            if (!bi->tryGetParameter(LinkParams::GENERATION, gp)) {
                gp = std::make_shared<ByteCodeParameter>(LinkParams::GENERATION, 0.0);
            }

            *stream << StringConvUtils::utou8(bi->getName()) << "(" << ByteCodeUtils::asDouble(*ap) << "," << ByteCodeUtils::asDouble(*sp) << "," << ByteCodeUtils::asDouble(*gp) << ")";

            return 1;
        }
        
        int8_t writeState(std::shared_ptr<ByteCodeBlock> b, std::shared_ptr<std::ostream> stream)
        {
            std::shared_ptr<ByteCodeParameter> p;
            if (b->tryGetParameter(ElementParams::INPUT, p) && p->value.byteValue) {
                *stream << "i";
            }
            if (b->tryGetParameter(ElementParams::OUTPUT, p) && p->value.byteValue) {
                *stream << "o";
            }
            if (b->tryGetParameter(ElementParams::EXPECTED, p) && p->value.byteValue) {
                *stream << "e";
            }
            
            if (b->tryGetParameter(ElementParams::TEMPLATE, p) && p->value.byteValue) {
                *stream << "-t";
            }
            
            *stream << "-" << StringConvUtils::utou8(b->getType());

            return 1;
        }
        
        int8_t writeBlock(std::shared_ptr<ByteCodeBlock> b, std::shared_ptr<std::ostream> stream)
        {
            std::shared_ptr<ByteCodeParameter> tp;
            if (!b->tryGetParameter(ElementParams::THRESHOLD, tp)) {
                tp = std::make_shared<ByteCodeParameter>(ElementParams::THRESHOLD, 0.0);
            }
            std::shared_ptr<ByteCodeParameter> bp;
            if (!b->tryGetParameter(ElementParams::BIAS, bp)) {
                bp = std::make_shared<ByteCodeParameter>(ElementParams::BIAS, 0.0);
            }
            std::shared_ptr<ByteCodeParameter> sp;
            if (!b->tryGetParameter(ElementParams::SUM, sp)) {
                sp = std::make_shared<ByteCodeParameter>(ElementParams::SUM, 0.0);
            }
            *stream << StringConvUtils::utou8(b->getName()) << "(" << ByteCodeUtils::asDouble(*sp) << "," << ByteCodeUtils::asDouble(*tp) << "," << ByteCodeUtils::asDouble(*bp) << "-";
            writeState(b, stream);
            *stream << "): ";
            auto r = b->getItems();
            for (auto l = std::get<0>(r); l != std::get<1>(r); ++l) {
                writeItem(*l, stream);
                *stream << " ";
            }

            return 1;
        }
        
        int8_t writePackage(std::shared_ptr<ByteCode> c, std::shared_ptr<std::ostream> stream)
        {
            *stream << ";" << StringConvUtils::utoescaped(c->getName(), true) << std::endl;
            auto r = c->getBlocks();
            for (auto i = std::get<0>(r); i != std::get<1>(r); ++i) {
                writeBlock(*i, stream);
                *stream << std::endl;
            }

            return 1;
        }
    };

    class JsonWriter : public Writer
    {
    public:
        JsonWriter(std::shared_ptr<Context> ctx) : Writer(ctx) { };

        int8_t writeParameter(std::shared_ptr<ByteCodeParameter> p, std::shared_ptr<std::ostream> stream)
        {
            if (p->getType() == ByteCodeParameterTypes::_INTERNAL_OBJECT) {
                return 0;
            }

            *stream << "\"" << StringConvUtils::utoescaped(p->getName(), true) << "\": {";
            *stream << "\"type\": " << (int32_t)p->getType();
            if (p->getType() != ByteCodeParameterTypes::_INTERNAL_OBJECT) {
                *stream << ", ";
                switch (p->getType()) {
                    case ByteCodeParameterTypes::BYTE:
                        *stream << "\"value\": " << (int32_t) p->value.byteValue;
                        break;
                    case ByteCodeParameterTypes::INTEGER:
                        *stream << "\"value\": " << p->value.integerValue;
                        break;
                    case ByteCodeParameterTypes::DOUBLE:
                        *stream << "\"value\": " << p->value.doubleValue;
                        break;
                    case ByteCodeParameterTypes::STRING:
                        *stream << "\"value\": \"" << StringConvUtils::utoescaped(p->value.stringValue, true) << "\"";
                        break;
                    default:
                        throw ByteCodeException(
                                StringUtils::spf(_i("err.bytecode.parameter.type.notimplemented"), (int) p->getType()));
                }
            }
            *stream << "}";

            return 1;
        }

        int8_t writeItem(std::shared_ptr<ByteCodeBlockItem> bi, std::shared_ptr<std::ostream> stream)
        {
            *stream << "{\"itemName\": \"" << StringConvUtils::utoescaped(bi->getName(), true) << "\", \"parameters\": {";
            int8_t m = 0;
            auto r = bi->getParameters();
            for (auto k = std::get<0>(r); k != std::get<1>(r); k++) {
                if (m) {
                    *stream << ", ";
                }
                m = writeParameter(*k, stream);
            }
            *stream << "}}";

            return 1;
        }

        int8_t writeBlock(std::shared_ptr<ByteCodeBlock> b, std::shared_ptr<std::ostream> stream)
        {
            *stream << "{\"blockName\": \"" << StringConvUtils::utoescaped(b->getName(), true) << "\", "
                << "\"type\": \"" << StringConvUtils::utoescaped(b->getType(), true) << "\", \"parameters\": {";
            int8_t m = 0;
            auto r = b->getParameters();
            for (auto k = std::get<0>(r); k != std::get<1>(r); ++k) {
                if (m) {
                    *stream << ", ";
                }
                m = writeParameter(*k, stream);
            }
            *stream << "}, \"items\": [";
            m = 0;
            auto r2 = b->getItems();
            for (auto l = std::get<0>(r2); l != std::get<1>(r2); ++l) {
                if (m) {
                    *stream << ", ";
                }
                m = writeItem(*l, stream);
            }
            *stream << "]}";

            return 1;
        }

        int8_t writePackage(std::shared_ptr<ByteCode> c, std::shared_ptr<std::ostream> stream)
        {
            *stream << "{\"packageName\": \"" << StringConvUtils::utoescaped(c->getName(), true) << "\", \"blocks\": [";
            int8_t m = 0;
            auto r = c->getBlocks();
            for (auto i = std::get<0>(r); i != std::get<1>(r); ++i) {
                if (!m) {
                    m = 1;
                }
                else {
                    *stream << ", ";
                }
                writeBlock(*i, stream);
            }
            *stream << "]}";

            return 1;
        }
    };

    class SourceWriter : public Writer
    {
    public:
        SourceWriter(std::shared_ptr<Context> ctx) : Writer(ctx) { };

        int8_t writeParameter(std::shared_ptr<ByteCodeParameter> p, std::shared_ptr<std::ostream> stream)
        {
            throw ByteCodeException(_i("err.bytecode.parameter_method.notimplemeted"));
        }

        int8_t writeItem(std::shared_ptr<ByteCodeBlockItem> bi, std::shared_ptr<std::ostream> stream)
        {
            throw ByteCodeException(_i("err.bytecode.item_method.notimplemeted"));
        }

        int8_t writeBlock(std::shared_ptr<ByteCodeBlock> b, std::shared_ptr<std::ostream> stream)
        {
            std::shared_ptr<ByteCodeParameter> isExpected;
            if (!b->tryGetParameter(ElementParams::EXPECTED, isExpected) || !isExpected->value.byteValue) {
                std::shared_ptr<ByteCodeParameter> isInput;
                if (b->tryGetParameter(ElementParams::INPUT, isInput) && isInput->value.byteValue) {
                    *stream << "\tin " << StringConvUtils::utou8(b->getName()) << std::endl;
                }
                std::shared_ptr<ByteCodeParameter> isOutput;
                if (b->tryGetParameter(ElementParams::OUTPUT, isOutput) && isOutput->value.byteValue) {
                    *stream << "\tout " << StringConvUtils::utou8(b->getName()) << std::endl;
                }
                *stream << "\t";
                std::shared_ptr<ByteCodeParameter> isTemplate;
                if (b->tryGetParameter(ElementParams::TEMPLATE, isTemplate) && isTemplate->value.byteValue) {
                    *stream << "[";
                    int8_t s = false;
                    auto r = b->getIncomes();
                    for (auto i = std::get<0>(r); i != std::get<1>(r); i++) {
                        std::shared_ptr<ByteCodeParameter> w;
                        if ((*i)->tryGetParameter(LinkParams::GENERATION, w)) {
                            if (s) {
                                *stream << ", ";
                            }
                            else {
                                s = true;
                            }
                            *stream << StringConvUtils::utou8((*i)->getOwner().lock()->getName());
                        }
                    }
                    *stream << "] ";
                }
                std::shared_ptr<WritableNeuron> code;
                try {
                    code = ctx->setup->getByteCodeWriterSetup()->getNeuron(b->getType());
                } catch (ByteCodeException e) {
                    ctx->getNeuron(b->getType());
                    code = ctx->setup->getByteCodeWriterSetup()->getNeuron(b->getType());
                }
                if (!code) {
                    if (!ctx->setup->getProcessorSetup()->tolerate_missing) {
                        throw ByteCodeException(StringUtils::spf(_i("err.bytecode.type.unknown"), StringConvUtils::utow(b->getType())));
                    }
                }
                code->writeSourceBlock(b, stream);
                *stream << std::endl;
            }

            return 1;
        }

        int8_t writePackage(std::shared_ptr<ByteCode> c, std::shared_ptr<std::ostream> stream)
        {
            *stream << "\"package " << StringConvUtils::utou8(c->getName()) << "\"" << std::endl;
            *stream << "network {" << std::endl;
            auto r = c->getBlocks();
            std::list<std::shared_ptr<ByteCodeBlock>> blocks(std::get<0>(r), std::get<1>(r));
            blocks.sort([](std::shared_ptr<ByteCodeBlock> a, std::shared_ptr<ByteCodeBlock> b) { return a->getName() < b->getName(); });
            for (auto i = blocks.begin(); i != blocks.end(); i++) {
                writeBlock(*i, stream);
            }
            *stream << "}" << std::endl;

            return 1;
        }
    };

    int8_t ByteCodeWriter::write(std::shared_ptr<ByteCode> code)
    {
        safe();
        return !BinaryWriter(this->ctx).writePackage(code, stream);
    }

    int8_t ByteCodeWriter::writeJson(std::shared_ptr<ByteCode> code)
    {
        safe();
        return !JsonWriter(this->ctx).writePackage(code, stream);
    }

    int8_t ByteCodeWriter::writeConjugate(std::shared_ptr<ByteCode> code)
    {
        safe();
        return !ConjugateWriter(this->ctx).writePackage(code, stream);
    }

    int8_t ByteCodeWriter::writeXml(std::shared_ptr<ByteCode> code)
    {
        safe();
        return 0;
        //return !XmlWriter(this->ctx).writePackage(code, stream);
    }

    int8_t ByteCodeWriter::writeSource(std::shared_ptr<ByteCode> code)
    {
        safe();
        return !SourceWriter(this->ctx).writePackage(code, stream);
    }
}
