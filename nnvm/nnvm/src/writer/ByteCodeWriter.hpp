/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#pragma once

namespace nnvm {
    class ByteCodeWriter;
}

#include <predef.h>
#include <ostream>
#include <memory>
#include <Disposable.hpp>
#include <ByteCode.hpp>
#include <MachineSetup.hpp>
#include <ByteCodeLoader.hpp>

namespace nnvm {
    class ByteCodeWriter : public Disposable
    {
    public:
        ByteCodeWriter(std::shared_ptr<std::ostream> stream, std::shared_ptr<Context> ctx);
        virtual ~ByteCodeWriter();
        int8_t write(std::shared_ptr<ByteCode> code);
        int8_t writeConjugate(std::shared_ptr<ByteCode> code);
        int8_t writeJson(std::shared_ptr<ByteCode> code);
        int8_t writeXml(std::shared_ptr<ByteCode> code);
        int8_t writeSource(std::shared_ptr<ByteCode> code);
    private:
        std::shared_ptr<std::ostream> stream;
        std::shared_ptr<Context> ctx;
    };
}
