/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#include <WritableNeuron.hpp>
#include <list>
#include <ProcessorKnownParameters.hpp>
#include <ByteCodeUtils.hpp>
#include "../utils/Utils.hpp"

namespace nnvm {
    int8_t WritableNeuron::dumpItems(icu::UnicodeString itemType, std::shared_ptr<ByteCodeBlock> code, std::shared_ptr<std::ostream> stream)
    {
        int8_t s = false;
        auto r = code->getIncomes();
        std::list<std::shared_ptr<ByteCodeBlockItem>> items(std::get<0>(r), std::get<1>(r));
        items.sort([](std::shared_ptr<ByteCodeBlockItem> a, std::shared_ptr<ByteCodeBlockItem> b) {
            return a->getOwner().lock()->getName() < b->getOwner().lock()->getName();
        });
        for (auto i = items.begin(); i != items.end(); ++i) {
            std::shared_ptr<ByteCodeParameter> w;
            if ((*i)->tryGetParameter(itemType, w)) {
                if (!s) {
                    *stream << "+";
                    s = true;
                }
                else {
                    *stream << ",";
                }
                *stream << " (* " << StringConvUtils::utou8(ByteCodeUtils::asString(*w)) << ", " << StringConvUtils::utou8((*i)->getOwner().lock()->getName()) << ")";
            }
        }
        if (!s) {
            *stream << "+ 0";
        }
        return s;
    }

    WritableNeuron::WritableNeuron(std::shared_ptr<MachineSetup> setup, std::shared_ptr<LibraryLoader> loader)
            : setup(setup), loader(loader)
    {
        auto s = this;
        argumentWriters =
        {
            [&s](std::shared_ptr<ByteCodeBlock> code, std::shared_ptr<std::ostream> stream)
            {
                *stream << "(";
                s->dumpItems(LinkParams::ACTIVATION, code, stream);
                *stream << ")";
            },
            [&s](std::shared_ptr<ByteCodeBlock> code, std::shared_ptr<std::ostream> stream)
            {
                *stream << "(";
                s->dumpItems(LinkParams::STREAM, code, stream);
                std::shared_ptr<ByteCodeParameter> source;
                auto paramName = StringConvUtils::wtou(StringConvUtils::utow(ElementParams::BIAS));
                if (code->tryGetParameter(paramName, source)) {
                    *stream << ", " << StringConvUtils::utou8(ByteCodeUtils::asString(*source));
                }
                *stream << ")";
            },
            [&s](std::shared_ptr<ByteCodeBlock> code, std::shared_ptr<std::ostream> stream)
            {
                std::shared_ptr<ByteCodeParameter> source;
                auto paramName = StringConvUtils::wtou(StringConvUtils::utow(ElementParams::INTERNAL_STATE));
                if (!code->tryGetParameter(paramName, source)) {
                    *stream << "0";
                    return;
                }
                *stream << StringConvUtils::utou8(ByteCodeUtils::asString(*source));
            }
        };
    }
}
