/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#include <Calculator.hpp>
#include <StringConv.hpp>
#include <ByteCodeUtils.hpp>
#include "utils/Utils.hpp"

namespace nnvm {
    std::map<icu::UnicodeString, std::function<icu::UnicodeString(Token<TokenKind>&, Token<TokenKind>&)>> Calculator::actions = {
        {USS("+"), [](Token<TokenKind>& left, Token<TokenKind>& right) {
            return ByteCodeUtils::asString(ByteCodeUtils::asNumber(left.value) + ByteCodeUtils::asNumber(right.value)); }},
        {USS("-"), [](Token<TokenKind>& left, Token<TokenKind>& right) {
            return ByteCodeUtils::asString(ByteCodeUtils::asNumber(left.value) - ByteCodeUtils::asNumber(right.value)); }},
        {USS("*"), [](Token<TokenKind>& left, Token<TokenKind>& right) {
            return ByteCodeUtils::asString(ByteCodeUtils::asNumber(left.value) * ByteCodeUtils::asNumber(right.value)); }},
        {USS("/"), [](Token<TokenKind>& left, Token<TokenKind>& right) {
            return ByteCodeUtils::asNumber(right.value) != 0.0 ? ByteCodeUtils::asString(ByteCodeUtils::asNumber(left.value) / ByteCodeUtils::asNumber(right.value)) : USS("0"); }},
        {USS(">="), [](Token<TokenKind>& left, Token<TokenKind>& right) {
            return ByteCodeUtils::asNumber(left.value) >= ByteCodeUtils::asNumber(right.value) != 0.0 ? USS("1") : USS("0"); }},
        {USS("<="), [](Token<TokenKind>& left, Token<TokenKind>& right) {
            return ByteCodeUtils::asNumber(left.value) <= ByteCodeUtils::asNumber(right.value) != 0.0 ? USS("1") : USS("0"); }},
        {USS("=="), [](Token<TokenKind>& left, Token<TokenKind>& right) {
            return ByteCodeUtils::asNumber(left.value) == ByteCodeUtils::asNumber(right.value) != 0.0 ? USS("1") : USS("0"); }},
        {USS("!="), [](Token<TokenKind>& left, Token<TokenKind>& right) {
            return ByteCodeUtils::asNumber(left.value) != ByteCodeUtils::asNumber(right.value) != 0.0 ? USS("1") : USS("0"); }},
        {USS(">"), [](Token<TokenKind>& left, Token<TokenKind>& right) {
            return ByteCodeUtils::asNumber(left.value) > ByteCodeUtils::asNumber(right.value) != 0.0 ? USS("1") : USS("0"); }},
        {USS("<"), [](Token<TokenKind>& left, Token<TokenKind>& right) {
            return ByteCodeUtils::asNumber(left.value) < ByteCodeUtils::asNumber(right.value) != 0.0 ? USS("1") : USS("0"); }},
    };

    int8_t checkIfNumber(Operand& operand)
    {
        return (int8_t) (operand.literal && operand.literal->isOf(TokenKind::NUMBER) ? 1 : 0);
    }

    int8_t Calculator::process(Operand& operand)
    {
        if (operand.call) {
            return 0;
        }
        if (operand.literal) {
            return (int8_t) (operand.literal->isOf(TokenKind::NUMBER) || operand.literal->isOf(TokenKind::AS_NUMBER) ? 1 : 0);
        }
        if (!operand.expression) {
            return 0;
        }
        if (!operand.expression->left->literal) {
            if (!process(*operand.expression->left)) {
                return 0;
            }
        }
        if (!operand.expression->right->literal) {
            if (!process(*operand.expression->right)) {
                return 0;
            }
        }
        if (!checkIfNumber(*operand.expression->left)) {
            return 0;
        }
        if (!checkIfNumber(*operand.expression->right)) {
            return 0;
        }
        operand.literal = std::make_shared<Token<TokenKind>>(operand.loader);
        operand.literal->type.push_back(TokenKind::NUMBER);
        operand.literal->value = actions[operand.expression->middle](*operand.expression->left->literal, *operand.expression->right->literal);

        operand.expression = nullptr;
        operand.call =  nullptr;
        return 1;
    }
}
