/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#include <Logger.hpp>
#include <algorithm>
#include <iostream>
#include <inttypes.h>
#include <map>
#include <string>
#include <algorithm>
#include <locale>
#include <unicode/ustream.h>
#include "utils/Utils.hpp"
#include "utils/RogueLikeUtil.h"

namespace nnvm {
    Logger::Logger() : limitLevel(LogLevel::Info) {}
    Logger::Logger(const LogLevel level) : limitLevel(level) {}
    Logger::~Logger() {}
    void Logger::setLevel(const LogLevel level) { limitLevel = level; }
    void Logger::debug(const icu::UnicodeString& message) { log(LogLevel::Debug, message); }
    void Logger::info(const icu::UnicodeString& message) { log(LogLevel::Info, message); }
    void Logger::warn(const icu::UnicodeString& message) { log(LogLevel::Warn, message); }
    void Logger::error(const icu::UnicodeString& message) { log(LogLevel::Fatal, message); }

    std::map<icu::UnicodeString, LogLevel> names{
        { USS("debug"), LogLevel::Debug },
        { USS("info"), LogLevel::Info },
        { USS("warn"), LogLevel::Warn },
        { USS("error"), LogLevel::Fatal },
    };

    LogLevel Logger::parseLevel(const icu::UnicodeString& level)
    {
        auto f = names.find(icu::UnicodeString(level).toLower());
        if (f != names.end()) {
            return f->second;
        }
        return LogLevel::Info;
    }

    std::map<LogLevel, icu::UnicodeString> levels{
        { LogLevel::Debug, USS("[DEBUG]") },
        { LogLevel::Info, USS("[INFO]") },
        { LogLevel::Warn, USS("[WARN]") },
        { LogLevel::Fatal, USS("[ERROR]") },
    };

    std::map<LogLevel, int> colors{
        { LogLevel::Debug, RogueLikeUtil::BLUE },
        { LogLevel::Info, RogueLikeUtil::GREEN },
        { LogLevel::Warn, RogueLikeUtil::YELLOW },
        { LogLevel::Fatal, RogueLikeUtil::RED },
    };

    StdRight::StdRight() : Logger() {}
    StdRight::StdRight(const LogLevel level) : Logger(level) {}

    void StdRight::log(const LogLevel level, const icu::UnicodeString& message)
    {
        if (this->limitLevel > level) {
            return;
        }
        //RogueLikeUtil::setColor(colors[level]);
    #if defined(_WIN32)
        auto tmp = StringConvUtils::utow(message);
        auto l = StringConvUtils::utow(levels[level]);
        while (tmp.length()) {
            auto t = tmp.substr(0, nnvm_min(50, tmp.length()));
            tmp = tmp.substr(t.length());
            auto sp = std::wstring("");
            for (auto i = 0; i < 80 - l.length() - t.length() - 2; i++) {
                sp += ' ';
            }
            std::wcout << sp + t + L" " + l + L"\n";
        }
    #else
        auto tmp = StringConvUtils::utou8(message);
        auto l = StringConvUtils::utou8(levels[level]);
        auto tm = StringUtils::timeStr(PerformanceUtils::now_nanoseconds());
        auto timelen = static_cast<int32_t>(tm.length());
        const int32_t columns = 80;
        const int32_t extraFormat = 3;
        std::string accumulated;
        while (tmp.length()) {
            auto messageLimit = static_cast<int32_t>(columns - timelen - extraFormat - l.length());
            auto t = tmp.substr(0, nnvm_min(messageLimit, tmp.length()));
            tmp = tmp.substr(t.length());
            auto leftSpace = static_cast<int32_t>(messageLimit - t.length());
            std::string sp;
            for (auto i = 0; i < leftSpace; i++) {
                sp += ' ';
            }
            accumulated += StringUtils::spf("{0}{1}({2}) {3}\n", sp, t, tm, l);
        }
        std::cout << accumulated;
    #endif
        //RogueLikeUtil::setColor(RogueLikeUtil::GREY);
    }
}
