// StringUtils is also available under the CPOL license as part of the FormatString article
// on CodeProject: http://www.codeproject.com/KB/string/FormatString.aspx

#include "StringUtils.hpp"
#include <cstdio>
#include <cassert>

#ifndef STR_USE_WIN32_CONV
#include <cstdlib>
#endif

///////////////////////////////////////////////////////////////////////////////

#ifdef STR_USE_WIN32_DBCS
static int g_MaxCharSize=0;
#endif

// Returns 1 if the ANSI code page is single-byte, or 2 if it is double-byte (DBCS)
long GetMaxCharSize( void )
{
#ifdef STR_USE_WIN32_DBCS
	if (!g_MaxCharSize) {
		CPINFO info;
		if (GetCPInfo(CP_ACP,&info))
			g_MaxCharSize=info.MaxCharSize;
	}
	return g_MaxCharSize;
#else
	return MB_CUR_MAX;
#endif
}

// Copies src to dst. size is the size of dst in bytes, including the terminating 0.
// Returns the number of characters copied, excluding the terminating 0.
// The return value is <=size-1. If size is 0, returns 0 and does nothing.
// The result in dst is always 0 terminated.
long Strcpy( char *dst, long size, const char *src )
{
	assert(dst);
	assert(src);
	assert(size>0);
	if (size<=0) return 0;
	char *dst0=dst;
	if (GetMaxCharSize()==1) {
		// SBCS version
		while (size>1) {
			if (*src==0) break;
			*dst++=*src++;
			size--;
		}
	}
	else {
		// DBCS version
		while (size>1) {
			if (*src==0) break;
#ifdef STR_USE_WIN32_DBCS
			if (IsDBCSLeadByte((BYTE)*src)) {
#else
			if (isleadbyte((unsigned char)*src)) {
#endif
				if (size==2) break;
				if (src[1]==0) break;
				*dst++=*src++;
				*dst++=*src++;
				size-=2;
			}
			else {
				*dst++=*src++;
				size--;
			}
		}
	}
	*dst=0;
	return dst-dst0;
}

long Strcpy( wchar_t *dst, long size, const wchar_t *src )
{
	assert(dst);
	assert(src);
	assert(size>0);
	if (size<=0) return 0;
	long len=Strlen(src);
	if (len>size-1)
		len=size-1;
	if (len>0 && src[len]>=0xDC00 && src[len]<=0xDFFF) // check for trailing surrogate
		len--;
	memcpy(dst,src,len*sizeof(wchar_t));
	dst[len]=0;
	return len;
}

// Copies src to dst. size is the size of dst in characters, including the terminating 0.
// Copies up to len characters and always appends terminating 0.
long Strncpy( char *dst, long size, const char *src, long len )
{
	assert(dst);
	assert(src);
	assert(size>0);
	if (size<=0) return 0;
	char *dst0=dst;
	const char *end=src+len;
	if (GetMaxCharSize()==1) {
		// SBCS version
		while (size>1) {
			if (src==end) break;
			*dst++=*src++;
			size--;
		}
	}
	else {
		// DBCS version
		while (size>1) {
			if (src==end) break;
#ifdef STR_USE_WIN32_DBCS
			if (IsDBCSLeadByte((BYTE)*src)) {
#else
			if (isleadbyte((unsigned char)*src)) {
#endif
				if (size==2) break;
				if (src+1==end) break;
				*dst++=*src++;
				*dst++=*src++;
				size-=2;
			}
			else {
				*dst++=*src++;
				size--;
			}
		}
	}
	*dst=0;
	return dst-dst0;
}

long Strncpy( wchar_t *dst, long size, const wchar_t *src, long len )
{
	assert(dst);
	assert(src);
	assert(size>0);
	if (size<=0) return 0;
	if (len>size-1)
		len=size-1;
	if (len>0 && src[len]>=0xDC00 && src[len]<=0xDFFF) // check for trailing surrogate
		len--;
	memcpy(dst,src,len*sizeof(wchar_t));
	dst[len]=0;
	return len;
}

// Appends src to dst. size is the size of dst in bytes, including the terminating 0.
// Returns the number of characters copied, excluding the terminating 0.
// The return value is <=size-1-strlen(dst). If size>=strlen(dst), returns 0 and does nothing.
// The result in dst is always 0 terminated.
long Strcat( char *dst, long size, const char *src )
{
	assert(dst);
	long len=Strlen(dst);
	assert(len<size);
	return Strcpy(dst+len,size-len,src);
}

long Strcat( wchar_t *dst, long size, const wchar_t *src )
{
	assert(dst);
	long len=Strlen(dst);
	assert(len<size);
	return Strcpy(dst+len,size-len,src);
}

// Writes formatted string to dst. size is the size of dst in characters, including the terminating 0.
// Returns the number of characters written, excluding the terminating 0.
// The return value is <=size-1. If size is 0, returns 0 and does nothing.
// The result in dst is always 0 terminated.
long Sprintf( char *dst, long size, const char *format, ... )
{
	va_list args;
	va_start(args,format);
	long len=Vsprintf(dst,size,format,args);
	va_end(args);
	return len;
}

long Sprintf( wchar_t *dst, long size, const wchar_t *format, ... )
{
	va_list args;
	va_start(args,format);
	long len=Vsprintf(dst,size,format,args);
	va_end(args);
	return len;
}

long Vsprintf( char *dst, long size, const char *format, va_list args )
{
	assert(dst);
	assert(format);
	assert(size>0);
	if (size<=0) return 0;
#if defined(_WIN32) || defined(_WIN64)
	long len = _vsnprintf_s(dst, size, size - 1, format, args);
#else
	long len = vsnprintf(dst, size-1, format, args);
#endif
	if (len<0)
		len=size-1;
	dst[len]=0;
	return len;
}

long Vsprintf( wchar_t *dst, long size, const wchar_t *format, va_list args )
{
	assert(dst);
	assert(format);
	assert(size>0);
	if (size<=0) return 0;
#if defined(_WIN32) || defined(_WIN64)
	long len=_vsnwprintf_s(dst,size,size-1,format,args);
#else
	long len=vswprintf(dst,size-1,format,args);
#endif
	if (len<0)
		len=size-1;
	dst[len]=0;
	return len;
}

// Convert between multi-byte and wide characters. size is the size of dst in characters, including the
// terminating 0.
// Return the number of characters copied, excluding the terminating 0.
// The return value is <=size-1. If size is 0, returns 0 and does nothing.
// The result in dst is always 0 terminated.

#ifdef STR_USE_WIN32_CONV
long MbsToWcs( wchar_t *dst, long size, const char *src, int codePage )
{
	if (!dst)
		return MultiByteToWideChar(codePage,0,src,Strlen(src),NULL,0);

	assert(size);
	if (size==0) return 0;
	if (size==1) {
		dst[0]=0;
		return 0;
	}
	long len=Strlen(src);
	dst[size-2]=0;
	int res=MultiByteToWideChar(codePage,0,src,len,dst,size-1);
	if (res) {
		// the result fits
		dst[res]=0;
		return res;
	}
	if (GetLastError()!=ERROR_INSUFFICIENT_BUFFER) { // some unknown error
		dst[0]=0;
		return 0;
	}

	if (!dst[size-2]) // could not fit a surrogate pair
		return size-2;
	dst[size-1]=0;
	return size-1;
}

long WcsToMbs( char *dst, long size, const wchar_t *src, int codePage )
{
	if (!dst)
		return WideCharToMultiByte(codePage,0,src,Strlen(src),NULL,0,NULL,NULL);

	assert(size);
	if (size==0) return 0;
	if (size==1) {
		dst[0]=0;
		return 0;
	}
	long len=Strlen(src);
	long l=size;
	if (l>10) l=10;
	memset(dst+size-l,0,l); // fill the end with zeros (up to 10 bytes)
	int res=WideCharToMultiByte(codePage,0,src,len,dst,size-1,NULL,NULL);
	if (res) {
		// the result fits
		dst[res]=0;
		return res;
	}
	if (GetLastError()!=ERROR_INSUFFICIENT_BUFFER) { // some unknown error
		dst[0]=0;
		return 0;
	}

	// find the last non-zero to return the correct length
	for (len=size-1;len>0;len--)
		if (dst[len-1])
			return len;
	return 0;
}
#else
long MbsToWcs( wchar_t *dst, long size, const char *src )
{
	if (!dst) {
#if _MSC_VER>=1400 // VC8.0
		size_t res;
		if (mbstowcs_s(&res,NULL,0,src,0)!=0)
			return 0;
		return res-1;
#else
		long res=mbstowcs(NULL,src,0);
		if (res<0) return 0;
		return res;
#endif
	}

	assert(size);
	if (size==0) return 0;
	if (size==1) {
		dst[0]=0;
		return 0;
	}
#if _MSC_VER>=1400 // VC8.0
	size_t res;
	mbstowcs_s(&res,dst,size,src,_TRUNCATE);
	return res-1;
#else
	long res=mbstowcs(dst,src,size-1);
	if (res<0) {
		dst[0]=0;
		return 0;
	}
	if (res==size-1)
		dst[res]=0;
	return res;
#endif
}

long WcsToMbs( char *dst, long size, const wchar_t *src )
{
	if (!dst) {
#if _MSC_VER>=1400 // VC8.0
		size_t res;
		if (wcstombs_s(&res,NULL,0,src,0)!=0)
			return 0;
		return res-1;
#else
		long res=wcstombs(NULL,src,0);
		if (res<0) return 0;
		return res;
#endif
	}

	assert(size);
	if (size==0) return 0;
	if (size==1) {
		dst[0]=0;
		return 0;
	}

#if _MSC_VER>=1400 // VC8.0
	size_t res;
	if (wcstombs_s(&res,dst,size,src,_TRUNCATE)!=0) {
		dst[0]=0;
		return 0;
	}
	return res-1;
#else
	long res=wcstombs(dst,src,size-1);
	if (res<0) {
		dst[0]=0;
		return 0;
	}
	if (res==size-1)
		dst[res]=0;
	return res;
#endif
}
#endif
