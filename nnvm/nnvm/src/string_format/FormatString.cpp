// StringUtils is also available under the CPOL license as part of the FormatString article
// on CodeProject: http://www.codeproject.com/KB/string/FormatString.aspx

#include "FormatString.hpp"
#include "StringUtils.hpp"
#include <cassert>
#include <cstdio>
#include <cstdlib>

#ifdef STR_USE_STL
#include <iostream>
#endif

#ifdef STR_USE_WIN32_NLS
#include <shlwapi.h>
#pragma comment(lib,"shlwapi.lib")
#endif

#ifdef STR_USE_WIN32_CONV
#define CODEPAGE_ARG ,codepage
#else
#define CODEPAGE_ARG
#endif

namespace nnvm {
    const int ALLOC_GRANULARITY=4;

    CFormatArg CFormatArg::s_Null;

    // Manages a temporary memory buffer
    template<class T> class CAutoBuf
    {
    public:
        CAutoBuf( long size ) { m_pData=(T*)malloc(size*sizeof(T)); }
        ~CAutoBuf( void ) { if (m_pData) free(m_pData); }
        T *GetData( void ) { return m_pData; }

    private:
        T *m_pData;
    };

///////////////////////////////////////////////////////////////////////////////
// ANSI FormatString

#ifdef STR_USE_WIN32_NLS
    // Creates a number format based on the system settings but with user-defined number of digits.
    // sep1 and sep2 must point to buffers to contain the decimal and thousand separators
    static void CreateNumberFormat( NUMBERFMTA &numFmt, int digits, char *sep1, char *sep2 )
    {
        numFmt.NumDigits=digits;
        GetLocaleInfoA(LOCALE_USER_DEFAULT,LOCALE_SDECIMAL,sep1,10);
        numFmt.lpDecimalSep=sep1;
        GetLocaleInfoA(LOCALE_USER_DEFAULT,LOCALE_STHOUSAND,sep2,10);
        numFmt.lpThousandSep=sep2;
        char buf[10];
        GetLocaleInfoA(LOCALE_USER_DEFAULT,LOCALE_ILZERO,buf,10);
        numFmt.LeadingZero=atol(buf);
        GetLocaleInfoA(LOCALE_USER_DEFAULT,LOCALE_INEGNUMBER,buf,10);
        numFmt.NegativeOrder=atol(buf);
        GetLocaleInfoA(LOCALE_USER_DEFAULT,LOCALE_SGROUPING,buf,10);

        numFmt.Grouping=0;
        for (const char *p=buf;*p;p++)
            if (*p>='1' && *p<='9')
                numFmt.Grouping=numFmt.Grouping*10+(*p-'0');
    }
#endif

    // Formats a string. The function can work in 3 modes - output to stream, output to fixed size buffer
    // or output to dynamically allocated buffer. Returns the length of the result, or -1 if error
    static long FormatStringInternal( char *&string, long len, CFormatStringAllocator *allocator, CFormatStringOutA *out, const char *format, int argc, const CFormatArg * const args[] ) {
		// no arguments, directly output the format string
		if (argc == 0) {
			long l = Strlen(format);
			if (out) {
				// output to stream
				return out->Output(format, l) ? l : -1;
			}

			// output to memory
			if (allocator) {
				// allocate buffer
				if (!allocator->Realloc((void *&) string, l + 1)) return -1;
			}
			else {
				// limit the size
				if (l >= len) {
					// don't end on a lead byte
#ifdef STR_USE_WIN32_DBCS
					if (GetMaxCharSize()==2 && IsDBCSLeadByte((BYTE)format[len-2]))
#else
					if (GetMaxCharSize() == 2 && isleadbyte((unsigned char) format[len - 2]))
#endif
						l = len - 2;
					else
						l = len - 1;
				}
			}
			memcpy(string, format, l);
			string[l] = 0;
			return l;
		}

		long i = 0;
		while (*format) {
			if (*format == '{') {
				format++;
				if (*format != '{') {
					char token[256];
					{
						const char *end = strchr(format, '}');
						if (!end) break;
						int l = (int) (end - format);
						if (l >= _countof(token)) break;
						memcpy(token, format, l);
						token[l] = 0;
						format = end + 1;
					}

					char *end2;
					long index = strtol(token, &end2, 10);
					assert(index >= 0 && index < argc); // the index of the argument is invalid
					long width = 0;
					if (*end2 == ',') {
						if (end2[1] != '*') {
							width = strtol(end2 + 1, &end2, 10);
						}
						else {
							width = strtol(end2 + 2, &end2, 10);
							assert(width >= 0 && width < argc); // the index of the width argument is invalid
							assert(args[width]->type == CFormatArg::TYPE_INT || args[width]->type ==
																				CFormatArg::TYPE_UINT); // the width argument must be integer value
							width = (int) args[width]->i;
						}
					}
					char *fmt = NULL;
					if (*end2 == ':') {
						fmt = end2 + 1;
						end2 = (char *) strchr(fmt, '@');
						if (end2) *end2 = 0;
					}

					char buf[256];
					buf[0] = 0;
					int type = args[index]->type;
					const char *text = NULL; // ansi string parameter
					const wchar_t *wtext = NULL; // unicode string parameter
#ifdef STR_USE_WIN32_CONV
					int codepage=CP_ACP;
#endif

					switch (type) {
						// integer
						case CFormatArg::TYPE_INT:
						case CFormatArg::TYPE_UINT:
						case CFormatArg::TYPE_INT64:
						case CFormatArg::TYPE_UINT64: {
							if (!fmt) {
								if (type == CFormatArg::TYPE_INT || type == CFormatArg::TYPE_INT64)
									fmt = (char *) "d"; // default for int
								else
									fmt = (char *) "u"; // default for unsigned int
							}
							switch (fmt[0]) {
								case 'c':
									buf[0] = (char) args[index]->i;
									buf[1] = 0;
									break;
								case 'd':
								case 'u':
								case 'x':
								case 'X': {
									// just use sprintf
									long w1 = width;
									if (w1 > _countof(buf) - 1)
										w1 = _countof(buf) - 1;
									if (width > 0) width -= w1; // right alignment up to 255 will be handled by sprintf

									bool plus = false;
									const char *fmt2 = fmt + 1;
									if (fmt[0] == 'd' && fmt2[0] == '+')
										plus = true, fmt2++;
									bool zero = (w1 > 0 && fmt2[0] == '0');
									char sfmt[20] = "%";
									int l = 1;
									if (plus) sfmt[l++] = '+';
									if (zero) sfmt[l++] = '0';
									if (w1 > 0) sfmt[l++] = '*';
									if (type == CFormatArg::TYPE_INT || type == CFormatArg::TYPE_UINT) {
										// 32 bit
										sfmt[l++] = fmt[0];
										sfmt[l] = 0;
										if (w1 > 0)
											Sprintf(buf, _countof(buf), sfmt, w1, args[index]->i);
										else
											Sprintf(buf, _countof(buf), sfmt, args[index]->i);
									}
									else {
										// 64 bit
										sfmt[l++] = 'l';
										sfmt[l++] = 'l';
										sfmt[l++] = fmt[0];
										sfmt[l] = 0;
										if (w1 > 0)
											Sprintf(buf, _countof(buf), sfmt, w1, args[index]->i64);
										else
											Sprintf(buf, _countof(buf), sfmt, args[index]->i64);
									}
								}
									break;
								case 'n': {
#ifdef STR_USE_WIN32_NLS
                                    char buf2[256];
                                    char *ptr=buf2;
                                    int size=_countof(buf2);
#else
									char *ptr = buf;
									int size = _countof(buf);
#endif
									switch (type) {
										case CFormatArg::TYPE_INT:
											Sprintf(ptr, size, "%d", args[index]->i);
											break;
										case CFormatArg::TYPE_UINT:
											Sprintf(ptr, size, "%u", args[index]->i);
											break;
										case CFormatArg::TYPE_INT64:
											Sprintf(ptr, size, "%lld", args[index]->i64);
											break;
										case CFormatArg::TYPE_UINT64:
											Sprintf(ptr, size, "%llu", args[index]->i64);
											break;
									}
#ifdef STR_USE_WIN32_NLS
                                    // use GetNumberFormat without the decimal part
                                    NUMBERFMTA numFmt;
                                    char sep1[10],sep2[10];
                                    CreateNumberFormat(numFmt,0,sep1,sep2);
                                    int res=GetNumberFormatA(LOCALE_USER_DEFAULT,0,buf2,&numFmt,buf,_countof(buf));
                                    if (!res) buf[0]=0;
#endif
								}
									break;
								case 'f':
									// format as file size
#ifdef STR_USE_WIN32_NLS
                                    if (type==CFormatArg::TYPE_INT || type==CFormatArg::TYPE_UINT)
                                        StrFormatByteSizeA((DWORD)args[index]->i,buf,_countof(buf));
                                    else
                                        StrFormatByteSize64A(args[index]->i64,buf,_countof(buf));
#else
								{
									int64_t size;
									if (type == CFormatArg::TYPE_INT || type == CFormatArg::TYPE_UINT)
										size = (unsigned int) args[index]->i;
									else
										size = args[index]->i64;

									if (size < 1024)
										Sprintf(buf, _countof(buf), "%I64u bytes", size);
									else {
										int div;
										const char *unit;
										if (size < 1024 * 1024)
											div = 1024, unit = "KB";
										else if (size < 1024 * 1024 * 1024)
											div = 1024 * 1024, unit = "MB";
										else
											div = 1024 * 1024 * 1024, unit = "GB";
										size = size * 100 / div;
										Sprintf(buf, _countof(buf), "%I64u.02%d %s", size / 100, (int) (size % 100));
									}
								}
#endif
									break;
								case 'k':
									// format as file size in KB
#ifdef STR_USE_WIN32_NLS
                                    if (type==CFormatArg::TYPE_INT || type==CFormatArg::TYPE_UINT)
                                        StrFormatKBSizeA(args[index]->i,buf,_countof(buf));
                                    else
                                        StrFormatKBSizeA(args[index]->i64,buf,_countof(buf));
#else
								{
									int64_t size;
									if (type == CFormatArg::TYPE_INT || type == CFormatArg::TYPE_UINT)
										size = (unsigned int) args[index]->i;
									else
										size = args[index]->i64;
									size = size * 100 / 1024;
									Sprintf(buf, _countof(buf), "%I64u.02%d KB", size / 100, (int) (size % 100));
								}
#endif
									break;
								case 't': {
									// format as time interval
#ifdef STR_USE_WIN32_NLS
                                    int digits=6;
                                    if (fmt[1]>='1' && fmt[1]<='6')
                                        digits=fmt[1]-'0';
                                    if (type==CFormatArg::TYPE_INT || type==CFormatArg::TYPE_UINT)
                                        StrFromTimeIntervalA(buf,_countof(buf),(DWORD)args[index]->i,digits);
                                    else
                                        StrFromTimeIntervalA(buf,_countof(buf),(DWORD)args[index]->i64,digits);
#else
									unsigned int sec;
									if (type == CFormatArg::TYPE_INT || type == CFormatArg::TYPE_UINT)
										sec = (unsigned int) (args[index]->i + 500) / 1000;
									else
										sec = (unsigned int) ((args[index]->i64 + 500) / 1000);

									unsigned int min = sec / 60;
									sec -= min * 60;
									unsigned int hr = min / 60;
									min -= hr * 60;
									if (hr > 0)
										Sprintf(buf, _countof(buf), "%d hr, %d min, %d sec", hr, min, sec);
									else if (min > 0)
										Sprintf(buf, _countof(buf), "%d min, %d sec", min, sec);
									else
										Sprintf(buf, _countof(buf), "%d sec", sec);
#endif
								}
									break;

								default:
									assert(0); // unknown format in fmt[0]
							}
							text = buf;
						}
							break;

							// float
						case CFormatArg::TYPE_DOUBLE: {
							if (!fmt)
								fmt = (char *) "f"; // the default format
							long digits = -1;
							if (fmt[1] == '*') {
								long index = atol(fmt + 2);
								assert(index >= 0 && index < argc); // the index of the digits argument is invalid
								assert(args[index]->type == CFormatArg::TYPE_INT || args[index]->type ==
																					CFormatArg::TYPE_UINT); // the digits argument must be integer value
								digits = args[index]->i;
							}
							else if (fmt[1] >= '0' && fmt[1] <= '9')
								digits = atol(fmt + 1);
							switch (fmt[0]) {
								case 'f':
								case 'e':
								case 'E':
								case 'g':
								case 'G': {
									// just use sprintf
									char sfmt[20] = "%";
									long l = 1;
									if (width > 0)
										sfmt[l++] = '*';
									if (digits >= 0) {
										sfmt[l++] = '.';
										sfmt[l++] = '*';
									}
									sfmt[l++] = fmt[0];
									if (width > 0) {
										long w1 = width;
										if (w1 > _countof(buf) - 1)
											w1 = _countof(buf) - 1;
										width -= w1; // right alignment up to 255 will be handled by sprintf
										if (digits >= 0)
											Sprintf(buf, _countof(buf), sfmt, w1, digits, args[index]->d);
										else
											Sprintf(buf, _countof(buf), sfmt, w1, args[index]->d);
									}
									else {
										if (digits >= 0)
											Sprintf(buf, _countof(buf), sfmt, digits, args[index]->d);
										else
											Sprintf(buf, _countof(buf), sfmt, args[index]->d);
									}
								}
									break;
								case '$': {
									// currency
#ifdef STR_USE_WIN32_NLS
                                    char buf2[256];
                                    Sprintf(buf2,_countof(buf2),"%f",args[index]->d);
                                    int res=GetCurrencyFormatA(LOCALE_USER_DEFAULT,0,buf2,NULL,buf,_countof(buf));
                                    if (!res) buf[0]=0;
#else
									Sprintf(buf, _countof(buf), "$%.2f", args[index]->d);
#endif
								}
									break;
								case 'n':
#ifdef STR_USE_WIN32_NLS
                                    {
                                        // use GetNumberFormat with custom fractional digits count
                                        char buf2[256];
                                        Sprintf(buf2,_countof(buf2),"%f",args[index]->d);
                                        if (digits>=0) {
                                            NUMBERFMTA numFmt;
                                            char sep1[10],sep2[10];
                                            CreateNumberFormat(numFmt,digits,sep1,sep2);
                                            int res=GetNumberFormatA(LOCALE_USER_DEFAULT,0,buf2,&numFmt,buf,_countof(buf));
                                            if (!res) buf[0]=0;
                                        }
                                        else {
                                            int res=GetNumberFormatA(LOCALE_USER_DEFAULT,0,buf2,NULL,buf,_countof(buf));
                                            if (!res) buf[0]=0;
                                        }
                                    }
#else
									if (digits >= 0)
										Sprintf(buf, _countof(buf), "%.*f", digits, args[index]->d);
									else
										Sprintf(buf, _countof(buf), "%.2f", args[index]->d);
#endif
									break;
								default:
									assert(0); // unknown format in fmt[0]
							}
							text = buf;
						}
							break;

							// string
						case CFormatArg::TYPE_STRING:
							text = args[index]->s;
							break;
						case CFormatArg::TYPE_WSTRING:
#ifdef STR_USE_WIN32_CONV
                            if (fmt) {
                                if (fmt[0]=='*') {
                                    int index=atol(fmt+1);
                                    assert(index>=0 && index<argc); // the index of the codepage argument is invalid
                                    assert(args[index]->type==CFormatArg::TYPE_INT || args[index]->type==CFormatArg::TYPE_UINT); // the codepage argument must be integer value
                                    codepage=(int)args[index]->i;
                                }
                                else
                                    codepage=atol(fmt);
                            }
#endif
							wtext = args[index]->ws;
							break;

							// date/time
						case CFormatArg::TYPE_TIME: {
							if (!fmt)
								fmt = (char *) "d"; // the default format
							char fmt0 = fmt[0];
							fmt++;
#ifdef STR_USE_WIN32_TIME
                            SYSTEMTIME local;
                            const SYSTEMTIME *time=args[index]->t;
                            if (fmt[0]=='l') {
                                fmt++;
                                SystemTimeToTzSpecificLocalTime(NULL,(SYSTEMTIME*)time,&local);
                                time=&local;
                            }
                            else if (fmt[0]=='f') {
                                fmt++;
                                FILETIME t1, t2;
                                SystemTimeToFileTime(time,&t1);
                                FileTimeToLocalFileTime(&t1,&t2);
                                FileTimeToSystemTime(&t2,&local);
                                time=&local;
                            }
                            switch (fmt0) {
                                case 'd':
                                    if (!GetDateFormatA(LOCALE_USER_DEFAULT,fmt[0]?0:DATE_SHORTDATE,time,fmt[0]?fmt:NULL,buf,_countof(buf)))
                                        buf[0]=0;
                                    break;
                                case 'D':
                                    if (!GetDateFormatA(LOCALE_USER_DEFAULT,fmt[0]?0:DATE_LONGDATE,time,fmt[0]?fmt:NULL,buf,_countof(buf)))
                                        buf[0]=0;
                                    break;
                                case 't':
                                    if (!GetTimeFormatA(LOCALE_USER_DEFAULT,fmt[0]?0:TIME_NOSECONDS,time,fmt[0]?fmt:NULL,buf,_countof(buf)))
                                        buf[0]=0;
                                    break;
                                case 'T':
                                    if (!GetTimeFormatA(LOCALE_USER_DEFAULT,0,time,fmt[0]?fmt:NULL,buf,_countof(buf)))
                                        buf[0]=0;
                                    break;
                                default:
                                    assert(0); // unknown format in fmt[0]
                            }
#else

							const tm *time;
#if _MSC_VER >= 1400 // VC8.0
                            tm tm_time;
                            time=&tm_time;
#endif
							if (fmt[0] == 'l' || fmt[0] == 'f') {
								fmt++;
#if _MSC_VER >= 1400 // VC8.0
								localtime_s(&tm_time,&args[index]->t);
#else
								time = localtime(&args[index]->t);
#endif
							}
							else {
#if _MSC_VER >= 1400 // VC8.0
								gmtime_s(&tm_time,&args[index]->t);
#else
								time = gmtime(&args[index]->t);
#endif
							}
							if (time) {
								switch (fmt0) {
									case 'd':
										strftime(buf, _countof(buf), fmt[0] ? fmt : "%x", time);
										break;
									case 'D':
										strftime(buf, _countof(buf), fmt[0] ? fmt : "%#x", time);
										break;
									case 't':
										strftime(buf, _countof(buf), fmt[0] ? fmt : "%#I:%M %p", time);
										break;
									case 'T':
										strftime(buf, _countof(buf), fmt[0] ? fmt : "%#I:%M:%S %p", time);
										break;
									default:
										assert(0); // unknown format in fmt[0]
								}
							}
							else
								buf[0] = 0;
#endif
							text = buf;
						}
							break;
					}

					// length of the string
					long l1 = 0;
					if (text)
						l1 = Strlen(text);
					else if (wtext)
						l1 = WcsToMbs(NULL, 0, wtext CODEPAGE_ARG);

					// expand the buffer
					if (allocator) {
						long l2 = std::abs(width);
						if (l2 < l1) l2 = l1;
						if (i + l2 >= len) {
							if (!allocator->Realloc((void *&) string, i + l2 + ALLOC_GRANULARITY)) return -1;
							len = i + l2 + ALLOC_GRANULARITY;
						}
					}

					// leading space
					if (width > l1) {
						if (out) {
							// output to stream
							for (long c = l1; c < width; c++)
								if (!out->Output(" ", 1)) return -1;
						}
						else {
							// output to memory
							long i2 = len - (allocator ? 0 : 1);
							if (i2 > i + width - l1)
								i2 = i + width - l1;
							for (; i < i2; i++)
								string[i] = ' ';
						}
					}

					// output string
					if (l1 > 0) {
						if (text) {
							if (out) {
								// output to stream
								if (!out->Output(text, l1)) return -1;
							}
							else if (allocator || l1 < len - i) {
								// output to memory
								memcpy(string + i, text, l1);
								i += l1;
							}
							else
								return i + Strcpy(string + i, len - i, text); // limited space, use Strcpy and exit
						}
						else if (wtext) {
							// convert from unicode to ansi
							if (out) {
								// output to stream
								CAutoBuf<char> buf(l1 + 1); // temporary buffer for the conversion
								WcsToMbs(buf.GetData(), l1 + 1, wtext CODEPAGE_ARG);
								if (!out->Output(buf.GetData(), l1)) return -1;
							}
							else if (allocator || l1 < len - i) {
								// output to memory
								WcsToMbs(string + i, l1 + 1, wtext CODEPAGE_ARG);
								i += l1;
							}
							else
								return i + WcsToMbs(string + i, len - i, wtext
													CODEPAGE_ARG); // limited space, use WcsToMbs and exit
						}
					}

					// trailing space
					if (width < 0) {
						width = -width;
						if (out) {
							// output to stream
							for (long c = l1; c < width; c++)
								if (!out->Output(" ", 1)) return -1;
						}
						else {
							// output to memory
							long i2 = len - (allocator ? 0 : 1);
							if (i2 > i + width - l1)
								i2 = i + width - l1;
							for (; i < i2; i++)
								string[i] = ' ';
						}
					}

					continue;
				}
			}
			else if (*format == '}') {
				format++;
				if (*format != '}')
					continue;
			}

			// the character in *format is not part of a formatting token
#ifdef STR_USE_WIN32_DBCS
			if (GetMaxCharSize()==2 && IsDBCSLeadByte((BYTE)*format)) {
#else
			if (GetMaxCharSize() == 2 && isleadbyte((unsigned char) *format)) {
#endif
				// two bytes
				if (out) {
					// output to stream
					if (!out->Output(format, 2)) return -1;
					format += 2;
				}
				else {
					// output to memory
					if (!allocator && i > len - 3) {
						string[i] = 0; // the string is full, exit now
						return i;
					}
					else if (allocator && i > len - 2) {
						// expand the buffer
						len += ALLOC_GRANULARITY;
						if (!allocator->Realloc((void *&) string, len)) return -1;
					}
					string[i++] = *format++;
					string[i++] = *format++;
				}
			}
			else {
				// one byte
				if (out) {
					// output to stream
					if (!out->Output(format, 1)) return -1;
					format++;
				}
				else {
					// output to memory
					if (!allocator && i > len - 2) {
						string[i] = 0; // the string is full, exit now
						return i;
					}
					else if (allocator && i > len - 1) {
						// expand the buffer
						len += ALLOC_GRANULARITY;
						if (!allocator->Realloc((void *&) string, len)) return -1;
					}
					string[i++] = *format++;
				}
			}
		}

		// done with the string
		if (out) return i; // outputting to stream, just quit

		if (i == len) {
			assert(allocator); // should never come here for fixed size buffers (i<len always)
			if (allocator) {
				// allocate extra space for the trailing 0
				if (!allocator->Realloc((void *&) string, len + 1)) return -1;
				len++;
			}
			else
				i--;
		}

		// terminate the string
		string[i] = 0;
		// trim the buffer
		if (len > i + 1 && allocator && !allocator->Realloc((void *&) string, i + 1))
			return -1;
		return i;
	}

// Formats a string in a fixed size buffer. The result is always zero-terminated.
// The return value is the number of characters in the output (excluding the zero terminator)
	long FormatString(char *string, long len, const char *format, FORMAT_STRING_ARGS_CPP) {
		assert(len > 0); // the buffer is empty
		if (len == 0) return 0;
		if (len == 1) {
			string[0] = 0;
			return 0;
		}
		const CFormatArg *args[MAX_FORMAT_ARGUMENTS] = {&arg1, &arg2, &arg3, &arg4, &arg5, &arg6, &arg7, &arg8, &arg9,
														&arg10};
		int argc;
		for (argc = 0; argc < MAX_FORMAT_ARGUMENTS; argc++)
			if (args[argc]->type == CFormatArg::TYPE_NONE)
				break;
		return FormatStringInternal(string, len, NULL, NULL, format, argc, args);
	}

// Formats a string in a dynamically allocated buffer. The result is always zero-terminated.
// If allocation of the buffer fails the result is NULL.
	char *FormatStringAlloc(CFormatStringAllocator &allocator, const char *format, FORMAT_STRING_ARGS_CPP) {
		const CFormatArg *args[MAX_FORMAT_ARGUMENTS] = {&arg1, &arg2, &arg3, &arg4, &arg5, &arg6, &arg7, &arg8, &arg9,
														&arg10};
		int argc;
		for (argc = 0; argc < MAX_FORMAT_ARGUMENTS; argc++)
			if (args[argc]->type == CFormatArg::TYPE_NONE)
				break;
		char *string = NULL;
		if (FormatStringInternal(string, 0, &allocator, NULL, format, argc, args) < 0) return NULL;
		return string;
	}

// Formats a string and outputs it to a stream. If the output fails returns false
	bool FormatStringOut(CFormatStringOutA &out, const char *format, FORMAT_STRING_ARGS_CPP) {
		const CFormatArg *args[MAX_FORMAT_ARGUMENTS] = {&arg1, &arg2, &arg3, &arg4, &arg5, &arg6, &arg7, &arg8, &arg9,
														&arg10};
		int argc;
		for (argc = 0; argc < MAX_FORMAT_ARGUMENTS; argc++)
			if (args[argc]->type == CFormatArg::TYPE_NONE)
				break;
		char *string = NULL;
		if (FormatStringInternal(string, 0, NULL, &out, format, argc, args) < 0) return false;
		return true;
	}

///////////////////////////////////////////////////////////////////////////////
// UNICODE FormatString

#ifdef STR_USE_WIN32_NLS
    // Creates a number format based on the system settings but with user-defined number of digits.
    // sep1 and sep2 must point to buffers to contain the decimal and thousand separators
    static void CreateNumberFormat( NUMBERFMTW &numFmt, int digits, wchar_t *sep1, wchar_t *sep2 )
    {
        numFmt.NumDigits=digits;
        GetLocaleInfoW(LOCALE_USER_DEFAULT,LOCALE_SDECIMAL,sep1,10);
        numFmt.lpDecimalSep=sep1;
        GetLocaleInfoW(LOCALE_USER_DEFAULT,LOCALE_STHOUSAND,sep2,10);
        numFmt.lpThousandSep=sep2;
        wchar_t buf[10];
        GetLocaleInfoW(LOCALE_USER_DEFAULT,LOCALE_ILZERO,buf,10);
        numFmt.LeadingZero=std::wcstol(buf, NULL, 0);
        GetLocaleInfoW(LOCALE_USER_DEFAULT,LOCALE_INEGNUMBER,buf,10);
        numFmt.NegativeOrder=std::wcstol(buf, NULL, 0);
        GetLocaleInfoW(LOCALE_USER_DEFAULT,LOCALE_SGROUPING,buf,10);
        numFmt.Grouping=0;
        for (const wchar_t *p=buf;*p;p++)
            if (*p>='1' && *p<='9')
                numFmt.Grouping=numFmt.Grouping*10+(*p-'0');
    }
#endif

// Formats a string. The function can work in 3 modes - output to stream, output to fixed size buffer
// or output to dynamically allocated buffer. Returns the length of the result, or -1 if error
	long FormatStringInternal(wchar_t *&string, long len, CFormatStringAllocator *allocator, CFormatStringOutW *out,
							  const wchar_t *format, int argc, const CFormatArg *const args[]) {
		// no arguments, directly output the format string
		if (argc == 0) {
			long l = Strlen(format);
			if (out) {
				// output to stream
				return out->Output(format, l) ? l : -1;
			}

			// output to memory
			if (allocator) {
				// allocate buffer
				if (!allocator->Realloc((void *&) string, (l + 1) * sizeof(wchar_t))) return -1;
			}
			else {
				// limit the size
				if (l >= len) {
					// don't end on a leading surrogate
					if (format[len - 2] >= 0xDC00 && format[len - 2] <= 0xDFFF)
						l = len - 2;
					else
						l = len - 1;
				}
			}
			memcpy(string, format, l * sizeof(wchar_t));
			string[l] = 0;
			return l;
		}

		long i = 0;
		while (*format) {
			if (*format == L'{') {
				format++;
				if (*format != L'{') {
					wchar_t token[256];
					{
						const wchar_t *end = wcschr(format, L'}');
						if (!end) break;
						long l = end - format;
						if (l >= _countof(token)) break;
						memcpy(token, format, l * sizeof(wchar_t));
						token[l] = 0;
						format = end + 1;
					}

					wchar_t *end2;
					long index = wcstol(token, &end2, 10);
					assert(index >= 0 && index < argc); // the index of the argument is invalid
					long width = 0;
					if (*end2 == L',') {
						if (end2[1] != '*') {
							width = wcstol(end2 + 1, &end2, 10);
						}
						else {
							width = wcstol(end2 + 2, &end2, 10);
							assert(width >= 0 && width < argc); // the index of the width argument is invalid
							assert(args[width]->type == CFormatArg::TYPE_INT || args[width]->type ==
																				CFormatArg::TYPE_UINT); // the width argument must be integer value
							width = (int) args[width]->i;
						}
					}
					wchar_t *fmt = NULL;
					if (*end2 == L':') {
						fmt = end2 + 1;
						end2 = wcschr(fmt, L'@');
						if (end2) *end2 = 0;
					}

					wchar_t buf[256];
					buf[0] = 0;
					int type = args[index]->type;
					const char *text = NULL; // ansi string parameter
					const wchar_t *wtext = NULL; // unicode string parameter
#ifdef STR_USE_WIN32_CONV
					int codepage=CP_ACP;
#endif

					switch (type) {
						// integer
						case CFormatArg::TYPE_INT:
						case CFormatArg::TYPE_UINT:
						case CFormatArg::TYPE_INT64:
						case CFormatArg::TYPE_UINT64: {
							if (!fmt) {
								if (type == CFormatArg::TYPE_INT || type == CFormatArg::TYPE_INT64)
									fmt = (wchar_t *) L"d"; // default for int
								else
									fmt = (wchar_t *) L"u"; // default for unsigned int
							}
							switch (fmt[0]) {
								case L'c':
									buf[0] = (wchar_t) args[index]->i;
									buf[1] = 0;
									break;
								case L'd':
								case L'u':
								case L'x':
								case L'X': {
									// just use sprintf
									long w1 = width;
									if (w1 > _countof(buf) - 1)
										w1 = _countof(buf) - 1;
									if (width > 0) width -= w1; // right alignment up to 255 will be handled by sprintf

									bool plus = false;
									const wchar_t *fmt2 = fmt + 1;
									if (fmt[0] == L'd' && fmt2[0] == L'+')
										plus = true, fmt2++;
									bool zero = (w1 > 0 && fmt2[0] == L'0');
									wchar_t sfmt[20] = L"%";
									int l = 1;
									if (plus) sfmt[l++] = L'+';
									if (zero) sfmt[l++] = L'0';
									if (w1 > 0) sfmt[l++] = L'*';
									if (type == CFormatArg::TYPE_INT || type == CFormatArg::TYPE_UINT) {
										// 32 bit
										sfmt[l++] = fmt[0];
										sfmt[l] = 0;
										if (w1 > 0)
											Sprintf(buf, _countof(buf), sfmt, w1, args[index]->i);
										else
											Sprintf(buf, _countof(buf), sfmt, args[index]->i);
									}
									else {
										// 64 bit
										sfmt[l++] = L'l';
										sfmt[l++] = L'l';
										sfmt[l++] = fmt[0];
										sfmt[l] = 0;
										if (w1 > 0)
											Sprintf(buf, _countof(buf), sfmt, w1, args[index]->i64);
										else
											Sprintf(buf, _countof(buf), sfmt, args[index]->i64);
									}
								}
									break;
								case L'n': {
#ifdef STR_USE_WIN32_NLS
                                    wchar_t buf2[256];
                                    wchar_t *ptr=buf2;
                                    int size=_countof(buf2);
#else
									wchar_t *ptr = buf;
									int size = _countof(buf);
#endif
									switch (type) {
										case CFormatArg::TYPE_INT:
											Sprintf(ptr, size, L"%d", args[index]->i);
											break;
										case CFormatArg::TYPE_UINT:
											Sprintf(ptr, size, L"%u", args[index]->i);
											break;
										case CFormatArg::TYPE_INT64:
											Sprintf(ptr, size, L"%lld", args[index]->i64);
											break;
										case CFormatArg::TYPE_UINT64:
											Sprintf(ptr, size, L"%llu", args[index]->i64);
											break;
									}
#ifdef STR_USE_WIN32_NLS
                                    // use GetNumberFormat without the decimal part
                                    NUMBERFMTW numFmt;
                                    wchar_t sep1[10],sep2[10];
                                    CreateNumberFormat(numFmt,0,sep1,sep2);
                                    int res=GetNumberFormatW(LOCALE_USER_DEFAULT,0,buf2,&numFmt,buf,_countof(buf));
                                    if (!res) buf[0]=0;
#endif
								}
									break;
								case L'f':
									// format as file size
#ifdef STR_USE_WIN32_NLS
                                    if (type==CFormatArg::TYPE_INT || type==CFormatArg::TYPE_UINT)
                                        StrFormatByteSizeW(args[index]->i,buf,_countof(buf));
                                    else
                                        StrFormatByteSizeW(args[index]->i64,buf,_countof(buf));
#else
								{
									int64_t size;
									if (type == CFormatArg::TYPE_INT || type == CFormatArg::TYPE_UINT)
										size = (unsigned int) args[index]->i;
									else
										size = args[index]->i64;

									if (size < 1024)
										Sprintf(buf, _countof(buf), L"%I64u bytes", size);
									else {
										int div;
										const wchar_t *unit;
										if (size < 1024 * 1024)
											div = 1024, unit = L"KB";
										else if (size < 1024 * 1024 * 1024)
											div = 1024 * 1024, unit = L"MB";
										else
											div = 1024 * 1024 * 1024, unit = L"GB";
										size = size * 100 / div;
										Sprintf(buf, _countof(buf), L"%I64u.02%d %s", size / 100, (int) (size % 100));
									}
								}
#endif
									break;
								case L'k':
									// format as file size in KB
#ifdef STR_USE_WIN32_NLS
                                    if (type==CFormatArg::TYPE_INT || type==CFormatArg::TYPE_UINT)
                                        StrFormatKBSizeW(args[index]->i,buf,_countof(buf));
                                    else
                                        StrFormatKBSizeW(args[index]->i64,buf,_countof(buf));
#else
								{
									int64_t size;
									if (type == CFormatArg::TYPE_INT || type == CFormatArg::TYPE_UINT)
										size = (unsigned int) args[index]->i;
									else
										size = args[index]->i64;
									size = size * 100 / 1024;
									Sprintf(buf, _countof(buf), L"%I64u.02%d KB", size / 100, (int) (size % 100));
								}
#endif
									break;
								case L't': {
									// format as time interval
#ifdef STR_USE_WIN32_NLS
                                    int digits=6;
                                    if (fmt[1]>=L'1' && fmt[1]<=L'6')
                                        digits=fmt[1]-L'0';
                                    if (type==CFormatArg::TYPE_INT || type==CFormatArg::TYPE_UINT)
                                        StrFromTimeIntervalW(buf,_countof(buf),(DWORD)args[index]->i,digits);
                                    else
                                        StrFromTimeIntervalW(buf,_countof(buf),(DWORD)args[index]->i64,digits);
#else
									unsigned int sec;
									if (type == CFormatArg::TYPE_INT || type == CFormatArg::TYPE_UINT)
										sec = (unsigned int) (args[index]->i + 500) / 1000;
									else
										sec = (unsigned int) ((args[index]->i64 + 500) / 1000);

									unsigned int min = sec / 60;
									sec -= min * 60;
									unsigned int hr = min / 60;
									min -= hr * 60;
									if (hr > 0)
										Sprintf(buf, _countof(buf), L"%d hr, %d min, %d sec", hr, min, sec);
									else if (min > 0)
										Sprintf(buf, _countof(buf), L"%d min, %d sec", min, sec);
									else
										Sprintf(buf, _countof(buf), L"%d sec", sec);
#endif
								}
									break;
								default:
									assert(0); // unknown format in fmt[0]
							}
							wtext = buf;
						}
							break;

							// float
						case CFormatArg::TYPE_DOUBLE: {
							if (!fmt)
								fmt = (wchar_t *) L"f"; // the default format
							long digits = -1;
							if (fmt[1] == '*') {
								long index = std::wcstol(fmt + 2, NULL, 0);
								assert(index >= 0 && index < argc); // the index of the digits argument is invalid
								assert(args[index]->type == CFormatArg::TYPE_INT || args[index]->type ==
																					CFormatArg::TYPE_UINT); // the digits argument must be integer value
								digits = (int) args[index]->i;
							}
							else if (fmt[1] >= L'0' && fmt[1] <= L'9')
								digits = std::wcstol(fmt + 1, NULL, 0);
							switch (fmt[0]) {
								case L'f':
								case L'e':
								case L'E':
								case L'g':
								case L'G': {
									// just use sprintf
									wchar_t sfmt[20] = L"%";
									int l = 1;
									if (width > 0)
										sfmt[l++] = L'*';
									if (digits >= 0) {
										sfmt[l++] = L'.';
										sfmt[l++] = L'*';
									}
									sfmt[l++] = fmt[0];
									if (width > 0) {
										long w1 = width;
										if (w1 > _countof(buf) - 1)
											w1 = _countof(buf) - 1;
										width -= w1; // right alignment up to 255 will be handled by sprintf
										if (digits >= 0)
											Sprintf(buf, _countof(buf), sfmt, w1, digits, args[index]->d);
										else
											Sprintf(buf, _countof(buf), sfmt, w1, args[index]->d);
									}
									else {
										if (digits >= 0)
											Sprintf(buf, _countof(buf), sfmt, digits, args[index]->d);
										else
											Sprintf(buf, _countof(buf), sfmt, args[index]->d);
									}
								}
									break;
								case L'$': {
									// currency
#ifdef STR_USE_WIN32_NLS
                                    wchar_t buf2[256];
                                    Sprintf(buf2,_countof(buf2),L"%f",args[index]->d);
                                    int res=GetCurrencyFormatW(LOCALE_USER_DEFAULT,0,buf2,NULL,buf,_countof(buf));
                                    if (!res) buf[0]=0;
#else
									Sprintf(buf, _countof(buf), L"$%.2f", args[index]->d);
#endif
								}
									break;
								case L'n':
#ifdef STR_USE_WIN32_NLS
                                    {
                                        // use GetNumberFormat with custom fractional digits count
                                        wchar_t buf2[256];
                                        Sprintf(buf2,_countof(buf2),L"%f",args[index]->d);
                                        if (digits>=0) {
                                            NUMBERFMTW numFmt;
                                            wchar_t sep1[10],sep2[10];
                                            CreateNumberFormat(numFmt,digits,sep1,sep2);
                                            int res=GetNumberFormatW(LOCALE_USER_DEFAULT,0,buf2,&numFmt,buf,_countof(buf));
                                            if (!res) buf[0]=0;
                                        }
                                        else {
                                            int res=GetNumberFormatW(LOCALE_USER_DEFAULT,0,buf2,NULL,buf,_countof(buf));
                                            if (!res) buf[0]=0;
                                        }
                                    }
#else
									if (digits >= 0)
										Sprintf(buf, _countof(buf), L"%.*f", digits, args[index]->d);
									else
										Sprintf(buf, _countof(buf), L"%.2f", args[index]->d);
#endif
									break;
								default:
									assert(0); // unknown format in fmt[0]
							}
							wtext = buf;
						}
							break;

							// string
						case CFormatArg::TYPE_STRING:
#ifdef STR_USE_WIN32_CONV
                            if (fmt) {
                                if (fmt[0]=='*') {
                                    long index=std::wcstol(fmt+1, NULL, 0);
                                    assert(index>=0 && index<argc); // the index of the codepage argument is invalid
                                    assert(args[index]->type==CFormatArg::TYPE_INT || args[index]->type==CFormatArg::TYPE_UINT); // the codepage argument must be integer value
                                    codepage=(int)args[index]->i;
                                }
                                else
                                    codepage=std::wcstol(fmt, NULL, 0);
                            }
#endif
							text = args[index]->s;
							break;
						case CFormatArg::TYPE_WSTRING:
							wtext = args[index]->ws;
							break;

							// date/time
						case CFormatArg::TYPE_TIME: {
							if (!fmt)
								fmt = (wchar_t *) L"d"; // the default format
							wchar_t fmt0 = fmt[0];
							fmt++;
#ifdef STR_USE_WIN32_TIME
                            SYSTEMTIME local;
                            const SYSTEMTIME *time=args[index]->t;
                            if (fmt[0]==L'l') {
                                fmt++;
                                SystemTimeToTzSpecificLocalTime(NULL,(SYSTEMTIME*)time,&local);
                                time=&local;
                            }
                            else if (fmt[0]=='f') {
                                fmt++;
                                FILETIME t1, t2;
                                SystemTimeToFileTime(time,&t1);
                                FileTimeToLocalFileTime(&t1,&t2);
                                FileTimeToSystemTime(&t2,&local);
                                time=&local;
                            }
                            switch (fmt0) {
                                case L'd':
                                    if (!GetDateFormatW(LOCALE_USER_DEFAULT,fmt[0]?0:DATE_SHORTDATE,time,fmt[0]?fmt:NULL,buf,_countof(buf)))
                                        buf[0]=0;
                                    break;
                                case L'D':
                                    if (!GetDateFormatW(LOCALE_USER_DEFAULT,fmt[0]?0:DATE_LONGDATE,time,fmt[0]?fmt:NULL,buf,_countof(buf)))
                                        buf[0]=0;
                                    break;
                                case L't':
                                    if (!GetTimeFormatW(LOCALE_USER_DEFAULT,fmt[0]?0:TIME_NOSECONDS,time,fmt[0]?fmt:NULL,buf,_countof(buf)))
                                        buf[0]=0;
                                    break;
                                case L'T':
                                    if (!GetTimeFormatW(LOCALE_USER_DEFAULT,0,time,fmt[0]?fmt:NULL,buf,_countof(buf)))
                                        buf[0]=0;
                                    break;
                                default:
                                    assert(0); // unknown format in fmt[0]
                            }
#else
							const tm *time;
#if _MSC_VER >= 1400 // VC8.0
                            tm tm_time;
                            time=&tm_time;
#endif
							if (fmt[0] == 'l' || fmt[0] == 'f') {
								fmt++;
#if _MSC_VER >= 1400 // VC8.0
								localtime_s(&tm_time,&args[index]->t);
#else
								time = localtime(&args[index]->t);
#endif
							}
							else {
#if _MSC_VER >= 1400 // VC8.0
								gmtime_s(&tm_time,&args[index]->t);
#else
								time = gmtime(&args[index]->t);
#endif
							}
							if (time) {
								switch (fmt0) {
									case 'd':
										wcsftime(buf, _countof(buf), fmt[0] ? fmt : L"%x", time);
										break;
									case 'D':
										wcsftime(buf, _countof(buf), fmt[0] ? fmt : L"%#x", time);
										break;
									case 't':
										wcsftime(buf, _countof(buf), fmt[0] ? fmt : L"%#I:%M %p", time);
										break;
									case 'T':
										wcsftime(buf, _countof(buf), fmt[0] ? fmt : L"%#I:%M:%S %p", time);
										break;
									default:
										assert(0); // unknown format in fmt[0]
								}
							}
							else
								buf[0] = 0;
#endif
							wtext = buf;
						}
							break;
					}

					// length of the string
					long l1 = 0;
					if (text)
						l1 = MbsToWcs(NULL, 0, text);
					else if (wtext)
						l1 = Strlen(wtext);

					// expand the buffer
					if (allocator) {
						long l2 = std::abs(width);
						if (l2 < l1) l2 = l1;
						if (i + l2 >= len) {
							if (!allocator->Realloc((void *&) string,
													(i + l2 + ALLOC_GRANULARITY) * sizeof(wchar_t)))
								return -1;
							len = i + l2 + ALLOC_GRANULARITY;
						}
					}

					// leading space
					if (width > l1) {
						if (out) {
							// output to stream
							for (long c = l1; c < width; c++)
								if (!out->Output(L" ", 1)) return -1;
						}
						else {
							// output to memory
							long i2 = len - (allocator ? 0 : 1);
							if (i2 > i + width - l1)
								i2 = i + width - l1;
							for (; i < i2; i++)
								string[i] = L' ';
						}
					}

					// output string
					if (l1 > 0) {
						if (wtext) {
							if (out) {
								// output to stream
								if (!out->Output(wtext, l1)) return -1;
							}
							else if (allocator || l1 < len - i) {
								// output to memory
								memcpy(string + i, wtext, l1 * sizeof(wchar_t));
								i += l1;
							}
							else
								return i + Strcpy(string + i, len - i, wtext); // limited space, use Strcpy and exit
						}
						else if (text) {
							// convert from ansi to unicode
							if (out) {
								// output to stream
								CAutoBuf<wchar_t> buf(l1 + 1); // temporary buffer for the conversion
								MbsToWcs(buf.GetData(), l1 + 1, text CODEPAGE_ARG);
								if (!out->Output(buf.GetData(), l1)) return -1;
							}
							else if (allocator || l1 < len - i) {
								// output to memory
								MbsToWcs(string + i, l1 + 1, text CODEPAGE_ARG);
								i += l1;
							}
							else
								return i + MbsToWcs(string + i, len - i, text
													CODEPAGE_ARG); // limited space, use MbsToWcs and exit
						}
					}

					// trailing space
					if (width < 0) {
						width = -width;
						if (out) {
							// output to stream
							for (long c = l1; c < width; c++)
								if (!out->Output(L" ", 1)) return -1;
						}
						else {
							// output to memory
							long i2 = len - (allocator ? 0 : 1);
							if (i2 > i + width - l1)
								i2 = i + width - l1;
							for (; i < i2; i++)
								string[i] = L' ';
						}
					}

					continue;
				}
			}
			else if (*format == L'}') {
				format++;
				if (*format != L'}')
					continue;
			}

			// the character in *format is not part of a formatting token
			if (*format >= 0xD800 && *format <= 0xDBFF) {
				// two words
				if (out) {
					// output to stream
					if (!out->Output(format, 2)) return -1;
					format += 2;
				}
				else {
					// output to memory
					if (!allocator && i > len - 3) {
						string[i] = 0; // the string is full, exit now
						return i;
					}
					else if (allocator && i > len - 2) {
						// expand the buffer
						len += ALLOC_GRANULARITY;
						if (!allocator->Realloc((void *&) string, len * sizeof(wchar_t))) return -1;
					}
					string[i++] = *format++;
					string[i++] = *format++;
				}
			}
			else {
				// one word
				if (out) {
					// output to stream
					if (!out->Output(format, 1)) return -1;
					format++;
				}
				else {
					// output to memory
					if (!allocator && i > len - 2) {
						string[i] = 0; // the string is full, exit now
						return i;
					}
					else if (allocator && i > len - 1) {
						// expand the buffer
						len += ALLOC_GRANULARITY;
						if (!allocator->Realloc((void *&) string, len * sizeof(wchar_t))) return -1;
					}
					string[i++] = *format++;
				}
			}
		}

		// done with the string
		if (out) return i; // outputting to stream, just quit

		if (i == len) {
			assert(allocator); // should never come here for fixed size buffers (i<len always)
			if (allocator) {
				// allocate extra space for the trailing 0
				if (!allocator->Realloc((void *&) string, (len + 1) * sizeof(wchar_t))) return -1;
				len++;
			}
			else
				i--;
		}

		// terminate the string
		string[i] = 0;
		// trim the buffer
		if (len > i + 1 && allocator && !allocator->Realloc((void *&) string, (i + 1) * sizeof(wchar_t)))
			return -1;
		return i;
	}

// Formats a string in a fixed size buffer. The result is always zero-terminated.
// The return value is the number of characters in the output (excluding the zero terminator)
	long FormatString(wchar_t *string, long len, const wchar_t *format, FORMAT_STRING_ARGS_CPP) {
		assert(len > 0); // the buffer is empty
		if (len == 0) return 0;
		if (len == 1) {
			string[0] = 0;
			return 0;
		}
		const CFormatArg *args[MAX_FORMAT_ARGUMENTS] = {&arg1, &arg2, &arg3, &arg4, &arg5, &arg6, &arg7, &arg8, &arg9,
														&arg10};
		int argc;
		for (argc = 0; argc < MAX_FORMAT_ARGUMENTS; argc++)
			if (args[argc]->type == CFormatArg::TYPE_NONE)
				break;
		return FormatStringInternal(string, len, NULL, NULL, format, argc, args);
	}

// Formats a string in a dynamically allocated buffer. The result is always zero-terminated.
// If allocation of the buffer fails the result is NULL.
	wchar_t *FormatStringAlloc(CFormatStringAllocator &allocator, const wchar_t *format, FORMAT_STRING_ARGS_CPP) {
		const CFormatArg *args[MAX_FORMAT_ARGUMENTS] = {&arg1, &arg2, &arg3, &arg4, &arg5, &arg6, &arg7, &arg8, &arg9,
														&arg10};
		int argc;
		for (argc = 0; argc < MAX_FORMAT_ARGUMENTS; argc++)
			if (args[argc]->type == CFormatArg::TYPE_NONE)
				break;
		wchar_t *string = NULL;
		if (FormatStringInternal(string, 0, &allocator, NULL, format, argc, args) < 0) return NULL;
		return string;
	}

// Formats a string and outputs it to a stream. If the output fails returns false
	bool FormatStringOut(CFormatStringOutW &out, const wchar_t *format, FORMAT_STRING_ARGS_CPP) {
		const CFormatArg *args[MAX_FORMAT_ARGUMENTS] = {&arg1, &arg2, &arg3, &arg4, &arg5, &arg6, &arg7, &arg8, &arg9,
														&arg10};
		int argc;
		for (argc = 0; argc < MAX_FORMAT_ARGUMENTS; argc++)
			if (args[argc]->type == CFormatArg::TYPE_NONE)
				break;
		wchar_t *string = NULL;
		if (FormatStringInternal(string, 0, NULL, &out, format, argc, args) < 0) return false;
		return true;
	}

///////////////////////////////////////////////////////////////////////////////
// Time conversions

#ifdef STR_USE_WIN32_TIME
    // Converts time_t to FILETIME. Does not perform any timezone conversions
    static void UnixTimeToFileTime( time_t t, FILETIME &ft )
    {
        __int64 ll=((__int64)t)*10000000+116444736000000000;
        ft.dwLowDateTime=(DWORD)ll;
        ft.dwHighDateTime=(DWORD)(ll>>32);
    }

    CFormatTime::CFormatTime( time_t t )
        :CFormatArg(st)
    {
        FILETIME ft;
        UnixTimeToFileTime(t,ft);
        FileTimeToSystemTime(&ft,&st);
    }

    CFormatTime::CFormatTime( const FILETIME &t )
        :CFormatArg(st)
    {
        FileTimeToSystemTime(&t,&st);
    }

    CFormatTime::CFormatTime( DATE t )
        :CFormatArg(st)
    {
        VariantTimeToSystemTime(t,&st);
    }
#else

	CFormatTime::CFormatTime(time_t t) {
		type = TYPE_TIME;
		this->t = t;
	}

#endif

///////////////////////////////////////////////////////////////////////////////
// Default string allocator

	CFormatStringAllocator CFormatStringAllocator::g_DefaultAllocator;

	bool CFormatStringAllocator::Realloc(void *&ptr, long size) {
		void *res = realloc(ptr, size);
		if (ptr && !res) free(ptr);
		ptr = res;
		return res != NULL;
	}

///////////////////////////////////////////////////////////////////////////////
// Default string output

	CFormatStringOutA CFormatStringOutA::g_DefaultOut;
	CFormatStringOutW CFormatStringOutW::g_DefaultOut;

	bool CFormatStringOutA::Output(const char *text, long len) {
		for (long i = 0; i < len; i++)
			if (putchar(text[i]) == EOF) return false;
		return true;
	}

	bool CFormatStringOutW::Output(const wchar_t *text, long len) {
		for (long i = 0; i < len; i++)
			if (putwchar(text[i]) == WEOF) return false;
		return true;
	}

#ifdef STR_USE_STL

///////////////////////////////////////////////////////////////////////////////
// Output to STL strings

// ANSI
	class CStdStringAllocator : public CFormatStringAllocator {
	public:
		CStdStringAllocator(std::string &buf) : m_Buffer(buf) { }

		virtual bool Realloc(void *&ptr, long size);

	private:
		std::string &m_Buffer;

		void operator=(const CStdStringAllocator &alloc);
	};

	bool CStdStringAllocator::Realloc(void *&ptr, long size) {
		m_Buffer.resize(size);
		ptr = &m_Buffer[0];
		return true;
	}

	std::string FormatStdString(const char *format, FORMAT_STRING_ARGS_CPP) {
		std::string res;
		CStdStringAllocator alloc(res);
		FormatStringAlloc(alloc, format, FORMAT_STRING_ARGS_PASS);
		return res;
	}

	void FormatStdString(std::string &string, const char *format, FORMAT_STRING_ARGS_CPP) {
		string.erase();
		CStdStringAllocator alloc(string);
		FormatStringAlloc(alloc, format, FORMAT_STRING_ARGS_PASS);
	}

// UNICODE
	class CStdWStringAllocator : public CFormatStringAllocator {
	public:
		CStdWStringAllocator(std::wstring &buf) : m_Buffer(buf) { }

		virtual bool Realloc(void *&ptr, long size);

	private:
		std::wstring &m_Buffer;

		void operator=(const CStdWStringAllocator &alloc);
	};

	bool CStdWStringAllocator::Realloc(void *&ptr, long size) {
		m_Buffer.resize(size);
		ptr = &m_Buffer[0];
		return true;
	}

	std::wstring FormatStdString(const wchar_t *format, FORMAT_STRING_ARGS_CPP) {
		std::wstring res;
		CStdWStringAllocator alloc(res);
		FormatStringAlloc(alloc, format, FORMAT_STRING_ARGS_PASS);
		return res;
	}

	void FormatStdString(std::wstring &string, const wchar_t *format, FORMAT_STRING_ARGS_CPP) {
		string.erase();
		CStdWStringAllocator alloc(string);
		FormatStringAlloc(alloc, format, FORMAT_STRING_ARGS_PASS);
	}

///////////////////////////////////////////////////////////////////////////////
// Output to STL streams

// ANSI
	StdStreamOut::StdStreamOut(const char *format, FORMAT_STRING_ARGS_CPP) {
		m_FormatA = format;
		m_FormatW = NULL;
		m_Args[0] = &arg1;
		m_Args[1] = &arg2;
		m_Args[2] = &arg3;
		m_Args[3] = &arg4;
		m_Args[4] = &arg5;
		m_Args[5] = &arg6;
		m_Args[6] = &arg7;
		m_Args[7] = &arg8;
		m_Args[8] = &arg9;
		m_Args[9] = &arg10;
		for (m_Argc = 0; m_Argc < MAX_FORMAT_ARGUMENTS; m_Argc++)
			if (m_Args[m_Argc]->type == CFormatArg::TYPE_NONE)
				break;
	}

	class StdStringStreamA : public CFormatStringOutA {
	public:
		StdStringStreamA(std::ostream &stream) : m_Stream(stream) { }

		virtual bool Output(const char *text, long len) {
			m_Stream.write(text, len);
			return true;
		}

	private:
		std::ostream &m_Stream;

		void operator=(const StdStringStreamA &stream);
	};

	std::ostream &operator<<(std::ostream &stream, const StdStreamOut &format) {
		char *string = NULL;
		StdStringStreamA s(stream);
		assert(format.m_FormatA); // most likely using ANSI stream with UNICODE format
		FormatStringInternal(string, 0, NULL, &s, format.m_FormatA, format.m_Argc, format.m_Args);
		return stream;
	}

// UNICODE
	StdStreamOut::StdStreamOut(const wchar_t *format, FORMAT_STRING_ARGS_CPP) {
		m_FormatW = format;
		m_FormatA = NULL;
		m_Args[0] = &arg1;
		m_Args[1] = &arg2;
		m_Args[2] = &arg3;
		m_Args[3] = &arg4;
		m_Args[4] = &arg5;
		m_Args[5] = &arg6;
		m_Args[6] = &arg7;
		m_Args[7] = &arg8;
		m_Args[8] = &arg9;
		m_Args[9] = &arg10;
		for (m_Argc = 0; m_Argc < MAX_FORMAT_ARGUMENTS; m_Argc++)
			if (m_Args[m_Argc]->type == CFormatArg::TYPE_NONE)
				break;
	}

	class StdStringStreamW : public CFormatStringOutW {
	public:
		StdStringStreamW(std::wostream &stream) : m_Stream(stream) { }

		virtual bool Output(const wchar_t *text, long len) {
			m_Stream.write(text, len);
			return true;
		}

	private:
		std::wostream &m_Stream;

		void operator=(const StdStringStreamW &stream);
	};

	std::wostream &operator<<(std::wostream &stream, const StdStreamOut &format) {
		wchar_t *string = NULL;
		StdStringStreamW s(stream);
		assert(format.m_FormatW); // most likely using UNICODE stream with ANSI format
		FormatStringInternal(string, 0, NULL, &s, format.m_FormatW, format.m_Argc, format.m_Args);
		return stream;
	}

#endif
}