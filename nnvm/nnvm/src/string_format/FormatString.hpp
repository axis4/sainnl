// StringUtils is also available under the CPOL license as part of the FormatString article
// on CodeProject: http://www.codeproject.com/KB/string/FormatString.aspx

#ifndef _FORMATSTRING_H
#define _FORMATSTRING_H

// FormatString - writes formatted data to string. The result can be ANSI or UNICODE string.
// Can be used with up to MAX_FORMAT_ARGUMENTS arguments If no arguments are given, the format string is
// copied directly to the output string.
// The arguments can be integer, floating point, ANSI string, UNICODE string, pointer,
// SYSTEMTIME or CFormatArg. In the format string '{' must be escaped as '{{' and '}' must be escaped as '}}'.
// The format string contains format specifications for the arguments in the pattern:
// {index[,width][:format][@comment]}
// index is the zero-based index of the argument. The same argument can be used more than once
// width - width of the string. >0 will be right-aligned, <0 will be left-aligned
//   The width can be in the format *<index>, then the argument with that index is used as width. The
//     argument must be integer.
// comment - ignored
// format - specifies the format of the string. depends on the argument:
//
// If the argument is 8, 16, 32 or 64 bit integer the format can be:
//   c - character
//   d+0 - signed integer. + - force + sign for positive numbers. 0 - add leading zeros
//   u0 - unsigned integer. 0 - add leading zeros
//   x0 - lower case hex integer. 0 - add leading zeros
//   X0 - upper case hex integer. 0 - add leading zeros
//   n - localized number - uses GetNumberFormat with no fractional digits
//   f - localized file size - uses StrFormatByteSize
//   k - localized file size in KB - uses StrFormatKBSize
//   t# - localized time interval in ms - uses StrFromTimeInterval, # number if significant digits
// 'd' is used by default for signed values and 'u' for unsigned values.
//
// If the argument is float or double the format can be:
//   f# - fixed point. # - number of fractional digits
//   f*# - fixed point. # - index of the argument used as the number of fractional digits
//   e/E# - exponential format. # - number of fractional digits
//   e/E*# - exponential format. # - index of the argument used as the number of fractional digits
//   g/G# - uses either f or e/E, whichever is shorter
//   g/G*# - uses either f or e/E, whichever is shorter
//   $ - localized currency - uses GetCurrencyFormat
//   n# - localized number - uses GetNumberFormat with # fractional digits
//   n*# - localized number - uses GetNumberFormat. # - index of the argument used as the number of fractional digits
// 'f' is used by default.
//
// If the argument is ANSI or UNICODE string the format is the codepage used to convert the string. It
// is only used when ANSI string is given to the UNICODE version of FormatString, or vice versa:
//   # - codepage number
//   *# - index of the argument used as the codepage number
//
// If the argument is SYSTEMTIME the format can be:
//   dl<format> - short date format. l - convert from UTC to local time. <format> optional format passed to GetDateFormat
//   Dl<format> - long date format. l - convert from UTC to local time. <format> optional format passed to GetDateFormat
//   tl<format> - time format, no seconds. l - convert from UTC to local time. <format> optional format passed to GetTimeFormat
//   Tl<format> - time format. l - convert from UTC to local time. <format> optional format passed to GetTimeFormat
//   df, Df, tf, Tf - convert from UTC to local time using the file time rules.
// 'd' is used by default.
//
// To define other functions that use FromatString use FORMAT_STRING_ARGS_H, FORMAT_STRING_ARGS_CPP
// and FORMAT_STRING_ARGS_PASS.
// FORMAT_STRING_ARGS_H is to be used with the function declaration. It will define default values for the parameters.
// FORMAT_STRING_ARGS_CPP is to be used with the function definition.
// FORMAT_STRING_ARGS_PASS is to be used when passing arguments to another function using FORMAT_STRING_ARGS_H.
//
//
// CFormatTime - class to help using FILETIME, time_t or DATE in FormatString. For example:
//   FormatString(s,20,"local time: {0:dl}  {0:tl}",CFormatTime(time())); -> prints the current date/time
//
///////////////////////////////////////////////////////////////////////////////

#include "StringUtils.hpp"
#include <ctime>
#include <unicode/unistr.h>
#include "../../include/StringUtils.hpp"
#include "../../include/StringConv.hpp"

namespace nnvm {
///////////////////////////////////////////////////////////////////////////////
// Arguments

// Formats a string in a fixed size buffer. The result is always zero-terminated.
// The return value is the number of characters in the output (excluding the zero terminator)
	long FormatString(char *string, long len, const char *format, FORMAT_STRING_ARGS_H);

	long FormatString(wchar_t *string, long len, const wchar_t *format, FORMAT_STRING_ARGS_H);

///////////////////////////////////////////////////////////////////////////////

	class CFormatStringAllocator {
	public:
		// FormatStringAlloc calls Realloc to create and grow the output buffer.
		// Realloc must reallocate the ptr buffer to the new size (in bytes) and return true.
		// If the reallocation fails Realloc must free the old buffer and return false.
		// The default implementation just calls the system realloc.
		virtual bool Realloc(void *&ptr, long size);

		static CFormatStringAllocator g_DefaultAllocator;
	};

// Formats a string in a dynamically allocated buffer. The result is always zero-terminated.
// If allocation of the buffer fails the result is NULL.
	char *FormatStringAlloc(CFormatStringAllocator &allocator, const char *format, FORMAT_STRING_ARGS_H);

	wchar_t *FormatStringAlloc(CFormatStringAllocator &allocator, const wchar_t *format, FORMAT_STRING_ARGS_H);

///////////////////////////////////////////////////////////////////////////////

	class CFormatStringOutA {
	public:
		// FormatStringOut calls Output multiple times to output the result string.
		// If Output fails it must return false. Then FormatStringOut will also return false
		// The default implementation just calls puts
		virtual bool Output(const char *text, long len);

		static CFormatStringOutA g_DefaultOut;
	};

	class CFormatStringOutW {
	public:
		// FormatStringOut calls Output multiple times to output the result string.
		// If Output fails it must return false. Then FormatStringOut will also return false
		// The default implementation just calls puts
		virtual bool Output(const wchar_t *text, long len);

		static CFormatStringOutW g_DefaultOut;
	};

// These functions format a string and output it.
	bool FormatStringOut(CFormatStringOutA &out, const char *format, FORMAT_STRING_ARGS_H);

	bool FormatStringOut(CFormatStringOutW &out, const wchar_t *format, FORMAT_STRING_ARGS_H);

///////////////////////////////////////////////////////////////////////////////
// Support for different date/time formats

	class CFormatTime : public CFormatArg {
	public:
		CFormatTime(time_t t);

#ifdef STR_USE_WIN32_TIME
        CFormatTime( const FILETIME &t );
        CFormatTime( DATE t );

    private:
        SYSTEMTIME st;
#endif
	};

///////////////////////////////////////////////////////////////////////////////
// STL support

#ifdef STR_USE_STL

// Formats a string and puts the result in a std::string or std::wstring
	std::string FormatStdString(const char *format, FORMAT_STRING_ARGS_H);

	std::wstring FormatStdString(const wchar_t *format, FORMAT_STRING_ARGS_H);

	void FormatStdString(std::string &string, const char *format, FORMAT_STRING_ARGS_H);

	void FormatStdString(std::wstring &string, const wchar_t *format, FORMAT_STRING_ARGS_H);

// Outputs a formatted string to a std::ostream or std::wstream. Use it like this:
// stream << StdStreamOut(format, parameters) << ...;
	class StdStreamOut {
	public:
		StdStreamOut(const char *format, FORMAT_STRING_ARGS_H);

		StdStreamOut(const wchar_t *format, FORMAT_STRING_ARGS_H);

	private:
		const char *m_FormatA;
		const wchar_t *m_FormatW;
		const CFormatArg *m_Args[MAX_FORMAT_ARGUMENTS];
		int m_Argc;

		friend std::ostream &operator<<(std::ostream &stream, const StdStreamOut &format);

		friend std::wostream &operator<<(std::wostream &stream, const StdStreamOut &format);
	};

	std::ostream &operator<<(std::ostream &stream, const StdStreamOut &format);

	std::wostream &operator<<(std::wostream &stream, const StdStreamOut &format);

#endif
}

#endif
