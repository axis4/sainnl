// StringUtils is also available under the CPOL license as part of the FormatString article
// on CodeProject: http://www.codeproject.com/KB/string/FormatString.aspx

#ifndef _STRINGUTILS_H
#define _STRINGUTILS_H

// StringUtils provides secure implementations of common string functions.
// All strings are guaranteed to be zero-terminated and to not contain partial
// double-byte characters or partial surrogate pairs.
// It also provides char and wchar_t overrides of the same functions.
//
///////////////////////////////////////////////////////////////////////////////

// Configuration of the string functions

#if defined(_WIN32) || defined(_WIN64)
// If defined, enables the use of WideCharToMultiByte and MultiByteToWideChar
// If not defined, wcstombs and mbstowcs are used instead. they always use the default code page
// and don't support surrogate pairs
#define STR_USE_WIN32_CONV

// If defined, enables the use of Windows localization functions - GetNumberFormat, GetTimeFormat, etc
// If not defined, then the numbers, currency, time and date use fixed formats
#define STR_USE_WIN32_NLS

// If defined, enables support for SYSTEMTIME, FILETIME and DATE
// If not defined, only time_t is supported as a time format
#define STR_USE_WIN32_TIME

// If defined, enables the use of IsDBCSLeadByte to support DBCS code pages
// If not defined, isleadbyte is used instead
#define STR_USE_WIN32_DBCS
#else
#ifndef _LEADBYTE
#define _LEADBYTE 0x8000
#define isleadbyte(_c) ((_c) & _LEADBYTE)
#endif
#endif

// enables support for STL strings and streams
#define STR_USE_STL

#include <cstring>
#include <cstdarg>

#if defined(_WIN32) || defined(_WIN64)
#include <windows.h>
#endif

#ifdef STR_USE_STL
#include <string>
#endif

// _countof: compute the number of elements in a statically-allocated array
// VS2005 supports this, but earlier versions do not
#ifndef _countof
#define _countof(x) (sizeof(x)/sizeof((x)[0]))
#endif

// Returns 1 if the ANSI code page is single-byte, or 2 if it is double-byte (DBCS)
long GetMaxCharSize( );

// Returns the length of a string
inline long Strlen( const char *str ) { return strlen(str); }
inline long Strlen( const wchar_t *str ) { return wcslen(str); }

// Copies src to dst. size is the size of dst in characters, including the terminating 0.
// Returns the number of characters copied, excluding the terminating 0.
// The return value is <=size-1. If size is 0, returns 0 and does nothing.
// The result in dst is always 0 terminated.
long Strcpy( char *dst, long size, const char *src );
long Strcpy( wchar_t *dst, long size, const wchar_t *src );

// Copies src to dst. size is the size of dst in characters, including the terminating 0.
// Copies up to len characters and always appends terminating 0.
long Strncpy( char *dst, long size, const char *src, long len );
long Strncpy( wchar_t *dst, long size, const wchar_t *src, long len );

// Appends src to dst. size is the size of dst in bytes, including the terminating 0.
// Returns the number of characters copied, excluding the terminating 0.
// The return value is <=size-1-strlen(dst). If size>=strlen(dst), returns 0 and does nothing.
// The result in dst is always 0 terminated.
long Strcat( char *dst, long size, const char *src );
long Strcat( wchar_t *dst, long size, const wchar_t *src );

// Writes formatted string to dst. size is the size of dst in characters, including the terminating 0.
// Returns the number of characters written, excluding the terminating 0.
// The return value is <=size-1. If size is 0, returns 0 and does nothing.
// The result in dst is always 0 terminated.
long Sprintf( char *dst, long size, const char *format, ... );
long Sprintf( wchar_t *dst, long size, const wchar_t *format, ... );
long Vsprintf( char *dst, long size, const char *format, va_list args );
long Vsprintf( wchar_t *dst, long size, const wchar_t *format, va_list args );

// Converts between multi-byte and wide characters. size is the size of dst in characters, including the
// terminating 0.
// Returns the number of characters copied, excluding the terminating 0.
// The return value is <=size-1. If size is 0, returns 0 and does nothing.
// The result in dst is always 0 terminated.
// If dst is NULL the size is ignored and the function just returns the number of characters (not counting the 0)
#ifdef STR_USE_WIN32_CONV
long MbsToWcs( wchar_t *dst, long size, const char *src, int codePage=CP_ACP );
long WcsToMbs( char *dst, long size, const wchar_t *src, int codePage=CP_ACP );
#else
long MbsToWcs( wchar_t *dst, long size, const char *src );
long WcsToMbs( char *dst, long size, const wchar_t *src );
#endif

#endif
