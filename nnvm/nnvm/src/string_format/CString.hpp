// StringUtils is also available under the CPOL license as part of the FormatString article
// on CodeProject: http://www.codeproject.com/KB/string/FormatString.aspx

#ifndef _CSTRING_H
#define _CSTRING_H

// CStringA, CStringW - simple string containers
// They use reference counting to avoid having multiple copies of the same string
// in memory.
//
///////////////////////////////////////////////////////////////////////////////

#include "StringUtils.hpp"
#include "FormatString.hpp"

///////////////////////////////////////////////////////////////////////////////
// ANSI version

namespace nnvm {
	class CStringA {
	public:
		CStringA(void) {
			m_pText = NULL;
		}

		CStringA(const CStringA &s) {
			m_pText = s.m_pText;
			if (m_pText) GetRef()++;
		}

		void operator=(const CStringA &s) {
			Clear();
			m_pText = s.m_pText;
			if (m_pText) GetRef()++;
		}

		explicit CStringA(const char *text, long len = -1) {
			m_pText = NULL;
			Set(text, len);
		}

		~CStringA(void) {
			Clear();
		}

		void Clear(void);

		void Set(const char *text, long len = -1);

		void Format(const char *format, FORMAT_STRING_ARGS_H);

		operator const char *(void) const {
			return m_pText ? m_pText : "";
		}

		operator CFormatArg(void) const {
			return CFormatArg(m_pText);
		}

	private:
		char *m_pText;

		long &GetRef(void) { return ((long *) m_pText)[-1]; }
	};

///////////////////////////////////////////////////////////////////////////////
// UNICODE version

	class CStringW {
	public:
		CStringW(void) {
			m_pText = NULL;
		}

		CStringW(const CStringW &s) {
			m_pText = s.m_pText;
			if (m_pText) GetRef()++;
		}

		void operator=(const CStringW &s) {
			Clear();
			m_pText = s.m_pText;
			if (m_pText) GetRef()++;
		}

		explicit CStringW(const wchar_t *text, long len = -1) {
			m_pText = NULL;
			Set(text, len);
		}

		~CStringW(void) {
			Clear();
		}

		void Clear(void);

		void Set(const wchar_t *text, long len = -1);

		void Format(const wchar_t *format, FORMAT_STRING_ARGS_H);

		operator const wchar_t *(void) const {
			return m_pText ? m_pText : L"";
		}

		operator CFormatArg(void) const {
			return CFormatArg(m_pText);
		}

	private:
		wchar_t *m_pText;

		long &GetRef(void) { return ((long *) m_pText)[-1]; }
	};

///////////////////////////////////////////////////////////////////////////////

#ifdef _UNICODE
	typedef CStringW CString;
#else
	typedef CStringA CString;
#endif
}

#endif
