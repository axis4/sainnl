/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#pragma once

namespace nnvm {
    class Processor;
}

#include <predef.h>
#include <unicode/unistr.h>
#include <vector>
#include <map>
#include <memory>
#ifdef _OPENMP
#include <omp.h>
#else
#define omp_get_thread_num() 0
#define omp_get_num_threads() 1
#endif
#include <chrono>
#include <Disposable.hpp>
#include <ProcessorKnownParameters.hpp>
#include <ByteCode.hpp>
#include <ByteCodeBlock.hpp>
#include <MachineSetup.hpp>
#include <Counters.hpp>
#include <ByteCodeLoader.hpp>
#include "../parser/Language.hpp"

namespace nnvm {
    typedef void (Processor::*processBlock)(std::shared_ptr<MachineSetup> options, std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock> > > code);

    struct Processing
    {
        processBlock processLinksClean;
        processBlock processElementsClean;
        processBlock processLinks;
        processBlock processElements;
    };

    class Processor : public Disposable
    {
    public:
        Processor(std::shared_ptr<MachineSetup> options, const std::shared_ptr<Context>& ctx);
        virtual ~Processor();
        void processStep();
        void terminate();
        int8_t processExternalLinks(const std::map<icu::UnicodeString, std::map<icu::UnicodeString, std::tuple<double, double, double> > >& flow);

    protected:
        std::shared_ptr<MachineSetup> options;
        std::shared_ptr<Context> ctx;
        std::map<ProcessorParallelMode, Processing> processing;
        std::chrono::time_point<std::chrono::steady_clock, std::chrono::nanoseconds> created; // moment of creation

        void updateStatusCounters(std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock>>> code);
        void incCounter(Counter& counter, double sample);
        void timeIt(Counter& counter, std::function<void()> call);
        void moveSignalThroughLink(std::shared_ptr<ByteCodeParameter> sum, std::shared_ptr<ByteCodeBlockItem> bi, icu::UnicodeString& linkType, std::shared_ptr<ByteCodeBlock> tb, std::shared_ptr<MachineSetup> options);
        void processTemplating(std::shared_ptr<MachineSetup> options, std::shared_ptr<ByteCodeBlock> block, std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock>>> codes, std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock>>> newCode, std::shared_ptr<std::deque<icu::UnicodeString>> removeCode);
        void processByType(std::shared_ptr<MachineSetup> options, std::shared_ptr<ByteCodeBlock> block, std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock>>> codes);
        void processLinkClean(std::shared_ptr<ByteCodeBlock> b, icu::UnicodeString& paramName);
        void processLinksClean(std::shared_ptr<MachineSetup> options, std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock>>> code);
        void processLinksCleanOMP(std::shared_ptr<MachineSetup> options, std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock>>> code);
        void processLinksCleanOCL(std::shared_ptr<MachineSetup> options, std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock>>> code);
        void processElementClean(std::shared_ptr<ByteCodeBlock> b);
        void processElementsClean(std::shared_ptr<MachineSetup> options, std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock>>> code);
        void processElementsCleanOMP(std::shared_ptr<MachineSetup> options, std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock>>> code);
        void processLinks(std::shared_ptr<MachineSetup> options, std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock>>> code);
        void processLinksOMP(std::shared_ptr<MachineSetup> options, std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock>>> code);
        void removeLinks(std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock>>> blocks, std::shared_ptr<std::deque<icu::UnicodeString>> removedBlocks);
        void removeByType(std::shared_ptr<MachineSetup> options, std::shared_ptr<ByteCodeBlock> block, std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock>>> codes);
        void removeBlocks(std::shared_ptr<MachineSetup> options, std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock>>> blocks, std::shared_ptr<std::deque<icu::UnicodeString>> removedBlocks);
        void expectBlocks(std::shared_ptr<MachineSetup> options, std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock>>> blocks, std::shared_ptr<std::deque<icu::UnicodeString>> removedBlocks);
        void processRemovedElements(std::shared_ptr<MachineSetup> options, std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock>>> code, std::shared_ptr<std::deque<icu::UnicodeString>> removedCode);
        void mergeIncomes(std::shared_ptr<ByteCodeBlock> block, std::shared_ptr<ByteCodeBlock> newBlock, std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock>>> blocks);
        void mergeItems(std::shared_ptr<ByteCodeBlock> block, std::shared_ptr<ByteCodeBlock> newBlock, std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock>>> blocks);
        void mergeBlocks(std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock>>> blocks, std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock>>> newBlocks);
        void mergeLinks(std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock>>> blocks, std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock>>> newBlocks);
        void processNewElements(std::shared_ptr<MachineSetup> options, std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock>>> code, std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock>>> newCode);
        std::vector<icu::UnicodeString> prepareProcessElements(std::shared_ptr<MachineSetup> options, std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock>>> code);
        void processElements(std::shared_ptr<MachineSetup> options, std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock>>> code);
        void processElementsOMP(std::shared_ptr<MachineSetup> options, std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock>>> code);
    };
}
