/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#include <Templater.hpp>
#include <ProcessorKnownParameters.hpp>
#include <ByteCodeUtils.hpp>
#include "../utils/Utils.hpp"

namespace nnvm {
    Templater::Templater()
    {
    }


    Templater::~Templater()
    {
    }

    std::shared_ptr<NameParts> applyRenameImpl(std::shared_ptr<NameParts> parts, const std::map<icu::UnicodeString, icu::UnicodeString>& renamings, int8_t sub)
    {
        if (!parts) {
            return std::shared_ptr<NameParts>(nullptr);
        }
        auto res = std::make_shared<NameParts>();
        if (sub) {
            auto fn = parts->toString();
            auto n = renamings.find(fn);
            if (n != renamings.end()) {
                res->name = n->second;
                return res;
            }
            else {
                res->name = parts->name;
                for (auto i = parts->indexes.begin(); i != parts->indexes.end(); ++i) {
                    res->indexes.push_back(std::make_shared<NameParts>(**i));
                }
                if (parts->subname) {
                    res->subname = std::make_shared<NameParts>(*parts->subname);
                }
            }
        }
        else {
            res->name = parts->name;
            for (auto i = parts->indexes.begin(); i != parts->indexes.end(); ++i) {
                res->indexes.push_back(applyRenameImpl(*i, renamings, true));
            }
            res->subname = applyRenameImpl(parts->subname, renamings, false);
        }
        return res;
    }


    std::shared_ptr<NameParts> Templater::applyRename(std::shared_ptr<NameParts> parts, const std::map<icu::UnicodeString, icu::UnicodeString>& renamings)
    {
        return applyRenameImpl(parts, renamings, false);
    }


    template<typename T>
    void updateParameters(
        std::shared_ptr<T> block,
        std::map<icu::UnicodeString, icu::UnicodeString> renamings)
    {
        std::map<icu::UnicodeString, double> replace;
        auto r = block->getParameters();
        for (auto i = std::get<0>(r); i != std::get<1>(r); ++i) {
            if ((*i)->getType() == ByteCodeParameterTypes::STRING) {
                auto f = renamings.find((*i)->value.stringValue);
                if (f != renamings.end()) {
                    replace[(*i)->getName()] = ByteCodeUtils::asNumber(f->second);
                }
            }
        }
        for (auto i = replace.begin(); i != replace.end(); ++i) {
            std::shared_ptr<ByteCodeParameter> p(new ByteCodeParameter(i->first, i->second));
            block->setParameter(p);
        }
    }

    int8_t isNameLinked(std::shared_ptr<ByteCodeBlock> block, const icu::UnicodeString& name)
    {
        auto r = block->getIncomes();
        for (auto i = std::get<0>(r); i != std::get<1>(r); ++i) {
            if (name == (*i)->getOwner().lock()->getName()) {
                return true;
            }
        }
        return false;
    }

    int8_t isScoped(std::shared_ptr<ByteCodeBlock> checked, std::shared_ptr<ByteCodeBlock> scope, const std::map<icu::UnicodeString, icu::UnicodeString>& renamings)
    {
        // renaming is from checked name
        if (renamings.find(checked->getName()) != renamings.end()) {
            return false;
        }
        // renaming touched both names
        auto ch = NameParts::parseName(checked->getName());
        auto sc = NameParts::parseName(scope->getName());
        std::map<icu::UnicodeString, int8_t> scParts;
        for (auto i = sc->indexes.begin(); i != sc->indexes.end(); ++i) {
            auto n = (*i)->toString();
            if (renamings.find(n) != renamings.end() && isNameLinked(scope, n)) {
                scParts[n] = 1;
            }
        }
        for (auto i = ch->indexes.begin(); i != ch->indexes.end(); ++i) {
            auto n = (*i)->toString();
            if (scParts.find(n) != scParts.end() && isNameLinked(checked, n)) {
                return true;
            }
        }
        return false;
    }

    std::shared_ptr<ByteCodeBlockItem> newLink(std::shared_ptr<ByteCodeBlock> fromBlock, std::shared_ptr<ByteCodeBlockItem> link, std::shared_ptr<ByteCodeBlock> toBlock, std::map<icu::UnicodeString, icu::UnicodeString> renamings)
    {
        std::shared_ptr<ByteCodeBlockItem> l(new ByteCodeBlockItem(toBlock->getName(), fromBlock));
        auto r = link->getParameters();
        for (auto k = std::get<0>(r); k != std::get<1>(r); ++k) {
            l->setParameter(std::make_shared<ByteCodeParameter>(**k));
        }
        updateParameters(l, renamings);
        return l;
    }

    void newIncome(std::shared_ptr<ByteCodeBlock> fromBlock, std::shared_ptr<ByteCodeBlockItem> link, std::shared_ptr<ByteCodeBlock> toBlock, std::map<icu::UnicodeString, icu::UnicodeString> renamings)
    {
        toBlock->addIncome(newLink(fromBlock, link, toBlock, renamings));
    }

    void newItem(std::shared_ptr<ByteCodeBlock> fromBlock, std::shared_ptr<ByteCodeBlockItem> link, std::shared_ptr<ByteCodeBlock> toBlock, std::map<icu::UnicodeString, icu::UnicodeString> renamings)
    {
        fromBlock->addItem(newLink(fromBlock, link, toBlock, renamings));
    }

    void Templater::processTemplate(
        std::shared_ptr<ByteCodeBlock> templateBlock,
        const std::map<icu::UnicodeString, icu::UnicodeString>& renamings,
        const std::map<icu::UnicodeString, int8_t>& removals,
        std::shared_ptr<NameParts> templateName,
        std::shared_ptr<NameParts> newName,
        std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock>>> codes,
        std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock>>> newCode,
        std::shared_ptr<std::deque<icu::UnicodeString>> removeCode)
    {
        int8_t inRemove = false;
        for (auto i = removals.begin(); i != removals.end(); ++i) {
            inRemove |= i->second;
        }
        if (inRemove) {
            removeCode->push_back(newName->toString());
        }
        else {
            // New block
            auto nb = templateBlock->isolatedCopy(newName->toString());

            // New parameters
            updateParameters(nb, renamings);

            // Links (items)
            auto r = templateBlock->getIncomes();
            for (auto i = std::get<0>(r); i != std::get<1>(r); ++i) {
                auto from = (*i)->getOwner().lock();
                auto fromLink = (*i);
                // v1: * -> A[I]
                if (!isScoped(from, templateBlock, renamings)) {
                    // v1.1: I -> A[I]
                    if (renamings.find(from->getName()) != renamings.end()) {
                        continue; // link unattached
                    }
                    // v1.2: * -> A[I]
                    else {
                        newIncome(from, fromLink, nb, renamings);
                    }
                }
                // v2: *[I] -> A[I]
                else {
                    auto fromNew = applyRename(NameParts::parseName(from->getName()), renamings)->toString();
                    std::shared_ptr<ByteCodeBlock> fromNewBlock;
                    auto ex = codes->find(fromNew);
                    if (ex != codes->end()) {
                        fromNewBlock = ex->second;
                    }
                    else {
                        ex = newCode->find(fromNew);
                        if (ex != newCode->end()) {
                            fromNewBlock = ex->second;
                        }
                        else {
                            continue; // not yet created source of link
                        }
                    }
                    newIncome(fromNewBlock, fromLink, nb, renamings);
                }
            }
            r = templateBlock->getItems();
            for (auto i = std::get<0>(r); i != std::get<1>(r); ++i) {
                std::shared_ptr<ByteCodeBlock> to;
                auto tmp = codes->find((*i)->getName());
                if (tmp == codes->end()) {
                    continue;
                }
                to = tmp->second;
                auto toLink = (*i);
                // v3: A[I] -> *
                if (!isScoped(to, templateBlock, renamings)) {
                    newItem(nb, toLink, to, renamings);
                }
                // v4: A[I] -> *[I]
                else {
                    auto toNew = applyRename(NameParts::parseName(to->getName()), renamings)->toString();
                    std::shared_ptr<ByteCodeBlock> toNewBlock;
                    auto ex = codes->find(toNew);
                    if (ex != codes->end()) {
                        toNewBlock = ex->second;
                    }
                    else {
                        ex = newCode->find(toNew);
                        if (ex != newCode->end()) {
                            toNewBlock = ex->second;
                        }
                        else {
                            continue; // not yet created destination of link
                        }
                    }
                    newIncome(nb, toLink, toNewBlock, renamings);
                }
            }

            // Is still a template?
            int8_t isTemplate = false;
            r = nb->getIncomes();
            for (auto i = std::get<0>(r); i != std::get<1>(r); ++i) {
                std::shared_ptr<ByteCodeParameter> w;
                if ((*i)->tryGetParameter(LinkParams::GENERATION, w) && ByteCodeUtils::asDouble(*w) > 0) {
                    isTemplate = true;
                }
            }
            std::shared_ptr<ByteCodeParameter> tp;
            if (!nb->tryGetParameter(ElementParams::TEMPLATE, tp) || tp->getType() != ByteCodeParameterTypes::BYTE) {
                tp = std::make_shared<ByteCodeParameter>(ElementParams::TEMPLATE, (int8_t)0);
                nb->setParameter(tp);
            }
            tp->value.byteValue = isTemplate;
            nb->setParameter(std::make_shared<ByteCodeParameter>(ElementParams::EXPECTED, (int8_t)0));

            // Done
            newCode->operator[](newName->toString()) = nb;
        }
    }
}
