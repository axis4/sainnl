/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#include "Processor.hpp"
#include <cmath>
#include <OpenCL/opencl.h>
#include <ByteCodeException.hpp>
#include <ByteCodeParameter.hpp>
#include <ByteCodeBlockItem.hpp>
#include <Internationalization.hpp>
#include <NameParts.hpp>
#include <ByteCodeUtils.hpp>
#include <utility>
#include "../utils/Utils.hpp"
#include "../parser/Language.hpp"
//#include "kernel.cl.hpp"

namespace nnvm {
    Processor::Processor(std::shared_ptr<MachineSetup> options, const std::shared_ptr<Context>& ctx) : options(std::move(options)), ctx(ctx)
    {
        processing[ProcessorParallelMode::NONE] = { &Processor::processLinksClean, &Processor::processElementsClean, &Processor::processLinks, &Processor::processElements };
        processing[ProcessorParallelMode::OMP] = { &Processor::processLinksCleanOMP, &Processor::processElementsCleanOMP, &Processor::processLinksOMP, &Processor::processElementsOMP };
    //    processing[ProcessorParallelMode::OCL] = { &Processor::processLinksCleanOCL, &Processor::processElementsCleanOCL, &Processor::processLinksOCL, &Processor::processElementsOCL };
        created = std::chrono::high_resolution_clock::now();
        ctx->counters.stepCount = 0;
    }
    Processor::~Processor() = default;

    void Processor::incCounter(Counter& counter, double sample)
    {
        counter.incCounter(sample);
    }

    void Processor::timeIt(Counter& counter, std::function<void()> call)
    {
        PerformanceUtils::timeit(std::move(call), [this, &counter](int64_t time){
            incCounter(counter, (double)time);
        });
    }

    void Processor::moveSignalThroughLink(std::shared_ptr<ByteCodeParameter> sum, std::shared_ptr<ByteCodeBlockItem> bi, icu::UnicodeString& linkType, std::shared_ptr<ByteCodeBlock> tb, std::shared_ptr<MachineSetup> options)
    {
        double res = 0;
        std::shared_ptr<ByteCodeParameter> weight;
        if (!bi->tryGetParameter(linkType, weight)) {
            return;
        }
        switch (options->getProcessorSetup()->mode)
        {
        case ProcessorMode::BYTE:
            res = (char)(ByteCodeUtils::asDouble(*weight) * sum->value.byteValue);
            break;
        case ProcessorMode::INTEGER:
            res = (double)(int64_t)(ByteCodeUtils::asDouble(*weight) * sum->value.integerValue);
            break;
        case ProcessorMode::REAL:
            res = ByteCodeUtils::asDouble(*weight) * sum->value.doubleValue;
            break;
        default:
            break;
        }
        if (options->getProcessorSetup()->verbose) {
            options->getLogger()->debug(StringUtils::spf(USS("Signal move {0} -{1}={2}-> {3}"), StringConvUtils::utow(bi->getOwner().lock()->getName()), StringConvUtils::utow(linkType), res, StringConvUtils::utow(bi->getName())));
        }

        auto closure = [](double res, std::shared_ptr<ByteCodeBlockItem> bi, icu::UnicodeString& linkType, std::shared_ptr<ByteCodeBlock> tb)
        {
            std::shared_ptr<ByteCodeParameter> source;
            auto paramName = linkType + ElementParams::SOURCE_SUFFIX;
            if (!tb->tryGetParameter(paramName, source)) {
                source = std::make_shared<ByteCodeParameter>(paramName, (std::shared_ptr<void>)std::make_shared<std::map<icu::UnicodeString, double>>());
            }
            if (source->getType() != ByteCodeParameterTypes::_INTERNAL_OBJECT) {
                throw ByteCodeException(StringUtils::spf(_i("err.bytecode.parameter.illegal"), StringConvUtils::utow(paramName)));
            }
            std::static_pointer_cast<std::map<icu::UnicodeString, double>>(source->value.objectValue)->operator[](bi->getOwner().lock()->getName()) = res;
            
            std::shared_ptr<ByteCodeParameter> sum;
            auto paramSumName = linkType + ElementParams::SUM_SUFFIX;
            if (!tb->tryGetParameter(paramSumName, sum) || sum->getType() != ByteCodeParameterTypes::DOUBLE) {
                sum = std::make_shared<ByteCodeParameter>(paramSumName, (double)0);
                tb->setParameter(sum);
            }
            sum->value.doubleValue += res;
        };
        
        if (options->getProcessorSetup()->parallelMode == ProcessorParallelMode::OMP) {
            #pragma omp critical(dataupdate)
            {
                closure(res, bi, linkType, tb);
            }
        }
        else {
            closure(res, bi, linkType, tb);
        }
    }

    void Processor::processTemplating(std::shared_ptr<MachineSetup> options, std::shared_ptr<ByteCodeBlock> block, std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock>>> codes, std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock>>> newCode, std::shared_ptr<std::deque<icu::UnicodeString>> removeCode)
    {
        std::map<icu::UnicodeString, std::shared_ptr<ByteCodeParameter>> params;
        auto gsn = StringConvUtils::wtou(StringConvUtils::utow(LinkParams::GENERATION + ElementParams::SOURCE_SUFFIX));
        auto gsumn = StringConvUtils::wtou(StringConvUtils::utow(LinkParams::GENERATION + ElementParams::SUM_SUFFIX));
        block->getParams(std::vector<icu::UnicodeString>{ gsn, gsumn }, params);

        auto s = params[gsumn]->value;
        auto p = params[gsn]->value;
        auto generations = std::static_pointer_cast<std::map<icu::UnicodeString, double>>(p.objectValue);

        if (!generations || !generations->size() || fabs(s.doubleValue) < DBL_EPSILON) {
            return;
        }
        
        std::map<icu::UnicodeString, icu::UnicodeString> renamings;
        std::map<icu::UnicodeString, int8_t> removals;

        for (auto i = generations->begin(); i != generations->end(); i++) {
            auto v = int64_t(fabs(i->second));
            if (v == 0) {
                continue;
            }
            auto name = i->first;
            auto value = StringUtils::spf(USS("{0}"), v);
            renamings[name] = value;
            removals[name] = i->second < 0;
        }

        auto parts = NameParts::parseName(block->getName());
        auto renamedParts = options->getProcessorSetup()->getTemplateStrategy()->applyRename(parts, renamings);

        if (options->getProcessorSetup()->verbose) {
            options->getLogger()->debug(StringUtils::spf(USS("Apply template from {0} to {1}"), StringConvUtils::utow(parts->toString()), StringConvUtils::utow(renamedParts->toString())));
        }

        options->getProcessorSetup()->getTemplateStrategy()->processTemplate(block, renamings, removals, parts, renamedParts, codes, newCode, removeCode);
    }

    void Processor::processByType(std::shared_ptr<MachineSetup> options, std::shared_ptr<ByteCodeBlock> block, std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock>>> codes)
    {
        timeIt(ctx->counters.eachNode, [this, &options, &block, &codes]()
               {
                   auto blockCounter = ctx->counters.nodes.find(block->getType());
                   if (blockCounter == ctx->counters.nodes.end()) {
                       ctx->counters.nodes.insert({ block->getType(), Counter(options->getDefaultSampling()) });
                       blockCounter = ctx->counters.nodes.find(block->getType());
                   }
                   timeIt(blockCounter->second, [this, &options, &block, &codes]()
                          {
                                std::shared_ptr<ExecutableNeuron> code;
                                try {
                                    code = options->getProcessorSetup()->getNeuron(block->getType());
                                } catch (ByteCodeException e) {
                                    ctx->getNeuron(block->getType());
                                    try {
                                        code = options->getProcessorSetup()->getNeuron(block->getType());
                                    } catch (ByteCodeException e) { /* ignore */ }
                                }
                                if (!code) {
                                    if (!options->getProcessorSetup()->tolerate_missing) {
                                        throw ByteCodeException(StringUtils::spf(_i("err.bytecode.type.unknown"),
                                                                                 StringConvUtils::utow(block->getType())));
                                    }
                                }
                                else {
                                    code->call(block, codes);
                                }
                          });
               });
    }

    void Processor::processLinkClean(std::shared_ptr<ByteCodeBlock> b, icu::UnicodeString& paramName)
    {
        b->setParameter(std::make_shared<ByteCodeParameter>(
            paramName + ElementParams::SOURCE_SUFFIX,
            (std::shared_ptr<void>)std::make_shared<std::map<icu::UnicodeString, double>>()));
        b->setParameter(std::make_shared<ByteCodeParameter>(paramName + ElementParams::SUM_SUFFIX, (double)0));
    }

    void Processor::processLinksClean(std::shared_ptr<MachineSetup> options, std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock>>> code)
    {
        std::vector<icu::UnicodeString> codeNames;
        for (auto i = code->begin(); i != code->end(); i++) {
            codeNames.push_back(i->first);
        }
        for (int64_t i = 0; i < codeNames.size(); i++) {
            auto b = code->operator[](codeNames[(unsigned int)i]);
            processLinkClean(b, LinkParams::ACTIVATION);
            processLinkClean(b, LinkParams::STREAM);
            processLinkClean(b, LinkParams::GENERATION);
        }
    }

    void Processor::processLinksCleanOMP(std::shared_ptr<MachineSetup> options, std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock>>> code)
    {
        std::vector<icu::UnicodeString> codeNames;
        for (auto i = code->begin(); i != code->end(); i++) {
            codeNames.push_back(i->first);
        }
        #pragma omp parallel for shared(code, codeNames) schedule(dynamic)
        for (int64_t i = 0; i < codeNames.size(); i++) {
            // no locks as each ByteCodeBlock is cleaned in a separate thread
            auto b = code->operator[](codeNames[(unsigned int)i]);
            processLinkClean(b, LinkParams::ACTIVATION);
            processLinkClean(b, LinkParams::STREAM);
            processLinkClean(b, LinkParams::GENERATION);
        }
    }

    void Processor::processLinksCleanOCL(std::shared_ptr<MachineSetup> options, std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock>>> code)
    {
    /*    // create a compute context with GPU device
        auto context = clCreateContextFromType(NULL, CL_DEVICE_TYPE_GPU, NULL, NULL, NULL);
        
        // create a command queue
        cl_device_id device_id;
        clGetDeviceIDs( NULL, CL_DEVICE_TYPE_DEFAULT, 1, &device_id, NULL );
        auto queue = clCreateCommandQueue(context, device_id, 0, NULL);
        
        // allocate the buffer memory objects
        memobjs[0] = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(float)*2*num_entries, srcA, NULL);
        memobjs[1] = clCreateBuffer(context, CL_MEM_READ_WRITE, sizeof(float)*2*num_entries, NULL, NULL);
        
        // create the compute program
        program = clCreateProgramWithSource(context, 1, &fft1D_1024_kernel_src, NULL, NULL);
        
        // build the compute program executable
        clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
        
        // create the compute kernel
        kernel = clCreateKernel(program, "fft1D_1024", NULL);
        
        // set the args values
        clSetKernelArg(kernel, 0, sizeof(cl_mem), (void *)&memobjs[0]);
        clSetKernelArg(kernel, 1, sizeof(cl_mem), (void *)&memobjs[1]);
        clSetKernelArg(kernel, 2, sizeof(float)*(local_work_size[0]+1)*16, NULL);
        clSetKernelArg(kernel, 3, sizeof(float)*(local_work_size[0]+1)*16, NULL);
        
        // create N-D range object with work-item dimensions and execute kernel
        global_work_size[0] = num_entries;
        local_work_size[0] = 64; //Nvidia: 192 or 256
        clEnqueueNDRangeKernel(queue, kernel, 1, NULL, global_work_size, local_work_size, 0, NULL, NULL);
        
        
        std::vector<icu::UnicodeString> codeNames;
        for (auto i = code->begin(); i != code->end(); i++) {
            codeNames.push_back(i->first);
        }
        #pragma omp parallel for shared(code, codeNames) schedule(dynamic)
        for (int64_t i = 0; i < codeNames.size(); i++) {
            // no locks as each ByteCodeBlock is cleaned in a separate thread
            auto b = code->operator[](codeNames[(unsigned int)i]);
            processLinkClean(b, LinkParams::ACTIVATION);
            processLinkClean(b, LinkParams::STREAM);
            processLinkClean(b, LinkParams::GENERATION);
        }*/
    }

    void Processor::processElementClean(std::shared_ptr<ByteCodeBlock> b)
    {
        b->setParameter(std::make_shared<ByteCodeParameter>(ElementParams::SUM, (double)0));
    }

    void Processor::processElementsClean(std::shared_ptr<MachineSetup> options, std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock>>> code)
    {
        std::vector<icu::UnicodeString> codeNames;
        for (auto i = code->begin(); i != code->end(); i++) {
            codeNames.push_back(i->first);
        }
        for (int64_t i = 0; i < codeNames.size(); i++) {
            processElementClean(code->operator[](codeNames[(unsigned int)i]));
        }
    }

    void Processor::processElementsCleanOMP(std::shared_ptr<MachineSetup> options, std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock>>> code)
    {
        std::vector<icu::UnicodeString> codeNames;
        for (auto i = code->begin(); i != code->end(); i++) {
            codeNames.push_back(i->first);
        }
        #pragma omp parallel for shared(code, codeNames) schedule(dynamic)
        for (int64_t i = 0; i < codeNames.size(); i++) {
            // no locks as each ByteCodeBlock cleans its SUM
            processElementClean(code->operator[](codeNames[(unsigned int)i]));
        }
    }

    void Processor::processLinks(std::shared_ptr<MachineSetup> options, std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock>>> code)
    {
        std::vector<icu::UnicodeString> codeNames;
        for (auto i = code->begin(); i != code->end(); i++) {
            codeNames.push_back(i->first);
        }
        for (int64_t i = 0; i < codeNames.size(); i++) {
            auto b = code->operator[](codeNames[(unsigned int)i]);
            auto r = b->getIncomes();
            for (auto k = std::get<0>(r); k != std::get<1>(r); k++) {
                auto bi = *k;
                auto sb = bi->getOwner().lock();
                std::shared_ptr<ByteCodeParameter> sum;
                if (!sb->tryGetParameter(ElementParams::SUM, sum)) {
                    sum = std::make_shared<ByteCodeParameter>(ElementParams::SUM, (double)0);
                }
                options->getProcessorSetup()->offsetByMode(sum);
                moveSignalThroughLink(sum, bi, LinkParams::ACTIVATION, b, options);
                moveSignalThroughLink(sum, bi, LinkParams::STREAM, b, options);
                moveSignalThroughLink(sum, bi, LinkParams::GENERATION, b, options);
            }
        }
    }

    void Processor::processLinksOMP(std::shared_ptr<MachineSetup> options, std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock>>> code)
    {
        std::vector<icu::UnicodeString> codeNames;
        for (auto & i : *code) {
            codeNames.push_back(i.first);
        }
        #pragma omp parallel for shared(code, codeNames) schedule(dynamic)
        for (int64_t i = 0; i < codeNames.size(); i++) {
            if (options->getProcessorSetup()->verbose) {
                options->getLogger()->debug(StringUtils::spf(USS("Processing thread {0}/{1}"), omp_get_thread_num() + 1, omp_get_num_threads()));
            }
            auto b = code->operator[](codeNames[(unsigned int)i]);
            // no locks as each ByteCodeBlock fills its SOURCES and SUMS from incomes
            auto r = b->getIncomes();
            for (auto k = std::get<0>(r); k != std::get<1>(r); k++) {
                auto bi = *k;
                auto sb = bi->getOwner().lock();
                std::shared_ptr<ByteCodeParameter> sum;
                if (!sb->tryGetParameter(ElementParams::SUM, sum)) {
                    sum = std::make_shared<ByteCodeParameter>(ElementParams::SUM, (double)0);
                }
                options->getProcessorSetup()->offsetByMode(sum);
                moveSignalThroughLink(sum, bi, LinkParams::ACTIVATION, b, options);
                moveSignalThroughLink(sum, bi, LinkParams::STREAM, b, options);
                moveSignalThroughLink(sum, bi, LinkParams::GENERATION, b, options);
            }
        }
    }

    void Processor::removeLinks(std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock> > > blocks, std::shared_ptr<std::deque<icu::UnicodeString> > removedBlocks)
    {
        for (auto & i : *removedBlocks) {
            for (auto & k : *blocks) {
                auto r = k.second->getItems();
                for (auto l = std::get<0>(r); l != std::get<1>(r); l++) {
                    if ((*l)->getName() == i) {
                        k.second->eraseItem(l);
                    }
                }
                r = k.second->getIncomes();
                for (auto l = std::get<0>(r); l != std::get<1>(r); l++) {
                    if ((*l)->getName() == i) {
                        k.second->eraseIncome(l);
                    }
                }
            }
        }
    }

    void Processor::removeByType(std::shared_ptr<MachineSetup> options, std::shared_ptr<ByteCodeBlock> block, std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock> > > codes)
    {
        std::shared_ptr<ExecutableNeuron> code;
        try {
            code = options->getProcessorSetup()->getNeuron(block->getType());
        } catch (ByteCodeException &e) {
            ctx->getNeuron(block->getType());
            code = options->getProcessorSetup()->getNeuron(block->getType());
        }
        if (!code) {
            if (!options->getProcessorSetup()->tolerate_missing) {
                throw ByteCodeException(StringUtils::spf(_i("err.bytecode.type.unknown"), StringConvUtils::utow(block->getType())));
            }
        }
        code->removed(block, std::move(codes));
    }

    void Processor::removeBlocks(std::shared_ptr<MachineSetup> options, std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock> > > blocks, std::shared_ptr<std::deque<icu::UnicodeString> > removedBlocks)
    {
        for (auto & i : *removedBlocks) {
            auto t = blocks->find(i);
            if (t != blocks->end()) {
                t->second->dispose();
                removeByType(options, t->second, blocks);
                blocks->erase(i);
            }
        }
    }

    void Processor::expectBlocks(std::shared_ptr<MachineSetup> options, std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock> > > blocks, std::shared_ptr<std::deque<icu::UnicodeString> > removedBlocks)
    {
        for (auto & i : *removedBlocks) {
            auto t = blocks->find(i);
            if (t != blocks->end()) {
                
                t->second->setParameter(std::make_shared<ByteCodeParameter>(ElementParams::EXPECTED, (int8_t)1));
            }
        }
    }

    void Processor::processRemovedElements(std::shared_ptr<MachineSetup> options, std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock> > > code, std::shared_ptr<std::deque<icu::UnicodeString> > removedCode)
    {
        if (removedCode) {
            if (!removedCode->empty()) {
                //removeLinks(code, removedCode);
                //removeBlocks(code, removedCode);
                expectBlocks(options, code, removedCode);
            }
        }
    }

    void Processor::mergeIncomes(std::shared_ptr<ByteCodeBlock> block, std::shared_ptr<ByteCodeBlock> newBlock, std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock> > > blocks)
    {
        auto r = newBlock->getIncomes();
        for (auto i = std::get<0>(r); i != std::get<1>(r); ++i) {
            auto nbi = *i;
            auto t = blocks->find(nbi->getOwner().lock()->getName());
            if (t == blocks->end()) {
                throw ByteCodeException(StringUtils::spf(_i("err.bytecode.block.notfound"), StringConvUtils::utow(nbi->getOwner().lock()->getName())));
            }
            std::shared_ptr<ByteCodeBlockItem> fbi = nullptr;
            auto r2 = block->getIncomes();
            for (auto k = std::get<0>(r2); k != std::get<1>(r2); ++k) {
                auto bi = *k;
                if (bi->getOwner().lock()->getName() == nbi->getOwner().lock()->getName()) {
                    fbi = bi;
                }
            }
            if (!fbi) {
                fbi = std::make_shared<ByteCodeBlockItem>(block->getName(), t->second);
                auto r3 = nbi->getParameters();
                for (auto l = std::get<0>(r3); l != std::get<1>(r3); ++l) {
                    fbi->setParameter(std::make_shared<ByteCodeParameter>(**l));
                }
                t->second->addItem(fbi);
                block->addIncome(fbi);
            }
        }
    }

    void Processor::mergeItems(std::shared_ptr<ByteCodeBlock> block, std::shared_ptr<ByteCodeBlock> newBlock, std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock> > > blocks)
    {
        auto r = newBlock->getItems();
        for (auto i = std::get<0>(r); i != std::get<1>(r); i++) {
            auto nbi = *i;
            auto t = blocks->find(nbi->getName());
            if (t == blocks->end()) {
                throw ByteCodeException(StringUtils::spf(_i("err.bytecode.block.notfound"), StringConvUtils::utow(nbi->getName())));
            }
            std::shared_ptr<ByteCodeBlockItem> fbi = nullptr;
            auto r2 = block->getItems();
            for (auto k = std::get<0>(r2); k != std::get<1>(r2); k++) {
                auto bi = *k;
                if (bi->getName() == nbi->getName()) {
                    fbi = bi;
                }
            }
            if (!fbi) {
                fbi = std::make_shared<ByteCodeBlockItem>(nbi->getName(), block);
                auto r3 = nbi->getParameters();
                for (auto l = std::get<0>(r3); l != std::get<1>(r3); l++) {
                    fbi->setParameter(std::make_shared<ByteCodeParameter>(**l));
                }
                block->addItem(fbi);
                t->second->addIncome(fbi);
            }
        }
    }

    void Processor::mergeBlocks(std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock> > > blocks, std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock> > > newBlocks)
    {
        for (auto i = newBlocks->begin(); i != newBlocks->end(); ++i) {
            auto b = blocks->find(i->first);
            if (b == blocks->end()) {
                blocks->operator[](i->first) = i->second->isolatedCopy();
            }
            else {
                auto r = i->second->getParameters();
                for (auto p = std::get<0>(r); p != std::get<1>(r); ++p) {
                    b->second->setParameter(*p);
                }
            }
        }
    }

    void Processor::mergeLinks(std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock> > > blocks, std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock> > > newBlocks)
    {
        for (auto i = newBlocks->begin(); i != newBlocks->end(); i++) {
            auto b = blocks->find(i->first);
            mergeItems(b->second, i->second, blocks);
            mergeIncomes(b->second, i->second, blocks);
        }
    }

    void Processor::processNewElements(std::shared_ptr<MachineSetup> options, std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock> > > code, std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock> > > newCode)
    {
        if (newCode) {
            if (!newCode->empty()) {
                mergeBlocks(code, newCode);
                mergeLinks(code, newCode);
                for (auto & i : *newCode) {
                    i.second->dispose();
                }
            }
        }
    }

    std::vector<icu::UnicodeString> Processor::prepareProcessElements(std::shared_ptr<MachineSetup> options, std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock> > > code)
    {
        timeIt(ctx->counters.templatePhase, [this, &options, &code]()
               {
                   auto newCode = std::make_shared<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock>>>();
                   auto removeCode = std::make_shared<std::deque<icu::UnicodeString>>();
                   // sequential processing of generators
                   for (auto i = code->begin(); i != code->end(); i++) {
                       std::shared_ptr<ByteCodeParameter> isExpected;
                       if (!i->second->tryGetParameter(ElementParams::EXPECTED, isExpected) || !isExpected->value.byteValue) {
                           std::shared_ptr<ByteCodeParameter> isTemplate;
                           if (i->second->tryGetParameter(ElementParams::TEMPLATE, isTemplate) && isTemplate->value.byteValue) {
                               processTemplating(options, i->second, code, newCode, removeCode);
                           }
                       }
                   }

                   ctx->counters.removedNodes.incCounter(removeCode->size());
                   // sequential removal of destroyed ByteCodeBlocks
                   processRemovedElements(options, code, removeCode);
                   if (options->getProcessorSetup()->verbose) {
                       options->getLogger()->debug(StringUtils::spf(USS("Nodes removed: {0}"), removeCode->size()));
                   }

                   ctx->counters.addedNodes.incCounter(newCode->size());
                   // sequential addition of new ByteCodeBlocks
                   processNewElements(options, code, newCode);
                   if (options->getProcessorSetup()->verbose) {
                       options->getLogger()->debug(StringUtils::spf(USS("Nodes added: {0}"), newCode->size()));
                   }
               });
        
        std::vector<icu::UnicodeString> codeNames;
        for (auto i = code->begin(); i != code->end(); i++) {
            codeNames.push_back(i->first);
            
            // debug output
            if (options->getProcessorSetup()->verbose) {
                options->getLogger()->debug(StringUtils::spf(USS("State {0}:"), StringConvUtils::utow(i->first)));
                auto r = i->second->getParameters();
                for (auto k = std::get<0>(r); k != std::get<1>(r); k++) {
                    auto p = (*k);
                    switch (p->getType()) {
                        case ByteCodeParameterTypes::BYTE:
                            options->getLogger()->debug(StringUtils::spf(USS("{0} = {1}"), StringConvUtils::utow(p->getName()), p->value.byteValue));
                            break;
                        case ByteCodeParameterTypes::DOUBLE:
                            options->getLogger()->debug(StringUtils::spf(USS("{0} = {1}"), StringConvUtils::utow(p->getName()), p->value.doubleValue));
                            break;
                        case ByteCodeParameterTypes::INTEGER:
                            options->getLogger()->debug(StringUtils::spf(USS("{0} = {1}"), StringConvUtils::utow(p->getName()), p->value.integerValue));
                            break;
                        case ByteCodeParameterTypes::STRING:
                            options->getLogger()->debug(StringUtils::spf(USS("{0} = {1}"), StringConvUtils::utow(p->getName()), StringConvUtils::utow(p->value.stringValue)));
                            break;
                        case ByteCodeParameterTypes::_INTERNAL_OBJECT:
                            if (p->getName() == LinkParams::ACTIVATION + ElementParams::SOURCE_SUFFIX || p->getName() == LinkParams::STREAM + ElementParams::SOURCE_SUFFIX || p->getName() == LinkParams::GENERATION + ElementParams::SOURCE_SUFFIX) {
                                auto pv = std::static_pointer_cast<std::map<icu::UnicodeString, double>>(p->value.objectValue);
                                for (auto n = pv->begin(); n != pv->end(); n++) {
                                    options->getLogger()->debug(StringUtils::spf(USS("{0}.{1} = {2}"), StringConvUtils::utow(p->getName()), StringConvUtils::utow(n->first), n->second));
                                }
                            }
                            break;
                    }
                }
            }
        }
        
        return codeNames;
    }

    void Processor::processElements(std::shared_ptr<MachineSetup> options, std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock> > > code)
    {
        auto codeNames = prepareProcessElements(options, code);
        for (int64_t i = 0; i < codeNames.size(); i++) {
            auto b = code->operator[](codeNames[(unsigned int)i]);
            std::shared_ptr<ByteCodeParameter> isExpected;
            if (!b->tryGetParameter(ElementParams::EXPECTED, isExpected) || !isExpected->value.byteValue) {
                std::shared_ptr<ByteCodeParameter> isTemplate;
                if (!b->tryGetParameter(ElementParams::TEMPLATE, isTemplate) || !isTemplate->value.byteValue) {
                    processByType(options, b, code);
                }
            }
        }
    }

    void Processor::processElementsOMP(std::shared_ptr<MachineSetup> options, std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock> > > code)
    {
        auto codeNames = prepareProcessElements(options, code);
        #pragma omp parallel for shared(code, codeNames) schedule(dynamic)
        for (int64_t i = 0; i < codeNames.size(); i++) {
            auto b = code->operator[](codeNames[(unsigned int)i]);
            // no locks as each ByteCodeBlock processes its SOURCES and SUMS into a final SUM (or other neuron operations)
            std::shared_ptr<ByteCodeParameter> isExpected;
            if (!b->tryGetParameter(ElementParams::EXPECTED, isExpected) || !isExpected->value.byteValue) {
                std::shared_ptr<ByteCodeParameter> isTemplate;
                if (!b->tryGetParameter(ElementParams::TEMPLATE, isTemplate) || !isTemplate->value.byteValue) {
                    processByType(options, b, code);
                }
            }
        }
    }

    void Processor::processStep()
    {
        timeIt(ctx->counters.step, [this]()
               {
                    auto p = processing[options->getProcessorSetup()->parallelMode];
                    if (options->getProcessorSetup()->verbose) {
                        options->getLogger()->debug(StringUtils::spf(USS("Starting step {0}"), options->getProcessorSetup()->tick));
                    }
                    timeIt(ctx->counters.linkPhase, [this, &p]()
                           {
                                (this->*p.processLinksClean)(options, options->getProcessorSetup()->getCodes());
                                if (options->getProcessorSetup()->verbose) {
                                    options->getLogger()->debug(USS("Links cleaned"));
                                }
                                (this->*p.processLinks)(options, options->getProcessorSetup()->getCodes());
                                if (options->getProcessorSetup()->verbose) {
                                    options->getLogger()->debug(USS("Links refilled"));
                                }
                           });
                    timeIt(ctx->counters.nodePhase, [this, &p]()
                           {
                                (this->*p.processElementsClean)(options, options->getProcessorSetup()->getCodes());
                                if (options->getProcessorSetup()->verbose) {
                                    options->getLogger()->debug(USS("Nodes cleaned"));
                                }
                                (this->*p.processElements)(options, options->getProcessorSetup()->getCodes());
                                if (options->getProcessorSetup()->verbose) {
                                    options->getLogger()->debug(USS("Nodes processed"));
                                }
                           });
                   ctx->counters.stepCount++;
                   updateStatusCounters(options->getProcessorSetup()->getCodes());
               });
    }

    int8_t Processor::processExternalLinks(const std::map<icu::UnicodeString, std::map<icu::UnicodeString, std::tuple<double, double, double> > >& flow)
    {
        options->getProcessorSetup()->getCodes();
        //TODO:
    }

    void Processor::updateStatusCounters(std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock> > > code)
    {
        ctx->counters.uptime = PerformanceUtils::nanoseconds(created);
        ctx->counters.nodeCount = (int64_t) code->size();
        int64_t edgeCount = 0;
        for (auto & i : *code) {
            edgeCount += i.second->getIncomesCount();
        }
        ctx->counters.edgeCount = edgeCount;
    }

    void Processor::terminate()
    {
        // stop all async neurons
        for (auto i = options->getProcessorSetup()->getCodes()->begin(); i != options->getProcessorSetup()->getCodes()->end(); i++) {
            removeByType(options, i->second, options->getProcessorSetup()->getCodes());
            i->second->dispose();
        }
        options->getProcessorSetup()->getCodes()->clear();

        options->getLogger()->info(USS("Terminated all blocks"));
    }
}
