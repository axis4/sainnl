/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#pragma once

namespace nnvm {
    class VM;
}

#include <predef.h>
#include <map>
#include <unicode/unistr.h>
#include <memory>
#include <Disposable.hpp>
#include <ByteCode.hpp>
#include <ByteCodeLoader.hpp>
#include <MachineSetup.hpp>
#include "processor/Processor.hpp"
#include "Server.hpp"
#include "utils/ReleasePool.hpp"

namespace nnvm {
    class VM : public Disposable
    {
    public:
        explicit VM(std::map<icu::UnicodeString, icu::UnicodeString>& args, std::shared_ptr<Processor> processor = nullptr, std::shared_ptr<ByteCodeLoader> loader = nullptr, std::shared_ptr<MachineSetup> setup = nullptr);
        ~VM() override;
        int8_t execute();
    private:
        icu::UnicodeString mainPackage;
        ReleasePool pointerPool;
        std::shared_ptr<Context> ctx;
        std::shared_ptr<Processor> processor;
        std::shared_ptr<grid::Server> server;
        int8_t compiler;

        void readNetwork();
        int8_t runNetwork();
    };
}
