/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#include <Internationalization.hpp>
#include <exception>
#include <fstream>
#include <BinaryUtils.hpp>
#include "utils/Utils.hpp"

namespace nnvm {
    InternationalizationException::InternationalizationException(const icu::UnicodeString& reason):
        std::runtime_error(StringConvUtils::utou8(this->reason))
    {
        this->reason = reason;
    }

    Internationalization Internationalization::default_state()
    {
        auto s = produce(USS("i18n.txt"));
        return s;
    }

    std::map<icu::UnicodeString, Internationalization> Internationalization::cache;

    Internationalization Internationalization::produce(const icu::UnicodeString& file)
    {
        auto f = cache.find(file);
        if (f == cache.end()) {
            cache[file] = Internationalization(file);
        }
        return cache[file];
    }

    Internationalization::Internationalization() {}

    Internationalization::Internationalization(const icu::UnicodeString& file)
    {
        int64_t err;
        auto s = BinaryUtils::openTextStream(file, err);
        if (s->is_open()) {
            while (s->good()) {
                std::string l;
                std::getline(*s, l);
                if (s) {
                    std::vector<icu::UnicodeString> res;
                    if (l.size() > 0 && l.back() == '\r') {
                        l.pop_back();
                    }
                    StringUtils::split(res, StringConvUtils::u8tou(l), "=");
                    if (res.size() >= 2) {
                        auto val = res.begin();
                        std::advance(val, 1);
                        items[res[0]] = StringUtils::joinArray(std::vector<icu::UnicodeString>(val, res.end()), "=");
                    }
                }
            }
        }
    }

    Internationalization::Internationalization(const Internationalization& obj)
    {
        this->items = obj.items;
    }

    Internationalization::~Internationalization() = default;

    icu::UnicodeString Internationalization::operator[](const icu::UnicodeString& key)
    {
        auto f = items.find(key);
        if (f == items.end()) {
            return key;
        }
        return f->second;
    }
}
