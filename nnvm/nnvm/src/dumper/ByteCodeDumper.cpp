/*
 Copyright 2013-2016 Sergey Ionov

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#include "../../include/ByteCodeDumper.hpp"

namespace nnvm {
    ByteCodeDumper::ByteCodeDumper(std::shared_ptr<Context> ctx) : ctx(ctx)
    {
    }

    void ByteCodeDumper::add(icu::UnicodeString name, VMDumper dumper)
    {
        dumpers[name] = dumper;
    }

    int8_t ByteCodeDumper::dump(std::shared_ptr<ByteCode> code, int64_t tick) {
        int8_t res = 0;
        for (auto i = dumpers.begin(); i != dumpers.end(); i++) {
            res |= i->second(code, tick);
        }
        return res;
    }
}
