/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#include <ByteCodeParserSetup.hpp>

namespace nnvm {
    ByteCodeParserSetup::ByteCodeParserSetup()
    {
        safe();
    }

    ByteCodeParserSetup::ByteCodeParserSetup(const ByteCodeParserSetup& obj)
    {
        safe();
        obj.safe();
        this->libraryNeuron = obj.libraryNeuron;
        this->primitiveNeuron = obj.primitiveNeuron;
        this->additionNeuron = obj.additionNeuron;
        this->multiplicationNeuron = obj.multiplicationNeuron;
        for (auto i = obj.aliases.begin(); i != obj.aliases.end(); ++i) {
            this->aliases[i->first] = i->second;
        }
        for (auto i = obj.arithmeticNeuronNames.begin(); i != obj.arithmeticNeuronNames.end(); ++i) {
            this->arithmeticNeuronNames.insert(*i);
        }
        for (auto i = obj.neurons.begin(); i != obj.neurons.end(); ++i) {
            this->neurons[i->first] = i->second;
        }
    }
}
