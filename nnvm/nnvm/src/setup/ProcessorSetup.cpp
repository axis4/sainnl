/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#include <ProcessorSetup.hpp>
#include <set>
#include <ByteCodeException.hpp>
#include <Internationalization.hpp>
#include "../utils/Utils.hpp"

namespace nnvm {
    ProcessorSetup::ProcessorSetup() : ProcessorSetup(std::make_shared<Templater>()) { }

    ProcessorSetup::ProcessorSetup(const ProcessorSetup& obj)
    {
        safe();
        obj.safe();
        this->verbose = obj.verbose;
        this->exitCode = obj.exitCode;
        this->running = obj.running;
        this->tolerate_missing = obj.tolerate_missing;
        this->mode = obj.mode;
        this->parallelMode = obj.parallelMode;
        this->tick = obj.tick;
        this->templateStrategy = obj.templateStrategy;
        this->constants = obj.constants;
        for (auto i = obj.neurons.begin(); i != obj.neurons.end(); ++i) {
            this->neurons[i->first] = i->second;
        }
        this->remoteSync = obj.remoteSync;
        this->allowSync = obj.allowSync;
        this->allowDebug = obj.allowDebug;
        this->allowDistributed = obj.allowDistributed;
    }

    ProcessorSetup::ProcessorSetup(std::shared_ptr<Templater> templateStrategy)
            : templateStrategy(templateStrategy)
    {
        neurons = std::map<icu::UnicodeString, std::shared_ptr<ExecutableNeuron>>();
        verbose = 0;
        exitCode = 0;
        running = 0;
        tolerate_missing = 0;
        tick = 0;
        mode = ProcessorMode::REAL;
        parallelMode = ProcessorParallelMode::NONE;
        remoteSync = 0;
        allowSync = 0;
        allowDebug = 0;
        allowDistributed = 0;
    }

    ProcessorSetup::~ProcessorSetup()
    {
    }

    void ProcessorSetup::setConstantGetter(getConstant constants)
    {
        this->constants = constants;
    }

    std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock>>> ProcessorSetup::getCodes()
    {
        return running_codes;
    }

    void ProcessorSetup::preSetup(std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock>>>& codes)
    {
        running_codes = codes;
        std::vector<std::shared_ptr<ByteCodeParameter>> changes;
        for (auto i = codes->begin(); i != codes->end(); ++i) {
            auto p = i->second->getParameters();
            changes.clear();
            for (auto k = std::get<0>(p); k != std::get<1>(p); ++k) {
                double v;
                if (constants((*k)->value.stringValue, v)) {
                    changes.push_back(std::make_shared<ByteCodeParameter>((*k)->getName(), v));
                }
            }
            for (auto k = changes.begin(); k != changes.end(); ++k) {
                i->second->setParameter(*k);
            }
            auto t = i->second->getItems();
            for (auto j = std::get<0>(t); j != std::get<1>(t); ++j) {
                p = (*j)->getParameters();
                changes.clear();
                for (auto k = std::get<0>(p); k != std::get<1>(p); ++k) {
                    double v;
                    if (constants((*k)->value.stringValue, v)) {
                        changes.push_back(std::make_shared<ByteCodeParameter>((*k)->getName(), v));
                    }
                }
                for (auto k = changes.begin(); k != changes.end(); ++k) {
                    i->second->setParameter(*k);
                }
            }
        }
    }

    void ProcessorSetup::addNeuron(const icu::UnicodeString& name, std::shared_ptr<ExecutableNeuron> code)
    {
        neurons[name] = code;
    }

    std::shared_ptr<ExecutableNeuron> ProcessorSetup::getNeuron(const icu::UnicodeString& name)
    {
        auto f = neurons.find(name);
        if (f == neurons.end()) {
            throw ByteCodeException(StringUtils::spf(_i("err.plugin.missing"), StringConvUtils::utow(name)));
        }
        return f->second;
    }

    std::map<icu::UnicodeString, std::shared_ptr<ExecutableNeuron>>::iterator ProcessorSetup::getNeurons()
    {
        return neurons.begin();
    }

    std::map<icu::UnicodeString, std::shared_ptr<ExecutableNeuron>>::iterator ProcessorSetup::getNeuronsEnd()
    {
        return neurons.end();
    }

    std::shared_ptr<Templater> ProcessorSetup::getTemplateStrategy()
    {
        return templateStrategy;
    }

    void ProcessorSetup::offsetByMode(std::shared_ptr<ByteCodeParameter>& param)
    {
        std::shared_ptr<ByteCodeParameter> np = nullptr;
        ByteCodeParameterValue value = param->value;
        switch (this->mode)
        {
            case ProcessorMode::BYTE:
                np = std::make_shared<ByteCodeParameter>(param->getName(), (int8_t)value.doubleValue);
                break;
            case ProcessorMode::INTEGER:
                np = std::make_shared<ByteCodeParameter>(param->getName(), (int64_t)value.doubleValue);
                break;
            case ProcessorMode::REAL:
                np = std::make_shared<ByteCodeParameter>(param->getName(), (double)value.doubleValue);
                break;
            default:
                break;
        }
        param = np;
    }
}
