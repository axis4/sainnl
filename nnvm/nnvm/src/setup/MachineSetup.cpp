/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#include <MachineSetup.hpp>

namespace nnvm {
    class MachineSetupData
    {
    public:
        MachineSetupData(std::shared_ptr<ProcessorSetup> processorSetup, std::shared_ptr<ByteCodeLoaderSetup> loaderSetup, std::shared_ptr<ByteCodeParserSetup> parserSetup, std::shared_ptr<ByteCodeWriterSetup> writerSetup, std::shared_ptr<RemotingSetup> remotingSetup, std::vector<icu::UnicodeString> paths, std::istream& stdIn, std::ostream& stdOut)
                : pSet(processorSetup), bSet(loaderSetup), bpSet(parserSetup), bwSet(writerSetup), rSet(remotingSetup), libPaths(paths), stdIn(stdIn), stdOut(stdOut) { }

        std::vector<SampleSetting> defaultSampling;
        std::istream& stdIn;
        std::ostream& stdOut;
        std::shared_ptr<ProcessorSetup> pSet;
        std::shared_ptr<ByteCodeWriterSetup> bwSet;
        std::shared_ptr<ByteCodeLoaderSetup> bSet;
        std::shared_ptr<ByteCodeParserSetup> bpSet;
        std::shared_ptr<RemotingSetup> rSet;
        std::vector<icu::UnicodeString> libPaths;

        std::shared_ptr<std::map<icu::UnicodeString, std::tuple<int8_t, double>>> machineConstants;
    };

    MachineSetup::MachineSetup(std::shared_ptr<ProcessorSetup> processorSetup, std::shared_ptr<ByteCodeLoaderSetup> loaderSetup, std::shared_ptr<ByteCodeParserSetup> parserSetup, std::shared_ptr<ByteCodeWriterSetup> writerSetup, std::shared_ptr<RemotingSetup> remotingSetup, std::vector<icu::UnicodeString> paths, std::istream& stdIn, std::ostream& stdOut)
            : Setup(), data(std::make_shared<MachineSetupData>(processorSetup, loaderSetup, parserSetup, writerSetup, remotingSetup, paths, stdIn, stdOut))
    {
        setDefaultSampling();
        setMachineConstants();
    }

    MachineSetup::MachineSetup(std::shared_ptr<Logger> logger, std::shared_ptr<ProcessorSetup> processorSetup, std::shared_ptr<ByteCodeLoaderSetup> loaderSetup, std::shared_ptr<ByteCodeParserSetup> parserSetup, std::shared_ptr<ByteCodeWriterSetup> writerSetup, std::shared_ptr<RemotingSetup> remotingSetup, std::vector<icu::UnicodeString> paths, std::istream& stdIn, std::ostream& stdOut)
            : Setup(logger), data(std::make_shared<MachineSetupData>(processorSetup, loaderSetup, parserSetup, writerSetup, remotingSetup, paths, stdIn, stdOut))
    {
        setDefaultSampling();
        setMachineConstants();
    }

    std::shared_ptr<ProcessorSetup> MachineSetup::getProcessorSetup()
    {
        return this->data->pSet;
    }

    std::shared_ptr<ByteCodeWriterSetup> MachineSetup::getByteCodeWriterSetup()
    {
        return this->data->bwSet;
    }

    std::shared_ptr<ByteCodeLoaderSetup> MachineSetup::getByteCodeLoaderSetup()
    {
        return this->data->bSet;
    }

    std::shared_ptr<ByteCodeParserSetup> MachineSetup::getByteCodeParserSetup()
    {
        return this->data->bpSet;
    }

    std::shared_ptr<RemotingSetup> MachineSetup::getRemotingSetup()
    {
        return this->data->rSet;
    }

    std::vector<icu::UnicodeString> MachineSetup::getLibPaths()
    {
        return this->data->libPaths;
    }

    std::istream& MachineSetup::getStdIn()
    {
        return this->data->stdIn;
    }

    std::ostream& MachineSetup::getStdOut()
    {
        return this->data->stdOut;
    }

    std::vector<SampleSetting> MachineSetup::getDefaultSampling()
    {
        return this->data->defaultSampling;
    }

    void MachineSetup::setDefaultSampling()
    {
        this->data->defaultSampling = std::vector<SampleSetting> { SampleSetting(0.5, 0.5), SampleSetting(0.75, 0.5), SampleSetting(0.95, 0.5), SampleSetting(0.99, 0.5) };
    }

    void MachineSetup::setMachineConstants()
    {
        auto v = version();
        this->data->machineConstants = std::make_shared<std::map<icu::UnicodeString, std::tuple<int8_t, double>>>();
        this->data->machineConstants->operator[](USS("__MACHINE_MAJOR_VERSION")) = std::tuple<int8_t, double>(1, v.major);
        this->data->machineConstants->operator[](USS("__MACHINE_MINOR_VERSION")) = std::tuple<int8_t, double>(1, v.minor);
        this->data->machineConstants->operator[](USS("__COMPILER_MAJOR_VERSION")) = std::tuple<int8_t, double>(0, v.major);
        this->data->machineConstants->operator[](USS("__COMPILER_MINOR_VERSION")) = std::tuple<int8_t, double>(0, v.minor);
        this->data->machineConstants->operator[](USS("__ARCH")) = std::tuple<int8_t, double>(1, (double)arch());
        this->data->machineConstants->operator[](USS("__BUILD")) = std::tuple<int8_t, double>(1, (double)build());
        this->data->machineConstants->operator[](USS("__OS")) = std::tuple<int8_t, double>(1, (double)os());
        this->data->machineConstants->operator[](USS("__BIT")) = std::tuple<int8_t, double>(1, (double)bit());
        this->data->machineConstants->operator[](USS("__BIT_MULTIPLIER")) = std::tuple<int8_t, double>(1, bitMultiplier());
        switch (this->data->pSet->mode) {
            case ProcessorMode::BYTE:
                this->data->machineConstants->operator[](USS("__INF")) = std::tuple<int8_t, double>(1, std::numeric_limits<int8_t>::max());
                break;
            case ProcessorMode::INTEGER:
                this->data->machineConstants->operator[](USS("__INF")) = std::tuple<int8_t, double>(1, std::numeric_limits<int64_t>::max());
                break;
            case ProcessorMode::REAL:
                this->data->machineConstants->operator[](USS("__INF")) = std::tuple<int8_t, double>(1, std::numeric_limits<double>::max());
                break;
        }
        this->data->pSet->setConstantGetter(ProcessorSetup::getConstant(std::bind(&MachineSetup::getConstant, this, std::placeholders::_1, std::placeholders::_2)));
    }

    int8_t MachineSetup::getConstant(icu::UnicodeString name, double& value)
    {
        auto res = this->data->machineConstants->find(name);
        if (res == this->data->machineConstants->end()) {
            value = 0.0;
            return 0;
        }
        value = std::get<1>(res->second);
        return 1;
    }

    int8_t MachineSetup::isRuntimeConstant(icu::UnicodeString name)
    {
        return std::get<0>(this->data->machineConstants->operator[](name));
    }

    std::map<icu::UnicodeString, std::tuple<int8_t, double> >::const_iterator MachineSetup::getConstants()
    {
        return this->data->machineConstants->cbegin();
    }

    MachineVersion MachineSetup::version()
    {
        return MachineVersion { MACHINE_MAJOR_VERSION, MACHINE_MINOR_VERSION };
    }
    int8_t MachineSetup::versionsEqual(const MachineVersion& v1, const MachineVersion& v2)
    {
        return v1.major == v2.major && v1.minor == v2.minor;
    }
    int8_t MachineSetup::versionsCompatible(const MachineVersion& v1, const MachineVersion& v2)
    {
        return v1.major == v2.major;
    }
    MachineArch MachineSetup::arch()
    {
        // TODO: support all archs
        return MachineArch::X64;
    }
    MachineBuild MachineSetup::build()
    {
#ifdef U_DEBUG
        return MachineBuild::Debug;
#else
        return MachineBuild::Release;
#endif
    }
    MachineOS MachineSetup::os()
    {
#if U_PLATFORM == U_PF_AIX
        return MachineOS::AIX;
#elif U_PLATFORM == U_PF_ANDROID
        return MachineOS::ANDROID;
#elif defined(AMIGA)
        return MachineOS::AMIGA;
#elif defined(__BEOS__)
        return MachineOS::BEOS;
#elif defined(__bg__)
        return MachineOS::BLUE_GENE;
#elif defined(__bsdi__)
        return MachineOS::BSD;
#elif defined(__convex__)
        return MachineOS::CONVEX;
#elif U_PLATFORM == U_PF_LINUX
        return MachineOS::LINUX;
#elif defined(__hiuxmpp)
        return MachineOS::HI_UX;
#elif U_PLATFORM == U_PF_HPUX
        return MachineOS::HP_UX;
#elif U_PLATFORM == U_PF_OS400
        return MachineOS::OS400;
#elif defined(__Lynx__)
        return MachineOS::LYNX;
#elif U_PLATFORM == U_PF_DARWIN
        return MachineOS::OSX;
#elif U_PLATFORM == U_PF_IPHONE
        return MachineOS::IOS;
#elif defined(__minix)
        return MachineOS::MINIX;
#elif defined(__DOS__)
        return MachineOS::DOS;
#elif defined(__nucleus__)
        return MachineOS::NUCLEUS;
#elif defined(__OS2__)
        return MachineOS::OS2;
#elif defined(__palmos__)
        return MachineOS::PALM;
#elif defined(EPLAN9)
        return MachineOS::PLAN9;
#elif U_PLATFORM == U_PF_QNX
        return MachineOS::QNX;
#elif U_PLATFORM == U_PF_SOLARIS
        return MachineOS::SOLARIS;
#elif defined(__SYMBIAN32__)
        return MachineOS::SYMBIAN;
#elif U_PLATFORM == U_PF_WINDOWS
        return MachineOS::WINDOWS;
#elif defined(_WIN32_WCE)
        return MachineOS::WINDOWS_CE;
#elif defined(__MVS__)
        return MachineOS::ZOS;
#elif defined(__unix__)
        return MachineOS::UNIX;
#else
        // error condition
#endif
    }
    MachineBit MachineSetup::bit()
    {
#if INTPTR_MAX == INT8_MAX
        return MachineBit::X8;
#elif INTPTR_MAX == INT16_MAX
        return MachineBit::X16;
#elif INTPTR_MAX == INT32_MAX
        return MachineBit::X32;
#elif INTPTR_MAX == INT64_MAX
        return MachineBit::X64;
#else
        return MachineBit::X128;
#endif
    }
    int64_t MachineSetup::bitMultiplier()
    {
#if INTPTR_MAX == INT8_MAX
        return 8;
#elif INTPTR_MAX == INT16_MAX
        return 16;
#elif INTPTR_MAX == INT32_MAX
        return 32;
#elif INTPTR_MAX == INT64_MAX
        return 64;
#else
        return 128;
#endif
    }
}
