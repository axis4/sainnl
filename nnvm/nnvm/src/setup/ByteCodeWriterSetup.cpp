/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#include <ByteCodeWriterSetup.hpp>
#include <list>
#include <ByteCodeException.hpp>
#include <Internationalization.hpp>
#include "../utils/Utils.hpp"

namespace nnvm {
    ByteCodeWriterSetup::ByteCodeWriterSetup() { }

    ByteCodeWriterSetup::ByteCodeWriterSetup(const ByteCodeWriterSetup& obj)
    {
        safe();
        obj.safe();
        for (auto i = this->neurons.begin(); i != this->neurons.end(); ++i) {
            this->neurons[i->first] = i->second;
        }
    }

    ByteCodeWriterSetup::~ByteCodeWriterSetup() { }

    void ByteCodeWriterSetup::addNeuron(const icu::UnicodeString& name, std::shared_ptr<WritableNeuron> code)
    {
        neurons[name] = code;
    }

    std::shared_ptr<WritableNeuron> ByteCodeWriterSetup::getNeuron(const icu::UnicodeString& name)
    {
        auto f = neurons.find(name);
        if (f == neurons.end()) {
            throw ByteCodeException(StringUtils::spf(_i("err.plugin.missing"), StringConvUtils::utow(name)));
        }
        return f->second;
    }

    std::map<icu::UnicodeString, std::shared_ptr<WritableNeuron>>::iterator ByteCodeWriterSetup::getNeurons()
    {
        return neurons.begin();
    }

    std::map<icu::UnicodeString, std::shared_ptr<WritableNeuron>>::iterator ByteCodeWriterSetup::getNeuronsEnd()
    {
        return neurons.end();
    }
}
