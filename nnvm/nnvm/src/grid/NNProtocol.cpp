/*
 Copyright 2013-2016 Sergey Ionov

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

#include <NNProtocol.hpp>
#include <StringConv.hpp>
#include <Server.hpp>
#include <utility>
#include "../utils/Utils.hpp"
#include "../writer/ByteCodeWriter.hpp"

namespace nnvm {
    namespace grid {
        NNPayload::NNPayload()
                : NNPayload::NNPayload(NNPayload::P1, NNPayload::P2, MACHINE_MAJOR_VERSION, MACHINE_MINOR_VERSION, NNPayloadType::NOOP)
        { }
        NNPayload::NNPayload(const NNPayload& obj)
                : NNPayload::NNPayload(obj.pointer1, obj.pointer2, obj.majorVersion, obj.minorVersion, obj.type, obj.status, obj.state, obj.flow, obj.message)
        { }
        NNPayload::NNPayload(NNPayloadType type)
                : NNPayload::NNPayload(NNPayload::P1, NNPayload::P2, MACHINE_MAJOR_VERSION, MACHINE_MINOR_VERSION, type)
        { }
        NNPayload::NNPayload(NNStatus status)
                : NNPayload::NNPayload(NNPayload::P1, NNPayload::P2, MACHINE_MAJOR_VERSION, MACHINE_MINOR_VERSION, std::move(status))
        { }
        NNPayload::NNPayload(std::shared_ptr<ByteCode> state)
                : NNPayload::NNPayload(NNPayload::P1, NNPayload::P2, MACHINE_MAJOR_VERSION, MACHINE_MINOR_VERSION, std::move(state))
        { }
        NNPayload::NNPayload(NNFlow flow)
                : NNPayload::NNPayload(NNPayload::P1, NNPayload::P2, MACHINE_MAJOR_VERSION, MACHINE_MINOR_VERSION, std::move(flow))
        { }
        NNPayload::NNPayload(icu::UnicodeString message)
                : NNPayload::NNPayload(NNPayload::P1, NNPayload::P2, MACHINE_MAJOR_VERSION, MACHINE_MINOR_VERSION, message)
        { }
        NNPayload::NNPayload(int8_t pointer1, int32_t pointer2, int64_t majorVersion, int64_t minorVersion, NNPayloadType type)
                : pointer1(pointer1), pointer2(pointer2), majorVersion(majorVersion), minorVersion(minorVersion), type(type)
        { }
        NNPayload::NNPayload(int8_t pointer1, int32_t pointer2, int64_t majorVersion, int64_t minorVersion, NNStatus status)
                : NNPayload::NNPayload(pointer1, pointer2, majorVersion, minorVersion, NNPayloadType::STATUS_RESPONSE, std::move(status), nullptr, NNFlow(), USS(""))
        { }
        NNPayload::NNPayload(int8_t pointer1, int32_t pointer2, int64_t majorVersion, int64_t minorVersion, std::shared_ptr<ByteCode> state)
                : NNPayload::NNPayload(pointer1, pointer2, majorVersion, minorVersion, NNPayloadType::STATE_RESPONSE, NNStatus(), std::move(state), NNFlow(), USS(""))
        { }
        NNPayload::NNPayload(int8_t pointer1, int32_t pointer2, int64_t majorVersion, int64_t minorVersion, NNFlow flow)
                : NNPayload::NNPayload(pointer1, pointer2, majorVersion, minorVersion, NNPayloadType::FLOW, NNStatus(), nullptr, std::move(flow), USS(""))
        { }
        NNPayload::NNPayload(int8_t pointer1, int32_t pointer2, int64_t majorVersion, int64_t minorVersion, icu::UnicodeString message)
                : NNPayload::NNPayload(pointer1, pointer2, majorVersion, minorVersion, NNPayloadType::ACK, NNStatus(), nullptr, NNFlow(), message)
        { }
        NNPayload::NNPayload(int8_t pointer1, int32_t pointer2, int64_t majorVersion, int64_t minorVersion, NNPayloadType type, NNStatus status, std::shared_ptr<ByteCode> state, NNFlow flow, icu::UnicodeString message)
                : pointer1(pointer1), pointer2(pointer2), majorVersion(majorVersion), minorVersion(minorVersion), type(type), status(std::move(status)), state(std::move(state)), flow(std::move(flow)), message(message)
        { }

        int8_t NNPayload::validate()
        {
            return pointer1 == P1 && pointer2 == P2 && majorVersion == MACHINE_MAJOR_VERSION;
        }

        NNProtocol::NNProtocol(std::shared_ptr<Context> ctx) : CountedProtocol(std::move(ctx)) { }

        /*
         *
         *
         */

        void NNProtocol::onConnect(const Socket& connection)
        {
            this->safe();
            sendAck(connection);
        }

        void NNProtocol::onReceive(const Socket& connection, const NNPayload& buffer)
        {
            this->safe();
            switch (buffer.type) {
                case NNPayloadType::STATUS:
                    sendStructure(connection, NNPayload(fillStatus()));
                    break;
                case NNPayloadType::STATE:
                    if (ctx->setup->getProcessorSetup()->allowDebug) {
                        sendStructure(connection, NNPayload(ctx->executed));
                    }
                    else {
                        sendStructure(connection, NNPayload(_i("err.grid.debug_prohibited")));
                    }
                    break;
                case NNPayloadType::ENABLE_SYNC:
                    if (ctx->setup->getProcessorSetup()->allowSync) {
                        acceptEnableSync();
                        sendAck(connection);
                    }
                    else {
                        sendStructure(connection, NNPayload(_i("err.grid.remote_sync_prohibited")));
                    }
                    break;
                case NNPayloadType::SYNC_STEP:
                    if (ctx->setup->getProcessorSetup()->remoteSync) {
                        acceptSync();
                        sendAck(connection);
                    }
                    else {
                        sendStructure(connection, NNPayload(_i("err.grid.remote_sync_disabled")));
                    }
                    break;
                case NNPayloadType::FLOW:
                    if (ctx->setup->getProcessorSetup()->allowDistributed) {
                        acceptFlow(buffer.flow);
                        sendAck(connection);
                    }
                    else {
                        sendStructure(connection, NNPayload(_i("err.grid.distributed_flow_prohibited")));
                    }
                    break;
                case NNPayloadType::STATUS_RESPONSE:
                    acceptStatus(buffer.status);
                    sendAck(connection);
                    break;
                case NNPayloadType::STATE_RESPONSE:
                    acceptState(buffer.state);
                    sendAck(connection);
                    break;
                case NNPayloadType::NETWORK_SETUP:
                    if (ctx->setup->getProcessorSetup()->allowRemoteInit) {
                        if (ctx->setup->getProcessorSetup()->running && !ctx->setup->getProcessorSetup()->remoteSync) {
                            sendStructure(connection, NNPayload(_i("err.grid.initialize_already_running")));
                        }
                        else {
                            acceptInitialState(buffer.state);
                            sendAck(connection);
                        }
                    }
                    else {
                        sendStructure(connection, NNPayload(_i("err.grid.network_initialize_prohibited")));
                    }
                    break;
                case NNPayloadType::TERMINATE:
                    if (ctx->setup->getProcessorSetup()->remoteSync || ctx->setup->getProcessorSetup()->allowRemoteInit) {
                        acceptTerminate();
                    }
                    sendAck(connection);
                    break;
                case NNPayloadType::ACK:
                    acceptAck(buffer.message);
                    break;
                case NNPayloadType::NOOP:
                default:
                    break;
            }
        }

        void NNProtocol::onConnectError(const Socket& connection, const std::error_code &error)
        {
            safe();
            std::cout<<"Connect error\n";
        }

        void NNProtocol::onReceiveError(const Socket& connection, const std::error_code &error)
        {
            safe();
            std::cout<<"Receive error\n";
        }

        void NNProtocol::onSendEnd(const std::error_code &error, int64_t bytes_transferred, std::shared_ptr<std::vector<int8_t>> data)
        {
            safe();
            if (error) {
                std::cout << "Send error\n";
            }
        }

        void NNProtocol::sendAck(const Socket& connection)
        {
            safe();
            NNPayload p(USS("OK"));
            sendStructure(connection.getType(), StringConvUtils::u8tou(connection.getAddress()), connection.getPort(), p);
        }

        int8_t NNProtocol::doSerialize(const Socket& connection, const NNPayload& from, std::vector<int8_t>& to)
        {
            safe();
            auto ss = std::make_shared<std::stringstream>();
            if (!serialize(from.pointer1, ss)) {
                return 0;
            }
            if (!serialize(from.pointer2, ss)) {
                return 0;
            }
            if (!serialize(from.majorVersion, ss)) {
                return 0;
            }
            if (!serialize(from.minorVersion, ss)) {
                return 0;
            }
            if (!serialize((int16_t)from.type, ss)) {
                return 0;
            }
            switch (from.type)
            {
                case NNPayloadType::ACK:
                    if (!serialize(from.message, ss)) {
                        return 0;
                    }
                    break;
                case NNPayloadType::STATUS_RESPONSE:
                    if (!serialize(from.status, ss)) {
                        return 0;
                    }
                    break;
                case NNPayloadType::STATE_RESPONSE:
                    if (!serialize(from.state, ss)) {
                        return 0;
                    }
                    break;
                case NNPayloadType::FLOW:
                    if (!serialize(from.flow, ss)) {
                        return 0;
                    }
                    break;
                case NNPayloadType::NETWORK_SETUP:
                    if (!serialize(from.state, ss)) {
                        return 0;
                    }
                    break;
                case NNPayloadType::TERMINATE:
                case NNPayloadType::NOOP:
                case NNPayloadType::STATUS:
                case NNPayloadType::STATE:
                case NNPayloadType::SYNC_STEP:
                case NNPayloadType::ENABLE_SYNC:
                    // no payload
                    break;
            }

            BinaryUtils::asVector(*ss, to);
            return 1;
        }

        int8_t NNProtocol::serialize(const NNCounterStatus& from, std::shared_ptr<std::ostream> to)
        {
            safe();
            if (!serialize(from.min, to)) {
                return 0;
            }
            if (!serialize(from.max, to)) {
                return 0;
            }
            if (!serialize(from.avg, to)) {
                return 0;
            }
            if (!serialize(from.sum, to)) {
                return 0;
            }
            if (!serialize((int64_t)from.quantiles.size(), to)) {
                return 0;
            }
            for (auto quantile : from.quantiles) {
                if (!serialize(quantile.first, to)) {
                    return 0;
                }
                if (!serialize(quantile.second, to)) {
                    return 0;
                }
            }

            return 1;
        }

        int8_t NNProtocol::serialize(const NNStatus& from, std::shared_ptr<std::ostream> to)
        {
            safe();
            if (!serialize(from.verbose, to)) {
                return 0;
            }
            if (!serialize(from.exitCode, to)) {
                return 0;
            }
            if (!serialize(from.running, to)) {
                return 0;
            }
            if (!serialize(from.tolerate_missing, to)) {
                return 0;
            }
            if (!serialize(from.parallelMode, to)) {
                return 0;
            }
            if (!serialize(from.mode, to)) {
                return 0;
            }
            if (!serialize(from.remoteSync, to)) {
                return 0;
            }
            if (!serialize(from.allowSync, to)) {
                return 0;
            }
            if (!serialize(from.allowDebug, to)) {
                return 0;
            }
            if (!serialize(from.allowDistributed, to)) {
                return 0;
            }
            if (!serialize(from.allowRemoteInit, to)) {
                return 0;
            }

            if (!serialize(from.nodeCount, to)) {
                return 0;
            }
            if (!serialize(from.edgeCount, to)) {
                return 0;
            }
            if (!serialize(from.stepCount, to)) {
                return 0;
            }
            if (!serialize(from.uptime, to)) {
                return 0;
            }
            if (!serialize(from.stepAll, to)) {
                return 0;
            }
            if (!serialize(from.stepTemplate, to)) {
                return 0;
            }
            if (!serialize(from.stepLinks, to)) {
                return 0;
            }
            if (!serialize(from.stepNodes, to)) {
                return 0;
            }
            if (!serialize(from.node, to)) {
                return 0;
            }
            if (!serialize(from.addedNodes, to)) {
                return 0;
            }
            if (!serialize(from.removedNodes, to)) {
                return 0;
            }

            if (!serialize((int64_t)from.nodes.size(), to)) {
                return 0;
            }
            for (const auto &node : from.nodes) {
                if (!serialize(node.first, to)) {
                    return 0;
                }
                if (!serialize(node.second, to)) {
                    return 0;
                }
            }

            return 1;
        }

        int8_t NNProtocol::serialize(const std::shared_ptr<ByteCode>& from, std::shared_ptr<std::ostream> to)
        {
            safe();
            return ByteCodeWriter(std::move(to), ctx).write(from) ? (int8_t)0 : (int8_t)1;
        }

        int8_t NNProtocol::serialize(const NNFlow& from, std::shared_ptr<std::ostream> to)
        {
            safe();
            if (!serialize((int64_t)from.destinations.size(), to)) {
                return 0;
            }
            for (const auto &destination : from.destinations) {
                if (!serialize(destination.first, to)) {
                    return 0;
                }
                if (!serialize((int64_t) destination.second.size(), to)) {
                    return 0;
                }
                for (const auto &k : destination.second) {
                    if (!serialize(k.first, to)) {
                        return 0;
                    }
                    if (!serialize(k.second.activation, to)) {
                        return 0;
                    }
                    if (!serialize(k.second.stream, to)) {
                        return 0;
                    }
                    if (!serialize(k.second.generation, to)) {
                        return 0;
                    }
                }
            }

            return 1;
        }

        int8_t NNProtocol::serialize(const icu::UnicodeString& from, std::shared_ptr<std::ostream> to)
        {
            safe();
            return BinaryUtils::writeString(from, to);
        }

        int8_t NNProtocol::serialize(const int8_t & from, std::shared_ptr<std::ostream> to)
        {
            safe();
            return BinaryUtils::writeByte(from, to);
        }

        int8_t NNProtocol::serialize(const int64_t& from, std::shared_ptr<std::ostream> to)
        {
            safe();
            return BinaryUtils::writeInteger(from, to);
        }

        int8_t NNProtocol::serialize(const int32_t& from, std::shared_ptr<std::ostream> to)
        {
            safe();
            return BinaryUtils::writeInteger(from, to);
        }

        int8_t NNProtocol::serialize(const int16_t& from, std::shared_ptr<std::ostream> to)
        {
            safe();
            return BinaryUtils::writeInteger(from, to);
        }

        int8_t NNProtocol::serialize(const double& from, std::shared_ptr<std::ostream> to)
        {
            safe();
            return BinaryUtils::writeDouble(from, to);
        }

        int8_t NNProtocol::doDeserialize(const Socket& connection, const std::vector<int8_t>& from, NNPayload& to)
        {
            safe();
            auto ss = std::make_shared<std::stringstream>();
            BinaryUtils::asStream(from, *ss);
            if (!deserialize(ss, to.pointer1)) {
                return 0;
            }
            if (!deserialize(ss, to.pointer2)) {
                return 0;
            }
            if (!deserialize(ss, to.majorVersion)) {
                return 0;
            }
            if (!deserialize(ss, to.minorVersion)) {
                return 0;
            }
            if (!to.validate()) {
                return 0;
            }

            int16_t tmp;
            if (!deserialize(ss, tmp)) {
                return 0;
            }
            to.type = (NNPayloadType) tmp;

            switch (to.type)
            {
                case NNPayloadType::ACK:
                    if (!deserialize(ss, to.message)) {
                        return 0;
                    }
                    break;
                case NNPayloadType::STATUS_RESPONSE:
                    if (!deserialize(ss, to.status)) {
                        return 0;
                    }
                    break;
                case NNPayloadType::STATE_RESPONSE:
                    if (!deserialize(ss, to.state)) {
                        return 0;
                    }
                    break;
                case NNPayloadType::FLOW:
                    if (!deserialize(ss, to.flow)) {
                        return 0;
                    }
                    break;
                case NNPayloadType::NETWORK_SETUP:
                    if (!deserialize(ss, to.state)) {
                        return 0;
                    }
                    break;
                case NNPayloadType::TERMINATE:
                case NNPayloadType::NOOP:
                case NNPayloadType::STATUS:
                case NNPayloadType::STATE:
                case NNPayloadType::SYNC_STEP:
                case NNPayloadType::ENABLE_SYNC:
                    // no payload
                    break;
            }

            return 1;
        }

        int8_t NNProtocol::deserialize(std::shared_ptr<std::istream> from, NNCounterStatus& to)
        {
            safe();
            if (!deserialize(from, to.min)) {
                return 0;
            }
            if (!deserialize(from, to.max)) {
                return 0;
            }
            if (!deserialize(from, to.avg)) {
                return 0;
            }
            if (!deserialize(from, to.sum)) {
                return 0;
            }
            int64_t count;
            if (!deserialize(from, count)) {
                return 0;
            }
            for (auto i = 0; i < count; ++i) {
                double key;
                double value;
                if (!deserialize(from, key)) {
                    return 0;
                }
                if (!deserialize(from, value)) {
                    return 0;
                }
                to.quantiles[key] = value;
            }

            return 1;
        }

        int8_t NNProtocol::deserialize(std::shared_ptr<std::istream> from, NNStatus& to)
        {
            safe();
            if (!deserialize(from, to.verbose)) {
                return 0;
            }
            if (!deserialize(from, to.exitCode)) {
                return 0;
            }
            if (!deserialize(from, to.running)) {
                return 0;
            }
            if (!deserialize(from, to.tolerate_missing)) {
                return 0;
            }
            if (!deserialize(from, to.parallelMode)) {
                return 0;
            }
            if (!deserialize(from, to.mode)) {
                return 0;
            }
            if (!deserialize(from, to.remoteSync)) {
                return 0;
            }
            if (!deserialize(from, to.allowSync)) {
                return 0;
            }
            if (!deserialize(from, to.allowDebug)) {
                return 0;
            }
            if (!deserialize(from, to.allowDistributed)) {
                return 0;
            }
            if (!deserialize(from, to.allowRemoteInit)) {
                return 0;
            }

            if (!deserialize(from, to.nodeCount)) {
                return 0;
            }
            if (!deserialize(from, to.edgeCount)) {
                return 0;
            }
            if (!deserialize(from, to.stepCount)) {
                return 0;
            }
            if (!deserialize(from, to.uptime)) {
                return 0;
            }
            if (!deserialize(from, to.stepAll)) {
                return 0;
            }
            if (!deserialize(from, to.stepTemplate)) {
                return 0;
            }
            if (!deserialize(from, to.stepLinks)) {
                return 0;
            }
            if (!deserialize(from, to.stepNodes)) {
                return 0;
            }
            if (!deserialize(from, to.node)) {
                return 0;
            }
            if (!deserialize(from, to.addedNodes)) {
                return 0;
            }
            if (!deserialize(from, to.removedNodes)) {
                return 0;
            }

            int64_t count;
            if (!deserialize(from, count)) {
                return 0;
            }
            for (auto i = 0; i < count; ++i) {
                icu::UnicodeString key;
                if (!deserialize(from, key)) {
                    return 0;
                }
                NNCounterStatus value;
                if (!deserialize(from, value)) {
                    return 0;
                }
                to.nodes[key] = value;
            }

            return 1;
        }

        int8_t NNProtocol::deserialize(std::shared_ptr<std::istream> from, std::shared_ptr<ByteCode>& to)
        {
            safe();
            std::vector<int8_t> tmp;
            BinaryUtils::asVector(*from, tmp);
            auto ss = std::make_shared<std::stringstream>();
            BinaryUtils::asStream(tmp, *ss);

            auto res = ByteCodeParser(ss, USS(""), USS(""), ctx).read();
            if (res && !res->empty()) {
                to = res->at(0);
                return 1;
            }

            return 0;
        }

        int8_t NNProtocol::deserialize(std::shared_ptr<std::istream> from, NNFlow& to)
        {
            safe();
            int64_t count;
            if (!deserialize(from, count)) {
                return 0;
            }
            for (auto i = 0; i < count; ++i) {
                icu::UnicodeString key;
                if (!deserialize(from, key)) {
                    return 0;
                }
                int64_t itemCount;
                if (!deserialize(from, itemCount)) {
                    return 0;
                }
                for (auto k = 0; k < itemCount; ++k) {
                    icu::UnicodeString key_from;
                    NNFlowItem value{};
                    if (!deserialize(from, key_from)) {
                        return 0;
                    }
                    if (!deserialize(from, value.activation)) {
                        return 0;
                    }
                    if (!deserialize(from, value.stream)) {
                        return 0;
                    }
                    if (!deserialize(from, value.generation)) {
                        return 0;
                    }
                    to.destinations[key][key_from] = value;
                }
            }

            return 1;
        }

        int8_t NNProtocol::deserialize(std::shared_ptr<std::istream> from, icu::UnicodeString& to)
        {
            safe();
            std::vector<uint8_t> tmp;
            return BinaryUtils::readString(*from, to, tmp);
        }

        int8_t NNProtocol::deserialize(std::shared_ptr<std::istream> from, int64_t& to)
        {
            safe();
            std::vector<uint8_t> tmp;
            return BinaryUtils::readInteger(*from, to, tmp);
        }

        int8_t NNProtocol::deserialize(std::shared_ptr<std::istream> from, int32_t& to)
        {
            safe();
            std::vector<uint8_t> tmp;
            return BinaryUtils::readInteger(*from, to, tmp);
        }

        int8_t NNProtocol::deserialize(std::shared_ptr<std::istream> from, int16_t& to)
        {
            safe();
            std::vector<uint8_t> tmp;
            return BinaryUtils::readInteger(*from, to, tmp);
        }

        int8_t NNProtocol::deserialize(std::shared_ptr<std::istream> from, int8_t& to)
        {
            safe();
            return BinaryUtils::readByte(*from, to);
        }

        int8_t NNProtocol::deserialize(std::shared_ptr<std::istream> from, double& to)
        {
            safe();
            std::vector<uint8_t> tmp;
            return BinaryUtils::readDouble(*from, to, tmp);
        }

        NNCounterStatus NNProtocol::fillCounterStatus(Counter c)
        {
            safe();
            auto res = NNCounterStatus();
            res.avg = c.avg;
            res.max = c.max;
            res.min = c.min;
            res.sum = c.sum;
            auto q = c.quantiles();
            for (auto &i : q) {
                res.quantiles[i.quantile] = i.value;
            }
            return res;
        }

        NNStatus NNProtocol::fillStatus()
        {
            safe();
            auto c = ctx->counters;
            auto res = NNStatus();
            res.verbose = ctx->setup->getProcessorSetup()->verbose;
            res.exitCode = ctx->setup->getProcessorSetup()->exitCode;
            res.running = ctx->setup->getProcessorSetup()->running;
            res.tolerate_missing = ctx->setup->getProcessorSetup()->tolerate_missing;
            res.parallelMode = (int32_t) ctx->setup->getProcessorSetup()->parallelMode;
            res.mode = (int32_t) ctx->setup->getProcessorSetup()->mode;
            res.remoteSync = ctx->setup->getProcessorSetup()->remoteSync;
            res.allowSync = ctx->setup->getProcessorSetup()->allowSync;
            res.allowDebug = ctx->setup->getProcessorSetup()->allowDebug;
            res.allowDistributed = ctx->setup->getProcessorSetup()->allowDistributed;
            res.allowRemoteInit = ctx->setup->getProcessorSetup()->allowRemoteInit;
            res.nodeCount = c.nodeCount;
            res.edgeCount = c.edgeCount;
            res.stepCount = c.stepCount;
            res.uptime = c.uptime;
            res.stepAll = fillCounterStatus(c.step);
            res.stepTemplate = fillCounterStatus(c.templatePhase);
            res.stepLinks = fillCounterStatus(c.linkPhase);
            res.stepNodes = fillCounterStatus(c.nodePhase);
            res.node = fillCounterStatus(c.eachNode);
            res.addedNodes = fillCounterStatus(c.addedNodes);
            res.removedNodes = fillCounterStatus(c.removedNodes);
            for (auto &node : c.nodes) {
                res.nodes[node.first] = fillCounterStatus(node.second);
            }
            return res;
        }

        NNFlow NNProtocol::fillFlow(const std::map<icu::UnicodeString, std::vector<std::tuple<icu::UnicodeString, double, double, double> > >& streamBatch)
        {
            safe();
            auto res = NNFlow();
            for (const auto &i : streamBatch) {
                for (auto k = i.second.begin(); k != i.second.end(); ++k) {
                    NNFlowItem fi{};
                    fi.activation = std::get<1>(*k);
                    fi.stream = std::get<2>(*k);
                    fi.generation = std::get<3>(*k);
                    res.destinations[i.first][std::get<0>(*k)] = fi;
                }
            }
            return res;
        }

        int8_t NNProtocol::acceptEnableSync()
        {
            safe();
            ctx->setup->getProcessorSetup()->remoteSync = 1;
            return 1;
        }

        int8_t NNProtocol::acceptSync()
        {
            safe();
            ctx->globalRelease();
            return 1;
        }

        int8_t NNProtocol::acceptTerminate()
        {
            safe();
            ctx->setup->getProcessorSetup()->running = 0;
            ctx->setup->getProcessorSetup()->exitCode = 0;
            ctx->globalRelease();
            return 1;
        }

        int8_t NNProtocol::acceptFlow(const NNFlow& flow)
        {
            safe();
            std::map<icu::UnicodeString, std::map<icu::UnicodeString, std::tuple<double, double, double> > > res;
            for (const auto &destination : flow.destinations) {
                std::map<icu::UnicodeString, std::tuple<double, double, double> > to;
                for (const auto &k : destination.second) {
                    std::tuple<double, double, double> f(k.second.activation, k.second.stream, k.second.generation);
                    to[k.first] = f;
                }
                res[destination.first] = to;
            }
            return ctx->processExternalLinks(res);
        }

        int8_t NNProtocol::acceptAck(const icu::UnicodeString& message)
        {
            // not a runtime requirement
            return 1;
        }

        int8_t NNProtocol::acceptStatus(const NNStatus& status)
        {
            // not a runtime requirement
            return 1;
        }

        int8_t NNProtocol::acceptState(std::shared_ptr<ByteCode> state)
        {
            // not a runtime requirement
            return 1;
        }

        int8_t NNProtocol::acceptInitialState(std::shared_ptr<ByteCode> state)
        {
            safe();
            ctx->executed = std::move(state);
            ctx->globalRelease();
            return 1;
        }
    }
}
