/*
 Copyright 2013-2016 Sergey Ionov

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

#define ASIO_STANDALONE

#include <Server.hpp>
#include <StringUtils.hpp>
#include <ByteCodeUtils.hpp>
#include <inttypes.h>
#include <utility>
#include <vector>
#include <numeric>
#include <asio.hpp>
#include "../utils/Utils.hpp"
#include "../utils/ThreadUtils.hpp"

namespace nnvm {
    namespace grid {
        const int64_t RECEIVE_BYTES = 4096;

        class UdpConnection : public Connection, public std::enable_shared_from_this<UdpConnection>
        {
        public:
            UdpConnection(
                    const std::shared_ptr<Logger>& logger,
                    const std::shared_ptr<Protocol>& protocol,
                    std::shared_ptr<asio::ip::udp::socket> socket,
                    asio::ip::udp::endpoint remoteEndpoint,
                    std::shared_ptr<asio::io_service> io_service)
                    : socket(std::move(socket)),
                      remoteEndpoint(std::move(remoteEndpoint)),
                      logger(logger),
                      protocol(protocol),
                      io_service(std::move(io_service)) {}

            ~UdpConnection() override
            {
                stop();
            }

            void start() override
            {
                safe();
            }

            void stop() override
            {
                safe();
                socket->cancel();
                socket->close();
            }
            std::shared_ptr<asio::ip::udp::socket> getSocket()
            {
                safe();
                return socket;
            }

            std::string getRemoteAddress()
            {
                safe();
                return remoteEndpoint.address().to_string();
            }
            int32_t getRemotePort()
            {
                safe();
                return remoteEndpoint.port();
            }
            Socket getRemote() override
            {
                safe();
                return Socket(getRemoteAddress(), getRemotePort(), ProtocolType::UDP);
            }
            std::string getLocalAddress()
            {
                safe();
                return socket->local_endpoint().address().to_string();
            }
            int32_t getLocalPort()
            {
                safe();
                return socket->local_endpoint().port();
            }
            Socket getLocal() override
            {
                safe();
                return Socket(getLocalAddress(), getLocalPort(), ProtocolType::UDP);
            }
            void handleSend(std::shared_ptr<std::vector<int8_t>> data) override
            {
                safe();
                PerformanceUtils::timeit([this, data]() {
                    if (!data->empty()) {
                        socket->async_send_to(
                                asio::buffer(*data),
                                remoteEndpoint,
                                [this, data](const asio::error_code &error, size_t bytes_transferred) {
                                    handleWrite(error, bytes_transferred, data);
                                });
                    }
                }, [this](int64_t reg) {
                    //TODO save in counters
                    logger->info(StringUtils::spf(
                            USS("Counted handleSend in {0} ms"), reg/1000000.0));
                });
            }

        protected:
            void handleWrite(
                    const asio::error_code& error,
                    size_t bytes_transferred,
                    std::shared_ptr<std::vector<int8_t>> data)
            {
                safe();
                if (auto plock = protocol.lock()) {
                    plock->handleSendEnd(error, (int64_t) bytes_transferred, std::move(data));
                }
            }

            std::shared_ptr<Logger> logger;
            std::weak_ptr<Protocol> protocol;
            std::shared_ptr<asio::ip::udp::socket> socket;
            asio::ip::udp::endpoint remoteEndpoint;
            std::shared_ptr<asio::io_service> io_service;
        };

        class TcpConnection : public Connection, public std::enable_shared_from_this<TcpConnection>
        {
        public:
            TcpConnection(
                    const std::shared_ptr<Logger>& logger,
                    const std::shared_ptr<Protocol>& protocol,
                    std::shared_ptr<asio::ip::tcp::socket> socket,
                    std::shared_ptr<asio::io_service> io_service)
                    : socket(std::move(socket)), logger(logger), protocol(protocol), io_service(std::move(io_service))
            {
            }

            TcpConnection(
                    const std::shared_ptr<Logger>& logger,
                    const std::shared_ptr<Protocol>& protocol,
                    std::shared_ptr<asio::io_service> io_service)
                    : socket(nullptr), logger(logger), protocol(protocol), io_service(io_service)
            {
                socket = std::make_shared<asio::ip::tcp::socket>(*io_service);
            }

            ~TcpConnection() override
            {
                stop();
            }

            void start() override
            {
                safe();
                startReceive();
            }
            void stop() override
            {
                safe();
                logger->info(StringUtils::spf(
                        USS("Closing {0}:{1}"),
                        socket->remote_endpoint().address().to_string(),
                        socket->remote_endpoint().port()));
                socket->cancel();
                socket->close();
            }
            std::shared_ptr<asio::ip::tcp::socket> getSocket()
            {
                safe();
                return socket;
            }

            std::string getRemoteAddress()
            {
                safe();
                return socket->remote_endpoint().address().to_string();
            }
            int32_t getRemotePort()
            {
                safe();
                return socket->remote_endpoint().port();
            }
            Socket getRemote() override
            {
                safe();
                return Socket(getRemoteAddress(), getRemotePort(), ProtocolType::TCP);
            }
            std::string getLocalAddress()
            {
                safe();
                return socket->local_endpoint().address().to_string();
            }
            int32_t getLocalPort()
            {
                safe();
                return socket->local_endpoint().port();
            }
            Socket getLocal() override
            {
                safe();
                return Socket(getLocalAddress(), getLocalPort(), ProtocolType::TCP);
            }
            void handleSend(std::shared_ptr<std::vector<int8_t>> data) override
            {
                safe();
                PerformanceUtils::timeit([this, data]() {
                    if (!data->empty()) {
                        auto sthis = shared_from_this();
                        asio::async_write(*socket,
                                          asio::buffer(*data),
                                          [sthis, data](const asio::error_code &error, size_t bytes_transferred) {
                                              sthis->handleWrite(error, bytes_transferred, data);
                                          });
                    }
                }, [this](int64_t reg) {
                    //TODO save in counters
                    logger->info(StringUtils::spf(
                            USS("Counted handleSend in {0} ms"), reg/1000000.0));
                });
            }

        protected:
            void startReceive()
            {
                safe();
                auto data = std::make_shared<std::vector<int8_t>>(RECEIVE_BYTES, 0);
                auto sthis = shared_from_this();
                socket->async_receive(
                        asio::buffer(*data),
                        [sthis, data](const asio::error_code &error, size_t bytes_transferred) {
                            if (bytes_transferred > 0) {
                                sthis->handleReceive(error, bytes_transferred, data);
                            }
                            sthis->startReceive();
                        });
            }

            void handleReceive(const asio::error_code& error, size_t bytes_transferred, std::shared_ptr<std::vector<int8_t>> data)
            {
                safe();
                logger->info(USS("Receiving"));
                PerformanceUtils::timeit([this, &error, &bytes_transferred, data]() {
                    if (auto plock = protocol.lock()) {
                        plock->handleReceive(
                                shared_from_this(),
                                std::vector<int8_t>(
                                        data->begin(),
                                        data->begin() + std::min(bytes_transferred, data->size())),
                                error);
                    }
                }, [this](int64_t reg) {
                    //TODO save in counters
                    logger->info(StringUtils::spf(
                            USS("Counted handleReceive in {0} ms"), reg/1000000.0));
                });
            }

            void handleWrite(const asio::error_code& error, size_t bytes_transferred, std::shared_ptr<std::vector<int8_t>> data)
            {
                safe();
                if (auto plock = protocol.lock()) {
                    plock->handleSendEnd(error, (int64_t) bytes_transferred, std::move(data));
                }
            }

            std::shared_ptr<Logger> logger;
            std::weak_ptr<Protocol> protocol;
            std::shared_ptr<asio::ip::tcp::socket> socket;
            std::shared_ptr<asio::io_service> io_service;
        };

        class TcpServer : public Disposable, public std::enable_shared_from_this<TcpServer>
        {
        public:
            TcpServer(
                    icu::UnicodeString address,
                    int32_t port,
                    std::shared_ptr<Context> ctx,
                    std::shared_ptr<Protocol> protocol,
                    std::shared_ptr<asio::io_service> io_service)
                    : acceptor(*io_service, asio::ip::tcp::endpoint(asio::ip::tcp::v4(), (unsigned short)port), true),
                      ctx(std::move(ctx)),
                      protocol(std::move(protocol)),
                      io_service(io_service),
                      inAccept(0)
            {
            }

            int8_t ready()
            {
                safe();
                return inAccept;
            }

            void start()
            {
                safe();
                startAccept();
            }
            void stop()
            {
                safe();
                acceptor.cancel();
                acceptor.close();
            }

        private:
            void startAccept()
            {
                safe();
                auto socket = std::make_shared<asio::ip::tcp::socket>(*io_service);
                auto sthis = shared_from_this();
                acceptor.async_accept(
                        *socket,
                        [sthis, socket](const asio::error_code &error) {
                            asio::ip::tcp::no_delay option(true);
                            socket->set_option(option);
                            auto connect = std::make_shared<TcpConnection>(
                                    sthis->ctx->setup->getLogger(),
                                    sthis->protocol,
                                    socket,
                                    sthis->io_service);
                            connect->start();
                            sthis->handleAccept(connect, error);
                        });
                inAccept = 1;
            }

            void handleAccept(std::shared_ptr<Connection> newConnection, const asio::error_code& error)
            {
                safe();
                ctx->setup->getLogger()->info(USS("Accepting"));
                protocol->handleConnect(std::move(newConnection), error);
                startAccept();
            }

            std::shared_ptr<Context> ctx;
            std::shared_ptr<Protocol> protocol;
            asio::ip::tcp::acceptor acceptor;
            std::shared_ptr<asio::io_service> io_service;
            int8_t inAccept;
        };

        class UdpServer : public Disposable, public std::enable_shared_from_this<UdpServer>
        {
        public:
            UdpServer(
                    icu::UnicodeString address,
                    int32_t port,
                    std::shared_ptr<Context> ctx,
                    std::shared_ptr<Protocol> protocol,
                    std::shared_ptr<asio::io_service> io_service)
                    : socket(std::make_shared<asio::ip::udp::socket>(*io_service, asio::ip::udp::endpoint(asio::ip::udp::v4(), (unsigned short)port))),
                      ctx(std::move(ctx)),
                      protocol(std::move(protocol)),
                      io_service(io_service),
                      inReceive(0)
            {
            }

            int8_t ready()
            {
                safe();
                return inReceive;
            }

            void start()
            {
                safe();
                startReceive();
            }
            void stop()
            {
                safe();
                socket->cancel();
                socket->close();
            }

        private:
            void startReceive()
            {
                safe();
                if (socket->is_open()) {
                    auto data = std::make_shared<std::vector<int8_t>>(RECEIVE_BYTES);
                    auto sthis = shared_from_this();
                    socket->async_receive_from(
                            asio::buffer(*data),
                            remoteEndpoint,
                            [sthis, data](const asio::error_code &error, size_t bytes_transferred) {
                                if (bytes_transferred > 0) {
                                    sthis->handleReceive(error, bytes_transferred, data);
                                }
                                sthis->startReceive();
                            });
                    inReceive = 1;
                }
            }

            void handleReceive(const asio::error_code& error, size_t bytes_transferred, std::shared_ptr<std::vector<int8_t>> data)
            {
                safe();
                protocol->handleReceive(
                        std::make_shared<UdpConnection>(
                                ctx->setup->getLogger(),
                                protocol,
                                socket,
                                remoteEndpoint,
                                io_service),
                        std::vector<int8_t>(
                                data->begin(),
                                data->begin() + std::min(bytes_transferred, data->size())),
                        error);
            }

            std::shared_ptr<Context> ctx;
            std::shared_ptr<Protocol> protocol;
            std::shared_ptr<asio::ip::udp::socket> socket;
            asio::ip::udp::endpoint remoteEndpoint;
            std::shared_ptr<asio::io_service> io_service;
            int8_t inReceive;
        };

        void nop() { }

        int8_t falseop() { return 0; }
        int8_t trueop() { return 1; }

        std::shared_ptr<asio::io_service> fromServerData(std::shared_ptr<void> data)
        {
            return std::static_pointer_cast<asio::io_service>(data);
        }

        Server::Server(std::shared_ptr<Context> ctx, std::shared_ptr<Protocol> protocol)
                : ctx(ctx),
                  logger(ctx->setup->getLogger()),
                  protocol(protocol),
                  stopAction(&nop),
                  serverData(ServerContext()),
                  running(0),
                  servicesUp(0),
                  threadsUp(0),
                  stopping(0),
                  runningSize(static_cast<unsigned long>(ctx->setup->getRemotingSetup()->poolSize), 0),
                  previousSum(0)
        {
            serverData.data = std::static_pointer_cast<void>(std::make_shared<asio::io_service>());
            protocol->setRunningContext(serverData);
            serviceThread = std::thread(&Server::runServer, this);
            ThreadUtils::wait(1);
        }

        Server::Server(const Server&) {}

        void Server::operator=(const Server&) {}

        Server::~Server()
        {
            stop();
            if (serviceThread.joinable()) {
                serviceThread.join();
            }
            logger->debug(USS("Stopped RunServer"));
        }

        int8_t Server::isRunning()
        {
            safe();
            return running && servicesUp && threadsUp;
        }

        void Server::stop()
        {
            safe();
            stopping = 1;
            stopAction();
        }

        void Server::runServer()
        {
            safe();
            auto serverNaming = StringUtils::spf("server at {0}:{1}",
                                                 StringConvUtils::utow(ctx->setup->getRemotingSetup()->listenAddress),
                                                 ctx->setup->getRemotingSetup()->listenPort);
            logger->info(StringUtils::spf(USS("Starting {0}"), serverNaming));
            try {
                if (serverData.data == nullptr) {
                    logger->info(StringUtils::spf(USS("Broken {0}"), serverNaming));
                    return;
                }

                auto io_service = fromServerData(serverData.data);
                running = 1;
                auto tcpServer = std::make_shared<TcpServer>(ctx->setup->getRemotingSetup()->listenAddress,
                                                             ctx->setup->getRemotingSetup()->listenPort,
                                                             ctx,
                                                             protocol,
                                                             io_service);
                auto udpServer = std::make_shared<UdpServer>(ctx->setup->getRemotingSetup()->listenAddress,
                                                             ctx->setup->getRemotingSetup()->listenPort,
                                                             ctx,
                                                             protocol,
                                                             io_service);
                tcpServer->start();
                udpServer->start();
                PerformanceUtils::timeit([this, tcpServer, udpServer]() {
                    ThreadUtils::spinWait([tcpServer, this]() { return tcpServer->ready() || stopping; });
                    ThreadUtils::spinWait([udpServer, this]() { return udpServer->ready() || stopping; });
                }, [this, serverNaming](int64_t reg) {
                    //TODO save in counters
                    logger->info(StringUtils::spf(
                            USS("Started transport servers for {1} in {0} ms"), reg/1000000.0, serverNaming));
                });

                servicesUp = 1;
                stopAction = [this, io_service] () {
                    stopAction = &nop;
                    io_service->stop();
                };
                PerformanceUtils::timeit([this, io_service]() {
                    for (auto i = 0; i < ctx->setup->getRemotingSetup()->poolSize; ++i) {
                        servingThreads.emplace_back([this, io_service](int32_t index) {
                                runningSize[index] = 1;
                                io_service->run();
                            },
                            i);
                    }

                    ThreadUtils::spinWait([this]() {
                        if (stopping) {
                            return int8_t(1);
                        }

                        int32_t runningSum = std::accumulate(runningSize.begin(), runningSize.end(), 0);
                        if (runningSum > previousSum) {
                            previousSum = runningSum;
                        }

                        return static_cast<int8_t>(runningSum >= ctx->setup->getRemotingSetup()->poolSize ? 1 : 0);
                    });
                }, [this, serverNaming](int64_t reg) {
                    //TODO save in counters
                    logger->info(StringUtils::spf(
                            USS("Started thread pools for {1} in {0} ms"), reg/1000000.0, serverNaming));
                });
                threadsUp = 1;

                ThreadUtils::wait(1);
                PerformanceUtils::timeit([this, io_service]() {
                    for (auto i = 0; i < ctx->setup->getRemotingSetup()->poolSize; ++i) {
                        if (servingThreads[i].joinable()) {
                            servingThreads[i].join();
                        }
                    }
                }, [this, serverNaming](int64_t reg) {
                    //TODO save in counters
                    logger->info(StringUtils::spf(
                            USS("Stopped thread pools for {1} in {0} ms"), reg/1000000.0, serverNaming));
                });
                threadsUp = 0;
                running = 0;

                tcpServer->stop();
                udpServer->stop();

                finally f([this]() {
                    servicesUp = 0;
                });

                logger->info(StringUtils::spf(USS("Stopped {0}"), serverNaming));
            }
            catch (std::exception &e) {
                logger->error(StringConvUtils::u8tou(e.what()));
            }
        }

        Client::Client(
                ProtocolType type,
                const icu::UnicodeString& address,
                int32_t port,
                std::shared_ptr<Logger> logger,
                std::shared_ptr<Protocol> protocol,
                const ServerContext& runningContext)
                : Client(
                        type,
                        Socket(StringConvUtils::utou8(address), port, type),
                        std::move(logger),
                        std::move(protocol),
                        runningContext) { }
        Client::Client(
                ProtocolType type,
                const Socket& socket,
                std::shared_ptr<Logger> logger,
                std::shared_ptr<Protocol> protocol,
                const ServerContext& runningContext)
                : type(type), socket(socket), logger(std::move(logger)), protocol(protocol)
        {
            auto io_service = fromServerData(runningContext.data);
            if (type == ProtocolType::TCP) {
                auto tcpSocket = std::make_shared<asio::ip::tcp::socket>(*io_service);
                try {
                    asio::ip::tcp::endpoint e(asio::ip::address::from_string(socket.getAddress()),
                                              (unsigned short) socket.getPort());
                    tcpSocket->connect(e);
                }
                catch(...) {
                    auto sp = StringConvUtils::utou8(ByteCodeUtils::asString((int64_t) socket.getPort()));
                    asio::ip::tcp::resolver resolver(*io_service);
                    asio::ip::tcp::resolver::query query(socket.getAddress(), sp);
                    asio::ip::tcp::resolver::iterator endpoint_iterator = resolver.resolve(query);
                    asio::connect(*tcpSocket, endpoint_iterator);
                }
                asio::ip::tcp::no_delay option(true);
                tcpSocket->set_option(option);
                auto c = std::make_shared<TcpConnection>(logger, protocol, tcpSocket, io_service);
                c->start();
                connection = c;
            }
            if (type == ProtocolType::UDP) {
                auto udpSocket = std::make_shared<asio::ip::udp::socket>(*io_service);
                try {
                    asio::ip::udp::endpoint e(asio::ip::address::from_string(socket.getAddress()),
                                              (unsigned short) socket.getPort());
                    udpSocket->connect(e);
                }
                catch(...) {
                    auto sp = StringConvUtils::utou8(ByteCodeUtils::asString((int64_t) socket.getPort()));
                    asio::ip::udp::resolver resolver(*io_service);
                    asio::ip::udp::resolver::query query(socket.getAddress(), sp);
                    asio::ip::udp::resolver::iterator endpoint_iterator = resolver.resolve(query);
                    asio::connect(*udpSocket, endpoint_iterator);
                }
                connection = std::make_shared<UdpConnection>(logger, protocol, udpSocket, udpSocket->remote_endpoint(), io_service);
            }
            if (connection) {
                protocol->handleConnect(connection, std::error_code());
            }
        }

        Client::~Client()
        {
            connection->stop();
        }

        void Client::handleSend(std::shared_ptr<std::vector<int8_t>> data)
        {
            safe();
            logger->info(
                    StringUtils::spf(
                            USS("Sending for {0}:{1}"),
                            socket.getAddress(),
                            socket.getPort()));
            connection->handleSend(std::move(data));
        }

        std::shared_ptr<Connection> Client::getConnection()
        {
            safe();
            return connection;
        }
    }
}
