/*
 Copyright 2013-2016 Sergey Ionov

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

#include <Protocol.hpp>
#include <StringConv.hpp>
#include <BinaryUtils.hpp>
#include <ByteCodeUtils.hpp>
#include <Server.hpp>
#include <utility>
#include "../utils/Utils.hpp"
#include "../writer/ByteCodeWriter.hpp"
#include "../guid/guid.hpp"

namespace nnvm {
    namespace grid {
        Socket::Socket(std::string address, int32_t port, ProtocolType type)
                : std::tuple<std::string, int32_t, ProtocolType>(address, port, type) {}

        Socket::Socket(const Socket &obj)
                : std::tuple<std::string, int32_t, ProtocolType>(obj.getAddress(), obj.getPort(), obj.getType()) {}

        std::string Socket::getAddress() const {
            safe();
            return std::get<0>(*this);
        }

        int32_t Socket::getPort() const {
            safe();
            return std::get<1>(*this);
        }

        ProtocolType Socket::getType() const {
            safe();
            return std::get<2>(*this);
        }

        Protocol::Protocol(std::shared_ptr<Logger> logger) : logger(std::move(logger))
        {
            std::stringstream ss;
            auto g = nnvm::utils::GuidGenerator::newGuid();
            ss << g;
            guid = StringConvUtils::u8tou(ss.str());
        }

        void Protocol::setRunningContext(const ServerContext& runningContext)
        {
            safe();
            this->runningContext.data = runningContext.data;
        }

        icu::UnicodeString Protocol::id()
        {
            safe();
            return guid;
        }

        ConnectedProtocol::ConnectedProtocol(std::shared_ptr<Logger> logger) : Protocol(std::move(logger)) {}

        ConnectedProtocol::~ConnectedProtocol()
        {
            connections.clear();
        }

        void ConnectedProtocol::send(ProtocolType type,
                                     icu::UnicodeString address,
                                     int32_t port,
                                     std::shared_ptr<std::vector<int8_t> > data)
        {
            safe();
            auto ip = StringConvUtils::utou8(address);
            auto defaultFunc =
                    std::function<std::shared_ptr<Connection>()>([&type, &address, &port, this]() -> std::shared_ptr<Connection> {
                        return PerformanceUtils::timeit(
                        std::function<std::shared_ptr<Connection>()>([this, &type, &address, &port]() -> std::shared_ptr<Connection> {
                            // slowest part. up to 20 ms
                            Client client(type, address, port, logger, shared_from_this(), runningContext);
                            return client.getConnection();
                        }), [](int64_t reg) {
                            //TODO save in counters
                            //std::cout<<StringUtils::spf("Counted connection default inside in {0} ms\n", reg/1000000.0);
                        });
                    });
            auto conn = PerformanceUtils::timeit(
            std::function<std::shared_ptr<Connection>()>([this, &type, &ip, &port, &defaultFunc]() -> std::shared_ptr<Connection> {
                return nnvm::CollectionUtils::getOrDefault(connections, defaultFunc, type, ip, port);
            }), [](int64_t reg) {
                //TODO save in counters
                //std::cout<<StringUtils::spf("Counted connection default outside in {0} ms\n", reg/1000000.0);
            });
            conn->handleSend(std::move(data));
        }

        void ConnectedProtocol::handleConnect(std::shared_ptr<Connection> connection, const std::error_code &error)
        {
            safe();
            if (error) {
                onConnectError(connection->getRemote(), error);
            }
            else {
                nnvm::CollectionUtils::set(connections,
                                           connection,
                                           ProtocolType::TCP,
                                           connection->getRemote().getAddress(),
                                           connection->getRemote().getPort());
                onConnect(connection->getRemote());
            }
        }

        void ConnectedProtocol::handleReceive(std::shared_ptr<Connection> connection,
                                              const std::vector<int8_t> &buffer,
                                              const std::error_code &error)
        {
            safe();
            if (error) {
                onReceiveError(connection->getRemote(), error);
            }
            else {
                onReceive(connection->getRemote(), buffer);
            }
        }

        void ConnectedProtocol::handleSendEnd(const std::error_code &error,
                                              int64_t bytes_transferred,
                                              std::shared_ptr<std::vector<int8_t> > data)
        {
            safe();
            onSendEnd(error, bytes_transferred, data);
        }

        ContinuousLock::ContinuousLock(ContinuousStream& stream) : lk(stream.cvMutex)
        {
            lk.lock();
        }

        ContinuousLock::~ContinuousLock()
        {
            lk.unlock();
        }

        std::shared_ptr<ContinuousLock> ContinuousStream::with_lock()
        {
            return std::make_shared<ContinuousLock>(*this);
        }
    }
}
