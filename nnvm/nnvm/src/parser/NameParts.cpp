/*
Copyright 2013-2016 Sergey Ionov

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
#include <NameParts.hpp>
#include <sstream>
#include <cassert>
#include <unicode/regex.h>
#include <unicode/schriter.h>
#include <unicode/uchar.h>
#include <StateMachine.hpp>
#include <NameParserException.hpp>
#include <Internationalization.hpp>
#include <StringConv.hpp>
#include "../utils/Utils.hpp"

namespace nnvm {
    NameParts::NameParts() : isDirty(false), isGlobal(false), name(USS("")), subname(nullptr) { }

    icu::UnicodeString NameParts::toString()
    {
        std::vector<icu::UnicodeString> indexes(this->indexes.size());
        for (auto i = 0; i < this->indexes.size(); i++) {
            indexes[i] = this->indexes[i]->toString();
        }
        auto g = this->isGlobal ? USS(".") : USS("");
        auto si = indexes.size() > 0 ? StringUtils::spf(
            USS("[{0}]"),
            StringConvUtils::utow(StringUtils::joinArray(indexes, USS(",")))) : USS("");
        auto ss = this->subname ? this->subname->toString() : USS("");
        return StringUtils::spf(USS("{0}{1}{2}{3}"), StringConvUtils::utow(g), StringConvUtils::utow(this->name), StringConvUtils::utow(si), StringConvUtils::utow(ss));
    }

    typedef StringState<std::deque<std::shared_ptr<NameParts>>, NameParts> NameState;

    NameState::stateMatcher isAllowed =
    [](std::vector<UChar32>& data, int64_t size) -> int8_t { return (int8_t) (size == 0 ? 0 : (data[0] == '\0' ? 0 : (u_isalnum(data[0]) || data[0] == USS("§").char32At(0) || data[0] == '.' || data[0] == ',' || data[0] == '[' || data[0] == ']' || u_isWhitespace(data[0]) || data[0] == '_' || data[0] == '#' || data[0] == '-' ? 1 : -1))); };

    NameState::stateMatcher isNamePart =
    [](std::vector<UChar32>& data, int64_t size) -> int8_t { return (int8_t) (size == 0 ? 0 : (data[0] == '\0' ? 0 : (u_isalnum(data[0]) || data[0] == USS("§").char32At(0) || data[0] == '.' || data[0] == ',' || u_isWhitespace(data[0]) || data[0] == '_' || data[0] == '#' || data[0] == '-' ? 1 : -1))); };

    NameState::stateFiller initStackItem =
    [](std::vector<UChar32>& data, int64_t& size, std::shared_ptr<std::deque<std::shared_ptr<NameParts>>>& carried, std::shared_ptr<NameParts>& result) -> int8_t
    {
        if (size == 0) {
            return 0;
        }

        result = std::make_shared<NameParts>();
        carried = std::make_shared<std::deque<std::shared_ptr<NameParts>>>();
        carried->push_back(result);
        result->isGlobal = data[0] == '.';
        result->isDirty = true;
        result->name = USS("");
        return 1;
    };

    auto restartGlobal = [](std::shared_ptr<std::deque<std::shared_ptr<NameParts>>>& carried)
    {
        auto tmp = carried->back();
        tmp->isDirty = true;
        tmp->isGlobal = true;
        tmp->name = USS("");
    };

    auto openSub = [](std::shared_ptr<std::deque<std::shared_ptr<NameParts>>>& carried)
    {
        if (carried->back()->name.length() == 0) {
            throw NameParserException(_i("err.nameparser"), _i("err.nameparser.nesteddots"), 1, 0);
        }
        auto tmp = std::make_shared<NameParts>();
        tmp->isDirty = true;
        tmp->isGlobal = true;
        tmp->name = USS("");
        carried->back()->subname = tmp;
        carried->push_back(tmp);
    };

    auto shiftIndex = [](std::shared_ptr<std::deque<std::shared_ptr<NameParts>>>& carried)
    {
        if (carried->empty()) {
            throw NameParserException(_i("err.nameparser"), _i("err.nameparser.extraclosing"), 1, 0);
        }
        auto cleanUp = carried->back()->isDirty;
        carried->pop_back();
        if (cleanUp) {
            carried->back()->indexes.pop_back();
        }
    };

    auto prepareNext = [](std::shared_ptr<std::deque<std::shared_ptr<NameParts>>>& carried)
    {
        if (carried->empty()) {
            throw NameParserException(_i("err.nameparser"), _i("err.nameparser.extraclosing"), 1, 0);
        }
        auto tmp = std::make_shared<NameParts>();
        tmp->isDirty = true;
        tmp->name = USS("");
        carried->back()->indexes.push_back(tmp);
        carried->push_back(tmp);
    };

    auto appendToName = [](std::vector<UChar32>& data, std::shared_ptr<std::deque<std::shared_ptr<NameParts>>>& carried)
    {
        carried->back()->name += data[0];
        carried->back()->isDirty = false;
    };

    NameState::stateFiller fillByChar =
    [](std::vector<UChar32>& data, int64_t& size, std::shared_ptr<std::deque<std::shared_ptr<NameParts>>>& carried, std::shared_ptr<NameParts>& result) -> int8_t
    {
        if (size == 0) {
            if (carried->back()->isDirty) {
                throw NameParserException(_i("err.nameparser"), _i("err.nameparser.singledot"), 1, 0);
            }
            return 1;
        }

        switch (data[0])
        {
        case '.':
            if (carried->back()->isDirty) {
                restartGlobal(carried);
            }
            else {
                openSub(carried);
            }
            break;
        case ',':
            shiftIndex(carried);
            prepareNext(carried);
            break;
        case '[':
            prepareNext(carried);
            break;
        case ']':
            shiftIndex(carried);
            break;
        case ' ':
        case '\t':
        case '\r':
        case '\n':
            //skip
            break;
        default:
            appendToName(data, carried);
            break;
        }
        return 1;
    };

    NameState::stateStepper stepNext =
    [](std::vector<UChar32>& data, int64_t size, std::shared_ptr<std::deque<std::shared_ptr<NameParts>>> c, int16_t& state) -> int8_t { state++; return 1; };

    NameState::stateStepper stay =
    [](std::vector<UChar32>& data, int64_t size, std::shared_ptr<std::deque<std::shared_ptr<NameParts>>> c, int16_t& state) -> int8_t { if (!size) { state++; } return 1; };

    NameState::stateError nameError =
    [](std::vector<UChar32>& data, int16_t state, int64_t symbol) -> icu::UnicodeString { return _i("err.name.invalid.character"); };

    auto peekChar = StringMachine<std::deque<std::shared_ptr<NameParts>>, NameParts>::Readers::peekChar().next;
    auto readChar = StringMachine<std::deque<std::shared_ptr<NameParts>>, NameParts>::Readers::readChar().next;

    std::vector<NameState> nameStates = {
        { peekChar, isNamePart, initStackItem, stepNext, nameError }, //0 .|\w
        { readChar, isAllowed, fillByChar, stay, nameError, 1 }, //1 \w|[|]|.
    };

    std::shared_ptr<NameParts> NameParts::parseName(icu::UnicodeString name)
    {
        ErrorHandlers<DataSource, UChar32, std::deque<std::shared_ptr<NameParts>>, NameParts> handlers;
        handlers.noParse = [](int16_t stateIndex, std::shared_ptr<std::vector<int16_t> > states) {
            throw NameParserException(_i("err.parser.parserloop"), _i("err.parser.illegalstate"), stateIndex, 0);
        };
        handlers.noRead = [](std::function<icu::UnicodeString(std::vector<UChar32>&, int16_t, int64_t)>& errorDetails, std::vector<UChar32>& buffer, int16_t stateIndex, std::shared_ptr<std::vector<int16_t> > states) {
            throw NameParserException(_i("err.parser.reader"), errorDetails(buffer, stateIndex, 0), stateIndex, 0);
        };
        handlers.noMatch = [](std::function<icu::UnicodeString(std::vector<UChar32>&, int16_t, int64_t)>& errorDetails, std::vector<UChar32>& buffer, int16_t stateIndex, std::shared_ptr<std::vector<int16_t> > states) {
            throw NameParserException(_i("err.parser.matcher"), errorDetails(buffer, stateIndex, 0), stateIndex, 0);
        };
        handlers.noFill = [](std::function<icu::UnicodeString(std::vector<UChar32>&, int16_t, int64_t)>& errorDetails, std::vector<UChar32>& buffer, int16_t stateIndex, std::shared_ptr<std::vector<int16_t> > states) {
            throw NameParserException(_i("err.parser.filler"), errorDetails(buffer, stateIndex, 0), stateIndex, 0);
        };
        handlers.noStep = [](std::function<icu::UnicodeString(std::vector<UChar32>&, int16_t, int64_t)>& errorDetails, std::vector<UChar32>& buffer, int16_t stateIndex, std::shared_ptr<std::vector<int16_t> > states) {
            throw NameParserException(_i("err.parser.stepper"), errorDetails(buffer, stateIndex, 0), stateIndex, 0);
        };

        StringMachine<std::deque<std::shared_ptr<NameParts>>, NameParts> a(nameStates, handlers);
        auto tw = StringConvUtils::utow(name);
        return a.run(name);
    }
}
