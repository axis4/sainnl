/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#include <ParsedNeuron.hpp>
#include <ByteCodeBlock.hpp>
#include <DefaultNeurons.hpp>
#include <ProcessorKnownParameters.hpp>
#include <ByteCodeUtils.hpp>
#include <cmath>
#include "Language.hpp"
#include "../utils/Utils.hpp"

namespace nnvm {
    ParsedNeuron::ParsedNeuron(std::shared_ptr<MachineSetup> setup, std::shared_ptr<LibraryLoader> loader)
            : setup(setup), loader(loader) { }

    ParsedNeuronArgumentType ParsedNeuron::argumentType(int64_t argumentIndex, int64_t argumentTotal)
    {
        safe();
        if (argumentIndex >= argumentTotal) {
            return ParsedNeuronArgumentType::FAIL;
        }
        return ParsedNeuronArgumentType::RAW;
    }

    std::vector<std::shared_ptr<ByteCodeBlockItem>> ParsedNeuron::compileExpression(Operand& operand, icu::UnicodeString name, icu::UnicodeString linkType, std::shared_ptr<ByteCodeBlock> current, std::vector<std::shared_ptr<ByteCodeBlock>>& rv, std::shared_ptr<Context> ctx)
    {
        safe();
        std::vector<std::shared_ptr<ByteCodeBlockItem>> res;
        // expression as links
        if (operand.call) {
            auto op = operand.call->blockName;
            auto an = ctx->setup->getByteCodeParserSetup()->aliases.find(op);
            if (an != ctx->setup->getByteCodeParserSetup()->aliases.end()) {
                op = an->second;
            }
            auto opw = StringConvUtils::utow(op);
            if (op == NeuronNames::MULTIPLY) {
                if (operand.call->arguments.size() > 2) {
                    return res;
                }
                if (operand.call->arguments.size() == 1) {
                    return compileExpression(operand.call->arguments[0].operand, name, linkType, current, rv, ctx);
                }
                auto left = operand.call->arguments[0].operand;
                auto right = operand.call->arguments[1].operand;
                if (left.literal && right.literal) {
                    std::shared_ptr<Token<TokenKind>> number = nullptr;
                    std::shared_ptr<Token<TokenKind>> source = nullptr;
                    auto expected = 1;
                    auto bias = 0.0;
                    auto biass = USS("");
                    
                    // number
                    if (left.literal->isOf(TokenKind::NUMBER) || left.literal->isOf(TokenKind::AS_NUMBER)) {
                        number = left.literal;
                    }
                    // source
                    else {
                        source = left.literal;
                    }
                    
                    if (right.literal->isOf(TokenKind::NUMBER) || right.literal->isOf(TokenKind::AS_NUMBER)) {
                        // number * number
                        if (number) {
                            if (right.literal->isOf(TokenKind::NUMBER)
                                && fabs(ByteCodeUtils::asNumber(right.literal->value)) < 1e-8) {
                                return res;
                            }
                            source = std::make_shared<Token<TokenKind>>(ctx->byteLoader);
                            // number * template or template * template
                            if (right.literal->isOf(TokenKind::AS_NUMBER)) {
                                biass = right.literal->value;
                                source->value = nnvm::Language::Language::cleanName(StringUtils::spf(USS("{0}.const[{1}]"), StringConvUtils::utow(name), StringConvUtils::utow(biass)));
                            }
                            // number * number or template * number
                            else {
                                bias = ByteCodeUtils::asNumber(right.literal->value);
                                source->value = nnvm::Language::Language::cleanName(StringUtils::spf(USS("{0}.const{1}"), StringConvUtils::utow(name), bias));
                            }
                            expected = 0;
                        }
                        // source * number or source * template
                        else {
                            number = right.literal;
                        }
                    }
                    else {
                        // number * source
                        if (number) {
                            source = right.literal;
                        }
                        // source * source
                        else {
                            // nothing - wrong state - should have been prepared for multiplicative plugin
                        }
                    }
                    
                    if (number && source) {
                        std::shared_ptr<ByteCodeBlock> f;
                        if (source->value == name) {
                            f = current;
                        }
                        else {
                            for (auto i = rv.begin(); i != rv.end(); ++i) {
                                if ((*i)->getName() == source->value) {
                                    f = *i;
                                }
                            }
                            if (!f) {
                                f = std::make_shared<ByteCodeBlock>(source->value, USS(""));
                                if (expected) {
                                    f->setParameter(std::make_shared<ByteCodeParameter>(ElementParams::EXPECTED, (int8_t)1));
                                }
                                else {
                                    if (biass == USS("")) {
                                        f->setParameter(std::make_shared<ByteCodeParameter>(ElementParams::BIAS, bias));
                                    }
                                    else {
                                        f->setParameter(std::make_shared<ByteCodeParameter>(ElementParams::BIAS, biass));
                                    }
                                }
                                rv.push_back(f);
                            }
                        }
                        auto edge = std::make_shared<ByteCodeBlockItem>(name, f);
                        if (number->isOf(TokenKind::AS_NUMBER)) {
                            edge->setParameter(std::make_shared<ByteCodeParameter>(linkType, number->value));
                        }
                        else {
                            edge->setParameter(std::make_shared<ByteCodeParameter>(linkType, ByteCodeUtils::asNumber(number->value)));
                        }
                        res.push_back(edge);
                    }
                }
            }
            if (op == NeuronNames::PLUS) {
                for (auto i = operand.call->arguments.begin(); i != operand.call->arguments.end(); ++i) {
                    auto r = compileExpression(i->operand, name, linkType, current, rv, ctx);
                    res.insert(res.end(), r.begin(), r.end());
                }
            }
        }
        // literal as constant
        if (operand.literal) {
            // the number constant
            if (operand.literal->isOf(TokenKind::NUMBER) || operand.literal->isOf(TokenKind::AS_NUMBER)) {
                if (operand.literal->isOf(TokenKind::NUMBER) && fabs(ByteCodeUtils::asNumber(operand.literal->value)) < 1e-8) {
                    return res;
                }
                auto bias = 0.0;
                auto biass = USS("");
                auto constName = USS("");
                if (operand.literal->isOf(TokenKind::AS_NUMBER)) {
                    biass = operand.literal->value;
                    constName = nnvm::Language::Language::cleanName(StringUtils::spf(USS("{0}[{1}].const{1}"), StringConvUtils::utow(name), StringConvUtils::utow(biass)));
                }
                else {
                    bias = ByteCodeUtils::asNumber(operand.literal->value);
                    constName = nnvm::Language::Language::cleanName(StringUtils::spf(USS("{0}.const{1}"), StringConvUtils::utow(name), bias));
                }
                std::shared_ptr<ByteCodeBlock> f;
                for (auto i = rv.begin(); i != rv.end(); ++i) {
                    if ((*i)->getName() == constName) {
                        f = *i;
                    }
                }
                if (!f) {
                    f = std::make_shared<ByteCodeBlock>(constName, USS(""));
                    if (biass != USS("")) {
                        f->setParameter(std::make_shared<ByteCodeParameter>(ElementParams::BIAS, biass));
                        f->setParameter(std::make_shared<ByteCodeParameter>(ElementParams::TEMPLATE, (int8_t)1));
                    }
                    else {
                        f->setParameter(std::make_shared<ByteCodeParameter>(ElementParams::BIAS, bias));
                    }
                    rv.push_back(f);
                }
                auto edge = std::make_shared<ByteCodeBlockItem>(name, f);
                edge->setParameter(std::make_shared<ByteCodeParameter>(linkType, (int8_t)1));
                res.push_back(edge);
            }
            else {
                std::shared_ptr<ByteCodeBlock> f;
                if (operand.literal->value == name) {
                    f = current;
                }
                else {
                    for (auto i = rv.begin(); i != rv.end(); ++i) {
                        if ((*i)->getName() == operand.literal->value) {
                            f = *i;
                        }
                    }
                    if (!f) {
                        f = std::make_shared<ByteCodeBlock>(operand.literal->value, USS(""));
                        f->setParameter(std::make_shared<ByteCodeParameter>(ElementParams::EXPECTED, (int8_t)1));
                        rv.push_back(f);
                    }
                }
                auto edge = std::make_shared<ByteCodeBlockItem>(name, f);
                edge->setParameter(std::make_shared<ByteCodeParameter>(linkType, (int8_t)1));
                res.push_back(edge);
            }
        }
        return res;
    }
}
