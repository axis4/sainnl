/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#pragma once

namespace nnvm
{
    namespace Language {
        class Language;
        class Permutator;
        class FilteredPermutator;
        class ProjectedFilteredPermutator;
    }
}

#include <predef.h>
#include <unicode/unistr.h>
#include <LanguageModel.hpp>

namespace nnvm {
    icu::UnicodeString tokenTypeString(TokenKind& type);
}

#include <vector>
#include <list>
#include <memory>
#include <istream>
#include <ByteCode.hpp>
#include <MachineSetup.hpp>
#include <LR.hpp>
#include <LibraryLoader.hpp>
#include <ByteCodeLoader.hpp>
#include "ByteCodeParserModel.hpp"
#include "../collection/Generator.hpp"

namespace nnvm
{
    namespace Language {
        class Permutator : public Generator<std::map<icu::UnicodeString, int64_t>, std::map<icu::UnicodeString, int64_t>>
        {
        public:
            Permutator(std::shared_ptr<std::map<icu::UnicodeString, int64_t>> rangeStates, std::list<icu::UnicodeString> orderedRange, std::map<icu::UnicodeString, std::tuple<int64_t, int64_t>> ranges);
        protected:
            std::map<icu::UnicodeString, std::tuple<int64_t, int64_t>> ranges;
            std::list<icu::UnicodeString> orderedRange;

            virtual int8_t yield(std::shared_ptr<std::map<icu::UnicodeString, int64_t>> ctx, std::map<icu::UnicodeString, int64_t>& res);
            virtual icu::UnicodeString asString(const std::map<icu::UnicodeString, int64_t>& s);
        };
        
        class FilteredPermutator : public Permutator
        {
        public:
            FilteredPermutator(std::shared_ptr<std::map<icu::UnicodeString, int64_t>> rangeStates, std::list<icu::UnicodeString> orderedRange, std::map<icu::UnicodeString, std::tuple<int64_t, int64_t>> ranges, std::function<int8_t(std::map<icu::UnicodeString, int64_t>&)> filter);
        protected:
            std::function<int8_t(std::map<icu::UnicodeString, int64_t>&)> filter;

            virtual int8_t yield(std::shared_ptr<std::map<icu::UnicodeString, int64_t>> ctx, std::map<icu::UnicodeString, int64_t>& res) override;
        };
        
        class ProjectedFilteredPermutator : public FilteredPermutator
        {
        public:
            ProjectedFilteredPermutator(std::shared_ptr<std::map<icu::UnicodeString, int64_t>> rangeStates, std::list<icu::UnicodeString> orderedRange, std::map<icu::UnicodeString, std::tuple<int64_t, int64_t>> ranges, std::function<int8_t(std::map<icu::UnicodeString, int64_t>&)> filter, std::set<icu::UnicodeString> indexes);
        protected:
            std::set<icu::UnicodeString> indexes;
            std::set<icu::UnicodeString> passed;

            virtual int8_t yield(std::shared_ptr<std::map<icu::UnicodeString, int64_t>> ctx, std::map<icu::UnicodeString, int64_t>& res) override;
            virtual icu::UnicodeString asString(const std::map<icu::UnicodeString, int64_t>& s) override;
        };
        
        class Language {
        public:
            static std::vector<ByteState> states;
            static int8_t compileName(LanguageBlock& block, icu::UnicodeString prefix, icu::UnicodeString& res);
            static int8_t compileRanges(LanguageBlock& block, icu::UnicodeString prefix, std::map<icu::UnicodeString, double>& context);
            static int8_t validateCode(std::shared_ptr<ByteCode> code);
            static int8_t compileBlock(LanguageBlock& block, icu::UnicodeString prefix, std::map<icu::UnicodeString, double>& context, std::shared_ptr<ByteCode>& res, std::map<icu::UnicodeString, std::shared_ptr<ParsedNeuron>>& plugins, std::shared_ptr<Context> ctx);
            static int8_t compileTree(std::shared_ptr<LanguageTree> tree, std::shared_ptr<std::vector<std::shared_ptr<ByteCode>>>& result, icu::UnicodeString prefix, std::map<icu::UnicodeString, std::shared_ptr<ParsedNeuron>>& plugins, std::shared_ptr<Context> ctx);
            static int8_t applyToTree(Operand& operand, std::function<void(Token<TokenKind>&)> action);
            static int8_t applyToTree(LanguageArgument& arg, std::function<void(Token<TokenKind>&)> action);
            static int8_t applyToTree(LanguageCall& call, std::function<void(Token<TokenKind>&)> action);
            static int8_t cleanupByteCode(std::shared_ptr<std::vector<std::shared_ptr<ByteCode> > > result);
            static icu::UnicodeString cleanName(icu::UnicodeString s);

            static void markAsNumberConstant(Operand operand, std::deque<icu::UnicodeString> templateNames, std::map<icu::UnicodeString, std::tuple<int64_t, int64_t>> ranges, std::map<icu::UnicodeString, double> &context, std::shared_ptr<Context> ctx);
            static void markAsNumberConstant(std::vector<Token<TokenKind>>& operand, std::deque<icu::UnicodeString> templateNames, std::map<icu::UnicodeString, std::tuple<int64_t, int64_t>> ranges, std::map<icu::UnicodeString, double> &context, std::shared_ptr<Context> ctx);
            static void markAsNumberConstant(Token<TokenKind>& operand, std::deque<icu::UnicodeString> templateNames, std::map<icu::UnicodeString, std::tuple<int64_t, int64_t>> ranges, std::map<icu::UnicodeString, double> &context, std::shared_ptr<Context> ctx);
            static int8_t collapseExpression(Operand& operand);
            
            static int8_t isBlockName(const unsigned char c);
            static int8_t isItemName(const unsigned char c);
            static int8_t isDefinitionLine(const unsigned char c);
            static int8_t isExpressionLine(const unsigned char c);
            static int8_t isNumber(const unsigned char c, std::vector<uint8_t>& read);
            static int8_t isUnicodeItemName(icu::UnicodeString& u);
            static int8_t isUnicodeNumber(icu::UnicodeString& u);

            static std::set<icu::UnicodeString> findNameIndexes(std::map<icu::UnicodeString, std::tuple<int64_t, int64_t> >& ranges, icu::UnicodeString s);
            static std::set<icu::UnicodeString> findNameIndexes(std::map<icu::UnicodeString, std::tuple<int64_t, int64_t> >& ranges, LanguageDefinition& d);
            static std::set<icu::UnicodeString> findTemplateIndexes(std::map<icu::UnicodeString, std::tuple<int64_t, int64_t> >& ranges, Operand& o);
            static std::set<icu::UnicodeString> findTemplateIndexes(std::map<icu::UnicodeString, std::tuple<int64_t, int64_t> >& ranges, LanguageArgument& a);
            static std::set<icu::UnicodeString> findTemplateIndexes(std::map<icu::UnicodeString, std::tuple<int64_t, int64_t> >& ranges, LanguageCall& c);
            static std::set<icu::UnicodeString> findTemplateIndexes(std::map<icu::UnicodeString, std::tuple<int64_t, int64_t> >& ranges, LanguageDefinition& d);
        private:
            static std::shared_ptr<std::map<icu::UnicodeString, int64_t>> buildRanges(std::map<icu::UnicodeString, std::tuple<int64_t, int64_t> >& ranges);
            static std::shared_ptr<Permutator> buildGenerator(std::map<icu::UnicodeString, std::tuple<int64_t, int64_t> >& ranges, std::list<icu::UnicodeString>& orderedRange, std::shared_ptr<std::map<icu::UnicodeString, int64_t>> rangeStates, const std::set<icu::UnicodeString>& indexes, const std::list<Operand>& conditions, const std::map<icu::UnicodeString, int64_t>& fixedState);
            static void setTokenIndexes(Token<TokenKind>& t, const std::map<icu::UnicodeString, int64_t>& s, const std::set<icu::UnicodeString>& allowedIndexes);
            static void setTemplateIndexes(Operand& operand, const std::map<icu::UnicodeString, int64_t>& s, const std::set<icu::UnicodeString>& allowedIndexes);
            static void setTemplateIndexes(LanguageArgument& arg, const std::map<icu::UnicodeString, int64_t>& s, const std::set<icu::UnicodeString>& allowedIndexes);
            static void setTemplateIndexes(LanguageCall& nc, const std::map<icu::UnicodeString, int64_t>& s, const std::set<icu::UnicodeString>& allowedIndexes);
            static void setTemplateIndexes(LanguageDefinition& nd, const std::map<icu::UnicodeString, int64_t>& s, const std::set<icu::UnicodeString>& allowedIndexes);
            
            static void fixSubExpression(LanguageCall& c);

            static void expandOperand(Operand& o, std::map<icu::UnicodeString, std::tuple<int64_t, int64_t> >& ranges, std::list<icu::UnicodeString>& orderedRange, std::shared_ptr<Context> ctx, const std::list<Operand>& conditions, const std::map<icu::UnicodeString, int64_t>& fixedState);
            static void expandArguments(Operand& o, std::map<icu::UnicodeString, std::tuple<int64_t, int64_t> >& ranges, std::list<icu::UnicodeString>& orderedRange, std::shared_ptr<Context> ctx, const std::list<Operand>& conditions, const std::map<icu::UnicodeString, int64_t>& fixedState);
        };
    }
}
