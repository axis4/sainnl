//
//  BOM.cpp
//  nnvm
//
//  Created by Serge Ionov on 17/01/15.
//  Copyright (c) 2015 Serge Ionov. All rights reserved.
//

#include "BOM.hpp"
#include <inttypes.h>
#include <unicode/unistr.h>
#include <Internationalization.hpp>
#include <ByteCodeParser.hpp>

namespace nnvm
{
    namespace BOM {
        auto readByte = ByteCodeState::readByte().next;
        auto alwaysTrue = ByteState::Matches::alwaysTrue().next;
        auto noop = ByteState::Fills().noop;
        template <int64_t default_step>
        using steps0 = ByteState::Steps::steps0<default_step>;
        using stepsInc = ByteState::Steps::stepsInc;
        template <uint8_t condition, int64_t step, uint8_t condition2, int64_t step2, int64_t default_step, int8_t require_conditions>
        using steps2 = ByteState::Steps::steps2<condition, step, condition2, step2, default_step, require_conditions>;

        ByteState::stateError unicodeError =
        [](std::vector<uint8_t>& data, int16_t state, int64_t symbol) -> icu::UnicodeString { return _i("err.parser.invalid.bom"); };

        std::vector<ByteState> BOM::states = {
            { readByte, alwaysTrue, noop, steps2<0, 1, (uint8_t)0xEF, 4, 6, 0>().next, unicodeError }, //0
            { readByte, alwaysTrue, noop, stepsInc().next, unicodeError }, //1
            { readByte, alwaysTrue, noop, stepsInc().next, unicodeError }, //2
            { readByte, alwaysTrue, noop, steps0<-1>().next, unicodeError, 1 }, //3
            { readByte, alwaysTrue, noop, stepsInc().next, unicodeError }, //4
            { readByte, alwaysTrue, noop, steps0<-1>().next, unicodeError, 1 }, //5
            { readByte, alwaysTrue, noop, steps0<-1>().next, unicodeError, 1 }, //6
        };
    }
}
