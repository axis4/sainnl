/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#pragma once

namespace nnvm {
    class Carried;
    struct ByteCodeState;
}

#include <predef.h>
#include <inttypes.h>
#include <istream>
#include <map>
#include <set>
#include <StateMachine.hpp>
#include <ByteCode.hpp>

namespace nnvm {
    typedef State<std::istream, uint8_t, Carried, std::vector<std::shared_ptr<ByteCode>>> ByteState;
}

#include <MachineSetup.hpp>
#include <LibraryLoader.hpp>
#include <ByteCodeLoader.hpp>
#include "JSON.hpp"
#include "Language.hpp"

namespace nnvm {
    class Carried
    {
    public:
        Carried();
        Carried(const Carried& obj);
        icu::UnicodeString string;
        std::vector<std::shared_ptr<nnvm::JSON::Object>> objects;
        std::shared_ptr<LanguageTree> tree;
        icu::UnicodeString prefix;
        icu::UnicodeString package;
        std::shared_ptr<Context> ctx;
        int64_t counter;
    };

    struct ByteCodeState
    {
        struct readByte {
            nnvm::ByteState::stateReader next =
                    [](std::shared_ptr<std::istream> dataSource, std::vector<uint8_t>& result) -> int64_t {
                        result.clear();
                        if (dataSource->good()) {
                            auto t = dataSource->get();
                            if (dataSource->good()) {
                                result.push_back((uint8_t)t);
                            }
                        }
                        if (dataSource->fail()) {
                            return -1;
                        }
                        return result.size();
                    };
        };
        struct peekByte {
            ByteState::stateReader next =
                    [](std::shared_ptr<std::istream> dataSource, std::vector<uint8_t>& result) -> int64_t
                    {
                        result.clear();
                        auto c = dataSource->peek();
                        if (dataSource->good()) {
                            result.push_back((uint8_t)c);
                            return 1;
                        }
                        return -1;
                    };
        };
        struct peekSafeByte {
            ByteState::stateReader next =
                    [](std::shared_ptr<std::istream> dataSource, std::vector<uint8_t>& result) -> int64_t
                    {
                        result.clear();
                        auto c = dataSource->peek();
                        if (dataSource->good()) {
                            result.push_back((uint8_t)c);
                            return 1;
                        }
                        return 0;
                    };
        };
        struct passWhiteSpaces {
            ByteState::stateReader next =
                    [](std::shared_ptr<std::istream> dataSource, std::vector<uint8_t>& result) -> int64_t
                    {
                        result.clear();
                        while (dataSource->good()) {
                            auto c = dataSource->peek();
                            if (dataSource->good()) {
                                if (ByteCodeParser::isWhiteSpace((uint8_t)c)) {
                                    c = dataSource->get();
                                }
                                else {
                                    result.push_back((uint8_t)c);
                                    break;
                                }
                            }
                        }
                        if (dataSource->eof() || !dataSource->fail()) {
                            return result.size();
                        }
                        return -1;
                    };
        };
        struct isWSpace {
            ByteState::stateMatcher next =
                    [](std::vector<uint8_t>& data, int64_t size) -> int8_t {
                        return (int8_t) (size == 1 && ByteCodeParser::isWhiteSpace(data[0]) ? 1 : -1);
                    };
        };
    };
}
