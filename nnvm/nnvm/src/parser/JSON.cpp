/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#include "JSON.hpp"
#include <sstream>
#include <ProcessorKnownParameters.hpp>
#include <Internationalization.hpp>
#include <StringConv.hpp>
#include <ByteCodeUtils.hpp>
#include <ByteCodeParser.hpp>
#include "../utils/Utils.hpp"

namespace nnvm
{
    namespace JSON {
        icu::UnicodeString typeString = USS("type");
        icu::UnicodeString valueString = USS("value");
        icu::UnicodeString packageNameString = USS("packageName");
        icu::UnicodeString blocksString = USS("blocks");
        icu::UnicodeString blockNameString = USS("blockName");
        icu::UnicodeString itemsString = USS("items");
        icu::UnicodeString itemNameString = USS("itemName");
        icu::UnicodeString parametersString = USS("parameters");

        std::map<icu::UnicodeString, icu::UnicodeString> jsonToInternal{
            { USS("activation"), LinkParams::ACTIVATION },
            { USS("stream"), LinkParams::STREAM },
            { USS("generation"), LinkParams::GENERATION },
            { USS("input"), ElementParams::INPUT },
            { USS("output"), ElementParams::OUTPUT },
            { USS("template"), ElementParams::TEMPLATE },
            { USS("internal_state"), ElementParams::INTERNAL_STATE },
            { USS("bias"), ElementParams::BIAS },
            { USS("initial_state"), ElementParams::INITIAL_STATE },
            { USS("threshold"), ElementParams::THRESHOLD },
            { USS("sum"), ElementParams::SUM },
            { USS("index"), ElementParams::INDEX },
        };

        int8_t getTranslation(const icu::UnicodeString& key, icu::UnicodeString& value)
        {
            auto i = jsonToInternal.find(key);
            if (i != jsonToInternal.end()) {
                value = i->second;
                return true;
            }
            return false;
        }

        int8_t buildParameter(const icu::UnicodeString& key, Value val, std::shared_ptr<ByteCodeParameter>& result)
        {
            if (val.object == nullptr) {
                return false;
            }
            auto pt = val.object->find(typeString);
            if (pt == val.object->end()) {
                return false;
            }
            auto pv = val.object->find(valueString);
            if (pv == val.object->end()) {
                return false;
            }
            auto type = pt->second;
            auto value = pv->second;
            if (type.isnull || type.array != nullptr || type.object != nullptr) {
                return false;
            }
            icu::UnicodeString newKey;
            getTranslation(key, newKey);
            switch ((ByteCodeParameterTypes)(int)type.number)
            {
                case ByteCodeParameterTypes::BYTE:
                    result = std::make_shared<ByteCodeParameter>(key, (int8_t)value.number);
                    break;
                case ByteCodeParameterTypes::INTEGER:
                    result = std::make_shared<ByteCodeParameter>(key, (int64_t)value.number);
                    break;
                case ByteCodeParameterTypes::DOUBLE:
                    result = std::make_shared<ByteCodeParameter>(key, value.number);
                    break;
                case ByteCodeParameterTypes::STRING:
                    result = std::make_shared<ByteCodeParameter>(key, value.string);
                    break;
                default:
                    return false;
            }
            return true;
        }

        int8_t buildBlockItem(Value val, std::shared_ptr<ByteCodeBlockItem>& result, std::shared_ptr<ByteCodeBlock> block)
        {
            auto in = val.object->find(itemNameString);
            if (in == val.object->end()) {
                return false;
            }
            result = std::make_shared<ByteCodeBlockItem>(in->second.string, block);
            auto ps = val.object->find(parametersString);
            if (ps != val.object->end()) {
                auto parameters = ps->second.object;
                for (auto i = parameters->begin(); i != parameters->end(); i++) {
                    std::shared_ptr<ByteCodeParameter> bp;
                    if (!buildParameter(i->first, i->second, bp)) {
                        return false;
                    }
                    result->setParameter(bp);
                }
            }
            return true;
        }

        int8_t buildBlock(Value val, std::shared_ptr<ByteCodeBlock>& result)
        {
            auto bn = val.object->find(blockNameString);
            if (bn == val.object->end()) {
                return false;
            }
            auto bt = val.object->find(typeString);
            if (bt == val.object->end()) {
                return false;
            }
            result = std::make_shared<ByteCodeBlock>(bn->second.string, bt->second.string);
            auto ps = val.object->find(parametersString);
            if (ps != val.object->end()) {
                auto parameters = ps->second.object;
                for (auto i = parameters->begin(); i != parameters->end(); i++) {
                    std::shared_ptr<ByteCodeParameter> bp;
                    if (!buildParameter(i->first, i->second, bp)) {
                        return false;
                    }
                    result->setParameter(bp);
                }
            }
            auto is = val.object->find(itemsString);
            if (is != val.object->end()) {
                auto items = is->second.array;
                for (auto i = items->begin(); i != items->end(); i++) {
                    auto v = *i;
                    if (v.object == nullptr) {
                        return false;
                    }
                    std::shared_ptr<ByteCodeBlockItem> bi;
                    if (!buildBlockItem(v, bi, result)) {
                        return false;
                    }
                    result->addItem(bi);
                }
            }
            return true;
        }

        int8_t JSONToByteCode::buildResult(Value val, std::shared_ptr<std::vector<std::shared_ptr<ByteCode>>>& result, std::shared_ptr<ByteCodeLoader> loader)
        {
            if (val.object == nullptr) {
                return false;
            }
            auto pn = val.object->find(packageNameString);
            if (pn == val.object->end()) {
                return false;
            }
            result = std::make_shared<std::vector<std::shared_ptr<ByteCode>>>();
            result->push_back(std::make_shared<ByteCode>(pn->second.string, loader));
            auto bs = val.object->find(blocksString);
            if (bs != val.object->end()) {
                auto blocks = bs->second.array;
                for (auto i = blocks->begin(); i != blocks->end(); i++) {
                    auto v = *i;
                    if (v.object == nullptr) {
                        return false;
                    }
                    std::shared_ptr<ByteCodeBlock> b;
                    if (!buildBlock(v, b)) {
                        return false;
                    }
                    result->back()->addBlock(b);
                }
            }
            return true;
        }

        auto passWhiteSpaces = ByteCodeState::passWhiteSpaces().next;
        auto readByte = ByteCodeState::readByte().next;
        auto peekByte = ByteCodeState::peekByte().next;
        auto peekSafeByte = ByteCodeState::peekSafeByte().next;
        auto alwaysTrue = ByteState::Matches::alwaysTrue().next;
        template <uint8_t c>
        using isAtom1 = ByteState::Matches::isAtom1<c>;
        template <uint8_t c, uint8_t c2>
        using isAtom2 = ByteState::Matches::isAtom2<c, c2>;
        template <uint8_t c, uint8_t c2, uint8_t c3>
        using isAtom3 = ByteState::Matches::isAtom3<c, c2, c3>;
        auto noop = ByteState::Fills().noop;
        template <int64_t default_step>
        using steps0 = ByteState::Steps::steps0<default_step>;
        using stepsInc = ByteState::Steps::stepsInc;
        template <uint8_t condition, int64_t step, int64_t default_step>
        using steps1 = ByteState::Steps::steps1<condition, step, default_step>;
        template <uint8_t condition, int64_t step, uint8_t condition2, int64_t step2, int64_t default_step, int8_t require_conditions>
        using steps2 = ByteState::Steps::steps2<condition, step, condition2, step2, default_step, require_conditions>;
        template <uint8_t condition, int64_t step, uint8_t condition2, int64_t step2, uint8_t condition3, int64_t step3, int64_t default_step, int8_t require_conditions>
        using steps3 = ByteState::Steps::steps3<condition, step, condition2, step2, condition3, step3, default_step, require_conditions>;
        template <uint8_t condition, int64_t step, uint8_t condition2, int64_t step2, uint8_t condition3, int64_t step3, uint8_t condition4, int64_t step4, int64_t default_step, int8_t require_conditions>
        using steps4 = ByteState::Steps::steps4<condition, step, condition2, step2, condition3, step3, condition4, step4, default_step, require_conditions>;
        template <uint8_t condition, int64_t step, uint8_t condition2, int64_t step2, uint8_t condition3, int64_t step3, uint8_t condition4, int64_t step4, uint8_t condition5, int64_t step5, int64_t default_step, int8_t require_conditions>
        using steps5 = ByteState::Steps::steps5<condition, step, condition2, step2, condition3, step3, condition4, step4, condition5, step5, default_step, require_conditions>;

        ByteState::stateReader readJsonNull = [](std::shared_ptr<std::istream> dataSource, std::vector<uint8_t>& result) -> int64_t
        {
            int8_t states[256][5] = { -1 };
            states['n'][0] = 1;
            states['u'][1] = 2;
            states['l'][2] = 3;
            states['l'][3] = 4;
            int8_t s = 0;
            while (dataSource->good()) {
                char c = (char) dataSource->get();       // get character from file
                if (dataSource->good()) {
                    s = states[c][s];
                    if (s < 0) {
                        return -1;
                    }
                    if (s == 4) {
                        break;
                    }
                }
            }
            if ((dataSource->eof() || !dataSource->fail()) && s == 4) {
                return 0;
            }
            return -1;
        };
        ByteState::stateReader readJsonBool = [](std::shared_ptr<std::istream> dataSource, std::vector<uint8_t>& result) -> int64_t
        {
            result.clear();
            int8_t states[256][10] = { -1 };
            states['t'][0] = 1;
            states['r'][1] = 2;
            states['u'][2] = 3;
            states['e'][3] = 4;
            states['f'][0] = 5;
            states['a'][5] = 6;
            states['l'][6] = 7;
            states['s'][7] = 8;
            states['e'][8] = 9;
            int8_t s = 0;
            while (dataSource->good()) {
                char c = (char) dataSource->get();       // get character from file
                if (dataSource->good()) {
                    s = states[c][s];
                    if (s < 0) {
                        return -1;
                    }
                    if (s == 4 || s == 9) {
                        break;
                    }
                }
            }
            if ((dataSource->eof() || !dataSource->fail()) && (s == 4 || s == 9)) {
                if (s == 4) {
                    char c[] = "true";
                    result.assign(c, c + sizeof(c));
                }
                if (s == 9) {
                    char c[] = "false";
                    result.assign(c, c + sizeof(c));
                }
                return 0;
            }
            return -1;
        };
        ByteState::stateReader readJsonNumber = [](std::shared_ptr<std::istream> dataSource, std::vector<uint8_t>& result) -> int64_t
        {
            result.clear();
            double res;
            (*dataSource) >> res;
            if (dataSource->fail()) {
                return -1;
            }
            std::ostringstream ss;
            ss.precision(std::numeric_limits<double>::digits10);
            ss << res;
            auto s = ss.str();
            result.assign(s.begin(), s.end());
            return (int64_t) s.size();
        };
        ByteState::stateReader readJsonString = [](std::shared_ptr<std::istream> dataSource, std::vector<uint8_t>& result) -> int64_t
        {
            result.clear();
            std::string tmp("");
            int8_t started = 0;
            int8_t inEscape = false;
            std::vector<int8_t> u;
            int64_t surrogate = -1;
            int8_t need_u = 0;
            while (dataSource->good()) {
                if (need_u == 1) {
                    need_u = 2;
                }
                char c[2] = { 0, 0 };
                c[0] = (char) dataSource->get();       // get character from file
                if (dataSource->good()) {
                    if (!started) {
                        if (c[0] == '"') {
                            started = 1;
                        }
                        else {
                            return -1;
                        }
                    }
                    else {
                        if (c[0] == '"' && !inEscape) {
                            if (need_u || u.size() > 0) {
                                return -1;
                            }
                            started = 2;
                            break;
                        }
                        else {
                            if (u.size() > 0) {
                                u.push_back(c[0]);
                            }
                            else {
                                tmp.append(c);
                            }
                        }
                    }
                    if (inEscape) {
                        inEscape = false;
                        char tc = tmp.back();
                        tmp.pop_back();
                        tmp.pop_back();
                        switch (tc) {
                            case '"':
                                tmp.append("\"");
                                break;
                            case '/':
                                tmp.append("/");
                                break;
                            case '\\':
                                tmp.append("\\");
                                break;
                            case 'b':
                                tmp.append("\b");
                                break;
                            case 'f':
                                tmp.append("\f");
                                break;
                            case 'n':
                                tmp.append("\n");
                                break;
                            case 'r':
                                tmp.append("\r");
                                break;
                            case 't':
                                tmp.append("\t");
                                break;
                            case 'u':
                                inEscape = true;
                                if (!u.size()) { // start collection
                                    u.push_back('\0');
                                    need_u = 0;
                                }
                                if (u.size() == 5) { // collected all items
                                    int64_t unicode = ByteCodeUtils::asHexNumber(StringConvUtils::u8tou(std::string(++u.begin(), u.end())));
                                    if (surrogate >= 0) {
                                        unicode = 0x10000 + ((surrogate & 0x3FF) << 10) + (unicode & 0x3FF);
                                        tmp += StringConvUtils::utou8(icu::UnicodeString(UChar32(unicode)));
                                    }
                                    else {
                                        if (unicode >= 0xD800 && unicode <= 0xDBFF) {
                                            surrogate = unicode;
                                            need_u = 1;
                                        }
                                        else {
                                            tmp += StringConvUtils::utou8(icu::UnicodeString(UChar32(unicode)));
                                        }
                                    }
                                    u.clear();
                                    inEscape = false;
                                }
                                else {
                                    tmp.append("\\u");
                                }
                                break;
                            default:
                                return -1;
                        }
                    }
                    if (c[0] == '\\') {
                        inEscape = true;
                    }
                    if (need_u == 2) {
                        if (!inEscape) {
                            return -1;
                        }
                        need_u = 3;
                    }
                    if (need_u == 3 && u.empty()) {
                        return -1;
                    }
                }
            }
            if (started != 2) {
                return -1;
            }
            if (!dataSource->fail()) {
                if (tmp.length()) {
                    result.assign(tmp.begin(), tmp.end());
                    return (int64_t) tmp.length();
                }
                result.push_back(0);
                return 0;
            }
            return -1;
        };

        ByteState::stateFiller setBoolValue =
        [](std::vector<uint8_t>& data, int64_t& size, std::shared_ptr<Carried>& carried, std::shared_ptr<std::vector<std::shared_ptr<ByteCode>>>& result) -> int8_t
        {
            Value v;
            v.isnull = false;
            v.boolean = data[0] == 't';
            carried->objects.push_back(std::shared_ptr<Object>(new Object{ { USS("value"), v } }));
            return 1;
        };
        ByteState::stateFiller prepareArrayValue =
        [](std::vector<uint8_t>& data, int64_t& size, std::shared_ptr<Carried>& carried, std::shared_ptr<std::vector<std::shared_ptr<ByteCode>>>& result) -> int8_t
        {
            Value v;
            v.isnull = false;
            v.array = std::make_shared<std::deque<Value>>();
            carried->objects.push_back(std::shared_ptr<Object>(new Object{ { USS("value"), v } }));
            return 1;
        };
        ByteState::stateFiller prepareObjectValue =
        [](std::vector<uint8_t>& data, int64_t& size, std::shared_ptr<Carried>& carried, std::shared_ptr<std::vector<std::shared_ptr<ByteCode>>>& result) -> int8_t
        {
            Value v;
            v.isnull = false;
            v.object = std::make_shared<Object>();
            carried->objects.push_back(std::shared_ptr<Object>(new Object{ { USS("value"), v } }));
            return 1;
        };
        ByteState::stateFiller prepareEmpty =
        [](std::vector<uint8_t>& data, int64_t& size, std::shared_ptr<Carried>& carried, std::shared_ptr<std::vector<std::shared_ptr<ByteCode>>>& result) -> int8_t
        {
            if (data[0] == ']') {
                Value v;
                v.isempty = true;
                carried->objects.push_back(std::shared_ptr<Object>(new Object{ { USS("value"), v } }));
            }
            return 1;
        };
        ByteState::stateFiller jsonStack =
        [](std::vector<uint8_t>& data, int64_t& size, std::shared_ptr<Carried>& carried, std::shared_ptr<std::vector<std::shared_ptr<ByteCode>>>& result) -> int8_t
        {
            if (!carried) {
                carried = std::make_shared<Carried>();
            }
            Value v;
            v.isnull = false;
            v.object = std::make_shared<Object>();
            carried->objects.push_back(std::shared_ptr<Object>(new Object{ { USS("value"), v } }));
            return 1;
        };
        ByteState::stateFiller setNullValue =
        [](std::vector<uint8_t>& data, int64_t& size, std::shared_ptr<Carried>& carried, std::shared_ptr<std::vector<std::shared_ptr<ByteCode>>>& result) -> int8_t
        {
            Value v;
            v.isnull = true;
            carried->objects.push_back(std::shared_ptr<Object>(new Object{ { USS("value"), v } }));
            return 1;
        };
        ByteState::stateFiller setNumberValue =
        [](std::vector<uint8_t>& data, int64_t& size, std::shared_ptr<Carried>& carried, std::shared_ptr<std::vector<std::shared_ptr<ByteCode>>>& result) -> int8_t
        {
            Value v;
            v.isnull = false;
            v.number = ByteCodeUtils::asNumber(StringConvUtils::u8tou(std::string(data.begin(), data.end())));
            carried->objects.push_back(std::shared_ptr<Object>(new Object{ { USS("value"), v } }));
            return 1;
        };
        ByteState::stateFiller setStringValue =
        [](std::vector<uint8_t>& data, int64_t& size, std::shared_ptr<Carried>& carried, std::shared_ptr<std::vector<std::shared_ptr<ByteCode>>>& result) -> int8_t
        {
            Value v;
            v.isnull = false;
            v.string = StringConvUtils::u8tou(std::string(data.begin(), data.end()));
            carried->objects.push_back(std::shared_ptr<Object>(new Object{ { USS("value"), v } }));
            return 1;
        };
        ByteState::stateFiller setKey =
        [](std::vector<uint8_t>& data, int64_t& size, std::shared_ptr<Carried>& carried, std::shared_ptr<std::vector<std::shared_ptr<ByteCode>>>& result) -> int8_t
        {
            carried->objects.back()->operator[](StringConvUtils::u8tou(std::string(data.begin(), data.end()))) = Value();
            return 1;
        };
        
        int8_t popLast(std::shared_ptr<Carried>& v, Value val, std::shared_ptr<std::vector<std::shared_ptr<ByteCode>>>& result)
        {
            if (v->objects.empty()) {
                if (val.isempty) {
                    return false;
                }
                return JSONToByteCode::buildResult(val, result, v->ctx->byteLoader);
            }
            if (v->objects.size() <= 2) {
                val.isnull = false;
            }
            auto po = v->objects.back();
            auto container = po->operator[](USS("value"));
            if (container.array != nullptr) {
                if (!val.isempty) {
                    container.array->push_back(val);
                }
            }
            else {
                auto key = container;
                v->objects.pop_back();
                if (v->objects.empty()) {
                    return false;
                }
                po = v->objects.back();
                container = po->operator[](USS("value"));
                if (container.object != nullptr && !val.isempty) {
                    container.object->operator[](key.string) = val;
                }
                else {
                    return false;
                }
            }
            return true;
        }
        
        ByteState::stateFiller actByEndingSymbol =
        [](std::vector<uint8_t>& data, int64_t& size, std::shared_ptr<Carried>& carried, std::shared_ptr<std::vector<std::shared_ptr<ByteCode>>>& result) -> int8_t
        {
            if (!carried->objects.size()) {
                return 0;
            }
            auto o = carried->objects.back();
            auto val = o->operator[](USS("value"));
            auto c = 0;
            if (size > 0) {
                c = data[0];
            }
            switch (c) {
                case 0:
                case ',':
                case ']':
                case '}':
                    carried->objects.pop_back();
                    if (!popLast(carried, val, result)) {
                        return 0;
                    }
                    break;
                default:
                    if (val.array != nullptr) {
                        data[0] = 'a';
                    }
                    else {
                        if (val.object != nullptr) {
                            data[0] = 'o';
                        }
                        else {
                            data[0] = 'e';
                        }
                    }
                    break;
            }
            return 1;
        };
        
        ByteState::stateStepper stepOutOrNext =
        [](std::vector<uint8_t>& data, int64_t size, std::shared_ptr<Carried> c, int16_t& state) -> int8_t
        {
            if (!c->objects.size()) {
                state = -1;
            }
            else {
                state = 17;
            }
            return 1;
        };

        ByteState::stateError jsonError =
        [](std::vector<uint8_t>& data, int16_t state, int64_t symbol) -> icu::UnicodeString { return _i("err.parser.invalid.json"); };

        std::vector<ByteState> JSONToByteCode::states = {
            { readByte, alwaysTrue, jsonStack, steps0<10>().next, jsonError }, //0
            // value
            { passWhiteSpaces, alwaysTrue, noop, stepsInc().next, jsonError }, //1
            { peekByte, alwaysTrue, prepareEmpty, steps4<'{', 9, '[', 8, ']', 15, '}', 15, 3, 0>().next, jsonError }, //2
            { peekByte, alwaysTrue, prepareEmpty, steps4<'f', 7, 't', 7, 'n', 6, '"', 5, 4, 0>().next, jsonError }, //3
            { readJsonNumber, alwaysTrue, setNumberValue, steps0<15>().next, jsonError }, //4
            { readJsonString, alwaysTrue, setStringValue, steps0<15>().next, jsonError }, //5
            { readJsonNull, alwaysTrue, setNullValue, steps0<15>().next, jsonError }, //6
            { readJsonBool, alwaysTrue, setBoolValue, steps0<15>().next, jsonError }, //7
            { readByte, alwaysTrue, prepareArrayValue, steps0<1>().next, jsonError }, //8
            { readByte, alwaysTrue, prepareObjectValue, steps0<10>().next, jsonError }, //9
            // object
            { passWhiteSpaces, alwaysTrue, noop, stepsInc().next, jsonError }, //10
            // key
            { peekByte, alwaysTrue, noop, steps1<'}', 15, 12>().next, jsonError }, //11
            { readJsonString, alwaysTrue, setStringValue, stepsInc().next, jsonError }, //12
            { passWhiteSpaces, alwaysTrue, noop, stepsInc().next, jsonError }, //13
            // :
            { readByte, isAtom1<':'>().next, noop, steps0<1>().next, jsonError }, //14
            // , } ]
            { passWhiteSpaces, alwaysTrue, noop, stepsInc().next, jsonError }, //15
            { readByte, isAtom3<',', ']', '}'>().next, actByEndingSymbol, stepsInc().next, jsonError, 1 }, //16
            { passWhiteSpaces, alwaysTrue, actByEndingSymbol, steps5<',', 18, ']', 18, '}', 18, 'a', 2, 'o', 12, 0, 1>().next, jsonError, 1 }, //17
            { readByte, alwaysTrue, noop, stepOutOrNext, jsonError, 1 }, //18
        };
    }
}