/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#include <NameParserException.hpp>
#include <Internationalization.hpp>
#include "../utils/Utils.hpp"

namespace nnvm {
    NameParserException::NameParserException(const icu::UnicodeString& source, const icu::UnicodeString& message, int16_t state, int64_t symbol):
        std::runtime_error(StringConvUtils::utou8(StringUtils::spf(_i("err.name.parsererror"), StringConvUtils::utow(source), state, symbol, StringConvUtils::utow(message))))
    {
        this->source = source;
        this->message = message;
        this->state = state;
        this->symbol = symbol;
    }
}
