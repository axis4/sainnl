/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#include "Language.hpp"
#include <StringConv.hpp>
#include <algorithm>
#include <sstream>
#include <cassert>
#include <unicode/regex.h>
#include <unicode/uchar.h>
#include <StateMachine.hpp>
#include <ProcessorKnownParameters.hpp>
#include <Internationalization.hpp>
#include <ByteCodeBlock.hpp>
#include <ByteCodeException.hpp>
#include <ByteCodeParserException.hpp>
#include <ByteCodeUtils.hpp>
#include <DefaultNeurons.hpp>
#include <ByteCodeParser.hpp>
#include <Calculator.hpp>
#include <utility>
#include "DefinitionBuilder.hpp"
#include "../utils/Utils.hpp"

namespace nnvm {
    icu::UnicodeString tokenTypeString(TokenKind& type)
    {
        switch (type) {
            case TokenKind::LITERAL:
                return USS("LITERAL");
                break;
                
            case TokenKind::NUMBER:
                return USS("NUMBER");
                break;
                
            case TokenKind::COLON:
                return USS("COLON");
                break;
                
            case TokenKind::KEY_ARG:
                return USS("KEY_ARG");
                break;
                
            case TokenKind::LBRACKET:
                return USS("LBRACKET");
                break;
                
            case TokenKind::RBRACKET:
                return USS("RBRACKET");
                break;
                
            case TokenKind::OPERATOR:
                return USS("OPERATOR");
                break;
                
            case TokenKind::LINEAR_OPERATOR:
                return USS("LINEAR_OPERATOR");
                break;
                
            case TokenKind::FACTOR_OPERATOR:
                return USS("FACTOR_OPERATOR");
                break;
                
            case TokenKind::FROM:
                return USS("FROM");
                break;
                
            case TokenKind::EQUAL:
                return USS("EQUAL");
                break;
                
            case TokenKind::NOT:
                return USS("NOT");
                break;
                
            case TokenKind::CONDITIONAL:
                return USS("CONDITIONAL");
                break;
                
            case TokenKind::ARG_DELIMITER:
                return USS("ARG_DELIMITER");
                break;
                
            case TokenKind::ARGS:
                return USS("ARGS");
                break;
                
            case TokenKind::ASSIGN:
                return USS("ASSIGN");
                break;
                
            case TokenKind::REDUCTION:
                return USS("REDUCTION");
                break;
                
            case TokenKind::ASSIGN_REDUCTION:
                return USS("ASSIGN_REDUCTION");
                break;
                
            case TokenKind::EXPRESSION:
                return USS("EXPRESSION");
                break;
                
            case TokenKind::CALL:
                return USS("CALL");
                break;
                
            case TokenKind::DEFINITION:
                return USS("DEFINITION");
                break;
                
            case TokenKind::TOKEN_EOF:
                return USS("TOKEN_EOF");
                break;
                
            default:
                return USS("unknown");
                break;
        }
    }

    LanguageNode::LanguageNode(std::shared_ptr<ByteCodeLoader> loader) : loader(std::move(loader)) { }
    LanguageNode::LanguageNode(const LanguageNode& obj) = default;

    LanguageTemplate::LanguageTemplate(std::shared_ptr<ByteCodeLoader> loader)
            : LanguageNode(std::move(loader)), name(USS("")), rename(std::shared_ptr<icu::UnicodeString>(nullptr)),
              rangeStart(std::shared_ptr<Operand>(nullptr)), rangeEnd(std::shared_ptr<Operand>(nullptr)),
              condition(std::shared_ptr<Operand>(nullptr)) { }
    LanguageTemplate::LanguageTemplate(const LanguageTemplate& obj) = default;

    LanguageHeader::LanguageHeader(std::shared_ptr<ByteCodeLoader> loader)
            : LanguageNode(std::move(loader)), inputs(std::deque<icu::UnicodeString>()), templates(std::deque<LanguageTemplate>()) { }
    LanguageHeader::LanguageHeader(const LanguageHeader& obj) = default;

    Operand::Operand(std::shared_ptr<ByteCodeLoader> loader)
            : LanguageNode(std::move(loader)), call(std::shared_ptr<LanguageCall>(nullptr)),
              literal(std::shared_ptr<Token<TokenKind> >()), expression(std::shared_ptr<ExpressionTree>(nullptr)) { }
    Operand::Operand(const Operand& obj) = default;

    ExpressionTree::ExpressionTree(std::shared_ptr<ByteCodeLoader> loader)
            : LanguageNode(std::move(loader)), left(std::shared_ptr<Operand>(nullptr)), right(std::shared_ptr<Operand>(nullptr)),
              middle(USS("")) { }
    ExpressionTree::ExpressionTree(const ExpressionTree& obj) = default;

    LanguageArgument::LanguageArgument(std::shared_ptr<ByteCodeLoader> loader)
            : LanguageNode(loader), operand(Operand(loader)),
              rawTokens(std::shared_ptr<std::deque<Token<TokenKind> > >(nullptr)) { }
    LanguageArgument::LanguageArgument(const LanguageArgument& obj) = default;

    LanguageCall::LanguageCall(std::shared_ptr<ByteCodeLoader> loader)
            : LanguageNode(std::move(loader)), blockName(USS("")), extractedName(USS("")),
              arguments(std::deque<LanguageArgument>()), templateArguments(std::deque<LanguageArgument>()),
              rawTokens(std::deque<std::deque<Token<TokenKind> > >()) { }
    LanguageCall::LanguageCall(const LanguageCall& obj) = default;

    LanguageDefinition::LanguageDefinition(std::shared_ptr<ByteCodeLoader> loader)
            : LanguageNode(loader), operand(Operand(loader)), templateNames(std::deque<icu::UnicodeString>()),
              name(std::shared_ptr<icu::UnicodeString>(nullptr)), reduction(0),
              fixedIndexState(std::map<icu::UnicodeString, int64_t>()) { }
    LanguageDefinition::LanguageDefinition(const LanguageDefinition& obj)
            : LanguageNode(obj), operand(obj.operand), templateNames(obj.templateNames),
              name(obj.name), reduction(obj.reduction), fixedIndexState(obj.fixedIndexState) { }

    LanguageBlock::LanguageBlock(std::shared_ptr<ByteCodeLoader> loader)
            : LanguageNode(loader), name(USS("")), header(LanguageHeader(loader)),
              definitions(std::deque<LanguageDefinition>()) { }
    LanguageBlock::LanguageBlock(const LanguageBlock& obj) = default;

    LanguageConst::LanguageConst(std::shared_ptr<ByteCodeLoader> loader)
            : LanguageNode(std::move(loader)), name(USS("")), value(0.0) { }
    LanguageConst::LanguageConst(const LanguageConst& obj)
            : LanguageNode(obj), name(obj.name), value(obj.value) { }

    LanguageTree::LanguageTree(std::shared_ptr<ByteCodeLoader> loader)
            : LanguageNode(std::move(loader)), consts(std::deque<LanguageConst>()), blocks(std::deque<LanguageBlock>()) { }
    LanguageTree::LanguageTree(const LanguageTree& obj) = default;

    Token<TokenKind> LanguageModel::fullCopy(const Token<TokenKind>& token)
    {
        Token<TokenKind> res(token.owner);
        res.value = token.value;
        for (auto i : token.type) {
            res.type.push_back(i);
        }
        for (const auto & subToken : token.subTokens) {
            res.subTokens.push_back(fullCopy(subToken));
        }
        return res;
    }

    std::deque<Token<TokenKind>> LanguageModel::fullCopy(const std::deque<Token<TokenKind>>& tokens)
    {
        std::deque<Token<TokenKind>> res;
        for (const auto & token : tokens) {
            res.push_back(fullCopy(token));
        }
        return res;
    }

    Operand LanguageModel::fullCopy(const Operand& operand)
    {
        Operand res(operand.loader);
        res.call = operand.call ? std::make_shared<LanguageCall>(fullCopy(*operand.call)) : nullptr;
        res.expression = operand.expression ? std::make_shared<ExpressionTree>(fullCopy(*operand.expression)) : nullptr;
        res.literal = operand.literal ? std::make_shared<Token<TokenKind>>(fullCopy(*operand.literal)) : nullptr;
        return res;
    }

    LanguageArgument LanguageModel::fullCopy(const LanguageArgument& argument)
    {
        LanguageArgument res(argument.loader);
        res.operand = fullCopy(argument.operand);
        res.rawTokens = argument.rawTokens ? std::make_shared<std::deque<Token<TokenKind>>>(fullCopy(*argument.rawTokens)) : nullptr;
        return res;
    }

    std::deque<LanguageArgument> LanguageModel::fullCopy(const std::deque<LanguageArgument>& arguments)
    {
        std::deque<LanguageArgument> res;
        for (const auto & argument : arguments) {
            res.push_back(fullCopy(argument));
        }
        return res;
    }

    LanguageCall LanguageModel::fullCopy(const LanguageCall& call)
    {
        LanguageCall res(call.loader);
        res.blockName = call.blockName;
        res.extractedName = call.extractedName;
        res.templateArguments = fullCopy(call.templateArguments);
        res.arguments = fullCopy(call.arguments);
        for (const auto & rawToken : call.rawTokens) {
            res.rawTokens.push_back(fullCopy(rawToken));
        }
        return res;
    }

    ExpressionTree LanguageModel::fullCopy(const ExpressionTree& expression)
    {
        ExpressionTree res(expression.loader);
        res.left = expression.left ? std::make_shared<Operand>(fullCopy(*expression.left)) : nullptr;
        res.middle = expression.middle;
        res.right = expression.right ? std::make_shared<Operand>(fullCopy(*expression.right)) : nullptr;
        return res;
    }

    std::deque<LanguageTemplate> LanguageModel::fullCopy(const std::deque<LanguageTemplate>& templates)
    {
        std::deque<LanguageTemplate> res;
        for (const auto & i : templates) {
            LanguageTemplate t(i.loader);
            t.name = i.name;
            t.rename = i.rename ? std::make_shared<icu::UnicodeString>(*i.rename) : nullptr;
            t.rangeStart = i.rangeStart ? std::make_shared<Operand>(fullCopy(*i.rangeStart)) : nullptr;
            t.rangeEnd = i.rangeEnd ? std::make_shared<Operand>(fullCopy(*i.rangeEnd)) : nullptr;
            t.condition = i.condition ? std::make_shared<Operand>(fullCopy(*i.condition)) : nullptr;
            res.push_back(t);
        }
        return res;
    }

    LanguageHeader LanguageModel::fullCopy(const LanguageHeader& header)
    {
        LanguageHeader res(header.loader);
        for (const auto & input : header.inputs) {
            res.inputs.push_back(input);
        }
        res.templates = fullCopy(header.templates);
        return res;
    }

    LanguageDefinition LanguageModel::fullCopy(const LanguageDefinition& definition)
    {
        LanguageDefinition res(definition.loader);
        res.name = definition.name ? std::make_shared<icu::UnicodeString>(*definition.name) : nullptr;
        res.reduction = definition.reduction;
        for (const auto & templateName : definition.templateNames) {
            res.templateNames.push_back(templateName);
        }
        res.operand = fullCopy(definition.operand);
        return res;
    }

    LanguageBlock LanguageModel::fullCopy(const LanguageBlock& block)
    {
        LanguageBlock res(block.loader);
        res.name = block.name;
        res.header = fullCopy(block.header);
        for (const auto & definition : block.definitions) {
            res.definitions.push_back(fullCopy(definition));
        }
        return res;
    }

    icu::UnicodeString LanguageModel::toString(const Token<TokenKind>& token)
    {
        return token.value;
    }

    icu::UnicodeString LanguageModel::toString(const std::deque<Token<TokenKind>>& tokens)
    {
        std::vector<icu::UnicodeString> s;
        for (const auto & token : tokens) {
            s.push_back(toString(token));
        }
        return StringUtils::joinArray(s, USS(" "));
    }

    icu::UnicodeString LanguageModel::toString(const Operand& o, int8_t is_top)
    {
        if (o.call) {
            return is_top ? toString(*o.call) : USS("(") + toString(*o.call) + USS(")");
        }
        if (o.literal) {
            return toString(*o.literal);
        }
        if (o.expression) {
            return is_top ? toString(*o.expression) : USS("(") + toString(*o.expression) + USS(")");
        }
        return USS("");
    }

    icu::UnicodeString LanguageModel::toString(const ExpressionTree& e)
    {
        auto res = USS("");
        if (e.left) {
            res += toString(*e.left, 0);
        }
        if (e.middle.countChar32()) {
            res += USS(" ") + e.middle + USS(" ");
        }
        if (e.right) {
            res += toString(*e.right, 0);
        }
        return res;
    }

    icu::UnicodeString LanguageModel::toString(const LanguageArgument& a)
    {
        return a.rawTokens ? toString(*a.rawTokens) : toString(a.operand, 0);
    }

    icu::UnicodeString LanguageModel::toString(const LanguageCall& c)
    {
        auto res = USS("");
        if (c.extractedName.countChar32()) {
            res += c.extractedName + USS(" from ");
        }
        res += c.blockName;
        std::vector<icu::UnicodeString> v;
        for (const auto & templateArgument : c.templateArguments) {
            v.push_back(toString(templateArgument));
        }
        if (!v.empty()) {
            res += USS(" \\[") + StringUtils::joinArray(v, USS(", ")) + USS("]");
        }
        v.clear();
        for (const auto & argument : c.arguments) {
            v.push_back(toString(argument));
        }
        if (!v.empty()) {
            res += USS(" ") + StringUtils::joinArray(v, USS(", "));
        }
        return res;
    }

    icu::UnicodeString LanguageModel::toString(const LanguageDefinition& d)
    {
        auto res = USS("");
        if (!d.templateNames.empty()) {
            res += USS("[") + StringUtils::joinArray(std::vector<icu::UnicodeString>(d.templateNames.begin(), d.templateNames.end()), USS(", ")) + USS("] ");
        }
        if (d.name) {
            res += *d.name + USS(" is ");
            if (d.reduction) {
                res += USS("reduction ");
            }
        }
        res += toString(d.operand, 1) + USS("\n");
        return res;
    }

    icu::UnicodeString LanguageModel::toString(const LanguageBlock& b)
    {
        auto res = b.name;
        if (!b.header.templates.empty()) {
            res += USS(" [");
            std::vector<icu::UnicodeString> ss;
            for (const auto & i : b.header.templates) {
                auto t = USS("");
                t += i.name;
                if (i.rename) {
                    t += USS(" = ") + *i.rename;
                }
                else {
                    if (i.rangeStart && i.rangeEnd) {
                        t += USS(" = ") + toString(*i.rangeStart, 1) + USS(" : ") + toString(*i.rangeEnd, 1);
                    }
                    else {
                        if (i.condition) {
                            t += USS(" ") + toString(*i.condition, 1);
                        }
                    }
                }
                ss.push_back(t);
            }
            res += StringUtils::joinArray(ss, USS(", "));
            res += USS("]");
        }
        if (!b.header.inputs.empty()) {
            res += USS(" ") + StringUtils::joinArray(std::vector<icu::UnicodeString>(b.header.inputs.begin(), b.header.inputs.end()), USS(", "));
        }
        res += USS(" {\n");
        for (const auto & definition : b.definitions) {
            res += USS("\t") + toString(definition);
        }
        res += USS("}\n");
        return res;
    }

    std::shared_ptr<ByteCode> LanguageModel::loadBlock(icu::UnicodeString blockName, icu::UnicodeString prefix, CompileContext& cctx, std::shared_ptr<Context> ctx, std::map<icu::UnicodeString, double>& templateContext)
    {
        std::shared_ptr<ByteCode> package = nullptr;

        auto fullName = cctx.prefix + USS(".") + blockName;

        // local name to an only prepared block
        auto p = ctx->prepared->find(fullName);
        if (p == ctx->prepared->end()) {
            // full name to an only prepared block
            p = ctx->prepared->find(blockName);
            if (p == ctx->prepared->end()) {
                // load from outside package
                try {
                    ByteCodeLoader loader(ctx->setup, ctx->loader);
                    package = loader.load(blockName, ctx);
                }
                catch (ByteCodeException &e) {
                    // pass, could fail for template
                }
            }
        }
        auto compiledName = blockName;
        auto t = ctx->prepared->find(fullName);
        if (t == ctx->prepared->end()) {
            t = ctx->prepared->find(blockName);
        }
        if (t != ctx->prepared->end()) {
            auto tb = LanguageModel::fullCopy(t->second);
            if (!nnvm::Language::Language::compileRanges(tb, cctx.prefix, templateContext) || !nnvm::Language::Language::compileName(tb, cctx.prefix, compiledName)) {
                throw ByteCodeParserException(_i("err.parser.language"), StringUtils::spf(_i("err.compiler.not_found_block"), StringConvUtils::utow(blockName)), 0, 0);
            }
        }
        auto fullCompiledName = cctx.prefix + USS(".") + compiledName;

        if (!package) {
            // local name to a compiled block
            auto c = ctx->compiled->find(fullCompiledName);
            if (c == ctx->compiled->end()) {
                // full name to a compiled block
                c = ctx->compiled->find(compiledName);
                if (c == ctx->compiled->end()) {
                    auto bc = ctx->compileStarted->find(compiledName);
                    if (bc != ctx->compileStarted->end()) {
                        throw ByteCodeParserException(_i("err.parser.language"), StringUtils::spf(_i("err.compiler.call_cycle"), StringConvUtils::utow(compiledName)), 0, 0);
                    }
                    bc = ctx->compileStarted->find(fullCompiledName);
                    if (bc != ctx->compileStarted->end()) {
                        throw ByteCodeParserException(_i("err.parser.language"), StringUtils::spf(_i("err.compiler.call_cycle"), StringConvUtils::utow(fullCompiledName)), 0, 0);
                    }

                    // full name to an only prepared block
                    p = ctx->prepared->find(blockName);
                    if (p == ctx->prepared->end()) {
                        // local name to an only prepared block
                        p = ctx->prepared->find(fullName);
                        if (p == ctx->prepared->end()) {
                            // load from outside package
                            try {
                                ByteCodeLoader loader(ctx->setup, ctx->loader);
                                package = loader.load(blockName, ctx);
                            }
                            catch (ByteCodeException &e) {
                                // will fail for template

                                // full name to an only prepared block
                                p = ctx->prepared->find(blockName);
                                if (p == ctx->prepared->end()) {
                                    // local name to an only prepared block
                                    p = ctx->prepared->find(fullName);
                                    if (p == ctx->prepared->end()) {
                                        throw e;
                                    }
                                    else {
                                        // compile from current package
                                        std::shared_ptr<ByteCode> tmpPackage;
                                        if (nnvm::Language::Language::compileBlock(p->second, cctx.prefix, templateContext, tmpPackage, ctx->setup->getByteCodeParserSetup()->neurons, ctx)) {
                                            package = tmpPackage;
                                        }
                                    }
                                }
                                else {
                                    // compile from current package
                                    std::shared_ptr<ByteCode> tmpPackage;
                                    if (nnvm::Language::Language::compileBlock(p->second, cctx.prefix, templateContext, tmpPackage, ctx->setup->getByteCodeParserSetup()->neurons, ctx)) {
                                        package = tmpPackage;
                                    }
                                }
                            }
                        }
                        else {
                            // compile from current package
                            std::shared_ptr<ByteCode> tmpPackage;
                            if (nnvm::Language::Language::compileBlock(p->second, cctx.prefix, templateContext, tmpPackage, ctx->setup->getByteCodeParserSetup()->neurons, ctx)) {
                                package = tmpPackage;
                            }
                        }
                    }
                    else {
                        // compile from current package
                        std::shared_ptr<ByteCode> tmpPackage;
                        if (nnvm::Language::Language::compileBlock(p->second, cctx.prefix, templateContext, tmpPackage, ctx->setup->getByteCodeParserSetup()->neurons, ctx)) {
                            package = tmpPackage;
                        }
                    }
                }
                else {
                    // use already compiled
                    package = c->second;
                }
            }
            else {
                // use already compiled
                package = c->second;
            }
        }

        if (!package) {
            throw ByteCodeParserException(_i("err.parser.language"), StringUtils::spf(_i("err.compiler.not_found_block"), StringConvUtils::utow(blockName)), 0, 0);
        }

        return package;
    }

    namespace Language
    {
        #define STANDARD_BLOCK NeuronNames::ADDITIVE

        template<typename TParam>
        void pickParameter(std::shared_ptr<TParam> oldBlock, std::shared_ptr<TParam> newBlock, icu::UnicodeString name, std::shared_ptr<ByteCodeParameter>& oldParameter, std::shared_ptr<ByteCodeParameter>& newParameter)
        {
            oldBlock->tryGetParameter(name, oldParameter);
            newBlock->tryGetParameter(name, newParameter);
        }
        
        void mergeParameterAll(std::shared_ptr<ByteCodeParameter> oldParameter, std::shared_ptr<ByteCodeParameter> newParameter)
        {
            oldParameter->value.byteValue &= (int8_t)ByteCodeUtils::asDouble(*newParameter);
        }
        
        void mergeParameterAny(std::shared_ptr<ByteCodeParameter> oldParameter, std::shared_ptr<ByteCodeParameter> newParameter)
        {
            oldParameter->value.byteValue |= (int8_t)ByteCodeUtils::asDouble(*newParameter);
        }
        
        void mergeParameterSum(std::shared_ptr<ByteCodeParameter> oldParameter, std::shared_ptr<ByteCodeParameter> newParameter)
        {
            oldParameter->value.doubleValue += ByteCodeUtils::asDouble(*newParameter);
        }
        
        template<typename TParam>
        void mergeParameterAll(std::shared_ptr<TParam> oldBlock, std::shared_ptr<TParam> newBlock, icu::UnicodeString name)
        {
            std::shared_ptr<ByteCodeParameter> oldParameter;
            std::shared_ptr<ByteCodeParameter> newParameter;
            pickParameter(oldBlock, newBlock, name, oldParameter, newParameter);
            if (oldParameter) {
                if (!newParameter) {
                    newParameter = std::make_shared<ByteCodeParameter>(name, (int8_t)0);
                }
                if (oldParameter->getType() != ByteCodeParameterTypes::BYTE) {
                    oldParameter = std::make_shared<ByteCodeParameter>(name, (int8_t)ByteCodeUtils::asDouble(*oldParameter));
                    oldBlock->setParameter(oldParameter);
                }
                mergeParameterAll(oldParameter, newParameter);
            }
        }
        
        template<typename TParam>
        void mergeParameterAny(std::shared_ptr<TParam> oldBlock, std::shared_ptr<TParam> newBlock, icu::UnicodeString name)
        {
            std::shared_ptr<ByteCodeParameter> oldParameter;
            std::shared_ptr<ByteCodeParameter> newParameter;
            pickParameter(oldBlock, newBlock, name, oldParameter, newParameter);
            if (newParameter) {
                if (!oldParameter) {
                    oldParameter = std::make_shared<ByteCodeParameter>(name, (int8_t)0);
                    oldBlock->setParameter(oldParameter);
                }
                if (oldParameter->getType() != ByteCodeParameterTypes::BYTE) {
                    oldParameter = std::make_shared<ByteCodeParameter>(name, (int8_t)ByteCodeUtils::asDouble(*oldParameter));
                    oldBlock->setParameter(oldParameter);
                }
                mergeParameterAny(oldParameter, newParameter);
            }
        }
        
        template<typename TParam>
        void mergeParameterSum(std::shared_ptr<TParam> oldBlock, std::shared_ptr<TParam> newBlock, icu::UnicodeString name)
        {
            std::shared_ptr<ByteCodeParameter> oldParameter;
            std::shared_ptr<ByteCodeParameter> newParameter;
            pickParameter(oldBlock, newBlock, name, oldParameter, newParameter);
            if (newParameter) {
                if (!oldParameter) {
                    oldParameter = std::make_shared<ByteCodeParameter>(name, 0.0);
                    oldBlock->setParameter(oldParameter);
                }
                if (oldParameter->getType() != ByteCodeParameterTypes::DOUBLE) {
                    oldParameter = std::make_shared<ByteCodeParameter>(name, ByteCodeUtils::asDouble(*oldParameter));
                    oldBlock->setParameter(oldParameter);
                }
                mergeParameterSum(oldParameter, newParameter);
            }
        }
        
        void mergeParameterIntegerNewOrOld(std::shared_ptr<ByteCodeBlock> oldBlock, std::shared_ptr<ByteCodeBlock> newBlock, icu::UnicodeString name)
        {
            std::shared_ptr<ByteCodeParameter> oldParameter;
            std::shared_ptr<ByteCodeParameter> newParameter;
            pickParameter(oldBlock, newBlock, name, oldParameter, newParameter);
            if (newParameter) {
                oldBlock->setParameter(std::make_shared<ByteCodeParameter>(name, (int64_t)ByteCodeUtils::asDouble(*newParameter)));
            }
        }

        int8_t mergeItem(std::shared_ptr<ParsedNeuron> neuron, std::shared_ptr<ByteCodeBlockItem> oldItem, std::shared_ptr<ByteCodeBlockItem> newItem)
        {
            // old.activation += new.activation
            mergeParameterSum(oldItem, newItem, LinkParams::ACTIVATION);
            // old.stream += new.stream
            mergeParameterSum(oldItem, newItem, LinkParams::STREAM);
            // old.generation |= new.generation
            mergeParameterAny(oldItem, newItem, LinkParams::GENERATION);
            
            return 1;
        }
        
        int8_t mergeBlock(std::shared_ptr<ParsedNeuron> neuron, std::shared_ptr<ByteCodeBlock> oldBlock, std::shared_ptr<ByteCodeBlock> newBlock)
        {
            // old.expected &= new.expected
            mergeParameterAll(oldBlock, newBlock, ElementParams::EXPECTED);
            // old.input |= new.input
            mergeParameterAny(oldBlock, newBlock, ElementParams::INPUT);
            // old.input = new.input ?? old.input
            mergeParameterIntegerNewOrOld(oldBlock, newBlock, ElementParams::INDEX);
            // old.output |= new.output
            mergeParameterAny(oldBlock, newBlock, ElementParams::OUTPUT);
            // old.template |= new.template
            mergeParameterAny(oldBlock, newBlock, ElementParams::TEMPLATE);
            // old.threshold += new.threshold
            mergeParameterSum(oldBlock, newBlock, ElementParams::THRESHOLD);
            // old.bias += new.bias
            mergeParameterSum(oldBlock, newBlock, ElementParams::BIAS);
            
            // TODO: merge parameters by neuron

            auto incomes = newBlock->getIncomes();
            auto items = newBlock->getItems();
            for (auto i = std::get<0>(incomes); i != std::get<1>(incomes); ++i) {
                auto oin = oldBlock->getIncomes();
                auto k = std::get<0>(oin);
                for (; k != std::get<1>(oin); ++k) {
                    if ((*k)->getOwner().lock()->getName() == (*i)->getOwner().lock()->getName()) {
                        break;
                    }
                }
                if (k == std::get<1>(oin)) {
                    oldBlock->addIncome(*i);
                }
                else {
                    // no merge to avoid double merging - blocks should be full
                }
            }
            for (auto i = std::get<0>(items); i != std::get<1>(items); ++i) {
                auto oin = oldBlock->getItems();
                auto k = std::get<0>(oin);
                for (; k != std::get<1>(oin); ++k) {
                    if ((*k)->getName() == (*i)->getName()) {
                        break;
                    }
                }
                if (k == std::get<1>(oin)) {
                    (*i)->changeOwner(oldBlock);
                    oldBlock->addItem(*i);
                }
                else {
                    mergeItem(neuron, *k, *i);
                }
            }
            
            if (oldBlock->getType() == USS("")) {
                oldBlock->changeType(newBlock->getType());
            }
            else {
                if (newBlock->getType() == USS("")) {
                    // nothing to do
                } else {
                    if (oldBlock->getType() != newBlock->getType()) {
                        return 0;
                    }
                }
            }
            
            std::shared_ptr<ByteCodeParameter> p;
            if (oldBlock->getType() == USS("") && (!oldBlock->tryGetParameter(ElementParams::EXPECTED, p) || !p->value.byteValue)) {
                oldBlock->changeType(NeuronNames::ADDITIVE);
            }
            
            return 1;
        }

        int8_t Language::applyToTree(Operand& operand, std::function<void(Token<TokenKind>&)> action)
        {
            if (operand.literal) {
                action(*operand.literal);
            }
            if (operand.expression) {
                if (operand.expression->left) {
                    applyToTree(*operand.expression->left, action);
                }
                if (operand.expression->right) {
                    applyToTree(*operand.expression->right, action);
                }
            }
            if (operand.call) {
                applyToTree(*operand.call, action);
            }
            return 1;
        }
        
        int8_t Language::applyToTree(LanguageArgument& arg, std::function<void(Token<TokenKind>&)> action)
        {
            return applyToTree(arg.operand, std::move(action));
        }
        
        int8_t Language::applyToTree(LanguageCall& call, std::function<void(Token<TokenKind>&)> action)
        {
            for (auto & templateArgument : call.templateArguments) {
                applyToTree(templateArgument, action);
            }
            for (auto & argument : call.arguments) {
                applyToTree(argument, action);
            }
            return 1;
        }
        
        int8_t Language::collapseExpression(Operand& operand)
        {
            return Calculator::process(operand);
        }
        
        Permutator::Permutator(std::shared_ptr<std::map<icu::UnicodeString, int64_t>> rangeStates, std::list<icu::UnicodeString> orderedRange, std::map<icu::UnicodeString, std::tuple<int64_t, int64_t>> ranges)
                : Generator([this](std::shared_ptr<std::map<icu::UnicodeString, int64_t>> ctx, std::map<icu::UnicodeString, int64_t>& res) {
                    return this->yield(std::move(ctx), res);
                }, std::move(rangeStates)), ranges(std::move(ranges)), orderedRange(std::move(orderedRange))
        {
        }
        
        int8_t Permutator::yield(std::shared_ptr<std::map<icu::UnicodeString, int64_t>> ctx, std::map<icu::UnicodeString, int64_t>& res)
        {
            if (!ctx || ctx->empty() || ctx->find(USS("")) != ctx->end()) {
                return 0;
            }
            res = *ctx;
            for (auto & i : orderedRange) {
                ctx->operator[](i)++;
                if (ctx->operator[](i) <= std::get<1>(ranges[i])) {
                    break;
                }
                ctx->operator[](i) = std::get<0>(ranges[i]);
            }
            return 1;
        }
        
        icu::UnicodeString Permutator::asString(const std::map<icu::UnicodeString, int64_t>& s)
        {
            std::vector<icu::UnicodeString> res;
            for (auto & i : orderedRange) {
                auto m = s.find(i);
                if (m != s.end()) {
                    res.push_back(StringUtils::spf(USS("{0}={1}"), StringConvUtils::utow(m->first), m->second));
                }
            }
            return StringUtils::joinArray(res, USS("§"));
        }
        
        FilteredPermutator::FilteredPermutator(std::shared_ptr<std::map<icu::UnicodeString, int64_t>> rangeStates, std::list<icu::UnicodeString> orderedRange, std::map<icu::UnicodeString, std::tuple<int64_t, int64_t>> ranges, std::function<int8_t(std::map<icu::UnicodeString, int64_t>&)> filter)
                : Permutator(std::move(rangeStates), std::move(orderedRange), std::move(ranges)), filter(std::move(filter))
        {
        }
        
        int8_t FilteredPermutator::yield(std::shared_ptr<std::map<icu::UnicodeString, int64_t>> ctx, std::map<icu::UnicodeString, int64_t>& res)
        {
            int8_t r = 1;
            while ((r = this->Permutator::yield(ctx, res)) && !filter(res));
            return r;
        }
        
        ProjectedFilteredPermutator::ProjectedFilteredPermutator(std::shared_ptr<std::map<icu::UnicodeString, int64_t>> rangeStates, std::list<icu::UnicodeString> orderedRange, std::map<icu::UnicodeString, std::tuple<int64_t, int64_t>> ranges, std::function<int8_t(std::map<icu::UnicodeString, int64_t>&)> filter, std::set<icu::UnicodeString> indexes)
                : FilteredPermutator(std::move(rangeStates), std::move(orderedRange), std::move(ranges), std::move(filter)), indexes(std::move(indexes)), passed(std::set<icu::UnicodeString>())
        {
        }
        
        int8_t ProjectedFilteredPermutator::yield(std::shared_ptr<std::map<icu::UnicodeString, int64_t>> ctx, std::map<icu::UnicodeString, int64_t>& res)
        {
            int8_t r = 1;
            while ((r = this->FilteredPermutator::yield(ctx, res)) && passed.find(asString(res)) != passed.end());
            if (r) {
                passed.insert(asString(res));
            }
            return r;
        }
        
        icu::UnicodeString ProjectedFilteredPermutator::asString(const std::map<icu::UnicodeString, int64_t>& s)
        {
            std::vector<icu::UnicodeString> res;
            for (auto & i : orderedRange) {
                if (indexes.find(i) != indexes.end()) {
                    auto m = s.find(i);
                    if (m != s.end()) {
                        res.push_back(StringUtils::spf(USS("{0}={1}"), StringConvUtils::utow(m->first), m->second));
                    }
                }
            }
            return StringUtils::joinArray(res, USS("§"));
        }
        
        std::shared_ptr<std::map<icu::UnicodeString, int64_t> > Language::buildRanges(std::map<icu::UnicodeString, std::tuple<int64_t, int64_t> >& ranges)
        {
            std::map<icu::UnicodeString, int64_t> rangeStates;
            for (auto & range : ranges) {
                rangeStates[range.first] = std::get<0>(range.second);
            }
            return std::make_shared<std::map<icu::UnicodeString, int64_t> >(rangeStates);
        }
        std::shared_ptr<Permutator> Language::buildGenerator(std::map<icu::UnicodeString, std::tuple<int64_t, int64_t> >& ranges, std::list<icu::UnicodeString>& orderedRange, std::shared_ptr<std::map<icu::UnicodeString, int64_t> > rangeStates, const std::set<icu::UnicodeString> &indexes, const std::list<Operand>& conditions, const std::map<icu::UnicodeString, int64_t>& fixedState)
        {
            return std::make_shared<ProjectedFilteredPermutator>(rangeStates, orderedRange, ranges,
                                                                 [&orderedRange, &conditions, &fixedState](std::map<icu::UnicodeString, int64_t>& s)
            {
                // keep only matching to fixed states
                for (const auto & i : fixedState) {
                    auto f = s.find(i.first);
                    if (f != s.end() && f->second != i.second) {
                        return 0;
                    }
                }
                // keep only matching to user conditions states
                for (const auto & condition : conditions) {
                    auto e = std::make_shared<Operand>(LanguageModel::fullCopy(condition));
                    setTemplateIndexes(*e, s, std::set<icu::UnicodeString>());
                    if (!collapseExpression(*e)) {
                        return 0;
                    }
                    if (ByteCodeUtils::asNumber(e->literal->value) == 0) {
                        return 0;
                    }
                }
                return 1;
            }, indexes);
        }
        std::set<icu::UnicodeString> Language::findNameIndexes(std::map<icu::UnicodeString, std::tuple<int64_t, int64_t> >& ranges, icu::UnicodeString s)
        {
            std::set<icu::UnicodeString> parts;
            auto nt = NameParts::parseName(std::move(s));
            // find mutation index
            for (auto k = nt->indexes.begin(); k != nt->indexes.end(); ++k) {
                auto vk = ranges.find((*k)->toString());
                if (vk != ranges.end()) {
                    parts.insert((*k)->toString());
                }
            }
            return parts;
        }
        std::set<icu::UnicodeString> Language::findNameIndexes(std::map<icu::UnicodeString, std::tuple<int64_t, int64_t> >& ranges, LanguageDefinition& d)
        {
            if (d.name) {
                return findNameIndexes(ranges, *d.name);
            }
            return std::set<icu::UnicodeString>();
        }
        std::set<icu::UnicodeString> Language::findTemplateIndexes(std::map<icu::UnicodeString, std::tuple<int64_t, int64_t> >& ranges, Operand& o)
        {
            std::set<icu::UnicodeString> parts;
            applyToTree(o, [&parts, &ranges](Token<TokenKind>& t) {
                auto vs = ranges.find(t.value);
                if (vs != ranges.end()) {
                    parts.insert(t.value);
                }
                else {
                    auto nt = NameParts::parseName(t.value);
                    // find mutation index
                    for (auto k = nt->indexes.begin(); k != nt->indexes.end(); ++k) {
                        auto vk = ranges.find((*k)->toString());
                        if (vk != ranges.end()) {
                            parts.insert((*k)->toString());
                        }
                    }
                }
            });
            return parts;
        }
        std::set<icu::UnicodeString> Language::findTemplateIndexes(std::map<icu::UnicodeString, std::tuple<int64_t, int64_t> >& ranges, LanguageArgument& a)
        {
            std::set<icu::UnicodeString> parts;
            applyToTree(a, [&parts, &ranges](Token<TokenKind>& t) {
                auto vs = ranges.find(t.value);
                if (vs != ranges.end()) {
                    parts.insert(t.value);
                }
                else {
                    auto nt = NameParts::parseName(t.value);
                    // find mutation index
                    for (auto k = nt->indexes.begin(); k != nt->indexes.end(); ++k) {
                        auto vk = ranges.find((*k)->toString());
                        if (vk != ranges.end()) {
                            parts.insert((*k)->toString());
                        }
                    }
                }
            });
            return parts;
        }
        std::set<icu::UnicodeString> Language::findTemplateIndexes(std::map<icu::UnicodeString, std::tuple<int64_t, int64_t> >& ranges, LanguageCall& c)
        {
            std::set<icu::UnicodeString> parts;
            applyToTree(c, [&parts, &ranges](Token<TokenKind>& t) {
                auto vs = ranges.find(t.value);
                if (vs != ranges.end()) {
                    parts.insert(t.value);
                }
                else {
                    auto nt = NameParts::parseName(t.value);
                    // find mutation index
                    for (auto k = nt->indexes.begin(); k != nt->indexes.end(); ++k) {
                        auto vk = ranges.find((*k)->toString());
                        if (vk != ranges.end()) {
                            parts.insert((*k)->toString());
                        }
                    }
                }
            });
            return parts;
        }
        std::set<icu::UnicodeString> Language::findTemplateIndexes(std::map<icu::UnicodeString, std::tuple<int64_t, int64_t> >& ranges, LanguageDefinition& d)
        {
            return findTemplateIndexes(ranges, d.operand);
        }
        void Language::setTokenIndexes(Token<TokenKind>& t, const std::map<icu::UnicodeString, int64_t>& s, const std::set<icu::UnicodeString>& allowedIndexes)
        {
            auto vs = s.find(t.value);
            auto es = allowedIndexes.find(t.value);
            if (vs != s.end() && (es != allowedIndexes.end() || allowedIndexes.empty())) {
                t.value = ByteCodeUtils::asString(vs->second);
                t.type.clear();
                t.type.push_back(TokenKind::NUMBER);
                t.type.push_back(TokenKind::LITERAL);
            }
            else {
                auto nt = NameParts::parseName(t.value);
                // find mutation index
                int8_t any2 = 0;
                for (auto k = nt->indexes.begin(); k != nt->indexes.end(); ++k) {
                    auto vk = s.find((*k)->toString());
                    auto ek = allowedIndexes.find((*k)->toString());
                    if (vk != s.end() && (ek != allowedIndexes.end() || allowedIndexes.empty())) {
                        any2 = 1;
                        (*k)->name = ByteCodeUtils::asString(vk->second);
                        (*k)->indexes.clear();
                        (*k)->subname = nullptr;
                    }
                }
                if (any2) {
                    t.value = nt->toString();
                }
            }
        }
        void Language::setTemplateIndexes(Operand& operand, const std::map<icu::UnicodeString, int64_t>& s, const std::set<icu::UnicodeString>& allowedIndexes)
        {
            applyToTree(operand, [&s, &allowedIndexes](Token<TokenKind>& t) {
                setTokenIndexes(t, s, allowedIndexes);
            });
        }
        void Language::setTemplateIndexes(LanguageArgument& arg, const std::map<icu::UnicodeString, int64_t>& s, const std::set<icu::UnicodeString>& allowedIndexes)
        {
            applyToTree(arg, [&s, &allowedIndexes](Token<TokenKind>& t) {
                setTokenIndexes(t, s, allowedIndexes);
            });
        }
        void Language::setTemplateIndexes(LanguageCall& nc, const std::map<icu::UnicodeString, int64_t>& s, const std::set<icu::UnicodeString>& allowedIndexes)
        {
            applyToTree(nc, [&s, &allowedIndexes](Token<TokenKind>& t) {
                setTokenIndexes(t, s, allowedIndexes);
            });
        }
        void Language::setTemplateIndexes(LanguageDefinition& nd, const std::map<icu::UnicodeString, int64_t>& s, const std::set<icu::UnicodeString>& allowedIndexes)
        {
            setTemplateIndexes(nd.operand, s, allowedIndexes);
        }
        void Language::fixSubExpression(LanguageCall& c)
        {
            if (c.blockName == USS("") && !c.arguments.empty() && c.arguments[0].operand.call) {
                auto tmp = c.arguments[0].operand.call;
                c.arguments.clear();
                c.templateArguments.clear();
                c.rawTokens.clear();
                c.blockName = tmp->blockName;
                c.extractedName = tmp->extractedName;
                c.templateArguments = tmp->templateArguments;
                c.rawTokens = tmp->rawTokens;
                c.arguments = tmp->arguments;
            }
        }
        void Language::markAsNumberConstant(Operand operand,
                                            std::deque<icu::UnicodeString> templateNames,
                                            std::map<icu::UnicodeString, std::tuple<int64_t, int64_t>> ranges,
                                            std::map<icu::UnicodeString, double> &context,
                                            std::shared_ptr<Context> ctx)
        {
            applyToTree(operand, [&templateNames, &ranges, &context, ctx](Token<TokenKind> &t) {
                markAsNumberConstant(t, templateNames, ranges, context, ctx);
            });
        }
        void Language::markAsNumberConstant(std::vector<Token<TokenKind>>& tokens,
                                            std::deque<icu::UnicodeString> templateNames,
                                            std::map<icu::UnicodeString, std::tuple<int64_t, int64_t>> ranges,
                                            std::map<icu::UnicodeString, double> &context,
                                            std::shared_ptr<Context> ctx)
        {
            for (auto & token : tokens) {
                markAsNumberConstant(token, templateNames, ranges, context, ctx);
            }
        }
        void Language::markAsNumberConstant(Token<TokenKind>& token,
                                            std::deque<icu::UnicodeString> templateNames,
                                            std::map<icu::UnicodeString, std::tuple<int64_t, int64_t>> ranges,
                                            std::map<icu::UnicodeString, double> &context,
                                            std::shared_ptr<Context> ctx)
        {
            // definition dynamic indexes
            for (auto & templateName : templateNames) {
                if (token.value == templateName) {
                    if (!token.isOf(TokenKind::AS_NUMBER)) {
                        token.type.push_back(TokenKind::AS_NUMBER);
                    }
                }
            }
            // block static indexes
            for (auto & range : ranges) {
                if (token.value == range.first) {
                    if (!token.isOf(TokenKind::AS_NUMBER)) {
                        token.type.push_back(TokenKind::AS_NUMBER);
                    }
                }
            }
            // global and call static indexes
            for (auto & k : context) {
                if (token.value == k.first) {
                    token.value = ByteCodeUtils::asString(k.second);
                    token.type.clear();
                    token.type.push_back(TokenKind::LITERAL);
                    token.type.push_back(TokenKind::NUMBER);
                }
            }
            // machine constants
            double value;
            if (ctx->setup->getConstant(token.value, value)) {
                if (!ctx->setup->isRuntimeConstant(token.value)) {
                    token.value = ByteCodeUtils::asString(value);
                    token.type.clear();
                    token.type.push_back(TokenKind::LITERAL);
                    token.type.push_back(TokenKind::NUMBER);
                }
                else {
                    if (!token.isOf(TokenKind::AS_NUMBER)) {
                        token.type.push_back(TokenKind::AS_NUMBER);
                    }
                }
            }
        }
        void Language::expandOperand(Operand& operand, std::map<icu::UnicodeString, std::tuple<int64_t, int64_t> >& ranges, std::list<icu::UnicodeString>& orderedRange, std::shared_ptr<Context> ctx, const std::list<Operand>& conditions, const std::map<icu::UnicodeString, int64_t>& fixedState)
        {
            if (operand.expression) {
                auto fti = findTemplateIndexes(ranges, operand);
                if (!fti.empty()) {
                    expandOperand(*operand.expression->left, ranges, orderedRange, ctx, conditions, fixedState);
                    expandOperand(*operand.expression->right, ranges, orderedRange, ctx, conditions, fixedState);
                }
            }
            else {
                if (operand.call) {
                    fixSubExpression(*operand.call);
                }

                auto fti = findTemplateIndexes(ranges, operand);
                if (!fti.empty()) {
                    if (operand.call && operand.call->blockName == NeuronNames::PLUS) {
                        for (auto i = 0; i < operand.call->arguments.size(); ++i) {
                            auto oldarg = operand.call->arguments[i];

                            auto ofti = findTemplateIndexes(ranges, oldarg);
                            if (!ofti.empty()) {
                                auto p = operand.call->arguments.begin();
                                std::advance(p, i);
                                operand.call->arguments.erase(p);
                                --i;
                                
                                auto g = buildGenerator(ranges, orderedRange, buildRanges(ranges), ofti, conditions, fixedState);
                                for (auto s = g->begin(); s != g->end(); ++s) {
                                    auto newarg = LanguageModel::fullCopy(oldarg);
                                    setTemplateIndexes(newarg, *s, std::set<icu::UnicodeString>());
                                    auto newarg_s = LanguageModel::toString(newarg);
                                    
                                    ++i;
                                    p = operand.call->arguments.begin();
                                    std::advance(p, i);
                                    operand.call->arguments.insert(p, newarg);
                                }
                            }
                        }
                    }
                    else {
                        auto g = buildGenerator(ranges, orderedRange, buildRanges(ranges), fti, conditions, fixedState);
                        auto sample = LanguageModel::fullCopy(operand);
                        operand.call = std::make_shared<LanguageCall>(ctx->byteLoader);
                        operand.literal = nullptr;
                        operand.expression = nullptr;
                        
                        operand.call->blockName = NeuronNames::PLUS;
                        
                        for (auto s = g->begin(); s != g->end(); ++s) {
                            auto o = LanguageModel::fullCopy(sample);
                            setTemplateIndexes(o, *s, std::set<icu::UnicodeString>());
                            LanguageArgument a(ctx->byteLoader);
                            a.operand = o;
                            operand.call->arguments.push_back(a);
                        }
                    }
                    
                    std::deque<LanguageDefinition> tempc;
                    int64_t temph;
                    std::cout << StringConvUtils::utou8(LanguageModel::toString(operand, 1)) << std::endl;
                    std::deque<icu::UnicodeString> templateNames;
                    DefinitionBuilder(ctx).simplifyExpression(operand, tempc, ranges, templateNames, temph);
                    std::cout << StringConvUtils::utou8(LanguageModel::toString(operand, 1)) << std::endl;
                    if (temph > 0 || !tempc.empty()) {
                        std::cout << "Issue!" << std::endl;
                    }
                }
            }
        }
        void Language::expandArguments(Operand& o, std::map<icu::UnicodeString, std::tuple<int64_t, int64_t> >& ranges, std::list<icu::UnicodeString>& orderedRange, std::shared_ptr<Context> ctx, const std::list<Operand>& conditions, const std::map<icu::UnicodeString, int64_t>& fixedState)
        {
            if (!o.call) {
                expandOperand(o, ranges, orderedRange, ctx, conditions, fixedState);
            }
            else {
                fixSubExpression(*o.call);
                auto bp = ctx->setup->getByteCodeParserSetup()->neurons.find(o.call->blockName);
                auto neuron = bp != ctx->setup->getByteCodeParserSetup()->neurons.end()
                    ? bp->second
                    : ctx->setup->getByteCodeParserSetup()->neurons[ctx->setup->getByteCodeParserSetup()->libraryNeuron];
                for (auto i = 0; i < o.call->arguments.size(); ++i) {
                    auto fti = findTemplateIndexes(ranges, o.call->arguments[i]);
                    if (fti.empty()) {
                        continue;
                    }
                    
                    auto oldarg = o.call->arguments[i];
                    if (oldarg.operand.expression) {
                        expandOperand(o.call->arguments[i].operand, ranges, orderedRange, ctx, conditions, fixedState);
                    }
                    else {
                        auto type = neuron->argumentType(i, (int64_t) o.call->arguments.size());
                        if (type == ParsedNeuronArgumentType::FLAT_CONDITIONAL || type == ParsedNeuronArgumentType::FLAT || type == ParsedNeuronArgumentType::TREE) {
                            expandOperand(o.call->arguments[i].operand, ranges, orderedRange, ctx, conditions, fixedState);
                        }
                        else {
                            auto p = o.call->arguments.begin();
                            std::advance(p, i);
                            o.call->arguments.erase(p);
                            --i;
                            
                            auto g = buildGenerator(ranges, orderedRange, buildRanges(ranges), fti, conditions, fixedState);
                            for (auto s = g->begin(); s != g->end(); ++s) {
                                auto newarg = LanguageModel::fullCopy(oldarg);
                                setTemplateIndexes(newarg, *s, std::set<icu::UnicodeString>());
                                auto newarg_s = LanguageModel::toString(newarg);
                                
                                ++i;
                                p = o.call->arguments.begin();
                                std::advance(p, i);
                                o.call->arguments.insert(p, newarg);
                            }
                        }
                    }
                }
            }
        }
        
        int8_t Language::compileName(LanguageBlock& block, icu::UnicodeString prefix, icu::UnicodeString& res)
        {
            res = prefix + USS(".") + block.name;
            
            // check if the block is a permutation and prepare all ranges
            if (!block.header.templates.empty()) {
                // check if the block is a valid permutation
                for (auto & i : block.header.templates) {
                    if (i.condition) {
                        continue;
                    }
                    if (!i.rangeStart || !i.rangeEnd) {
                        continue;
                    }
                    if (!i.rangeStart->literal || !i.rangeStart->literal->isOf(TokenKind::NUMBER)) {
                        return 0;
                    }
                    if (!i.rangeEnd->literal || !i.rangeEnd->literal->isOf(TokenKind::NUMBER)) {
                        return 0;
                    }

                    res += StringUtils::spf(USS("_{0}_{1}_{2}"), StringConvUtils::utow(i.name), StringConvUtils::utow(i.rangeStart->literal->value), StringConvUtils::utow(i.rangeEnd->literal->value));
                }
            }
            return 1;
        }

        int8_t Language::compileRanges(LanguageBlock& block, icu::UnicodeString prefix, std::map<icu::UnicodeString, double>& context)
        {
            // check if the block is a permutation and prepare all ranges
            if (!block.header.templates.empty()) {
                // check if the block is a valid permutation
                for (auto i = block.header.templates.begin(); i != block.header.templates.end(); ++i) {
                    if (i->condition) {
                        continue;
                    }
                    if (!i->rangeStart || !i->rangeEnd) {
                        continue;
                    }
                    
                    LanguageCall tmp(block.loader);
                    LanguageArgument a(block.loader);
                    a.operand = *i->rangeStart;
                    tmp.arguments.push_back(a);
                    LanguageArgument a2(block.loader);
                    a2.operand = *i->rangeEnd;
                    tmp.arguments.push_back(a2);
                    applyToTree(tmp, [&context, &prefix](Token<TokenKind>& t) {
                        auto c = context.find(t.value);
                        if (c == context.end()) {
                            c = context.find(prefix + USS(".") + t.value);
                        }
                        if (c != context.end()) {
                            std::cout << StringConvUtils::utou8(c->first) << "=" << c->second << std::endl;
                            t.value = ByteCodeUtils::asString(c->second);
                            t.type.clear();
                            t.type.push_back(TokenKind::NUMBER);
                            t.type.push_back(TokenKind::LITERAL);
                        }
                    });
                    
                    if (!collapseExpression(*i->rangeStart)) {
                        return 0;
                    }
                    if (!collapseExpression(*i->rangeEnd)) {
                        return 0;
                    }
                }
            }
            return 1;
        }
        
        int8_t Language::validateCode(std::shared_ptr<ByteCode> code)
        {
            auto r = code->getBlocks();
            for (auto i = std::get<0>(r); i != std::get<1>(r); ++i) {
                auto b = *i;
                auto bi = b->getItems();
                for (auto k = std::get<0>(bi); k != std::get<1>(bi); ++k) {
                    auto tb = (*k)->getOwner().lock();
                    if (!tb || tb != b) {
                        return 0;
                    }
                    if (code->getBlock((*k)->getName()) == std::get<1>(r)) {
                        return 0;
                    }
                }
                bi = b->getIncomes();
                for (auto k = std::get<0>(bi); k != std::get<1>(bi); ++k) {
                    auto tb = (*k)->getOwner().lock();
                    if (code->getBlock((*k)->getName()) == std::get<1>(r) || (*k)->getName() != b->getName()) {
                        return 0;
                    }
                    if (!tb || code->getBlock(tb->getName()) == std::get<1>(r) || *code->getBlock(tb->getName()) != tb) {
                        return 0;
                    }
                }
            }
            return 1;
        }
        
        int8_t Language::compileBlock(LanguageBlock& block, icu::UnicodeString prefix, std::map<icu::UnicodeString, double>& context, std::shared_ptr<ByteCode>& res, std::map<icu::UnicodeString, std::shared_ptr<ParsedNeuron> >& neurons, std::shared_ptr<Context> ctx)
        {
            // N = expected size of block graph, M = expected number of edges between nodes

            LanguageBlock tempBlock = LanguageModel::fullCopy(block);
            icu::UnicodeString fullName;
            if (!compileRanges(tempBlock, prefix, context) || !compileName(tempBlock, prefix, fullName)) {
                ctx->setup->getLogger()->debug(USS("Block header is not compilable due to inconclusive constant setup: ") + block.name);
                res = nullptr;
                return 1;
            }

            std::map<icu::UnicodeString, std::tuple<int64_t, int64_t> > ranges;
            std::list<Operand> conditions;
            
            // check if the block is a permutation and prepare all ranges
            if (!tempBlock.header.templates.empty()) {
                // check if the block is a valid permutation
                for (auto & i : tempBlock.header.templates) {
                    if (i.condition) {
                        conditions.push_back(LanguageModel::fullCopy(*i.condition));
                        continue;
                    }
                    if (!i.rangeStart || !i.rangeEnd) {
                        continue;
                    }
                    
                    ranges[i.name] = std::tuple<int64_t, int64_t>(
                            (int64_t)ByteCodeUtils::asNumber(i.rangeStart->literal->value),
                            (int64_t)ByteCodeUtils::asNumber(i.rangeEnd->literal->value));
                }
            }
            
            // check if the block is already compiled
            if (ctx->compiled->find(fullName) != ctx->compiled->end()) {
                ctx->setup->getLogger()->debug(USS("Reusing cached block: ") + block.name);
                res = (*ctx->compiled)[fullName];
                return 1;
            }

            // preprocess all definition literals for AS_NUMBER kind O(N^2 * M) and for constants
            for (auto & definition : tempBlock.definitions) {
                if (definition.name) {
                    auto any = 0;
                    auto np = NameParts::parseName(*definition.name);
                    // find mutation index
                    for (auto k = np->indexes.begin(); k != np->indexes.end(); ++k) {
                        auto vk = context.find((*k)->toString());
                        if (vk != context.end()) {
                            any = 1;
                            (*k)->name = ByteCodeUtils::asString(vk->second);
                            (*k)->indexes.clear();
                            (*k)->subname = nullptr;
                        }
                    }
                    if (any) {
                        definition.name = std::make_shared<icu::UnicodeString>(np->toString());
                    }
                }

                markAsNumberConstant(definition.operand, definition.templateNames, ranges, context, ctx);
            }

            // generate new inputs and definitions
            if (!tempBlock.header.templates.empty()) {
                std::list<icu::UnicodeString> orderedRange;
                for (auto & range : ranges) {
                    orderedRange.push_back(range.first);
                }
                orderedRange.emplace_back(USS(""));
                
                // each original input
                std::deque<icu::UnicodeString> inputs;
                for (auto & input : tempBlock.header.inputs) {
                    auto g = buildGenerator(ranges, orderedRange, buildRanges(ranges), findNameIndexes(ranges, input), conditions, std::map<icu::UnicodeString, int64_t>());

                    int8_t any = 1;
                    if (g->begin() == g->end()) {
                        inputs.push_back(input);
                    }
                    // each valid permutation
                    for (auto s = g->begin(); s != g->end() && any; ++s) {
                        any = 0;
                        auto inp = input;
                        auto np = NameParts::parseName(inp);
                        // find mutation index
                        for (auto k = np->indexes.begin(); k != np->indexes.end(); ++k) {
                            auto vk = s->find((*k)->toString());
                            if (vk != s->end()) {
                                any = 1;
                                (*k)->name = ByteCodeUtils::asString(vk->second);
                                (*k)->indexes.clear();
                                (*k)->subname = nullptr;
                            }
                        }
                        if (any) {
                            inp = np->toString();
                        }
                        inputs.push_back(inp);
                    }
                }
                tempBlock.header.inputs = inputs;

                ctx->setup->getLogger()->debug(USS("Inputs are static generated: ") + block.name);
                
                // each original definition
                std::deque<LanguageDefinition> definitions;
                for (auto & definition : tempBlock.definitions) {
                    auto validParts = findNameIndexes(ranges, definition);
                    auto parts = findTemplateIndexes(ranges, definition);
                    std::set<icu::UnicodeString> reductionParts;
                    if (definition.reduction) {
                        // possible only for "A =+ call" case
                        std::set_difference(parts.begin(), parts.end(), validParts.begin(), validParts.end(), std::inserter(reductionParts, reductionParts.end()));
                    }
                    
                    if (!reductionParts.empty()) {
                        // for reduction generate new name and use old name in aggregator
                        auto aggName = *definition.name;
                        auto newName = aggName;
                        for (const auto & reductionPart : reductionParts) {
                            newName += USS("_§") + reductionPart + USS("[") + reductionPart + USS("]");
                        }
                        definition.name = std::make_shared<icu::UnicodeString>(newName);
                        definition.reduction = 0;
                        LanguageDefinition agg(ctx->byteLoader);
                        agg.name = std::make_shared<icu::UnicodeString>(aggName);
                        agg.templateNames = definition.templateNames;
                        Token<TokenKind> t(ctx->byteLoader);
                        t.value = newName;
                        t.type.push_back(TokenKind::LITERAL);
                        agg.operand.literal = std::make_shared<Token<TokenKind>>(t);
                        definitions.push_back(agg);
                        
                        validParts = findNameIndexes(ranges, newName);
                        parts = findTemplateIndexes(ranges, definition);
                    }
                    
                    int8_t any = 1;
                    auto g = buildGenerator(ranges, orderedRange, buildRanges(ranges), validParts, conditions, std::map<icu::UnicodeString, int64_t>());
                    if (g->begin() == g->end()) {
                        definitions.push_back(definition);
                    }
                    // each valid permutation
                    for (auto s = g->begin(); s != g->end() && any; ++s) {
                        any = 0;
                        auto inp = USS("");
                        if (definition.name) {
                            inp = *definition.name;
                            auto np = NameParts::parseName(inp);
                            // find mutation index
                            for (auto k = np->indexes.begin(); k != np->indexes.end(); ++k) {
                                auto vk = s->find((*k)->toString());
                                if (vk != s->end()) {
                                    any = 1;
                                    (*k)->name = ByteCodeUtils::asString(vk->second);
                                    (*k)->indexes.clear();
                                    (*k)->subname = nullptr;
                                }
                            }
                            if (any) {
                                inp = np->toString();
                            }
                        }
                        // duplicate definitions
                        if (any) {
                            auto nd = LanguageModel::fullCopy(definition);
                            for (auto & k : *s) {
                                if (validParts.find(k.first) != validParts.end()) {
                                    nd.fixedIndexState[k.first] = k.second;
                                }
                            }
                            nd.name = std::make_shared<icu::UnicodeString>(inp);
                            setTemplateIndexes(nd, *s, validParts);
                            definitions.push_back(nd);
                        }
                        // in one definition duplicate operands and arguments on second run
                        else {
                            definitions.push_back(definition);
                        }
                    }
                }
                // rerun permutation for internal operands and arguments
                for (auto & definition : definitions) {
                    auto parts = findTemplateIndexes(ranges, definition);
                    // only if at all template
                    if (!parts.empty()) {
                        expandArguments(definition.operand, ranges, orderedRange, ctx, conditions, definition.fixedIndexState);
                    }
                }
                tempBlock.definitions = definitions;

                ctx->setup->getLogger()->debug(USS("Definitions are static generated: ") + block.name);
            }
            
            res = std::make_shared<ByteCode>(fullName, ctx->byteLoader);
            std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock> > resBlocks;
            std::vector<std::tuple<std::shared_ptr<ParsedNeuron>, std::vector<std::shared_ptr<ByteCodeBlock> > > > blocks;
            
            // add input definition, O(N)
            if (!tempBlock.header.inputs.empty()) {
                LanguageDefinition d(ctx->byteLoader);
                d.operand.call = std::make_shared<LanguageCall>(ctx->byteLoader);
                d.operand.call->blockName = NeuronNames::INPOS;
                for (auto & input : tempBlock.header.inputs) {
                    LanguageArgument a(ctx->byteLoader);
                    a.operand.literal = std::make_shared<Token<TokenKind> >(ctx->byteLoader);
                    a.operand.literal->type.push_back(TokenKind::LITERAL);
                    a.operand.literal->value = input;
                    d.operand.call->arguments.push_back(a);
                }
                tempBlock.definitions.push_back(d);
            }

            std::map<icu::UnicodeString, icu::UnicodeString> remapOutputs;
            
            CompileContext cctx { prefix, &context, &remapOutputs, tempBlock.definitions };
            
            // transform each definition into nodes O(2 * N^2 * M + N^3)
            for (auto i = cctx.definitions.begin(); i != cctx.definitions.end(); ++i) {
                if (!i->operand.call) {
                    LanguageArgument a(ctx->byteLoader);
                    a.operand = i->operand;
                    i->operand.call = std::make_shared<LanguageCall>(ctx->byteLoader);
                    i->operand.call->blockName = EXPRESSION_BLOCK;
                    i->operand.call->arguments.push_back(a);
                    i->operand.literal = nullptr;
                    i->operand.expression = nullptr;
                }
                auto neuron = ctx->getNeuron(i->operand.call->blockName);
                if (neuron == neurons.end()) {
                    return 0;
                }

                // let each neuron compile
                auto name = i->name ? *i->name : StringUtils::spf(USS("noresult_{0}"), ctx->getNewIndex());
                ctx->setup->getLogger()->debug(USS("Compiling ") + name + USS(" as ") + i->operand.call->blockName);
                auto bs = neuron->second->compile(name, *i->operand.call, cctx, ctx);
                std::vector<std::shared_ptr<ByteCodeBlock>> nbs = ByteCodeBlock::fullCopy(bs);
                
                // wire template sources O(N^2 * M + N^3)
                for (auto & templateName : i->templateNames) {
                    // from index node with generator
                    auto n = std::make_shared<ByteCodeBlock>(templateName, USS(""));
                    n->setParameter(std::make_shared<ByteCodeParameter>(ElementParams::EXPECTED, (int8_t)1));
                    nbs.push_back(n);
                    // check all nodes on using this index O(N * M + N^2)
                    for (auto & nb : nbs) {
                        // check if node has templated index
                        auto gn = NameParts::parseName(nb->getName());
                        std::shared_ptr<NameParts> fin;
                        // O(N)
                        for (auto gni = gn->indexes.begin(); gni != gn->indexes.end(); ++gni) {
                            if ((*gni)->name == templateName) {
                                fin = *gni;
                            }
                        }
                        if (!fin) {
                            continue;
                        }

                        nb->setParameter(std::make_shared<ByteCodeParameter>(ElementParams::TEMPLATE, (int8_t)1));
                        
                        // setup generator link from index node
                        auto gl = std::make_shared<ByteCodeBlockItem>(nb->getName(), n);
                        gl->setParameter(std::make_shared<ByteCodeParameter>(LinkParams::GENERATION, (int8_t)1));

                        auto r = nb->getIncomes();
                        // O(M)
                        auto p = std::get<0>(r);
                        for (; p != std::get<1>(r); ++p) {
                            if ((*p)->getOwner().lock()->getName() == gl->getOwner().lock()->getName()) {
                                break;
                            }
                        }
                        if (p == std::get<1>(r)) {
                            nb->addIncome(gl);
                        }
                        else {
                            mergeParameterAny(*p, gl, LinkParams::GENERATION);
                        }
                    }
                }

                blocks.emplace_back(neuron->second, nbs);
            }

            ctx->setup->getLogger()->debug(USS("Compiled all definitions: ") + block.name);

            // merge nodes into single block O(2 * N^2 * log(N) + 2 * N^3 * M * log(M) + N^3 * M^2)
            for (auto & block : blocks) {
                auto neuron = std::get<0>(block);
                auto bs = std::get<1>(block);
                
                std::map<std::shared_ptr<ByteCodeBlock>, std::shared_ptr<ByteCodeBlock> > pointerRewrite;
                // add each compiled node to result, O(2 * N * log(N))
                for (auto & k : bs) {
                    auto b = k;
                    if (remapOutputs.find(b->getName()) != remapOutputs.end()) {
                        b = b->isolatedCopy(remapOutputs[b->getName()]);
                    }
                    
                    auto i = resBlocks.find(b->getName());
                    if (i == resBlocks.end()) {
                        resBlocks[b->getName()] = b;
                        if (b != k) {
                            pointerRewrite[k] = b;
                        }
                    }
                    else {
                        if (!mergeBlock(neuron, i->second, b)) {
                            return 0;
                        }
                        pointerRewrite[k] = i->second;
                    }
                }
                
                // reassign pointers from extra "expected" blocks to actually existing, O(N^2 * 2 * M * log(M))
                for (auto k = pointerRewrite.begin(); k != pointerRewrite.end(); ++k) {
                    for (auto & resBlock : resBlocks) {
                        auto r = resBlock.second->getIncomes();
                        for (auto income = std::get<0>(r); income != std::get<1>(r); ++income) {
                            auto c = pointerRewrite.find((*income)->getOwner().lock());
                            if (c != pointerRewrite.end()) {
                                (*income)->changeOwner(c->second);
                            }
                        }
                        r = resBlock.second->getItems();
                        for (auto item = std::get<0>(r); item != std::get<1>(r); ++item) {
                            auto c = pointerRewrite.find((*item)->getOwner().lock());
                            if (c != pointerRewrite.end()) {
                                (*item)->changeOwner(c->second);
                            }
                        }
                    }
                }
                
                // restore incomes as items, O(N^2 * M^2)
                for (auto & k : bs) {
                    auto r = k->getIncomes();
                    for (auto in = std::get<0>(r); in != std::get<1>(r); ++in) {
                        std::shared_ptr<ByteCodeBlock> b;
                        auto o = resBlocks.find((*in)->getOwner().lock()->getName());
                        if (o == resBlocks.end()) {
                            b = std::make_shared<ByteCodeBlock>((*in)->getOwner().lock()->getName(), USS(""));
                            b->setParameter(std::make_shared<ByteCodeParameter>(ElementParams::EXPECTED, (int8_t)1));
                            resBlocks[(*in)->getOwner().lock()->getName()] = b;
                        }
                        else {
                            b = o->second;
                        }
                        auto r2 = b->getItems();
                        if (!ByteCodeUtils::isInItems(*in, std::get<0>(r2), std::get<1>(r2))) {
                            (*in)->changeOwner(b);
                            b->addItem(*in);
                        }
                    }
                }
            }

            ctx->setup->getLogger()->debug(USS("Fixed all links for definitions: ") + block.name);

            // O(N)
            for (auto & resBlock : resBlocks) {
                if (resBlock.second->getType() == USS("")) {
                    resBlock.second->changeType(NeuronNames::ADDITIVE);
                }
                
                res->addBlock(resBlock.second);
            }

            if (!validateCode(res)) {
                return 0;
            }

            return 1;
        }

        int8_t Language::compileTree(std::shared_ptr<LanguageTree> tree, std::shared_ptr<std::vector<std::shared_ptr<ByteCode> > >& result, icu::UnicodeString prefix, std::map<icu::UnicodeString, std::shared_ptr<ParsedNeuron> >& neurons, std::shared_ptr<Context> ctx)
        {
            ctx->setup->getLogger()->debug(USS("Starting compilation of AST"));

            std::map<icu::UnicodeString, double> context;
            // user overrides
            for (auto & i : tree->consts) {
                context[prefix + USS(".") + i.name] = i.value;
            }
            for (auto & block : tree->blocks) {
                ctx->prepared->insert({prefix + USS(".") + block.name, block});
            }

            std::vector<icu::UnicodeString> existing;
            for (auto & i : *ctx->compiled) {
                existing.push_back(i.first);
            }
            
            for (auto & block : tree->blocks) {
                ctx->compileStarted->insert(prefix + USS(".") + block.name);
                std::shared_ptr<ByteCode> b = nullptr;
                if (!compileBlock(block, prefix, context, b, neurons, ctx)) {
                    return 0;
                }
                if (b) {
                    (*ctx->compiled)[prefix + USS(".") + block.name] = b;
                }
            }
            result = std::make_shared<std::vector<std::shared_ptr<ByteCode> > >();
            for (auto & i : *ctx->compiled) {
                if (std::find(existing.begin(), existing.end(), i.first) == existing.end()) {
                    result->push_back(i.second);
                }
            }

            ctx->setup->getLogger()->debug(USS("Finalizing compilation of AST"));

            return cleanupByteCode(result);
        }
        
        int8_t Language::cleanupByteCode(std::shared_ptr<std::vector<std::shared_ptr<ByteCode> > > result)
        {
            for (auto & i : *result) {
                auto b = i->getBlocks();
                for (auto k = std::get<0>(b); k != std::get<1>(b); ++k) {
                    (*k)->compact();
                }
            }
            return 1;
        }
        
        icu::UnicodeString Language::cleanName(icu::UnicodeString s)
        {
            return s.findAndReplace(USS("-"), USS("_")).findAndReplace(USS("'"), USS(""));
        }
        
        auto pass = ByteState::Readers::pass().next;
        auto passWhiteSpaces = ByteCodeState::passWhiteSpaces().next;
        auto readByte = ByteCodeState::readByte().next;
        auto peekByte = ByteCodeState::peekByte().next;
        auto alwaysTrue = ByteState::Matches::alwaysTrue().next;
        auto isWSpace = ByteCodeState::isWSpace().next;

        int8_t Language::isBlockName(const unsigned char c)
        {
            return (c >= '0' && c <= '9') || (c >= 'A' && c <='Z') || (c >= 'a' && c <='z') || c == '_';
        }

        int8_t Language::isItemName(const unsigned char c)
        {
            return isBlockName(c) || c == '.' || c == '#' || c >= 127;
        }
        
        int8_t Language::isDefinitionLine(const unsigned char c)
        {
            return c != '\r' && c != '\n' && c != ';' && c != '}';
        }
        
        int8_t Language::isExpressionLine(const unsigned char c)
        {
            return c != ':' && c != ',' && c != ']' && isDefinitionLine(c);
        }
        
        int8_t Language::isNumber(const unsigned char c, std::vector<uint8_t>& read)
        {
            if (c == '-' && read.empty()) {
                return 1;
            }
            if (c == '.' && (read.empty() || std::find(read.begin(), read.end(), '.') != read.end())) {
                return 0;
            }
            return (c >= '0' && c <= '9') || c == '.';
        }

        int8_t Language::isUnicodeItemName(icu::UnicodeString& u)
        {
            auto isBlock = true;
            for (auto i = 0; i < u.countChar32(); ++i) {
                isBlock &= (u.char32At(i) >= '0' && u.char32At(i) <= '9')
                || (u.char32At(i) >= 'A' && u.char32At(i) <='Z')
                || (u.char32At(i) >= 'a' && u.char32At(i) <='z')
                || u.char32At(i) == '_' || u.char32At(i) == '.'
                || u.char32At(i) == '#' || u.char32At(i) >= 127
                || u.char32At(i) == '[' || u.char32At(i) == ']';
            }
            return (int8_t) (isBlock ? 1 : 0);
        }
        
        int8_t Language::isUnicodeNumber(icu::UnicodeString& u)
        {
            auto isBlock = true;
            auto dotted = false;
            for (auto i = 0; i < u.countChar32(); ++i) {
                if (u.char32At(i) == '.') {
                    if (!dotted) {
                        dotted = true;
                    }
                    else {
                        isBlock = false;
                    }
                }
                if (u.char32At(i) == '-' && i != 0) {
                    isBlock = false;
                }
                isBlock &= (u.char32At(i) >= '0' && u.char32At(i) <= '9');
            }
            return (int8_t) (isBlock ? 1 : 0);
        }
        
        ByteState::stateFiller fillConstName =
        [](std::vector<uint8_t>& data, int64_t& size, std::shared_ptr<Carried>& carried, std::shared_ptr<std::vector<std::shared_ptr<ByteCode>>>& result) -> int8_t
        {
            if (size == 1 && data[0] == ']') {
                return 1;
            }
            carried->string = StringConvUtils::u8tou(std::string(data.begin(), data.end()));
            return 1;
        };
        
        ByteState::stateFiller fillConst =
        [](std::vector<uint8_t>& data, int64_t& size, std::shared_ptr<Carried>& carried, std::shared_ptr<std::vector<std::shared_ptr<ByteCode>>>& result) -> int8_t
        {
            LanguageConst l(carried->ctx->byteLoader);
            l.name = carried->string;
            l.value = ByteCodeUtils::asNumber(StringConvUtils::u8tou(std::string(data.begin(), data.end())));
            carried->string = USS("");
            carried->tree->consts.push_back(l);
            return 1;
        };
        
        ByteState::stateFiller fillString =
        [](std::vector<uint8_t>& data, int64_t& size, std::shared_ptr<Carried>& carried, std::shared_ptr<std::vector<std::shared_ptr<ByteCode>>>& result) -> int8_t
        {
            carried->string += StringConvUtils::u8tou(std::string(data.begin(), data.end()));
            return 1;
        };
        
        ByteState::stateFiller fillBlock =
        [](std::vector<uint8_t>& data, int64_t& size, std::shared_ptr<Carried>& carried, std::shared_ptr<std::vector<std::shared_ptr<ByteCode>>>& result) -> int8_t
        {
            LanguageBlock l(carried->ctx->byteLoader);
            l.name = carried->string;
            carried->string = USS("");
            carried->tree->blocks.push_back(l);
            return 1;
        };
        
        ByteState::stateFiller fillHeaderTemplateSingle =
        [](std::vector<uint8_t>& data, int64_t& size, std::shared_ptr<Carried>& carried, std::shared_ptr<std::vector<std::shared_ptr<ByteCode>>>& result) -> int8_t
        {
            if (carried && carried->string.length()) {
                LanguageTemplate t(carried->ctx->byteLoader);
                t.name = carried->string;
                carried->string = data[0] == '=' ||  data[0] == '!' ||  data[0] == '<' ||  data[0] == '>' ? StringConvUtils::u8tou(std::string(data.begin(), data.end())) : USS("");
                carried->tree->blocks.back().header.templates.push_back(t);
            }
            return 1;
        };
        
        ByteState::stateFiller keepString =
        [](std::vector<uint8_t>& data, int64_t& size, std::shared_ptr<Carried>& carried, std::shared_ptr<std::vector<std::shared_ptr<ByteCode>>>& result) -> int8_t
        {
            if (carried) {
                if (carried->string.length()) {
                    carried->string.insert(carried->string.length(), (UChar)0);
                }
                carried->string += StringConvUtils::u8tou(std::string(data.begin(), data.end()));
            }
            return 1;
        };
        
        ByteState::stateFiller dropString =
        [](std::vector<uint8_t>& data, int64_t& size, std::shared_ptr<Carried>& carried, std::shared_ptr<std::vector<std::shared_ptr<ByteCode>>>& result) -> int8_t
        {
            if (carried) {
                carried->string = USS("");
            }
            return 1;
        };
        
        ByteState::stateFiller fillMainArgument =
        [](std::vector<uint8_t>& data, int64_t& size, std::shared_ptr<Carried>& carried, std::shared_ptr<std::vector<std::shared_ptr<ByteCode>>>& result) -> int8_t
        {
            if (carried) {
                carried->tree->blocks.back().header.inputs.push_back(StringConvUtils::u8tou(std::string(data.begin(), data.end())));
            }
            return 1;
        };
        
        ByteState::stateFiller fillHeaderTemplateRangeStart =
        [](std::vector<uint8_t>& data, int64_t& size, std::shared_ptr<Carried>& carried, std::shared_ptr<std::vector<std::shared_ptr<ByteCode>>>& result) -> int8_t
        {
            if (carried) {
                if (data[0] == ':') {
                    auto d = std::make_shared<Operand>(carried->ctx->byteLoader);
                    std::map<icu::UnicodeString, std::tuple<int64_t, int64_t>> ranges;
                    if (!DefinitionBuilder(carried->ctx).parseExpression(carried->string, *d, ranges)) {
                        return 0;
                    }
                    carried->tree->blocks.back().header.templates.back().rangeStart = d;
                }
                else {
                    if (!Language::isUnicodeItemName(carried->string)) {
                        return 0;
                    }
                    carried->tree->blocks.back().header.templates.back().rename = std::make_shared<icu::UnicodeString>(carried->string);
                }
                carried->string = USS("");
            }
            return 1;
        };
        
        ByteState::stateFiller fillHeaderTemplateRangeEnd =
        [](std::vector<uint8_t>& data, int64_t& size, std::shared_ptr<Carried>& carried, std::shared_ptr<std::vector<std::shared_ptr<ByteCode>>>& result) -> int8_t
        {
            if (carried) {
                auto d = std::make_shared<Operand>(carried->ctx->byteLoader);
                auto s = StringConvUtils::u8tou(std::string(data.begin(), data.end()));
                std::map<icu::UnicodeString, std::tuple<int64_t, int64_t>> ranges;
                if (!DefinitionBuilder(carried->ctx).parseExpression(s, *d, ranges)) {
                    return 0;
                }
                carried->tree->blocks.back().header.templates.back().rangeEnd = d;
            }
            return 1;
        };
        
        ByteState::stateFiller fillHeaderTemplateCondition =
        [](std::vector<uint8_t>& data, int64_t& size, std::shared_ptr<Carried>& carried, std::shared_ptr<std::vector<std::shared_ptr<ByteCode>>>& result) -> int8_t
        {
            if (carried) {
                auto n = carried->tree->blocks.back().header.templates.back().name;
                auto c = std::make_shared<Operand>(carried->ctx->byteLoader);
                c->expression = std::make_shared<ExpressionTree>(carried->ctx->byteLoader);
                auto d = std::make_shared<Operand>(carried->ctx->byteLoader);
                auto s = StringConvUtils::u8tou(std::string(data.begin(), data.end()));
                std::map<icu::UnicodeString, std::tuple<int64_t, int64_t>> ranges;
                if (!DefinitionBuilder(carried->ctx).parseExpression(s, *d, ranges)) {
                    return 0;
                }
                
                c->expression->left = std::make_shared<Operand>(carried->ctx->byteLoader);
                c->expression->left->literal = std::make_shared<Token<TokenKind>>(carried->ctx->byteLoader);
                c->expression->left->literal->value = n;
                c->expression->left->literal->type.push_back(TokenKind::LITERAL);
                
                c->expression->middle = carried->string;
                
                c->expression->right = d;
                
                carried->tree->blocks.back().header.templates.back().condition = c;
                carried->string = USS("");
            }
            return 1;
        };
        
        ByteState::stateFiller setDefinitionTree =
        [](std::vector<uint8_t>& data, int64_t& size, std::shared_ptr<Carried>& carried, std::shared_ptr<std::vector<std::shared_ptr<ByteCode>>>& result) -> int8_t
        {
            LanguageDefinition d(carried->ctx->byteLoader);
            if (carried && carried->string.length()) {
                UErrorCode status = U_ZERO_ERROR;
                icu::RegexMatcher m(USS("\\x00"), 0, status);
                const int maxWords = 1024; // pay attention to such implementation specific limitations
                icu::UnicodeString words[maxWords];
                int numWords = m.split(carried->string, words, maxWords, status);
                for (auto i = 0; i < numWords; ++i) {
                    d.templateNames.push_back(words[i]);
                }
                carried->string = USS("");
            }
            if (!size) {
                return 1;
            }
            auto definition = StringConvUtils::u8tou(std::string(data.begin(), data.end()));
            auto cb = &carried->tree->blocks.back();
            std::map<icu::UnicodeString, std::tuple<int64_t, int64_t>> ranges;
            for (auto & i : cb->header.templates) {
                if (!i.rangeStart || !i.rangeEnd) {
                    continue;
                }
                ranges[i.name] = std::tuple<int64_t, int64_t>(0, 0);
            }
            if (!DefinitionBuilder(carried->ctx).parseDefinition(definition, d, cb->definitions, ranges)) {
                std::cout << "Failed: " << StringConvUtils::utou8(definition) << std::endl;
                return 0;
            }
            
            cb->definitions.push_back(d);
            return 1;
        };
        
        auto languageReader = [](std::function<int8_t(const unsigned char, bool)> logic, std::shared_ptr<std::istream> dataSource) -> int64_t
        {
            auto inString = false;
            auto inLineComment = false;
            auto inMultiComment = false;
            while (dataSource->good()) {
                auto c = (unsigned char) dataSource->peek();
                if (dataSource->good()) {
                    if (inLineComment) {
                        c = (unsigned char) dataSource->get();
                        if (c == '"' || c == '\r' || c == '\n') {
                            inLineComment = false;
                        }
                        continue;
                    }
                    if (inMultiComment) {
                        c = (unsigned char) dataSource->get();
                        if (c == '"') {
                            c = (unsigned char) dataSource->peek();
                            if (dataSource->good() && c == '"') {
                                c = (unsigned char) dataSource->get();
                                c = (unsigned char) dataSource->peek();
                                if (dataSource->good() && c == '"') {
                                    c = (unsigned char) dataSource->get();
                                    inMultiComment = false;
                                }
                            }
                        }
                        continue;
                    }
                    if (inString) {
                        c = (unsigned char) dataSource->get();
                        if (c == '\'') {
                            inString = false;
                        }
                        if (!logic(c, inString)) {
                            break;
                        }
                        continue;
                    }
                    if (c == '"') {
                        c = (unsigned char) dataSource->get();
                        c = (unsigned char) dataSource->peek();
                        if (dataSource->good() && c == '"') {
                            c = (unsigned char) dataSource->get();
                            c = (unsigned char) dataSource->peek();
                            if (dataSource->good() && c == '"') {
                                c = (unsigned char) dataSource->get();
                                inMultiComment = true;
                            }
                            else {
                                inLineComment = false;
                            }
                        }
                        else {
                            inLineComment = true;
                        }
                        continue;
                    }
                    if (c == '\'') {
                        inString = true;
                        continue;
                    }
                    if (!logic(c, inString)) {
                        break;
                    }
                }
            }
            if (dataSource->eof() || !dataSource->fail()) {
                return 1;
            }
            return -1;
        };
        
        ByteState::stateReader passWhiteSpacesAndComments =
        [](std::shared_ptr<std::istream> dataSource, std::vector<uint8_t>& result) -> int64_t
        {
            result.clear();
            auto res = languageReader(
                [&result, &dataSource](const unsigned char c, bool isStr)
                {
                    if (isStr) {
                        return 0;
                    }
                    std::vector<uint8_t> w{c};
                    if (isWSpace(w, 1) > 0) {
                        dataSource->get();
                    }
                    else {
                        result.push_back(c);
                        return 0;
                    }
                    return 1;
                }, dataSource);
            if (res < 0) {
                return res;
            }
            return (int64_t) result.size();
        };
        
        auto readBracketedLanguageName = [](std::function<int8_t(const unsigned char)> checkName, const unsigned char delimiter, const char nestStart, const char nestEnd, std::shared_ptr<std::istream> dataSource, std::vector<uint8_t>& result, int8_t& numberMark) -> int64_t
        {
            auto depth = 0;
            numberMark = 1;
            result.clear();
            auto res = languageReader(
                [&result, &dataSource, &numberMark, &depth, &nestStart, &nestEnd, &delimiter, &checkName](const unsigned char c, bool isStr)
                {
                    if (isStr) {
                        result.push_back(c);
                        return 1;
                    }
                    if (!Language::isNumber(c, result)) {
                        numberMark = 0;
                    }
                    if (nestStart) {
                        std::vector<uint8_t> w{c};
                        if (isWSpace(w, 1) > 0) {
                            if (depth) { result.push_back((uint8_t) dataSource->get()); } else { return 0; }
                        }
                        else {
                            if (c == delimiter) {
                                if (depth) { result.push_back((uint8_t) dataSource->get()); } else { return 0; }
                            }
                            else {
                                if (c == nestEnd) {
                                    if (depth) { depth--; result.push_back((uint8_t) dataSource->get()); } else { return 0; }
                                }
                                else {
                                    if (c == nestStart) {
                                        depth++; result.push_back((uint8_t) dataSource->get());
                                    }
                                    else {
                                        if (checkName(c)) { result.push_back((uint8_t) dataSource->get()); } else { return 0; }
                                    }
                                }
                            }
                        }
                    }
                    else {
                        if (checkName(c)) { result.push_back((uint8_t) dataSource->get()); } else { return 0; }
                    }
                    return 1;
                }, dataSource);
            if (result.empty()) {
                numberMark = 0;
            }
            if (res < 0) {
                return res;
            }
            return (int64_t) result.size();
        };
        auto readLanguageName = [](std::function<int8_t(const unsigned char)> checkName, std::shared_ptr<std::istream> dataSource, std::vector<uint8_t>& result, int8_t& numberMark) -> int64_t
        {
            return readBracketedLanguageName(std::move(checkName), 0, 0, 0, std::move(dataSource), result, numberMark);
        };
        ByteState::stateReader readItemName = [](std::shared_ptr<std::istream> dataSource, std::vector<uint8_t>& result) -> int64_t
        {
            int8_t number;
            auto res = readBracketedLanguageName(Language::isItemName, ',', '[', ']', std::move(dataSource), result, number);
            return number ? 0 : res;
        };
        ByteState::stateReader readBlockName = [](std::shared_ptr<std::istream> dataSource, std::vector<uint8_t>& result) -> int64_t
        {
            int8_t number;
            auto res = readLanguageName(Language::isBlockName, std::move(dataSource), result, number);
            return number ? 0 : res;
        };
        ByteState::stateReader readLiteral = [](std::shared_ptr<std::istream> dataSource, std::vector<uint8_t>& result) -> int64_t
        {
            int8_t number;
            return readLanguageName(Language::isItemName, std::move(dataSource), result, number);
        };
        ByteState::stateReader readNumber = [](std::shared_ptr<std::istream> dataSource, std::vector<uint8_t>& result) -> int64_t
        {
            result.clear();
            double res;
            (*dataSource) >> res;
            if (dataSource->fail()) {
                return -1;
            }
            std::ostringstream ss;
            ss.precision(std::numeric_limits<double>::digits10);
            ss << res;
            auto s = ss.str();
            result.assign(s.begin(), s.end());
            return (int64_t) s.size();
        };
        ByteState::stateReader readDefinitionTokens = [](std::shared_ptr<std::istream> dataSource, std::vector<uint8_t>& result) -> int64_t
        {
            int8_t number;
            auto res = readLanguageName(Language::isDefinitionLine, std::move(dataSource), result, number);
            return number ? 0 : res;
        };
        ByteState::stateReader readExpression = [](std::shared_ptr<std::istream> dataSource, std::vector<uint8_t>& result) -> int64_t
        {
            int8_t number;
            auto res = readLanguageName(Language::isExpressionLine, std::move(dataSource), result, number);
            return number ? 0 : res;
        };

        template <uint8_t c>
        using isAtom1 = ByteState::Matches::isAtom1<c>;
        template <uint8_t c, uint8_t c2>
        using isAtom2 = ByteState::Matches::isAtom2<c, c2>;
        template <uint8_t c, uint8_t c2, uint8_t c3>
        using isAtom3 = ByteState::Matches::isAtom3<c, c2, c3>;
        auto noop = ByteState::Fills().noop;
        template <int64_t default_step>
        using steps0 = ByteState::Steps::steps0<default_step>;
        using stepsInc = ByteState::Steps::stepsInc;
        template <uint8_t condition, int64_t step, int64_t default_step>
        using steps1 = ByteState::Steps::steps1<condition, step, default_step>;
        template <uint8_t condition, int64_t step, uint8_t condition2, int64_t step2, int64_t default_step, int8_t require_conditions>
        using steps2 = ByteState::Steps::steps2<condition, step, condition2, step2, default_step, require_conditions>;
        template <uint8_t condition, int64_t step, uint8_t condition2, int64_t step2, uint8_t condition3, int64_t step3, int64_t default_step, int8_t require_conditions>
        using steps3 = ByteState::Steps::steps3<condition, step, condition2, step2, condition3, step3, default_step, require_conditions>;
        template <uint8_t condition, int64_t step, uint8_t condition2, int64_t step2, uint8_t condition3, int64_t step3, uint8_t condition4, int64_t step4, int64_t default_step, int8_t require_conditions>
        using steps4 = ByteState::Steps::steps4<condition, step, condition2, step2, condition3, step3, condition4, step4, default_step, require_conditions>;
        template <uint8_t condition, int64_t step, uint8_t condition2, int64_t step2, uint8_t condition3, int64_t step3, uint8_t condition4, int64_t step4, uint8_t condition5, int64_t step5, int64_t default_step, int8_t require_conditions>
        using steps5 = ByteState::Steps::steps5<condition, step, condition2, step2, condition3, step3, condition4, step4, condition5, step5, default_step, require_conditions>;
        template <uint8_t condition, int64_t step, uint8_t condition2, int64_t step2, uint8_t condition3, int64_t step3, uint8_t condition4, int64_t step4, uint8_t condition5, int64_t step5, uint8_t condition6, int64_t step6, int64_t default_step, int8_t require_conditions>
        using steps6 = ByteState::Steps::steps6<condition, step, condition2, step2, condition3, step3, condition4, step4, condition5, step5, condition6, step6, default_step, require_conditions>;
        
        ByteState::stateFiller prepareTree =
        [](std::vector<uint8_t>& data, int64_t& size, std::shared_ptr<Carried>& carried, std::shared_ptr<std::vector<std::shared_ptr<ByteCode>>>& result) -> int8_t
        {
            if (carried->string.length() > 0) {
                data.push_back(StringConvUtils::utou8(carried->string.charAt(0)));
                size = 1;
            }
            if (carried->tree == nullptr) {
                carried->tree = std::make_shared<LanguageTree>(carried->ctx->byteLoader);
            }
            return 1;
        };

        ByteState::stateError languageError =
        [](std::vector<uint8_t>& data, int16_t state, int64_t symbol) -> icu::UnicodeString { return _i("err.parser.invalid.language"); };

        std::vector<ByteState> Language::states = {
            // [ \w
            { peekByte, alwaysTrue, prepareTree, steps6<'[', 2, '"', 1, ' ', 1, '\t', 1, '\n', 1, '\r', 1, 11, 0>().next, languageError, 1 }, //0
            { passWhiteSpacesAndComments, alwaysTrue, noop, steps1<'[', 2, 11>().next, languageError, 1 }, //1
            // const setup [VAR : 10, ...]
            { readByte, alwaysTrue, noop, stepsInc().next, languageError }, //2
            { passWhiteSpacesAndComments, alwaysTrue, noop, stepsInc().next, languageError }, //3
            { readItemName, alwaysTrue, fillConstName, stepsInc().next, languageError }, //4
            { passWhiteSpacesAndComments, alwaysTrue, noop, stepsInc().next, languageError }, //5
            { readByte, isAtom1<':'>().next, noop, stepsInc().next, languageError }, //6
            { passWhiteSpacesAndComments, alwaysTrue, noop, stepsInc().next, languageError }, //7
            { readNumber, alwaysTrue, fillConst, stepsInc().next, languageError }, //8
            { passWhiteSpacesAndComments, alwaysTrue, noop, stepsInc().next, languageError }, //9
            { readByte, alwaysTrue, noop, steps2<',', 3, ']', 1, 1, 1>().next, languageError, 1 }, //10
            // block setup
            { readBlockName, alwaysTrue, fillString, stepsInc().next, languageError }, //11
            { readByte, isWSpace, noop, stepsInc().next, languageError }, //12
            { passWhiteSpacesAndComments, alwaysTrue, fillBlock, steps2<'[', 14, '{', 35, 31, 0>().next, languageError }, //13
            // template arguments
            // [
            { readByte, alwaysTrue, noop, stepsInc().next, languageError }, //14
            { passWhiteSpacesAndComments, alwaysTrue, fillHeaderTemplateSingle, steps1<']', 31, 16>().next, languageError }, //15
            // A
            { readItemName, alwaysTrue, fillString, stepsInc().next, languageError }, //16
            { passWhiteSpacesAndComments, alwaysTrue, noop, stepsInc().next, languageError }, //17
            // = , ]
            { readByte, alwaysTrue, fillHeaderTemplateSingle, steps6<'=', 19, '!', 27, '<', 27, '>', 27, ',', 15, ']', 31, 1, 1>().next, languageError }, //18
            { peekByte, alwaysTrue, noop, steps1<'=', 28, 20>().next, languageError }, //19
            { passWhiteSpacesAndComments, alwaysTrue, dropString, stepsInc().next, languageError }, //20
            // B 10
            { readExpression, alwaysTrue, keepString, stepsInc().next, languageError }, //21
            { passWhiteSpacesAndComments, alwaysTrue, noop, stepsInc().next, languageError }, //22
            // : , ]
            { readByte, alwaysTrue, fillHeaderTemplateRangeStart, steps3<':', 24, ',', 15, ']', 31, 1, 1>().next, languageError }, //23
            { passWhiteSpacesAndComments, alwaysTrue, noop, stepsInc().next, languageError }, //24
            // C 20
            { readExpression, alwaysTrue, fillHeaderTemplateRangeEnd, stepsInc().next, languageError }, //25
            { passWhiteSpacesAndComments, alwaysTrue, noop, steps0<30>().next, languageError }, //26
            // != <= >= ==
            { peekByte, alwaysTrue, noop, steps1<'=', 28, 29>().next, languageError }, //27
            { readByte, isAtom1<'='>().next, fillString, stepsInc().next, languageError }, //28
            { readExpression, alwaysTrue, fillHeaderTemplateCondition, stepsInc().next, languageError }, //29
            // , ]
            { readByte, alwaysTrue, noop, steps2<',', 15, ']', 31, 1, 1>().next, languageError }, //30
            // main arguments
            { passWhiteSpacesAndComments, alwaysTrue, noop, steps1<'{', 35, 32>().next, languageError }, //31
            { readItemName, alwaysTrue, fillMainArgument, stepsInc().next, languageError }, //32
            { passWhiteSpacesAndComments, alwaysTrue, noop, steps2<'{', 35, ',', 34, 1, 1>().next, languageError }, //33
            { readByte, alwaysTrue, noop, steps0<31>().next, languageError }, //34
            // body setup
            { readByte, alwaysTrue, noop, stepsInc().next, languageError }, //35
            { passWhiteSpacesAndComments, alwaysTrue, noop, steps1<'}', 37, 38>().next, languageError }, //36
            { readByte, alwaysTrue, noop, steps0<1>().next, languageError, 1 }, //37
            // definition
            { peekByte, alwaysTrue, noop, steps1<'[', 39, 45>().next, languageError }, //38
            // [A, B]
            { readByte, alwaysTrue, noop, stepsInc().next, languageError }, //39
            { passWhiteSpacesAndComments, alwaysTrue, noop, steps1<']', 44, 41>().next, languageError }, //40
            { readItemName, alwaysTrue, keepString, stepsInc().next, languageError }, //41
            { passWhiteSpacesAndComments, alwaysTrue, noop, steps2<']', 44, ',', 43, 1, 1>().next, languageError }, //42
            { readByte, alwaysTrue, noop, steps0<40>().next, languageError }, //43
            { readByte, alwaysTrue, noop, stepsInc().next, languageError }, //44
            // C is D args
            // C = D args
            // D args
            { passWhiteSpacesAndComments, alwaysTrue, noop, stepsInc().next, languageError }, //45
            { readDefinitionTokens, alwaysTrue, setDefinitionTree, stepsInc().next, languageError }, //46
            { readByte, alwaysTrue, noop, steps4<'}', 1, ';', 36, '\r', 36, '\n', 36, 1, 1>().next, languageError }, //47
        };
    }
}
