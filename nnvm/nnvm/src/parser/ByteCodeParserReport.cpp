/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#include "ByteCodeParserReport.hpp"
#include <StringConv.hpp>
#include <chrono>

namespace nnvm {
    ByteCodeParserReportContext::ByteCodeParserReportContext() : file(USS("")), line(0), symbol(0)
    {
    }
    ByteCodeParserReportContext::ByteCodeParserReportContext(const ByteCodeParserReportContext& obj) : file(obj.file), line(obj.line), symbol(obj.symbol)
    {
    }

    ByteCodeParserReportItem::ByteCodeParserReportItem(ByteCodeParserReportLevel level, icu::UnicodeString title, icu::UnicodeString message, const ByteCodeParserReportContext& context) : level(level), title(title), message(message), context(context), when(std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now().time_since_epoch()).count())
    {
    }
    ByteCodeParserReportItem::ByteCodeParserReportItem(ByteCodeParserReportLevel level, icu::UnicodeString title, icu::UnicodeString message, const ByteCodeParserReportContext& context, int64_t time_since_epoch) : level(level), title(title), message(message), context(context), when(time_since_epoch)
    {
    }
    ByteCodeParserReportItem::ByteCodeParserReportItem(const ByteCodeParserReportItem& obj) : level(obj.level), title(obj.title), message(obj.message), context(obj.context), when(obj.when)
    {
    }
    ByteCodeParserReportItem::ByteCodeParserReportItem() : level(ByteCodeParserReportLevel::VERBOSE), title(USS("")), message(USS("")), context(ByteCodeParserReportContext()), when(std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now().time_since_epoch()).count())
    {
    }

    ByteCodeParserReport::ByteCodeParserReport()
    {
        errors.clear();
    }
    ByteCodeParserReport::ByteCodeParserReport(const ByteCodeParserReport& obj)
    {
        errors.assign(obj.errors.begin(), obj.errors.end());
    }

    std::tuple<std::list<ByteCodeParserReportItem>::iterator, std::list<ByteCodeParserReportItem>::iterator> ByteCodeParserReport::getRecords(ByteCodeParserReportLevel level)
    {
        return std::tuple<std::list<ByteCodeParserReportItem>::iterator, std::list<ByteCodeParserReportItem>::iterator>(this->errors.begin(), this->errors.end());
    }

    void ByteCodeParserReport::record(const ByteCodeParserReportItem& item)
    {
        this->errors.push_back(item);
    }
}
