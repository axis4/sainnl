/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#include "ByteCodeParserModel.hpp"
#include <ByteCodeBlock.hpp>
#include <cmath>

namespace nnvm {
    Carried::Carried()
            : string(USS("")), tree(nullptr), prefix(USS("")), package(USS("")), ctx(nullptr), counter(0) { }
    Carried::Carried(const Carried& obj)
            : string(obj.string), tree(obj.tree), prefix(obj.prefix), package(obj.package), ctx(obj.ctx), counter(obj.counter) { }
}
