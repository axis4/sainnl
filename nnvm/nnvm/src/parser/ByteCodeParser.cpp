/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#include <ByteCodeParser.hpp>
#include <deque>
#include <map>
#include <iostream>
#include <sstream>
#include <limits>
#include <algorithm> // added for VS
#include <unicode/regex.h>
#include <unicode/unistr.h>
#include <ProcessorKnownParameters.hpp>
#include <ByteCodeParserException.hpp>
#include <Internationalization.hpp>
#include <BinaryUtils.hpp>
#include <ByteCodeUtils.hpp>
#include "BOM.hpp"
#include "JSON.hpp"
#include "Language.hpp"
#include "ByteCodeParserModel.hpp"
#include "../utils/Utils.hpp"

namespace nnvm {
    ByteCodeParser::ByteCodeParser(std::shared_ptr<std::istream> stream, icu::UnicodeString prefix, icu::UnicodeString package, std::shared_ptr<Context> ctx) : stream(stream), prefix(prefix), package(package), ctx(ctx) { safe(); }
    ByteCodeParser::~ByteCodeParser() { safe(); }

    int8_t ByteCodeParser::isWhiteSpace(const unsigned char c)
    {
        return c == ' ' || c == '\t' || c == '\r' || c == '\n';
    }

    /** Stream readers **/
    ByteState::stateReader readDouble = [](std::shared_ptr<std::istream> dataSource, std::vector<uint8_t>& result) -> int64_t
    {
        return BinaryUtils::readBinary(*dataSource, result, sizeof(int64_t) * 2) * sizeof(int64_t) * 2;
    };
    ByteState::stateReader readInteger = [](std::shared_ptr<std::istream> dataSource, std::vector<uint8_t>& result) -> int64_t
    {
        return BinaryUtils::readBinary(*dataSource, result, sizeof(int64_t)) * sizeof(int64_t);
    };
    ByteState::stateReader readString = [](std::shared_ptr<std::istream> dataSource, std::vector<uint8_t>& result) -> int64_t
    {
        icu::UnicodeString tmp;
        return BinaryUtils::readString(*dataSource, tmp, result) * result.size();
    };

    /** Data checkers **/
    ByteState::stateMatcher allowedBlockName =
    [](std::vector<uint8_t>& data, int64_t size) -> int8_t {
        icu::UnicodeString s;
        if (!BinaryUtils::readString(data, s)) {
            return 0;
        }
        return s.countChar32() == 0 ? 0 :
            std::all_of(StringUtils::beginIterator(s), StringUtils::endIterator(s),
                        [](UChar32 c) { return u_isalnum(c) || c == '.' || c == '[' || c == ']'
                            || c == '#' || c == ',' || c == ' '; }) ? 1 : -1; };
    ByteState::stateMatcher allowedPackageName =
    [](std::vector<uint8_t>& data, int64_t size) -> int8_t {
        icu::UnicodeString s;
        if (!BinaryUtils::readString(data, s)) {
            return 0;
        }
        return s.countChar32() == 0 ? 0 : s.char32At(0) == StringConvUtils::u8tou('.') ? -1 :
            std::all_of(StringUtils::beginIterator(s), StringUtils::endIterator(s),
                        [](UChar32 c) {
                            //std::cout << (u_isalnum(c) || c == '.' ? "Ook2 " : "Boo2 ") << char(c) << std::endl;
                            return u_isalnum(c) || c == '.';
                        }) ? 1 : -1; };
    ByteState::stateMatcher allowedParameterName =
    [](std::vector<uint8_t>& data, int64_t size) -> int8_t {
        icu::UnicodeString s;
        if (!BinaryUtils::readString(data, s)) {
            return 0;
        }
        return s.countChar32() == 0 ? 0 : std::all_of(StringUtils::beginIterator(s), StringUtils::endIterator(s), u_isalnum) ? 1 : -1;
    };
    ByteState::stateMatcher first =
    [](std::vector<uint8_t>& data, int64_t size) -> int8_t { return 1; /*(data[0] == 2 || data[0] == '{') && size == 1 ? 1 : -1;*/ };
    ByteState::stateMatcher knownType =
    [](std::vector<uint8_t>& data, int64_t size) -> int8_t { return data[0] >= 0 && data[0] <= 3 && size == 1 ? 1 : -1; };
    ByteState::stateMatcher knownElementType =
    [](std::vector<uint8_t>& data, int64_t size) -> int8_t { return data[0] >= 0 && data[0] <= 1 && size == 1 ? 1 : -1; };
    ByteState::stateMatcher twoMatch =
    [](std::vector<uint8_t>& data, int64_t size) -> int8_t { return data[0] == 2 && size == 1 ? 1 : -1; };
    ByteState::stateMatcher nMatch =
    [](std::vector<uint8_t>& data, int64_t size) -> int8_t { return data[0] == 'N' && size == 1 ? 1 : -1; };
    ByteState::stateMatcher mMatch =
    [](std::vector<uint8_t>& data, int64_t size) -> int8_t { return data[0] == 'M' && size == 1 ? 1 : -1; };
    ByteState::stateMatcher vMatch =
    [](std::vector<uint8_t>& data, int64_t size) -> int8_t { return data[0] == 'V' && size == 1 ? 1 : -1; };

    /** Data fillers **/
    ByteState::stateFiller init =
    [](std::vector<uint8_t>& data, int64_t& size, std::shared_ptr<Carried>& carried, std::shared_ptr<std::vector<std::shared_ptr<ByteCode>>>& result) -> int8_t
    {
        icu::UnicodeString d;
        if (data.size() > 0 && !BinaryUtils::readString(data, d)) {
            return 0;
        }
        if (d.countChar32()) {
            result = std::make_shared<std::vector<std::shared_ptr<ByteCode>>>();
            result->push_back(std::make_shared<ByteCode>(d, carried->ctx->byteLoader));
        }
        return 1;
    };
    ByteState::stateFiller initBlock =
    [](std::vector<uint8_t>& data, int64_t& size, std::shared_ptr<Carried>& carried, std::shared_ptr<std::vector<std::shared_ptr<ByteCode>>>& result) -> int8_t
    {
        carried = std::make_shared<Carried>();
        if (data.size() > 0 && !BinaryUtils::readString(data, carried->string)) {
            return 0;
        }
        if (!carried->string.countChar32()) {
            carried = nullptr;
        }
        return 1;
    };
    ByteState::stateFiller initItem =
    [](std::vector<uint8_t>& data, int64_t& size, std::shared_ptr<Carried>& carried, std::shared_ptr<std::vector<std::shared_ptr<ByteCode>>>& result) -> int8_t
    {
        icu::UnicodeString d;
        if (data.size() > 0 && !BinaryUtils::readString(data, d)) {
            return 0;
        }
        if (d.countChar32()) {
            result->back()->getLastBlock()->addItem(std::make_shared<ByteCodeBlockItem>(d, result->back()->getLastBlock()));
        }
        return 1;
    };
    ByteState::stateFiller initParameter = initBlock;
    ByteState::stateFiller setBlockParameterByte =
    [](std::vector<uint8_t>& data, int64_t& size, std::shared_ptr<Carried>& carried, std::shared_ptr<std::vector<std::shared_ptr<ByteCode>>>& result) -> int8_t
    {
        if (data.size() > 0) {
            result->back()->getLastBlock()->setParameter(std::make_shared<ByteCodeParameter>(carried->string, (int8_t) data[0]));
        }
        carried = nullptr;
        return 1;
    };
    ByteState::stateFiller setBlockParameterInteger =
    [](std::vector<uint8_t>& data, int64_t& size, std::shared_ptr<Carried>& carried, std::shared_ptr<std::vector<std::shared_ptr<ByteCode>>>& result) -> int8_t
    {
        if (data.size() > 0) {
            int64_t d;
            if (!BinaryUtils::readInteger(data, d)) {
                return 0;
            }
            result->back()->getLastBlock()->setParameter(std::make_shared<ByteCodeParameter>(carried->string, d));
        }
        carried = nullptr;
        return 1;
    };
    ByteState::stateFiller setBlockParameterDouble =
    [](std::vector<uint8_t>& data, int64_t& size, std::shared_ptr<Carried>& carried, std::shared_ptr<std::vector<std::shared_ptr<ByteCode>>>& result) -> int8_t
    {
        if (data.size() > 0) {
            double d;
            if (!BinaryUtils::readDouble(data, d)) {
                return 0;
            }
            result->back()->getLastBlock()->setParameter(std::make_shared<ByteCodeParameter>(carried->string, d));
        }
        carried = nullptr;
        return 1;
    };
    ByteState::stateFiller setBlockParameterString =
    [](std::vector<uint8_t>& data, int64_t& size, std::shared_ptr<Carried>& carried, std::shared_ptr<std::vector<std::shared_ptr<ByteCode>>>& result) -> int8_t
    {
        if (data.size() > 0) {
            icu::UnicodeString d;
            if (!BinaryUtils::readString(data, d)) {
                return 0;
            }
            result->back()->getLastBlock()->setParameter(std::make_shared<ByteCodeParameter>(carried->string, d));
        }
        carried = nullptr;
        return 1;
    };
    ByteState::stateFiller setBlockType =
    [](std::vector<uint8_t>& data, int64_t& size, std::shared_ptr<Carried>& carried, std::shared_ptr<std::vector<std::shared_ptr<ByteCode>>>& result) -> int8_t
    {
        if (data.size() > 0) {
            icu::UnicodeString d;
            if (!BinaryUtils::readString(data, d)) {
                return 0;
            }
            result->back()->addBlock(std::make_shared<ByteCodeBlock>(carried->string, d));
        }
        carried = nullptr;
        return 1;
    };
    ByteState::stateFiller setItemParameterByte =
    [](std::vector<uint8_t>& data, int64_t& size, std::shared_ptr<Carried>& carried, std::shared_ptr<std::vector<std::shared_ptr<ByteCode>>>& result) -> int8_t
    {
        if (data.size() > 0) {
            result->back()->getLastBlock()->getLastItem()->setParameter(std::make_shared<ByteCodeParameter>(carried->string, (int8_t) data[0]));
        }
        carried = nullptr;
        return 1;
    };
    ByteState::stateFiller setItemParameterInteger =
    [](std::vector<uint8_t>& data, int64_t& size, std::shared_ptr<Carried>& carried, std::shared_ptr<std::vector<std::shared_ptr<ByteCode>>>& result) -> int8_t
    {
        if (data.size() > 0) {
            int64_t d;
            if (!BinaryUtils::readInteger(data, d)) {
                return 0;
            }
            result->back()->getLastBlock()->getLastItem()->setParameter(std::make_shared<ByteCodeParameter>(carried->string, d));
        }
        carried = nullptr;
        return 1;
    };
    ByteState::stateFiller setItemParameterDouble =
    [](std::vector<uint8_t>& data, int64_t& size, std::shared_ptr<Carried>& carried, std::shared_ptr<std::vector<std::shared_ptr<ByteCode>>>& result) -> int8_t
    {
        double d;
        if (!BinaryUtils::readDouble(data, d)) {
            return 0;
        }
        result->back()->getLastBlock()->getLastItem()->setParameter(std::make_shared<ByteCodeParameter>(carried->string, d));
        carried = nullptr;
        return 1;
    };
    ByteState::stateFiller setItemParameterString =
    [](std::vector<uint8_t>& data, int64_t& size, std::shared_ptr<Carried>& carried, std::shared_ptr<std::vector<std::shared_ptr<ByteCode>>>& result) -> int8_t
    {
        icu::UnicodeString d;
        if (!BinaryUtils::readString(data, d)) {
            return 0;
        }
        result->back()->getLastBlock()->getLastItem()->setParameter(std::make_shared<ByteCodeParameter>(carried->string, d));
        carried = nullptr;
        return 1;
    };

    /** State changers **/
    ByteState::stateStepper itemStep =
    [](std::vector<uint8_t>& data, int64_t size, std::shared_ptr<Carried> c, int16_t& state) -> int8_t
    {
        icu::UnicodeString d;
        if (!BinaryUtils::readString(data, d)) {
            return 0;
        }
        if (!d.countChar32()) { state = 6; } else { state++; } return 1;
    };
    ByteState::stateStepper stepByParameterType =
    [](std::vector<uint8_t>& data, int64_t size, std::shared_ptr<Carried> c, int16_t& state) -> int8_t { state += data[0] + 1; return 1; };
    ByteState::stateStepper stepNextBinaryOrJsonOrLanguageOrBOM =
    [](std::vector<uint8_t>& data, int64_t size, std::shared_ptr<Carried> c, int16_t& state) -> int8_t
    {
        state = (data[0] == 0 || data[0] == (uint8_t)0xEF || data[0] == (uint8_t)0xFE
                 ? 22
                 : (data[0] == ' ' || data[0] == '\t' || data[0] == '\n' || data[0] == '\r'
                    ? 24
                    : (data[0] == 2
                       ? 1
                       : (data[0] == '{' ? 21 : 23))));
        return 1;
    };
    ByteState::stateStepper stepParameterTypeOrBlockItems =
    [](std::vector<uint8_t>& data, int64_t size, std::shared_ptr<Carried> c, int16_t& state) -> int8_t
    {
        icu::UnicodeString d;
        if (!BinaryUtils::readString(data, d)) {
            return 0;
        }
        if (!d.countChar32()) { state = 14; } else { state++; } return 1;
    };

    /** Error reporters **/
    ByteState::stateError blockError =
    [](std::vector<uint8_t>& data, int16_t state, int64_t symbol) -> icu::UnicodeString { return _i("err.parser.invalid.blockname"); };
    ByteState::stateError itemError =
    [](std::vector<uint8_t>& data, int16_t state, int64_t symbol) -> icu::UnicodeString { return _i("err.parser.invalid.itemname"); };
    ByteState::stateError headerError =
    [](std::vector<uint8_t>& data, int16_t state, int64_t symbol) -> icu::UnicodeString { return _i("err.parser.invalid.header"); };
    ByteState::stateError packageError =
    [](std::vector<uint8_t>& data, int16_t state, int64_t symbol) -> icu::UnicodeString { return _i("err.parser.invalid.packagename"); };
    ByteState::stateError parameterError =
    [](std::vector<uint8_t>& data, int16_t state, int64_t symbol) -> icu::UnicodeString { return _i("err.parser.invalid.parametername"); };
    ByteState::stateError parameterTypeError =
    [](std::vector<uint8_t>& data, int16_t state, int64_t symbol) -> icu::UnicodeString { return _i("err.parser.invalid.parametertype"); };

    auto peekByte = ByteCodeState::peekByte().next;
    auto readByte = ByteCodeState::readByte().next;
    auto alwaysTrue = ByteState::Matches::alwaysTrue().next;
    auto noop = ByteState::Fills().noop;
	auto stepsInc = ByteState::Steps::stepsInc().next;
	template <int64_t default_step>
    using steps0 = ByteState::Steps::steps0<default_step>;
    template <uint8_t condition, int64_t step, int64_t default_step>
    using steps1 = ByteState::Steps::steps1<condition, step, default_step>;
    template <uint8_t condition, int64_t step, uint8_t condition2, int64_t step2, int64_t default_step, int8_t require_conditions>
    using steps2 = ByteState::Steps::steps2<condition, step, condition2, step2, default_step, require_conditions>;
    template <uint8_t condition, int64_t step, uint8_t condition2, int64_t step2, uint8_t condition3, int64_t step3, int64_t default_step, int8_t require_conditions>
    using steps3 = ByteState::Steps::steps3<condition, step, condition2, step2, condition3, step3, default_step, require_conditions>;
    template <uint8_t condition, int64_t step, uint8_t condition2, int64_t step2, uint8_t condition3, int64_t step3, uint8_t condition4, int64_t step4, int64_t default_step, int8_t require_conditions>
    using steps4 = ByteState::Steps::steps4<condition, step, condition2, step2, condition3, step3, condition4, step4, default_step, require_conditions>;

    int8_t terminalParse(std::shared_ptr<Carried>& carried, std::shared_ptr<std::vector<std::shared_ptr<ByteCode>>>& result)
    {
        if (carried && carried->tree != nullptr) {
            return nnvm::Language::Language::compileTree(carried->tree, result, carried->prefix, carried->ctx->setup->getByteCodeParserSetup()->neurons, carried->ctx);
        }
        else {
            if (result && carried && carried->prefix != USS("")) {
                for (auto i = result->begin(); i != result->end(); ++i) {
                    (*i)->addPackagePath(carried->prefix);
                }
            }
        }
        return 1;
    }

    std::shared_ptr<std::vector<std::shared_ptr<ByteCode>>> ByteCodeParser::read()
    {
        safe();

        ErrorHandlers<std::istream, uint8_t, Carried, std::vector<std::shared_ptr<ByteCode>>> handlers;
        this->stream->seekg(0);
        this->stream->clear();
        auto stream = this->stream;
        handlers.noParse = [&stream](int16_t stateIndex, std::shared_ptr<std::vector<int16_t> > states) {
            throw ByteCodeParserException(_i("err.parser.parserloop"), _i("err.parser.illegalstate"), stateIndex, stream->tellg());
        };
        handlers.noRead = [&stream](std::function<icu::UnicodeString(std::vector<uint8_t>&, int16_t, int64_t)>& errorDetails, std::vector<uint8_t>& buffer, int16_t stateIndex, std::shared_ptr<std::vector<int16_t> > states) {
            throw ByteCodeParserException(_i("err.parser.reader"), errorDetails(buffer, stateIndex, stream->tellg()), stateIndex, stream->tellg());
        };
        handlers.noMatch = [&stream](std::function<icu::UnicodeString(std::vector<uint8_t>&, int16_t, int64_t)>& errorDetails, std::vector<uint8_t>& buffer, int16_t stateIndex, std::shared_ptr<std::vector<int16_t> > states) {
            throw ByteCodeParserException(_i("err.parser.matcher"), errorDetails(buffer, stateIndex, stream->tellg()), stateIndex, stream->tellg());
        };
        handlers.noFill = [&stream](std::function<icu::UnicodeString(std::vector<uint8_t>&, int16_t, int64_t)>& errorDetails, std::vector<uint8_t>& buffer, int16_t stateIndex, std::shared_ptr<std::vector<int16_t> > states) {
            throw ByteCodeParserException(_i("err.parser.filler"), errorDetails(buffer, stateIndex, stream->tellg()), stateIndex, stream->tellg());
        };
        handlers.noStep = [&stream](std::function<icu::UnicodeString(std::vector<uint8_t>&, int16_t, int64_t)>& errorDetails, std::vector<uint8_t>& buffer, int16_t stateIndex, std::shared_ptr<std::vector<int16_t> > states) {
            throw ByteCodeParserException(_i("err.parser.stepper"), errorDetails(buffer, stateIndex, stream->tellg()), stateIndex, stream->tellg());
        };
        handlers.noTerminal = [&stream](std::shared_ptr<Carried>& carried, std::shared_ptr<std::vector<std::shared_ptr<ByteCode>>>& result, int16_t stateIndex, std::shared_ptr<std::vector<int16_t> > states) {
            throw ByteCodeParserException(_i("err.parser.terminal"), USS(""), stateIndex, stream->tellg());
        };
        
        auto json = SubMachine<std::istream, uint8_t, Carried, std::vector<std::shared_ptr<ByteCode>>>(nnvm::JSON::JSONToByteCode::states, handlers, -1, 1);
        auto bom = SubMachine<std::istream, uint8_t, Carried, std::vector<std::shared_ptr<ByteCode>>>(nnvm::BOM::BOM::states, handlers, 0, 0);
        auto sainnl = SubMachine<std::istream, uint8_t, Carried, std::vector<std::shared_ptr<ByteCode>>>(nnvm::Language::Language::states, handlers, -1, 1);
        
        std::vector<ByteState> readMachinery = {
            //// BINARY
            // header
            { peekByte, first, noop, stepNextBinaryOrJsonOrLanguageOrBOM, headerError }, //0
            { readByte, twoMatch, noop, stepsInc, headerError }, //1
            { readByte, nMatch, noop, stepsInc, headerError }, //2
            { readByte, vMatch, noop, stepsInc, headerError }, //3
            { readByte, mMatch, noop, stepsInc, headerError, 1 }, //4
            { readString, allowedPackageName, init, stepsInc, packageError, 1 }, //5
            // block
            { readString, allowedBlockName, initBlock, stepsInc, blockError, 1 }, //6
            { readString, allowedPackageName, setBlockType, stepsInc, blockError, 1 }, //7
            // parameter
            // parameter name
            { readString, allowedParameterName, initParameter, stepParameterTypeOrBlockItems, parameterError }, //8
            // parameter type
            { readByte, knownType, noop, stepByParameterType, parameterTypeError }, //9
            // parameter value
            { readByte, alwaysTrue, setBlockParameterByte, steps0<8>().next, parameterError, 1 }, //10
            { readInteger, alwaysTrue, setBlockParameterInteger, steps0<8>().next, parameterError, 1 }, //11
            { readDouble, alwaysTrue, setBlockParameterDouble, steps0<8>().next, parameterError, 1 }, //12
            { readString, alwaysTrue, setBlockParameterString, steps0<8>().next, parameterError, 1 }, //13
            // item
            { readString, allowedBlockName, initItem, itemStep, itemError, 1 }, //14
            // parameter
            // parameter name
            { readString, allowedParameterName, initParameter, stepParameterTypeOrBlockItems, parameterError, 1 }, //15
            // parameter type
            { readByte, knownType, noop, stepByParameterType, parameterTypeError }, //16
            // parameter value
            { readByte, alwaysTrue, setItemParameterByte, steps0<15>().next, parameterError, 1 }, //17
            { readInteger, alwaysTrue, setItemParameterInteger, steps0<15>().next, parameterError, 1 }, //18
            { readDouble, alwaysTrue, setItemParameterDouble, steps0<15>().next, parameterError, 1 }, //19
            { readString, alwaysTrue, setItemParameterString, steps0<15>().next, parameterError, 1 }, //20
            //// JSON
            json, //21
            //// BOM
            bom, //22
            //// SAINNL
            sainnl, //23
            // skip a byte
            { readByte, alwaysTrue, noop, steps0<0>().next, headerError } //24
        };
        
        StateMachine<std::istream, uint8_t, Carried, std::vector<std::shared_ptr<ByteCode>>>::setupFiller setup = [this](std::shared_ptr<Carried>& carried) -> int8_t
        {
            if (!carried) {
                carried = std::make_shared<Carried>();
            }
            auto weak = std::weak_ptr<Carried>(carried);
            carried->ctx = this->ctx;
            carried->ctx->getNewIndex = [weak](){
                auto l = weak.lock();
                return l ? ++l->counter : 0;
            };
            carried->prefix = this->prefix;
            carried->package = this->package;
            return 1;
        };

        ctx->setup->getLogger()->debug(USS("Prepared state machine for ") + package);

        // main parsing
        StateMachine<std::istream, uint8_t, Carried, std::vector<std::shared_ptr<ByteCode>>> a(readMachinery, handlers, setup, terminalParse);
        std::shared_ptr<std::vector<std::shared_ptr<ByteCode>>> result = a.run(stream);
        if (result) {
            ctx->setup->getLogger()->debug(USS("Finalization of parsing for ") + package);
            for (auto i = result->begin(); i != result->end(); ++i) {
                ByteCodeUtils::finalizeCode(*i);
            }
        }

        ctx->setup->getLogger()->debug(USS("State machine ended for ") + package);

        return result;
    }
}