/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#pragma once

namespace nnvm {
    class DefinitionBuilder;
}

#include <predef.h>
#include <vector>
#include <inttypes.h>
#include <unicode/unistr.h>
#include <Disposable.hpp>
#include <LoaderModel.hpp>
#include <LR.hpp>
#include <ByteCodeLoader.hpp>
#include "Language.hpp"

#define EXPRESSION_BLOCK USS("plus")

namespace nnvm {
    class DefinitionBuilder : public Disposable
    {
    public:
        typedef StringState<int64_t, std::vector<Token<TokenKind>>> DefinitionState;
        
        DefinitionBuilder(std::shared_ptr<Context> ctx);
        int8_t parseDefinition(const icu::UnicodeString& definition, LanguageDefinition& languageDefinition, std::deque<LanguageDefinition>& container, std::map<icu::UnicodeString, std::tuple<int64_t, int64_t> > &ranges);
        int8_t parseExpression(const icu::UnicodeString& definition, Operand& operand, std::map<icu::UnicodeString, std::tuple<int64_t, int64_t> > &ranges);
        int8_t parseTemplate(const icu::UnicodeString& templateDef, Token<TokenKind>& token);
        int8_t simplifyExpression(Operand& operand, std::deque<LanguageDefinition>& container, std::map<icu::UnicodeString, std::tuple<int64_t, int64_t> > &ranges, std::deque<icu::UnicodeString> &templateNames, int64_t& height);
        int8_t simplifyArithmericBlock(LanguageCall& block, std::deque<LanguageDefinition>& container, std::map<icu::UnicodeString, std::tuple<int64_t, int64_t> > &ranges, std::deque<icu::UnicodeString> &templateNames, int64_t& height);
        void addDelay(Operand& operand, std::deque<LanguageDefinition>& container, std::map<icu::UnicodeString, std::tuple<int64_t, int64_t> > &ranges, std::deque<icu::UnicodeString> &templateNames, int64_t height);
        void fillTokenType(Token<TokenKind>& token);
        void nextToken(std::vector<Token<TokenKind> >& result);
        int8_t saveToken(std::vector<UChar32>& data, int64_t& size, std::shared_ptr<int64_t>& carried, std::shared_ptr<std::vector<Token<TokenKind> > >& result);
        
        static int8_t isLiteral(const Operand& operand);
        static int8_t isNumber(const Operand& operand);
        
        static void dumpTokens(std::vector<Token<TokenKind> >& tokens, int64_t level=0);
    private:
        static int8_t incAndReturn(int8_t res, int64_t sourceHeight, int64_t& targetHeight);
        static std::vector<Token<TokenKind> > combineLeafs(Token<TokenKind>& token);
        static icu::UnicodeString aggregateName(const Operand& operand);
        static icu::UnicodeString indexedName(LanguageCall& call, std::map<icu::UnicodeString, std::tuple<int64_t, int64_t> > &ranges);
        static icu::UnicodeString indexedName(Operand& operand, std::map<icu::UnicodeString, std::tuple<int64_t, int64_t> > &ranges);

        std::vector<DefinitionBuilder::DefinitionState> defStates();

        void visitToken(const LR<TokenKind>& lr, std::vector<Token<TokenKind> >::iterator tokensBegin, std::vector<Token<TokenKind> >::iterator tokensEnd, Token<TokenKind>& rootToken);

        int8_t flatPlus(LanguageCall& block, std::vector<int64_t>& heights, std::deque<LanguageDefinition>& container, std::map<icu::UnicodeString, std::tuple<int64_t, int64_t> > &ranges, std::deque<icu::UnicodeString> &templateNames);
        int8_t flatMultiply(LanguageCall& block, std::vector<int64_t>& heights, std::deque<LanguageDefinition>& container, std::map<icu::UnicodeString, std::tuple<int64_t, int64_t> > &ranges, std::deque<icu::UnicodeString> &templateNames);
        int8_t asTree(Token<TokenKind>& token, std::vector<Token<TokenKind> >& rawTokens, Operand& operand, std::deque<LanguageDefinition>& container, std::map<icu::UnicodeString, std::tuple<int64_t, int64_t> > &ranges, std::deque<icu::UnicodeString> &templateNames);
        int8_t asTree(Token<TokenKind>& token, std::vector<Token<TokenKind> >& rawTokens, Operand& operand, std::deque<LanguageDefinition>& container, std::map<icu::UnicodeString, std::tuple<int64_t, int64_t> > &ranges, std::deque<icu::UnicodeString> &templateNames, int8_t atRoot);
        int8_t asTemplateTree(Token<TokenKind>& token, std::vector<Token<TokenKind> >& rawTokens, LanguageCall& languageCall, std::deque<LanguageDefinition>& container);
        int8_t prepareCall(std::shared_ptr<ParsedNeuron> neuron, Operand& operand, std::deque<LanguageDefinition>& container, std::map<icu::UnicodeString, std::tuple<int64_t, int64_t> > &ranges, std::deque<icu::UnicodeString> &templateNames, int64_t& height);
        void balanceHeights(LanguageCall& call, const std::vector<int64_t>& heights, std::deque<LanguageDefinition>& container, std::map<icu::UnicodeString, std::tuple<int64_t, int64_t> > &ranges, std::deque<icu::UnicodeString> &templateNames);
        int8_t extractToLiteral(Operand& operand, std::deque<LanguageDefinition>& container, std::map<icu::UnicodeString, std::tuple<int64_t, int64_t>> &ranges, std::deque<icu::UnicodeString> &templateNames, int64_t& height);
        int8_t separateExpressionDefinition(Operand& operand, std::deque<LanguageDefinition>& container, std::map<icu::UnicodeString, std::tuple<int64_t, int64_t>> &ranges, std::deque<icu::UnicodeString> &templateNames, int64_t& height);
        int8_t parseCall(std::vector<Token<TokenKind> >& subTokens, std::vector<Token<TokenKind> >& templateSubTokens, std::shared_ptr<ParsedNeuron> neuron, Operand& operand, std::deque<LanguageDefinition>& container, std::map<icu::UnicodeString, std::tuple<int64_t, int64_t> > &ranges, std::deque<icu::UnicodeString> &templateNames);
        int8_t parseDefinition(std::vector<Token<TokenKind> >& tokens, LanguageDefinition& languageDefinition, std::deque<LanguageDefinition>& container, std::map<icu::UnicodeString, std::tuple<int64_t, int64_t> > &ranges);
        int8_t parseExpression(Token<TokenKind>& rootToken, Operand& operand, std::deque<LanguageDefinition>& container, std::map<icu::UnicodeString, std::tuple<int64_t, int64_t>> &ranges);
        
        std::function<int64_t()> getNewIndex;
        std::shared_ptr<Context> ctx;
    };
}
