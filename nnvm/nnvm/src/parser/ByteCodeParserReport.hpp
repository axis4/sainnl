/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#pragma once

#include <predef.h>
#include <Disposable.hpp>
#include <list>
#include <unicode/unistr.h>
#include <chrono>

namespace nnvm {
    enum class ByteCodeParserReportLevel
    {
        VERBOSE,
        INFO,
        WARN,
        ERROR
    };

    class ByteCodeParserReportContext : public Disposable
    {
    public:
        ByteCodeParserReportContext();
        ByteCodeParserReportContext(const ByteCodeParserReportContext& obj);
        
        icu::UnicodeString file;
        int64_t line;
        int64_t symbol;
    };

    class ByteCodeParserReportItem : public Disposable
    {
    public:
        ByteCodeParserReportItem(ByteCodeParserReportLevel level, icu::UnicodeString title, icu::UnicodeString message, const ByteCodeParserReportContext& context);
        ByteCodeParserReportItem(ByteCodeParserReportLevel level, icu::UnicodeString title, icu::UnicodeString message, const ByteCodeParserReportContext& context, int64_t time_since_epoch);
        ByteCodeParserReportItem(const ByteCodeParserReportItem& obj);
        ByteCodeParserReportItem();
        
        ByteCodeParserReportLevel level;
        icu::UnicodeString title;
        icu::UnicodeString message;
        ByteCodeParserReportContext context;
        int64_t when;
    };

    class ByteCodeParserReport : public Disposable
    {
    public:
        ByteCodeParserReport();
        ByteCodeParserReport(const ByteCodeParserReport& obj);
        
        std::tuple<std::list<ByteCodeParserReportItem>::iterator, std::list<ByteCodeParserReportItem>::iterator> getRecords(ByteCodeParserReportLevel level);
        void record(const ByteCodeParserReportItem& item);
    private:
        std::list<ByteCodeParserReportItem> errors;
    };
}
