/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#include "DefinitionBuilder.hpp"
#include <queue>
#include <math.h>
#include <ByteCodeException.hpp>
#include <LanguageModel.hpp>
#include <ByteCodeUtils.hpp>
#include <DefaultNeurons.hpp>
#include <ByteCodeParser.hpp>
#include "ByteCodeParserException.hpp"
#include "Language.hpp"
#include "../utils/Utils.hpp"

namespace nnvm {
    DefinitionBuilder::DefinitionBuilder(std::shared_ptr<Context> ctx) : getNewIndex(ctx->getNewIndex), ctx(ctx) { }

    // grammar for expression tokens
    std::vector<TokenState<TokenKind>> expStates
    {
        // OPERATOR: NOT EQUAL
        //         | CONDITIONAL EQUAL
        //         | CONDITIONAL
        //         | EQUAL EQUAL
        TokenState<TokenKind>::prepare({ TokenKind::NOT, TokenKind::EQUAL }, TokenKind::OPERATOR, 100),
        TokenState<TokenKind>::prepare({ TokenKind::CONDITIONAL, TokenKind::EQUAL }, TokenKind::OPERATOR, 100),
        TokenState<TokenKind>::prepare({ TokenKind::EQUAL, TokenKind::EQUAL }, TokenKind::OPERATOR, 100),
        TokenState<TokenKind>::prepare({ TokenKind::CONDITIONAL }, TokenKind::OPERATOR, 100),
        
        // EXPRESSION: LITERAL OPERATOR LITERAL
        //           | LITERAL LINEAR_OPERATOR LITERAL
        //           | LITERAL FACTOR_OPERATOR LITERAL
        TokenState<TokenKind>::prepare({ TokenKind::LITERAL, TokenKind::FACTOR_OPERATOR, TokenKind::LITERAL }, { TokenKind::TOKEN_EOF }, TokenKind::EXPRESSION, 90),
        TokenState<TokenKind>::prepare({ TokenKind::LITERAL, TokenKind::LINEAR_OPERATOR, TokenKind::LITERAL }, { TokenKind::TOKEN_EOF }, TokenKind::EXPRESSION, 90),
        TokenState<TokenKind>::prepare({ TokenKind::LITERAL, TokenKind::OPERATOR, TokenKind::LITERAL }, { TokenKind::TOKEN_EOF }, TokenKind::EXPRESSION, 90),
        TokenState<TokenKind>::prepare({ TokenKind::LITERAL, TokenKind::FACTOR_OPERATOR, TokenKind::LITERAL }, { TokenKind::RBRACKET }, TokenKind::EXPRESSION, 90),
        TokenState<TokenKind>::prepare({ TokenKind::LITERAL, TokenKind::LINEAR_OPERATOR, TokenKind::LITERAL }, { TokenKind::RBRACKET }, TokenKind::EXPRESSION, 90),
        TokenState<TokenKind>::prepare({ TokenKind::LITERAL, TokenKind::OPERATOR, TokenKind::LITERAL }, { TokenKind::RBRACKET }, TokenKind::EXPRESSION, 90),
        TokenState<TokenKind>::prepare({ TokenKind::LITERAL, TokenKind::FACTOR_OPERATOR, TokenKind::LITERAL }, { TokenKind::ARG_DELIMITER }, TokenKind::EXPRESSION, 90),
        TokenState<TokenKind>::prepare({ TokenKind::LITERAL, TokenKind::LINEAR_OPERATOR, TokenKind::LITERAL }, { TokenKind::ARG_DELIMITER }, TokenKind::EXPRESSION, 90),
        TokenState<TokenKind>::prepare({ TokenKind::LITERAL, TokenKind::OPERATOR, TokenKind::LITERAL }, { TokenKind::ARG_DELIMITER }, TokenKind::EXPRESSION, 90),
        
        // LITERAL: LITERAL
        //        | LBRACKET EXPRESSION RBRACKET
        TokenState<TokenKind>::prepare({ TokenKind::LBRACKET, TokenKind::EXPRESSION, TokenKind::RBRACKET }, TokenKind::LITERAL, 80),
    };

    // grammar for expression tokens in static template arguments
    std::vector<TokenState<TokenKind>> tempStates
    {
        // OPERATOR: NOT EQUAL
        //         | CONDITIONAL EQUAL
        //         | CONDITIONAL
        //         | EQUAL EQUAL
        TokenState<TokenKind>::prepare({ TokenKind::NOT, TokenKind::EQUAL }, TokenKind::OPERATOR, 100),
        TokenState<TokenKind>::prepare({ TokenKind::CONDITIONAL, TokenKind::EQUAL }, TokenKind::OPERATOR, 100),
        TokenState<TokenKind>::prepare({ TokenKind::EQUAL, TokenKind::EQUAL }, TokenKind::OPERATOR, 100),
        TokenState<TokenKind>::prepare({ TokenKind::CONDITIONAL }, TokenKind::OPERATOR, 100),
        
        // EXPRESSION: LITERAL OPERATOR LITERAL
        //           | LITERAL LINEAR_OPERATOR LITERAL
        //           | LITERAL FACTOR_OPERATOR LITERAL
        TokenState<TokenKind>::prepare({ TokenKind::LITERAL, TokenKind::FACTOR_OPERATOR, TokenKind::LITERAL }, { TokenKind::TOKEN_EOF }, TokenKind::EXPRESSION, 90),
        TokenState<TokenKind>::prepare({ TokenKind::LITERAL, TokenKind::LINEAR_OPERATOR, TokenKind::LITERAL }, { TokenKind::TOKEN_EOF }, TokenKind::EXPRESSION, 90),
        TokenState<TokenKind>::prepare({ TokenKind::LITERAL, TokenKind::OPERATOR, TokenKind::LITERAL }, { TokenKind::TOKEN_EOF }, TokenKind::EXPRESSION, 90),
        TokenState<TokenKind>::prepare({ TokenKind::LITERAL, TokenKind::FACTOR_OPERATOR, TokenKind::LITERAL }, { TokenKind::RBRACKET }, TokenKind::EXPRESSION, 90),
        TokenState<TokenKind>::prepare({ TokenKind::LITERAL, TokenKind::LINEAR_OPERATOR, TokenKind::LITERAL }, { TokenKind::RBRACKET }, TokenKind::EXPRESSION, 90),
        TokenState<TokenKind>::prepare({ TokenKind::LITERAL, TokenKind::OPERATOR, TokenKind::LITERAL }, { TokenKind::RBRACKET }, TokenKind::EXPRESSION, 90),
        TokenState<TokenKind>::prepare({ TokenKind::LITERAL, TokenKind::FACTOR_OPERATOR, TokenKind::LITERAL }, { TokenKind::ARG_DELIMITER }, TokenKind::EXPRESSION, 90),
        TokenState<TokenKind>::prepare({ TokenKind::LITERAL, TokenKind::LINEAR_OPERATOR, TokenKind::LITERAL }, { TokenKind::ARG_DELIMITER }, TokenKind::EXPRESSION, 90),
        TokenState<TokenKind>::prepare({ TokenKind::LITERAL, TokenKind::OPERATOR, TokenKind::LITERAL }, { TokenKind::ARG_DELIMITER }, TokenKind::EXPRESSION, 90),
        
        // LITERAL: LBRACKET EXPRESSION RBRACKET
        //        | LBRACKET LITERAL RBRACKET
        //        | LBRACKET CALL RBRACKET
        TokenState<TokenKind>::prepare({ TokenKind::LBRACKET, TokenKind::EXPRESSION, TokenKind::RBRACKET }, TokenKind::LITERAL, 80),
        TokenState<TokenKind>::prepare({ TokenKind::LBRACKET, TokenKind::LITERAL, TokenKind::RBRACKET }, TokenKind::LITERAL, 80),
        TokenState<TokenKind>::prepare({ TokenKind::LBRACKET, TokenKind::CALL, TokenKind::RBRACKET }, TokenKind::LITERAL, 80),
        
        // KEY_ARG: LITERAL COLON LITERAL
        //        | LITERAL COLON EXPRESSION
        TokenState<TokenKind>::prepare({ TokenKind::LITERAL, TokenKind::COLON, TokenKind::LITERAL }, TokenKind::KEY_ARG, 70),
        TokenState<TokenKind>::prepare({ TokenKind::LITERAL, TokenKind::COLON, TokenKind::EXPRESSION }, TokenKind::KEY_ARG, 70),
        
        // ARGS: KEY_ARG ARG_DELIMITER KEY_ARG
        //     | ARGS ARG_DELIMITER KEY_ARG
        TokenState<TokenKind>::prepare({ TokenKind::KEY_ARG, TokenKind::ARG_DELIMITER, TokenKind::KEY_ARG }, { TokenKind::RBRACKET }, TokenKind::ARGS),
        TokenState<TokenKind>::prepare({ TokenKind::ARGS, TokenKind::ARG_DELIMITER, TokenKind::KEY_ARG }, { TokenKind::RBRACKET }, TokenKind::ARGS),
        TokenState<TokenKind>::prepare({ TokenKind::KEY_ARG, TokenKind::ARG_DELIMITER, TokenKind::KEY_ARG }, { TokenKind::TOKEN_EOF }, TokenKind::ARGS),
        TokenState<TokenKind>::prepare({ TokenKind::ARGS, TokenKind::ARG_DELIMITER, TokenKind::KEY_ARG }, { TokenKind::TOKEN_EOF }, TokenKind::ARGS),
    };

    // grammar for definition tokens - "all definitions"
    std::vector<TokenState<TokenKind>> tokenStates
    {
        // OPERATOR: NOT EQUAL
        //         | CONDITIONAL EQUAL
        //         | CONDITIONAL
        //         | EQUAL EQUAL
        TokenState<TokenKind>::prepare({ TokenKind::NOT, TokenKind::EQUAL }, TokenKind::OPERATOR, 100),
        TokenState<TokenKind>::prepare({ TokenKind::CONDITIONAL, TokenKind::EQUAL }, TokenKind::OPERATOR, 100),
        TokenState<TokenKind>::prepare({ TokenKind::EQUAL, TokenKind::EQUAL }, TokenKind::OPERATOR, 100),
        TokenState<TokenKind>::prepare({ TokenKind::CONDITIONAL }, TokenKind::OPERATOR, 100),
        
        // EXPRESSION: LITERAL OPERATOR LITERAL
        TokenState<TokenKind>::prepare({ TokenKind::LITERAL, TokenKind::OPERATOR, TokenKind::LITERAL }, { TokenKind::TOKEN_EOF }, TokenKind::EXPRESSION, 90),
        TokenState<TokenKind>::prepare({ TokenKind::LITERAL, TokenKind::OPERATOR, TokenKind::LITERAL }, { TokenKind::RBRACKET }, TokenKind::EXPRESSION, 90),
        TokenState<TokenKind>::prepare({ TokenKind::LITERAL, TokenKind::OPERATOR, TokenKind::LITERAL }, { TokenKind::ARG_DELIMITER }, TokenKind::EXPRESSION, 90),

        // LITERAL: LBRACKET EXPRESSION RBRACKET
        //        | LBRACKET LITERAL RBRACKET
        //        | LBRACKET CALL RBRACKET
        TokenState<TokenKind>::prepare({ TokenKind::LBRACKET, TokenKind::EXPRESSION, TokenKind::RBRACKET }, TokenKind::LITERAL, 80),
        TokenState<TokenKind>::prepare({ TokenKind::LBRACKET, TokenKind::LITERAL, TokenKind::RBRACKET }, TokenKind::LITERAL, 80),
        TokenState<TokenKind>::prepare({ TokenKind::LBRACKET, TokenKind::CALL, TokenKind::RBRACKET }, TokenKind::LITERAL, 80),
        
        // KEY_ARG: LITERAL COLON LITERAL
        TokenState<TokenKind>::prepare({ TokenKind::LITERAL, TokenKind::COLON, TokenKind::LITERAL }, TokenKind::KEY_ARG, 70),
        
        // ARGS: LITERAL ARG_DELIMITER LITERAL
        //     | LITERAL ARG_DELIMITER KEY_ARG
        //     | KEY_ARG ARG_DELIMITER LITERAL
        //     | KEY_ARG ARG_DELIMITER KEY_ARG
        //     | ARGS ARG_DELIMITER LITERAL
        //     | ARGS ARG_DELIMITER KEY_ARG
        //     | LITERAL ARG_DELIMITER ARGS
        //     | KEY_ARG ARG_DELIMITER ARGS
        TokenState<TokenKind>::prepare({ TokenKind::LITERAL, TokenKind::ARG_DELIMITER, TokenKind::LITERAL }, { TokenKind::RBRACKET }, TokenKind::ARGS),
        TokenState<TokenKind>::prepare({ TokenKind::LITERAL, TokenKind::ARG_DELIMITER, TokenKind::KEY_ARG }, { TokenKind::RBRACKET }, TokenKind::ARGS),
        TokenState<TokenKind>::prepare({ TokenKind::KEY_ARG, TokenKind::ARG_DELIMITER, TokenKind::LITERAL }, { TokenKind::RBRACKET }, TokenKind::ARGS),
        TokenState<TokenKind>::prepare({ TokenKind::KEY_ARG, TokenKind::ARG_DELIMITER, TokenKind::KEY_ARG }, { TokenKind::RBRACKET }, TokenKind::ARGS),
        TokenState<TokenKind>::prepare({ TokenKind::ARGS, TokenKind::ARG_DELIMITER, TokenKind::LITERAL }, { TokenKind::RBRACKET }, TokenKind::ARGS),
        TokenState<TokenKind>::prepare({ TokenKind::ARGS, TokenKind::ARG_DELIMITER, TokenKind::KEY_ARG }, { TokenKind::RBRACKET }, TokenKind::ARGS),
        TokenState<TokenKind>::prepare({ TokenKind::LITERAL, TokenKind::ARG_DELIMITER, TokenKind::ARGS }, { TokenKind::RBRACKET }, TokenKind::ARGS),
        TokenState<TokenKind>::prepare({ TokenKind::KEY_ARG, TokenKind::ARG_DELIMITER, TokenKind::ARGS }, { TokenKind::RBRACKET }, TokenKind::ARGS),
        TokenState<TokenKind>::prepare({ TokenKind::LITERAL, TokenKind::ARG_DELIMITER, TokenKind::LITERAL }, { TokenKind::TOKEN_EOF }, TokenKind::ARGS),
        TokenState<TokenKind>::prepare({ TokenKind::LITERAL, TokenKind::ARG_DELIMITER, TokenKind::KEY_ARG }, { TokenKind::TOKEN_EOF }, TokenKind::ARGS),
        TokenState<TokenKind>::prepare({ TokenKind::KEY_ARG, TokenKind::ARG_DELIMITER, TokenKind::LITERAL }, { TokenKind::TOKEN_EOF }, TokenKind::ARGS),
        TokenState<TokenKind>::prepare({ TokenKind::KEY_ARG, TokenKind::ARG_DELIMITER, TokenKind::KEY_ARG }, { TokenKind::TOKEN_EOF }, TokenKind::ARGS),
        TokenState<TokenKind>::prepare({ TokenKind::ARGS, TokenKind::ARG_DELIMITER, TokenKind::LITERAL }, { TokenKind::TOKEN_EOF }, TokenKind::ARGS),
        TokenState<TokenKind>::prepare({ TokenKind::ARGS, TokenKind::ARG_DELIMITER, TokenKind::KEY_ARG }, { TokenKind::TOKEN_EOF }, TokenKind::ARGS),
        TokenState<TokenKind>::prepare({ TokenKind::LITERAL, TokenKind::ARG_DELIMITER, TokenKind::ARGS }, { TokenKind::TOKEN_EOF }, TokenKind::ARGS),
        TokenState<TokenKind>::prepare({ TokenKind::KEY_ARG, TokenKind::ARG_DELIMITER, TokenKind::ARGS }, { TokenKind::TOKEN_EOF }, TokenKind::ARGS),
        
        // CALL: LITERAL FROM BLOCK_NAME ARGS
        //     | LITERAL FROM BLOCK_NAME LITERAL
        //     | LITERAL FROM BLOCK_NAME
        //     | BLOCK_NAME ARGS
        //     | BLOCK_NAME LITERAL
        //     | BLOCK_NAME
        TokenState<TokenKind>::prepare({ TokenKind::LITERAL, TokenKind::FROM, TokenKind::LITERAL, TokenKind::ARGS }, { TokenKind::RBRACKET }, TokenKind::CALL),
        TokenState<TokenKind>::prepare({ TokenKind::LITERAL, TokenKind::FROM, TokenKind::LITERAL, TokenKind::ARGS }, { TokenKind::TOKEN_EOF }, TokenKind::CALL),
        TokenState<TokenKind>::prepare({ TokenKind::LITERAL, TokenKind::FROM, TokenKind::LITERAL, TokenKind::LITERAL }, { TokenKind::RBRACKET }, TokenKind::CALL),
        TokenState<TokenKind>::prepare({ TokenKind::LITERAL, TokenKind::FROM, TokenKind::LITERAL, TokenKind::LITERAL }, { TokenKind::TOKEN_EOF }, TokenKind::CALL),
        TokenState<TokenKind>::prepare({ TokenKind::LITERAL, TokenKind::FROM, TokenKind::LITERAL, TokenKind::KEY_ARG }, { TokenKind::RBRACKET }, TokenKind::CALL),
        TokenState<TokenKind>::prepare({ TokenKind::LITERAL, TokenKind::FROM, TokenKind::LITERAL, TokenKind::KEY_ARG }, { TokenKind::TOKEN_EOF }, TokenKind::CALL),
        TokenState<TokenKind>::prepare({ TokenKind::LITERAL, TokenKind::FROM, TokenKind::LITERAL }, { TokenKind::RBRACKET }, TokenKind::CALL),
        TokenState<TokenKind>::prepare({ TokenKind::LITERAL, TokenKind::FROM, TokenKind::LITERAL }, { TokenKind::TOKEN_EOF }, TokenKind::CALL),
        TokenState<TokenKind>::prepare({ TokenKind::LITERAL, TokenKind::ARGS }, { TokenKind::RBRACKET }, TokenKind::CALL),
        TokenState<TokenKind>::prepare({ TokenKind::LITERAL, TokenKind::ARGS }, { TokenKind::TOKEN_EOF }, TokenKind::CALL),
        TokenState<TokenKind>::prepare({ TokenKind::LITERAL, TokenKind::LITERAL }, { TokenKind::RBRACKET }, TokenKind::CALL),
        TokenState<TokenKind>::prepare({ TokenKind::LITERAL, TokenKind::LITERAL }, { TokenKind::TOKEN_EOF }, TokenKind::CALL),
        TokenState<TokenKind>::prepare({ TokenKind::LITERAL, TokenKind::KEY_ARG }, { TokenKind::RBRACKET }, TokenKind::CALL),
        TokenState<TokenKind>::prepare({ TokenKind::LITERAL, TokenKind::KEY_ARG }, { TokenKind::TOKEN_EOF }, TokenKind::CALL),
        TokenState<TokenKind>::prepare({ TokenKind::LITERAL }, { TokenKind::RBRACKET }, TokenKind::CALL),
        TokenState<TokenKind>::prepare({ TokenKind::LITERAL }, { TokenKind::TOKEN_EOF }, TokenKind::CALL),
        TokenState<TokenKind>::prepare({ TokenKind::LITERAL, TokenKind::TEMPLATER, TokenKind::TEMPLATER_CONTENT, TokenKind::ARGS }, { TokenKind::RBRACKET }, TokenKind::CALL),
        TokenState<TokenKind>::prepare({ TokenKind::LITERAL, TokenKind::TEMPLATER, TokenKind::TEMPLATER_CONTENT, TokenKind::ARGS }, { TokenKind::TOKEN_EOF }, TokenKind::CALL),
        TokenState<TokenKind>::prepare({ TokenKind::LITERAL, TokenKind::TEMPLATER, TokenKind::TEMPLATER_CONTENT, TokenKind::LITERAL }, { TokenKind::RBRACKET }, TokenKind::CALL),
        TokenState<TokenKind>::prepare({ TokenKind::LITERAL, TokenKind::TEMPLATER, TokenKind::TEMPLATER_CONTENT, TokenKind::LITERAL }, { TokenKind::TOKEN_EOF }, TokenKind::CALL),
        TokenState<TokenKind>::prepare({ TokenKind::LITERAL, TokenKind::TEMPLATER, TokenKind::TEMPLATER_CONTENT, TokenKind::KEY_ARG }, { TokenKind::RBRACKET }, TokenKind::CALL),
        TokenState<TokenKind>::prepare({ TokenKind::LITERAL, TokenKind::TEMPLATER, TokenKind::TEMPLATER_CONTENT, TokenKind::KEY_ARG }, { TokenKind::TOKEN_EOF }, TokenKind::CALL),
        TokenState<TokenKind>::prepare({ TokenKind::LITERAL, TokenKind::TEMPLATER, TokenKind::TEMPLATER_CONTENT }, { TokenKind::RBRACKET }, TokenKind::CALL),
        TokenState<TokenKind>::prepare({ TokenKind::LITERAL, TokenKind::TEMPLATER, TokenKind::TEMPLATER_CONTENT }, { TokenKind::TOKEN_EOF }, TokenKind::CALL),
    };

    void DefinitionBuilder::fillTokenType(Token<TokenKind>& token)
    {
        if (token.value == USS("\\")) {
            token.type.push_back(TokenKind::TEMPLATER);
        }
        if (token.value == USS(":")) {
            token.type.push_back(TokenKind::COLON);
        }
        if (token.value == USS("(")) {
            token.type.push_back(TokenKind::LBRACKET);
        }
        if (token.value == USS(")")) {
            token.type.push_back(TokenKind::RBRACKET);
        }
        if (token.value == USS(",")) {
            token.type.push_back(TokenKind::ARG_DELIMITER);
        }
        if (token.value == USS("=") || token.value == USS("is")) {
            token.type.push_back(TokenKind::ASSIGN);
        }
        if (token.value == USS("+") || token.value == USS("reduction")) {
            token.type.push_back(TokenKind::REDUCTION);
        }
        if (token.value == USS("@") || token.value == USS("from")) {
            token.type.push_back(TokenKind::FROM);
        }
        if (token.value == USS("+") || token.value == USS("-")) {
            token.type.push_back(TokenKind::OPERATOR);
        }
        if (token.value == USS("*") || token.value == USS("/")) {
            token.type.push_back(TokenKind::FACTOR_OPERATOR);
        }
        if (token.value == USS("=")) {
            token.type.push_back(TokenKind::EQUAL);
        }
        if (token.value == USS("!")) {
            token.type.push_back(TokenKind::NOT);
        }
        if (token.value == USS(">") || token.value == USS("<")) {
            token.type.push_back(TokenKind::CONDITIONAL);
        }
        if (nnvm::Language::Language::isUnicodeNumber(token.value)) {
            token.type.push_back(TokenKind::NUMBER);
        }
        if (token.value.char32At(0) == '\'') {
            token.type.push_back(TokenKind::STRING);
        }
        if (nnvm::Language::Language::isUnicodeItemName(token.value)
                || token.value == USS("#") || token.value == USS("&") || token.value == USS("$")
                || token.value == USS("+") || token.value == USS("-") || token.value == USS("*")) {
            token.type.push_back(TokenKind::LITERAL);
        }
        if (token.type.size() == 0) {
            // special case of template configuration on block call
            if (token.value.char32At(0) == USS("[").char32At(0) && token.value.char32At(token.value.countChar32() - 1) == USS("]").char32At(0)) {
                if (!parseTemplate(token.value, token)) {
                    throw ByteCodeParserException(_i("err.parser.parserloop"), token.value, 0, 0);
                }
            }
            else {
                throw ByteCodeParserException(_i("err.parser.parserloop"), token.value, 0, 0);
            }
        }
    }

    void DefinitionBuilder::nextToken(std::vector<Token<TokenKind>>& result)
    {
        if (result.empty() || result.back().value.length() > 0) {
            if (!result.empty()) {
                fillTokenType(result.back());
            }
            result.push_back(Token<TokenKind>(ctx->byteLoader));
            result.back().value = USS("");
        }
    }

    void DefinitionBuilder::dumpTokens(std::vector<Token<TokenKind>>& tokens, int64_t level)
    {
        for (auto i = tokens.begin(); i != tokens.end(); ++i) {
            for (auto k = 0; k < level; ++k) {
                std::cout << "  ";
            }
            std::cout << StringConvUtils::utou8(i->value) << " (";
            for (auto k = i->type.begin(); k != i->type.end(); ++k) {
                std::cout << StringConvUtils::utou8(tokenTypeString(*k)) << ", ";
            }
            std::cout << ")";
            if (i->subTokens.size()) {
                std::cout << ":" << std::endl;
                dumpTokens(i->subTokens, level + 1);
            }
            else {
                std::cout << std::endl;
            }
        }
    }

    auto readCharacter = StringMachine<int64_t, std::vector<Token<TokenKind>>>::Readers::readChar().next;
    auto alwaysDefTrue = DefinitionBuilder::DefinitionState::Matches::alwaysTrue().next;

    int8_t DefinitionBuilder::saveToken(std::vector<UChar32>& data, int64_t& size, std::shared_ptr<int64_t>& carried, std::shared_ptr<std::vector<Token<TokenKind>>>& result)
    {
        if (size == 0) {
            return 1;
        }
        if (!carried) {
            carried = std::make_shared<int64_t>(0);
        }
        if (!result) {
            result = std::make_shared<std::vector<Token<TokenKind>>>();
            DefinitionBuilder::nextToken(*result);
        }
        std::vector<UChar32> symbols { '=', ',', '(', ')', '+', '-', '*', '/', '>', '<', '!', '@', ':', '\\' };
        auto special = (std::find(symbols.begin(), symbols.end(), data[0]) != symbols.end()
                   || std::find(symbols.begin(), symbols.end(), result->back().value.char32At(0)) != symbols.end());
        if (data[0] == '[') {
            if (special) {
                DefinitionBuilder::nextToken(*result);
            }
            (*carried)++;
        }
        if (data[0] == ']') {
            if (!*carried) {
                return 0;
            }
            (*carried)--;
        }
        auto us = StringConvUtils::asUnicodeString(data.begin(), data.end());
        if (*carried) {
            result->back().value += us;
        }
        else {
            std::vector<uint8_t> w(data.begin(), data.end());
            if (ByteCodeState::isWSpace().next(w, 1) > 0) {
                DefinitionBuilder::nextToken(*result);
            }
            else {
                if (special) {
                    DefinitionBuilder::nextToken(*result);
                }
                result->back().value += us;
            }
        }
        return 1;
    };

    DefinitionBuilder::DefinitionState::stateStepper stayDef =
    [](std::vector<UChar32>& data, int64_t size, std::shared_ptr<int64_t> c, int16_t& state) -> int8_t { if (!size) { state++; } return 1; };

    DefinitionBuilder::DefinitionState::stateError defError =
    [](std::vector<UChar32>& data, int16_t state, int64_t symbol) -> icu::UnicodeString { return _i("err.name.invalid.character"); };

    std::vector<DefinitionBuilder::DefinitionState> DefinitionBuilder::defStates()
    {
        return std::vector<DefinitionBuilder::DefinitionState> {
            { readCharacter, alwaysDefTrue, std::bind(&DefinitionBuilder::saveToken, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4), stayDef, defError, 1 },
        };
    }

    int8_t DefinitionBuilder::isLiteral(const Operand& operand)
    {
        return operand.literal && operand.literal->isOf(TokenKind::LITERAL);
    }

    int8_t DefinitionBuilder::isNumber(const Operand& operand)
    {
        return isLiteral(operand) && (operand.literal->isOf(TokenKind::NUMBER) || operand.literal->isOf(TokenKind::AS_NUMBER));
    }

    std::vector<Token<TokenKind>> DefinitionBuilder::combineLeafs(Token<TokenKind>& token)
    {
        if (token.subTokens.size()) {
            std::vector<Token<TokenKind>> res;
            for (auto i = token.subTokens.begin(); i != token.subTokens.end(); ++i) {
                auto t = combineLeafs(*i);
                for (auto k = t.begin(); k != t.end(); ++k) {
                    res.push_back(*k);
                }
            }
            return res;
        }
        
        return std::vector<Token<TokenKind>> {token};
    }

    int8_t DefinitionBuilder::incAndReturn(int8_t res, int64_t sourceHeight, int64_t& targetHeight)
    {
        targetHeight = sourceHeight + 1;
        return res;
    }

    int8_t DefinitionBuilder::asTree(Token<TokenKind>& token, std::vector<Token<TokenKind>>& rawTokens, Operand& operand, std::deque<LanguageDefinition>& container, std::map<icu::UnicodeString, std::tuple<int64_t, int64_t> > &ranges, std::deque<icu::UnicodeString> &templateNames)
    {
        return asTree(token, rawTokens, operand, container, ranges, templateNames, false);
    }

    int8_t DefinitionBuilder::asTree(Token<TokenKind>& token, std::vector<Token<TokenKind>>& rawTokens, Operand& operand, std::deque<LanguageDefinition>& container, std::map<icu::UnicodeString, std::tuple<int64_t, int64_t> > &ranges, std::deque<icu::UnicodeString> &templateNames, int8_t atRoot)
    {
        if (atRoot) {
            // make current token as call argument
            if (!token.isOf(TokenKind::CALL)) {
                Token<TokenKind> n(token);
                
                token.type.clear();
                token.type.push_back(TokenKind::CALL);
                
                Token<TokenKind> bt(ctx->byteLoader);
                bt.value = EXPRESSION_BLOCK;
                bt.type.push_back(TokenKind::LITERAL);
                Token<TokenKind> at(ctx->byteLoader);
                at.type.push_back(TokenKind::ARGS);
                at.subTokens.push_back(n);
                
                token.subTokens.clear();
                token.subTokens.push_back(bt);
                token.subTokens.push_back(at);
            }
        }
        
        if (token.isOf(TokenKind::LITERAL)) {
            // ( expression )
            if (token.subTokens.size()) {
                if (token.subTokens.size() == 3 && token.subTokens[0].isOf(TokenKind::LBRACKET) && token.subTokens[2].isOf(TokenKind::RBRACKET)) {
                    std::vector<Token<TokenKind>> sraw(rawTokens.begin() + 1, rawTokens.begin() + rawTokens.size() - 1);
                    return asTree(token.subTokens[1], sraw, operand, container, ranges, templateNames);
                }
            }
            // A
            else {
                operand.literal = std::make_shared<Token<TokenKind>>(token);
                return 1;
            }
        }
        if (token.isOf(TokenKind::KEY_ARG) && token.subTokens.size() == 3) {
            operand.expression = std::make_shared<ExpressionTree>(ctx->byteLoader);
            operand.expression->left = std::make_shared<Operand>(ctx->byteLoader);
            operand.expression->middle = token.subTokens[1].value;
            operand.expression->right = std::make_shared<Operand>(ctx->byteLoader);
            if (!token.subTokens[0].isOf(TokenKind::LITERAL)) {
                return 0;
            }
            operand.expression->left->literal = std::make_shared<Token<TokenKind>>(token.subTokens[0]);
            auto right_leafs = combineLeafs(token.subTokens[2]);
            return asTree(token.subTokens[2], right_leafs, *operand.expression->right, container, ranges, templateNames);
        }
        if (token.isOf(TokenKind::EXPRESSION) && token.subTokens.size() == 3) {
            operand.expression = std::make_shared<ExpressionTree>(ctx->byteLoader);
            operand.expression->left = std::make_shared<Operand>(ctx->byteLoader);
            operand.expression->middle = token.subTokens[1].value;
            operand.expression->right = std::make_shared<Operand>(ctx->byteLoader);
            auto left_leafs = combineLeafs(token.subTokens[0]);
            auto right_leafs = combineLeafs(token.subTokens[2]);
            return asTree(token.subTokens[0], left_leafs, *operand.expression->left, container, ranges, templateNames)
            && asTree(token.subTokens[2], right_leafs, *operand.expression->right, container, ranges, templateNames);
        }
        if (token.isOf(TokenKind::CALL)) {
            // with extractor or without
            // 0 1 2     3 4       5
            // d @ block \ [i , k] (a , b , c)
            // 0 1 2     3 4
            // d @ block \ [i , k]
            // 0 1 2     3
            // d @ block (a , b , c)
            // 0 1 2
            // d @ block
            // 0     1 2       3
            // block \ [i , k] (a , b , c)
            // 0     1 2
            // block \ [i , k]
            // 0     1
            // block (a , b , c)
            // 0
            // block
            int64_t nameOffset = 0;
            int64_t argOffset = -1;
            int64_t tempOffset = -1;
            if (token.subTokens.size() > 6 || token.subTokens.size() < 1) {
                return 0;
            }
            switch (token.subTokens.size()) {
                case 6:
                {
                    nameOffset = 2;
                    argOffset = 5;
                    tempOffset = 4;
                    break;
                }
                case 5:
                {
                    nameOffset = 2;
                    tempOffset = 4;
                    break;
                }
                case 4:
                {
                    if (token.subTokens[1].isOf(TokenKind::FROM)) {
                        nameOffset = 2;
                    }
                    else {
                        nameOffset = 0;
                        argOffset = 3;
                        tempOffset = 2;
                    }
                    break;
                }
                case 3:
                {
                    if (token.subTokens[1].isOf(TokenKind::FROM)) {
                        nameOffset = 2;
                        argOffset = 3;
                    }
                    else {
                        nameOffset = 0;
                        tempOffset = 2;
                    }
                    break;
                }
                case 2:
                {
                    nameOffset = 0;
                    argOffset = 1;
                    break;
                }
                case 1:
                {
                    nameOffset = 0;
                    break;
                }
                default:
                    break;
            }
            operand.call = std::make_shared<LanguageCall>(ctx->byteLoader);
            if (nameOffset > 0) {
                operand.call->extractedName = token.subTokens[0].value;
            }
            if (tempOffset >= 0) {
                if (!token.subTokens[tempOffset].isOf(TokenKind::TEMPLATER_CONTENT)) {
                    return 0;
                }
                if (token.subTokens[tempOffset].subTokens.size() > 0 && !token.subTokens[tempOffset].subTokens[0].isOf(TokenKind::ARGS)) {
                    // was the only argument
                    if (token.subTokens[tempOffset].subTokens[0].isOf(TokenKind::KEY_ARG)) {
                        auto st = token.subTokens[tempOffset].subTokens[0];
                        auto nt = Token<TokenKind>(ctx->byteLoader);
                        nt.type.clear();
                        nt.type.push_back(TokenKind::ARGS);
                        nt.subTokens.push_back(st);
                        token.subTokens[tempOffset].subTokens[0] = nt;
                    }
                    else {
                        return 0;
                    }
                }
            }
            if (argOffset >= 0 && !token.subTokens[argOffset].isOf(TokenKind::ARGS)) {
                // was the only argument
                if (token.subTokens[argOffset].isOf(TokenKind::EXPRESSION) || token.subTokens[argOffset].isOf(TokenKind::LITERAL) || token.subTokens[argOffset].isOf(TokenKind::KEY_ARG)) {
                    auto st = token.subTokens[argOffset];
                    auto nt = Token<TokenKind>(ctx->byteLoader);
                    nt.type.clear();
                    nt.type.push_back(TokenKind::ARGS);
                    nt.subTokens.push_back(st);
                    token.subTokens[argOffset] = nt;
                }
                else {
                    return 0;
                }
            }
            operand.call->rawTokens.push_back(std::deque<Token<TokenKind>>(rawTokens.begin(), rawTokens.end()));
            operand.call->blockName = token.subTokens[nameOffset].value;
            
            auto alias = ctx->setup->getByteCodeParserSetup()->aliases.find(operand.call->blockName);
            // # *** => additive ***
            if (alias != ctx->setup->getByteCodeParserSetup()->aliases.end()) {
                operand.call->blockName = alias->second;
            }
            
            auto neuron = ctx->getNeuron(operand.call->blockName);
            // sgl.function.block ***
            if (neuron == ctx->setup->getByteCodeParserSetup()->neurons.end()) {
                if (argOffset < 0) {
                    std::cout << StringConvUtils::utou8(operand.call->blockName) << "|" << std::endl;
                }
                
                operand.call->blockName = ctx->setup->getByteCodeParserSetup()->libraryNeuron;
                
                // arguments
                // ,
                if (argOffset >= 0) {
                    token.subTokens[argOffset].subTokens.insert(token.subTokens[argOffset].subTokens.begin(), Token<TokenKind>(ctx->byteLoader));
                    token.subTokens[argOffset].subTokens[0].type.push_back(TokenKind::ARG_DELIMITER);
                    token.subTokens[argOffset].subTokens[0].value = USS(",");
                    
                    // 'blockname'
                    token.subTokens[argOffset].subTokens.insert(token.subTokens[argOffset].subTokens.begin(), Token<TokenKind>(ctx->byteLoader));
                    token.subTokens[argOffset].subTokens[0].type.push_back(TokenKind::STRING);
                    token.subTokens[argOffset].subTokens[0].value = USS("'") + token.subTokens[nameOffset].value + USS("'");
                }
                
                // call name
                // blockcall
                token.subTokens[nameOffset].value = ctx->setup->getByteCodeParserSetup()->libraryNeuron;
                
                neuron = ctx->getNeuron(operand.call->blockName);
                if (neuron == ctx->setup->getByteCodeParserSetup()->neurons.end()) {
                    return 0;
                }
            }
            
            // flat ARGS
            if (argOffset >= 0 && token.subTokens[argOffset].subTokens.size()) {
                ctx->setup->getLogger()->debug(USS("Flattening arguments of call: ") + nnvm::LanguageModel::toString(token));
                auto point = token.subTokens[argOffset].subTokens.begin();
                while (point != token.subTokens[argOffset].subTokens.end()) {
                    if (point->isOf(TokenKind::ARGS) && !(point->isOf(TokenKind::CALL) || point->isOf(TokenKind::EXPRESSION) || point->isOf(TokenKind::LITERAL) || point->isOf(TokenKind::KEY_ARG))) {
                        auto t = *point;
                        point = token.subTokens[argOffset].subTokens.erase(point);
                        for (auto k = t.subTokens.rbegin(); k != t.subTokens.rend(); ++k) {
                            point = token.subTokens[argOffset].subTokens.insert(point, *k);
                        }
                    }
                    else {
                        ++point;
                    }
                }
            }
            
            // flat TEMPLATE ARGS
            if (tempOffset >= 0 && token.subTokens[tempOffset].subTokens[0].subTokens.size()) {
                ctx->setup->getLogger()->debug(USS("Flattening template arguments of call: ") + nnvm::LanguageModel::toString(token));
                auto point = token.subTokens[tempOffset].subTokens[0].subTokens.begin();
                while (point != token.subTokens[tempOffset].subTokens[0].subTokens.end()) {
                    if (point->isOf(TokenKind::ARGS) && !(point->isOf(TokenKind::CALL) || point->isOf(TokenKind::EXPRESSION) || point->isOf(TokenKind::LITERAL) || point->isOf(TokenKind::KEY_ARG))) {
                        auto t = *point;
                        point = token.subTokens[tempOffset].subTokens[0].subTokens.erase(point);
                        for (auto k = t.subTokens.rbegin(); k != t.subTokens.rend(); ++k) {
                            point = token.subTokens[tempOffset].subTokens[0].subTokens.insert(point, *k);
                        }
                    }
                    else {
                        ++point;
                    }
                }
            }
            
            std::vector<Token<TokenKind>> subTokens;
            std::vector<Token<TokenKind>> templateSubTokens;

            ctx->setup->getLogger()->debug(USS("Flattened tokens prepared of call: ") + nnvm::LanguageModel::toString(token));
            return parseCall(argOffset >= 0 ? token.subTokens[argOffset].subTokens : subTokens, tempOffset >= 0 ? token.subTokens[tempOffset].subTokens[0].subTokens : templateSubTokens, neuron->second, operand, container, ranges, templateNames);
        }
        return 0;
    }

    int8_t DefinitionBuilder::asTemplateTree(Token<TokenKind>& token, std::vector<Token<TokenKind> >& rawTokens, LanguageCall& languageCall, std::deque<LanguageDefinition>& container)
    {
        if (token.isOf(TokenKind::KEY_ARG) && token.subTokens.size() == 3) {
            LanguageArgument a(ctx->byteLoader);
            a.operand.expression = std::make_shared<ExpressionTree>(ctx->byteLoader);
            a.operand.expression->left = std::make_shared<Operand>(ctx->byteLoader);
            a.operand.expression->middle = token.subTokens[1].value;
            a.operand.expression->right = std::make_shared<Operand>(ctx->byteLoader);
            languageCall.templateArguments.push_back(a);
            if (!token.subTokens[0].isOf(TokenKind::LITERAL)) {
                return 0;
            }
            a.operand.expression->left->literal = std::make_shared<Token<TokenKind> >(token.subTokens[0]);
            auto rightLeafs = combineLeafs(token.subTokens[2]);
            std::map<icu::UnicodeString, std::tuple<int64_t, int64_t> > ranges;
            std::deque<icu::UnicodeString> templateNames;
            return asTree(token.subTokens[2], rightLeafs, *a.operand.expression->right, container, ranges, templateNames);
        }
        return 0;
    }

    icu::UnicodeString DefinitionBuilder::aggregateName(const Operand& operand)
    {
        icu::UnicodeString res("");
        if (operand.call) {
            res += operand.call->blockName;

            for (auto i = operand.call->arguments.begin(); i != operand.call->arguments.end(); ++i) {
                if (i->rawTokens && i->rawTokens->size()) {
                    for (auto k = i->rawTokens->begin(); k != i->rawTokens->end(); ++k) {
                        if (res.countChar32() > 0) {
                            res += USS("__");
                        }
                        res += nnvm::Language::Language::cleanName(k->value);
                    }
                }
                else {
                    if (res.countChar32() > 0) {
                        res += USS("__");
                    }
                    res += aggregateName(i->operand);
                }
            }
        }
        else {
            if (operand.literal) {
                res += nnvm::Language::Language::cleanName(operand.literal->value);
            }
            else {
                if (operand.expression) {
                    res += aggregateName(*operand.expression->left) + USS("__") + aggregateName(*operand.expression->right);
                }
            }
        }
        return res;
    }

    icu::UnicodeString DefinitionBuilder::indexedName(LanguageCall& call, std::map<icu::UnicodeString, std::tuple<int64_t, int64_t>> &ranges)
    {
        auto res = USS("");
        auto indexes = nnvm::Language::Language::findTemplateIndexes(ranges, call);
        for (auto i = indexes.begin(); i != indexes.end(); ++i) {
            res += StringUtils::spf(USS("[{0}]"), StringConvUtils::utow(*i));
        }
        return res;
    }

    icu::UnicodeString DefinitionBuilder::indexedName(Operand& operand, std::map<icu::UnicodeString, std::tuple<int64_t, int64_t>> &ranges)
    {
        auto res = USS("");
        auto indexes = nnvm::Language::Language::findTemplateIndexes(ranges, operand);
        for (auto i = indexes.begin(); i != indexes.end(); ++i) {
            res += StringUtils::spf(USS("[{0}]"), StringConvUtils::utow(*i));
        }
        return res;
    }

    int8_t DefinitionBuilder::prepareCall(std::shared_ptr<ParsedNeuron> neuron, Operand& operand, std::deque<LanguageDefinition>& container, std::map<icu::UnicodeString, std::tuple<int64_t, int64_t>> &ranges, std::deque<icu::UnicodeString> &templateNames, int64_t& height)
    {
        if (!operand.call) {
            return 0;
        }
        
        ctx->setup->getLogger()->debug(USS("Setting call definition for ") + nnvm::LanguageModel::toString(operand, 1));
        
        int8_t res = 1;
        for (auto i = 0; i < operand.call->templateArguments.size(); ++i) {
            auto a = &operand.call->templateArguments[i];
            if (a->operand.expression && a->operand.expression->middle == USS(":") && a->operand.expression->left && a->operand.expression->left->literal && a->operand.expression->right) {
                if (!a->operand.expression->right->literal) {
                    int64_t h;
                    res &= separateExpressionDefinition(*a->operand.expression->right, container, ranges, templateNames, h);
                }
            }
            else {
                res &= 0;
            }
        }

        if (ctx->setup->getByteCodeParserSetup()->arithmeticNeuronNames.find(operand.call->blockName) != ctx->setup->getByteCodeParserSetup()->arithmeticNeuronNames.end()) {
            if (!simplifyArithmericBlock(*operand.call, container, ranges, templateNames, height)) {
                return 0;
            }
            if (operand.call->arguments.size() == 1) {
                auto tmp = operand.call->arguments[0].operand;
                operand = tmp;
            }
            return 1;
        }
        
        std::vector<int64_t> heights;
        for (auto i = 0; i < operand.call->arguments.size(); ++i) {
            int64_t h = 0;
            auto a = &operand.call->arguments[i];
            auto tp = neuron->argumentType(i, (int64_t) operand.call->arguments.size());
            switch (tp) {
                case ParsedNeuronArgumentType::RAW:
                case ParsedNeuronArgumentType::TREE:
                {
                    break;
                }
                case ParsedNeuronArgumentType::REQUIRED_SINGLE:
                {
                    if (!a->operand.literal) {
                        res &= 0;
                    }
                    break;
                }
                case ParsedNeuronArgumentType::FLAT:
                {
                    if (a->operand.call) {
                        res &= simplifyExpression(a->operand, container, ranges, templateNames, h);
                    }
                    break;
                }
                case ParsedNeuronArgumentType::FLAT_CONDITIONAL:
                {
                    if (!a->operand.expression || a->operand.expression->middle.length() == 0 ||
                        (a->operand.expression->middle != USS(">=") && a->operand.expression->middle != USS("<=")
                         && a->operand.expression->middle != USS("==") && a->operand.expression->middle != USS("!=")
                         && a->operand.expression->middle != USS(">") && a->operand.expression->middle != USS("<"))) {
                            auto tmp = a->operand;
                            a->operand.expression = std::make_shared<ExpressionTree>(ctx->byteLoader);
                            a->operand.expression->left = std::make_shared<Operand>(tmp);
                            a->operand.expression->middle = USS(">=");
                            a->operand.expression->right = std::make_shared<Operand>(ctx->byteLoader);
                            Token<TokenKind> t(ctx->byteLoader);
                            t.value = USS("1");
                            t.type.push_back(TokenKind::LITERAL);
                            t.type.push_back(TokenKind::NUMBER);
                            a->operand.expression->right->literal = std::make_shared<Token<TokenKind>>(t);
                    }
                    // if right of conditinal is not number
                    if (!a->operand.expression->right->literal || (!a->operand.expression->right->literal->isOf(TokenKind::NUMBER) && !a->operand.expression->right->literal->isOf(TokenKind::AS_NUMBER))) {
                        if (a->operand.expression->left->literal) {
                            // move it to left with minus creating a call
                            auto tmp = a->operand.expression->left->literal;
                            a->operand.expression->left->literal = nullptr;
                            a->operand.expression->left->call = std::make_shared<LanguageCall>(ctx->byteLoader);
                            a->operand.expression->left->call->blockName = NeuronNames::PLUS;
                            // A, B -> (+ A -B), 0
                            LanguageArgument a1(ctx->byteLoader);
                            a1.operand.literal = tmp;
                            a->operand.expression->left->call->arguments.push_back(a1);
                            LanguageArgument a2(ctx->byteLoader);
                            if (a->operand.expression->right->literal && a->operand.expression->right->literal->isOf(TokenKind::NUMBER)) {
                                a2.operand = *a->operand.expression->right;
                                a2.operand.literal->value = ByteCodeUtils::asString(-1 * ByteCodeUtils::asNumber(a2.operand.literal->value));
                            }
                            else {
                                a2.operand.call = std::make_shared<LanguageCall>(ctx->byteLoader);
                                a2.operand.call->blockName = NeuronNames::MULTIPLY;
                                LanguageArgument a3(ctx->byteLoader);
                                a3.operand = *a->operand.expression->right;
                                a2.operand.call->arguments.push_back(a3);
                                LanguageArgument a4(ctx->byteLoader);
                                a4.operand.literal = std::make_shared<Token<TokenKind>>(ctx->byteLoader);
                                a4.operand.literal->type.push_back(TokenKind::NUMBER);
                                a4.operand.literal->type.push_back(TokenKind::LITERAL);
                                a4.operand.literal->value = USS("-1");
                                a2.operand.call->arguments.push_back(a4);
                            }
                            a->operand.expression->left->call->arguments.push_back(a2);
                        }
                        else {
                            // move it to left with minus adding to existing call
                            LanguageArgument a1(ctx->byteLoader);
                            a1.operand.call = std::make_shared<LanguageCall>(ctx->byteLoader);
                            a1.operand.call->blockName = NeuronNames::MULTIPLY;

                            LanguageArgument a2(ctx->byteLoader);
                            a2.operand.literal = std::make_shared<Token<TokenKind>>(ctx->byteLoader);
                            a2.operand.literal->type.push_back(TokenKind::NUMBER);
                            a2.operand.literal->type.push_back(TokenKind::LITERAL);
                            a2.operand.literal->value = USS("-1");
                            a1.operand.call->arguments.push_back(a2);
                            LanguageArgument a3(ctx->byteLoader);
                            a3.operand = *a->operand.expression->right;
                            a1.operand.call->arguments.push_back(a3);
                            a->operand.expression->left->call->arguments.push_back(a1);
                        }

                        a->operand.expression->right = std::make_shared<Operand>(ctx->byteLoader);
                        auto t = std::make_shared<Token<TokenKind>>(ctx->byteLoader);
                        t->value = USS("0");
                        t->type.push_back(TokenKind::LITERAL);
                        t->type.push_back(TokenKind::NUMBER);
                        a->operand.expression->right->literal = t;
                    }

                    res &= simplifyExpression(*a->operand.expression->left, container, ranges, templateNames, h);
                    break;
                }
                case ParsedNeuronArgumentType::SINGLE:
                {
                    if (!a->operand.literal) {
                        res &= separateExpressionDefinition(a->operand, container, ranges, templateNames, h);
                    }
                    break;
                }
                case ParsedNeuronArgumentType::KEY:
                {
                    if (a->operand.expression && a->operand.expression->middle == USS(":") && a->operand.expression->left && a->operand.expression->left->literal && a->operand.expression->right) {
                        if (!a->operand.expression->right->literal) {
                            res &= separateExpressionDefinition(*a->operand.expression->right, container, ranges, templateNames, h);
                        }
                    }
                    else {
                        res &= 0;
                    }
                    break;
                }
                case ParsedNeuronArgumentType::FAIL:
                default:
                    res &= 0;
                    break;
            }
            heights.push_back(h);
        }
        
        ctx->setup->getLogger()->debug(USS("Heights of ") + nnvm::LanguageModel::toString(operand, 1));
        for (auto i = heights.begin(); i != heights.end(); ++i) {
            ctx->setup->getLogger()->debug(StringUtils::spf(USS("{0}"), *i));
        }
        
        height = heights.size() ? ((int64_t)*std::max_element(heights.begin(), heights.end())) : 0;
        
        balanceHeights(*operand.call, heights, container, ranges, templateNames);
        
        return res;
    }

    void DefinitionBuilder::balanceHeights(LanguageCall& call, const std::vector<int64_t>& heights, std::deque<LanguageDefinition>& container, std::map<icu::UnicodeString, std::tuple<int64_t, int64_t> > &ranges, std::deque<icu::UnicodeString> &templateNames)
    {
        if (heights.size()) {
            auto height = (int64_t)*std::max_element(heights.begin(), heights.end());
            for (auto i = 0; i < heights.size(); ++i) {
                auto diff = height - heights[i];
                auto isConstant = call.arguments[i].operand.literal && (call.arguments[i].operand.literal->isOf(TokenKind::NUMBER) || call.arguments[i].operand.literal->isOf(TokenKind::AS_NUMBER));
                if (diff && !isConstant) {
                    addDelay(call.arguments[i].operand, container, ranges, templateNames, diff);
                }
            }
        }
    }

    int8_t DefinitionBuilder::extractToLiteral(Operand& operand, std::deque<LanguageDefinition>& container, std::map<icu::UnicodeString, std::tuple<int64_t, int64_t> > &ranges, std::deque<icu::UnicodeString> &templateNames, int64_t& height)
    {
        // string is a special parameter. raw parameters are also ignored - not part of operand.
        if (operand.literal && operand.literal->isOf(TokenKind::STRING)) {
            return 1;
        }
        
        LanguageDefinition d(ctx->byteLoader);
        d.name = std::make_shared<icu::UnicodeString>(StringUtils::spf(USS("result__{0}{1}{2}"), StringConvUtils::utow(aggregateName(operand)), getNewIndex(), StringConvUtils::utow(indexedName(operand, ranges))));
        d.operand = operand;
        d.templateNames = templateNames;
        container.push_back(d);

        operand.expression = nullptr;
        operand.call = nullptr;
        operand.literal = std::make_shared<Token<TokenKind>>(ctx->byteLoader);
        operand.literal->type.push_back(TokenKind::LITERAL);
        operand.literal->value = *d.name;

        height = 0;
        // height of called block is always the height of arguments. internals are considered "0" as they are not yet compilable here.
        int64_t h;
        if (d.operand.call) {
            auto neuron = ctx->getNeuron(d.operand.call->blockName);
            if (neuron == ctx->setup->getByteCodeParserSetup()->neurons.end()) {
                return 0;
            }
            return incAndReturn(prepareCall(neuron->second, d.operand, container, ranges, templateNames, h), h, height);
        }
        if (d.operand.expression) {
            return incAndReturn(simplifyExpression(d.operand, container, ranges, templateNames, h), h, height);
        }
        return 1;
    }

    void DefinitionBuilder::addDelay(Operand& operand, std::deque<LanguageDefinition>& container, std::map<icu::UnicodeString, std::tuple<int64_t, int64_t> > &ranges, std::deque<icu::UnicodeString> &templateNames, int64_t height)
    {
        for (; height > 0; --height) {
            int64_t h;
            extractToLiteral(operand, container, ranges, templateNames, h);
        }
    }

    int8_t DefinitionBuilder::simplifyArithmericBlock(LanguageCall& block, std::deque<LanguageDefinition>& container, std::map<icu::UnicodeString, std::tuple<int64_t, int64_t> > &ranges, std::deque<icu::UnicodeString> &templateNames, int64_t& height)
    {
        auto res = 1;
        std::vector<int64_t> heights;
        
        // TODO: have the height for arithmetic only
        
        // simplify each argument
        for (auto i = block.arguments.begin(); i != block.arguments.end(); ++i) {
            int64_t h;
            res &= simplifyExpression(i->operand, container, ranges, templateNames, h);
            heights.push_back(h);
        }
        
        // if minus, make it plus
        if (block.blockName == NeuronNames::MINUS) {
            ctx->setup->getLogger()->debug(USS("Inversing block operation: ") + nnvm::LanguageModel::toString(block));
            block.blockName = NeuronNames::PLUS;
            
            std::function<void(Operand&, int64_t&)> make_minus = [this](Operand& operand, int64_t& h) {
                if (operand.literal) {
                    if (operand.literal->isOf(TokenKind::NUMBER) || operand.literal->isOf(TokenKind::AS_NUMBER)) {
                        operand.literal->value = ByteCodeUtils::asString(ByteCodeUtils::asNumber(operand.literal->value) * -1);
                    }
                    else {
                        LanguageArgument a(ctx->byteLoader);
                        a.operand = operand;
                        operand.call = std::make_shared<LanguageCall>(ctx->byteLoader);
                        operand.call->blockName = NeuronNames::MULTIPLY;
                        operand.call->arguments.push_back(a);
                        operand.literal = nullptr;
                        h++;
                    }
                }
                if (operand.call) {
                    LanguageArgument a(ctx->byteLoader);
                    a.operand.literal = std::make_shared<Token<TokenKind>>(ctx->byteLoader);
                    a.operand.literal->type.push_back(TokenKind::LITERAL);
                    a.operand.literal->type.push_back(TokenKind::NUMBER);
                    a.operand.literal->value = USS("-1");
                    operand.call->arguments.push_back(a);
                }
            };
            
            auto i = block.arguments.begin();
            ++i;
            auto h = heights.begin();
            ++h;
            for (;i != block.arguments.end(); ++i, ++h) {
                if (i->operand.call && (block.blockName != NeuronNames::PLUS || i->operand.call->blockName != NeuronNames::MULTIPLY)) {
                    int64_t th;
                    res &= extractToLiteral(i->operand, container, ranges, templateNames, th);
                    *h += th;
                }
                make_minus(i->operand, *h);
            }
            
            if (block.arguments.size() == 1) {
                make_minus(block.arguments[0].operand, heights[0]);
            }
        }
        
        // flatten same operation expressions
        auto h = heights.begin();
        for (auto i = block.arguments.begin(); i != block.arguments.end(); ++i, ++h) {
            if (i->operand.call && i->operand.call->blockName == block.blockName) {
                auto tmp = *i;
                i = block.arguments.erase(i);
                auto hv = *h;
                h = heights.erase(h);
                i = block.arguments.insert(i, tmp.operand.call->arguments.begin(), tmp.operand.call->arguments.end());
                for (auto k = tmp.operand.call->arguments.begin(); k != tmp.operand.call->arguments.end(); ++k) {
                    h = heights.insert(h, hv);
                }
            }
        }
        
        auto rank = [](const LanguageArgument& a) {
            if (a.operand.literal) {
                if (a.operand.literal->isOf(TokenKind::NUMBER)) {
                    return 3;
                }
                if (a.operand.literal->isOf(TokenKind::AS_NUMBER)) {
                    return 2;
                }
                return 1;
            }
            return 0;
        };
        
        // sort operands to keep literals to the right and numbers to the most right
        std::vector<LanguageArgument> spv(block.arguments.begin(), block.arguments.end());
        std::function<int64_t(const LanguageArgument&, const LanguageArgument&)> spf = [&rank] (const LanguageArgument& a, const LanguageArgument& b) { return (int64_t)(rank(a) - rank(b)); };
        auto sp = CollectionUtils::sortPermutation(spv, spf);
        spv = CollectionUtils::applyPermutation(spv, sp);
        block.arguments = std::deque<LanguageArgument>(spv.begin(), spv.end());
        heights = CollectionUtils::applyPermutation(heights, sp);
        
        // merge numbers into single literal
        double sv = block.blockName == NeuronNames::MULTIPLY ? 1 : 0;
        double v = block.blockName == NeuronNames::MULTIPLY ? 1 : 0;
        auto i = block.arguments.rbegin();
        auto rh = heights.rbegin();
        for (; i != block.arguments.rend(); ++i, ++rh) {
            if (i->operand.literal && i->operand.literal->isOf(TokenKind::NUMBER)) {
                if (block.blockName == NeuronNames::PLUS) {
                    v += ByteCodeUtils::asNumber(i->operand.literal->value);
                }
                if (block.blockName == NeuronNames::MULTIPLY) {
                    v *= ByteCodeUtils::asNumber(i->operand.literal->value);
                }
            }
            else {
                break;
            }
        }
        if (i != block.arguments.rbegin()) {
            block.arguments.erase(i.base(), block.arguments.end());
            heights.erase(rh.base(), heights.end());
        }
        
        // keep it only if significant
        if (fabs(sv - v) > DBL_EPSILON || !block.arguments.size()) {
            LanguageArgument a(ctx->byteLoader);
            a.operand.literal = std::make_shared<Token<TokenKind>>(ctx->byteLoader);
            a.operand.literal->type.push_back(TokenKind::NUMBER);
            a.operand.literal->type.push_back(TokenKind::LITERAL);
            a.operand.literal->value = ByteCodeUtils::asString(v);
            block.arguments.push_back(a);
        }
        
        // reduce MULTIPLY to just two significant arguments with only PLUS or literal
        if (block.blockName == NeuronNames::MULTIPLY) {
            res &= flatMultiply(block, heights, container, ranges, templateNames);
        }

        // reduce PLUS to just arguments with only literal or MULTIPLY with two literals
        if (block.blockName == NeuronNames::PLUS) {
            res &= flatPlus(block, heights, container, ranges, templateNames);
        }
        
        ctx->setup->getLogger()->debug(USS("Heights of ") + nnvm::LanguageModel::toString(block));
        for (auto i = heights.begin(); i != heights.end(); ++i) {
            ctx->setup->getLogger()->debug(StringUtils::spf(USS("{0}"), *i));
        }

        height = *std::max_element(heights.begin(), heights.end());
        
        balanceHeights(block, heights, container, ranges, templateNames);
        
        ctx->setup->getLogger()->debug(USS("Simplified arithmetic ") + nnvm::LanguageModel::toString(block));
        
        return (int8_t) res;
    }

    int8_t DefinitionBuilder::flatMultiply(LanguageCall& block, std::vector<int64_t>& heights, std::deque<LanguageDefinition>& container, std::map<icu::UnicodeString, std::tuple<int64_t, int64_t> > &ranges, std::deque<icu::UnicodeString> &templateNames)
    {
        // only two operands
        if (block.arguments.size() > 2) {
            std::deque<LanguageArgument> extra(++block.arguments.begin(), block.arguments.end());
            block.arguments.erase(++block.arguments.begin(), block.arguments.end());
            LanguageArgument a(ctx->byteLoader);
            a.operand.call = std::make_shared<LanguageCall>(ctx->byteLoader);
            a.operand.call->blockName = NeuronNames::MULTIPLY;
            a.operand.call->arguments = extra;
            block.arguments.push_back(a);
        }

        int8_t res = 1;
        auto h = heights.begin();
        
        for (auto i = block.arguments.begin(); i != block.arguments.end(); ++i, ++h) {
            if (i->operand.call) {
                // other calls to literal
                if (i->operand.call->blockName != NeuronNames::PLUS) {
                    int64_t th;
                    res &= extractToLiteral(i->operand, container, ranges, templateNames, th);
                    *h += th;
                }
                // PLUS call standard simple
                else {
                    std::vector<int64_t> th(i->operand.call->arguments.size());
                    res &= flatPlus(*i->operand.call, th, container, ranges, templateNames);
                    *h += *std::max_element(th.begin(), th.end());
                }
            }
        }
        return res;
    }

    int8_t DefinitionBuilder::flatPlus(LanguageCall& block, std::vector<int64_t>& heights, std::deque<LanguageDefinition>& container, std::map<icu::UnicodeString, std::tuple<int64_t, int64_t> > &ranges, std::deque<icu::UnicodeString> &templateNames)
    {
        int8_t res = 1;
        auto h = heights.begin();
        for (auto i = block.arguments.begin(); i != block.arguments.end(); ++i, ++h) {
            if (i->operand.call) {
                // other calls to literal
                if (i->operand.call->blockName != NeuronNames::MULTIPLY) {
                    int64_t th;
                    res &= extractToLiteral(i->operand, container, ranges, templateNames, th);
                    *h += th;
                }
                // MULTIPLY call only with two literal arguments
                else {
                    if (i->operand.call->arguments.size() > 2) {
                        std::deque<LanguageArgument> extra(++i->operand.call->arguments.begin(), i->operand.call->arguments.end());
                        i->operand.call->arguments.erase(++i->operand.call->arguments.begin(), i->operand.call->arguments.end());
                        LanguageArgument a(ctx->byteLoader);
                        a.operand.call = std::make_shared<LanguageCall>(ctx->byteLoader);
                        a.operand.call->blockName = NeuronNames::MULTIPLY;
                        a.operand.call->arguments = extra;
                        i->operand.call->arguments.push_back(a);
                    }
                    std::vector<int64_t> theights;
                    for (auto k = i->operand.call->arguments.begin(); k != i->operand.call->arguments.end(); ++k) {
                        if (!k->operand.literal) {
                            int64_t th;
                            res &= extractToLiteral(k->operand, container, ranges, templateNames, th);
                            theights.push_back(th);
                        }
                    }
                    if (theights.size()) {
                        *h += *std::max_element(theights.begin(), theights.end());
                    }
                }
            }
        }
        return res;
    }

    int8_t DefinitionBuilder::simplifyExpression(Operand& operand, std::deque<LanguageDefinition>& container, std::map<icu::UnicodeString, std::tuple<int64_t, int64_t> > &ranges, std::deque<icu::UnicodeString> &templateNames, int64_t& height)
    {
        // height rules:
        // 1. from literal to current - 1 edge, literal give height 0, current gives ++
        // 2. from runtime block to current - 1 edge, block give height 0, current gives ++
        // 3. from multiplication to current - 1 edge, multiplication give height 1, current gives ++

        std::map<icu::UnicodeString, double> context;
        nnvm::Language::Language::markAsNumberConstant(operand, templateNames, ranges, context, ctx);

        // literal is already simple
        if (operand.literal) {
            height = 0;
            return 1;
        }
        // blockcall needs simplification
        if (operand.call) {
            auto a = ctx->setup->getByteCodeParserSetup()->aliases.find(operand.call->blockName);
            if (a != ctx->setup->getByteCodeParserSetup()->aliases.end()) {
                operand.call->blockName = a->second;
            }
            
            // if blockcall is an arithmetic operation, then simplify them as expression
            if (ctx->setup->getByteCodeParserSetup()->arithmeticNeuronNames.find(operand.call->blockName) != ctx->setup->getByteCodeParserSetup()->arithmeticNeuronNames.end()) {
                if (!simplifyArithmericBlock(*operand.call, container, ranges, templateNames, height)) {
                    return 0;
                }
                ctx->setup->getLogger()->debug(USS("Height of ") + nnvm::LanguageModel::toString(operand, 1) + StringUtils::spf(USS(" {0}"), height));
                if (operand.call->arguments.size() == 1) {
                    auto tmp = operand.call->arguments[0].operand;
                    operand = tmp;
                }
                return 1;
            }
            
            int64_t h;
            return incAndReturn(extractToLiteral(operand, container, ranges, templateNames, h), h, height);
        }
        if (operand.expression) {
            // check flat expression
            if (operand.expression->middle == USS("+")) {
                // ! working with raw pointers, shouldn't leave scope of this if block
                std::vector<std::tuple<Operand*, int64_t>> sums;

                std::deque<Operand*> queue;
                queue.push_back(&operand);
                
                auto res = 1;

                while (queue.size()) {
                    auto i = queue[0];
                    queue.pop_front();
                    int64_t h;
                    if (i->expression->middle == USS("+")) {
                        if (i->expression->left->call) {
                            res &= extractToLiteral(*i->expression->left, container, ranges, templateNames, h);
                            sums.push_back(std::tuple<Operand*, int64_t>(i->expression->left.get(), h));
                        }
                        else {
                            if (i->expression->left->expression) {
                                queue.push_back(i->expression->left.get());
                            }
                            else {
                                sums.push_back(std::tuple<Operand*, int64_t>(i->expression->left.get(), 0));
                            }
                        }
                        if (i->expression->right->call) {
                            res &= extractToLiteral(*i->expression->right, container, ranges, templateNames, h);
                            sums.push_back(std::tuple<Operand*, int64_t>(i->expression->right.get(), h));
                        }
                        else {
                            if (i->expression->right->expression) {
                                queue.push_back(i->expression->right.get());
                            }
                            else {
                                sums.push_back(std::tuple<Operand*, int64_t>(i->expression->right.get(), 0));
                            }
                        }
                    }
                    else {
                        res &= simplifyExpression(*i, container, ranges, templateNames, h);
                        sums.push_back(std::tuple<Operand*, int64_t>(i, h));
                    }
                }
                
                ctx->setup->getLogger()->debug(USS("Heights of ") + nnvm::LanguageModel::toString(operand, 1));
                for (auto i = sums.begin(); i != sums.end(); ++i) {
                    ctx->setup->getLogger()->debug(StringUtils::spf(USS("{0}"), std::get<1>(*i)));
                }

                height = std::get<1>(*std::max_element(sums.begin(), sums.end(), [](std::tuple<Operand*, int64_t> a, std::tuple<Operand*, int64_t> b) { return std::get<1>(a) < std::get<1>(b); }));
                
                for (auto i = sums.begin(); i != sums.end(); ++i) {
                    auto h = std::get<1>(*i);
                    if (h < height) {
                        addDelay(*std::get<0>(*i), container, ranges, templateNames, height - h);
                    }
                }
                
                return (int8_t) res;
            }
            else {
                auto res = 1;
                int64_t lefth = 0;
                int64_t righth = 0;
                if (!operand.expression->right->literal) {
                    res &= extractToLiteral(*operand.expression->right, container, ranges, templateNames, righth);
                }
                if (!operand.expression->left->literal) {
                    res &= extractToLiteral(*operand.expression->left, container, ranges, templateNames, lefth);
                }
                if (lefth > 0 && righth > 0 && lefth != righth) {
                    if (lefth > righth) {
                        addDelay(*operand.expression->right, container, ranges, templateNames, lefth - righth);
                    }
                    if (righth > lefth) {
                        addDelay(*operand.expression->left, container, ranges, templateNames, righth - lefth);
                    }
                }
                height = std::max(lefth, righth);
                if (!isNumber(*operand.expression->left) && !isNumber(*operand.expression->right)) {
                    // multiplication through &
                    LanguageCall tc(ctx->byteLoader);
                    tc.blockName = ctx->setup->getByteCodeParserSetup()->multiplicationNeuron;
                    // & 0, B, A
                    if (operand.expression->left->literal->isOf(TokenKind::NUMBER) || operand.expression->left->literal->isOf(TokenKind::AS_NUMBER)) {
                        LanguageArgument a(ctx->byteLoader);
                        a.operand.literal = std::make_shared<Token<TokenKind>>(ctx->byteLoader);
                        a.operand.literal->type.push_back(TokenKind::LITERAL);
                        a.operand.literal->type.push_back(TokenKind::NUMBER);
                        a.operand.literal->value = USS("0");
                        tc.arguments.push_back(a);
                        LanguageArgument a2(ctx->byteLoader);
                        a2.operand.literal = operand.expression->right->literal;
                        tc.arguments.push_back(a2);
                        LanguageArgument a3(ctx->byteLoader);
                        a3.operand.literal = operand.expression->left->literal;
                        tc.arguments.push_back(a3);
                    }
                    // & A, B
                    else {
                        auto indexedName = USS("");
                        auto indexes = nnvm::Language::Language::findTemplateIndexes(ranges, *operand.expression->left);
                        for (auto i = indexes.begin(); i != indexes.end(); ++i) {
                            indexedName += StringUtils::spf(USS("[{0}]"), StringConvUtils::utow(*i));
                        }
                        
                        // minus
                        LanguageDefinition d(ctx->byteLoader);
                        d.name = std::make_shared<icu::UnicodeString>(USS("minus__") + operand.expression->left->literal->value + indexedName);
                        d.operand.call = std::make_shared<LanguageCall>(ctx->byteLoader);
                        
                        LanguageArgument a(ctx->byteLoader);
                        a.operand.expression = std::make_shared<ExpressionTree>(ctx->byteLoader);
                        a.operand.expression->middle = USS("-");
                        a.operand.expression->left = std::make_shared<Operand>(ctx->byteLoader);
                        a.operand.expression->left->literal = operand.expression->left->literal;
                        a.operand.expression->right = std::make_shared<Operand>(ctx->byteLoader);
                        a.operand.expression->right->literal = std::make_shared<Token<TokenKind>>(ctx->byteLoader);
                        a.operand.expression->right->literal->type.push_back(TokenKind::LITERAL);
                        a.operand.expression->right->literal->value = *d.name;
                        tc.arguments.push_back(a);

                        LanguageArgument a2(ctx->byteLoader);
                        a2.operand.literal = operand.expression->right->literal;
                        tc.arguments.push_back(a2);

                        LanguageArgument a3(ctx->byteLoader);
                        a3.operand.literal = std::make_shared<Token<TokenKind>>(nnvm::LanguageModel::fullCopy(*operand.expression->left->literal));
                        d.operand.call->arguments.push_back(a3);
                        
                        container.push_back(d);
                    }
                    operand.call = std::make_shared<LanguageCall>(tc);
                    operand.expression = nullptr;
                    operand.literal = nullptr;
                    
                    int64_t h;
                    // make the multiplicative a separate definition
                    res &= extractToLiteral(operand, container, ranges, templateNames, h);
                }

                ctx->setup->getLogger()->debug(USS("Height of ") + nnvm::LanguageModel::toString(operand, 1) + StringUtils::spf(USS(" {0}"), height));
                
                return (int8_t) res;
            }
        }
        return 0;
    }

    int8_t DefinitionBuilder::separateExpressionDefinition(Operand& operand, std::deque<LanguageDefinition>& container, std::map<icu::UnicodeString, std::tuple<int64_t, int64_t>> &ranges, std::deque<icu::UnicodeString> &templateNames, int64_t& height)
    {
        // extract operand into a separate line of expression block, leaving a literal reference to it
        LanguageDefinition d(ctx->byteLoader);
        d.name = std::make_shared<icu::UnicodeString>(aggregateName(operand) + StringUtils::spf(USS("{0}"), getNewIndex()));
        d.templateNames = templateNames;
        d.operand.call = std::make_shared<LanguageCall>(ctx->byteLoader);
        d.operand.call->blockName = EXPRESSION_BLOCK;
        LanguageArgument a(ctx->byteLoader);
        a.operand = operand;
        d.operand.call->arguments.push_back(a);
        container.push_back(d);

        operand.expression = nullptr;
        operand.call = nullptr;
        operand.literal = std::make_shared<Token<TokenKind>>(ctx->byteLoader);
        operand.literal->type.push_back(TokenKind::LITERAL);
        operand.literal->value = *d.name;
        
        int64_t h;
        // then simplify the extracted expression
        return incAndReturn(simplifyExpression(d.operand.call->arguments[0].operand, container, ranges, templateNames, h), h, height);
    }

    int8_t DefinitionBuilder::parseCall(std::vector<Token<TokenKind>>& subTokens, std::vector<Token<TokenKind>>& templateSubTokens, std::shared_ptr<ParsedNeuron> neuron, Operand& operand, std::deque<LanguageDefinition>& container, std::map<icu::UnicodeString, std::tuple<int64_t, int64_t> > &ranges, std::deque<icu::UnicodeString>& templateNames)
    {
        // only call and its arguments
        if (!operand.call) {
            return 0;
        }
        
        int8_t res = 1;
        for (auto i = 0; i < templateSubTokens.size(); i += 2) {
            auto leafs = combineLeafs(templateSubTokens[i]);
            res &= asTemplateTree(templateSubTokens[i], leafs, *operand.call, container);
        }
        ctx->setup->getLogger()->debug(USS("Template tokens transformed to AST"));
        for (auto i = 0; i < subTokens.size(); i += 2) {
            LanguageArgument a(ctx->byteLoader);
            auto t = neuron->argumentType(i / 2, (int64_t) subTokens.size());
            auto leafs = combineLeafs(subTokens[i]);
            switch (t) {
                case ParsedNeuronArgumentType::RAW:
                {
                    a.rawTokens = std::make_shared<std::deque<Token<TokenKind>>>(leafs.begin(), leafs.end());
                    break;
                }
                case ParsedNeuronArgumentType::REQUIRED_SINGLE:
                {
                    if (subTokens[i].subTokens.size()) {
                        return 0;
                    }
                    if (!subTokens[i].isOf(TokenKind::LITERAL)) {
                        return 0;
                    }
                }
                case ParsedNeuronArgumentType::KEY:
                case ParsedNeuronArgumentType::TREE:
                case ParsedNeuronArgumentType::FLAT:
                case ParsedNeuronArgumentType::FLAT_CONDITIONAL:
                case ParsedNeuronArgumentType::SINGLE:
                {
                    // just parse AST
                    res &= asTree(subTokens[i], leafs, a.operand, container, ranges, templateNames);
                    break;
                }
                case ParsedNeuronArgumentType::FAIL:
                default:
                    return 0;
            }
            operand.call->arguments.push_back(a);
        }
        ctx->setup->getLogger()->debug(USS("Main argument tokens transformed to AST"));

        int64_t h;
        res &= prepareCall(neuron, operand, container, ranges, templateNames, h);
        
        return res;
    }

    void DefinitionBuilder::visitToken(const LR<TokenKind>& lr, std::vector<Token<TokenKind>>::iterator tokensBegin, std::vector<Token<TokenKind>>::iterator tokensEnd, Token<TokenKind>& rootToken)
    {
        //std::vector<Token<TokenKind>> v(tokensBegin, tokensEnd);
        //this->ctx->setup->getLogger()->debug(lr.dumpTokenCollapse(rootToken, v));
    }

    int8_t DefinitionBuilder::parseDefinition(std::vector<Token<TokenKind>>& tokens, LanguageDefinition& languageDefinition, std::deque<LanguageDefinition>& container, std::map<icu::UnicodeString, std::tuple<int64_t, int64_t> > &ranges)
    {
        // name += ***
        if (tokens[1].isOf(TokenKind::REDUCTION)) {
            if (!tokens[2].isOf(TokenKind::ASSIGN)) {
                return 0;
            }
            languageDefinition.name = std::make_shared<icu::UnicodeString>(tokens[0].value);
            languageDefinition.reduction = 1;
            tokens.erase(tokens.begin(), tokens.begin() + 3);
        }
        // name = ***
        if (tokens[1].isOf(TokenKind::ASSIGN)) {
            languageDefinition.name = std::make_shared<icu::UnicodeString>(tokens[0].value);
            tokens.erase(tokens.begin(), tokens.begin() + 2);
        }
        if (!tokens.back().type.size()) {
            fillTokenType(tokens.back());
        }

        std::map<icu::UnicodeString, double> context;
        nnvm::Language::Language::markAsNumberConstant(tokens, languageDefinition.templateNames, ranges, context, ctx);

        std::vector<Token<TokenKind>> data(tokens.begin(), tokens.end());

        ctx->setup->getLogger()->debug(USS("Prepared LR"));
        
        auto tv = [this](const LR<TokenKind>& lr, std::vector<Token<TokenKind>>::iterator childTokensStart, std::vector<Token<TokenKind>>::iterator childTokensEnd, Token<TokenKind>& collapsedToken) {
            this->visitToken(lr, childTokensStart, childTokensEnd, collapsedToken);
        };
        LR<TokenKind> lr(TokenKind::TOKEN_EOF, tokenStates, tv, tokenTypeString, ctx->byteLoader);
        lr.collapse(data);
        if (data.size() != 1) {
            ctx->setup->getLogger()->error(USS("Failed to parse syntax. Tokens:"));
            for (auto i = data.begin(); i != data.end(); ++i) {
                ctx->setup->getLogger()->error(nnvm::LanguageModel::toString(*i) + USS(" {") + tokenTypeString(i->type[0]) + USS("}"));
            }
            return 0;
        }

        ctx->setup->getLogger()->debug(USS("Start parsing token tree to AST: ") + LanguageModel::toString(std::deque<Token<TokenKind> >(data.begin(), data.end())));
        
        return asTree(data[0], tokens, languageDefinition.operand, container, ranges, languageDefinition.templateNames, true);
    }

    int8_t DefinitionBuilder::parseDefinition(const icu::UnicodeString& definition, LanguageDefinition& languageDefinition, std::deque<LanguageDefinition>& container, std::map<icu::UnicodeString, std::tuple<int64_t, int64_t> > &ranges)
    {
        ErrorHandlers<DataSource, UChar32, int64_t, std::vector<Token<TokenKind>>> handlers;
        handlers.noParse = [](int16_t stateIndex, std::shared_ptr<std::vector<int16_t> > states) {
            throw ByteCodeParserException(_i("err.parser.parserloop"), _i("err.parser.illegalstate"), stateIndex, 0);
        };
        handlers.noRead = [](std::function<icu::UnicodeString(std::vector<UChar32>&, int16_t, int64_t)>& errorDetails, std::vector<UChar32>& buffer, int16_t stateIndex, std::shared_ptr<std::vector<int16_t> > states) {
            throw ByteCodeParserException(_i("err.parser.reader"), errorDetails(buffer, stateIndex, 0), stateIndex, 0);
        };
        handlers.noMatch = [](std::function<icu::UnicodeString(std::vector<UChar32>&, int16_t, int64_t)>& errorDetails, std::vector<UChar32>& buffer, int16_t stateIndex, std::shared_ptr<std::vector<int16_t> > states) {
            throw ByteCodeParserException(_i("err.parser.matcher"), errorDetails(buffer, stateIndex, 0), stateIndex, 0);
        };
        handlers.noFill = [](std::function<icu::UnicodeString(std::vector<UChar32>&, int16_t, int64_t)>& errorDetails, std::vector<UChar32>& buffer, int16_t stateIndex, std::shared_ptr<std::vector<int16_t> > states) {
            throw ByteCodeParserException(_i("err.parser.filler"), errorDetails(buffer, stateIndex, 0), stateIndex, 0);
        };
        handlers.noStep = [](std::function<icu::UnicodeString(std::vector<UChar32>&, int16_t, int64_t)>& errorDetails, std::vector<UChar32>& buffer, int16_t stateIndex, std::shared_ptr<std::vector<int16_t> > states) {
            throw ByteCodeParserException(_i("err.parser.stepper"), errorDetails(buffer, stateIndex, 0), stateIndex, 0);
        };

        ctx->setup->getLogger()->debug(USS("Prepared state machine for ") + definition);
        
        StringMachine<int64_t, std::vector<Token<TokenKind>>> a(defStates(), handlers);
        auto tokens = a.run(definition);
        if (tokens->back().value.length() == 0) {
            tokens->pop_back();
        }
        if (tokens->back().type.size() == 0) {
            fillTokenType(tokens->back());
        }
        
        if (!tokens->at(0).isOf(TokenKind::LITERAL)) {
            return 0;
        }

        ctx->setup->getLogger()->debug(USS("Start parsing token stream to AST for ") + definition);
        
        return parseDefinition(*tokens, languageDefinition, container, ranges);
    }

    int8_t DefinitionBuilder::parseExpression(Token<TokenKind>& rootToken, Operand& operand, std::deque<LanguageDefinition>& container, std::map<icu::UnicodeString, std::tuple<int64_t, int64_t> > &ranges)
    {
        auto t = combineLeafs(rootToken);
        std::deque<icu::UnicodeString> templateNames;
        return asTree(rootToken, t, operand, container, ranges, templateNames);
    }

    int8_t DefinitionBuilder::parseExpression(const icu::UnicodeString& definition, Operand& operand, std::map<icu::UnicodeString, std::tuple<int64_t, int64_t> > &ranges)
    {
        ErrorHandlers<DataSource, UChar32, int64_t, std::vector<Token<TokenKind>>> handlers;
        handlers.noParse = [](int16_t stateIndex, std::shared_ptr<std::vector<int16_t> > states) {
            throw ByteCodeParserException(_i("err.parser.parserloop"), _i("err.parser.illegalstate"), stateIndex, 0);
        };
        handlers.noRead = [](std::function<icu::UnicodeString(std::vector<UChar32>&, int16_t, int64_t)>& errorDetails, std::vector<UChar32>& buffer, int16_t stateIndex, std::shared_ptr<std::vector<int16_t> > states) {
            throw ByteCodeParserException(_i("err.parser.reader"), errorDetails(buffer, stateIndex, 0), stateIndex, 0);
        };
        handlers.noMatch = [](std::function<icu::UnicodeString(std::vector<UChar32>&, int16_t, int64_t)>& errorDetails, std::vector<UChar32>& buffer, int16_t stateIndex, std::shared_ptr<std::vector<int16_t> > states) {
            throw ByteCodeParserException(_i("err.parser.matcher"), errorDetails(buffer, stateIndex, 0), stateIndex, 0);
        };
        handlers.noFill = [](std::function<icu::UnicodeString(std::vector<UChar32>&, int16_t, int64_t)>& errorDetails, std::vector<UChar32>& buffer, int16_t stateIndex, std::shared_ptr<std::vector<int16_t> > states) {
            throw ByteCodeParserException(_i("err.parser.filler"), errorDetails(buffer, stateIndex, 0), stateIndex, 0);
        };
        handlers.noStep = [](std::function<icu::UnicodeString(std::vector<UChar32>&, int16_t, int64_t)>& errorDetails, std::vector<UChar32>& buffer, int16_t stateIndex, std::shared_ptr<std::vector<int16_t> > states) {
            throw ByteCodeParserException(_i("err.parser.stepper"), errorDetails(buffer, stateIndex, 0), stateIndex, 0);
        };

        ctx->setup->getLogger()->debug(USS("Prepared state machine for ") + definition);

        StringMachine<int64_t, std::vector<Token<TokenKind>>> a(defStates(), handlers);
        auto tokens = a.run(definition);
        if (tokens->back().value.length() == 0) {
            tokens->pop_back();
        }
        if (tokens->back().type.size() == 0) {
            fillTokenType(tokens->back());
        }
        
        std::deque<LanguageDefinition> d;
        std::vector<Token<TokenKind>> data(tokens->begin(), tokens->end());

        ctx->setup->getLogger()->debug(USS("Prepared LR for ") + definition);
        
        auto tv = [this](const LR<TokenKind>& lr, std::vector<Token<TokenKind>>::iterator childTokensStart, std::vector<Token<TokenKind>>::iterator childTokensEnd, Token<TokenKind>& collapsedToken) {
            this->visitToken(lr, childTokensStart, childTokensEnd, collapsedToken);
        };
        LR<TokenKind> lr(TokenKind::TOKEN_EOF, expStates, tv, tokenTypeString, ctx->byteLoader);
        lr.collapse(data);
        if (data.size() != 1) {
            return 0;
        }
        
        ctx->setup->getLogger()->debug(USS("Start parsing token tree to AST for ") + definition);
        
        return parseExpression(data[0], operand, d, ranges);
    }

    int8_t DefinitionBuilder::parseTemplate(const icu::UnicodeString& templateDef, Token<TokenKind>& token)
    {
        token.type.clear();
        token.type.push_back(TokenKind::TEMPLATER_CONTENT);
        
        ErrorHandlers<DataSource, UChar32, int64_t, std::vector<Token<TokenKind>>> handlers;
        handlers.noParse = [](int16_t stateIndex, std::shared_ptr<std::vector<int16_t> > states) {
            throw ByteCodeParserException(_i("err.parser.parserloop"), _i("err.parser.illegalstate"), stateIndex, 0);
        };
        handlers.noRead = [](std::function<icu::UnicodeString(std::vector<UChar32>&, int16_t, int64_t)>& errorDetails, std::vector<UChar32>& buffer, int16_t stateIndex, std::shared_ptr<std::vector<int16_t> > states) {
            throw ByteCodeParserException(_i("err.parser.reader"), errorDetails(buffer, stateIndex, 0), stateIndex, 0);
        };
        handlers.noMatch = [](std::function<icu::UnicodeString(std::vector<UChar32>&, int16_t, int64_t)>& errorDetails, std::vector<UChar32>& buffer, int16_t stateIndex, std::shared_ptr<std::vector<int16_t> > states) {
            throw ByteCodeParserException(_i("err.parser.matcher"), errorDetails(buffer, stateIndex, 0), stateIndex, 0);
        };
        handlers.noFill = [](std::function<icu::UnicodeString(std::vector<UChar32>&, int16_t, int64_t)>& errorDetails, std::vector<UChar32>& buffer, int16_t stateIndex, std::shared_ptr<std::vector<int16_t> > states) {
            throw ByteCodeParserException(_i("err.parser.filler"), errorDetails(buffer, stateIndex, 0), stateIndex, 0);
        };
        handlers.noStep = [](std::function<icu::UnicodeString(std::vector<UChar32>&, int16_t, int64_t)>& errorDetails, std::vector<UChar32>& buffer, int16_t stateIndex, std::shared_ptr<std::vector<int16_t> > states) {
            throw ByteCodeParserException(_i("err.parser.stepper"), errorDetails(buffer, stateIndex, 0), stateIndex, 0);
        };

        ctx->setup->getLogger()->debug(USS("Prepared state machine for ") + templateDef);

        StringMachine<int64_t, std::vector<Token<TokenKind>>> a(defStates(), handlers);
        auto td = StringConvUtils::utow(templateDef);
        icu::UnicodeString s(templateDef, templateDef.getChar32Limit(1), templateDef.getChar32Start(templateDef.length() - templateDef.getChar32Limit(1) - 1));
        auto tt = StringConvUtils::utow(s);
        auto tokens = a.run(s);
        if (tokens->back().value.length() == 0) {
            tokens->pop_back();
        }
        if (tokens->back().type.size() == 0) {
            fillTokenType(tokens->back());
        }
        
        std::vector<Token<TokenKind>> data(tokens->begin(), tokens->end());
        
        ctx->setup->getLogger()->debug(USS("Prepared LR for ") + templateDef);
        
        auto tv = [this](const LR<TokenKind>& lr, std::vector<Token<TokenKind>>::iterator childTokensStart, std::vector<Token<TokenKind>>::iterator childTokensEnd, Token<TokenKind>& collapsedToken) {
            this->visitToken(lr, childTokensStart, childTokensEnd, collapsedToken);
        };
        LR<TokenKind> lr(TokenKind::TOKEN_EOF, tempStates, tv, tokenTypeString, ctx->byteLoader);
        lr.collapse(data);
        token.subTokens = data;

        ctx->setup->getLogger()->debug(USS("Token tree created for ") + templateDef);

        return 1;
    }
}
