/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#pragma once

namespace nnvm
{
    namespace JSON {
        struct Value;
        class JSONToByteCode;
    }
}

#include <predef.h>
#include <deque>
#include <inttypes.h>
#include <map>
#include <memory>
#include <unicode/unistr.h>

namespace nnvm {
    namespace JSON {
        typedef std::map<icu::UnicodeString, Value> Object;
    }
}

#include <ByteCode.hpp>
#include "ByteCodeParserModel.hpp"

namespace nnvm {
    namespace JSON
    {
        struct Value
        {
            int8_t isempty = false;
            int8_t isnull = true;
            int8_t boolean = false;
            double number = 0;
            icu::UnicodeString string;
            std::shared_ptr<std::deque<Value>> array = nullptr;
            std::shared_ptr<Object> object = nullptr;
        };
        
        class JSONToByteCode
        {
        public:
            static std::vector<ByteState> states;
            static int8_t buildResult(Value val, std::shared_ptr<std::vector<std::shared_ptr<ByteCode>>>& result, std::shared_ptr<ByteCodeLoader> loader);
        };
    }
}
