/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#include <StringConv.hpp>
#include <codecvt>
#include <locale>
#include <string>
#include <iomanip>
#include <sstream>
#include <deque>
#include <memory>
#include <cassert>
#include <unicode/ustring.h>
#include <unicode/brkiter.h>

namespace nnvm {
    template<int SZ_INT>
    std::u32string _wtou32(const std::wstring& v)
    {
        throw std::bad_exception();
    }

    template<>
    std::u32string _wtou32<16>(const std::wstring& v)
    {
        auto s = std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t>().to_bytes(v.c_str());
        return std::wstring_convert<std::codecvt_utf8<char32_t>, char32_t>().from_bytes(s);
    }

    template<>
    std::u32string _wtou32<32>(const std::wstring& v)
    {
        return std::u32string(v.begin(), v.end());
    }

    std::u32string StringConvUtils::wtou32(const std::wstring& v)
    {
        return _wtou32<sizeof(wchar_t)>(v);
    }

    template<int SZ_INT>
    std::wstring _u32tow(const std::u32string& v)
    {
        throw std::bad_exception();
    }

    template<>
    std::wstring _u32tow<16>(const std::u32string& v)
    {
        auto s = std::wstring_convert<std::codecvt_utf8<char32_t>, char32_t>().to_bytes(v.c_str());
        return std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t>().from_bytes(s);
    }

    template<>
    std::wstring _u32tow<32>(const std::u32string& v)
    {
        return std::wstring(v.begin(), v.end());
    }

    std::wstring StringConvUtils::u32tow(const std::u32string& v)
    {
        return _u32tow<sizeof(wchar_t)>(v);
    }

    std::string StringConvUtils::wtou8(const std::wstring& v)
    {
        return StringConvUtils::utou8(StringConvUtils::wtou(v));
        //return std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t>().to_bytes(v);
    }

    std::wstring StringConvUtils::u8tow(const std::string& v)
    {
        return StringConvUtils::utow(StringConvUtils::u8tou(v));
        //return std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t>().from_bytes(v);
    }

    icu::UnicodeString StringConvUtils::u8tou(const std::string& v)
    {
        return icu::UnicodeString::fromUTF8(icu::StringPiece(v.c_str(), (int32_t)v.size()));
    }

    std::string StringConvUtils::utou8(const icu::UnicodeString& v)
    {
        std::string res;
        v.toUTF8String(res);
        return res;
    }

    icu::UnicodeString StringConvUtils::wtou(const std::wstring& v)
    {
        if (sizeof(wchar_t) == 2) {
            return icu::UnicodeString((UChar*)&v[0], v.size());
        }
        return StringConvUtils::asUnicodeString((UChar32*)&v[0], (int64_t) v.size());
    }

    std::wstring StringConvUtils::utow(const icu::UnicodeString& v)
    {
        if (sizeof(wchar_t) == 2) {
            auto buffer = new UChar[v.length()];
            UErrorCode e;
            v.extract(buffer, v.length(), e);
            return std::wstring((wchar_t*)buffer, (wchar_t*)buffer + v.length());
        }
        auto n = v.countChar32();
        std::wstring res(L"");
        res.reserve((unsigned long) n);
        for (auto i = 0; i < n; ++i) {
            res += v.char32At(i);
        }
        return res;
    }

    UChar StringConvUtils::u8tou(const unsigned char& v)
    {
        UChar dest[10] = {0};
        int32_t p = 0;
        UErrorCode e = U_ZERO_ERROR;
        u_strFromUTF8(dest, 10, &p, (const char *) &v, 1, &e);
        if (e == U_INVALID_CHAR_FOUND) {
            return 0;
        }
        return dest[0];
    }

    unsigned char StringConvUtils::utou8(const UChar& v)
    {
        char dest[10];
        int32_t p;
        UErrorCode e;
        u_strToUTF8(dest, 10, &p, &v, 1, &e);
        return (unsigned char) dest[0];
    }

    std::string StringConvUtils::utoescaped(const icu::UnicodeString& v, int8_t asciiSafe)
    {
        std::deque<char> t;

        UErrorCode err = U_ZERO_ERROR;
        std::unique_ptr<icu::BreakIterator> i(icu::BreakIterator::createCharacterInstance(icu::Locale::getDefault(), err));
        assert(U_SUCCESS(err));
        i->setText(v);
        i->first();

        auto s = i->current();
        for (auto e = i->next(); e != icu::BreakIterator::DONE; e = i->next()) {
            auto c = icu::UnicodeString(v, s, e - s).char32At(0);
            switch (c) {
            case '\"':
                t.push_back('\\');
                t.push_back('"');
                break;
            case '\\':
                t.push_back('\\');
                t.push_back('\\');
                break;
            case '\b':
                t.push_back('\\');
                t.push_back('b');
                break;
            case '\f':
                t.push_back('\\');
                t.push_back('f');
                break;
            case '\n':
                t.push_back('\\');
                t.push_back('n');
                break;
            case '\r':
                t.push_back('\\');
                t.push_back('r');
                break;
            case '\t':
                t.push_back('\\');
                t.push_back('t');
                break;
            case '/': 
                t.push_back('\\');
                t.push_back('/');
                break;
            default:
                std::string ts;
                if ((c >= 0 && c <= 0x1F) || (asciiSafe && c > 0x7F)) {
                    std::ostringstream oss;
                    if (c < 0x10000) {
                        oss << "\\u" << std::hex << std::uppercase << std::setfill('0') << std::setw(4) << c;
                    }
                    else {
                        oss << "\\u" << std::hex << std::uppercase << std::setfill('0') << std::setw(4) << ((c >> 10) + 0xD7C0);
                        oss << "\\u" << std::hex << std::uppercase << std::setfill('0') << std::setw(4) << ((c & 0x3FF) + 0xDC00);
                    }
                    ts = oss.str();
                }
                else {
                    ts = StringConvUtils::utou8(icu::UnicodeString(c));
                }
                auto it = t.cbegin();
                std::advance(it, t.size());
                t.insert(it, ts.begin(), ts.end());
                break;
            }
            s = e;
        }
        return std::string(t.begin(), t.end());
    }

    icu::UnicodeString StringConvUtils::asUnicodeString(const UChar32* begin, int64_t count)
    {
        icu::UnicodeString res("");
        for (auto i = 0; i < count; ++i) {
            res.append(*(begin + i));
        }
        return res;
    }

    icu::UnicodeString StringConvUtils::asUnicodeString(std::vector<UChar32>::const_iterator begin, std::vector<UChar32>::const_iterator end)
    {
        icu::UnicodeString res("");
        for (auto i = begin; i != end; ++i) {
            res.append(*i);
        }
        return res;
    }
}
