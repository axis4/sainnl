/*
 Copyright 2013-2016 Sergey Ionov

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#pragma once

#include <predef.h>

namespace nnvm {
    class ReleasePool;
}

#include <list>
#include <memory>
#include <mutex>
#include "ThreadUtils.hpp"

namespace nnvm {
    class ReleasePool : Timer {
    public:
        ReleasePool();
        template<typename T> void track(const std::shared_ptr<T>& object) {
            if (object == nullptr) {
                return;
            }

            std::lock_guard<std::mutex> lock(m);
            pool.emplace_back(object);
        }
    protected:
        virtual void releaseCheck();
    private:
        void timerCallback();
        std::mutex m;
        std::list<std::shared_ptr<void>> pool;
    };
}
