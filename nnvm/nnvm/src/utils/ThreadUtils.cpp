/*
 Copyright 2013-2016 Sergey Ionov

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#include "ThreadUtils.hpp"
#include <chrono>
#include <thread>
#include <utility>
#include "Utils.hpp"

namespace nnvm {
    void ThreadUtils::wait(int64_t ns)
    {
        std::this_thread::sleep_for(std::chrono::nanoseconds(ns));
    }

    int8_t ThreadUtils::spinWait(const std::function<int8_t()> &exitPredicate, int64_t timeoutNs)
    {
        int8_t result = 0;
        auto start = PerformanceUtils::now_nanoseconds();
        while (!(result = exitPredicate()) && (timeoutNs == 0 || PerformanceUtils::now_nanoseconds() - start < timeoutNs)) {
            ThreadUtils::wait(1);
        }

        return result;
    }

    Timer::Timer(std::function<void()> handler, int64_t periodNs, int64_t delayNs)
            : handler(std::move(handler)), active(1), periodNs(periodNs), delayNs(delayNs),
              timerThread(&Timer::timerHandler, this)
    {

    }

    Timer::~Timer()
    {
        active = 0;
        timerThread.join();
    }

    void Timer::run()
    {
        std::this_thread::sleep_for(std::chrono::nanoseconds(delayNs));
        while (active) {
            handler();
            std::this_thread::sleep_for(std::chrono::nanoseconds(periodNs));
        }
    }

    void Timer::timerHandler()
    {
        run();
    }

    StableTimer::StableTimer(std::function<void()> handler, int64_t parallelLimit, int64_t periodNs, int64_t delayNs)
            : Timer(std::move(handler), periodNs, delayNs), parallelLimit(parallelLimit),
              stablePool(static_cast<unsigned long>(parallelLimit > 0 ? parallelLimit : 16))
    {
    }

    StableTimer::~StableTimer()
    {
        active = 0;
        timerThread.join();
        for (auto& i : stablePool) {
            std::get<1>(*i).join();
        }
        stablePool.clear();
    }

    void StableTimer::run()
    {
        std::this_thread::sleep_for(std::chrono::nanoseconds(delayNs));
        while (active) {
            int8_t any = 0;
            for (auto& t : stablePool) {
                if (!std::get<0>(*t)) {
                    any = 1;
                    std::get<0>(*t) = 1;
                    std::get<1>(*t) = std::thread(&StableTimer::handlerThread, this, t);
                }
            }
            if (!any && (parallelLimit <= 0 || stablePool.size() < parallelLimit)) {
                auto t = std::make_shared<std::tuple<int8_t, std::thread>>();
                std::get<0>(*t) = 1;
                std::get<1>(*t) = std::thread(&StableTimer::handlerThread, this, t);
                stablePool.emplace_back(t);
            }
            std::this_thread::sleep_for(std::chrono::nanoseconds(periodNs));
        }
    }

    void StableTimer::handlerThread(std::shared_ptr<std::tuple<int8_t, std::thread>> stableThread)
    {
        finally f([stableThread]() { std::get<0>(*stableThread) = 0; });
        handler();
    }
}
