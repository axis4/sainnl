//
// Created by Serge Ionov on 8/30/18.
//

#include "ReleasePool.hpp"

namespace nnvm {
    ReleasePool::ReleasePool() :
            Timer([this]() { timerCallback(); }, 1000000000)
    {

    }

    void ReleasePool::releaseCheck()
    {
        std::lock_guard<std::mutex> lock(m);
        pool.erase(std::remove_if(pool.begin(), pool.end(), [](auto& object) { return object.use_count() <= 1; }),
                   pool.end());
    }

    void ReleasePool::timerCallback()
    {
        releaseCheck();
    }
}
