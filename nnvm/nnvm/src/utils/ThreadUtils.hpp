/*
 Copyright 2013-2016 Sergey Ionov

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#pragma once

#include <predef.h>

namespace nnvm {
    class ThreadUtils;
    class Timer;
    class StableTimer;
}

#include <cinttypes>
#include <memory>
#include <functional>
#include <tuple>
#include <thread>
#include <vector>

namespace nnvm {
    class ThreadUtils {
    public:
        static void wait(int64_t ns);
        static int8_t spinWait(const std::function<int8_t()> &exitPredicate, int64_t timeoutNs = 0);
    };

    class Timer {
    public:
        Timer(std::function<void()> handler, int64_t periodNs, int64_t delayNs = 0);
        virtual ~Timer();
    protected:
        virtual void run();
        std::function<void()> handler;
        int8_t active;
        int64_t periodNs;
        int64_t delayNs;
        std::thread timerThread;
    private:
        void timerHandler();
    };

    class StableTimer : public Timer {
    public:
        StableTimer(std::function<void()> handler, int64_t parallelLimit, int64_t periodNs, int64_t delayNs = 0);
        ~StableTimer() override;
    protected:
        void run() override;
        virtual void handlerThread(std::shared_ptr<std::tuple<int8_t, std::thread>> stableThread);
        int64_t parallelLimit;
        std::vector<std::shared_ptr<std::tuple<int8_t, std::thread>>> stablePool;
    };
}
