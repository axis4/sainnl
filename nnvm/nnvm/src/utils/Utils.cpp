/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#include "Utils.hpp"
#include <sstream>
#include <utility>
#include <unicode/regex.h>
#include <unicode/ucnv.h>

namespace nnvm {
    void PerformanceUtils::timeit(std::function<void()> call)
    {
        timeit(std::move(call), [](int64_t v) { std::cout << "[" << double(v)/1000000 << "ms]" << std::endl; });
    }

    void PerformanceUtils::timeit(std::function<void()> call, const icu::UnicodeString& title)
    {
        std::cout << StringConvUtils::utou8(title) << " ";
        timeit(std::move(call));
    }

    void PerformanceUtils::timeit(std::function<void()> call, std::function<void(int64_t)> reg)
    {
        const auto tp1 = std::chrono::high_resolution_clock::now();
        finally f([&]{
            const auto tp2 = std::chrono::high_resolution_clock::now();
            const auto d = std::chrono::duration_cast<std::chrono::nanoseconds>(tp2 - tp1);
            reg(d.count());
        });
        call();
    }

    int64_t PerformanceUtils::now_nanoseconds()
    {
        return std::chrono::high_resolution_clock::now().time_since_epoch().count();
    }

    int64_t PerformanceUtils::nanoseconds(std::chrono::time_point<std::chrono::steady_clock, std::chrono::nanoseconds> start)
    {
        return nanoseconds(start, std::chrono::high_resolution_clock::now());
    }

    int64_t PerformanceUtils::nanoseconds(std::chrono::time_point<std::chrono::steady_clock, std::chrono::nanoseconds> start, std::chrono::time_point<std::chrono::steady_clock, std::chrono::nanoseconds> end)
    {
        return std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count();
    }

    int64_t PerformanceUtils::nanoseconds(int64_t start)
    {
        return nanoseconds(start, std::chrono::high_resolution_clock::now().time_since_epoch().count());
    }

    int64_t PerformanceUtils::nanoseconds(int64_t start, int64_t end)
    {
        return end - start;
    }

    finally::finally(const std::function<void(void)> &functor) : functor(functor) {}
    finally::~finally() { functor(); }
}
