/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#include <BinaryUtils.hpp>
#include <sstream>
#include <cmath>
#include <unicode/regex.h>
#include <unicode/ucnv.h>
#include <iostream>
#include <StringConv.hpp>

namespace nnvm {
    const double EPSILON = DBL_EPSILON;
    const int64_t MAX_STRING = 1024 * 1024 * 10;
    std::string BinaryUtils::packageSeparator = ".";
    std::wstring BinaryUtils::packageSeparatorWide = L".";
    icu::UnicodeString BinaryUtils::packageSeparatorUnicode = USS(".");
    #if defined(_WIN32)
    std::string BinaryUtils::pathSeparator = "\\";
    std::wstring BinaryUtils::pathSeparatorWide = L"\\";
    icu::UnicodeString BinaryUtils::pathSeparatorUnicode = USS("\\");
    #else
    std::string BinaryUtils::pathSeparator = "/";
    std::wstring BinaryUtils::pathSeparatorWide = L"/";
    icu::UnicodeString BinaryUtils::pathSeparatorUnicode = USS("/");
    #endif

    int64_t BinaryUtils::readBinary(std::istream& stream, std::vector<uint8_t>& result, int64_t size)
    {
        result.clear();
        int64_t i = 0;
        while (i < size && stream.good()) {
            int64_t t = stream.get();
            if (stream.good()) {
                result.push_back((const unsigned char &) t);       // get character from file
            }
            i++;
        }
        if (stream.eof() && i < size) {
            return 0;
        }
        if (stream.fail()) {
            return -1;
        }
        return size;
    }

    int64_t BinaryUtils::readBinary(std::istream& stream, std::vector<int8_t>& result, int64_t size)
    {
        std::vector<uint8_t> tmp;
        auto res = readBinary(stream, tmp, size);
        result = std::vector<int8_t>(tmp.begin(), tmp.end());
        return res;
    }

    int64_t BinaryUtils::readBinary(std::istream& stream, std::vector<UChar32>& result, int64_t size)
    {
        result.clear();
        int64_t i = 0;

        /*
         **********************************************************************
         *   Copyright (C) 2001-2014, International Business Machines
         *   Corporation and others.  All Rights Reserved.
         **********************************************************************
         *  FILE NAME : ustream.cpp
         *
         *   Modification History:
         *
         *   Date        Name        Description
         *   06/25/2001  grhoten     Move iostream from unistr.h to here
         ******************************************************************************
         */

        /* ipfx should eat whitespace when ios::skipws is set */
        UChar uBuffer[16];
        char buffer[16];
        int32_t idx = 0;
        UConverter *converter = nullptr;
        UErrorCode errorCode = U_ZERO_ERROR;
        
        // use the default converter to convert chunks of text
        if(U_SUCCESS(errorCode)) {
            UChar *us;
            const UChar *uLimit = uBuffer + sizeof(uBuffer)/sizeof(*uBuffer);
            const char *s, *sLimit;
            int64_t ch;
            UChar ch32;
            int8_t initialWhitespace = true;
            int8_t continueReading = true;
            icu::UnicodeString str;
            
            /* We need to consume one byte at a time to see what is considered whitespace. */
            while (continueReading) {
                ch = stream.get();
                if (stream.eof()) {
                    // The EOF is only set after the get() of an unavailable byte.
                    if (!initialWhitespace) {
                        stream.clear(std::istream::eofbit);
                    }
                    continueReading = false;
                }
                sLimit = (const char *) (&ch + (int64_t)continueReading);
                us = uBuffer;
                s = (const char *) &ch;
                errorCode = U_ZERO_ERROR;
                /*
                 Since we aren't guaranteed to see the state before this call,
                 this code won't work on stateful encodings like ISO-2022 or an EBCDIC stateful encoding.
                 We flush on the last byte to ensure that we output truncated multibyte characters.
                 */
                ucnv_toUnicode(converter, &us, uLimit, &s, sLimit, nullptr, !continueReading, &errorCode);
                if(U_FAILURE(errorCode)) {
                    /* Something really bad happened. setstate() isn't always an available API */
                    stream.clear(std::istream::failbit);
                    continueReading = false;
                }
                else {
                    /* Was the character consumed? */
                    if (us != uBuffer) {
                        /* Reminder: ibm-1390 & JISX0213 can output 2 Unicode code points */
                        auto uBuffSize = (int64_t) (us - uBuffer);
                        int64_t uBuffIdx = 0;
                        while (uBuffIdx < uBuffSize) {
                            U16_NEXT(uBuffer, uBuffIdx, uBuffSize, ch32);
                            if (u_isWhitespace(ch32)) {
                                if (!initialWhitespace) {
                                    buffer[idx++] = (char) ch;
                                    while (idx > 0) {
                                        stream.putback(buffer[--idx]);
                                    }
                                    continueReading = false;
                                }
                                /* else skip initialWhitespace */
                            }
                            else {
                                if (initialWhitespace) {
                                    /*
                                     When initialWhitespace is TRUE, we haven't appended any
                                     character yet.  This is where we truncate the string,
                                     to avoid modifying the string before we know if we can
                                     actually read from the stream.
                                     */
                                    str.truncate(0);
                                    initialWhitespace = false;
                                }
                                str.append(ch32);
                                i++;
                                if (i >= size) {
                                    continueReading = false;
                                }
                            }
                        }
                        idx = 0;
                    }
                    else {
                        buffer[idx++] = (char) ch;
                    }
                }
            }
        }
        if (stream.eof() && i < size) {
            return 0;
        }
        if (stream.fail()) {
            return -1;
        }
        return size;
    }

    int8_t BinaryUtils::readByte(std::istream& stream, int8_t& result)
    {
        std::vector<uint8_t> tmp;
        if (readBinary(stream, tmp, sizeof(int8_t)) > 0) {
            result = tmp[0];
            return 1;
        }

        return 0;
    }

    template<typename T>
    int8_t readIntegerImpl(const std::vector<uint8_t>& tmp, T& result)
    {
        auto n = (int64_t)sizeof(T);
        if (tmp.size() != n) {
            return 0;
        }

        result = 0;
        for (auto i = 0; i < n; ++i) {
            result |= ((T)tmp[i]) << i * 8;
        }

        return 1;
    }
    template<typename T>
    int8_t readIntegerImpl(std::istream& stream, std::vector<uint8_t>& tmp, T& result)
    {
        auto n = (int64_t)sizeof(T);
        tmp = std::vector<uint8_t>((unsigned long) n);
        if (BinaryUtils::readBinary(stream, tmp, n) == n) {
            return readIntegerImpl(tmp, result);
        }

        return 0;
    }
    int8_t BinaryUtils::readInteger(std::istream& stream, int16_t& result, std::vector<uint8_t>& decomposed)
    {
        uint16_t tmp;
        auto r = readIntegerImpl(stream, decomposed, tmp);
        result = tmp;
        return r;
    }
    int8_t BinaryUtils::readInteger(const std::vector<uint8_t>& decomposed, int16_t& result)
    {
        uint16_t tmp;
        auto r = readIntegerImpl(decomposed, tmp);
        result = tmp;
        return r;
    }
    int8_t BinaryUtils::readInteger(std::istream& stream, int32_t& result, std::vector<uint8_t>& decomposed)
    {
        uint32_t tmp;
        auto r = readIntegerImpl(stream, decomposed, tmp);
        result = tmp;
        return r;
    }
    int8_t BinaryUtils::readInteger(const std::vector<uint8_t>& decomposed, int32_t& result)
    {
        uint32_t tmp;
        auto r = readIntegerImpl(decomposed, tmp);
        result = tmp;
        return r;
    }
    int8_t BinaryUtils::readInteger(std::istream& stream, int64_t& result, std::vector<uint8_t>& decomposed)
    {
        uint64_t tmp;
        auto r = readIntegerImpl(stream, decomposed, tmp);
        result = tmp;
        return r;
    }
    int8_t BinaryUtils::readInteger(const std::vector<uint8_t>& decomposed, int64_t& result)
    {
        uint64_t tmp;
        auto r = readIntegerImpl(decomposed, tmp);
        result = tmp;
        return r;
    }
    int8_t BinaryUtils::readDouble(std::istream& stream, double& result, std::vector<uint8_t>& decomposed)
    {
        int64_t significand;
        int64_t exp;
        std::vector<uint8_t> atmp2;
        if (!readIntegerImpl(stream, decomposed, significand)) {
            return 0;
        }

        if (!readIntegerImpl(stream, atmp2, exp)) {
            return 0;
        }

        decomposed.insert(decomposed.end(), atmp2.begin(), atmp2.end());
        return readDouble(decomposed, result);
    }
    int8_t BinaryUtils::readDouble(const std::vector<uint8_t>& decomposed, double& result)
    {
        if (decomposed.size() != 2 * sizeof(int64_t)) {
            return 0;
        }

        const int bits = 64;
        int64_t significand;
        int64_t exp;
        int8_t sign = 1;
        long double significandone = (1LL << (bits - 2)) + 0.5f;

        if (decomposed.size() != sizeof(int64_t) * 2) {
            return 0;
        }
        if (!readIntegerImpl(std::vector<uint8_t>(decomposed.begin(), decomposed.begin() + sizeof(int64_t)), significand)) {
            return 0;
        }
        if (!readIntegerImpl(std::vector<uint8_t>(decomposed.begin() + sizeof(int64_t), decomposed.end()), exp)) {
            return 0;
        }

        if (significand < 0) {
            significand = -significand;
            sign = -1;
        }

        long double fnorm = ((long double)significand / significandone);
        while (exp != 0) {
            if (exp < 0) {
                exp++;
                fnorm /= 2.0;
            }

            if (exp > 0) {
                exp--;
                fnorm *= 2.0;
            }
        }

        result = static_cast<double>(fnorm * sign);
        return 1;
    }
    int8_t BinaryUtils::readString(std::istream& stream, icu::UnicodeString& result, std::vector<uint8_t>& decomposed)
    {
        int64_t count;
        if (!readInteger(stream, count, decomposed)) {
            return 0;
        }

        // awkward input. avoid memory overflow.
        if (count > MAX_STRING) {
            return 0;
        }

        for (auto i = 0; i < count; ++i) {
            int8_t tmp;
            if (!readByte(stream, tmp)) {
                return 0;
            }
            decomposed.push_back((uint8_t)tmp);
        }

        return readString(decomposed, result);
    }
    int8_t BinaryUtils::readString(const std::vector<uint8_t>& decomposed, icu::UnicodeString& result)
    {
        if (decomposed.size() < sizeof(int64_t)) {
            return 0;
        }

        std::string tmp(decomposed.begin() + sizeof(int64_t), decomposed.end());
        result = StringConvUtils::u8tou(tmp);
        return 1;
    }

    int8_t BinaryUtils::writeBinary(const uint8_t* value, std::ostream& stream, int64_t size)
    {
        for (int i = 0; i < size; i++) {
            stream << (char)value[i];
        }

        return 1;
    }

    int8_t BinaryUtils::writeBinary(const std::vector<uint8_t>& value, std::ostream& stream)
    {
        return writeBinary(&value[0], stream, (int64_t) value.size());
    }

    int8_t BinaryUtils::writeBinary(const std::vector<int8_t>& value, std::ostream& stream)
    {
        auto tmp = std::vector<uint8_t>(value.begin(), value.end());
        return writeBinary(&tmp[0], stream, (int64_t) tmp.size());
    }

    int8_t BinaryUtils::writeByte(const int8_t value, const std::shared_ptr<std::ostream> &stream)
    {
        return BinaryUtils::writeBinary((const uint8_t *) &value, *stream, sizeof(int8_t));
    }
    template<typename T>
    int8_t writeIntegerImpl(const T value, const std::shared_ptr<std::ostream> &stream)
    {
        auto n = (int64_t)sizeof(T);
        for (auto i = 0; i < n; ++i) {
            auto v = (uint8_t)(value >> i * 8);
            if (!BinaryUtils::writeBinary(&v, *stream, sizeof(int8_t))) {
                return 0;
            }
        }

        return 1;
    }
    int8_t BinaryUtils::writeInteger(const int64_t value, const std::shared_ptr<std::ostream> &stream)
    {
        return writeIntegerImpl((uint64_t)value, stream);
    }
    int8_t BinaryUtils::writeInteger(const int32_t value, const std::shared_ptr<std::ostream> &stream)
    {
        return writeIntegerImpl((uint32_t)value, stream);
    }
    int8_t BinaryUtils::writeInteger(const int16_t value, const std::shared_ptr<std::ostream> &stream)
    {
        return writeIntegerImpl((uint16_t)value, stream);
    }
    int8_t BinaryUtils::writeDouble(const double value, const std::shared_ptr<std::ostream> &stream)
    {
        const int bits = 64;
        long double fnorm;
        int64_t shift;
        int8_t sign;
        long double significandone = (1LL << (bits - 2)) + 0.5f;

        int64_t significand = 0;
        int64_t exp = 0;

        if (fabs(value) > EPSILON) {
            if (value < 0) {
                sign = -1;
                fnorm = -value;
            }
            else {
                sign = 1;
                fnorm = value;
            }

            shift = 0;
            while (fnorm >= 1.0) {
                fnorm /= 2.0;
                shift++;
            }

            while (fnorm < 0.5) {
                fnorm *= 2.0;
                shift--;
            }

            significand = static_cast<int64_t>(fnorm * significandone * sign);
            exp = shift;
        }

        return static_cast<int8_t>(BinaryUtils::writeInteger(significand, stream) &&
                                   BinaryUtils::writeInteger(exp, stream) ? 1 : 0);
    }
    int8_t BinaryUtils::writeString(const icu::UnicodeString& value, const std::shared_ptr<std::ostream> &stream)
    {
        auto s = StringConvUtils::utou8(value);
        // awkward output. will not be accepted back
        if (s.size() > MAX_STRING) {
            return writeInteger((int64_t)0, stream);
        }

        if (!writeInteger((int64_t)s.size(), stream)) {
            return 0;
        }

        for (char &i : s) {
            if (!writeByte((int8_t) i, stream)) {
                return 0;
            }
        }

        return 1;
    }

    std::shared_ptr<std::ifstream> BinaryUtils::openStream(const icu::UnicodeString& file, int64_t& err)
    {
        std::shared_ptr<std::ifstream> s(new std::ifstream());
        errno = 0;
    #if defined(_WIN32)
        s->open(StringConvUtils::utow(file), std::ios::in | std::ios::binary);
    #else
        s->open(StringConvUtils::utou8(file), std::ios::in | std::ios::binary);
    #endif
        err = errno;
        return s;
    }

    std::shared_ptr<std::ifstream> BinaryUtils::openTextStream(const icu::UnicodeString& file, int64_t& err)
    {
        std::shared_ptr<std::ifstream> s(new std::ifstream());
        errno = 0;
    #if defined(_WIN32)
        auto f = StringConvUtils::utow(file);
    #else
        auto f = StringConvUtils::utou8(file);
    #endif
        s->open(f, std::ios::in);
        err = errno;
        s->imbue(std::locale());
        return s;
    }

    std::shared_ptr<std::ofstream> BinaryUtils::openWriteStream(const icu::UnicodeString& file, int64_t& err)
    {
        std::shared_ptr<std::ofstream> s(new std::ofstream());
        errno = 0;
    #if defined(_WIN32)
        s->open(StringConvUtils::utow(file), std::ios::out | std::ios::binary);
    #else
        s->open(StringConvUtils::utou8(file), std::ios::out | std::ios::binary);
    #endif
        err = errno;
        return s;
    }

    std::shared_ptr<std::ofstream> BinaryUtils::openWriteTextStream(const icu::UnicodeString& file, int64_t& err)
    {
        std::shared_ptr<std::ofstream> s(new std::ofstream());
        errno = 0;
    #if defined(_WIN32)
        s->open(StringConvUtils::utow(file), std::ios::out);
    #else
        s->open(StringConvUtils::utou8(file), std::ios::out);
    #endif
        err = errno;
        s->imbue(std::locale());
        return s;
    }


    void BinaryUtils::asVector(std::istream& ss, std::vector<int8_t>& to)
    {
        readBinary(ss, to, 0xFFFFFFFFUL);
    }

    void BinaryUtils::asStream(const std::vector<int8_t>& from, std::ostream& ss)
    {
        writeBinary(from, ss);
    }
}
