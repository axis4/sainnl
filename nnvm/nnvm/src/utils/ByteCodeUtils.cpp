/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#include <ByteCodeUtils.hpp>
#include <sstream>
#include <iostream>
#include <unicode/numfmt.h>
#include <StringConv.hpp>
#include <utility>

namespace nnvm {
    double ByteCodeUtils::asDouble(const ByteCodeParameter& param)
    {
        switch (param.getType())
        {
            case ByteCodeParameterTypes::BYTE:
                return (double)param.value.byteValue;
            case ByteCodeParameterTypes::INTEGER:
                return (double)param.value.integerValue;
            case ByteCodeParameterTypes::DOUBLE:
                return param.value.doubleValue;
            default:
                return 0;
        }
    }

    icu::UnicodeString ByteCodeUtils::asString(const ByteCodeParameter& param)
    {
        switch (param.getType())
        {
            case ByteCodeParameterTypes::BYTE:
                return asString(param.value.byteValue);
            case ByteCodeParameterTypes::INTEGER:
                return asString(param.value.integerValue);
            case ByteCodeParameterTypes::DOUBLE:
                return asString(param.value.doubleValue);
            case ByteCodeParameterTypes::STRING:
                return param.value.stringValue;
            default:
                return USS("0");
        }
    }

    icu::UnicodeString ByteCodeUtils::asString(const double value)
    {
        icu::UnicodeString res;
        UErrorCode success = U_ZERO_ERROR;
        auto l = icu::Locale::getRoot();
        auto nf = icu::NumberFormat::createInstance(l, success);
        nf->setGroupingUsed(false);
        nf->format(value, res);
        return res;
    }

    icu::UnicodeString ByteCodeUtils::asString(const int64_t value)
    {
        icu::UnicodeString res;
        UErrorCode success = U_ZERO_ERROR;
        auto l = icu::Locale::getRoot();
        auto nf = icu::NumberFormat::createInstance(l, success);
        nf->setGroupingUsed(false);
        nf->format(value, res);
        return res;
    }

    icu::UnicodeString ByteCodeUtils::asString(const int8_t value)
    {
        icu::UnicodeString res;
        UErrorCode success = U_ZERO_ERROR;
        auto l = icu::Locale::getRoot();
        auto nf = icu::NumberFormat::createInstance(l, success);
        nf->setGroupingUsed(false);
        nf->format((int32_t)value, res);
        return res;
    }

    double ByteCodeUtils::asNumber(const icu::UnicodeString& s)
    {
        double rs;
        std::wstringstream ss;
        ss << StringConvUtils::utow(s).c_str();
        ss >> rs;
        return rs;
    }

    int64_t ByteCodeUtils::asHexNumber(const icu::UnicodeString& s)
    {
        int64_t res = 0;
        auto w = StringConvUtils::utow(s);
        for (wchar_t &i : w) {
            int64_t v = 0;
            if (i >= '0' && i <= '9') {
                v = i - '0';
            }
            if (i >= 'a' && i <= 'f') {
                v = i - 'a' + 10;
            }
            if (i >= 'A' && i <= 'F') {
                v = i - 'A' + 10;
            }
            res = (res << 4) + v;
        }
        return res;
    }

    int8_t ByteCodeUtils::isInIncomes(std::shared_ptr<ByteCodeBlockItem> edge, std::deque<std::shared_ptr<ByteCodeBlockItem>>::iterator incomeStart, std::deque<std::shared_ptr<ByteCodeBlockItem>>::iterator incomeEnd)
    {
        return isInItems(std::move(edge), incomeStart, incomeEnd);
    }

    int8_t ByteCodeUtils::isInItems(std::shared_ptr<ByteCodeBlockItem> edge, std::deque<std::shared_ptr<ByteCodeBlockItem>>::iterator itemStart, std::deque<std::shared_ptr<ByteCodeBlockItem>>::iterator itemEnd)
    {
        for (; itemStart != itemEnd; ++itemStart) {
            auto e = edge->getOwner();
            auto ie = (*itemStart);
            auto i = ie->getOwner();
            auto el = e.lock();
            auto il = i.lock();
            if (el && il && el->getName() == il->getName() && edge->getName() == ie->getName()) {
                return 1;
            }
        }

        return 0;
    }

    int8_t ByteCodeUtils::isInItems(std::shared_ptr<ByteCodeBlockItem> edge)
    {
        auto r = edge->getOwner().lock()->getItems();
        return isInItems(edge, std::get<0>(r), std::get<1>(r));
    }

    void ByteCodeUtils::finalizeCode(std::shared_ptr<ByteCode> code)
    {
        if (code) {
            // build income links from items
            auto r = code->getBlocks();
            for (auto i = std::get<0>(r); i != std::get<1>(r); i++) {
                auto b = *i;
                auto r2 = b->getItems();
                for (auto k = std::get<0>(r2); k != std::get<1>(r2); k++) {
                    auto tb = code->getBlock((*k)->getName());
                    auto tbe = code->getBlocks();
                    if (tb != std::get<1>(tbe)) {
                        auto r3 = (*tb)->getIncomes();
                        if (!isInIncomes(*k, std::get<0>(r3), std::get<1>(r3))) {
                            (*tb)->addIncome(*k);
                        }
                    }
                }
            }
        }
    }

    void ByteCodeUtils::reveal(std::shared_ptr<ByteCodeBlock> block)
    {
        std::cout << "Reveal: " << StringConvUtils::utou8(block->getName()) << "(" << StringConvUtils::utou8(block->getType()) << ")" << std::endl;
        auto r = block->getParameters();
        reveal(std::vector<std::shared_ptr<ByteCodeParameter> >(std::get<0>(r), std::get<1>(r)));
        auto r2 = block->getIncomes();
        for (auto i = std::get<0>(r2); i != std::get<1>(r2); ++i) {
            std::cout << "<-" << StringConvUtils::utou8((*i)->getOwner().lock()->getName()) << "[";
            r = (*i)->getParameters();
            for (auto p = std::get<0>(r); p != std::get<1>(r); ++p) {
                std::cout << StringConvUtils::utou8((*p)->getName()) << "(" << (int)(*p)->getType() << ")=" << ByteCodeUtils::asDouble(**p) << ",";
            }
            std::cout<< "]" << std::endl;
        }
        r2 = block->getItems();
        for (auto i = std::get<0>(r2); i != std::get<1>(r2); ++i) {
            std::cout << "->" << StringConvUtils::utou8((*i)->getName()) << "[";
            r = (*i)->getParameters();
            for (auto p = std::get<0>(r); p != std::get<1>(r); ++p) {
                std::cout << StringConvUtils::utou8((*p)->getName()) << "(" << (int)(*p)->getType() << ")=" << ByteCodeUtils::asDouble(**p) << ",";
            }
            std::cout<< "]" << std::endl;
        }
    }

    void ByteCodeUtils::reveal(std::vector<std::shared_ptr<ByteCodeParameter> > parameters)
    {
        for (auto i = std::begin(parameters); i != std::end(parameters); ++i) {
            std::cout << StringConvUtils::utou8((*i)->getName()) << "(" << (int)(*i)->getType() << ")=" << ByteCodeUtils::asDouble(**i) << std::endl;
        }
    }
}
