/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#include <StringUtils.hpp>
#include <sstream>
#include <unicode/regex.h>
#include <unicode/ucnv.h>
#include "string_format/FormatString.hpp"
#include <StringConv.hpp>
#include <iomanip>

namespace nnvm {
    UnicodeIterator& UnicodeIterator::operator=(icu::UnicodeString str)
    {
        s = str;
        pos = 0;
        return (*this);
    }

    UnicodeIterator::operator bool()const
    {
        return true;
    }

    bool UnicodeIterator::operator==(const UnicodeIterator& rawIterator)const
    {
        return pos == rawIterator.pos;
    }

    bool UnicodeIterator::operator!=(const UnicodeIterator& rawIterator)const
    {
        return pos != rawIterator.pos;
    }

    UnicodeIterator& UnicodeIterator::operator+=(const int32_t& movement)
    {
        pos += movement;
        return (*this);
    }

    UnicodeIterator& UnicodeIterator::operator-=(const int32_t& movement)
    {
        pos -= movement;
        return (*this);
    }

    UnicodeIterator& UnicodeIterator::operator++()
    {
        ++pos;
        return (*this);
    }

    UnicodeIterator& UnicodeIterator::operator--()
    {
        --pos;
        return (*this);
    }

    const UnicodeIterator UnicodeIterator::operator++(int32_t)
    {
        auto temp(*this);
        ++pos;
        return temp;
    }

    const UnicodeIterator UnicodeIterator::operator--(int32_t)
    {
        auto temp(*this);
        --pos;
        return temp;
    }

    UnicodeIterator UnicodeIterator::operator+(const int32_t& movement)
    {
        auto oldPtr = pos;
        pos += movement;
        auto temp(*this);
        pos = oldPtr;
        return temp;
    }

    UnicodeIterator UnicodeIterator::operator-(const int32_t& movement)
    {
        auto oldPtr = pos;
        pos -= movement;
        auto temp(*this);
        pos = oldPtr;
        return temp;
    }

    int32_t UnicodeIterator::operator-(const UnicodeIterator& rawIterator)
    {
        return (int32_t)(rawIterator.pos - pos);
    }

    UChar32 UnicodeIterator::operator*()
    {
        return s.char32At((int32_t)pos);
    }

    std::vector<icu::UnicodeString>& StringUtils::split(std::vector<icu::UnicodeString>& result, const icu::UnicodeString& s, const icu::UnicodeString& pattern)
    {
        result.clear();

        UErrorCode status = U_ZERO_ERROR;
        icu::RegexMatcher m(pattern, 0, status);
        const int32_t maxWords = s.length();
        auto* words = new icu::UnicodeString[maxWords];
        int numWords = m.split(s, words, maxWords, status);
        icu::UnicodeString* end = words + numWords;
        result.assign(words, end);
        delete[] words;
        
        return result;
    }

    std::vector<icu::UnicodeString>& StringUtils::split(std::vector<icu::UnicodeString>& result, const icu::UnicodeString& s, const char* pattern)
    {
        return split(result, s, icu::UnicodeString(pattern));
    }

    std::vector<icu::UnicodeString>& StringUtils::split(std::vector<icu::UnicodeString>& result, const icu::UnicodeString& s, const wchar_t* pattern)
    {
        return split(result, s, StringConvUtils::wtou(std::wstring(pattern)));
    }

    int8_t StringUtils::matches(const icu::UnicodeString& s, const icu::UnicodeString& pattern)
    {
        UErrorCode status = U_ZERO_ERROR;
        icu::RegexMatcher m(pattern, 0, status);
        m.reset(s);
        return (int8_t) (m.matches(status) ? 1 : 0);
    }

    icu::UnicodeString StringUtils::joinArray(const std::vector<icu::UnicodeString>& v, const icu::UnicodeString& delim)
    {
        std::wstringstream ss;
        for (size_t i = 0; i < v.size(); ++i) {
            if (i != 0) {
                ss << StringConvUtils::utow(delim);
            }
            auto w = StringConvUtils::utow(v[i]);
            ss << w;
        }
        auto s = ss.str();
        return StringConvUtils::wtou(s);
    }

    icu::UnicodeString StringUtils::joinArray(const std::vector<icu::UnicodeString>& v, const char* delim)
    {
        return joinArray(v, icu::UnicodeString(delim));
    }

    icu::UnicodeString StringUtils::joinArray(const std::vector<icu::UnicodeString>& v, const wchar_t* delim)
    {
        return joinArray(v, StringConvUtils::wtou(std::wstring(delim)));
    }

    UnicodeIterator StringUtils::beginIterator(icu::UnicodeString s)
    {
        return UnicodeIterator(s);
    }

    UnicodeIterator StringUtils::endIterator(icu::UnicodeString s)
    {
        auto i = UnicodeIterator(s);
        i.pos = s.countChar32();
        return i;
    }

    template<typename T>
    void _spf(T &s, const T fmt, FORMAT_STRING_ARGS_CPP)
    {
        FormatStdString(s, fmt.c_str(), FORMAT_STRING_ARGS_PASS);
    }
        
    std::string StringUtils::spf(const std::string fmt, FORMAT_STRING_ARGS_CPP)
    {
        std::string res;
        _spf<std::string>(res, fmt, FORMAT_STRING_ARGS_PASS);
        res.resize(res.find((char)0));
        return res;
    }

    std::wstring StringUtils::spf(const std::wstring fmt, FORMAT_STRING_ARGS_CPP)
    {
        std::wstring res;
        _spf<std::wstring>(res, fmt, FORMAT_STRING_ARGS_PASS);
        res.resize(res.find((wchar_t)0));
        return res;
    }

    std::u32string StringUtils::spf(const std::u32string fmt, FORMAT_STRING_ARGS_CPP)
    {
        return StringConvUtils::wtou32(spf(StringConvUtils::u32tow(fmt), FORMAT_STRING_ARGS_PASS));
    }

    icu::UnicodeString StringUtils::spf(const icu::UnicodeString fmt, FORMAT_STRING_ARGS_CPP)
    {
        return StringConvUtils::wtou(spf(StringConvUtils::utow(fmt), FORMAT_STRING_ARGS_PASS));
    }

    static auto hrc_offset = std::chrono::system_clock::now().time_since_epoch() - std::chrono::high_resolution_clock::now().time_since_epoch();

    std::string StringUtils::timeStr(int64_t nanoseconds)
    {
        std::chrono::nanoseconds dur(nanoseconds);
        std::chrono::time_point<std::chrono::system_clock> dt(std::chrono::duration_cast<std::chrono::microseconds>(dur + hrc_offset));
        auto t = std::chrono::system_clock::to_time_t(dt);
        auto bt = *std::localtime(&t);

        auto mcs = nanoseconds / 1000 % 1000000;

        std::ostringstream oss;

        oss << std::put_time(&bt, "%Y-%m-%dT%H:%M:%S");
        oss << '.' << std::setfill('0') << std::setw(6) << mcs;

        return oss.str();
    }
}
