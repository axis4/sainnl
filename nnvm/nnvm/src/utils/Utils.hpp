/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#pragma once

namespace nnvm {
    class CollectionUtils;
    class finally;
    class PerformanceUtils;
    struct no_separator;
}

#include <predef.h>
#include <vector>
#include <istream>
#include <fstream>
#include <unicode/unistr.h>
#include <unicode/ustream.h>
#include <memory>
#include <inttypes.h>
#include <ByteCode.hpp>
#include <ByteCodeParameter.hpp>
#include <ProcessorSetup.hpp>
#include "../string_format/FormatString.hpp"

#define nnvm_min(a, b) (((a) < (b)) ? (a) : (b))

namespace nnvm {
    class CollectionUtils
    {
    public:
        template <typename T>
        static std::vector<int64_t> sortPermutation(const std::vector<T>& vec, const std::function<int64_t(const T&, const T&)>& compare)
        {
            std::vector<int64_t> p(vec.size());
            for (int64_t i = 0; i < vec.size(); ++i) {
                p[i] = i;
            }
            std::sort(p.begin(), p.end(), [&vec, &compare](int64_t i, int64_t j) { return compare(vec[i], vec[j]); });
            return p;
        }
        
        template <typename T>
        static std::vector<T> applyPermutation(const std::vector<T>& vec, const std::vector<int64_t>& p)
        {
            std::vector<T> sorted_vec;
            sorted_vec.reserve(vec.size());
            std::transform(p.begin(), p.end(), std::back_inserter(sorted_vec), [&vec](int64_t i) { return vec[i]; });
            return sorted_vec;
        }

        template <typename TK, typename TV>
        static TV getOrDefault(std::map<TK, TV>& map, std::function<TV()> defaultFunc, const TK& k)
        {
            auto f = map.find(k);
            if (f == map.end()) {
                map[k] = defaultFunc();
                return map[k];
            }

            return f->second;
        }

        template <typename TK, typename... TOthers, typename TM, typename TV>
        static TV getOrDefault(std::map<TK, TM>& map, std::function<TV()> defaultFunc, const TK& k, const TOthers& ... others)
        {
            auto f = map.find(k);
            if (f == map.end()) {
                map[k] = TM();
                return getOrDefault(map[k], defaultFunc, others...);
            }

            return getOrDefault(f->second, defaultFunc, others...);
        }

        template <typename TK, typename TV>
        static void set(std::map<TK, TV>& map, const TV& v, const TK& k)
        {
            map[k] = v;
        }

        template <typename TK, typename... TOthers, typename TM, typename TV>
        static void set(std::map<TK, TM>& map, const TV& v, const TK& k, const TOthers& ... others)
        {
            auto f = map.find(k);
            if (f == map.end()) {
                map[k] = TM();
                set(map[k], v, others...);
            }
            else {
                set(f->second, v, others...);
            }
        }
    };

    class finally
    {
        std::function<void(void)> functor;
    public:
        explicit finally(const std::function<void(void)> &functor);
        ~finally();
    };

    class PerformanceUtils
    {
    public:
        static void timeit(std::function<void()> call);
        static void timeit(std::function<void()> call, const icu::UnicodeString& title);
        static void timeit(std::function<void()> call, std::function<void(int64_t)> reg);
        
        template<typename T>
        static T timeit(std::function<T()> call)
        {
            return timeit(call, [](int64_t v) { std::cout << "[" << double(v)/1000000 << "ms]" << std::endl; });
        }
        
        template<typename T>
        static T timeit(std::function<T()> call, const icu::UnicodeString& title)
        {
            std::cout << StringConvUtils::utou8(title) << " ";
            return timeit(call);
        }
        
        template<typename T>
        static T timeit(std::function<T()> call, const std::function<void(int64_t)> &reg)
        {
            const auto tp1 = std::chrono::high_resolution_clock::now();
            finally end([&]{
                const auto tp2 = std::chrono::high_resolution_clock::now();
                const auto d = std::chrono::duration_cast<std::chrono::nanoseconds>(tp2 - tp1);
                reg(d.count());
            });
            return call();
        }

        static int64_t now_nanoseconds();
        static int64_t nanoseconds(std::chrono::time_point<std::chrono::steady_clock, std::chrono::nanoseconds> start);
        static int64_t nanoseconds(std::chrono::time_point<std::chrono::steady_clock, std::chrono::nanoseconds> start, std::chrono::time_point<std::chrono::steady_clock, std::chrono::nanoseconds> end);
        static int64_t nanoseconds(int64_t start);
        static int64_t nanoseconds(int64_t start, int64_t end);
    };

    struct no_separator : std::numpunct<char> {
    protected:
        virtual string_type do_grouping() const
        { return "\000"; } // groups of 0 (disable)
    };
}
