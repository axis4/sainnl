/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#pragma once

#include "predef.h"
#include "Disposable.hpp"
#include <inttypes.h>
#include <memory>

namespace nnvm {
    template<typename TResult, typename TContext>
    class Generator : public Disposable, public std::enable_shared_from_this<Generator<TResult, TContext>>
    {
    public:
        class iterator
        {
            friend class Generator<TResult, TContext>;
        public:
            typedef iterator self_type;
            typedef TResult value_type;
            typedef TResult& reference;
            typedef TResult* pointer;
            self_type operator++()
            {
                self_type i = *this;
                this->generator->add(1);
                return i;
            }
            self_type operator++(int junk)
            {
                this->generator->add(1);
                return *this;
            }
            self_type operator+=(int64_t n)
            {
                this->generator->add(n);
                return *this;
            }
            reference operator*()
            {
                return *this->generator->deref();
            }
            pointer operator->()
            {
                return this->generator->deref();
            }
            bool operator==(const self_type& rhs)
            {
                return !this->operator!=(rhs);
            }
            bool operator!=(const self_type& rhs)
            {
                return this->generator->any();
            }
        private:
            iterator(std::shared_ptr<Generator<TResult, TContext>> generator) : generator(generator)
            {
                this->generator->current();
            }
            std::shared_ptr<Generator<TResult, TContext>> generator;
        };
        friend class iterator;
        friend class std::shared_ptr<Generator<TResult, TContext>>;
        
        static std::shared_ptr<Generator<TResult, TContext>> create(std::function<int8_t(std::shared_ptr<TContext>, TResult&)> yielder, std::shared_ptr<TContext> context)
        {
            return std::make_shared<Generator<TResult, TContext>>(yielder, context);
        }
        int8_t next()
        {
            if (this->ok) {
                this->ok = this->yielder(this->ctx, this->last) ? 1 : 0;
            }
            return this->ok;
        }
        TResult current()
        {
            if (this->ok < 0) {
                this->next();
            }
            return this->last;
        }
        std::tuple<iterator, iterator> range()
        {
            return std::tuple<iterator, iterator>(this->begin(), this->end());
        }
        iterator begin()
        {
            return iterator(this->shared_from_this());
        }
        iterator end()
        {
            return iterator(this->shared_from_this());
        }
        Generator(std::function<int8_t(std::shared_ptr<TContext>, TResult&)> yielder, std::shared_ptr<TContext> context)
        : yielder(yielder), ctx(context), ok(-1)
        {
        }
        Generator(Generator<TResult, TContext>& obj)
        : yielder(obj.yielder), ctx(obj.ctx), last(obj.last), ok(obj.ok)
        {
        }
    private:
        std::function<int8_t(std::shared_ptr<TContext>, TResult&)> yielder;
        std::shared_ptr<TContext> ctx;
        TResult last;
        int8_t ok;
        
        int8_t any() const
        {
            return this->ok;
        }
        TResult* deref() const
        {
            return (TResult*)&this->last;
        }
        void add(int64_t n)
        {
            for (auto k = 0; k < n; ++k) {
                this->next();
            }
        }
    };
}
