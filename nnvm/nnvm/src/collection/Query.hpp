/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#pragma once

namespace nnvm {
    class QueryException;

    template<typename TItem>
    class IteratorStatus;
    template<typename TItem>
    class Iterator;

    template<typename TItem, typename TResult>
    class Select;
    template<typename TItem, typename TResult>
    class SelectMany;
    template<typename TItem>
    class Where;
    template<typename TItem>
    class Query;
}

#include "predef.h"
#include <exception>
#include <memory>
#include <functional>
#include <inttypes.h>
#include <unicode/unistr.h>
#include "StringConv.hpp"

namespace nnvm {
    class QueryException : public std::runtime_error
    {
    public:
        QueryException(const icu::UnicodeString& source, const icu::UnicodeString& message);
    private:
        icu::UnicodeString source;
        icu::UnicodeString message;
    };


    template<typename TItem>
    class IteratorStatus
    {
    };

    template<typename TItem>
    class Iterator
    {
    public:
        typedef Iterator<TItem> self_type;
        typedef TItem value_type;
        typedef TItem& reference;
        typedef TItem* pointer;
        
        Iterator() : status(std::make_shared<IteratorStatus<TItem>>())
        {
        }

        virtual int8_t hasNext()
        {
            return 0;
        }
        virtual void next()
        {
        }
        virtual TItem current()
        {
            throw QueryException(USS("Iterator"), USS("No data"));
        }
        self_type begin()
        {
            return *this;
        }
        self_type end()
        {
            return *this;
        }
        self_type operator++()
        {
            this->next();
            return *this;
        }
        self_type operator++(int junk)
        {
            this->next();
            return *this;
        }
        self_type operator+=(int64_t n)
        {
            while (n) {
                --n;
                this->next();
            }
            return *this;
        }
        reference operator*()
        {
            return *this->current(); // TODO
        }
        pointer operator->()
        {
            return this->current(); // TODO
        }
        operator bool()
        {
            return this->hasNext();
        }
        bool operator==(const self_type& obj)
        {
            return !this->operator!=(obj);
        }
        bool operator!=(const self_type& obj)
        {
            if (this->status != obj.status) {
                throw QueryException(USS("Iterator"), USS("Different iterators"));
            }
            return this->hasNext();
        }
        
    protected:
        std::shared_ptr<IteratorStatus<TItem>> status;
    };

    template<typename TItem>
    class Iterable
    {
    public:
        virtual Iterator<TItem> begin() = 0;
        virtual Iterator<TItem> end() = 0;
    };

    template<typename TItem, typename TResult>
    class Select : Query<TResult>
    {
    public:
        Select(Query<TItem> source, std::function<TResult(TItem)> selector) : source(source), selector(selector) {}
    private:
        Query<TItem> source;
        std::function<TResult(TItem)> selector;
    };

    template<typename TItem, typename TResult>
    class SelectMany : Select<TItem, Query<TResult>>
    {
    public:
        SelectMany(Query<TItem> source, std::function<Query<TResult>(TItem)> selector) : source(source), selector(selector) {}
    private:
        Query<TItem> source;
        std::function<Query<TResult>(TItem)> selector;
    };

    template<typename TItem>
    class Where : Query<TItem>
    {
    public:
        Where(Query<TItem> source, std::function<int8_t(TItem)> predicate) : source(source), predicate(predicate) {}
    private:
        Query<TItem> source;
        std::function<int8_t(TItem)> predicate;
    };

    template<typename TItem>
    class Query : Iterable<TItem>
    {
    public:
        Query(Iterator<TItem> source) : source(source)
        {
            
        }

        Query(const Query<TItem>& obj) : source(obj.source)
        {

        }

        template<typename TResult>
        Select<TItem, TResult> select(std::function<TResult(TItem)> selector)
        {
            return Select<TItem, TResult>(this, selector);
        }
        Where<TItem> where(std::function<int8_t(TItem)> predicate)
        {
            return Where<TItem>(this, predicate);
        }
        template<typename TResult>
        SelectMany<TItem, TResult> selectMany(std::function<Query<TResult>(TItem)> selector)
        {
            return SelectMany<TItem, TResult>(this, selector);
        }
    private:
        Iterator<TItem> source;
    };
    //
    //Permutator::Permutator(std::shared_ptr<std::map<icu::UnicodeString, int64_t>> range_states, std::list<icu::UnicodeString> ordered_range, std::map<icu::UnicodeString, std::tuple<int64_t, int64_t>> ranges)
    //: Generator([this](std::shared_ptr<std::map<icu::UnicodeString, int64_t>> ctx, std::map<icu::UnicodeString, int64_t>& res) {
    //    return this->yield(ctx, res);
    //}, range_states), ranges(ranges), ordered_range(ordered_range)
    //{
    //}
    //
    //int8_t Permutator::yield(std::shared_ptr<std::map<icu::UnicodeString, int64_t>> ctx, std::map<icu::UnicodeString, int64_t>& res)
    //{
    //    if (!ctx || !ctx->size() || ctx->find(USS("")) != ctx->end()) {
    //        return 0;
    //    }
    //    res = *ctx;
    //    for (auto i = ordered_range.begin(); i != ordered_range.end(); ++i) {
    //        ctx->operator[](*i)++;
    //        if (ctx->operator[](*i) <= std::get<1>(ranges[*i])) {
    //            break;
    //        }
    //        ctx->operator[](*i) = std::get<0>(ranges[*i]);
    //    }
    //    return 1;
    //}
    //
    //icu::UnicodeString Permutator::asString(const std::map<icu::UnicodeString, int64_t>& s)
    //{
    //    std::vector<icu::UnicodeString> res;
    //    for (auto i = ordered_range.begin(); i != ordered_range.end(); ++i) {
    //        auto m = s.find(*i);
    //        if (m != s.end()) {
    //            res.push_back(StringUtils::spf(USS("{0}={1}"), StringConvUtils::utow(m->first), m->second));
    //        }
    //    }
    //    return StringUtils::joinArray(res, USS("§"));
    //}
    //
    //FilteredPermutator::FilteredPermutator(std::shared_ptr<std::map<icu::UnicodeString, int64_t>> range_states, std::list<icu::UnicodeString> ordered_range, std::map<icu::UnicodeString, std::tuple<int64_t, int64_t>> ranges, std::function<int8_t(std::map<icu::UnicodeString, int64_t>&)> filter) : Permutator(range_states, ordered_range, ranges), filter(filter)
    //{
    //}
    //
    //int8_t FilteredPermutator::yield(std::shared_ptr<std::map<icu::UnicodeString, int64_t>> ctx, std::map<icu::UnicodeString, int64_t>& res)
    //{
    //    int8_t r = 1;
    //    while ((r = this->Permutator::yield(ctx, res)) && !filter(res));
    //    return r;
    //}
    //
    //ProjectedFilteredPermutator::ProjectedFilteredPermutator(std::shared_ptr<std::map<icu::UnicodeString, int64_t>> range_states, std::list<icu::UnicodeString> ordered_range, std::map<icu::UnicodeString, std::tuple<int64_t, int64_t>> ranges, std::function<int8_t(std::map<icu::UnicodeString, int64_t>&)> filter, std::set<icu::UnicodeString> indexes) : FilteredPermutator(range_states, ordered_range, ranges, filter), indexes(indexes), passed(std::set<icu::UnicodeString>())
    //{
    //}
    //
    //int8_t ProjectedFilteredPermutator::yield(std::shared_ptr<std::map<icu::UnicodeString, int64_t>> ctx, std::map<icu::UnicodeString, int64_t>& res)
    //{
    //    int8_t r = 1;
    //    while ((r = this->FilteredPermutator::yield(ctx, res)) && passed.find(asString(res)) != passed.end());
    //    if (r) {
    //        passed.insert(asString(res));
    //    }
    //    return r;
    //}
    //
    //icu::UnicodeString ProjectedFilteredPermutator::asString(const std::map<icu::UnicodeString, int64_t>& s)
    //{
    //    std::vector<icu::UnicodeString> res;
    //    for (auto i = ordered_range.begin(); i != ordered_range.end(); ++i) {
    //        if (indexes.find(*i) != indexes.end()) {
    //            auto m = s.find(*i);
    //            if (m != s.end()) {
    //                res.push_back(StringUtils::spf(USS("{0}={1}"), StringConvUtils::utow(m->first), m->second));
    //            }
    //        }
    //    }
    //    return StringUtils::joinArray(res, USS("§"));
    //}
}