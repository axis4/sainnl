/*
 Copyright 2013-2016 Sergey Ionov

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#pragma once

#include "predef.h"

namespace nnvm {
    namespace grid {
        NNVM_EXPORTED class ServerContext;
        NNVM_EXPORTED class Socket;
        NNVM_EXPORTED class Connection;
        NNVM_EXPORTED enum class ProtocolType;
        NNVM_EXPORTED class Protocol;
        NNVM_EXPORTED class ConnectedProtocol;
        NNVM_EXPORTED enum class StructuredProtocolError;
        NNVM_EXPORTED class StructuredProtocolErrorCategory;
        template<typename T>
        NNVM_EXPORTED class StructuredProtocol;
        NNVM_EXPORTED class ContinuousLock;
        NNVM_EXPORTED class ContinuousStream;
        template<typename T>
        NNVM_EXPORTED class ContinuousStructuredProtocol;
    }
}

#include <system_error>
#include <atomic>
#include <condition_variable>
#include <ios>
#include <iostream>
#include <sstream>
#include <vector>
#include <thread>
#include <list>
#include <map>
#include <cinttypes>
#include <unicode/unistr.h>
#include <Disposable.hpp>
#include <StringConv.hpp>
#include <BinaryUtils.hpp>
#include <LoaderModel.hpp>

namespace nnvm {
    namespace grid {
        class ServerContext
        {
        public:
            std::shared_ptr<void> data;
        };

        enum class ProtocolType {
            TCP,
            UDP
        };

        class Socket : public Disposable, public std::tuple<std::string, int32_t, ProtocolType> {
        public:
            Socket(std::string address, int32_t port, ProtocolType type);
            Socket(const Socket &obj);
            std::string getAddress() const;
            int32_t getPort() const;
            ProtocolType getType() const;
        };

        class Connection : public Disposable {
        public:
            virtual Socket getRemote() = 0;
            virtual Socket getLocal() = 0;
            virtual void handleSend(std::shared_ptr<std::vector<int8_t>> data) = 0;
            virtual void start() = 0;
            virtual void stop() = 0;
        };

        class Protocol : public Disposable {
        public:
            explicit Protocol(std::shared_ptr<Logger> logger);

            void setRunningContext(const ServerContext& runningContext);

            virtual void handleConnect(std::shared_ptr<Connection> connection, const std::error_code &error) = 0;

            virtual void handleReceive(std::shared_ptr<Connection> connection,
                                       const std::vector<int8_t>& buffer, const std::error_code &error) = 0;

            virtual void handleSendEnd(const std::error_code &error, int64_t bytes_transferred,
                                         std::shared_ptr<std::vector<int8_t>> data) = 0;

            icu::UnicodeString id();
        protected:
            ServerContext runningContext;
            icu::UnicodeString guid;
            std::shared_ptr<Logger> logger;
        };

        class ConnectedProtocol : public Protocol, public std::enable_shared_from_this<ConnectedProtocol> {
        public:
            explicit ConnectedProtocol(std::shared_ptr<Logger> logger);

            ~ConnectedProtocol() override;

            virtual void onConnect(const Socket& connection) = 0;

            virtual void onReceive(const Socket& connection, const std::vector<int8_t>& buffer) = 0;

            virtual void onSendEnd(const std::error_code &error, int64_t bytes_transferred,
                                   std::shared_ptr<std::vector<int8_t>> data) = 0;

            virtual void onReceiveError(const Socket& connection, const std::error_code &error) = 0;

            virtual void onConnectError(const Socket& connection, const std::error_code &error) = 0;

            virtual void send(ProtocolType type, icu::UnicodeString address, int32_t port,
                      std::shared_ptr<std::vector<int8_t>> data);

            void handleConnect(std::shared_ptr<Connection> connection, const std::error_code &error) override;

            void handleReceive(std::shared_ptr<Connection> connection,
                               const std::vector<int8_t>& buffer, const std::error_code &error) override;

            void handleSendEnd(const std::error_code &error, int64_t bytes_transferred,
                               std::shared_ptr<std::vector<int8_t>> data) override;

        private:
            std::map<ProtocolType, std::map<std::string, std::map<int32_t, std::shared_ptr<Connection>>>> connections;
        };

        enum class StructuredProtocolError : int
        {
            failed_deserialize = 1
        };

        class StructuredProtocolErrorCategory : public std::error_category
        {
            const char* name() const noexcept override
            {
                return "StructuredProtocolError";
            }

            std::string message(int ec) const override
            {
                std::string msg;
                switch (StructuredProtocolError(ec))
                {
                    case StructuredProtocolError::failed_deserialize:
                        msg = "Failed to deserialize protocol payload.";
                        break;
                    default:
                        msg = "unknown.";
                }

                return msg;
            }

            std::error_condition default_error_condition(int ec) const noexcept override
            {
                return {ec, *this};
            }
        };

        template<typename T>
        class StructuredProtocol : public ConnectedProtocol
        {
        public:
            explicit StructuredProtocol(std::shared_ptr<Logger> logger) : ConnectedProtocol(std::move(logger)) {}

            void onConnect(const Socket& connection) override = 0;

            virtual void onReceive(const Socket& connection, const T& buffer) = 0;

            void onReceiveError(const Socket& connection, const std::error_code &error) override = 0;

            void onSendEnd(const std::error_code &error, int64_t bytes_transferred,
                                   std::shared_ptr<std::vector<int8_t>> data) override = 0;

            void onConnectError(const Socket& connection, const std::error_code &error) override = 0;

            virtual int8_t serialize(const Socket& connection, const T& from, std::vector<int8_t>& to) = 0;

            virtual int8_t deserialize(const Socket& connection, const std::vector<int8_t>& from, T& to) = 0;

            virtual void sendStructure(const Socket& connection, const T& data)
            {
                safe();
                auto tmp = std::make_shared<std::vector<int8_t>>();
                if (serialize(connection, data, *tmp)) {
                    send(connection.getType(), StringConvUtils::u8tou(connection.getAddress()), connection.getPort(), tmp);
                }
            }

            virtual void sendStructure(ProtocolType type, icu::UnicodeString address, int32_t port, const T& data)
            {
                safe();
                auto tmp = std::make_shared<std::vector<int8_t>>();
                if (serialize(Socket(StringConvUtils::utou8(address), port, type), data, *tmp)) {
                    send(type, address, port, tmp);
                }
            }

            void onReceive(const Socket& connection, const std::vector<int8_t>& buffer) override
            {
                safe();
                T tmp;
                if (deserialize(connection, buffer, tmp)) {
                    onReceive(connection, tmp);
                }
                else {
                    onReceiveError(connection,
                                   std::error_code(static_cast<int>(StructuredProtocolError::failed_deserialize),
                                                   StructuredProtocolErrorCategory()));
                }
            }
        };

        class ContinuousLock
        {
        public:
            explicit ContinuousLock(ContinuousStream& stream);
            ~ContinuousLock();

        private:
            std::unique_lock<std::mutex> lk;
        };

        class ContinuousStream
        {
        public:
            std::shared_ptr<std::stringstream> stream;
            int64_t streamSize;
            int64_t streamDeprecation;
            std::condition_variable cv;
            std::mutex cvMutex;

            std::shared_ptr<ContinuousLock> with_lock();
        };

        template<typename T>
        class ContinuousStructuredProtocol : public StructuredProtocol<T>
        {
        public:
            ContinuousStructuredProtocol() : mainLockStream(std::make_shared<ContinuousStream>()) { }

            ~ContinuousStructuredProtocol() override {
                for (auto & stream : streams) {
                    std::get<0>(stream.second)->stream->close();
                    std::get<0>(stream.second)->streamDeprecation = 0;
                    std::get<1>(stream.second)->join();
                }
            }

            int8_t deserialize(const Socket& connection, const std::vector<int8_t>& from, T& to) override
            {
                this->safe();
                std::tuple<std::shared_ptr<ContinuousStream>, std::shared_ptr<std::thread>> target;
                {
                    auto handler = mainLockStream->with_lock();

                    // clean up lost streams
                    std::list<Socket> toDelete;
                    for (auto & stream : streams) {
                        if (isDeprecated(stream.second)) {
                            reset(std::get<0>(stream.second));
                            std::get<0>(stream.second)->stream->close();
                            toDelete.push_back(stream.first);
                        }
                    }

                    for (auto & i : toDelete) {
                        streams.erase(i);
                    }

                    auto reader = streams.find(connection);
                    if (reader == streams.end()) {
                        auto stream = std::make_shared<ContinuousStream>();
                        stream->stream = std::make_shared<std::stringstream>();
                        stream->streamDeprecation = timeNow();
                        stream->streamSize = 0;
                        auto storedConnection = connection;
                        auto thread = std::make_shared<std::thread>([this, storedConnection, stream]() {
                            while(stream->streamDeprecation > 0) {
                                T tmp;
                                if (deserialize(storedConnection, stream->stream, tmp)) {
                                    this->onReceive(storedConnection, tmp);
                                } else {
                                    // reset stream
                                    this->reset(stream);
                                    this->onReceiveError(storedConnection,
                                                         std::error_code(
                                                                 static_cast<int>(StructuredProtocolError::failed_deserialize),
                                                                 StructuredProtocolErrorCategory()));
                                }
                            }
                        });
                        target = std::tuple<std::shared_ptr<ContinuousStream>, std::shared_ptr<std::thread>>();
                        streams[connection] = target;
                    } else {
                        target = reader->second;
                        std::get<0>(target)->streamDeprecation = timeNow();
                    }
                }


                if (std::get<0>(target)->stream->tellg() == std::get<0>(target)->streamSize) {
                    reset(target);
                }
                {
                    auto handler = std::get<0>(target)->with_lock();
                    std::get<0>(target)->streamSize += from.size();
                    BinaryUtils::asStream(from, *std::get<0>(target)->stream);
                }
                return 1;
            }

            virtual int8_t deserialize(std::shared_ptr<std::istream> from, std::function<void(T&)> to) = 0;

            virtual int64_t timeNow() = 0;

            virtual int8_t isDeprecated(std::tuple<std::shared_ptr<ContinuousStream>, std::shared_ptr<std::thread>> target)
            {
                this->safe();
                return timeNow() - std::get<0>(target)->streamDeprecation > deprecationThreshold;
            }

            void onReceive(const Socket& connection, const std::vector<int8_t>& buffer) override
            {
                this->safe();
                T tmp;
                deserialize(connection, buffer, tmp);
            }

            const int64_t deprecationThreshold = 60LL * 1000LL * 1000LL * 1000LL; // 1 min

        private:
            void reset(std::shared_ptr<ContinuousStream> stream)
            {
                this->safe();
                auto handler = stream->with_lock();
                stream->streamSize = 0;
                stream->stream->seekg(0, stream->stream->beg);
                stream->stream->seekp(0, stream->stream->beg);
                stream->streamDeprecation = timeNow();
            }

            std::map<Socket, std::tuple<std::shared_ptr<ContinuousStream>, std::shared_ptr<std::thread>>> streams;

            std::shared_ptr<ContinuousStream> mainLockStream;
        };
    }
}
