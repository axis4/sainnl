/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#pragma once

#include "predef.h"

namespace nnvm {
    NNVM_EXPORTED class SelfCheckedNeuron;
    NNVM_EXPORTED class FunctionNeuron;
    NNVM_EXPORTED class ThreadedNeuron;
    template<typename T> NNVM_EXPORTED class IONeuron;
    template<typename T> NNVM_EXPORTED class QueuedIONeuron;
    template<typename T> NNVM_EXPORTED class RealtimeIONeuron;
}

#include <unicode/unistr.h>
#include <atomic>
#include <mutex>
#include <memory>
#include <map>
#include <inttypes.h>
#include <ByteCodeBlock.hpp>
#include <LanguageModel.hpp>
#include <ProcessorSetup.hpp>
#include <ByteCodeWriterSetup.hpp>
#include <ProcessorKnownParameters.hpp>

namespace nnvm {
    class SelfCheckedNeuron : public ExecutableNeuron
    {
    public:
        SelfCheckedNeuron(std::shared_ptr<MachineSetup> setup, std::shared_ptr<LibraryLoader> loader);
        void call(std::shared_ptr<ByteCodeBlock> block, std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock> > > blocks) override;
    protected:
        virtual icu::UnicodeString getEligibleType() = 0;
        virtual void safeCall(std::shared_ptr<ByteCodeBlock> block, std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock> > > blocks) = 0;
        void removed(std::shared_ptr<ByteCodeBlock> code, std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock> > > codes) override;
    };

    class FunctionNeuron : public SelfCheckedNeuron, public WritableNeuron, public ParsedNeuron
    {
    public:
        FunctionNeuron(std::shared_ptr<MachineSetup> setup, std::shared_ptr<LibraryLoader> loader);
        void writeSourceBlock(std::shared_ptr<ByteCodeBlock> code, std::shared_ptr<std::ostream> stream) override;
        ParsedNeuronArgumentType argumentType(int64_t argumentIndex, int64_t argumentTotal) override;
        std::vector<std::shared_ptr<ByteCodeBlock> > compile(icu::UnicodeString name, LanguageCall& call, CompileContext& cctx, std::shared_ptr<Context> ctx) override;
    protected:
        virtual int64_t getArgumentCount() = 0;
    };

    struct ThreadNeuronDescriptor
    {
        std::atomic<int8_t> running;
        std::shared_ptr<std::thread> thread;
        icu::UnicodeString block;
        std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock> > > blocks;
    };

    class ThreadedNeuron : public SelfCheckedNeuron
    {
    public:
        ThreadedNeuron(std::shared_ptr<MachineSetup> setup, std::shared_ptr<LibraryLoader> loader);
        virtual ~ThreadedNeuron();
    protected:
        virtual void safeCall(std::shared_ptr<ByteCodeBlock> block, std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock> > > blocks);
        virtual void removed(std::shared_ptr<ByteCodeBlock> code, std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock> > > codes);
        virtual void subprocessLogic(std::shared_ptr<ByteCodeBlock> block) = 0; // launch in another thread
        std::map<icu::UnicodeString, std::shared_ptr<ThreadNeuronDescriptor> > threads;
    private:
        void threadFunc(std::shared_ptr<ThreadNeuronDescriptor>);
        // keep setup until after threads got destroyed
        std::shared_ptr<MachineSetup> lockedSetup;
    };

    template<typename TData>
    class IONeuron : public ThreadedNeuron
    {
    public:
        IONeuron(std::shared_ptr<MachineSetup> setup, std::shared_ptr<LibraryLoader> loader)
                : ThreadedNeuron(setup, loader) { }
    protected:
        virtual int8_t readData(TData& data) = 0; // plugin <|- IO
        virtual int8_t writeData(TData& data) = 0; // plugin -|> IO
        virtual int8_t getInputData(TData& data) = 0; // plugin <|- memory(IO in)
        virtual int8_t storeInputData(TData& data) = 0; // plugin -|> memory(IO in)
        virtual int8_t getOutputData(TData& data) = 0; // plugin <|- memory(IO out)
        virtual int8_t storeOutputData(TData& data) = 0; // plugin -|> memory(IO out)
        void safeCall(std::shared_ptr<ByteCodeBlock> block, std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock> > > blocks)
        {
            ThreadedNeuron::safeCall(block, blocks);
            
            std::map<icu::UnicodeString, std::shared_ptr<ByteCodeParameter> > params;
            block->getParams(std::vector<icu::UnicodeString>{
                    LinkParams::ACTIVATION + ElementParams::SUM_SUFFIX,
                    LinkParams::STREAM + ElementParams::SUM_SUFFIX,
                    ElementParams::THRESHOLD,
                    ElementParams::BIAS }, params);
            
            auto strSum = params[LinkParams::STREAM + ElementParams::SUM_SUFFIX];
            
            setup->getProcessorSetup()->offsetByMode(strSum);
            
            lock.lock();
            TData to;
            switch (setup->getProcessorSetup()->mode)
            {
                case ProcessorMode::BYTE:
                    to = (TData)strSum->value.byteValue;
                    break;
                case ProcessorMode::INTEGER:
                    to = (TData)strSum->value.integerValue;
                    break;
                case ProcessorMode::REAL:
                    to = (TData)strSum->value.doubleValue;
                    break;
                default:
                    break;
            }
            storeOutputData(to);
            
            TData t;
            if (getInputData(t)) {
                auto tmp = std::make_shared<ByteCodeParameter>(ElementParams::SUM, (double)0);
                std::shared_ptr<ByteCodeParameter> in = nullptr;
                if (block->tryGetParameter(ElementParams::SUM, in)) {
                    if (in->getType() != ByteCodeParameterTypes::DOUBLE) {
                        in = tmp;
                        block->setParameter(tmp);
                    }
                }
                else {
                    in = tmp;
                    block->setParameter(tmp);
                }
                
                in->value.doubleValue += (double)t;
            }
            lock.unlock();
        }
        void subprocessLogic(std::shared_ptr<ByteCodeBlock> block)
        {
            lock.lock();
            TData t;
            if (readData(t)) {
                storeInputData(t);
            }
            if (getOutputData(t)) {
                writeData(t);
            }
            lock.unlock();
        }
        std::mutex lock;
    };

    const int64_t IO_TICK_TIME = 10;

    template<typename TData>
    class QueuedIONeuron : public IONeuron<TData>
    {
    public:
        QueuedIONeuron(std::shared_ptr<MachineSetup> setup, std::shared_ptr<LibraryLoader> loader)
                : IONeuron<TData>(setup, loader) { }
    protected:
        int8_t getInputData(TData& data)
        {
            if (!queueIn.empty()) {
                data = queueIn.front();
                queueIn.erase(queueIn.begin());
                return true;
            }
            return false;
        }
        int8_t storeInputData(TData& data)
        {
            queueIn.push_back(data);
            return true;
        }
        int8_t getOutputData(TData& data)
        {
            if (!queueOut.empty()) {
                data = queueOut.front();
                queueOut.erase(queueOut.begin());
                return true;
            }
            return false;
        }
        int8_t storeOutputData(TData& data)
        {
            queueOut.push_back(data);
            return true;
        }
        std::deque<TData> queueIn;
        std::deque<TData> queueOut;
    };

    template<typename TData>
    class RealtimeIONeuron : public IONeuron<TData>
    {
    public:
        RealtimeIONeuron(std::shared_ptr<MachineSetup> setup, std::shared_ptr<LibraryLoader> loader)
                : IONeuron<TData>(setup, loader) { }
    protected:
        int8_t getInputData(TData& data)
        {
            if (currentIn != nullptr) {
                data = *currentIn;
                currentIn = nullptr;
                return true;
            }
            return false;
        }
        int8_t storeInputData(TData& data)
        {
            currentIn = data;
            std::this_thread::sleep_for(std::chrono::milliseconds(IO_TICK_TIME));
            return true;
        }
        int8_t getOutputData(TData& data)
        {
            if (currentOut != nullptr) {
                data = *currentOut;
                currentOut = nullptr;
                return true;
            }
            return false;
        }
        int8_t storeOutputData(TData& data)
        {
            currentOut = data;
            return true;
        }
        std::atomic<std::unique_ptr<TData> > currentIn;
        std::atomic<std::unique_ptr<TData> > currentOut;
    };
}
