/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#pragma once

#include "predef.h"

namespace nnvm {
    NNVM_EXPORTED class NeuronNames;
}

#include <unicode/unistr.h>

namespace nnvm {
    class NeuronNames {
    public:
        static icu::UnicodeString IN;
        static icu::UnicodeString INPOS;
        static icu::UnicodeString OUT;
        static icu::UnicodeString ADDMULT;
        static icu::UnicodeString BLOCKCALL;
        static icu::UnicodeString BRIDGE;
        static icu::UnicodeString STRING;
        static icu::UnicodeString ADDITIVE;
        static icu::UnicodeString MULTIPLICATIVE;
        static icu::UnicodeString EXIT;
        static icu::UnicodeString TICK;
        static icu::UnicodeString TIME;
        static icu::UnicodeString STDIN;
        static icu::UnicodeString STDOUT;
        static icu::UnicodeString PLUS;
        static icu::UnicodeString MINUS;
        static icu::UnicodeString MULTIPLY;
    };
}
