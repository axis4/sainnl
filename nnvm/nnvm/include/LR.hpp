/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#pragma once

#include "predef.h"

namespace nnvm {
    template<typename TTokenKind> NNVM_EXPORTED class TokenState;
    template<typename TTokenKind> NNVM_EXPORTED class Token;
    template<typename TTokenKind> NNVM_EXPORTED class LR;
}

#include <functional>
#include <vector>
#include <memory>
#include <unicode/unistr.h>
#include <inttypes.h>
#include <StringConv.hpp>

namespace nnvm {
    template<typename TTokenKind>
    class Token
    {
    public:
        Token(std::shared_ptr<void> owner) : owner(owner) {}
        Token(const Token<TTokenKind>& obj)
                : type(obj.type.begin(), obj.type.end()), value(obj.value),
                  subTokens(obj.subTokens.begin(), obj.subTokens.end()), owner(obj.owner) {}
        ~Token()
        {
            type.clear();
            subTokens.clear();
        }

        icu::UnicodeString value;
        std::vector<TTokenKind> type;
        std::vector<Token<TTokenKind> > subTokens;
        std::shared_ptr<void> owner;

        int8_t isOf(TTokenKind type) const
        {
            return std::find(this->type.begin(), this->type.end(), type) != this->type.end() ? 1 : 0;
        }
    };

    template<typename TTokenKind>
    class TokenState
    {
    public:
        static TokenState<TTokenKind> prepare(std::initializer_list<TTokenKind> types, TTokenKind finalType, int64_t weight = 0, int8_t nestable = 1)
        {
            return TokenState<TTokenKind>(types.begin(), types.end(), types.size(), finalType, weight, nestable);
        }
        
        static TokenState<TTokenKind> prepare(std::initializer_list<TTokenKind> types, std::initializer_list<TTokenKind> lookup, TTokenKind finalType, int64_t weight = 0, int8_t nestable = 1)
        {
            return TokenState<TTokenKind>(types.begin(), types.end(), types.size(), lookup.begin(), lookup.end(), finalType, weight, nestable);
        }
        
        template<typename TToken>
        int64_t matches(std::vector<TToken>& tokens)
        {
            int64_t res = captureTokens.size() + lookupTokens.size();
            // checking from the end
            auto t = tokens.rbegin();
            auto s = chainedTokens.rbegin();
            for (; t != tokens.rend() && s != chainedTokens.rend(); ++t, ++s) {
                if (!t->isOf(*s)) {
                    return 0;
                }
            }
            // checking if tokens match full state
            if (s != chainedTokens.rend()) {
                return 0;
            }
            return res;
        }
        
        int64_t weight()
        {
            return this->weightValue;
        }
        
        int64_t capture()
        {
            return this->captureTokens.size();
        }
        
        int64_t lookup()
        {
            return this->chainedTokens.size();
        }
        
        TTokenKind result()
        {
            return this->finalType;
        }
        
        int8_t nestable()
        {
            return this->isNestable;
        }
    private:
        TTokenKind finalType;
        int64_t captureSize;
        int64_t weightValue;
        int8_t isNestable;
        std::vector<TTokenKind> captureTokens;
        std::vector<TTokenKind> lookupTokens;
        std::vector<TTokenKind> chainedTokens;
        
        TokenState(typename std::initializer_list<TTokenKind>::iterator capture_start, typename std::initializer_list<TTokenKind>::iterator capture_end, int64_t captureSize, TTokenKind finalType, int64_t weight, int8_t nestable) : captureTokens(capture_start, capture_end), finalType(finalType), captureSize(captureSize), weightValue(weight), isNestable(nestable)
        {
            chainedTokens.insert(chainedTokens.begin(), captureTokens.begin(), captureTokens.end());
        }
        
        TokenState(typename std::vector<TTokenKind>::iterator capture_start, typename std::vector<TTokenKind>::iterator capture_end, int64_t captureSize, TTokenKind finalType, int64_t weight, int8_t nestable) : captureTokens(capture_start, capture_end), finalType(finalType), captureSize(captureSize), weightValue(weight), isNestable(nestable)
        {
            chainedTokens.insert(chainedTokens.begin(), captureTokens.begin(), captureTokens.end());
        }
        
        TokenState(typename std::initializer_list<TTokenKind>::iterator capture_start, typename std::initializer_list<TTokenKind>::iterator capture_end, int64_t captureSize, typename std::initializer_list<TTokenKind>::iterator lookup_start, typename std::initializer_list<TTokenKind>::iterator lookup_end, TTokenKind finalType, int64_t weight, int8_t nestable) : captureTokens(capture_start, capture_end), lookupTokens(lookup_start, lookup_end), finalType(finalType), captureSize(captureSize), weightValue(weight), isNestable(nestable)
        {
            chainedTokens.insert(chainedTokens.begin(), lookupTokens.begin(), lookupTokens.end());
            chainedTokens.insert(chainedTokens.begin(), captureTokens.begin(), captureTokens.end());
        }
        
        TokenState(typename std::vector<TTokenKind>::iterator capture_start, typename std::vector<TTokenKind>::iterator capture_end, int64_t captureSize, typename std::vector<TTokenKind>::iterator lookup_start, typename std::vector<TTokenKind>::iterator lookup_end, TTokenKind finalType, int64_t weight, int8_t nestable) : captureTokens(capture_start, capture_end), lookupTokens(lookup_start, lookup_end), finalType(finalType), captureSize(captureSize), weightValue(weight), isNestable(nestable)
        {
            chainedTokens.insert(chainedTokens.begin(), lookupTokens.begin(), lookupTokens.end());
            chainedTokens.insert(chainedTokens.begin(), captureTokens.begin(), captureTokens.end());
        }
    };

    template<typename TTokenKind>
    class LR
    {
    public:
        typedef std::function<void(const LR<TokenKind>& lr, typename std::vector<Token<TTokenKind>>::iterator childTokensStart, typename std::vector<Token<TTokenKind>>::iterator childTokensEnd, Token<TTokenKind>& collapsedToken)> TokenVisitor;
        typedef icu::UnicodeString (*TokenTypeString)(TTokenKind& tokenKind);
        
        LR(TTokenKind eofKind, const std::vector<TokenState<TTokenKind>>& tokenStates, const TokenVisitor visitToken, const TokenTypeString showTokenType, std::shared_ptr<void> owner)
            : eofKind(eofKind), tokenStates(tokenStates), visitToken(visitToken), showTokenType(showTokenType), owner(owner) { }

        icu::UnicodeString dumpTokenCollapse(Token<TTokenKind>& token, std::vector<Token<TTokenKind>>& tokens) const {
            auto res = USS("");
            for (auto i = tokens.begin(); i != tokens.end(); ++i) {
                res += USS("[");
                for (auto k = i->type.begin(); k != i->type.end(); ++k) {
                    res += showTokenType(*k) + USS(", ");
                }
                res += USS("], ");
            }
            res += USS(" -> [");
            for (auto k = token.type.begin(); k != token.type.end(); ++k) {
                res += showTokenType(*k) + USS(", ");
            }
            res += USS("]\n");
            return res;
        }
        
        void collapse(std::vector<Token<TTokenKind>>& tokens)
        {
            // append EOF
            Token<TTokenKind> t(owner);
            t.type.push_back(eofKind);
            tokens.push_back(t);
            auto again = true;
            while (again) {
                again = false; // whether restart search after state match
                
                // find greediest match
                int64_t start = 0;
                int64_t max_m = 0;
                int64_t max_w = 0;
                TokenState<TTokenKind>* state = nullptr;
                for (auto i = tokenStates.begin(); i != tokenStates.end(); ++i) {
                    for (long k = 0; tokens.size() >= i->lookup() && k <= tokens.size() - i->lookup(); ++k) {
                        auto extract = subTokens(tokens, k + i->lookup());
                        auto m = i->matches(extract);
                        auto w = i->weight();
                        // if state match
                        if (m > 0 && m >= i->capture()) {
                            if (w > max_w || (w == max_w && m > max_m)) {
                                // already applied -> pass state
                                if (i->capture() == 1) {
                                    auto s = tokens.begin();
                                    std::advance(s, k);
                                    if (s->isOf(i->result())) {
                                        continue;
                                    }
                                }
                                
                                // select best match
                                max_m = m;
                                max_w = w;
                                state = &*i;
                                start = k;
                            }
                        }
                    }
                }
                
                // if found any match
                if (state) {
                    auto start_i = start;
                    auto end_i = start_i + state->capture();
                    
                    auto s = tokens.begin();
                    std::advance(s, start_i);
                    
                    auto e = tokens.begin();
                    if (end_i == tokens.size()) {
                        e = tokens.end();
                    }
                    else {
                        std::advance(e, end_i);
                    }
                    
                    Token<TTokenKind> t(owner);
                    t.value = USS("");
                    t.type.push_back(state->result());
                    for (auto l = s; l != e; ++l) {
                        t.value += l->value;
                        t.subTokens.push_back(*l);
                    }
                    
                    s = tokens.begin();
                    std::advance(s, start_i);

                    visitToken(*this, s, e, t);
                    tokens.erase(s, e);
                    
                    s = tokens.begin();
                    std::advance(s, start_i);
                    tokens.insert(s, t);
                    again = state->nestable() > 0;
                }
            }
            
            // remove EOF
            if (tokens.back().isOf(eofKind)) {
                tokens.pop_back();
            }
        }
        
        std::vector<Token<TTokenKind> > subTokens(std::vector<Token<TTokenKind> >& tokens, int64_t size)
        {
            if (size == tokens.size()) {
                return tokens;
            }
            auto e = tokens.begin();
            std::advance(e, size);
            return std::vector<Token<TTokenKind> >(tokens.begin(), e);
        }
        
    private:
        TTokenKind eofKind;
        std::vector<TokenState<TTokenKind> > tokenStates;
        TokenVisitor visitToken;
        TokenTypeString showTokenType;
        std::shared_ptr<void> owner;
    };
}
