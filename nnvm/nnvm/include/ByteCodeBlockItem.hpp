/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#pragma once

#include "predef.h"

namespace nnvm {
    NNVM_EXPORTED class ByteCodeBlockItem;
}

#include <unicode/unistr.h>
#include <inttypes.h>
#include <tuple>
#include <deque>
#include <map>
#include <Disposable.hpp>
#include <ByteCodeParameter.hpp>
#include <ByteCodeBlock.hpp>

namespace nnvm {
    class ByteCodeBlockItem : public Disposable
    {
    public:
        ByteCodeBlockItem(const icu::UnicodeString& itemName, std::weak_ptr<ByteCodeBlock> owner);
        ByteCodeBlockItem(ByteCodeBlockItem& obj);
        virtual ~ByteCodeBlockItem();
        std::shared_ptr<ByteCodeBlockItem> isolatedCopy();
        std::shared_ptr<ByteCodeBlockItem> isolatedCopy(const icu::UnicodeString& rename);
        std::tuple<std::deque<std::shared_ptr<ByteCodeParameter> >::iterator, std::deque<std::shared_ptr<ByteCodeParameter> >::iterator> getParameters();
        std::shared_ptr<ByteCodeParameter> getParameter(const icu::UnicodeString& name);
        int8_t tryGetParameter(const icu::UnicodeString& name, std::shared_ptr<ByteCodeParameter>& result);
        void setParameter(std::shared_ptr<ByteCodeParameter> parameter);

        std::shared_ptr<ByteCodeParameter> getFastActivation();
        std::shared_ptr<ByteCodeParameter> getFastStream();
        std::shared_ptr<ByteCodeParameter> getFastGeneration();

        icu::UnicodeString& getName();
        std::weak_ptr<ByteCodeBlock> getOwner();
        void changeOwner(std::weak_ptr<ByteCodeBlock> owner);
        void compact();
    private:
        icu::UnicodeString name;
        std::weak_ptr<ByteCodeBlock> owner;

        std::shared_ptr<ByteCodeParameter> fastActivation;
        std::shared_ptr<ByteCodeParameter> fastStream;
        std::shared_ptr<ByteCodeParameter> fastGeneration;

        std::deque<std::shared_ptr<ByteCodeParameter> > parameters;
        std::map<icu::UnicodeString, int64_t> parametersIndex;
    };
}