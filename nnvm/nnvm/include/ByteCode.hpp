/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#pragma once

#include "predef.h"

namespace nnvm {
    NNVM_EXPORTED class ByteCode;
}

#include <unicode/unistr.h>
#include <inttypes.h>
#include <tuple>
#include <deque>
#include <memory>
#include <Disposable.hpp>
#include <ByteCodeBlock.hpp>
#include <ByteCodeLoader.hpp>

namespace nnvm {
    class ByteCode : public Disposable
    {
    public:
        ByteCode(const icu::UnicodeString& packageName, std::shared_ptr<ByteCodeLoader> loader);
        ByteCode(ByteCode& obj);
        virtual ~ByteCode();
        void dispose();
        void addBlock(std::shared_ptr<ByteCodeBlock> block);
        std::tuple<std::deque<std::shared_ptr<ByteCodeBlock> >::iterator, std::deque<std::shared_ptr<ByteCodeBlock> >::iterator> getBlocks();
        std::deque<std::shared_ptr<ByteCodeBlock> >::iterator getBlock(int64_t index);
        std::deque<std::shared_ptr<ByteCodeBlock> >::iterator getBlock(icu::UnicodeString name);
        std::shared_ptr<ByteCodeBlock> getLastBlock();
        icu::UnicodeString& getName();
        void addPackagePath(icu::UnicodeString& path);

    private:
        icu::UnicodeString packageName;
        std::deque<std::shared_ptr<ByteCodeBlock> > blocks;
        std::shared_ptr<ByteCodeLoader> loader;
    };
}