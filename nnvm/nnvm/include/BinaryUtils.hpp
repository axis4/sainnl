/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#pragma once

#include "predef.h"

namespace nnvm {
    NNVM_EXPORTED class BinaryUtils;
}

#include <vector>
#include <istream>
#include <fstream>
#include <unicode/unistr.h>
#include <unicode/ustream.h>
#include <memory>
#include <inttypes.h>

namespace nnvm {
    class BinaryUtils
    {
    public:
        static std::string pathSeparator;
        static std::wstring pathSeparatorWide;
        static icu::UnicodeString pathSeparatorUnicode;
        static std::string packageSeparator;
        static std::wstring packageSeparatorWide;
        static icu::UnicodeString packageSeparatorUnicode;

        static std::shared_ptr<std::ifstream> openStream(const icu::UnicodeString& file, int64_t& err);
        static std::shared_ptr<std::ifstream> openTextStream(const icu::UnicodeString& file, int64_t& err);
        static std::shared_ptr<std::ofstream> openWriteStream(const icu::UnicodeString& file, int64_t& err);
        static std::shared_ptr<std::ofstream> openWriteTextStream(const icu::UnicodeString& file, int64_t& err);

        static int64_t readBinary(std::istream& stream, std::vector<uint8_t>& result, int64_t size);
        static int64_t readBinary(std::istream& stream, std::vector<int8_t>& result, int64_t size);
        static int64_t readBinary(std::istream& stream, std::vector<UChar32>& result, int64_t size);
        static int8_t readByte(std::istream& stream, int8_t& result);
        static int8_t readInteger(std::istream& stream, int16_t& result, std::vector<uint8_t>& decomposed);
        static int8_t readInteger(const std::vector<uint8_t>& decomposed, int16_t& result);
        static int8_t readInteger(std::istream& stream, int32_t& result, std::vector<uint8_t>& decomposed);
        static int8_t readInteger(const std::vector<uint8_t>& decomposed, int32_t& result);
        static int8_t readInteger(std::istream& stream, int64_t& result, std::vector<uint8_t>& decomposed);
        static int8_t readInteger(const std::vector<uint8_t>& decomposed, int64_t& result);
        static int8_t readDouble(std::istream& stream, double& result, std::vector<uint8_t>& decomposed);
        static int8_t readDouble(const std::vector<uint8_t>& decomposed, double& result);
        static int8_t readString(std::istream& stream, icu::UnicodeString& result, std::vector<uint8_t>& decomposed);
        static int8_t readString(const std::vector<uint8_t>& decomposed, icu::UnicodeString& result);

        static int8_t writeBinary(const uint8_t* value, std::ostream& stream, int64_t size);
        static int8_t writeBinary(const std::vector<uint8_t>& value, std::ostream& stream);
        static int8_t writeBinary(const std::vector<int8_t>& value, std::ostream& stream);
        static int8_t writeByte(const int8_t value, const std::shared_ptr<std::ostream> &stream);
        static int8_t writeInteger(const int64_t value, const std::shared_ptr<std::ostream> &stream);
        static int8_t writeInteger(const int32_t value, const std::shared_ptr<std::ostream> &stream);
        static int8_t writeInteger(const int16_t value, const std::shared_ptr<std::ostream> &stream);
        static int8_t writeDouble(const double value, const std::shared_ptr<std::ostream> &stream);
        static int8_t writeString(const icu::UnicodeString& value, const std::shared_ptr<std::ostream> &stream);

        static void asVector(std::istream& ss, std::vector<int8_t>& to);
        static void asStream(const std::vector<int8_t>& from, std::ostream& ss);
    };
}
