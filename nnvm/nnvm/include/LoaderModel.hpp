/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#pragma once

#include "predef.h"

namespace nnvm {
    NNVM_EXPORTED class Context;
}

#include <map>
#include <set>
#include <unicode/unistr.h>
#include <memory>
#include <atomic>
#include <condition_variable>
#include <Disposable.hpp>
#include <Counters.hpp>
#include <MachineSetup.hpp>
#include <ByteCodeLoader.hpp>
#include <ByteCodeDumper.hpp>
#include <LibraryLoader.hpp>
#include <ParsedNeuron.hpp>

namespace nnvm {
    class Context : public Disposable
    {
    public:
        Context(std::shared_ptr<MachineSetup> setup, std::shared_ptr<LibraryLoader> loader, std::shared_ptr<ByteCodeLoader> byteLoader);
        std::function<int64_t()> getNewIndex;
        std::shared_ptr<MachineSetup> setup;
        std::shared_ptr<LibraryLoader> loader;
        std::shared_ptr<ByteCodeLoader> byteLoader;
        std::shared_ptr<ByteCodeDumper> byteDumper;
        std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCode> > > compiled;
        std::shared_ptr<std::set<icu::UnicodeString> > compileStarted;
        std::shared_ptr<std::map<icu::UnicodeString, LanguageBlock> > prepared;
        std::shared_ptr<ByteCode> executed;
        Counters counters;
        std::condition_variable cv;
        std::mutex cvMutex;

        void useByteCode(std::shared_ptr<ByteCode> code);
        std::function<int8_t(const std::map<icu::UnicodeString, std::map<icu::UnicodeString, std::tuple<double, double, double> > >&)> processExternalLinks;
        std::function<int8_t(std::map<icu::UnicodeString, std::map<icu::UnicodeString, std::tuple<double, double, double> > >&)> prepareExternalLinks;
        std::map<icu::UnicodeString, std::shared_ptr<ParsedNeuron> >::iterator getNeuron(icu::UnicodeString blockName);

        void globalBlock();
        void globalRelease();
    };
}
