/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#pragma once

#include "predef.h"

namespace nnvm {
    NNVM_EXPORTED class Templater;
}

#include <memory>
#include <map>
#include <deque>
#include <unicode/unistr.h>
#include <NameParts.hpp>
#include <ByteCodeBlock.hpp>

namespace nnvm {
    class Templater
    {
    public:
        Templater();
        ~Templater();
        
        std::shared_ptr<NameParts> applyRename(std::shared_ptr<NameParts> parts, const std::map<icu::UnicodeString, icu::UnicodeString>& renamings);
        
        void processTemplate(
            std::shared_ptr<ByteCodeBlock> templateBlock,
            const std::map<icu::UnicodeString, icu::UnicodeString>& renamings,
            const std::map<icu::UnicodeString, int8_t>& removals,
            std::shared_ptr<NameParts> templateName,
            std::shared_ptr<NameParts> newName,
            std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock>>> codes,
            std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock>>> newCode,
            std::shared_ptr<std::deque<icu::UnicodeString>> removeCode);
    };
}
