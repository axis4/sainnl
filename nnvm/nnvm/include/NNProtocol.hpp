/*
 Copyright 2013-2016 Sergey Ionov

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#pragma once

#include "predef.h"

// For debugging! Remove now!
#include "../src/utils/Utils.hpp"

namespace nnvm {
    namespace grid {
        NNVM_EXPORTED class NNCounterStatus;
        NNVM_EXPORTED class NNStatus;
        NNVM_EXPORTED class NNFlowItem;
        NNVM_EXPORTED class NNFlow;
        NNVM_EXPORTED class NNPayload;
        NNVM_EXPORTED class NNProtocol;
    }
}

#include <map>
#include <cinttypes>
#include <unicode/unistr.h>
#include <Protocol.hpp>
#include <LoaderModel.hpp>
#include <BinaryUtils.hpp>
#include <utility>

namespace nnvm {
    namespace grid {
        enum NNPayloadType {
            NOOP, // stub type to verify connection

            // request and response types
            STATUS, // request stats on network nodes: timings, performance, count
            STATUS_RESPONSE,
            STATE, // request full network state: nodes, names, edges, parameters - full dump network
            STATE_RESPONSE,

            // request types
            FLOW, // send bordering network state: destination nodes with already weighted values. Process only Bridge nodes.
            SYNC_STEP, // request network to perform next step, useless if network not in sync/debug mode
            ENABLE_SYNC, // request network to switch into sync mode, useless if network not in allowSync mode
            NETWORK_SETUP, // request to start new network via config in payload
            TERMINATE, // request to terminate network

            // response types
            ACK, // response with error or success flags plus maybe some message
        };

        class NNCounterStatus {
        public:
            double min; // ns
            double max; // ns
            double avg; // ns
            double sum; // ns
            std::map<double, double> quantiles; // percentile, ns
        };

        class NNStatus {
        public:
            int8_t verbose;
            int8_t exitCode;
            int8_t running;
            int8_t tolerate_missing;
            int32_t parallelMode;
            int32_t mode;
            int8_t remoteSync;
            int8_t allowSync;
            int8_t allowDebug;
            int8_t allowDistributed;
            int8_t allowRemoteInit;
            int64_t tick;
            int64_t nodeCount;
            int64_t edgeCount;
            int64_t stepCount;
            int64_t uptime; //ms
            NNCounterStatus stepAll;
            NNCounterStatus stepTemplate;
            NNCounterStatus stepLinks;
            NNCounterStatus stepNodes;
            NNCounterStatus node;
            NNCounterStatus addedNodes;
            NNCounterStatus removedNodes;
            std::map<icu::UnicodeString, NNCounterStatus> nodes;
        };

        class NNFlowItem {
        public:
            double activation;
            double stream;
            double generation;
        };

        class NNFlow {
        public:
            std::map<icu::UnicodeString, std::map<icu::UnicodeString, NNFlowItem> > destinations;
        };

        class NNPayload {
        public:
            static const int8_t P1 = (int8_t) 0xA4;
            static const int32_t P2 = (int32_t) 0x24344454;

            NNPayload();
            NNPayload(const NNPayload &obj);

            explicit NNPayload(NNPayloadType type);
            explicit NNPayload(NNStatus status);
            explicit NNPayload(std::shared_ptr<ByteCode> state);
            explicit NNPayload(NNFlow flow);
            explicit NNPayload(icu::UnicodeString message);

            NNPayload(int8_t pointer1, int32_t pointer2, int64_t majorVersion, int64_t minorVersion, NNPayloadType type);
            NNPayload(int8_t pointer1, int32_t pointer2, int64_t majorVersion, int64_t minorVersion, NNStatus status);
            NNPayload(int8_t pointer1, int32_t pointer2, int64_t majorVersion, int64_t minorVersion, std::shared_ptr<ByteCode> state);
            NNPayload(int8_t pointer1, int32_t pointer2, int64_t majorVersion, int64_t minorVersion, NNFlow flow);
            NNPayload(int8_t pointer1, int32_t pointer2, int64_t majorVersion, int64_t minorVersion, icu::UnicodeString message);

            int8_t pointer1;
            int32_t pointer2;
            int64_t majorVersion;
            int64_t minorVersion;
            NNPayloadType type;

            NNStatus status;
            std::shared_ptr<ByteCode> state;
            NNFlow flow;
            icu::UnicodeString message;

            int8_t validate();

        private:
            NNPayload(int8_t pointer1, int32_t pointer2, int64_t majorVersion, int64_t minorVersion, NNPayloadType type,
                      NNStatus status, std::shared_ptr<ByteCode> state, NNFlow flow, icu::UnicodeString message);
        };

        template<typename T>
        class CountedProtocol : public StructuredProtocol<T> {
        public:
            explicit CountedProtocol(std::shared_ptr<Context> ctx) : StructuredProtocol<T>(ctx->setup->getLogger()), ctx(ctx) { }

            void send(ProtocolType type, icu::UnicodeString address, int32_t port, std::shared_ptr<std::vector<int8_t>> data) override {
                this->safe();
                PerformanceUtils::timeit([this, &type, &address, &port, &data]() {
                    this->StructuredProtocol<T>::send(type, address, port, data);
                }, [this](int64_t reg) {
                    //TODO save in counters
                    this->logger->info(StringUtils::spf(USS("Counted send in {0} ms"), reg/1000000.0));
                });
            }

            void sendStructure(const Socket& connection, const T& data) override {
                this->safe();
                PerformanceUtils::timeit([this, &connection, &data]() {
                    this->StructuredProtocol<T>::sendStructure(connection, data);
                }, [this](int64_t reg) {
                    //TODO save in counters
                    this->logger->info(StringUtils::spf(USS("Counted sendStructure in {0} ms"), reg/1000000.0));
                });
            }

            void sendStructure(ProtocolType type, icu::UnicodeString address, int32_t port, const T& data) override {
                this->safe();
                PerformanceUtils::timeit([this, &type, &address, &port, &data]() {
                    this->StructuredProtocol<T>::sendStructure(type, address, port, data);
                }, [this](int64_t reg) {
                    //TODO save in counters
                    this->logger->info(StringUtils::spf(USS("Counted sendStructure in {0} ms"), reg/1000000.0));
                });
            }

            void onReceive(const Socket &connection, const std::vector<int8_t>& buffer) override {
                this->safe();
                PerformanceUtils::timeit([this, &connection, &buffer]() {
                    this->StructuredProtocol<T>::onReceive(connection, buffer);
                }, [this](int64_t reg) {
                    //TODO save in counters
                    this->logger->info(StringUtils::spf(USS("Counted onReceive in {0} ms"), reg/1000000.0));
                });
            }

            int8_t serialize(const Socket& connection, const T &from, std::vector<int8_t> &to) override {
                this->safe();
                return PerformanceUtils::timeit<int8_t>([this, &connection, &from, &to]() {
                    return this->doSerialize(connection, from, to);
                }, [this](int64_t reg) {
                    //TODO save in counters
                    this->logger->info(StringUtils::spf(USS("Counted serialize in {0} ms"), reg/1000000.0));
                });
            }

            int8_t deserialize(const Socket& connection, const std::vector<int8_t> &from, T &to) override {
                this->safe();
                return PerformanceUtils::timeit<int8_t>([this, &connection, &from, &to]() {
                    return this->doDeserialize(connection, from, to);
                }, [this](int64_t reg) {
                    //TODO save in counters
                    this->logger->info(StringUtils::spf(USS("Counted deserialize in {0} ms"), reg/1000000.0));
                });
            }

            virtual int8_t doSerialize(const Socket& connection, const T &from, std::vector<int8_t> &to) = 0;

            virtual int8_t doDeserialize(const Socket& connection, const std::vector<int8_t> &from, T &to) = 0;

        protected:
            std::shared_ptr<Context> ctx;
        };

        class NNProtocol : public CountedProtocol<NNPayload> {
        public:
            explicit NNProtocol(std::shared_ptr<Context> ctx);
            void onConnect(const Socket &connection) override;
            void onReceive(const Socket &connection, const NNPayload &buffer) override;
            void onReceiveError(const Socket &connection, const std::error_code &error) override;
            void onSendEnd(const std::error_code &error, int64_t bytes_transferred, std::shared_ptr<std::vector<int8_t>> data) override;
            void onConnectError(const Socket &connection, const std::error_code &error) override;
            int8_t doSerialize(const Socket& connection, const NNPayload &from, std::vector<int8_t> &to) override;
            int8_t doDeserialize(const Socket& connection, const std::vector<int8_t> &from, NNPayload &to) override;

        protected:
            virtual int8_t acceptEnableSync();
            virtual int8_t acceptSync();
            virtual int8_t acceptTerminate();
            virtual int8_t acceptFlow(const NNFlow& flow);
            virtual int8_t acceptAck(const icu::UnicodeString& message);
            virtual int8_t acceptStatus(const NNStatus& status);
            virtual int8_t acceptState(std::shared_ptr<ByteCode> state);
            virtual int8_t acceptInitialState(std::shared_ptr<ByteCode> state);

        private:
            template<typename T>
            int8_t serialize(const T &from, std::vector<int8_t> &to) {
                this->safe();
                auto ss = std::make_shared<std::stringstream>();
                auto res = serialize(from, ss);
                BinaryUtils::asVector(*ss, to);
                return res;
            }

            int8_t serialize(const NNCounterStatus &from, std::shared_ptr<std::ostream> to);
            int8_t serialize(const NNStatus &from, std::shared_ptr<std::ostream> to);
            int8_t serialize(const std::shared_ptr<ByteCode> &from, std::shared_ptr<std::ostream> to);
            int8_t serialize(const NNFlow &from, std::shared_ptr<std::ostream> to);
            int8_t serialize(const icu::UnicodeString &from, std::shared_ptr<std::ostream> to);
            int8_t serialize(const int64_t &from, std::shared_ptr<std::ostream> to);
            int8_t serialize(const int32_t &from, std::shared_ptr<std::ostream> to);
            int8_t serialize(const int16_t &from, std::shared_ptr<std::ostream> to);
            int8_t serialize(const int8_t &from, std::shared_ptr<std::ostream> to);
            int8_t serialize(const double &from, std::shared_ptr<std::ostream> to);

            template<typename T>
            int8_t deserialize(const std::vector<int8_t> &from, T &to) {
                this->safe();
                auto ss = std::make_shared<std::stringstream>();
                BinaryUtils::asStream(from, *ss);
                auto res = deserialize(ss, to);
                return res;
            }

            int8_t deserialize(std::shared_ptr<std::istream> from, NNCounterStatus &to);
            int8_t deserialize(std::shared_ptr<std::istream> from, NNStatus &to);
            int8_t deserialize(std::shared_ptr<std::istream> from, std::shared_ptr<ByteCode> &to);
            int8_t deserialize(std::shared_ptr<std::istream> from, NNFlow &to);
            int8_t deserialize(std::shared_ptr<std::istream> from, icu::UnicodeString &to);
            int8_t deserialize(std::shared_ptr<std::istream> from, int64_t &to);
            int8_t deserialize(std::shared_ptr<std::istream> from, int32_t &to);
            int8_t deserialize(std::shared_ptr<std::istream> from, int16_t &to);
            int8_t deserialize(std::shared_ptr<std::istream> from, int8_t &to);
            int8_t deserialize(std::shared_ptr<std::istream> from, double &to);

            void sendAck(const Socket &connection);

            NNCounterStatus fillCounterStatus(Counter c);
            NNStatus fillStatus();
            NNFlow fillFlow(const std::map<icu::UnicodeString, std::vector<std::tuple<icu::UnicodeString, double, double, double> > >& streamBatch);
        };
    }
}