/*
 Copyright 2013-2016 Sergey Ionov

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#pragma once

#include "predef.h"

namespace nnvm {
    NNVM_EXPORTED class ByteCodeLoader;
}

#include <vector>
#include <functional>
#include <map>
#include <set>
#include <unicode/unistr.h>
#include <memory>
#include <fstream>
#include <Disposable.hpp>
#include <ByteCode.hpp>
#include <MachineSetup.hpp>
#include <LoaderModel.hpp>
#include <ByteCodeParser.hpp>
#include <LibraryLoader.hpp>

namespace nnvm {
    typedef std::function<void(const std::wstring& libraryPath, const std::wstring& packagePath, const std::wstring& blockPath, const std::wstring& block, std::wstring& fullpath, std::shared_ptr<std::ifstream>& stream, int8_t& is_block, int64_t& err)> ProbeDelegate;

    class ByteCodeLoader : public Disposable, public std::enable_shared_from_this<ByteCodeLoader>
    {
    public:
        ByteCodeLoader(std::shared_ptr<MachineSetup> setup, std::shared_ptr<LibraryLoader> library);
        virtual ~ByteCodeLoader();
        std::shared_ptr<ByteCode> load(const icu::UnicodeString& package, std::shared_ptr<Context> ctx);
        std::shared_ptr<std::map<icu::UnicodeString, std::shared_ptr<ByteCodeBlock>>> mergeByteCodes(const std::vector<std::shared_ptr<ByteCode>>& codes);
        std::shared_ptr<std::set<icu::UnicodeString>> extractNeurons(const std::vector<std::shared_ptr<ByteCode>>& codes);

        static std::wstring sourceExt;
        static std::wstring jsonExt;
        static std::wstring binExt;

    protected:
        std::shared_ptr<MachineSetup> setup;
        std::shared_ptr<LibraryLoader> library;
        std::vector<ProbeDelegate> fileProbes;
    };
}
