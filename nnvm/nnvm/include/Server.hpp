/*
 Copyright 2013-2016 Sergey Ionov

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#pragma once

#include "predef.h"

namespace nnvm {
    namespace grid {
        NNVM_EXPORTED class Server;
        NNVM_EXPORTED class Client;
    }
}

#include <thread>
#include <memory>
#include <Disposable.hpp>
#include <MachineSetup.hpp>
#include <LoaderModel.hpp>
#include <Protocol.hpp>

namespace nnvm {
    namespace grid {
        class Server : public Disposable
        {
        public:
            Server(std::shared_ptr<Context> ctx, std::shared_ptr<Protocol> protocol);
            ~Server() override;
            void stop();
            int8_t isRunning();
        private:
            Server(const Server&);
            void operator=(const Server&);
            ServerContext serverData;
            std::shared_ptr<Context> ctx;
            std::shared_ptr<Logger> logger;
            std::shared_ptr<Protocol> protocol;
            std::thread serviceThread;
            std::vector<std::thread> servingThreads;
            std::function<void()> stopAction;
            int8_t running;
            int8_t servicesUp;
            int8_t threadsUp;
            int8_t stopping;
            std::vector<int8_t> runningSize;
            int32_t previousSum;
            void runServer();
        };

        class Client : public Disposable
        {
        public:
            Client(ProtocolType type, const icu::UnicodeString& address, int32_t port, std::shared_ptr<Logger> logger, std::shared_ptr<Protocol> protocol, const ServerContext& runningContext);
            Client(ProtocolType type, const Socket& socket, std::shared_ptr<Logger> logger, std::shared_ptr<Protocol> protocol, const ServerContext& runningContext);
            ~Client() override;
            std::shared_ptr<Connection> getConnection();
            void handleSend(std::shared_ptr<std::vector<int8_t>> data);
            ProtocolType type;
            Socket socket;
            std::shared_ptr<Logger> logger;
            std::shared_ptr<Protocol> protocol;
        private:
            std::shared_ptr<Connection> connection;
        };
    }
}
