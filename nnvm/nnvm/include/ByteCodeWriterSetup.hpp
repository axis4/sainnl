/*
 Copyright 2013-2016 Sergey Ionov
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#pragma once

#include "predef.h"

namespace nnvm {
    NNVM_EXPORTED class ByteCodeWriterSetup;
}

#include <map>
#include <memory>
#include <unicode/unistr.h>
#include <Disposable.hpp>
#include <WritableNeuron.hpp>

namespace nnvm {
    class ByteCodeWriterSetup : public Disposable
    {
    public:
        ByteCodeWriterSetup();
        ByteCodeWriterSetup(const ByteCodeWriterSetup& obj);
        virtual ~ByteCodeWriterSetup();
        void addNeuron(const icu::UnicodeString& name, std::shared_ptr<WritableNeuron> code);
        std::shared_ptr<WritableNeuron> getNeuron(const icu::UnicodeString& name);
        std::map<icu::UnicodeString, std::shared_ptr<WritableNeuron> >::iterator getNeurons();
        std::map<icu::UnicodeString, std::shared_ptr<WritableNeuron> >::iterator getNeuronsEnd();
    protected:
        std::map<icu::UnicodeString, std::shared_ptr<WritableNeuron> > neurons;
    };
}
